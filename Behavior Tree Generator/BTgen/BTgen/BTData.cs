﻿using System;

namespace BTgen
{

    public class BTData
    {
        // types for checking rules
        public enum NodeType
        {
            TYPE_LEAF,
            TYPE_SELECTOR,
            TYPE_DECORATOR
        };

        public BTData(NodeType type) 
        {
            Type_ = type;
            numChildren_ = 0;
            name_ = "";
            StrType_ = "";
        }

        public string RuleCheck()
        {
            // add check for supported types!

            switch (Type_)
            {
                case NodeType.TYPE_SELECTOR:
                    if (numChildren_ < 1)
                        return "Design Violation: Node '" + name_ + "' must have at least 1 child";
                    break;
                case NodeType.TYPE_DECORATOR:
                    if (numChildren_ != 1)
                        return "Design Violation: Node '" + name_ + "' must have only 1 child";
                    break;
                case NodeType.TYPE_LEAF:
                    if (numChildren_ != 0)
                        return "Design Violation: Node '" + name_ + "' can't have children";
                    break;
            }
            return "";
        }

        public NodeType Type_;
        public string StrType_;
        public int numChildren_;
        public string name_;
    }

}
