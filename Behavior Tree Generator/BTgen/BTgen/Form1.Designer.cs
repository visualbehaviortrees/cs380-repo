﻿namespace BTgen
{
    partial class BT_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainTreeView = new System.Windows.Forms.TreeView();
            this.AddChildButton = new System.Windows.Forms.Button();
            this.typeComboBox = new System.Windows.Forms.ComboBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.RemoveButton = new System.Windows.Forms.Button();
            this.LeafComboBox = new System.Windows.Forms.ComboBox();
            this.RuleCheckButton = new System.Windows.Forms.Button();
            this.GenerateButton = new System.Windows.Forms.Button();
            this.AddRootButton = new System.Windows.Forms.Button();
            this.SelectorComboBox = new System.Windows.Forms.ComboBox();
            this.DecoratorComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // mainTreeView
            // 
            this.mainTreeView.Location = new System.Drawing.Point(0, 0);
            this.mainTreeView.Name = "mainTreeView";
            this.mainTreeView.Size = new System.Drawing.Size(539, 1003);
            this.mainTreeView.TabIndex = 0;
            // 
            // AddChildButton
            // 
            this.AddChildButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.AddChildButton.Location = new System.Drawing.Point(559, 348);
            this.AddChildButton.Name = "AddChildButton";
            this.AddChildButton.Size = new System.Drawing.Size(136, 35);
            this.AddChildButton.TabIndex = 1;
            this.AddChildButton.Text = "Add Child";
            this.AddChildButton.UseVisualStyleBackColor = true;
            this.AddChildButton.Click += new System.EventHandler(this.AddChildButton_Click);
            // 
            // typeComboBox
            // 
            this.typeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typeComboBox.Location = new System.Drawing.Point(560, 59);
            this.typeComboBox.Name = "typeComboBox";
            this.typeComboBox.Size = new System.Drawing.Size(168, 21);
            this.typeComboBox.TabIndex = 2;
            this.typeComboBox.SelectedIndexChanged += new System.EventHandler(this.typeComboBox_SelectedIndexChanged);
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(559, 280);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(168, 20);
            this.nameTextBox.TabIndex = 3;
            // 
            // RemoveButton
            // 
            this.RemoveButton.Location = new System.Drawing.Point(610, 400);
            this.RemoveButton.Name = "RemoveButton";
            this.RemoveButton.Size = new System.Drawing.Size(178, 35);
            this.RemoveButton.TabIndex = 4;
            this.RemoveButton.Text = "Remove";
            this.RemoveButton.UseVisualStyleBackColor = true;
            this.RemoveButton.Click += new System.EventHandler(this.RemoveButton_Click);
            // 
            // LeafComboBox
            // 
            this.LeafComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.LeafComboBox.DropDownWidth = 230;
            this.LeafComboBox.FormattingEnabled = true;
            this.LeafComboBox.Location = new System.Drawing.Point(559, 221);
            this.LeafComboBox.Name = "LeafComboBox";
            this.LeafComboBox.Size = new System.Drawing.Size(168, 21);
            this.LeafComboBox.TabIndex = 5;
            this.LeafComboBox.SelectedIndexChanged += new System.EventHandler(this.LeafComboBox_SelectedIndexChanged);
            // 
            // RuleCheckButton
            // 
            this.RuleCheckButton.Location = new System.Drawing.Point(610, 520);
            this.RuleCheckButton.Name = "RuleCheckButton";
            this.RuleCheckButton.Size = new System.Drawing.Size(178, 35);
            this.RuleCheckButton.TabIndex = 6;
            this.RuleCheckButton.Text = "Rule Check";
            this.RuleCheckButton.UseVisualStyleBackColor = true;
            this.RuleCheckButton.Click += new System.EventHandler(this.RuleCheckButton_Click);
            // 
            // GenerateButton
            // 
            this.GenerateButton.Location = new System.Drawing.Point(610, 571);
            this.GenerateButton.Name = "GenerateButton";
            this.GenerateButton.Size = new System.Drawing.Size(178, 36);
            this.GenerateButton.TabIndex = 7;
            this.GenerateButton.Text = "Generate Trees";
            this.GenerateButton.UseVisualStyleBackColor = true;
            this.GenerateButton.Click += new System.EventHandler(this.GenerateButton_Click);
            // 
            // AddRootButton
            // 
            this.AddRootButton.Location = new System.Drawing.Point(701, 348);
            this.AddRootButton.Name = "AddRootButton";
            this.AddRootButton.Size = new System.Drawing.Size(131, 35);
            this.AddRootButton.TabIndex = 8;
            this.AddRootButton.Text = "Add Root";
            this.AddRootButton.UseVisualStyleBackColor = true;
            this.AddRootButton.Click += new System.EventHandler(this.AddRootButton_Click);
            // 
            // SelectorComboBox
            // 
            this.SelectorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SelectorComboBox.FormattingEnabled = true;
            this.SelectorComboBox.Location = new System.Drawing.Point(560, 113);
            this.SelectorComboBox.Name = "SelectorComboBox";
            this.SelectorComboBox.Size = new System.Drawing.Size(169, 21);
            this.SelectorComboBox.TabIndex = 9;
            this.SelectorComboBox.SelectedIndexChanged += new System.EventHandler(this.SelectorComboBox_SelectedIndexChanged);
            // 
            // DecoratorComboBox
            // 
            this.DecoratorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.DecoratorComboBox.FormattingEnabled = true;
            this.DecoratorComboBox.Location = new System.Drawing.Point(560, 165);
            this.DecoratorComboBox.Name = "DecoratorComboBox";
            this.DecoratorComboBox.Size = new System.Drawing.Size(168, 21);
            this.DecoratorComboBox.TabIndex = 10;
            this.DecoratorComboBox.SelectedIndexChanged += new System.EventHandler(this.DecoratorComboBox_SelectedIndexChanged);
            // 
            // BT_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(912, 1002);
            this.Controls.Add(this.DecoratorComboBox);
            this.Controls.Add(this.SelectorComboBox);
            this.Controls.Add(this.AddRootButton);
            this.Controls.Add(this.GenerateButton);
            this.Controls.Add(this.RuleCheckButton);
            this.Controls.Add(this.LeafComboBox);
            this.Controls.Add(this.RemoveButton);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.typeComboBox);
            this.Controls.Add(this.AddChildButton);
            this.Controls.Add(this.mainTreeView);
            this.Name = "BT_form";
            this.Text = "BT Generator";
            this.Load += new System.EventHandler(this.BTgen_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView mainTreeView;
        private System.Windows.Forms.Button AddChildButton;
        private System.Windows.Forms.ComboBox typeComboBox;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Button RemoveButton;
        private System.Windows.Forms.ComboBox LeafComboBox;
        private System.Windows.Forms.Button RuleCheckButton;
        private System.Windows.Forms.Button GenerateButton;
        private System.Windows.Forms.Button AddRootButton;
        private System.Windows.Forms.ComboBox SelectorComboBox;
        private System.Windows.Forms.ComboBox DecoratorComboBox;
    }
}

