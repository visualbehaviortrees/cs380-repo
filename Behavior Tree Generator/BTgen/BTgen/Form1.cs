﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BTgen
{
    public partial class BT_form : Form
    {
        public BT_form()
        {
            InitializeComponent();
        }

        private void AddChildButton_Click(object sender, EventArgs e)
        {
            string nodeName = nameTextBox.Text.ToString();

            if (nodeName == "")
            {
                ViolationMsg("Design Violation: Missing Node Name");
                return;
            }

            if(typeComboBox.Text == "")
            {
                ViolationMsg("Design Violation: Node Type not selected");
                return;
            }

            TreeNode newNode = new TreeNode();

            switch (typeComboBox.Text)
            {
                case "Selectors":
                    if(ValidParent())
                    {
                        if (SelectorComboBox.Text != "")
                        {
                            newNode.Tag = new BTData(BTData.NodeType.TYPE_SELECTOR);
                            newNode.Text = nodeName + " <" + SelectorComboBox.Text + ">";
                            ((BTData)newNode.Tag).StrType_ = SelectorComboBox.Text;
                            ((BTData)newNode.Tag).name_ = nodeName;
                        }
                        else
                        {
                            ViolationMsg("Design Violation: Selector Type not selected");
                            return;
                        }
                    }
                    else
                    {
                        return;
                    }
                    break;
                case "Decorators":
                    if(ValidParent())
                    {
                        if (DecoratorComboBox.Text != "")
                        {
                            newNode.Tag = new BTData(BTData.NodeType.TYPE_DECORATOR);
                            newNode.Text = nodeName + " <" + "Decorator" + DecoratorComboBox.Text + ">";
                            ((BTData)newNode.Tag).StrType_ = "Decorator" + DecoratorComboBox.Text;
                            ((BTData)newNode.Tag).name_ = nodeName;
                        }
                        else
                        {
                            ViolationMsg("Design Violation: Decorator Type not selected");
                            return;
                        }
                    }
                    else
                    {
                        return;
                    }
                    break;
                case "Leaves":
                    if (ValidParent())
                    {
                        if (LeafComboBox.Text != "")
                        {
                            newNode.Tag = new BTData(BTData.NodeType.TYPE_LEAF);
                            newNode.Text = nodeName + " <" + LeafComboBox.Text + ">";
                            ((BTData)newNode.Tag).StrType_ = "Leaf";
                            ((BTData)newNode.Tag).name_ = LeafComboBox.Text;
                        }
                        else
                        {
                            ViolationMsg("Design Violation: Leaf Type not selected");
                            return;
                        }
                    }
                    else
                    {
                        return;
                    }
                    break;
            }

            // add child node
            mainTreeView.SelectedNode.Nodes.Add(newNode);
            ++((BTData)mainTreeView.SelectedNode.Tag).numChildren_;
        }

        private void AddRootButton_Click(object sender, EventArgs e)
        {
            string nodeName = nameTextBox.Text.ToString();

            if (nodeName == "")
            {
                ViolationMsg("Design Violation: Missing Node Name");
                return;
            }

            if (typeComboBox.Text != "Selectors")
            {
                ViolationMsg("Design Violation: Root nodes must be of Node Type 'Selectors'");
                return;
            }

            if(SelectorComboBox.Text == "")
            {
                ViolationMsg("Design Violation: Selector Type not selected");
                return;
            }

            TreeNode newNode = new TreeNode();
            newNode.Tag = new BTData(BTData.NodeType.TYPE_SELECTOR);
            newNode.Text = nodeName + " <" + SelectorComboBox.Text + ">";
            ((BTData)newNode.Tag).StrType_ = SelectorComboBox.Text;
            ((BTData)newNode.Tag).name_ = nodeName;
            mainTreeView.Nodes.Add(newNode);
        }

        // remove selected node and all children
        private void RemoveButton_Click(object sender, EventArgs e)
        {
            if (mainTreeView.SelectedNode != null)
            {
                if (mainTreeView.SelectedNode.Parent != null)
                    --((BTData)mainTreeView.SelectedNode.Parent.Tag).numChildren_;
                mainTreeView.Nodes.Remove(mainTreeView.SelectedNode);
            }
        }

        // check to make sure no rules are violated
        private void RuleCheckButton_Click(object sender, EventArgs e)
        {
            int items = mainTreeView.Nodes.Count;
            for (int i = 0; i < items; ++i) // for all roots
            {
                RuleCheckRec(mainTreeView.Nodes[i]);
            }
        }

        // recursive rule check for traversing the treeview
        private bool RuleCheckRec(TreeNode parent)
        {
            bool ret = true; ;
            int items = parent.Nodes.Count;

            string result = ((BTData)parent.Tag).RuleCheck();
            if (result != "")
            {
                ret = false;
                ViolationMsg(result);
            }

            for(int i = 0; i < items; ++i) // for all children
            {
                RuleCheckRec(parent.Nodes[i]);
            }
            return ret;
        }

        // generate text file for all behavior trees
        private void GenerateButton_Click(object sender, EventArgs e)
        {
            int items = mainTreeView.Nodes.Count;
            for (int i = 0; i < items; ++i) // for all roots, generate a tree file
            {
                if (!RuleCheckRec(mainTreeView.Nodes[i]))
                {
                    ViolationMsg("Failed to generate " + ((BTData)mainTreeView.Nodes[i].Tag).name_ + ".bhv");
                    return;
                }

                string path = System.IO.Path.GetDirectoryName(Application.ExecutablePath) + "\\" + ((BTData)mainTreeView.Nodes[i].Tag).name_ + ".bhv";
                System.IO.FileStream fout = new System.IO.FileStream(path, System.IO.FileMode.Create); // open file named after the root
                GenerateTreeRec(0, mainTreeView.Nodes[i], fout);
                fout.Close();
                CompleteMsg("Successfully generated " + ((BTData)mainTreeView.Nodes[i].Tag).name_ + ".bhv");
            }
        }

        // recursive behavior tree text generation
        private void GenerateTreeRec(int depth, TreeNode parent, System.IO.FileStream fout)
        {
            string tabString = "";
            for (int j = 0; j < depth; ++j)  // number of tabs depends on depth
                tabString += "\t";

            byte[] nodeInfo = System.Text.Encoding.ASCII.GetBytes(tabString + ((BTData)parent.Tag).name_ + " " + ((BTData)parent.Tag).StrType_ + "\n"); // output string
            fout.Write(nodeInfo, 0, nodeInfo.Length);

            // recursion to children
            int items = parent.Nodes.Count;
            for(int i = 0; i < items; ++i)
            {
                GenerateTreeRec(depth + 1, parent.Nodes[i], fout);
            }
        }

        private bool ValidParent()
        {
            if (mainTreeView.SelectedNode == null)
            {
                ViolationMsg("Design Violation: No parent Node selected");
                return false;
            }
            BTData parent = (BTData)mainTreeView.SelectedNode.Tag;
            if (parent.Type_ == BTData.NodeType.TYPE_LEAF)
            {
                ViolationMsg("Design Violation: Node of type Leaf can't have children");
                return false;
            }

            if (parent.Type_ == BTData.NodeType.TYPE_DECORATOR
               && parent.numChildren_ == 1)
            {
                ViolationMsg("Design Violation: Node of type Decorator must have only 1 child");
                return false;
            }
            return true;
        }
 
        private void BTgen_Load(object sender, EventArgs e)
        {
            UI_Load();
            AcquireInfo();
            System.Drawing.Graphics graphics = this.CreateGraphics();
        }

        private void UI_Load()
        {
            drawFont_ = new System.Drawing.Font("Calibri", 16);
            // text
            mainTreeView.Font = drawFont_;
            typeComboBox.Font = drawFont_;
            SelectorComboBox.Font = drawFont_;
            DecoratorComboBox.Font = drawFont_;
            LeafComboBox.Font = drawFont_;
            nameTextBox.Font = drawFont_;
            AddChildButton.Font = drawFont_;
            AddRootButton.Font = drawFont_;
            RemoveButton.Font = drawFont_;
            RuleCheckButton.Font = drawFont_;
            GenerateButton.Font = drawFont_;
            // visibility
            SelectorComboBox.Visible = false;
            DecoratorComboBox.Visible = false;
            LeafComboBox.Visible = false;
        }

        // read from nodes.vbt to get info for supported nodes
        private void AcquireInfo()
        {
            string NodeType = "";
            string path = System.IO.Path.GetDirectoryName(Application.ExecutablePath) + "\\nodes.vbt";
            try
            {
                if (System.IO.File.Exists(path))
                {
                    System.IO.StreamReader rin = new System.IO.StreamReader(path);
                    DecoratorDescriptors_ = new List<string>();
                    SelectorDescriptors_ = new List<string>();
                    LeafDescriptors_ = new List<string>();
                    while (rin.Peek() >= 0)
                    {
                        string nodeInfo =  rin.ReadLine();
                        // parse line
                        if (nodeInfo[0] != '\t')  // this line is a node super type
                        {
                            NodeType = nodeInfo;
                            typeComboBox.Items.Add(NodeType);
                        }
                        else
                        {
                            // get type name string
                            string nodeName = "";
                            for (int i = 1; i < nodeInfo.Length; ++i)
                            {
                                if(nodeInfo[i] == ' ' || nodeInfo[i] == '\n')
                                    break;
                                nodeName += nodeInfo[i];
                            }
                            
                            // get descriptor string
                            bool dstart = false;
                            string nodeDesc = "";
                            for (int i = 0; i < nodeInfo.Length; ++i)
                            {
                                if (nodeInfo[i] == '"')
                                    dstart = !dstart;
                                else if (dstart)
                                   nodeDesc += nodeInfo[i];
                            }
                            
                            // add name and descriptor to appropriate list
                            if (NodeType == "Selectors")
                            {
                                SelectorComboBox.Items.Add(nodeName);
                                SelectorDescriptors_.Add(nodeDesc);
                            }
                            else if (NodeType == "Decorators")
                            {
                                DecoratorComboBox.Items.Add(nodeName.Substring(9));
                                DecoratorDescriptors_.Add(nodeDesc);
                            }
                            else // Leaf
                            {
                                LeafComboBox.Items.Add(nodeName);
                                LeafDescriptors_.Add(nodeDesc);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e); // base paint method

            // Draw static text
            UI_DrawText();
        }

        private void UI_DrawText()
        {
            // Based on msdn.microsoft.com example of DrawString to windows form
            // MUST BE CALLED IN OnPaint TO UPDATE FORM PROPERLY
            System.Drawing.Graphics formGraphics = this.CreateGraphics();
            System.Drawing.SolidBrush drawBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
            System.Drawing.StringFormat drawFormat = new System.Drawing.StringFormat();

            //
            // Draw text above add node options (above type combo box): "New Node Options"
            //
            string drawString = "New Node Options:";
            Point pointTCB = typeComboBox.Location;   // get location of the type ComboBox
            Size sizeTCB = typeComboBox.Size;         // get size of the type ComboBox
            formGraphics.DrawString(drawString, drawFont_, drawBrush, pointTCB.X, pointTCB.Y - 40.0f, drawFormat); // draw text offset from the combo box

            //
            // Draw text : "Desciptions:"
            //
            drawString = "Description:";
            Point pointDesc = new Point(560, 650);   // get location of the type ComboBox
            formGraphics.DrawString(drawString, drawFont_, drawBrush, pointDesc.X, pointDesc.Y, drawFormat); // draw text offset from the combo box

            //
            // Draw text by node type combo box: "Node Type"
            //
            drawString = "Node Type";
            formGraphics.DrawString(drawString, drawFont_, drawBrush, pointTCB.X + sizeTCB.Width, pointTCB.Y, drawFormat); // draw text offset from the combo box

            //
            // Draw text by node type combo box: "Selector Type"
            //
            drawString = "Selector Type";
            Point pointSCB = SelectorComboBox.Location;
            formGraphics.DrawString(drawString, drawFont_, drawBrush, pointSCB.X + sizeTCB.Width, pointSCB.Y, drawFormat); // draw text offset from the combo box

            //
            // Draw text by node type combo box: "Decorator Type"
            //
            drawString = "Decorator Type";
            Point pointDCB = DecoratorComboBox.Location;
            formGraphics.DrawString(drawString, drawFont_, drawBrush, pointDCB.X + sizeTCB.Width, pointDCB.Y, drawFormat); // draw text offset from the combo box

            //
            // Draw text by leaf combo box: "Leaf Node"
            //
            drawString = "Leaf Node";
            Point pointLCB = LeafComboBox.Location;
            formGraphics.DrawString(drawString, drawFont_, drawBrush, pointLCB.X + sizeTCB.Width, pointLCB.Y, drawFormat); // draw text offset from the combo box

            //
            // Draw text by node name text box: "Node Name"
            //
            drawString = "Node Name";
            Point pointNTB = nameTextBox.Location;
            formGraphics.DrawString(drawString, drawFont_, drawBrush, pointNTB.X + sizeTCB.Width, pointNTB.Y, drawFormat); // draw text offset from the combo box

            drawBrush.Dispose();
            formGraphics.Dispose();
        }

        private void UpdateDescriptor()
        {
            System.Drawing.Graphics descGraphics = this.CreateGraphics();
            System.Drawing.SolidBrush descBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
            System.Drawing.StringFormat descFormat = new System.Drawing.StringFormat();
            System.Drawing.Font descFont = new System.Drawing.Font("Calibri", 14);

            descGraphics.Clear(this.BackColor);

            if (SelectorComboBox.Visible)
            {
                string description1 = SelectorDescriptors_[SelectorComboBox.SelectedIndex];
                RectangleF rect1 = new RectangleF(560, 700, 300, 200);
                descGraphics.DrawString(description1, descFont, descBrush, rect1);
            }
            else if (DecoratorComboBox.Visible)
            {
                string description1 = DecoratorDescriptors_[DecoratorComboBox.SelectedIndex];
                RectangleF rect1 = new RectangleF(560, 700, 300, 200);
                descGraphics.DrawString(description1, descFont, descBrush, rect1);
            }
            else if (LeafComboBox.Visible)
            {
                string description1 = LeafDescriptors_[LeafComboBox.SelectedIndex];
                RectangleF rect1 = new RectangleF(560, 700, 300, 200);
                descGraphics.DrawString(description1, descFont, descBrush, rect1);
            }

            UI_DrawText();
        }

        private void ViolationMsg(string message)
        {
            MessageBox.Show( message,
                             "Error",
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Warning );
        }

        private void CompleteMsg(string message)
        {
            MessageBox.Show(message,
                             "Complete",
                             MessageBoxButtons.OK);
        }

        private System.Drawing.Font drawFont_;
        private List<string> SelectorDescriptors_;
        private List<string> DecoratorDescriptors_;
        private List<string> LeafDescriptors_;

        private void typeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectorComboBox.Visible = false;
            DecoratorComboBox.Visible = false;
            LeafComboBox.Visible = false;

            if (typeComboBox.Text == "Selectors")
            {
                SelectorComboBox.Visible = true;
            }
            else if (typeComboBox.Text == "Decorators")
            {
                DecoratorComboBox.Visible = true;
            }
            else if(typeComboBox.Text == "Leaves")
            {
                LeafComboBox.Visible = true;
            }
        }

        private void SelectorComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateDescriptor();
        }

        private void DecoratorComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateDescriptor();
        }

        private void LeafComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateDescriptor();
        }
    }
}
