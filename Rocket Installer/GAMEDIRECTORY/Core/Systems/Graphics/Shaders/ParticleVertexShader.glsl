#version 330

in vec3 position;
in vec4 color;

out vec4 Color;

uniform mat4 wv_m;
uniform mat4 vp_m;

void main ()
{
  Color = color;

  vec4 eyePos = wv_m * vec4 (position, 1.0);
  gl_Position =  eyePos * vp_m;

  float dist = length (eyePos.xyz);
  //float att = inversesqrt (0.1f * dist);
  //gl_PointSize = 10 * att;
}