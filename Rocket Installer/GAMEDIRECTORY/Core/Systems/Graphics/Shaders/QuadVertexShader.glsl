#version 330

in vec3 position;
in vec4 color;
in vec2 texcoord;

out vec4 Color;
out vec2 Texcoord;

uniform mat4 mvp;

void main ()
{
  gl_Position = mvp * vec4 (position, 1.0);
  Color = color;
  Texcoord = texcoord;
}