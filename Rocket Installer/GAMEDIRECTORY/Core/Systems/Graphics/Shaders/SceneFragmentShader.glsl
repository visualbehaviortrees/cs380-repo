#version 330

in vec3 Eye;
in vec2 Texcoord;

uniform mat4 Matrix;

// OUTPUT COLOR AFTER ALL LIGHTING CALCULATIONS
out vec4 outColor;

// TEXTURE TO BE MAPPED ON QUAD
uniform sampler2D image;
uniform sampler2D uniNormalMap;

uniform vec3 lightpos[150];
uniform float intensity [150];

// MATERIAL PROPERTIES OF SELF
uniform vec3 mambient = vec3(0.0, 0.0, 0.0);
uniform vec3 mdiffuse = vec3 (0.6, 0.6, 0.6);
uniform vec3 mspecular = vec3 (1, 1, 1);
uniform float shininess = 10;
uniform float drawornot = 1.0;

// MATERIAL PROPERTIES OF LIGHT
uniform vec3 lambient = vec3 (0.6, 0.6, 0.6);
uniform vec3 ldiffuse = vec3 (0.6, 0.6, 0.6);
uniform vec3 lspecular[150];
uniform int numLights;

uniform int uniValue = 0;

void main()
{
	vec3 Normal =  normalize (texture2D (uniNormalMap, Texcoord).rgb);
	Normal.r -= 0.5;
	Normal.g -= 0.5;
	//Normal.b = -Normal.b;
	Normal.b = -1.0;

	//Normal = normalize (Normal);

	if (uniValue == 0)
	{
		Normal = vec3 (0,0,-1);
	}
	//vec3 l = vec3 (0,0,0);
	vec4 fragColor = vec4 (0,0,0,0);

	for(int i = 0; i < numLights; ++i)
	{
	    vec4 WorldPos = Matrix * vec4(lightpos[i], 1.0f);
		vec3 Pos = vec3(WorldPos.x * 2.0f, WorldPos.y * 2.0f, WorldPos.z);

		//distance from light-source to surface
		float dist = length (Pos - Eye);
		dist = normalize(dist);

		// calculate attentuation using distance from light
		float att = 1 / (1.0 + 0.1 * dist + 0.01 * dist * dist);

		//the ambient light
		vec3 ambient = mambient * lambient;

		// calculate diffuse color
		vec3 surf2light = normalize (Pos - Eye);
		vec3 norm = normalize (Normal);
		float dcont = max (0.0, dot (vec3 (0,0,-1), surf2light));
		vec3 diffuse = dcont * (mdiffuse * ldiffuse);

		// calculate specular color
		vec3 surf2view = normalize (-Eye);
		vec3 reflection = reflect (-surf2light, norm);
 
		float scont = pow (max (0.0, dot(surf2view, reflection)), intensity[i]);

		vec3 specular = scont * lspecular[i] * mspecular;

		fragColor += vec4 ((ambient +  diffuse + specular) * att, 1.0);
	}

	// calculate resulting color
	if(drawornot == 1.0)
	  outColor = texture (image, Texcoord) * fragColor;
	else
	  outColor = texture (image, Texcoord);


	 // outColor = texture2D (uniNormalMap, Texcoord);
}