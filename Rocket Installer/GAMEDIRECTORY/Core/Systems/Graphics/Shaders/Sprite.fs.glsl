#version 330

in vec4 Color;
in vec2 Texcoord;

layout (location=0) out vec4 outColor;
layout (location=1) out vec4 outColorNormal;

uniform sampler2D uniTexture;
uniform sampler2D uniNormalTexture;
uniform int uniUseNormal = 1;

void main ()
{
  outColor = texture2D (uniTexture, Texcoord) * Color;
  if (uniUseNormal == 1)
  {
	outColorNormal = texture2D (uniNormalTexture, Texcoord);
  }
  else
  {
	outColorNormal = texture2D (uniTexture, Texcoord);
  }
}