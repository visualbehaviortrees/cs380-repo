#version 330

in mat4 modelMatrix;
in vec4 color;
in vec4 texcoord;

out mat4 gModelMatrix;
out vec4 gColor;
out vec4 gTexcoord;

void main ()
{
  gl_Position = vec4 (0,0,0,1);

  gModelMatrix = modelMatrix;
  gColor = color;
  gTexcoord = texcoord;
}