Steps to changing the Installer:

0. Install inno at http://www.jrsoftware.org/isdl.php
1. Add any new resources to GAMEDIRECTORY\ *rest of path here*
2. Add the new release build executable to GAMEDIRECTORY
3. Delete old .exe
4. Rename .exe to "BreakfastApocalypse.exe"
5. Delete old Installer from INSTALLER\
6. Uninstall current version of BreakfastApocalypse
7. Run Inno Setup Script (.iss) in base of Installer
8. Re-Compile the .iss to recreate the Installer
9. Installer now sits in INSTALLER/