//	File name:		Component.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.EC_Parent

#pragma once
#include <iostream>



// When you add a component, add an enum for it here.
// This is used for type introspection
enum EComponent
{
  EC_Alive               = 0,
  EC_Parent              = 1,
  EC_Powerup             = 2,
  EC_RigidBody           = 3,
  EC_Transform           = 4,
  EC_Player              = 5,
  EC_Team                = 6,
  EC_Audio               = 7,
  EC_Bullet              = 8,
  EC_Base                = 9,
  EC_Drawable            = 10,
  EC_Sprite              = 11,
  EC_SpriteText          = 12,
  EC_ParticleEmitter     = 13,
  EC_Flag                = 14,
  EC_Health              = 15,
  EC_Score               = 16,
  EC_Behavior            = 17,
  EC_Turret              = 18,
  EC_PowerupSpawner      = 19,
  EC_ParticleEffect      = 20,
  EC_GUIItem             = 21,
  EC_Light               = 22,
  EC_Door                = 23,
  EC_Death               = 24,
  EC_Minimap             = 25,
  EC_BulletEffect        = 26,
  EC_HUDElement          = 27,
  EC_Key                 = 28,
  EC_MenuButtonComponent = 29,
  EC_Script              = 30,
  EC_Movement			 = 31,

  EC_Max
};

// When you add a component, add a make enum for it here.
// This is used to rapidly determine which components an object has.
enum  MComponent
{
  MC_NONE                = 0,
  MC_Alive               = 1,
  MC_Parent              = 1 << 1,
  MC_Powerup             = 1 << 2,
  MC_RigidBody           = 1 << 3,
  MC_Transform           = 1 << 4,
  MC_Player              = 1 << 5,
  MC_Team                = 1 << 6,
  MC_Audio               = 1 << 7,
  MC_Bullet              = 1 << 8,
  MC_Base                = 1 << 9,
  MC_Drawable            = 1 << 10,
  MC_Sprite              = 1 << 11,
  MC_SpriteText          = 1 << 12,
  MC_ParticleEmitter     = 1 << 13,
  MC_Flag                = 1 << 14,
  MC_Health              = 1 << 15,
  MC_Score               = 1 << 16,
  MC_Behavior            = 1 << 17,
  MC_Turret              = 1 << 18,
  MC_PowerupSpawner      = 1 << 19,
  MC_ParticleEffect      = 1 << 20,
  MC_GUIItem             = 1 << 21,
  MC_Light               = 1 << 22,
  MC_Door                = 1 << 23,
  MC_Death               = 1 << 24,
  MC_Minimap             = 1 << 25,
  MC_BulletEffect        = 1 << 26,
  MC_HUDElement          = 1 << 27,
  MC_Key                 = 1 << 28,
  MC_MenuButtonComponent = 1 << 29,
  MC_Script              = 1 << 30,
  MC_Movement			 = 1 << 31
};

// typedef an unsigned to a mask so it can be expanded later and it's purpose is clear.
typedef unsigned mask;


// define expands into the enums for the component type and flag.
// Should be used when calling the base Component constructor.
#define CMP( C ) \
	EC_##C, MC_##C

class Component;
typedef Component * (*ComponentCreateFunction)(void);
// This is the base class every component should derive from.
// Implement them like velocity and position if you aren't sure.
// If you add logic to them (accessors and mutators are fine) I will destroy you.
class Component
{
public:
/**
 * @brief Initialize a component with its type and mask enums
 * @details Initializer for the component base class. Should be called from derived components.
 * this sets the EComponent and MComponent enums for introspection.
 *
 * @param EC The engines internal type identifier for components.
 * @param mc The engines internal component mask for quickly finding relevant entities.
 */
	Component(EComponent EC, MComponent mc) : _active(true), _type(EC), _mask(mc){}
	/**
	 * @brief Simple debug printing function.
	 * @details This functionc an be used for any debug printing information.
	 * It isn't required, but if you have bugs it is highly encouraged.
	 */
	virtual void Print(void){};
	/**
	 * @brief virtual component destructor.
	 * @details You shouldn't dynamically allocate anything in a component ever.
	 */
	virtual ~Component(){}

	// I will need this for the real engine but BasicEngine doesn't need it.
	//virtual void Init(void) = 0;
/**
 * @brief Frees a component. Currently unused.
 * @details Frees a component without deallocating it. Currently unused.
 */
	virtual void Free(){ _active = false; }

	virtual void Init(){}
/**
 * @brief Sets the state of a component, active or inactive.
 * @details Used to set a component to active or inactive. Setter function.
 * @return Returns a reference to the _active bool of the component.
 */
	bool& Active(){ return _active; }
	/**
	 * @brief Determines the state of a component, active or inactive.
	 * @details Used to determine whether a component is currently active and in use,
	 * or if it is inactive.
	 * @return Returns a const reference to the _active bool of the component.
	 */
	const bool& Active() const { return _active; }
	/**
	 * @brief Gets the type of the component.
	 * @details Gets the enumerated component type of the component instance.
	 * @return Returns a copy of the component type.
	 */
	EComponent Type(void) const { return _type; }
	/**
	 * @brief Gets the Component mask value of the component.
	 * @details Gets the Component Mask value which is used for determining compatibility
	 * of an object with a system.
	 * @return Returns the components mask.
	 */
	MComponent Mask(void) const { return _mask; }
  // static Component* CreateComponent() {return new <thistype>();
	friend class Entity;
private:
	bool _active;
	EComponent _type;
	MComponent _mask;
};

