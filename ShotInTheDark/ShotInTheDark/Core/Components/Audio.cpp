#include <stdafx.h>

#include "Audio.h"


Audio::Audio(int playernum)
	:Component(CMP(Audio)), eventInstance(0), currentEvent(AE_NONE), type(AT_NONE)
{
	switch ( playernum)
	{
		// Purple player
	default:
	case 1:
		audioMap[AE_HURT] = "wizardPainPurple";
		audioMap[AE_ATTACK] = "magicCastPurple";
		audioMap[AE_DIE] = "purpleDie";
		audioMap[AE_JOIN] = "purpleJoin";
		break;
		// Pink player
	case 2:
		audioMap[AE_HURT] = "wizardPainPink";
		audioMap[AE_ATTACK] = "magicCastPink";
		audioMap[AE_DIE] = "pinkDie";
		audioMap[AE_JOIN] = "pinkJoin";
		break;
		// Teal Player
	case 3:
		audioMap[AE_HURT] = "wizardPainTeal";
		audioMap[AE_ATTACK] = "magicCastBlue";
		audioMap[AE_DIE] = "tealDie";
		audioMap[AE_JOIN] = "tealJoin";
		break;
		// Orange Player
	case 4:
		audioMap[AE_HURT] = "wizardPainOrange";
		audioMap[AE_ATTACK] = "magicCastOrange";
		audioMap[AE_DIE] = "orangeDie";
		audioMap[AE_JOIN] = "orangeJoin";
		break;
	}
}



void Audio::Play(AudioEvent e)
{
	currentEvent = e;
	type = AT_PLAY;
}

void Audio::PlayNow(AudioEvent e)
{
	GETSYS(AudioSystem)->PlaySound(audioMap[e]);
	Stop();
}


void Audio::Narrate(AudioEvent e)
{
	currentEvent = e;
	type = AT_NARRATE;
}

void Audio::Stop()
{
	type = AT_NONE;
}

void Audio::Free()
{
	if (eventInstance)
	{
		eventInstance->stop(FMOD_STUDIO_STOP_IMMEDIATE);
	    eventInstance->release();
	}
}