//	File name:		Audio.h
//	Project name:	Shot In The Dark
//	Author(s):		Connor Drose
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../Component.hpp"
#include "../Systems/Audio/AudioSystem.hpp"
#include <vector>

struct Audio : public Component
{
public:
  enum AudioEvent
  {
	  AE_NONE = 0,
	  AE_HURT,
	  AE_ATTACK,
	  AE_DIE,
	  AE_JOIN
  };

  enum AudioPlayType
  {
	  AT_NONE,
	  AT_PLAY,
	  AT_NARRATE
  };

	Audio():Component(CMP(Audio)), eventInstance(0), currentEvent(AE_NONE), type(AT_NONE) {}

	Audio(int playerNum);

	// Plays a sound
	void Play(AudioEvent e);
	
	// Plays a non-looping sound once, in case you're about to be destroyed
	void PlayNow(AudioEvent e);

	// Narrates a sound through the narrator queue
	void Narrate(AudioEvent e);
	// Stops a looping sound
	void Stop();

	void Init(){}
	void Free();
  static Component *CreateComponent() { return new Audio(); }
  
  AudioEvent currentEvent;
  AudioPlayType type;
  FMOD::Studio::EventInstance* eventInstance;

  // A map of which events go to which effects
  std::map<AudioEvent, std::string> audioMap;
};
