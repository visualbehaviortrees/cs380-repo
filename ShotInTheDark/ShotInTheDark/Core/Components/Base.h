//	File name:		Base.h
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once
#include "../Component.hpp"

struct Base : public Component
{
public:
  Base():Component(CMP(Base)){}
  void Init(){}
  void Free(){}
  ~Base(){}

  static Component *CreateComponent() { return new Base(); }


};
