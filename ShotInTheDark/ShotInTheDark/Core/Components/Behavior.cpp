#pragma once

#include "stdafx.h"
#include "Behavior.hpp"
#include "../Systems/AI/BehaviorTree.h"
#include <sstream>
#include <thread>
#include <mutex>
// Don't touch this it's mine - Allan
std::once_flag behaviorTreeSerializeDataHasBeenGenerated;


Behavior::Behavior(const EnemyType& newType) : Component(CMP(Behavior)), root(0), targetID(-1)
{
  enemyType = newType; 
 
  if (enemyType != ET_INVALID)
    Init();
}

void Behavior::Init()
{
	std::call_once(behaviorTreeSerializeDataHasBeenGenerated, GenerateNodesFiles);
}

void Behavior::Free()
{
}

int GetDepth(std::string &line)
{
	int depth = 0;

	for (unsigned int i = 0; i < line.size(); ++i)
	{
		if (line[i] == '\t')
		{
			depth++;
		}
		else
		{
			break;
		}
	}

	return depth;
}

void Behavior::GenerateTreeRecursive(IBTLogicNode * parent, int depth, 
	          std::string &line, std::ifstream &input)
{
	IBTLogicNode *last = 0;

	do
	{
	startLoop:
		int currentDepth = GetDepth(line);

		std::stringstream ss;

		ss.str(line);

		std::string token;
		std::string nodeName;
		std::string nodeType;

		do
		{
			getline(ss, token, '\t');
		} while (token == "");

		std::stringstream ss2;
		ss2.str(token);

		ss2 >> nodeName;
		ss2 >> nodeType;

		if (currentDepth)
		{
			if (currentDepth == depth)
			{
				if (nodeType == "Leaf")
				{
					IBTNode *node = createLeafNode(nodeName, nodeName);
					parent->m_children.push_back(node);
					node->p_parent = parent;
					last = 0;
				}
				else
				{
					IBTLogicNode* node = createLogicNode(nodeType, nodeName);
					parent->m_children.push_back(node);
					node->p_parent = parent;
					last = node;
				}
			}
			else if (currentDepth == depth + 1)
			{
				GenerateTreeRecursive(last, currentDepth, line, input);

				if (currentDepth == 0)
				{
					return;
				}
				else
				{
					if (line == "")
					{
						return;
					}
					else
					{
						goto startLoop;
					}
				}
			}
			else if (currentDepth < depth)
			{
				return;
			}
		}
		else
		{
			root = createLogicNode(nodeType, nodeName);
			last = root;
			root->p_parent = 0;
		}
	} while (getline(input, line));

	line = "";
}

void Behavior::GenerateTree(std::string const &filename)
{
	//Delete root if it exists
	FreeTree();

	std::ifstream input(filename);

	std::string line;
	getline(input, line);

	GenerateTreeRecursive(0, 0, line, input);

	input.close();
}

Behavior::~Behavior()
{
  FreeTree();
}
