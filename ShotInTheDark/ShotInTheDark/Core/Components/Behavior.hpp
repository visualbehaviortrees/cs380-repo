#pragma once
//	File name:		Behavior.h
//	Project name:	Shot In The Dark
//	Author(s):		Connor Drose
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.
#include "..\Core\Component.hpp"

#include "../Systems/AI/BehaviorTree.h"

enum EnemyType
{
	ET_INVALID,
	ET_MAGE,
	ET_THEGUY,
	ET_PLAYER,
	ET_KAMIKZE,
	ET_JUGGERNAUT,
	ET_SNIPER,
	ET_COUNT
};

struct Behavior : public Component
{
public:
  Behavior(const EnemyType& newType = EnemyType::ET_INVALID);
  ~Behavior();

  // allocate custom AI data
  void Init();

  void Free();

  static Component *CreateComponent() { return new Behavior(); }

  void GenerateTree(std::string const &filename);

  void FreeTree()
  {
	  if (root)
	  {
		  delete root;
		  root = 0;
	  }
  }

  //AI type and class
  EnemyType enemyType;

  void Update(Entity *e, float dt)
  {
	  if (root)
	  {
		  root->Update(e, dt);
	  }
  }

  int targetID;				// used to track target enemy
  Math::Point2D targetLocation;		// used to indicate target position, in cases target is not an enemy

  void SetFileId(int id)
  {
	  fileId = id;
  }

  int GetFileId()
  {
	  if (root)
	  {
		  return fileId;
	  }
	  else
	  {
		  return 0;
	  }
  }

private:

	int fileId;
	IBTLogicNode * root;
	void GenerateTreeRecursive(IBTLogicNode * parent, int depth,
		std::string &line, std::ifstream &input);
};
