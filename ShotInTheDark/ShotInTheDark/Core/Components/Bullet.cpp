//	File name:		Bullet.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//	
//  Changelog
//	1/21 - added
//
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"

#include "Bullet.h"
#include "..\Systems\Physics\bouncingbullets.hpp"

void Bullet::CreateExplosionEffect(Entity* bullet)
{
	BulletParticleInfo& info = bullet->GET(Bullet)->particle_info;

	if (info.count == 0)
		return;

	Transform * location = new Transform();
	location->Position = bullet->GET(Transform)->Position;

	Drawable * draw = new Drawable();

	// This is to act as a lifetime, so we don't clog up the entity list.
	// It's hacky, so if it causes problems, we need to pull lifetime out into its own component AND system...
	Bullet* lifetime = new Bullet();
	lifetime->lifetime = 5.0f;
	lifetime->damage = 0.0f;
	lifetime->pierces = true;

	glm::vec4 start_color_min,
		start_color_max,
		end_color_min,
		end_color_max;

	start_color_min = info.color;
	start_color_max = info.color;
	end_color_min = info.color * 0.5f;
	end_color_max = info.color * 0.f;

	ExplosionEffect* effect = new ExplosionEffect(
		location, /*info.count*/100, 0.7,
		start_color_min, start_color_max, end_color_min, end_color_max
		);

	ENGINE->Factory->Compose(4, location, effect, lifetime, draw);
}

void Bullet::Hit(Entity *hitEntity, float dt, Entity* source)
{
	// if the enemy has health, decrement/clamp it
	if (hitEntity->HasComponent(EC_Health))
	{
		if (hitEntity->HasComponent(EC_Powerup))
			hitEntity->GET_COMPONENT(Powerup)->OnTakeDamage(hitEntity, damage, source);

		hitEntity->GET_COMPONENT(Health)->ApplyDamage(damage);
	}

	// Then apply all our effects to the enemy
	for (BulletEffect e : effects)
	{
		e(source, hitEntity, dt);
	}
}

void Bullet_Effect_DestroyOnDamage(Entity* b, Entity* hitEntity, float dt)
{
	if (!Team::FriendlyTeams(b, hitEntity) && hitEntity->HasComponent(EC_Health))
		b->GET_COMPONENT(Bullet)->lifetime = 0.0f;
}


void Bullet_Effect_Bouncy(Entity* b, Entity* hitEntity, float dt)
{
	Poly BounceOff = hitEntity->GET_COMPONENT(Transform)->transform * hitEntity->GET_COMPONENT(Drawable)->mpMesh->Poly;

	std::vector<LineSegment> Contact;

	for (unsigned i = 0; i < BounceOff.GetVertNum(); ++i)
	{
		Contact.push_back(LineSegment(BounceOff.GetVert(i), BounceOff.GetVert((i + 1) % BounceOff.GetVertNum())));
	}
	Contact.pop_back();

	LineSegment MoveRay(b->GC(RigidBody)->StartPoint, b->GC(Transform)->Position);

	float FirstIntTime = LS2LS(MoveRay, Contact.back());
	float IntTime = (FirstIntTime >= 0.0f && FirstIntTime <= 1.0f) ? FirstIntTime : 2.0f;
	unsigned index = Contact.size() - 1;

	for (unsigned i = 0; i < Contact.size() - 1; ++i)
	{
		float ScalarCheck = LS2LS(MoveRay, Contact[i]);
		float Scalar = (ScalarCheck >= 0.0f && ScalarCheck <= 1.0f) ? ScalarCheck : -1.0f;
		if (Scalar < IntTime && Scalar != -1.0f)
		{
			IntTime = Scalar;
			index = i;
		}
	}

	if (IntTime < 0.0f || IntTime > 1.0f)
		return;

	Math::Point2D CSFoot = Foot(Contact[index], MoveRay.END());

	// find the reflection
	Math::Vector2D ReflectVec = CSFoot - MoveRay.END();
	ReflectVec.x *= 2;
	ReflectVec.y *= 2;
	Math::Point2D NewPt = b->GC(Transform)->Position + ReflectVec;

	// Use the new int time to set our new direction & velocity
	Math::Point2D Intersection = MoveRay.START() + ((MoveRay.END() - MoveRay.START())*IntTime);
	b->GC(RigidBody)->direction = NewPt - Intersection;
	b->GC(RigidBody)->direction.Normalize();
	b->GC(RigidBody)->vel = b->GC(RigidBody)->direction * b->GC(RigidBody)->speed / b->GC(RigidBody)->density;

	// my sad attempt to avoid collisions in consecutive frames with the same object
	b->GC(Transform)->Position = NewPt;
	//}
	//--(b->GC(Bullet)->WeirdBounceCounter);
}

