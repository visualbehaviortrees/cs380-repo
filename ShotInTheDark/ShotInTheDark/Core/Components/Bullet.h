//	File name:		Bullet.h
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch, Luc Kadletz
//	
//  Changelog
//	1/20 - changed damage to floats, added simple inline helper functions,
//		and a framework for bullet effects
//
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../Component.hpp"
#include <string>
#include "../Systems/Audio/AudioTypes.h"

struct BulletParticleInfo
{
	int count = 0;
	glm::vec4 color;
};


// For particles
struct Bullet : public Component
{
public:

	typedef void(*BulletEffect)(Entity* bullet, Entity* hitEntity, float dt);

	Bullet() :Component(CMP(Bullet)), damage(10.0), lifetime(1.0f), pierces(false), WeirdBounceCounter(0){}
	void Init(){}
	void Free(){}
	~Bullet(){}

	static Component *CreateComponent() { return new Bullet(); }

	void Hit(Entity *hitEntity, float dt, Entity* source);

	static void CreateExplosionEffect(Entity* bullet);

	BulletParticleInfo particle_info;
	float damage;
	float lifetime;
	bool pierces;
	unsigned WeirdBounceCounter;
	SFX::SFXType hitSound;
	std::vector<BulletEffect> effects;
};

// BULLET EFFECTS

/*
Almost like not piercing, but destroys on hurting something, not walls and other
inanimate objects
*/
void Bullet_Effect_DestroyOnDamage(Entity* bullet, Entity* hitEntity, float dt);


void Bullet_Effect_Bouncy(Entity* b, Entity* hitEntity, float dt);
