//  File name:    BulletEffect.h
//  Project name: Shot In The Dark
//  Author(s):    Allan Deutsch
//  
//  All content © 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once
#include "../Component.hpp"

struct BulletEffect : public Component
{
public:
  BulletEffect():Component(CMP(BulletEffect)){}
  void Init(){}
  void Free(){}
  ~BulletEffect(){}

  static Component *CreateComponent() { return new BulletEffect(); }


};
