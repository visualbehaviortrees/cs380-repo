//  File name:    Death.h
//  Project name: Shot In The Dark
//  Author(s):    Allan Deutsch
//  
//  All content © 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once
#include "../Component.hpp"

struct Death : public Component
{
public:
  Death():Component(CMP(Death)){}
  void Init(){}
  void Free(){}
  ~Death(){}

  static Component *CreateComponent() { return new Death(); }


};
