//	File name:		Door.h
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../Component.hpp"

struct Door : public Component
{
public:
  Door() : Component(CMP(Door)) {}
  void Init(){}
  void Free(){}
  ~Door(){}

  static Component *CreateComponent() { return new Door(); }

};
