//	File name:		Drawable.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "Drawable.h"

const Drawable Drawable::BlueScheme(true,false,0.0f,0.0f,0.2f,1.0f,1,true,true,0.0f,0.0f,1.0f,1.0f);
const Drawable Drawable::RedScheme(true,false,0.2f,0.0f,0.0f,1.0f,1,true,true,1.0f,0.0f,0.0f,1.0f);
const Drawable Drawable::YellowScheme(true,false,0.2f,0.2f,0.0f,1.0f,1,true,true,1.0f,1.0f,0.0f,1.0f);
const Drawable Drawable::GrayScheme(true,false,0.4f,0.4f,0.4f,1.0f,15,false,false,0.4f,0.4f,0.4f,1.0f);
