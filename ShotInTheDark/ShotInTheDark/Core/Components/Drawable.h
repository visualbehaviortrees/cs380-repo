//	File name:		Drawable.h
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../Component.hpp"

struct Drawable : public Component
{
public:
  Drawable():Component(CMP(Drawable)),
             visible(true),
             useSprite(false),
             layer(1),
             r(1.0f),
             g(0.f),
             b(0.f),
             a(1.f),
             drawFromCenter(false),
             startPointColor(false),
             beginR(0.0f),
             beginG(0.0f),
             beginB(0.0f),
             beginA(1.0f),
			 mpMesh(nullptr),
             clipped(false)
             {}

  Drawable(bool Visible,
  	       bool UseSprite,
  	       float R,
  	       float G,
  	       float B,
  	       float A,
  	       unsigned Layer = 1,
           bool DFC = false,
           bool SPC = false,
           float BeginR = 0.0f,
           float BeginG = 0.0f,
           float BeginB = 0.0f,
           float BeginA = 0.0f)
          :Component(CMP(Drawable)),
           visible(Visible),
           useSprite(UseSprite),
           layer(Layer),
           r(R),
           g(G),
           b(B),
           a(A),
           drawFromCenter(DFC),
           startPointColor(SPC),
           beginR(BeginR),
           beginG(BeginG),
           beginB(BeginB),
           beginA(BeginA),
		   mpMesh(nullptr),
           clipped(false)
           {}
  Drawable(const Drawable &rhs) :
                                Component(CMP(Drawable)),
                                visible(rhs.visible),
                                useSprite(rhs.useSprite),
                                layer(rhs.layer),
                                r(rhs.r),
                                g(rhs.g),
                                b(rhs.b),
                                a(rhs.a),
                                drawFromCenter(rhs.drawFromCenter),
                                startPointColor(rhs.startPointColor),
                                beginR(rhs.beginR),
                                beginG(rhs.beginG),
                                beginB(rhs.beginB),
                                beginA(rhs.beginA),
								mpMesh(nullptr),
								clipped(false)
                                {}
             
  void Init(){}
  void Free(){}
  ~Drawable(){}

  static Component *CreateComponent() { return new Drawable(); }

  bool visible;
  bool clipped;
  bool useSprite;
  // determines if the shape should be drawn from the first vertex or the center.
  bool drawFromCenter;
  // Set a custom color for the vertex shared by all faces of a convex poly.
  bool startPointColor;
  float r;
  float g;
  float b;
  float a;
  float beginR;
  float beginG;
  float beginB;
  float beginA;
  unsigned layer;
  Mesh* mpMesh = nullptr;
  static const Drawable BlueScheme;
  static const Drawable RedScheme;
  static const Drawable YellowScheme;
  static const Drawable GrayScheme;
};

