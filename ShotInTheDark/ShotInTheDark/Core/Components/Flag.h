//	File name:		Flag.h
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../Component.hpp"
#include <string>
#include "../Systems/Audio/AudioTypes.h"

struct Flag : public Component
{
public:
  Flag():Component(CMP(Flag)){}
  void Init(){}
  void Free(){}
  ~Flag(){}

  static Component *CreateComponent() { return new Flag(); }


};
