//	File name:		GUIItem.h
//	Project name:	Shot In The Dark
//	Author(s):		Connor Drose
//
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../Component.hpp"

struct GUIItem : public Component
{
public:
  GUIItem():Component(CMP(GUIItem)){}
  void Init(){}
  void Free(){}
  ~GUIItem(){}

  static Component *CreateComponent() { return new GUIItem(); }
  Point2D screenPos;
};
