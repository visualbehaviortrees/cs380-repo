//  File name:    HUDElement.h
//  Project name: Shot In The Dark
//  Author(s):    Allan Deutsch
//  
//  All content © 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once
#include "../Component.hpp"

struct HUDElement : public Component
{
public:
  HUDElement():Component(CMP(HUDElement)){}
  void Init(){}
  void Free(){}
  ~HUDElement(){}

  static Component *CreateComponent() { return new HUDElement(); }


};
