//	File name:		Health.h
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch, Luc Kadletz
//	
//  Changelog
//	1/20 - changed health to floats, added simple inline helper functions
//
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../Component.hpp"
#include <string>
struct Health : public Component
{
public:
  Health():Component(CMP(Health)), health(10.0f), maxHealth(10.0f), regen(0.0f), safeTime(0.0f), timeSinceLastHit(0.0f){}
  Health(float maxhp) :Component(CMP(Health)), health(maxhp), maxHealth(maxhp), regen(0.0f), safeTime(0.0f), timeSinceLastHit(0.0f){}
  void Init(){}
  void Free(){}
  ~Health(){}

  static Component *CreateComponent() { return new Health(); }

  float health;
  float maxHealth;
  float regen;
  float safeTime; // How long after taking damage until we can take more

  float timeSinceLastHit;

  inline void Update(float dt);
  // Returns true if the damage reduces our health to 0
  inline bool ApplyDamage(float damage);
  inline float GetHealth(void) const;
  inline int GetHealthPercent(void) const;
  inline float GetHealthValue2() const;


  enum HealthState{ HS_FULL, HS_HIGH, HS_MID, HS_LOW };
  inline HealthState GetHealthState() const;
};


void Health::Update(float dt)
{
	timeSinceLastHit += dt;

  health = clamp(health + regen * dt, 0.0f, maxHealth);
}

bool Health::ApplyDamage(float damage)
{
	// Don't take damage if we're still safe
	if (timeSinceLastHit < safeTime)
		return false;

	timeSinceLastHit = 0.0f;
  health = clamp(health - damage, 0.0f, maxHealth);
  
  return health == 0.0f;
}

float Health::GetHealth(void) const
{
  return (health / maxHealth);
}

float Health::GetHealthValue2() const
{
	return health;
}

int Health::GetHealthPercent(void) const
{
  return (int)(GetHealth() * 100);
}

Health::HealthState Health::GetHealthState() const
{
  int hpPercent = GetHealthPercent();
  assert(hpPercent <= 100);
  if (hpPercent == 100)
  {
    return HS_FULL;
  }
  else if (hpPercent > 50) //TODO should be hpPercent > 70
  {
    return HS_HIGH;
    /*
  }
  else if (hpPercent >= 40)
  {
    return HS_MID;
  */
  }
  else
  {
    return HS_LOW;
  }
}
