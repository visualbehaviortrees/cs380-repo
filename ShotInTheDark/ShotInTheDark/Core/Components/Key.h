//  File name:    Key.h
//  Project name: Shot In The Dark
//  Author(s):    Allan Deutsch
//  
//  All content © 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once
#include "../Component.hpp"

struct Key : public Component
{
public:
  Key():Component(CMP(Key)){}
  void Init(){}
  void Free(){}
  ~Key(){}

  static Component *CreateComponent() { return new Key(); }


};
