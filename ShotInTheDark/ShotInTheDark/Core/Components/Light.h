//	File name:		Light.h
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once
#define INTENSITY 40
#define LIGHTOFF -1.0f
#include "../Component.hpp"
#include <string>
#include "..\Systems/Graphics\GraphicsMath.hpp"
#include "../Systems/Audio/AudioTypes.h"

typedef glm::vec4 Color;
#define InvisibleColor Color(0.0f, 0.0f, 0.0f, 0.0f)
#define BlackColor Color(0.0f, 0.0f, 0.0f, 1.0f)
#define RedColor Color(1.0f, 0.0f, 0.0f, 1.0f)
#define YellowColor Color(1.0f, 1.0f, 0.0f, 1.0f)
#define GreenColor Color(0.0f, 1.0f, 0.0f, 1.0f)
#define CyanColor Color(0.0f, 1.0f, 1.0f, 1.0f)
#define BlueColor Color(0.0f, 0.0f, 1.0f, 1.0f)
#define MagentaColor Color(1.0f, 0.0f, 1.0f, 1.0f)


struct Light : public Component
{
public:
  Light(float _intensity = INTENSITY, const Color& newColor = Color(1.0f, 1.0f, 1.0f, 1.0f)) : Component(CMP(Light)), intensity(_intensity), currentLightColor(newColor), newLightColor(newColor){}
  void Init(){}
  void Free(){}
  ~Light(){}

  glm::vec3 GetCurrentColorRGB() { return glm::vec3(currentLightColor.r, currentLightColor.g, currentLightColor.b); }

  Color GetCurrentColor() { return currentLightColor; }

  void SetCurrentColor(const glm::vec4& newColor)
  {
    prevLightColor = newLightColor;
    newLightColor = currentLightColor = newColor;
  }

  void SetCurrentColor(const glm::vec3& newColor)
  {
    SetCurrentColor(glm::vec4(newColor, 1.0f));
  }

  void SetNewColor(const glm::vec4& newColor)
  {
    prevLightColor = newLightColor;
    newLightColor = newColor;
  }

  void InterpolateToNewColor(float dt)
  {
    if (currentLightColor != newLightColor)
      currentLightColor += (newLightColor - currentLightColor) * dt;
  }

  static Component *CreateComponent() { return new Light(); }

  float intensity;
  glm::vec3 position;
  Color prevLightColor;
  Color currentLightColor;
  Color newLightColor;
};
