//  File name:    MenuButtonComponent.h
//  Project name: Shot In The Dark
//  Author(s):    Allan Deutsch
//  
//  All content © 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once
#include "../Component.hpp"

struct MenuButtonComponent : public Component
{
public:
  MenuButtonComponent():Component(CMP(MenuButtonComponent)){}
  void Init(){}
  void Free(){}
  ~MenuButtonComponent(){}

  static Component *CreateComponent() { return new MenuButtonComponent(); }


};
