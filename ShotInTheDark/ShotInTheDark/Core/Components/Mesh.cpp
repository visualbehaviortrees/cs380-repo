//	File name:		Mesh.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "Mesh.hpp"
#include "../Systems/Graphics/LoadShader.hpp"
#include "Components.h"

GLuint Shader = 0;
GLuint Matrix = 0;

std::list <std::pair <std::string, Mesh*>> MeshList;
unsigned uniform;

void Shader_Init()
{
	Shader = LoadShaders("Core/Systems/Graphics/Shaders/VertexShader.glsl", "Core/Systems/Graphics/Shaders/FragmentShader.glsl");
}

void Mesh::Init()
{
	if (Shader == 0)
	{
		Shader_Init();
	}
}



void Mesh::Specify_Attributes()
{
	// Specify the layout of the vertex data
	GLuint posAttrib = glGetAttribLocation(Shader, "position");
	glEnableVertexAttribArray(posAttrib);
	glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 7 * sizeof(GLfloat), 0);
	//                                                                                                         
	GLuint colAttrib = glGetAttribLocation(Shader, "color");
	glEnableVertexAttribArray(colAttrib);
	glVertexAttribPointer(colAttrib, 4, GL_FLOAT, GL_FALSE, 7 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));

	Matrix = glGetUniformLocation(Shader, "matrix");
}

void Mesh::CreateMesh(Drawable* draw)
{
	this->drawable = draw;
	std::vector <float> vertices;
	unsigned ops = 0;
	ops += draw->drawFromCenter;
	ops += 2 * draw->startPointColor;
	Math::Point2D center = Poly.CENTER();
	switch (ops)
	{
	case 0:
		for (unsigned i = 0; i < Poly.GetVertNum() - 2; ++i)
		{
			vertices.push_back(Poly.GetVert(0).x);
			vertices.push_back(Poly.GetVert(0).y);
			vertices.push_back(0.0f);

			vertices.push_back(draw->r);
			vertices.push_back(draw->g);
			vertices.push_back(draw->b);
			vertices.push_back(draw->a);

			vertices.push_back(Poly.GetVert(i + 1).x);
			vertices.push_back(Poly.GetVert(i + 1).y);
			vertices.push_back(0.0f);

			vertices.push_back(draw->r);
			vertices.push_back(draw->g);
			vertices.push_back(draw->b);
			vertices.push_back(draw->a);

			vertices.push_back(Poly.GetVert(i + 2).x);
			vertices.push_back(Poly.GetVert(i + 2).y);
			vertices.push_back(0.0f);

			vertices.push_back(draw->r);
			vertices.push_back(draw->g);
			vertices.push_back(draw->b);
			vertices.push_back(draw->a);
		}
		break;
	case 1:
		for (unsigned i = 0; i < Poly.GetVertNum() - 1; ++i)
		{
			vertices.push_back(0.0f);
			vertices.push_back(0.0f);
			vertices.push_back(0.0f);

			vertices.push_back(draw->r);
			vertices.push_back(draw->g);
			vertices.push_back(draw->b);
			vertices.push_back(draw->a);

			vertices.push_back(Poly.GetVert(i).y);
			vertices.push_back(0.0f);

			vertices.push_back(draw->r);
			vertices.push_back(draw->g);
			vertices.push_back(draw->b);
			vertices.push_back(draw->a);

			vertices.push_back(Poly.GetVert(i + 1).x);
			vertices.push_back(Poly.GetVert(i + 1).y);
			vertices.push_back(0.0f);

			vertices.push_back(draw->r);
			vertices.push_back(draw->g);
			vertices.push_back(draw->b);
			vertices.push_back(draw->a);
		}
		break;
	case 2:
		for (unsigned i = 0; i < Poly.GetVertNum() - 2; ++i)
		{
			vertices.push_back(Poly.GetVert(0).x);
			vertices.push_back(Poly.GetVert(0).y);
			vertices.push_back(0.0f);
			vertices.push_back(draw->beginR);
			vertices.push_back(draw->beginG);
			vertices.push_back(draw->beginB);
			vertices.push_back(draw->beginA);
			vertices.push_back(Poly.GetVert(i + 1).x);
			vertices.push_back(Poly.GetVert(i + 1).y);
			vertices.push_back(0.0f);
			vertices.push_back(draw->r);
			vertices.push_back(draw->g);
			vertices.push_back(draw->b);
			vertices.push_back(draw->a);
			vertices.push_back(Poly.GetVert(i + 2).x);
			vertices.push_back(Poly.GetVert(i + 2).y);
			vertices.push_back(0.0f);
			vertices.push_back(draw->r);
			vertices.push_back(draw->g);
			vertices.push_back(draw->b);
			vertices.push_back(draw->a);
		}
		break;
	case 3:
		for (unsigned i = 0; i < Poly.GetVertNum() - 1; ++i)
		{
			vertices.push_back(0.0f);
			vertices.push_back(0.0f);
			vertices.push_back(0.0f);
			vertices.push_back(draw->beginR);
			vertices.push_back(draw->beginG);
			vertices.push_back(draw->beginB);
			vertices.push_back(draw->beginA);
			vertices.push_back(Poly.GetVert(i).x);
			vertices.push_back(Poly.GetVert(i).y);
			vertices.push_back(0.0f);
			vertices.push_back(draw->r);
			vertices.push_back(draw->g);
			vertices.push_back(draw->b);
			vertices.push_back(draw->a);
			vertices.push_back(Poly.GetVert(i + 1).x);
			vertices.push_back(Poly.GetVert(i + 1).y);
			vertices.push_back(0.0f);
			vertices.push_back(draw->r);
			vertices.push_back(draw->g);
			vertices.push_back(draw->b);
			vertices.push_back(draw->a);
		}
		break;
	}

	if (vao)
	{
		delete vao;
	}
	if (vbo)
	{
		delete vbo;
	}

	vao = new VAO();
	vbo = new VBO(vertices.size() * sizeof(float), vertices.data());
	Specify_Attributes();
	vao->unBind();
}

void Mesh::Draw(Drawable *draw, Math::Matrix4& matrix)
{
	if (draw->visible && !draw->clipped)
	{
		glUseProgram(Shader);
		vao->bind();
		glUniformMatrix4fv(Matrix, 1, GL_FALSE, &matrix.matrix4[0][0]);
		glDrawArrays(GL_TRIANGLES, 0, vbo->getVerticeCount());
		vao->unBind();
		//glUseProgram(0);
	}
}

void Mesh::Register(std::string name /*= "Mesh"*/)
{
	meshName = name;
	MeshList.push_back(std::make_pair(name, this));
}

Mesh* Mesh::Get_Mesh(std::string name)
{
	for (auto& i : MeshList)
	{
		if (i.first == name)
			return i.second;
	}

	std::cout << "Mesh not found: " << name << std::endl;
	//throw ("Mesh Not Found ");
	return nullptr;
}

void Mesh::Free()
{
	//if(vao)
	//{
	//  delete vao;
	//}
	//if(vbo)
	//{
	//  delete vbo;
	//}

	//MeshList.remove(std::make_pair(meshName, this));
}

void Mesh::Update_Vertices(Entity *entity)
{
	if (!(entity->HasComponent(EC_Drawable)))
		return;
	Drawable * draw = entity->GET_COMPONENT(Drawable);
	CreateMesh(draw);
}

#pragma region Init
//
//void Mesh::Init_Mesh(Meshes number)
//{
//  Drawable* draw;
//  Mesh* mesh;
//  switch (number)
//  {
//    case Mesh_TurretBullet:
//      draw = new Drawable();
//      draw->layer = 100;
//      mesh = new Mesh(5);
//      mesh->Poly.SetVert(0) = Math::Point2D(-1.0f,0.f);
//      mesh->Poly.SetVert(1) = Math::Point2D(0.f,-0.3f);
//      mesh->Poly.SetVert(2) = Math::Point2D(1.0f,0.f);
//      mesh->Poly.SetVert(3) = Math::Point2D(0.0f,0.3f);
//      mesh->Poly.SetVert(4) = mesh->Poly.GetVert(0);
//      mesh->CreateMesh(draw);
//      mesh->Register("turretbullet");
//      break;
//
//
//    case Mesh_RedBullet:
//      draw = new Drawable();
//      draw->layer = 100;
//      mesh = new Mesh(5);
//      mesh->Poly.SetVert(0) = Math::Point2D(0.0f,1.0f);
//      mesh->Poly.SetVert(1) = Math::Point2D(-0.3f,0.f);
//      mesh->Poly.SetVert(2) = Math::Point2D(0.0f,-1.0f);
//      mesh->Poly.SetVert(3) = Math::Point2D(0.3f,0.0f);
//      mesh->Poly.SetVert(4) = Math::Point2D(0.0f,1.0f);
//      mesh->CreateMesh(draw);
//      mesh->Register("RedBullet");
//      break;
//
//
//    case Mesh_BlueBullet:
//      draw = new Drawable();
//      *draw = Drawable::BlueScheme;
//      draw->layer = 100;
//      mesh = new Mesh(5);
//      mesh->Poly.SetVert(0) = Math::Point2D(0.0f,1.0f);
//      mesh->Poly.SetVert(1) = Math::Point2D(-0.3f,0.f);
//      mesh->Poly.SetVert(2) = Math::Point2D(0.0f,-1.0f);
//      mesh->Poly.SetVert(3) = Math::Point2D(0.3f,0.0f);
//      mesh->Poly.SetVert(4) = Math::Point2D(0.0f,1.0f);
//      mesh->CreateMesh(draw);
//      mesh->Register("BlueBullet");
//      break;
//
//
//    case Mesh_Box:
//      draw = new Drawable();
//      *draw = Drawable::BlueScheme;
//      draw->layer = 3;
//      mesh = new Mesh(5);
//      mesh->Poly.SetVert(0) = Math::Point2D(1.f, 1.f);
//      mesh->Poly.SetVert(1) = Math::Point2D(1.f, -1.f);
//      mesh->Poly.SetVert(2) = Math::Point2D(-1.f, -1.f);
//      mesh->Poly.SetVert(3) = Math::Point2D(-1.f, 1.f);
//      mesh->Poly.SetVert(4) = Math::Point2D(1.f, 1.f);
//      mesh->CreateMesh(draw);
//      mesh->Register("box");
//      break;
//
//
//    case Mesh_Turret:
//      draw = new Drawable();
//      *draw = Drawable::BlueScheme;
//      draw->layer = 6;
//      mesh = new Mesh(8);
//      mesh->Poly.SetVert(0) = Math::Point2D(.0f,1.f);
//      mesh->Poly.SetVert(1) = Math::Point2D(.2f,.4f);
//      mesh->Poly.SetVert(2) = Math::Point2D(.5f,.3f);
//      mesh->Poly.SetVert(3) = Math::Point2D(.3f,-.4f);
//      mesh->Poly.SetVert(4) = Math::Point2D(-.3f,-.4f);
//      mesh->Poly.SetVert(5) = Math::Point2D(-.5f,.3f);
//      mesh->Poly.SetVert(6) = Math::Point2D(-.2f,0.4f);
//      mesh->Poly.SetVert(7) = Math::Point2D(.0f,1.f);
//      mesh->CreateMesh(draw);
//      mesh->Register("turret");
//      break;
//
//
//
//    case Mesh_RedTurr:
//      draw = new Drawable();
//      *draw = Drawable::RedScheme;
//      draw->layer = 6;
//      mesh = new Mesh(8);
//      mesh->Poly.SetVert(0) = Math::Point2D(.0f,1.f);
//      mesh->Poly.SetVert(1) = Math::Point2D(.2f,.4f);
//      mesh->Poly.SetVert(2) = Math::Point2D(.5f,.3f);
//      mesh->Poly.SetVert(3) = Math::Point2D(.3f,-.4f);
//      mesh->Poly.SetVert(4) = Math::Point2D(-.3f,-.4f);
//      mesh->Poly.SetVert(5) = Math::Point2D(-.5f,.3f);
//      mesh->Poly.SetVert(6) = Math::Point2D(-.2f,0.4f);
//      mesh->Poly.SetVert(7) = Math::Point2D(.0f,1.f);
//      mesh->CreateMesh(draw);
//      mesh->Register("redturret");
//      break;
//
//
//
//    case Mesh_NeutralTurr:
//      draw = new Drawable();
//      *draw = Drawable::YellowScheme;
//      draw->layer = 6;
//      mesh = new Mesh(8);
//      mesh->Poly.SetVert(0) = Math::Point2D(.0f,1.f);
//      mesh->Poly.SetVert(1) = Math::Point2D(.2f,.4f);
//      mesh->Poly.SetVert(2) = Math::Point2D(.5f,.3f);
//      mesh->Poly.SetVert(3) = Math::Point2D(.3f,-.4f);
//      mesh->Poly.SetVert(4) = Math::Point2D(-.3f,-.4f);
//      mesh->Poly.SetVert(5) = Math::Point2D(-.5f,.3f);
//      mesh->Poly.SetVert(6) = Math::Point2D(-.2f,0.4f);
//      mesh->Poly.SetVert(7) = Math::Point2D(.0f,1.f);
//      mesh->CreateMesh(draw);
//      mesh->Register("neutralturr");
//      break;
//
//
//
//
//    case Mesh_PowerUp:
//      draw = new Drawable();
//      *draw = Drawable::YellowScheme;
//      mesh = new Mesh(5);
//      mesh->Poly.SetVert(0) = Math::Point2D(1.f, 1.f);
//      mesh->Poly.SetVert(1) = Math::Point2D(1.f, -1.f);
//      mesh->Poly.SetVert(2) = Math::Point2D(-1.f, -1.f);
//      mesh->Poly.SetVert(3) = Math::Point2D(-1.f, 1.f);
//      mesh->Poly.SetVert(4) = Math::Point2D(1.f, 1.f);
//      mesh->CreateMesh(draw);
//      mesh->Register("PowerUp");
//      break;
//
//
//    case Mesh_BluePlayer:
//      draw = new Drawable();
//      *draw = Drawable::BlueScheme;
//      draw->layer = 5;
//      mesh = new Mesh(8);
//      mesh->Poly.SetVert(0) = Math::Point2D(0.f,1.f);
//      mesh->Poly.SetVert(1) = Math::Point2D(-2.f/3.f,-1+(2.5f/3.25f));
//      mesh->Poly.SetVert(2) = Math::Point2D(-5.f/9.f,-1+(1.0f/3.25f));
//      mesh->Poly.SetVert(3) = Math::Point2D(-2.f/9.f,-1.f);
//      mesh->Poly.SetVert(4) = Math::Point2D(2.f/9.f,-1.f);
//      mesh->Poly.SetVert(5) = Math::Point2D(5.f/9.f,-1+(1.0f/3.25f));
//      mesh->Poly.SetVert(6) = Math::Point2D(2.f/3.f,-1+(2.5f/3.25f));
//      mesh->Poly.SetVert(7) = Math::Point2D(0.f,1.f);
//      mesh->CreateMesh(draw);
//      mesh->Register("BluePlayer");
//      break;
//
//
//    case Mesh_RedPlayer:
//      draw = new Drawable();
//      *draw = Drawable::RedScheme;
//      draw->layer = 5;
//      mesh = new Mesh(8);
//      mesh->Poly.SetVert(0) = Math::Point2D(0.f,1.f);
//      mesh->Poly.SetVert(1) = Math::Point2D(-2.f/3.f,-1+(2.5f/3.25f));
//      mesh->Poly.SetVert(2) = Math::Point2D(-5.f/9.f,-1+(1.0f/3.25f));
//      mesh->Poly.SetVert(3) = Math::Point2D(-2.f/9.f,-1.f);
//      mesh->Poly.SetVert(4) = Math::Point2D(2.f/9.f,-1.f);
//      mesh->Poly.SetVert(5) = Math::Point2D(5.f/9.f,-1+(1.0f/3.25f));
//      mesh->Poly.SetVert(6) = Math::Point2D(2.f/3.f,-1+(2.5f/3.25f));
//      mesh->Poly.SetVert(7) = Math::Point2D(0.f,1.f);
//      mesh->CreateMesh(draw);
//      mesh->Register("RedPlayer");
//      break;
//
//
//    case Mesh_BlueBase:
//      draw = new Drawable();
//      *draw = Drawable::BlueScheme;
//      draw->layer = 1;
//      mesh = new Mesh(9);
//      mesh->Poly.SetVert(0) = Math::Point2D(1.f,-0.5f);
//      mesh->Poly.SetVert(1) = Math::Point2D(1.f,0.5f);
//      mesh->Poly.SetVert(2) = Math::Point2D(0.5f,1.f);
//      mesh->Poly.SetVert(3) = Math::Point2D(-0.5f,1.f);
//      mesh->Poly.SetVert(4) = Math::Point2D(-1.f,0.5f);
//      mesh->Poly.SetVert(5) = Math::Point2D(-1.f,-0.5f);
//      mesh->Poly.SetVert(6) = Math::Point2D(-0.5f,-1.f);
//      mesh->Poly.SetVert(7) = Math::Point2D(0.5f,-1.f);
//      mesh->Poly.SetVert(8) = mesh->Poly.GetVert(0);
//      mesh->CreateMesh(draw);
//      mesh->Register("BlueBase");
//      break;
//
//
//    case Mesh_RedBase:
//      draw = new Drawable();
//      *draw = Drawable::RedScheme;
//      draw->layer = 1;
//      mesh = new Mesh(9);
//      mesh->Poly.SetVert(0) = Math::Point2D(1.f,-0.5f);
//      mesh->Poly.SetVert(1) = Math::Point2D(1.f,0.5f);
//      mesh->Poly.SetVert(2) = Math::Point2D(0.5f,1.f);
//      mesh->Poly.SetVert(3) = Math::Point2D(-0.5f,1.f);
//      mesh->Poly.SetVert(4) = Math::Point2D(-1.f,0.5f);
//      mesh->Poly.SetVert(5) = Math::Point2D(-1.f,-0.5f);
//      mesh->Poly.SetVert(6) = Math::Point2D(-0.5f,-1.f);
//      mesh->Poly.SetVert(7) = Math::Point2D(0.5f,-1.f);
//      mesh->Poly.SetVert(8) = mesh->Poly.GetVert(0);
//      mesh->CreateMesh(draw);
//      mesh->Register("RedBase");
//      break;
//
//
//    case Mesh_Flag:
//      draw = new Drawable(true,false,0.f,0.f,0.f,1.0f,10,true,true,0.8f,0.8f,0.8f,1.0f);
//      mesh = new Mesh(4);
//      mesh->Poly.SetVert(0) = Math::Point2D(-0.375f,-0.25f);
//      mesh->Poly.SetVert(1) = Math::Point2D(0.375f, 0.0f);
//      mesh->Poly.SetVert(2) = Math::Point2D(-0.375f,0.25f);
//      mesh->Poly.SetVert(3) = Math::Point2D(-0.375f,-0.25f);
//      mesh->CreateMesh(draw);
//      mesh->Register("Flag");
//      break;
//
//
//    case Mesh_BlueFlag:
//      draw = new Drawable();
//      *draw = Drawable::BlueScheme;
//      draw->layer = 10;
//      mesh = new Mesh(4);
//      mesh->Poly.SetVert(0) = Math::Point2D(-0.375f,-0.25f);
//      mesh->Poly.SetVert(1) = Math::Point2D(0.375f, 0.0f);
//      mesh->Poly.SetVert(2) = Math::Point2D(-0.375f,0.25f);
//      mesh->Poly.SetVert(3) = Math::Point2D(-0.375f,-0.25f);
//      mesh->CreateMesh(draw);
//      mesh->Register("BlueFlag");
//      break;
//
//
//    case Mesh_RedFlag:
//      draw = new Drawable();
//      *draw = Drawable::RedScheme;
//      draw->layer = 10;
//      mesh = new Mesh(4);
//      mesh->Poly.SetVert(0) = Math::Point2D(-0.375f,-0.25f);
//      mesh->Poly.SetVert(1) = Math::Point2D(0.375f, 0.0f);
//      mesh->Poly.SetVert(2) = Math::Point2D(-0.375f,0.25f);
//      mesh->Poly.SetVert(3) = Math::Point2D(-0.375f,-0.25f);
//      mesh->CreateMesh(draw);
//      mesh->Register("RedFlag");
//      break;
//
//
//    case Mesh_PauseBox:
//      draw = new Drawable();
//      *draw = Drawable::GrayScheme;
//      mesh = new Mesh(5);
//      mesh->Poly.SetVert(0) = Math::Point2D(1.f, 1.f);
//      mesh->Poly.SetVert(1) = Math::Point2D(1.f, -1.f);
//      mesh->Poly.SetVert(2) = Math::Point2D(-1.f, -1.f);
//      mesh->Poly.SetVert(3) = Math::Point2D(-1.f, 1.f);
//      mesh->Poly.SetVert(4) = Math::Point2D(1.f, 1.f);
//      mesh->CreateMesh(draw);
//      mesh->Register("PauseBox");
//      break;
//
//   }
//}
#pragma endregion

void Mesh::Init_Mesh(unsigned number)
{
	Drawable* draw;
	Mesh* mesh;
	switch (number)
	{
	case Mesh_TurretBullet:
		draw = new Drawable();
		draw->layer = 100;
		mesh = new Mesh(5);
		mesh->Poly.SetVert(0) = Math::Point2D(-1.f, 0.f);
		mesh->Poly.SetVert(1) = Math::Point2D(0.f, -1.f);
		mesh->Poly.SetVert(2) = Math::Point2D(1.f, 0.f);
		mesh->Poly.SetVert(3) = Math::Point2D(0.0f, 1.f);
		mesh->Poly.SetVert(4) = mesh->Poly.GetVert(0);
		mesh->CreateMesh(draw);
		mesh->Register("turretbullet");
		break;


	case Mesh_RedBullet:
		draw = new Drawable();
		*draw = Drawable::RedScheme;
		draw->layer = 100;
		mesh = new Mesh(5);
		mesh->Poly.SetVert(0) = Math::Point2D(-1.f, 0.f);
		mesh->Poly.SetVert(1) = Math::Point2D(0.f, -1.f);
		mesh->Poly.SetVert(2) = Math::Point2D(1.f, 0.f);
		mesh->Poly.SetVert(3) = Math::Point2D(0.0f, 1.f);
		mesh->Poly.SetVert(4) = mesh->Poly.GetVert(0);
		mesh->CreateMesh(draw);
		mesh->Register("RedBullet");
		break;


	case Mesh_BlueBullet:
		draw = new Drawable();
		*draw = Drawable::BlueScheme;
		draw->layer = 100;
		mesh = new Mesh(5);
		mesh->Poly.SetVert(0) = Math::Point2D(-1.f, 0.f);
		mesh->Poly.SetVert(1) = Math::Point2D(0.f, -1.f);
		mesh->Poly.SetVert(2) = Math::Point2D(1.f, 0.f);
		mesh->Poly.SetVert(3) = Math::Point2D(0.0f, 1.f);
		mesh->Poly.SetVert(4) = mesh->Poly.GetVert(0);
		mesh->CreateMesh(draw);
		mesh->Register("BlueBullet");
		break;


	case Mesh_Box:
		draw = new Drawable();
		*draw = Drawable::YellowScheme;
		draw->layer = 3;
		mesh = new Mesh(5);
		mesh->Poly.SetVert(0) = Math::Point2D(1.01f, 1.01f);
		mesh->Poly.SetVert(1) = Math::Point2D(1.01f, -1.01f);
		mesh->Poly.SetVert(2) = Math::Point2D(-1.01f, -1.01f);
		mesh->Poly.SetVert(3) = Math::Point2D(-1.01f, 1.01f);
		mesh->Poly.SetVert(4) = Math::Point2D(1.01f, 1.01f);
		mesh->CreateMesh(draw);
		mesh->Register("box");
		break;


	case Mesh_Turret:
		draw = new Drawable();
		*draw = Drawable::BlueScheme;
		draw->layer = 6;
		mesh = new Mesh(8);
		mesh->Poly.SetVert(0) = Math::Point2D(.0f, 1.f);
		mesh->Poly.SetVert(1) = Math::Point2D(.2f, .4f);
		mesh->Poly.SetVert(2) = Math::Point2D(.5f, .3f);
		mesh->Poly.SetVert(3) = Math::Point2D(.3f, -.4f);
		mesh->Poly.SetVert(4) = Math::Point2D(-.3f, -.4f);
		mesh->Poly.SetVert(5) = Math::Point2D(-.5f, .3f);
		mesh->Poly.SetVert(6) = Math::Point2D(-.2f, 0.4f);
		mesh->Poly.SetVert(7) = Math::Point2D(.0f, 1.f);
		mesh->CreateMesh(draw);
		mesh->Register("turret");
		break;


	case Mesh_PowerUp:
		draw = new Drawable();
		*draw = Drawable::YellowScheme;
		mesh = new Mesh(5);
		mesh->Poly.SetVert(0) = Math::Point2D(1.f, 1.f);
		mesh->Poly.SetVert(1) = Math::Point2D(1.f, -1.f);
		mesh->Poly.SetVert(2) = Math::Point2D(-1.f, -1.f);
		mesh->Poly.SetVert(3) = Math::Point2D(-1.f, 1.f);
		mesh->Poly.SetVert(4) = Math::Point2D(1.f, 1.f);
		mesh->CreateMesh(draw);
		mesh->Register("PowerUp");
		break;


	case Mesh_BluePlayer:
		draw = new Drawable();
		*draw = Drawable::BlueScheme;
		draw->layer = 5;
		mesh = new Mesh(8);
		mesh->Poly.SetVert(0) = Math::Point2D(0.f, 1.f);
		mesh->Poly.SetVert(1) = Math::Point2D(-2.f / 3.f, -1 + (2.5f / 3.25f));
		mesh->Poly.SetVert(2) = Math::Point2D(-5.f / 9.f, -1 + (1.0f / 3.25f));
		mesh->Poly.SetVert(3) = Math::Point2D(-2.f / 9.f, -1.f);
		mesh->Poly.SetVert(4) = Math::Point2D(2.f / 9.f, -1.f);
		mesh->Poly.SetVert(5) = Math::Point2D(5.f / 9.f, -1 + (1.0f / 3.25f));
		mesh->Poly.SetVert(6) = Math::Point2D(2.f / 3.f, -1 + (2.5f / 3.25f));
		mesh->Poly.SetVert(7) = Math::Point2D(0.f, 1.f);
		mesh->CreateMesh(draw);
		mesh->Register("BluePlayer");
		break;


	case Mesh_RedPlayer:
		draw = new Drawable();
		*draw = Drawable::RedScheme;
		draw->layer = 5;
		mesh = new Mesh(8);
		mesh->Poly.SetVert(0) = Math::Point2D(0.f, 1.f);
		mesh->Poly.SetVert(1) = Math::Point2D(-2.f / 3.f, -1 + (2.5f / 3.25f));
		mesh->Poly.SetVert(2) = Math::Point2D(-5.f / 9.f, -1 + (1.0f / 3.25f));
		mesh->Poly.SetVert(3) = Math::Point2D(-2.f / 9.f, -1.f);
		mesh->Poly.SetVert(4) = Math::Point2D(2.f / 9.f, -1.f);
		mesh->Poly.SetVert(5) = Math::Point2D(5.f / 9.f, -1 + (1.0f / 3.25f));
		mesh->Poly.SetVert(6) = Math::Point2D(2.f / 3.f, -1 + (2.5f / 3.25f));
		mesh->Poly.SetVert(7) = Math::Point2D(0.f, 1.f);
		mesh->CreateMesh(draw);
		mesh->Register("RedPlayer");
		break;

	case Mesh_Strawberry:
		draw = new Drawable();
		*draw = Drawable::RedScheme;
		draw->layer = 5;
		mesh = new Mesh(8);
		mesh->Poly.SetVert(0) = Math::Point2D(0.f, -1.f);
		mesh->Poly.SetVert(1) = Math::Point2D(-2.f / 3.f, 1 - (2.5f / 3.25f));
		mesh->Poly.SetVert(2) = Math::Point2D(-5.f / 9.f, 1 - (1.0f / 3.25f));
		mesh->Poly.SetVert(3) = Math::Point2D(-2.f / 9.f, 1.f);
		mesh->Poly.SetVert(4) = Math::Point2D(2.f / 9.f, 1.f);
		mesh->Poly.SetVert(5) = Math::Point2D(5.f / 9.f, 1 - (1.0f / 3.25f));
		mesh->Poly.SetVert(6) = Math::Point2D(2.f / 3.f, 1 - (2.5f / 3.25f));
		mesh->Poly.SetVert(7) = Math::Point2D(0.f, -1.f);
		mesh->CreateMesh(draw);
		mesh->Register("Strawberry");
		break;


	case Mesh_BlueBase:
		draw = new Drawable();
		*draw = Drawable::BlueScheme;
		draw->layer = 1;
		mesh = new Mesh(9);
		mesh->Poly.SetVert(0) = Math::Point2D(1.f, -0.5f);
		mesh->Poly.SetVert(1) = Math::Point2D(1.f, 0.5f);
		mesh->Poly.SetVert(2) = Math::Point2D(0.5f, 1.f);
		mesh->Poly.SetVert(3) = Math::Point2D(-0.5f, 1.f);
		mesh->Poly.SetVert(4) = Math::Point2D(-1.f, 0.5f);
		mesh->Poly.SetVert(5) = Math::Point2D(-1.f, -0.5f);
		mesh->Poly.SetVert(6) = Math::Point2D(-0.5f, -1.f);
		mesh->Poly.SetVert(7) = Math::Point2D(0.5f, -1.f);
		mesh->Poly.SetVert(8) = mesh->Poly.GetVert(0);
		mesh->CreateMesh(draw);
		mesh->Register("BlueBase");
		break;


	case Mesh_RedBase:
		draw = new Drawable();
		*draw = Drawable::RedScheme;
		draw->layer = 1;
		mesh = new Mesh(9);
		mesh->Poly.SetVert(0) = Math::Point2D(1.f, -0.5f);
		mesh->Poly.SetVert(1) = Math::Point2D(1.f, 0.5f);
		mesh->Poly.SetVert(2) = Math::Point2D(0.5f, 1.f);
		mesh->Poly.SetVert(3) = Math::Point2D(-0.5f, 1.f);
		mesh->Poly.SetVert(4) = Math::Point2D(-1.f, 0.5f);
		mesh->Poly.SetVert(5) = Math::Point2D(-1.f, -0.5f);
		mesh->Poly.SetVert(6) = Math::Point2D(-0.5f, -1.f);
		mesh->Poly.SetVert(7) = Math::Point2D(0.5f, -1.f);
		mesh->Poly.SetVert(8) = mesh->Poly.GetVert(0);
		mesh->CreateMesh(draw);
		mesh->Register("RedBase");
		break;


	case Mesh_Flag:
		draw = new Drawable(true, false, 0.f, 0.f, 0.f, 1.0f, 10, true, true, 0.8f, 0.8f, 0.8f, 1.0f);
		mesh = new Mesh(4);
		mesh->Poly.SetVert(0) = Math::Point2D(-0.375f, -0.25f);
		mesh->Poly.SetVert(1) = Math::Point2D(0.375f, 0.0f);
		mesh->Poly.SetVert(2) = Math::Point2D(-0.375f, 0.25f);
		mesh->Poly.SetVert(3) = Math::Point2D(-0.375f, -0.25f);
		mesh->CreateMesh(draw);
		mesh->Register("Flag");
		break;


	case Mesh_BlueFlag:
		draw = new Drawable();
		*draw = Drawable::BlueScheme;
		draw->layer = 10;
		mesh = new Mesh(4);
		mesh->Poly.SetVert(0) = Math::Point2D(-0.375f, -0.25f);
		mesh->Poly.SetVert(1) = Math::Point2D(0.375f, 0.0f);
		mesh->Poly.SetVert(2) = Math::Point2D(-0.375f, 0.25f);
		mesh->Poly.SetVert(3) = Math::Point2D(-0.375f, -0.25f);
		mesh->CreateMesh(draw);
		mesh->Register("BlueFlag");
		break;


	case Mesh_RedFlag:
		draw = new Drawable();
		*draw = Drawable::RedScheme;
		draw->layer = 10;
		mesh = new Mesh(4);
		mesh->Poly.SetVert(0) = Math::Point2D(-0.375f, -0.25f);
		mesh->Poly.SetVert(1) = Math::Point2D(0.375f, 0.0f);
		mesh->Poly.SetVert(2) = Math::Point2D(-0.375f, 0.25f);
		mesh->Poly.SetVert(3) = Math::Point2D(-0.375f, -0.25f);
		mesh->CreateMesh(draw);
		mesh->Register("RedFlag");
		break;


	case Mesh_PauseBox:
		draw = new Drawable();
		*draw = Drawable::GrayScheme;
		mesh = new Mesh(5);
		mesh->Poly.SetVert(0) = Math::Point2D(1.f, 1.f);
		mesh->Poly.SetVert(1) = Math::Point2D(1.f, -1.f);
		mesh->Poly.SetVert(2) = Math::Point2D(-1.f, -1.f);
		mesh->Poly.SetVert(3) = Math::Point2D(-1.f, 1.f);
		mesh->Poly.SetVert(4) = Math::Point2D(1.f, 1.f);
		mesh->CreateMesh(draw);
		mesh->Register("PauseBox");
		break;

	}
}

void Mesh::Init_All_Meshes(void)
{
	Shader_Init();
	for (unsigned i = 0; i < Mesh_FINAL; ++i)
	{
		Init_Mesh(i);
	}
}
