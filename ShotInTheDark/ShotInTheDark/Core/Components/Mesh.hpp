//	File name:		Mesh.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../Component.hpp"
#include "../Systems/Physics/Shape.hpp"
#include <map>
#include <list>
#include <utility>
#include <string>
#include <vector>
#include "../Systems/Graphics/BufferObjects.hpp"

enum Meshes
{
	Mesh_TurretBullet,
	Mesh_RedBullet,
	Mesh_BlueBullet,
	Mesh_Box,
	Mesh_Turret,
	Mesh_RedTurr,
	Mesh_NeutralTurr,
	Mesh_PowerUp,
	Mesh_BluePlayer,
	Mesh_RedPlayer,
	Mesh_Strawberry,
	Mesh_BlueBase,
	Mesh_RedBase,
	Mesh_Flag,
	Mesh_BlueFlag,
	Mesh_RedFlag,
	Mesh_PauseBox,


	Mesh_FINAL
};

struct Drawable;
struct Transform;

struct Mesh
{
public:
	Mesh()
	{
		meshName = "Mesh";
		vao = nullptr;
		vbo = nullptr;
	}
	Mesh(unsigned vertices) :Poly(vertices)
	{
		meshName = "Mesh";
		vao = nullptr;
		vbo = nullptr;
	}

	void Register(std::string name);
	void Init();
	void Free();
	~Mesh(){}


	Poly Poly;
	std::string meshName;
	VAO* vao;
	VBO* vbo;
	//static GLuint shader;
	Drawable* drawable;

	static void Init_Mesh(Meshes number);
	static void Init_Mesh(unsigned number);
	static void Init_All_Meshes(void);

	static Mesh* Get_Mesh(std::string name);

	void Specify_Attributes(void);
	void CreateMesh(Drawable* drawable);
	void Draw(Drawable* draw, Math::Matrix4& matrix);
	void Update_Vertices(Entity *entity);
	static bool AddMesh(std::string name, const ::Poly poly);
	static void SetMesh(std::string name, const ::Poly poly);
	static const ::Poly GetMesh(std::string name);


};

void Shader_Init();
extern std::list <std::pair <std::string, Mesh*>> MeshList;
extern unsigned uniform;