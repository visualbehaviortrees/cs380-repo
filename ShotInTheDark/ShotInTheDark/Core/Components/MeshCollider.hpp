
#pragma once
#include "../Component.hpp"
#include "../Systems/Physics/Shape.hpp"

struct Mesh : public Component
{
public:
	Mesh():Component(CMP(Mesh)){}
	void Init(){}
	void Free(){}
	~Mesh(){}

  static Component* CreateComponent() { return new Mesh(); }
	Poly A;
	// vertices should be in clockwise rotation.
	// std::vector<Vector2DClass> vertices;
};
