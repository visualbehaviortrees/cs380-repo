//  File name:    Minimap.h
//  Project name: Shot In The Dark
//  Author(s):    Allan Deutsch
//  
//  All content © 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once
#include "../Component.hpp"

struct Minimap : public Component
{
public:
  Minimap():Component(CMP(Minimap)){}
  void Init(){}
  void Free(){}
  ~Minimap(){}

  static Component *CreateComponent() { return new Minimap(); }


};
