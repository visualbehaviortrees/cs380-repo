//	File name:		MoveToDestination.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Connor Drose
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "Movement.hpp"
#include "../Core/Systems/Gameplay/DungeonGameplay.h"

void Movement::Init()
{
	int(*h)(int, int, int, int) = PathFind::OctileHeuristic;

	astar.Initialize(h, 1.02f, false);
}

bool Movement::SetDestination(Point2D pos)
{
	waypointList.clear();
	astar.Reset();
	owner->GET_COMPONENT(RigidBody)->speed = 0.0f;

	if (astar.Run(trans->Position, pos))
	{
		astar.PopulatePath(waypointList);
	}
	else
	{
		return false;
	}

	return true;
}

// returns if two points are close enough to call good
bool CloseEnough(Point2D myPos, Point2D target)
{
	Vector2D distance = target - myPos;

	return distance.x * distance.x + distance.y * distance.y < 0.1;
}

void Movement::PreInit(Entity *owner)
{
	this->owner = owner;
	this->trans = owner->GET_COMPONENT(Transform);

	g_terrain->GetRowColumn(trans->Position, &curX, &curY);
}

Point2D Movement::GetDestination()
{
	assert(waypointList.size() > 1);

	return waypointList.back();
}

void Movement::Stop()
{
	owner->GET_COMPONENT(RigidBody)->speed = 0.0f;
	waypointList.clear();
}

void Movement::Update(float dt)
{
	if (waypointList.size())
	{
		owner->GET_COMPONENT(RigidBody)->speed = 5.0f;

		if (CloseEnough(trans->Position, waypointList.front())){
			waypointList.pop_front();

			if (!waypointList.size())
			{
				owner->GET_COMPONENT(RigidBody)->speed = 0.0f;
				return;
			}
		}

		Vector2D dir = waypointList.front() - trans->Position;
		dir.Normalize();

		assert(dir.x == dir.x);

		/// MOVE HERE
		owner->GET_COMPONENT(RigidBody)->direction = dir;

		int x, y;

		g_terrain->GetRowColumn(trans->Position, &x, &y);

		if (x != curX || y != curY)
		{
			g_terrain->MoveTile(curX, curY, x, y);
			curX = x;
			curY = y;
		}
	}
}