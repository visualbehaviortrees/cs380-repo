//	File name:		MoveToDestination.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Connor Drose
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "Component.hpp"
#include "../../Core/Systems/AI/Astar.h"

// responsible for moving the entity to the set destination
class Movement : public Component
{
public:
	Movement() : Component(CMP(Movement)){}

	void Init();

	void Update(float dt);
	bool SetDestination(Point2D pos);
	Point2D GetDestination();

	// save pointers to my entity components 
	void PreInit(Entity *owner);

	int Size()
	{
		return waypointList.size();
	}

	void Stop();

private:
	Transform* trans = nullptr;
	Entity *owner = nullptr;
	int curX, curY;

	std::list<Point2D> waypointList;

	PathFind::Astar astar;
};

                                           