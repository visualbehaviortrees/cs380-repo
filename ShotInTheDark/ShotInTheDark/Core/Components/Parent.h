//	File name:		Parent.h
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once
#include "../Component.hpp"
#include "../Entity.hpp"

struct Parent : public Component
{
public:
  Parent():Component(CMP(Parent)),parent(NULL){}
  Parent(Entity *e):Component(CMP(Parent)),parent(e){}
  void Init(){}
  void Free(){}
  ~Parent(){}

  static Component *CreateComponent() { return new Parent(); }

  Entity *parent;

};
