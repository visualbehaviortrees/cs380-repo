//	File name:		ParticleEffect.h
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch & Gonzalo Rojo
//
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../Component.hpp"
#include "../Systems/Particles/ParticleSystem.h"
#include <vector>

struct ParticleEffect : public Component
{
public:
	ParticleEffect():Component(CMP(ParticleEffect)){}

	~ParticleEffect(){}

  static Component *CreateComponent() { return new ParticleEffect(); }

  virtual void Init() {}
  virtual void Update(float dt) {}
  virtual void Draw() {}
  virtual void Free() {}
  virtual bool OnOrOff() { return true; }
  virtual void SetBool(bool boolean) {}

};

class CircleEffect : public ParticleEffect
{
public:
  CircleEffect(Transform* trans, int ParticleCount = 1000, double maxTime = -1.0,
              glm::vec4 minstartcolor = glm::vec4(1.0f, 0.3f, 0.0f, 0.0f),
              glm::vec4 maxstartcolor = glm::vec4(1.0f, 0.3f, 0.0f, 1.0f),
              glm::vec4 minendcolor = glm::vec4(0.3f, 1.0f, 0.0f, 0.5f),
              glm::vec4 maxendcolor = glm::vec4(0.3f, 1.0f, 0.3f, 0.0f),
              bool OnOrOff = true)
  {
    m_minstartcolor = minstartcolor;
    m_maxstartcolor = maxstartcolor;
    m_minendcolor = minendcolor;
    m_maxendcolor = maxendcolor;
    m_particlecount = ParticleCount;
    m_maxTime = maxTime;
    mp_trans = trans;
    YesOrNo = OnOrOff;
  }
  virtual void Init();
  virtual void Update(float dt);
  virtual void Draw();
  virtual bool OnOrOff() { return YesOrNo; }
  virtual void SetBool(bool boolean) { YesOrNo = boolean; }

  ParticleSystem *p_sys;
  int m_particlecount;
  double m_maxTime;
  Transform* mp_trans;
  glm::vec4 m_minstartcolor;
  glm::vec4 m_maxstartcolor;
  glm::vec4 m_minendcolor;
  glm::vec4 m_maxendcolor;
  bool YesOrNo;
};

class ChaseEffect : public ParticleEffect
{
public:
  ChaseEffect(Transform* trans, int ParticleCount = 100, double maxTime = -1.0,
              glm::vec4 minstartcolor = glm::vec4(1.0f, 0.3f, 0.0f, 0.0f),
              glm::vec4 maxstartcolor = glm::vec4(1.0f, 0.3f, 0.0f, 1.0f),
              glm::vec4 minendcolor = glm::vec4(0.3f, 1.0f, 0.0f, 0.5f),
              glm::vec4 maxendcolor = glm::vec4(0.3f, 1.0f, 0.3f, 0.0f),
			        int rate = 2, bool OnOrOff = true)
  {
    mp_trans = trans;
    m_maxTime = maxTime;
    m_particlecount = ParticleCount;
    m_minstartcolor = minstartcolor;
    m_maxstartcolor = maxstartcolor;
    m_minendcolor = minendcolor;
    m_maxendcolor = maxendcolor;
  	m_rate = rate;
    YesOrNo = OnOrOff;
  }
  virtual void Init();
  virtual void Update(float dt);
  virtual void Draw();
  virtual bool OnOrOff() { return YesOrNo; }
  virtual void SetBool(bool boolean) { YesOrNo = boolean; }

  ParticleSystem *p_sys;
  int m_particlecount;
  double m_maxTime;
  int m_rate;
  Transform* mp_trans;
  glm::vec4 m_minstartcolor;
  glm::vec4 m_maxstartcolor;
  glm::vec4 m_minendcolor;
  glm::vec4 m_maxendcolor;
  bool YesOrNo;
};

class ShieldEffect : public ParticleEffect
{
public:
  ShieldEffect(Transform* trans, int ParticleCount = 500, double maxTime = -1.0,
    glm::vec4 minstartcolor = glm::vec4(1.0f, 0.3f, 0.0f, 0.0f),
    glm::vec4 maxstartcolor = glm::vec4(1.0f, 0.3f, 0.0f, 1.0f),
    glm::vec4 minendcolor = glm::vec4(0.3f, 1.0f, 0.0f, 0.5f),
    glm::vec4 maxendcolor = glm::vec4(0.3f, 1.0f, 0.3f, 0.0f),
    int rate = 2, bool OnOrOff = true)
  {
    mp_trans = trans;
    m_maxTime = maxTime;
    m_particlecount = ParticleCount;
    m_minstartcolor = minstartcolor;
    m_maxstartcolor = maxstartcolor;
    m_minendcolor = minendcolor;
    m_maxendcolor = maxendcolor;
    m_rate = rate;
    YesOrNo = OnOrOff;
  }
  virtual void Init();
  virtual void Update(float dt);
  virtual void Draw();
  virtual bool OnOrOff() { return YesOrNo; }
  virtual void SetBool(bool boolean) { YesOrNo = boolean; }

  ParticleSystem *p_sys;
  int m_particlecount;
  double m_maxTime;
  int m_rate;
  Transform* mp_trans;
  glm::vec4 m_minstartcolor;
  glm::vec4 m_maxstartcolor;
  glm::vec4 m_minendcolor;
  glm::vec4 m_maxendcolor;
  bool YesOrNo;
};

class GoToPosEffect : public ParticleEffect
{
public:
  GoToPosEffect(Transform* trans, Transform *Pos, int ParticleCount = 100, double maxTime = -1.0,
                glm::vec4 minstartcolor = glm::vec4(1.0f, 0.3f, 0.0f, 0.0f),
                glm::vec4 maxstartcolor = glm::vec4(1.0f, 0.3f, 0.0f, 1.0f),
                glm::vec4 minendcolor = glm::vec4(0.3f, 1.0f, 0.0f, 0.5f),
                glm::vec4 maxendcolor = glm::vec4(0.3f, 1.0f, 0.3f, 0.0f),
                bool OnOrOff = true)
  {
    m_minstartcolor = minstartcolor;
    m_maxstartcolor = maxstartcolor;
    m_minendcolor = minendcolor;
    m_maxendcolor = maxendcolor;
    m_particlecount = ParticleCount;
    m_maxTime = maxTime;
    m_PosToChase = Pos;
    mp_trans = trans;
    YesOrNo = OnOrOff;
  }

  virtual void Init();
  virtual void Update(float dt);
  virtual void Draw();
  virtual bool OnOrOff() { return YesOrNo; }
  virtual void SetBool(bool boolean) { YesOrNo = boolean; }

  ParticleSystem* p_sys;
  int m_particlecount;
  double m_maxTime;
  Transform* m_PosToChase;
  Transform* mp_trans;
  glm::vec4 m_minstartcolor;
  glm::vec4 m_maxstartcolor;
  glm::vec4 m_minendcolor;
  glm::vec4 m_maxendcolor;
  bool YesOrNo;
};

class FireEffect : public ParticleEffect
{
public:
  FireEffect(Transform* trans, int ParticleCount = 100, double maxTime = -1.0,
    glm::vec4 minstartcolor = glm::vec4(1.0f, 0.3f, 0.0f, 0.0f),
    glm::vec4 maxstartcolor = glm::vec4(1.0f, 0.3f, 0.0f, 1.0f),
    glm::vec4 minendcolor = glm::vec4(0.3f, 1.0f, 0.0f, 0.5f),
    glm::vec4 maxendcolor = glm::vec4(0.3f, 1.0f, 0.3f, 0.0f),
    bool OnOrOff = true)
  {
    m_minstartcolor = minstartcolor;
    m_maxstartcolor = maxstartcolor;
    m_minendcolor = minendcolor;
    m_maxendcolor = maxendcolor;
    m_particlecount = ParticleCount;
    m_maxTime = maxTime;
    mp_trans = trans;
    YesOrNo = OnOrOff;
  }

  virtual void Init();
  virtual void Update(float dt);
  virtual void Draw();
  virtual bool OnOrOff() { return YesOrNo; }
  virtual void SetBool(bool boolean) { YesOrNo = boolean; }

  ParticleSystem* p_sys;
  int m_particlecount;
  double m_maxTime;
  Transform* mp_trans;
  glm::vec4 m_minstartcolor;
  glm::vec4 m_maxstartcolor;
  glm::vec4 m_minendcolor;
  glm::vec4 m_maxendcolor;
  bool YesOrNo;
};

class ExplosionEffect : public ParticleEffect
{
public:
  ExplosionEffect(Transform* trans, int ParticleCount = 300, double maxTime = 0.7,
    glm::vec4 minstartcolor = glm::vec4(1.0f, 0.3f, 0.0f, 0.0f),
    glm::vec4 maxstartcolor = glm::vec4(1.0f, 0.3f, 0.0f, 1.0f),
    glm::vec4 minendcolor = glm::vec4(0.3f, 1.0f, 0.0f, 0.5f),
    glm::vec4 maxendcolor = glm::vec4(0.3f, 1.0f, 0.3f, 0.0f),
    bool OnOrOff = true)
  {
    m_minstartcolor = minstartcolor;
    m_maxstartcolor = maxstartcolor;
    m_minendcolor = minendcolor;
    m_maxendcolor = maxendcolor;
    m_particlecount = ParticleCount;
    m_maxTime = maxTime;
    mp_trans = trans;
    YesOrNo = OnOrOff;
  }

  virtual void Init();
  virtual void Update(float dt);
  virtual void Draw();
  virtual bool OnOrOff() { return YesOrNo; }
  virtual void SetBool(bool boolean) { YesOrNo = boolean; }

  ParticleSystem* p_sys;
  int m_particlecount;
  double m_maxTime;
  Transform* mp_trans;
  glm::vec4 m_minstartcolor;
  glm::vec4 m_maxstartcolor;
  glm::vec4 m_minendcolor;
  glm::vec4 m_maxendcolor;
  bool YesOrNo;

};