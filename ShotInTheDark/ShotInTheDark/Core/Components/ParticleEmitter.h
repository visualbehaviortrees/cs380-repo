#pragma once
#include "../Component.hpp"
#include "../Libraries/Math/Math2D.hpp"
#include "../Systems/Particles/Particle.h"

#define MAX_PARTICLES 100

struct ParticleEmitter : public Component
{
public:
  ParticleEmitter() : Component(CMP(ParticleEmitter)), endIndex(0), startIndex(0),
                      moveSpeed(0.f), rotSpeed(0.f), emitSpeed(1.f), lifeSpan(3.f)
  {
  }
  void Init(){}
  void Free(){}
  ~ParticleEmitter(){}

  static Component *CreateComponent() { return new ParticleEmitter(); }

  float moveSpeed;
  float rotSpeed;
  float emitSpeed;
  float emitDirection;
  float emitSpread;
  float lifeSpan;

  int startIndex, endIndex;

  Particle particles[MAX_PARTICLES];
};





