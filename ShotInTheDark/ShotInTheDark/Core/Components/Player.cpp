//	File name:		Player.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
// 
//	Created 1/20	
//
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.
#include "stdafx.h"
#include "Player.hpp"

#include "../Systems/Input/InputSystem.hpp"

float Player::base_health = 9.0f;
float Player::base_reload = 1.0f;
float Player::base_speed = 6.66f;
float Player::base_light = 25.0f;

// How many directions can we move?
Move_Style gMoveStyle = Move_Free;
// How many directions can we look?
Look_Style gLookStyle = Look_Free;
// Do we automatically shoot as we aim?
bool gShootOnLook = true;

int PlayerControls::_controllers_used = 0;

float PlayerControls::Move_Up()
{
	float input = CheckJSAxis(ControllerNum, LEFT_Y_AXIS);
	//input = input > LEFT_AXIS_THRESHOLD ? input : 0.0f;

	if(CheckKeyPressed(move_up))
		input = 1.0f;

	switch(gMoveStyle)
	{
	case(Move_8Way):
		// Force input to 1 or 0
		input = input > LEFT_AXIS_THRESHOLD ? 1.0f : 0.0f;
		break;

	case(Move_4Way):
		// compare controller Y axis against controller Y
		if(input < CheckJSAxis(ControllerNum, LEFT_X_AXIS) ||
			input < -CheckJSAxis(ControllerNum, LEFT_X_AXIS) ||
			CheckKeyPressed(move_left) || CheckKeyPressed(move_right))
			input = 0.0f;
		else
			input = 1.0f;

	case(Move_Free):
	default:
		break;
	};

	return input > LEFT_AXIS_THRESHOLD ? input : 0;

}

float PlayerControls::Move_Down()
{
	float input = -CheckJSAxis(ControllerNum, LEFT_Y_AXIS);

	if(CheckKeyPressed(move_down))
		input = 1.0f;

	switch(gMoveStyle)
	{
	case(Move_8Way):
		// Force input to 1 or 0
		input = input > LEFT_AXIS_THRESHOLD ? 1.0f : 0.0f;
		break;

	case(Move_4Way):
		// compare controller Y axis against controller Y
		if(input < CheckJSAxis(ControllerNum, LEFT_X_AXIS) ||
			input < -CheckJSAxis(ControllerNum, LEFT_X_AXIS)||
			CheckKeyPressed(move_left) || CheckKeyPressed(move_right))
			input = 0.0f;
		else
			input = 1.0f;

		break;

	case(Move_Free):
	default:
		break;
	};

	return input > LEFT_AXIS_THRESHOLD ? input : 0;

}

float PlayerControls::Move_Left()
{
	float input = -CheckJSAxis(ControllerNum, LEFT_X_AXIS);
	
	if(CheckKeyPressed(move_left))
		input = 1.0f;

	switch(gMoveStyle)
	{
	case(Move_8Way):
		// Force input to 1 or 0
		input = input > LEFT_AXIS_THRESHOLD ? 1.0f : 0.0f;
		break;

	case(Move_4Way):
		// compare controller Y axis against controller Y
		if(input < CheckJSAxis(ControllerNum, LEFT_Y_AXIS) ||
			input < -CheckJSAxis(ControllerNum, LEFT_Y_AXIS))
			input = 0.0f;
		else
			input = 1.0f;
		break;

	case(Move_Free):
	default:
		break;
	};

	return input > LEFT_AXIS_THRESHOLD ? input : 0;
}

float PlayerControls::Move_Right()
{
	float input = CheckJSAxis(ControllerNum, LEFT_X_AXIS);

	if(CheckKeyPressed(move_right))
		input = 1.0f;

	switch(gMoveStyle)
	{
	case(Move_8Way):
		// Force input to 1 or 0
		input = input > LEFT_AXIS_THRESHOLD ? 1.0f : 0.0f;
		break;

	case(Move_4Way):
		// compare controller Y axis against controller Y
		if(input < CheckJSAxis(ControllerNum, LEFT_Y_AXIS) ||
			input < -CheckJSAxis(ControllerNum, LEFT_Y_AXIS))
			input = 0.0f;
		else
			input = 1.0f;
		break;

	case(Move_Free):
	default:
		break;
	};

	return input > LEFT_AXIS_THRESHOLD ? input : 0;

}

float PlayerControls::Look_Left()
{
	float input = -CheckJSAxis(ControllerNum, RIGHT_X_AXIS);

	if(CheckKeyPressed(look_left))
		input = 1.0f;

	switch(gLookStyle)
	{
	case(Look_8way):
		// Force input to 1 or 0
		input = input > RIGHT_AXIS_THRESHOLD ? 1.0f : 0.0f;
		break;

	case(Look_4Way):
		// compare controller Y axis against controller Y
		if(input < CheckJSAxis(ControllerNum, RIGHT_Y_AXIS) ||
			input < -CheckJSAxis(ControllerNum, RIGHT_Y_AXIS))
			input = 0.0f;
		else
			input = 1.0f;
		break;

	case(Look_MoveLocked):
		input = Move_Left();
		break;

	case(Look_Free):
	default:
		break;
	};

	return input > RIGHT_AXIS_THRESHOLD ? input : 0;
}

float PlayerControls::Look_Right()
{
	float input = CheckJSAxis(ControllerNum, RIGHT_X_AXIS);

	if(CheckKeyPressed(look_right))
		input = 1.0f;

	switch(gLookStyle)
	{
	case(Look_8way):
		// Force input to 1 or 0
		input = input > RIGHT_AXIS_THRESHOLD ? 1.0f : 0.0f;
		break;

	case(Look_4Way) :
		// compare controller Y axis against controller Y
		if (input < CheckJSAxis(ControllerNum, RIGHT_Y_AXIS) ||
			input < -CheckJSAxis(ControllerNum, RIGHT_Y_AXIS))
			input = 0.0f;
		else
			input = 1.0f;
		break;

	case(Look_MoveLocked):
		input = Move_Right();
		break;

	case(Look_Free):
		break;

	default:
		break;
	};

	return input > RIGHT_AXIS_THRESHOLD ? input : 0;

}

float PlayerControls::Look_Up()
{
	float input = CheckJSAxis(ControllerNum, RIGHT_Y_AXIS);

	if(CheckKeyPressed(look_up))
		input = 1.0f;

	switch(gLookStyle)
	{
	case(Look_8way):
		// Force input to 1 or 0
		input = input > RIGHT_AXIS_THRESHOLD ? 1.0f : 0.0f;
		break;

	case(Look_4Way):
		// compare controller Y axis against controller Y
		if(input < CheckJSAxis(ControllerNum, RIGHT_X_AXIS) ||
			input < -CheckJSAxis(ControllerNum, RIGHT_X_AXIS) ||
			CheckKeyPressed(look_left) || CheckKeyPressed(look_right))
			input = 0.0f;
		else
			input = 1.0f;
		break;

	case(Look_MoveLocked):
		input = Move_Up();
		break;

	case(Look_Free):
		break;

	default:
		break;
	};

	return input > RIGHT_AXIS_THRESHOLD ? input : 0;

}

float PlayerControls::Look_Down()
{
float input = -CheckJSAxis(ControllerNum, RIGHT_Y_AXIS);

	if(CheckKeyPressed(look_down))
		input = 1.0f;

	switch(gLookStyle)
	{
	case(Look_8way):
		// Force input to 1 or 0
		input = input > RIGHT_AXIS_THRESHOLD ? 1.0f : 0.0f;
		break;

	case(Look_4Way):
		// compare controller Y axis against controller Y
		if(input < CheckJSAxis(ControllerNum, RIGHT_X_AXIS) ||
			input < -CheckJSAxis(ControllerNum, RIGHT_X_AXIS) ||
			CheckKeyPressed(look_left) || CheckKeyPressed(look_right))
			input = 0.0f;
		else
			input = 1.0f;
		break;

	case(Look_MoveLocked):
		input = Move_Down();
		break;

	case(Look_Free):
		break;

	default:
		break;
	};

	return input > RIGHT_AXIS_THRESHOLD ? input : 0;

}

bool PlayerControls::Shoot()
{
	if (gShootOnLook)
	{
		const float look_trigger = 0.33f;
		return (Look_Left() > look_trigger ||
			Look_Right() > look_trigger ||
			Look_Up() > look_trigger ||
			Look_Down() > look_trigger);
	}
	else
	{
		return (CheckKeyTriggered(shoot) || CheckJSButtonTriggered(ControllerNum, JS_BUTTON_6));
	}
}

int Player::GetNum()
{
	return num;
}

/*
	Give me "Purple" to load "players/Waffle_Purple_Move_Left.png" and friends
*/
void Player::Load_Texture_Set()
{
	// Decide which waffle we are here
	std::string wafflecolor;

	switch (num)
	{
	case 1:
		wafflecolor = "Purple";
		break;

	case 2:
		wafflecolor = "Pink";
		break;

	case 3:
		wafflecolor = "Teal";
		break;

	case 4:
		wafflecolor = "Orange";
		break;

	default:
		wafflecolor = "Purple";
		std::cout << "TOO MANY PLAYERS!";
		break;
	}


	tex_left = Animation(tex_prefix + wafflecolor + tex_postfix_left);
	tex_down = Animation(tex_prefix + wafflecolor + tex_postfix_down);
	tex_right = Animation(tex_prefix + wafflecolor + tex_postfix_right);
	tex_up = Animation(tex_prefix + wafflecolor + tex_postfix_up);
	tex_icon = tex_prefix + wafflecolor + tex_postfix_icon;


}

bool Player::InDoor()
{
	return state == Player_InDoor;
}

bool Player::IsAlive()
{
	return state == Player_Alive;
}

bool Player::IsDead()
{
	return state == Player_Dead;
}

glm::vec3 Player::GetColor()
{
	const float Desaturation = 0.0f;
	const float Darken = 0.0f;
	glm::vec3 color;
	// Start with the color of the wizard's hat
	switch (num)
	{
	default:
	case 1 :
		color = glm::vec3(0.616f, 0.329f, 0.878f);
		break;
	case 2:
		color = glm::vec3(0.824f, 0.447f, 0.788f);
		break;
	case 3:
		color = glm::vec3(0.122f, 0.631f, 0.631f);
		break;
	case 4:
		color = glm::vec3(0.843f, 0.472f, 0.212f);
		break;
	}
	// Then desaturate each value
	color = (color + Desaturation * glm::vec3(1.0f, 1.0f, 1.0f)) / (Desaturation + 1.0f);
	// And darken it
	color *= (1.0f - Darken);
	return color;
}