//	File name:		Player.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../Component.hpp"
#include "../Systems/Input/InputSystem.hpp"
#include "../Systems/Graphics/Animation.h"


struct PlayerControls
{
	// Default to keyboard
	PlayerControls(JOYSTICKS num = JOYSTICK_1) :
		move_up(KEY_W), move_down(KEY_S), move_left(KEY_A), move_right(KEY_D),
		look_up(KEY_UP), look_down(KEY_DOWN), look_left(KEY_LEFT), look_right(KEY_RIGHT),
		shoot(KEY_SPACE), ControllerNum(num)
	{
	}

	// Helper functions to check both keyboard and mouse
	float Move_Up();
	float Move_Down();
	float Move_Left();
	float Move_Right();

	float Look_Left();
	float Look_Right();
	float Look_Up();
	float Look_Down();

	bool Shoot();
	
private:
	// Keys & Controller num
	KEYS move_up;
	KEYS move_down;
	KEYS move_left;
	KEYS move_right;

	KEYS look_up;
	KEYS look_down;
	KEYS look_left;
	KEYS look_right;

	KEYS shoot;

	JOYSTICKS ControllerNum;

	static int _controllers_used;
};

enum Move_Style {
	Move_Free,
	Move_4Way, // Left/Right axis gets priority
	Move_8Way
};

enum Look_Style {
	Look_Free,
	Look_4Way,
	Look_8way,
	Look_MoveLocked
};

extern Move_Style gMoveStyle;
extern Look_Style gLookStyle;

enum Player_State
{
	Player_Alive,
	Player_Dead,
	Player_InDoor
};

struct Player : public Component
{
public:
	friend class PlayerHandler;
	friend class HUDsystem;

	// Gameplay stats
	static float base_reload;
	static float base_health;
	static float base_speed;
	static float base_light;
	

	Player(int player_num = 1):Component(CMP(Player)),reloadCounter(0.0f),reloadSpeed(base_reload),
		controls((JOYSTICKS)(player_num - 1)), num(player_num), state(Player_Alive)
           {}
  void Init(){}
	void Free(){}
	~Player(){}

  static Component *CreateComponent() { return new Player(); }

  float reloadCounter;
  float reloadSpeed;

  float moveSpeed = base_speed;

  float safeTime; // How long after taking damage until we can take more
  float timeSinceLastHit;

  PlayerControls controls;

  // What # player are we?
  int GetNum();

  bool InDoor();
  bool IsAlive();
  bool IsDead();
  glm::vec3 GetColor();


private:

	int num;
	/*
		Give me "Purple" to load "players/Waffle_Purple_Move_Left.png" and friends
	*/
	void Load_Texture_Set();
	const std::string tex_prefix = "Wizard_";
	const std::string tex_postfix_left =	"_Left.anim";
	const std::string tex_postfix_right =	"_Right.anim";
	const std::string tex_postfix_up =		"_Backward.anim";
	const std::string tex_postfix_down =	 "_Forward.anim";

	const std::string tex_postfix_icon = "_Icon.png";

	Player_State state;

  Animation  tex_left;
  Animation  tex_up;
  Animation  tex_right;
  Animation  tex_down;
  std::string  tex_icon;

};
