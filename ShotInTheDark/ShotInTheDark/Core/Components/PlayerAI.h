//	File name:		PlayerAI.h
//	Project name:	Shot In The Dark
//	Author(s):		Connor Drose
//	
//	All content � 2014 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../Component.hpp"
#include "../Systems/AI/Actions.hpp"
#include "../Libraries/Math/Math2D.hpp"
#include "../Systems/AI/AIStateEnum.hpp"
#include "Systems/AI/MoveClass.hpp"

#define PATH_AMOUNT 50


struct PlayerAI : public Component
{
public:
  PlayerAI() : Component(CMP(PlayerAI)) {}
  void Init()
  {
    aiState = AIState::gather;
    prevAiState = AIState::defend;
    enemyPos = Math::Vector2D(0.f,0.f);
    decideStateTime = 0.f;
    lookTimer = 0.f;
    lookDir = Math::Vector2D(0.f, -1.f);
    lookTar = Math::Vector2D(0.f, -1.f);
  }
  void Free(){}
  ~PlayerAI(){}

  static Component *CreateComponent() { return new PlayerAI(); }


  AIState::STATE aiState;
  AIState::STATE prevAiState;
  Math::Point2D destination; // position to move to
  Math::Vector2D enemyPos;  // target's last known position

  float decideStateTime;

  // look data
  Math::Vector2D lookTar;
  Math::Vector2D lookDir;
  float lookTimer;


  // Attack state stuff
  Entity *enemyTarget;      // the thing we are currently chasing / shooting

  // Gather state stuff
  Entity *objTarget;      // the objective we are going after
  Math::Vector2D searchDir;  // general direction we are moving in


  // CLASS THAT MOVES THIS ENTITY
  MoveClass Move;

  std::vector<Action> actionList; 
};


