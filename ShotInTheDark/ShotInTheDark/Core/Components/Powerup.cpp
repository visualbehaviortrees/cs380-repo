//	File name:		Powerup.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch, Luc Kadletz
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "Components.h"
#include "Powerup.h"
#include "../Engine.hpp"
#include "../Systems/PlayerHandler/PlayerHandler.hpp"
void Debug_Verify_Owner(Powerup* component, Entity* owner)
{

// We shouldn't do anything silly like give a powerup to one entity 
// and say its added to another
#ifdef _DEBUG
	assert(owner->GET_COMPONENT(Powerup) == component);
#endif // _DEBUG
}

void Powerup::AddEffect(PowerupEffect* p_effect, Entity* owner)
{
	Debug_Verify_Owner(this, owner);

	// Tell the powerup to say hi and do any constant changes it has to make
	p_effect->ApplyEffect(owner);
	// add it to our list
	effects.push_back(p_effect);
}

PowerupEffect* Powerup::RemoveLastEffect(Entity* owner)
{
	Debug_Verify_Owner(this, owner);

	if (effects.empty())
		return 0;

	// Tell the powerup to say goodbye and undo any constant changes it made
	effects.back()->RemoveEffect(owner);
	// Give it back to the powerup system and hope it deletes it
	PowerupEffect* p = effects.back();
	effects.pop_back();
	return p;
}

void Powerup::OnShoot(Entity* player, std::vector<Entity*>& bullets)
{
	Debug_Verify_Owner(this, player);

	for (PowerupEffect* p : effects)
		p->OnShoot(player, bullets);
}

void Powerup::OnTakeDamage(Entity* player, float damage, Entity* source)
{
	Debug_Verify_Owner(this, player);

	for (PowerupEffect* p : effects)
		p->OnTakeDamage(player, damage, source);
}



/// MoveSpeed ================================================================
Powerup_MoveSpeed::Powerup_MoveSpeed(float speed)
	: speed_amount(speed)
{ }

void Powerup_MoveSpeed::ApplyEffect(Entity* target)
{
	target->GET_COMPONENT(Player)->moveSpeed += speed_amount;
}

void Powerup_MoveSpeed::RemoveEffect(Entity* target)
{
	target->GET_COMPONENT(Player)->moveSpeed -= speed_amount;
}

// AttackSpeed ================================================================
Powerup_AttackSpeed::Powerup_AttackSpeed(float speed)
	: speed_amount(speed)
{ }

void Powerup_AttackSpeed::ApplyEffect(Entity* target)
{
	if (target->HasComponent(EC_Player))
		target->GET_COMPONENT(Player)->reloadSpeed -= speed_amount;
}

void Powerup_AttackSpeed::RemoveEffect(Entity* target)
{
	if (target->HasComponent(EC_Player))
		target->GET_COMPONENT(Player)->reloadSpeed += speed_amount;
}

/// RANGE ===================================================================
Powerup_Range::Powerup_Range(float range_time)
	: range_time_increase(range_time)
{ }


void Powerup_Range::OnShoot(Entity* player, std::vector<Entity*>& bullets)
{
	for (Entity* e : bullets)
		e->GET_COMPONENT(Bullet)->lifetime += range_time_increase;
}

/// DAMAGE ====================================================================
Powerup_Damage::Powerup_Damage(float damage)
	: damage_increase(damage)
{ }

void Powerup_Damage::OnShoot(Entity* player, std::vector<Entity*>& bullets)
{
	for (Entity* e : bullets)
		e->GET_COMPONENT(Bullet)->damage += damage_increase;
}

/// HEALTH ===================================================================
Powerup_Health::Powerup_Health(float hp)
	: hp_increase(hp)
{}


void Powerup_Health::ApplyEffect(Entity* target)
{
	if (target->HasComponent(EC_Health))
	{
		target->GET_COMPONENT(Health)->maxHealth += hp_increase;
		target->GET_COMPONENT(Health)->health += hp_increase;
	}
}
void Powerup_Health::RemoveEffect(Entity* target)
{
	if (target->HasComponent(EC_Health))
		target->GET_COMPONENT(Health)->maxHealth -= hp_increase;
}

Powerup_HealOnce::Powerup_HealOnce(float hp)
 : heal(hp) {}


void Powerup_HealOnce::ApplyEffect(Entity* target)
{
	if (target->HasComponent(EC_Health))
	{
		target->GET_COMPONENT(Health)->health += heal;
	}
}


// INVULNERABLE ===============================================================

Powerup_Invulnerability::Powerup_Invulnerability(float time)
	: invulnerable_time(time) {}

void Powerup_Invulnerability::ApplyEffect(Entity* target)
{
	if (target->HasComponent(EC_Health))
		target->GET_COMPONENT(Health)->timeSinceLastHit = -invulnerable_time - target->GET_COMPONENT(Health)->safeTime;
}

void Powerup_Invulnerability::RemoveEffect(Entity* target)
{
	if (target->HasComponent(EC_Health))
		target->GET_COMPONENT(Health)->timeSinceLastHit = target->GET_COMPONENT(Health)->safeTime;

}

// BOUNCY ====================================================================

void Powerup_BouncyBullets::OnShoot(Entity* player, std::vector<Entity*>& bullets)
{
	for (Entity * b : bullets)
	{
		b->GET_COMPONENT(Bullet)->pierces = true;
		b->GET_COMPONENT(Bullet)->effects.push_back(Bullet_Effect_DestroyOnDamage);
		b->GET_COMPONENT(Bullet)->effects.push_back(Bullet_Effect_Bouncy);
	}
}

// MULTISHOT ==================================================================

Powerup_MultiShot::Powerup_MultiShot(int numShots, float angle)
	:num(numShots), ang(angle) { }

// Rework this when entity deep copying is in
void Powerup_MultiShot::OnShoot(Entity* player, std::vector<Entity*>& bullets)
{
	float ang_min = -ang, ang_inc = 2.0f * ang / (num-1);

	Vector2D dir = bullets.front()->GET_COMPONENT(RigidBody)->direction;
	bullets.front()->GET_COMPONENT(RigidBody)->direction = bullets.front()->GET_COMPONENT(RigidBody)->direction % ang_min;

	for (int i = 1; i < num; ++i)
	{
		float a = ang_min + (i * ang_inc);
		bullets.push_back(GETSYS(PlayerHandler)->CreateBullet(player, dir % a));
	}
}