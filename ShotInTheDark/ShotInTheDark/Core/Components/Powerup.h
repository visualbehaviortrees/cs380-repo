//	File name:		Powerup.h
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../Component.hpp"

struct PowerupEffect
{
	// Called once, when this effect is acquired
	virtual void ApplyEffect(Entity* target) { }
	// Called once, when this effect is removed
	virtual void RemoveEffect(Entity* target) { }
	// Called as the player shoots. 
		// Modidy the bullet vector so that all effects can stack with multiple bullets
		// You may add effects to bullets with bulleteffect function pointers
	virtual void OnShoot(Entity* player, std::vector<Entity*>& bullets) { }
	// Called as the player takes damage, with the source of the damage
	virtual void OnTakeDamage(Entity* player, float damage, Entity* source) { }

	virtual ~PowerupEffect() {};
};

// This class aggregates all powerupeffects
class Powerup : public Component
{
public:
  Powerup():Component(CMP(Powerup)) {}
  ~Powerup() {};

  void AddEffect(PowerupEffect* effect, Entity* owner);
  PowerupEffect* RemoveLastEffect(Entity* owner);
  // These are aggregate functions that call all of the contained effects in order
  void OnShoot(Entity* player, std::vector<Entity*>& bullets);
  void OnTakeDamage(Entity* player, float damage, Entity* source);

private:
  std::deque<PowerupEffect *> effects;
};

// This powerup simply holds many effects and handles them as one
struct PowerupCompound : public PowerupEffect
{
	PowerupCompound(std::initializer_list<PowerupEffect*> effects)
	{
		for (PowerupEffect* e : effects)
			effects_.push_back(e);
	}

	// Called once, when this effect is acquired
	void ApplyEffect(Entity* target) 
	{
		for (PowerupEffect* e : effects_)
			e->ApplyEffect(target);
	}
	// Called once, when this effect is removed
	void RemoveEffect(Entity* target) 
	{
		for (PowerupEffect* e : effects_)
			e->RemoveEffect(target);
	}
	// Called as the player shoots. 
	// Modidy the bullet vector so that all effects can stack with multiple bullets
	// You may add effects to bullets with bulleteffect function pointers
	void OnShoot(Entity* player, std::vector<Entity*>& bullets)
	{
		for (PowerupEffect* e : effects_)
			e->OnShoot(player, bullets);
	}
	// Called as the player takes damage, with the source of the damage
	void OnTakeDamage(Entity* player, float damage, Entity* source)
	{
		for (PowerupEffect* e : effects_)
			e->OnTakeDamage(player, damage, source);
	}

	~PowerupCompound()
	{
		for (PowerupEffect* e : effects_)
			delete e;
	}

private:
	std::vector<PowerupEffect*> effects_;
};

// Increases Speed of movement
struct Powerup_MoveSpeed : public PowerupEffect
{
	Powerup_MoveSpeed(float speed = 1.0f);
	void ApplyEffect(Entity* target);
	void RemoveEffect(Entity* target);

	float speed_amount;
};

// Increases speed of firing
struct Powerup_AttackSpeed : public PowerupEffect
{
	Powerup_AttackSpeed(float speed = 0.01f);
	void ApplyEffect(Entity* target);
	void RemoveEffect(Entity* target);

	float speed_amount;
};

// Increases bullet range
struct Powerup_Range : public PowerupEffect
{
	Powerup_Range(float range_time = 0.5f);
	void OnShoot(Entity* player, std::vector<Entity*>& bullets);
	float range_time_increase;
};

// Increases damage
struct Powerup_Damage : public PowerupEffect
{
	Powerup_Damage(float dmg = 0.5f);
	float damage_increase;

	void OnShoot(Entity* player, std::vector<Entity*>& bullets);
};

// Increases health
struct Powerup_Health : public PowerupEffect
{
	Powerup_Health(float hp = 1.0f);
	float hp_increase;

	void ApplyEffect(Entity* target);
	void RemoveEffect(Entity* target);
};


// Increases health
struct Powerup_HealOnce : public PowerupEffect
{
	Powerup_HealOnce(float hp = 1.0f);
	float heal;

	void ApplyEffect(Entity* target);
};


// Temporary invulnerability
struct Powerup_Invulnerability : public PowerupEffect
{
	Powerup_Invulnerability(float time);

	float invulnerable_time;

	void ApplyEffect(Entity* target);
	void RemoveEffect(Entity* target);
};


// Bouncy blueberry bullets
struct Powerup_BouncyBullets : public PowerupEffect
{
	void OnShoot(Entity* player, std::vector<Entity*>& bullets);
};


// Multiple shots
struct Powerup_MultiShot : public PowerupEffect
{
	Powerup_MultiShot(int numShots, float angle = 45.0f);
	void OnShoot(Entity* player, std::vector<Entity*>& bullets);

	int num;
	float ang;
};


// TEST POWERUPS
struct Test_PUP_1 : public PowerupEffect
{
	inline void ApplyEffect(Entity* target) 
	{
		std::cout << "TEST POWERUP applied to: " << target->GetName() << '\n';
	}

	inline void RemoveEffect(Entity* target) 
	{
		std::cout << "TEST POWERUP removed from: " << target->GetName() << '\n';
	}

	inline void OnShoot(Entity* player, std::vector<Entity*>& bullets)
	{
		std::cout << "TEST POWERUP shooting " << player->GetName() <<'\n';
	}

	inline void OnTakeDamage(Entity* player, float damage, Entity* source) 
	{
		std::cout << "TEST POWERUP take damage " << player->GetName() << " taking "
			<< damage << " damage from " << source->GetName() << "\n";
	}
};

