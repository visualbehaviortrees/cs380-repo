//	File name:		PowerupSpawner.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include <string>
#include "../Component.hpp"
#include "Components.h"
#include "../Systems/Gameplay/PowerupHandler.hpp"
#include "../Engine.hpp"



