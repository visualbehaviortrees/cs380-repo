//	File name:		PowerupSpawner.h
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include <string>
#include "../Component.hpp"
#include "Powerup.h"

// This is for items that give a powerup effect when picked up
struct PowerupSpawner : public Component
{
public:
  PowerupSpawner():Component(CMP(PowerupSpawner)){}
  ~PowerupSpawner(){}

  static Component* CreateComponent(void)
	{ return new PowerupSpawner(); }

  PowerupEffect* effect;
};


