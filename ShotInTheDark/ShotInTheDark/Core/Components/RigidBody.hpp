//	File name:		RigidBody.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../Component.hpp"
#include "../Libraries/Math/Math2D.hpp"
#include <vector>
class Entity;

struct CollisionInfo
{
	CollisionInfo(Entity* e, Math::Vector2D &vec) : object(e), mtv(vec), resolved(false) {}
	Entity *object;
	Math::Vector2D mtv;
	bool resolved;
	float intersectionTime;
	Math::Vector2D reflection;
};


struct RigidBody : public Component
{
public:
	RigidBody() :Component(CMP(RigidBody)),
		vel(0, 0),
		density(1),
		area(1),
		rotationLocked(false),
		ghost(false){}
	RigidBody(
		Math::Vector2D Vel,
		bool IsStatic,
		//bool Kinematic,
		bool RotationLocked,
		float Density,
		float Area,
		bool Ghost)
		:Component(CMP(RigidBody)),
		vel(Vel),
		isStatic(IsStatic),
		// kinematic(Kinematic),
		rotationLocked(RotationLocked),
		density(Density),
		area(Area),
		ghost(Ghost) {}


	void Init(){}
	void Free(){}
	~RigidBody(){}

	static Component *CreateComponent() { return new RigidBody(); }

	// Member Data
	std::vector<CollisionInfo> collisionList;
	Math::Vector2D vel;
	Math::Vector2D direction;
	Math::Point2D StartPoint;
	float speed;
	float density;
	float area;
	bool ghost;
	bool isStatic;
	// bool kinematic;
	bool rotationLocked;

};
