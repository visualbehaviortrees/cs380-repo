//	File name:		Score.h
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../Component.hpp"
#include <string>

struct Score : public Component
{
public:
  Score():Component(CMP(Score)),score(0){}
  void Init(){}
  void Free(){}
  ~Score(){}

  static Component *CreateComponent() { return new Score(); }

  unsigned score;

};
