//	File name:		Team.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.
#include "stdafx.h"
#include "Script.hpp"

void Script::update(float dt)
{
	unsigned index = 0;
	for (auto& script : _scripts)
	{
		if ( (script->Mask() & script->Self()->Mask() ) == script->Mask())
		{
			ScriptStatus ss = script->update(dt);

			switch (ss)
			{
			case SS_Fail:
				//
				if (script->OnFail())
					RemoveScript(index);
				break;
			case SS_Running:
				continue;
				break;
			case SS_Succeed:
				//
				if (script->OnSuccess())
					RemoveScript(index);
				break;
			default:
				break;
			}
		}
		else
		{
			// Script requires components not attached to the entity.
			RemoveScript(index);
		}

		++index;
	}
	ClearOldScripts();
}

void Script::ClearOldScripts()
{
	mask scriptMask{ 0 };
	for (auto index : _removeScripts)
	{
		scriptMask |= _scripts[index]->Mask();

		delete _scripts[index];
		_scripts[index] = nullptr;
	}
	_scripts.erase(std::remove_if(_scripts.begin(), 
								  _scripts.end(), 
								  [](IScript *script){ return script == nullptr; }
								 )
				  );
	_removeScripts.clear();

	RemoveUnusedMaskBits(scriptMask);
}

void Script::RemoveUnusedMaskBits(const mask bits)
{
	mask newMask{ 0 };
	for (auto script : _scripts)
	{
		newMask |= (bits & script->Mask());
	}
	newMask ^= bits;
	_scriptMask -= newMask;
}
