//	File name:		Team.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once
#include <memory>
#include <vector>
#include "../Component.hpp"
class Entity;
enum ScriptStatus
{
	SS_Fail,
	SS_Running,
	SS_Succeed,
	SS_Err_Missing_Components
};

class IScript
{
public:
	virtual ScriptStatus update(float dt) = 0;
	virtual ~IScript() {}
	// these two functions should return true if the script is done and should be removed.
	virtual bool OnSuccess() { return true; }
	virtual bool OnFail() { return true; }

	IScript(Entity *self) : _self(self) {}
	inline mask Mask() { return _requirements; }
	const Entity * Self() { return _self; }
protected:
	void RegisterRequirement(MComponent requirement) { _requirements |= requirement; }
	const Entity * _self;
private:
	// bitwise and with each other IScript on an entity, then OR all the results together and XOR them to remove these requirements from it.
	// Alternatively, OR all the requirements for each script every time a check is done.
	mask _requirements;
};

struct Script : public Component
{
public:
  Script() : Component(CMP(Script)), _scriptMask(0) {}
  void Init(){}
  void Free(){}
  ~Script(){}
  void update(float dt);
  static Component *CreateComponent() { return new Script(); }

  void addScript(IScript * script){ _scripts.push_back(script); }
private:
	std::vector<IScript *> _scripts;
	std::vector<unsigned> _removeScripts;
	void RemoveScript(unsigned index) { _removeScripts.push_back(index); }
	void ClearOldScripts();
	void RemoveUnusedMaskBits(mask bits);
	mask _scriptMask;
};
