// Author : Batman

#include <stdafx.h>
#include "../Core/Systems/Graphics/SpriteBatcher.h"

Sprite::Sprite(std::string tex, bool noparticles) :Component(CMP(Sprite))
{
  mpTextureData = RESOURCES->Get_TextureData(tex);
  
  std::string normalTexture = "";

  if (tex != "")
  {
    normalTexture = tex;
    normalTexture.pop_back();
    normalTexture.pop_back();
    normalTexture.pop_back();
    normalTexture.pop_back();
    normalTexture += "_Normal.png";
  }

  mpNormalTextureData = RESOURCES->Get_TextureData(normalTexture);
  NoParticles = noparticles;
}

void Sprite::Init()
{
  SpriteBatcher::AddSprite (this);
}

void Sprite::Free()
{

}

Sprite::~Sprite()
{
  SpriteBatcher::RemoveSprite (this);
}

void Sprite::Draw(VBO* matrixVbo, VBO* colVbo, VBO* texVbo)
{
  if (matrixVbo != nullptr)
  {
    matrixVbo->AddData(&m_MVP.matrix4[0][0], sizeof m_MVP / sizeof m_MVP.matrix4[0][0]);
    colVbo->AddData(glm::value_ptr(this->Color), sizeof Color / sizeof Color[0]);
    Bounds b = mpTextureData->bounds;

    texVbo->AddData(&b.Min.x, sizeof this->mpTextureData->bounds / sizeof this->mpTextureData->bounds.Min.x);
  }
}

void Sprite::ChangeTexture(std::string name)
{
  mpTextureData = RESOURCES->Get_TextureData(name);
  std::string normalTexture = "";

  if (name != "")
  {
    normalTexture = name;
    normalTexture.pop_back();
    normalTexture.pop_back();
    normalTexture.pop_back();
    normalTexture.pop_back();
    normalTexture += "_Normal.png";
  }

  mpNormalTextureData = RESOURCES->Get_TextureData(normalTexture);
  SpriteBatcher::ChangeSpriteTexture(this, mpTextureData->mpAtlas);
}