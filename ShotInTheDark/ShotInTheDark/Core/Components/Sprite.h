 //	File name:		Sprite.h
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../Component.hpp"
#include "../Systems/Graphics/Texture.hpp"
#include "../Systems/Graphics/Graphics.hpp"
struct Sprite : public Component
{
public:
  Sprite(std::string tex = "", bool noparticles = false);
  void Init ();
  void Free();

  virtual ~Sprite();
  void Draw(VBO* matrixVbo, VBO* colVbo, VBO* texVbo);
  void ChangeTexture(std::string tex);
  static Component *CreateComponent() { return new Sprite(); }

  TextureData* mpTextureData;
  TextureData* mpNormalTextureData;
  bool flipX;
  bool flipY;
  bool animationActive;
  float animationSpeed;
  unsigned startFrame;
  unsigned currentFrame;
  bool NoParticles;
  Matrix4 m_MVP;

  glm::vec4 Color = glm::vec4 (1,1,1,1);

  inline static bool SortFunction(Sprite* s1, Sprite* s2)
  {
    if (s1->mpNormalTextureData == nullptr || s2->mpNormalTextureData == nullptr)
    {
      return s1->mpTextureData->mpAtlas->Get_ID() < s2->mpTextureData->mpAtlas->Get_ID();
    }
    else
    {
      return s1->mpTextureData->mpAtlas->Get_ID() < s2->mpTextureData->mpAtlas->Get_ID() &&
        s1->mpNormalTextureData->mpAtlas->Get_ID() < s2->mpNormalTextureData->mpAtlas->Get_ID();
    }
  }

};
