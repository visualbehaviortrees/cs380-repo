#include "stdafx.h"
#include "SpriteText.h"
#include "../Core/Systems/Graphics/Camera.hpp"
#include "../Core/Libraries/Math/Math2D.hpp"

void SpriteText::Draw(Point2D& Pos)
{
  // Set the font size and render a small text.
  glUseProgram(0);

  Window* GS_Window = GETSYS(Graphics)->GetCurrentWindow();
  font.FaceSize(static_cast<unsigned int>(static_cast<float>(fontSize) * (static_cast<float>(GS_Window->WindowHeight) / static_cast<float>(GS_Window->Desktop->height))
                                                                       * (static_cast<float>(STARTING_CAM_SIZE / GS_Window->main_Cam->size))));
  glPushAttrib(GL_ALL_ATTRIB_BITS);
  glColor4fv(&color.r);
  glRasterPos3f(Pos.x, Pos.y, Pos.z);
  font.Render(text.c_str());
  glPopAttrib();
}