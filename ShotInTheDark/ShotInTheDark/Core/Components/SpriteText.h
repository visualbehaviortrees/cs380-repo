//	File name:		SpriteText.h
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../Component.hpp"
#include "../Systems/Graphics/Alignment.h"
#include "../../FTGL/ftgl.h"
#include <string>
#include <sstream>

struct SpriteText : public Component
{
public:
  SpriteText(std::string name = "Default", unsigned Fontsize = 73, Vector2D Offset = Vector2D(0.0f,0.0f)) :Component(CMP(SpriteText))
  {
    text = name;
    fontSize = Fontsize;
    offset = Offset;
  }
  void Init() {};
  void Free(){}
  ~SpriteText(){}

  static Component *CreateComponent() { return new SpriteText(); }

  //Pos is in Window Coordinates
  void Draw(Point2D& Pos);

  Alignment boxPos;
  Alignment hAlign;
  Alignment vAlign;
  Vector2D offset;

  unsigned fontSize;
  std::string text;
  glm::vec4 color = glm::vec4 (1.0f, 1.0f, 1.0f, 1.0f);
  std::stringstream aibehavior;
  // Resource manager not yet implemented
  //std::string fontName;
  
private:
	FTGLPixmapFont font = FTGLPixmapFont("Resources/BuxtonSketch.ttf");
};