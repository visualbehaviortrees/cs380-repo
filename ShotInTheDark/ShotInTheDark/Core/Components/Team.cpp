//	File name:		Team.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.
#include "stdafx.h"
#include "Team.hpp"

bool Team::pvp_enabled = false;

bool Team::FriendlyTeams(Entity* A, Entity* B)
{
  Teams a, b; 
  if (A->HasComponent(EC_Team))
    a = A->GET_COMPONENT(Team)->teamID;
  else
    a = Team_NONE;

  if (B->HasComponent(EC_Team))
    b = B->GET_COMPONENT(Team)->teamID;
  else
    b = Team_NONE;

  if (a == Team_ALL || b == Team_ALL)
	  return true;
  if (a == Team_NONE || b == Team_NONE)
    return false;
	
  // check for special friends
  // We must know at this point that A and B have team components

  // Check A's special friends
  for (Teams special_friend : A->GET_COMPONENT(Team)->specialFriends)
	  if (special_friend == b)
		  return true;

  // Check B's special friends
  for (Teams special_friend : B->GET_COMPONENT(Team)->specialFriends)
	  if (special_friend == a)
		  return true;

  // We need to assume player teams are the same
  // If we want players on the same team, put them all to player 1
	if (!pvp_enabled)
	{
		// Clamp all teams between player 1 and player max
		if (a >= Team_Player_1 && a < TEAM_PLAYER_MAX)
			a = Team_Player_1;
		if (b >= Team_Player_1 && b < TEAM_PLAYER_MAX)
			b = Team_Player_1;
	}

	return a == b;
}