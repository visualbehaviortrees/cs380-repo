//	File name:		Team.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../Component.hpp"

enum Teams
{
  Team_ALL = 0,
  Team_NONE,
  Team_Environment,
  Team_Enemy,
  Team_Player_1,
  Team_Player_2,
  Team_Player_3,
  Team_Player_4,
  TEAM_PLAYER_MAX
};

struct Team : public Component
{
public:
	Team():Component(CMP(Team)),teamID(Team_NONE){}
  Team(Teams team):Component(CMP(Team)),teamID(team){}
	void Init(){}
	void Free(){}
	~Team(){}

  static Component *CreateComponent() { return new Team(); }

  std::vector<Teams> specialFriends;// Add a team here to be friends with it
  static bool FriendlyTeams(Entity*A, Entity*B);
  Teams teamID;
  static bool pvp_enabled;
};
