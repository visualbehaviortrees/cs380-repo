//	File name:		Transform.hpp
//	Project name:	Shot In The Dark
//	Author(s):		James Schmidt
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../Component.hpp"
#include "../Systems/Physics/Shape.hpp"
#include "../Libraries/Math/Math2D.hpp"

struct Transform : public Component
{
public:
	Transform():Component(CMP(Transform)),rotation(0),scale(1,1),Position(0,0){}
	void Init(){}
	void Free(){}
	~Transform(){}

  static Component *CreateComponent() { return new Transform(); }

  Math::Vector2D scale;
  float rotation;
  Math::Point2D Position;

  // Change this if needed.
  Math::Affine transform;

};
