//	File name:		Turret.h
//	Project name:	Shot In The Dark
//	Author(s):		Connor Drose
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../Component.hpp"

struct Turret : public Component
{
public:
  Turret():Component(CMP(Turret)), tracking(false), cooldownTime(0), trackingTime(0), timeDead(0){}
  void Init(){}
  void Free(){}
  ~Turret(){}

  static Component *CreateComponent() { return new Turret(); }

  float cooldownTime;
  float trackingTime;
  bool tracking;
  float timeDead;

  // TEMPORARY DEBUGGING   JUST FOR JSON STUFF
  float timeLastShot;
  float timeStartTracking;


};
