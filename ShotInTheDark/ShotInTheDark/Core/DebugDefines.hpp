//	File name:		DebugDefines.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

// Debugging and reminder defines.
// These will give your message with the line number and file that it occurred on.
#define Stringify( N )     #N
#define MakeString( M, N ) M(N)
#define __LINENUM__  MakeString( Stringify, __LINE__ )
#define Reminder  __FILE__ "(" __LINENUM__ "): Reminder: "
#define Warning  "Warning in " __FILE__ "(" __LINENUM__ "): "

//Usage:
// #pragma message(Warning "Message goes here.")
// #pragma message(Reminder "Message goes here.")

