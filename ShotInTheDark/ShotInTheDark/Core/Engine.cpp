//	File name:		Engine.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

// This is a silly little sample thing that shows you how the engine works.
// Allan Deutsch
// created 10/11/14
// Changelog:
// 1/15/15 - Bug fixes related to multiple system containers and the GSM.

#include "stdafx.h"
#include "Libraries\AntTweakBar\AntTweakBar.hpp"
#include <fstream>
#include <thread>

#include "Systems/Test/TestSystem.hpp"
#include "Systems/Input/InputSystem.hpp"
#include "Systems/Physics/PhysicsSystem.hpp"
#include "Systems/Gameplay/EnemyFactory.h"
#include "Systems/Graphics/Graphics.hpp"
#include "Systems/Physics/Resolution.hpp"
#include "Systems/Physics/Broadphase.hpp"
#include "Systems/Audio/AudioSystem.hpp"
#include "Systems/PlayerHandler/PlayerHandler.hpp"
//#include "Systems/Networking/NetworkSystem.h"
#include "Systems/Particles/ParticleSystem.h"
#include "Systems/AI/GridMap.hpp"
#include "Systems/Gameplay/PowerupHandler.hpp"
#include "Systems/PauseSystem/ObjectToWorld.hpp"
#include "Systems/Graphics/ResourceManager.h"
#include "Systems/HUD/HUD.hpp"
#include "Systems/Gameplay/DungeonGameplay.h"
#include "Systems/LevelEditor/Level_Creator.hpp"
#include "Gamestates\MenuGamestate.h"
#include "Systems/DebugTools/DebugTools.hpp"
#include "Engine.hpp"
#include "Systems/Gameplay/ScriptSystem.hpp"

#include "Libraries/ADLib/ScopeTimer.hpp"


#define ROOM_EDITOR_ON 0
#define GAME_ON 1



Engine *ENGINE = NULL;

extern std::string Level;

bool Minimized = false;
bool credit_screen = false;
bool pause_screen = false;
bool quit_screen = false;
bool quit = false;

Engine::Engine(): _updates(0)
{
	// Don't make extra engines. 50 lashings for you!
	assert(ENGINE == NULL);
	if (ENGINE == NULL)
	{
		//  ENGINE = this;
	}
	else
	{
		std::cout << "Catastrophic Error: multiple Engines created.\n";
	}
}

void Engine::AddSystem(System *system, systype type)
{
  pushSystems.push_back(std::pair<System *, systype>(system, type));
}

void Engine::pushNewSystems(void)
{
  while(pushSystems.size())
  {
    auto it = pushSystems.back();
    for(auto sys = _systems.begin(); sys != _systems.end(); ++sys)
    {
      if((*sys)->_type == it.second)
      {
        _systems.insert(sys, it.first);
        break;
      }
    }
    pushSystems.pop_back();
  }
}

void Engine::AddSystem(System *system, sysRuntime srt)
{
  if(srt == srt_any)
    _systems.push_back(system);
  else if(srt == srt_pre)
    _preSystems.push_back(system);
  else
	  _postSystems.push_back(system);
}

void Engine::Init()
{
  if(!glfwInit())
  {
    throw "GLFW failed to initialize";
  }

	OM = new ObjectManager();
	Factory = new ObjectFactory();


		// Add systems here:
	ENGINE->AddSystem(new InputSystem(), srt_pre);
	ENGINE->AddSystem(new ObjectToWorldSystem(), srt_pre);
	ENGINE->AddSystem(new PlayerHandler());
	ENGINE->AddSystem(new PhysicsSystem());
	ENGINE->AddSystem(new TestSystem()); // TESTS FOR COLLISIONS
	ENGINE->AddSystem(new HUDsystem());
	ENGINE->AddSystem(new PowerupHandler());
	ENGINE->AddSystem(new ResolutionSystem());
	ENGINE->AddSystem(new AudioSystem());
	ENGINE->AddSystem(new GridMap());
	ENGINE->AddSystem(new ScriptSystem());
	ENGINE->AddSystem(new ObjectToWorldSystem(), srt_post);
	ENGINE->AddSystem(new Graphics(), srt_post);
	ENGINE->AddSystem(new DebugTools(), srt_post);
	ENGINE->AddSystem(new EnemyFactory());

	// AddSystem() calls above this line.
	prevTime = (float)glfwGetTime();
	timeElapsed = 1.0f / 60.0f;
  for (System *it : _postSystems)
  {
    if((it)->_type == sys_Graphics)
    {
      (*it)();
      ENGINE->OM->AddRegistry(new OMRegistryEntry(it));
	  }
  }
  for (System *it : _postSystems)
  {
    if(it->_type != sys_Graphics)
    {
      (*it)();
      ENGINE->OM->AddRegistry(new OMRegistryEntry(it));
	  }
  }
  for (System *it : _preSystems)
  {
    (*it)();
    ENGINE->OM->AddRegistry(new OMRegistryEntry(it));
  }
  for (System *it : _systems)
  {
    (*it)();
    ENGINE->OM->AddRegistry(new OMRegistryEntry(it));
  }
  InitMyAntweakBar();

#if GAME_ON
  // Add initial gamestate here, below this line.
  DungeonGameplay * mg = new DungeonGameplay();
  ENGINE->pushGamestate(mg);

  ENGINE->OM->AddRegistry(new OMRegistryEntry(mg));
  // Add initial gamestate above this line.
#endif

#if ROOM_EDITOR_ON

  //ENGINE->popGamestate();
  EditorSystem* es = new EditorSystem(15, 9);
  ENGINE->pushGamestate(es);
#endif



}

void Engine::tickTimedLogic(float dt)
{
  static std::vector<ADL::ITimedLogic *> expired;

  for(auto logic : _TimedLogicObjects)
  {
    bool done = logic->tick(dt);;
    if (done)
      expired.push_back(logic);
  }

  for(auto logic : expired)
  {
    for (auto obj = _TimedLogicObjects.begin(); obj != _TimedLogicObjects.end(); ++obj)
      if(*obj == logic)
      {
        *obj = _TimedLogicObjects.back();
        _TimedLogicObjects.pop_back();
      }
    delete logic;
  }

  expired.clear();
}

float Engine::FrameTime(void)
{
	float NewTime = (float)glfwGetTime();
	timeElapsed = (NewTime - prevTime);
	prevTime = NewTime;
  //std::cout << timeElapsed << std::endl;
	return timeElapsed;
}

void Engine::Update(void)
{
  ADLib::ScopeTimer timer(&timeElapsed);
	//ENGINE->FrameTime();
  try
  {
	  ENGINE->Update(timeElapsed);
  }
  catch (...)
  {
  }

}

void Engine::Update(float dt)
{
  ++_updates;
  DoGSStuff();
  OM->PurgeObjects();
  OM->UpdateRegistry();

  float realdt = dt;
  // CHEAT LOW-FPS TRICKERY
  if (dt > 1.f / 30.f)
#ifdef _DEBUG
	  dt = 1.f / 30.f;
#else
    dt = 1.f / 60.f;
#endif

#if PRETHREADING
  std::vector<std::thread> prethreads;

  for (auto it : _preSystems)
  {
    prethreads.push_back( std::thread( [it, dt]{(*it)(dt); } ) );
    //(*it)(dt);
  }
  for (auto &it : prethreads)
    if (it.joinable())
      it.join();
#endif

  if (CheckKeyTriggered(KEY_F2, MOD_CTRL))
  {
    SendMsg(nullptr, nullptr, MSG_Screenshot_Mode, sys_Graphics);
    _screenshotMode = !_screenshotMode;
  }
  

  for (auto &it : _preSystems)
  {
    if (it->_type == sys_Input || !_screenshotMode)
#if BENCHMARKING
      (*it)(dt);
#endif
#if BENCHMARKING == 0
    it->Update(dt);
#endif
  }

  autoplay(dt);

  if (_screenshotMode && CheckKeyTriggered(KEY_F2))
  {
    _stepFrame = true;
    SendMsg(nullptr, nullptr, MSG_Screenshot_Mode, sys_Graphics);
    dt = 1.f / 60.f;
  }
  else if (_screenshotMode)
  {
    if(_stepFrame)
      SendMsg(nullptr, nullptr, MSG_Screenshot_Mode, sys_Graphics);
    _stepFrame = false;
  }

  if (!_screenshotMode || _stepFrame)
  {
    if (_gamestates.size())
    {
      Gamestate * gs = _gamestates.top();
      // run the pre-gamestate systems
      for (auto it : _systems)
      {
        if (FlagSet(gs->PreSystems(), it->_type))
        {
#if BENCHMARKING
          (*it)(dt);
#endif
#if BENCHMARKING == 0
          it->Update(dt);
#endif
        }
      } // gamestate pre-systems

      // Update the gamestate logic

#if BENCHMARKING
      (*gs)(dt);
#endif
#if BENCHMARKING == 0
      gs->Update(dt);
#endif
      // run the post-gamestate systems
      for (auto it : _systems)
      {
        if (FlagSet(gs->PostSystems(), it->_type))
        {
#if BENCHMARKING
          (*it)(dt);
#endif
#if BENCHMARKING == 0
          it->Update(dt);
#endif
        }
      } //post systems
    } // gamestate updates
  }

  tickTimedLogic(dt);

  for(auto it : _postSystems)
  {
#if BENCHMARKING
    (*it)(dt);
#endif
#if BENCHMARKING == 0
    it->Update(dt);
#endif
  }
#if BENCHMARKING
  if (_updates % 100000 == 0)
  {
    if (loggingThread.joinable())
      loggingThread.join();
    loggingThread = std::thread(&Engine::logPerformance, std::ref(*this));
  }
#endif
  if (CheckKeyTriggered(KEY_F10))
  {
    if(!Minimized)
    {
      Graphics* GS = (Graphics*)ENGINE->GetSystem(sys_Graphics);
      AudioSystem* AS = (AudioSystem*)ENGINE->GetSystem(sys_AudioSystem);

      if(AS->IsMusicPlaying())
      {
        AS->StopMusic(false);
		    AS->Update(0.0f); // fake an update
      }

      Minimized = !Minimized;
      glfwIconifyWindow(GS->GetCurrentWindow()->window);

      // Pause();
    }

  }

  if(Minimized)
  {
    Graphics* GS = (Graphics*)ENGINE->GetSystem(sys_Graphics);


    if (glfwGetWindowAttrib(GS->GetCurrentWindow()->window, GLFW_FOCUSED))
    {
      AudioSystem* AS = (AudioSystem*)ENGINE->GetSystem(sys_AudioSystem);

      Minimized = !Minimized;
      // Unpause();
      if(AS->IsMusicPlaying())
      {
        // AS->PlayMusic();
      }
    }
  }

  glfwSwapBuffers(GETSYS(Graphics)->GetCurrentWindow()->window);

  pushNewSystems();
  pushSystems.clear();
}

bool Engine::sysActive(systype type)
{
  if (_gamestates.empty())
    return false;
  if (FlagSet(_gamestates.top()->PreSystems(), type)
    || FlagSet(_gamestates.top()->PostSystems(), type)
    || FlagSet(_gamestates.top()->_type, type))
    return true;
  return false;
}

void Engine::MainLoop(void)
{
	_running = true;
	while (_running)
	{
		FrameTime();
		Update(timeElapsed);
  }

  // only log if something happened, not on super quick runs which will be inaccurate.
  if (GetTotalTime() > 60.0f)
    logPerformance();

  for(auto& it: _systems)
  {
      it->Shutdown();
  }
}

System* Engine::GetSystem(systype type)
{
  for(auto it : _systems)
  {
    if(it->_type == type)
    {
      return it;
    }
  }  
  for(auto it : _preSystems)
  {
    if(it->_type == type)
    {
      return it;
    }
  }
  for(auto it : _postSystems)
  {
    if(it->_type == type)
    {
      return it;
    }
  }
  return NULL;
}



void Engine::PopulateOMEntries()
{
  for (System *it : _postSystems)
  {
    ENGINE->OM->AddRegistry(new OMRegistryEntry(it));
  }
  for (System *it : _preSystems)
  {
    ENGINE->OM->AddRegistry(new OMRegistryEntry(it));
  }
  for (System *it : _systems)
  {
    if (FlagSet(ENGINE->getCurrentGamestate()->PreSystems()
             |  ENGINE->getCurrentGamestate()->PostSystems(),
             it->_type))
      ENGINE->OM->AddRegistry(new OMRegistryEntry(it));
  }
  ENGINE->OM->AddRegistry(new OMRegistryEntry(ENGINE->getCurrentGamestate()));
}
void Engine::pushGamestate(Gamestate *gs)
{
  _GSOps.push(gs);
}

void Engine::popGamestate(void)
{
  _GSOps.push(nullptr);
}

void Engine::PushGS(Gamestate *gs)
{
  _gamestates.push(gs);
  OM = gs->OM;
  _gamestates.top()->Init();
  // DO STUFF HERE WITH OM GS STUFF
  ENGINE->PopulateOMEntries();
}
void Engine::PopGS()
{
  systype st = _gamestates.top()->_type;
  _gamestates.top()->Shutdown();
  delete _gamestates.top();
  _gamestates.pop();
  ENGINE->OM = _gamestates.top()->OM;
  ENGINE->OM->_shouldUpdateRegistry = true;
  _gamestates.top()->PoppedTo(st);
}
void Engine::DoGSStuff()
{
  while (!_GSOps.empty())
  {
    if (_GSOps.front() == nullptr)
    {
      PopGS();
      _GSOps.pop();
    }
    else
    {
      PushGS(_GSOps.front());
      _GSOps.pop();
    }
  }
}

void Engine::logPerformance(void)
{
#if BENCHMARKING
  std::ofstream benchmarkLog("EnginePerformance.benchmark", std::ofstream::out | std::ofstream::app);

  benchmarkLog << std::endl;
  benchmarkLog << "/************************************/" << std::endl;
  benchmarkLog << "/***********/ Engine Log /***********/" << std::endl;
  benchmarkLog << "/************************************/" << std::endl;
  benchmarkLog.precision(15);

  float EngineTime = ENGINE->GetTotalTime();
  float systemTime = 0.0f;
//#endif
  for(auto& it: _systems)
  {
    systemTime += it->perfData.logData(benchmarkLog);
  }
  for(auto& it: _preSystems)
  {
    systemTime += it->perfData.logData(benchmarkLog);
  }
  for(auto& it: _postSystems)
  {
    systemTime += it->perfData.logData(benchmarkLog);
  }
  // Gamestate time
  systemTime += ENGINE->getCurrentGamestate()->perfData.logData(benchmarkLog);
//#if BENCHMARKING
  benchmarkLog << "Engine stats:" << std::endl;
  benchmarkLog << "Total time running:  " << ENGINE->GetTotalTime()                       << "s (" << static_cast<int>(ENGINE->GetTotalTime()) / 3600 << "h "
                                                                                          << (static_cast<int>(ENGINE->GetTotalTime()) / 60) % 60 << "m " 
                                                                                          << (static_cast<int>(ENGINE->GetTotalTime()) % 60) << "s)" <<  std::endl;
  benchmarkLog << "Engine time:         " << EngineTime - systemTime                      << std::endl;
  benchmarkLog << "Engine time percent: " << (EngineTime - systemTime) / EngineTime * 100 << std::endl;
  benchmarkLog << "Registry rebuilds:   " << ENGINE->OM->_rebuilds                        << std::endl;
  benchmarkLog << "Rebuilds per frame:  " << (float) ENGINE->OM->_rebuilds / _updates             << std::endl;
  benchmarkLog << "System time (total): " << systemTime                                   << std::endl;
  benchmarkLog << "System time (avg):   " << systemTime / _systems.size()                 << std::endl;
  benchmarkLog << "Average framerate:   " << _updates / ENGINE->GetTotalTime()            << std::endl;
  benchmarkLog.close();
#endif
}


void Engine::autoplay(float dt)
{
  static bool active = false;
  static float lx, ly, rx, ry;
  static float cooldown = 0.25f;
  if (CheckKeyTriggered(KEY_A, MOD_CTRL))
  {
    active = !active;
  }
  if (!active)
    return;

  if (cooldown <= 0.f)
  {
    lx = ADLib::randFloat() * 2.f - 1.f;
    ly = ADLib::randFloat() * 2.f - 1.f;
    cooldown = .25f;
  }
  else
    cooldown -= dt;

  rx = ADLib::randFloat() * 2.f - 1.f;
  ry = ADLib::randFloat() * 2.f - 1.f;

  JSAxisSetValue(JOYSTICK_1, LEFT_X_AXIS, lx);
  JSAxisSetValue(JOYSTICK_1, LEFT_Y_AXIS, ly);
  JSAxisSetValue(JOYSTICK_1, RIGHT_X_AXIS, rx);
  JSAxisSetValue(JOYSTICK_1, RIGHT_Y_AXIS, ry);

}

void Engine::SendMsg(Entity *e1, Entity *e2, message msg, systems recipients)
{
  if(recipients != -1)
  {
    for (auto it : _preSystems)
    {
      if ( FlagSet(it->_type, recipients ) )
        it->SendMsg( e1, e2, msg );
    }

    for (auto it : _systems)
    {
      if ( FlagSet( it->_type, recipients ) && sysActive( it->_type ) )
      {
        it->SendMsg(e1, e2, msg);
      }
    }

    for (auto it : _postSystems)
    {
      if (FlagSet(it->_type, recipients))
        it->SendMsg(e1, e2, msg);
    }

    // Message the gamestate if it's a recipient.
    if( FlagSet(getCurrentGamestate()->_type, recipients))
      getCurrentGamestate()->SendMsg(e1, e2, msg);
  }
  else
  {
    for(auto it : _preSystems)
      it->SendMsg(e1, e2, msg);

    for(auto it : _systems)
      if( sysActive(it->_type) )
        it->SendMsg(e1, e2, msg);

    for(auto it: _postSystems)
      it->SendMsg(e1, e2, msg);

    // Message the gamestate too.
    if(!_gamestates.empty())
      getCurrentGamestate()->SendMsg(e1, e2, msg);
  }
}
