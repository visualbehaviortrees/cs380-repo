//	File name:		Engine.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

// Basic Engine has the interface for a better engine but is actually a shitty engine. Cool huh?
// I basically just pooped this out in a couple hours so you guys have something functional to work with.
// Sorry I'm late but it's 6am and I decided way to late to do this, that's on me I'm sorry.
// Also I don't have the implementation side for systems, but a shitty slow one is really easy.
// just check Entity.HasComponent for the stuff you want, if it passes for everything it's valid.
// I'll write some less shitty stuff when I get to school.

//Author: Allan Deutsch
// Created: 10/11/14
//          12/XX/14 - added crappy gamestate management, pausing
//           1/03/15 - Added performance logging
//           1/11/15 - Initial groundwork for stack-based gamestate management using system-states.
//           1/12/15 - Secondary pass on stack-based gamestate management. 
//                       Update() is now altered if a gamestate exists. 
//                       Gamestates can be added and removed.
//           2/06/15 - Added macro to get systems by derived type
//           2/23/15 - Gamestate push/pop operations now happen at the start of frames instead of immediately.

# pragma once

#define BENCHMARKING 0

#include <vector>  // Duh
#include <stack> // gotta stack up dem gamestates yo
#include <queue> // FIFO LIKE A BAU5
#include <cassert> // Duh
#include <iostream> // Duh
#include <thread> // fo threads
#include <memory>
//#include <time.h>  //clock, clock_t, CLOCKS_PER_SECOND
#include "Component.hpp"
#include "DebugDefines.hpp"
#include "Entity.hpp"
#include "ObjectManager.hpp"
#include "System.hpp"
#include "ObjectFactory.h"
#include "Libraries/ADLib/ITimedLogic.h"
/**
 * @brief The actual engine class. There should only be one...
 * @details This is the core Engine. Systems should all be added before initialization.
 * The Engine has three modes of operation:
 * Full autonomy - Call MainLoop() and the engine does everything else for you. No external input.
 * Partial autonomy - Call Update() and it manages time itself. No framerate cap is allowed.
 * Minimal autonomy - call Update(dt) and all systems will be updated with the provided dt.
 */
class Engine
{
public:
  /**
   * @brief Creates the one and only Engine!
   * @details Creates the Engine and sets the global ENGINE pointer to itself.
   * Making another Engine while one is running causes a catastrophic failure.
   */
  Engine();
  /**
   * @brief Kills EVERYTHING!
   * @details This should only be called at program termination. It will call the destructor for everything.
   */
   //!!!! NOT IMPLEMENTED YET
   // #pragma message(Warning "~Engine() not implemented. Massive memory leak.")
   // #pragma message(Reminder "Allan needs to implement ~Engine()!")
  //~Engine();

  /**
   * @brief Adds a system to the Engine.
   * @details Adds a System to the Engine. Systems once all systems are added, their Init() function is called.
   * Every time the Engine Update() function is called, each Systems Update(dt) is called with the frame time.
   *
   * @param system Pointer to the System to add to the Engine.
   * Typically should just be AddSystem(new MySystem());
   */
  void AddSystem(System *system, sysRuntime = srt_any);
  void AddSystem(System *system, systype insertBefore);
  /**
   * @brief Initializes the Engine. Should only be called AFTER all systems have been added.
   * @details Sets the initial frametime, starts the Engine, and Initializes all the Systems.
   * This should not be called until all systems have been added, else they must be initialized manually.
   */
  void Init();
  /**
   * @brief Calls Update() repeatedly.
   * @details Calls Update repeatedly. This can be exited only by setting _running to false.
   */
  void MainLoop();
  /**
   * @brief Updates using the Engines built in frame time. This is uncapped and runs as fast as possible.
   * @details This function updates every System in the Engine using the Engines own uncapped frame time.
   * This should be used externally if a specific framerate is not important aand
   *   no significant time passes outside of the Engines operations.
   */
  void Update();
  /**
   * @brief Updates using a provided frame time.
   * @details Updates using an externally provided dt. This should be used if substantial time passes outside the Engine
   *   or if enforcing a specific framerate is necessary.
   *
   * @param dt The time passed since the last time the function was called.
   */
  void Update(float dt);

  void SendMsg(Entity *, Entity *, message, systems = -1);


  float GetTotalTime(void){ return prevTime; }

  float GetTime(void){return timeElapsed; }
  
 // unsigned GetElapsedTimeBlocks(void) { return ((int)prevTime / 5);}
  

#define GETSYS( type ) \
  ((type *)(ENGINE->GetSystem(sys_##type)))

  System* GetSystem(systype type);

  bool sysActive(systype type);

  Gamestate *getCurrentGamestate(void) { return _gamestates.top(); }
  void pushGamestate(Gamestate *gs);

  void popGamestate(void);

  
  /**
   * @brief Pointer to the ObjectManager being used by the Engine.
   */
  ObjectManager* OM;
  /**
   * @brief Pointer to the factory used to compose objects.
   */
  ObjectFactory *Factory;
  bool _running;

  systype GetActiveSystem() { return _activeSys; }
  void SetActiveSystem(systype s) { _activeSys = s; }

  void AddTimedLogicObject(ADL::ITimedLogic *obj) { _TimedLogicObjects.push_back( obj); }

private:
  void tickTimedLogic(float dt);
  systype _activeSys;
  float FrameTime(void);
  void pushNewSystems(void);
  std::vector<std::pair<System *, systype>> pushSystems;
  std::stack<Gamestate *> _gamestates;
  
  std::vector<System *> _systems;
  std::vector<System *> _preSystems;
  std::vector<System *> _postSystems;

  void PopulateOMEntries();
  void DoGSStuff();
  void PushGS(Gamestate *);
  void PopGS();
  std::queue < Gamestate * > _GSOps;
  
  // For tech demo-y things
  bool _screenshotMode = false;
  bool _stepFrame = false;

  // performance logging info
  std::thread loggingThread;
  void logPerformance(void);
  float prevTime;
  float timeElapsed;
  unsigned _updates;

  void autoplay(float dt);

  std::vector<ADL::ITimedLogic *> _TimedLogicObjects;
};

extern Engine *ENGINE;