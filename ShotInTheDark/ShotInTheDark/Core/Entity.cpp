//	File name:		Entity.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "Entity.hpp"
#include <cassert> // Duh
#include "Libraries/ADLib/ADLib.hpp"


bool Entity::HasComponent(mask components)
{
	return FlagSet(components, _mask);
}

bool Entity::RemoveComponent(Component *component)
{
	// if you are trying to delete another Entity's component, no.
	assert(component == _components[component->_type]);

	// Checks to make this is the owner of the component to be removed.
	if (component == _components[component->Type()])
	{
		_components[component->_type] = 0;

		// Double check, just in case.
		if ((_mask & component->Mask()) == component->Mask())
		{
			_mask &= ~(component->Mask());
		}
		// if (component->Type() != EC_Mesh) Meshes are no longer comnponents
		{
			component->Free();
			delete _components[component->_type];
		}
		return true;
	}
	else
	{
		return false;
	}
}

void Entity::RemoveComponent(EComponent type)
{
	assert(_components[type]);
	if (_components[type])
	{
		// Double check, just in case.
		if ((_mask & _components[type]->Mask()) == _components[type]->Mask())
		{
			_mask &= ~(_components[type]->Mask());
		}
		//if (_components [type]->Type() != EC_Mesh)
	{_components[type]->Free();
	delete _components[type];
	_components[type] = NULL;
	}
	}
}

void Entity::Init()
{
	static int id = 0;
	this->id = id++;

	_active = true;
	for (unsigned i = 0; i < EC_Max; i++)
	{
		_components[i] = NULL;
	}
	_mask = MC_Alive;
}

void Entity::Free()
{
	_active = false;

	// i=1 to skip the EC_NONE null component stored at [0]
	for (unsigned i = 1; i < EC_Max; ++i)
	{
		if (_components[i] != NULL)
		{
			Component* cmp = _components[i];
			if (cmp->Type() != EC_Drawable)
			{
				cmp->Free();
				delete cmp;
				_components[i] = NULL;
			}
		}
	}
	_mask = 0;
}

void Entity::AttachComponent(Component *component)
{
	_components[component->_type] = component;
	_mask |= component->Mask();
	component->Init();

}
