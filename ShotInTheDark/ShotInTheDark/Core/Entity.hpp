//	File name:		Entity.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include <cassert>
#include <string>
#include "Component.hpp"
#include "DebugDefines.hpp"

// this is the basis of an 'object'
// the function names are pretty self explanatory.
// Use a macro when you can, they're prettier. See usage in BasicEngine.cpp
class Entity
{

public:

	Entity() { Init(); }
	~Entity(){ Free(); }
	/**
	 * @brief Attachs an existing component to the Entity.
	 * @details Attaches an existing component of any type to the entity.
	 * Fails if the Entity already has a component of that type.
	 *
	 * @param component Pointer to the component to be attached.
	 */
	void AttachComponent(Component *component);
	/**
	 * @brief Removes a component by pointer from this object.
	 * @details Removes a component from this object, if the component is attached to this object.
	 * fails if it is not attached to this object.
	 *
	 * @param component Pointer to the component to remove.
	 * @return returns whether it was successful or not (true/false).
	 */
	bool RemoveComponent(Component *component);
	/**
	 * @brief Removes a component of the specified type.
	 *
	 * @param type Type of component to remove.
	 */
	void RemoveComponent(EComponent type);
	/**
	 * @brief Checks if the Entity has a component of the given type.
	 *
	 * @param type The type of component to check for.
	 * @return Whether or not the Entity has that type of component.
	 */
	bool HasComponent(EComponent type){ return (_components[type] != nullptr); }

  /**
  * @brief Checks if the Entity has all components specified in the input mask.
  
  * @param components mask of the MC_ component types (can be OR-ed together) to check for.
  * @return Returns whether the Entity has all the specified components.
  */
  bool HasComponent(mask components); 
/**
 * @brief Initializes the Entity.
 * @details Initializes the Entity with default values and NULL for all components.
 */
	void Init(void);
	/**
	 * @brief Cleanup for the Entity.
	 * @details Clears all components and deactivates the Entity. Doesn't destroy it.
	 */
	void Free(void);
	/**
	 * @brief Sets the active state of the Entity.
	 *
	 * @param Whether it should be set to true or false.
	 */
	void Active(bool active) { _active = active; }
	/**
	 * @brief Gets a copy of the Entity's mask.
	 * @details Gets the Entity's component mask, which is used to determine if it is
	 * a valid target for a system.
	 * @return copy of the mask.
	 */
	bool GetActive(void) const {return _active;}

	mask Mask(void) const { return _mask; }
	mask& GetMask(void) { return _mask; }

	const std::string& GetName(void) const { return _name; }
  
	std::string& SetName(void) { return _name; }
	void SetName(const std::string& Name) { _name = Name; }

	// DO NOT USE THESE DIRECTLY! USE THE MACROS BELOW!
	template <typename T>
	//GET_COMPONENT(typename)
	T* GetComponent(EComponent type);
	template <typename T>
	//ADD_COMPONENT(typename)
	void AddComponent(EComponent type);

	int GetId()
	{
		return id;
	}

private:
	std::string _name;
	mask _mask;
	bool _active;
	Component * _components[EC_Max];
	int id;
};

// Entity macros
// Use these to Add and access components. C is just the raw class type, like Velocity or Position.
// it doesn't take an enum or an instance or anything, just the typename.
/**
 * @brief Gets a pointer to the specified component on this Entity.
 * @details If the Entity has the specified component, it returns a pointer
 * to the instance of that component type that is attached to the Entity.
 *
 * @return Pointer to component of the specified type.
 */
#define GET_COMPONENT( C ) \
	GetComponent<C>(EC_##C)
#define GC( C ) \
  GetComponent<C>(EC_##C)
#define GET( C ) \
  GetComponent<C>(EC_##C)
	/**
	 * @brief Adds a component of the specified type to the Entity.
	 * @details Takes a component typename and attaches a new instance of it to the Entity.
	 * asserts if multiple components of one type are added to the same Entity.
	 */
#define ADD_COMPONENT( C ) \
	AddComponent<C>(EC_##C)

template <typename T>
void Entity::AddComponent(EComponent type)
{
	assert(_components[type] == NULL);
	_components[type] = new T();
	_mask |= _components[type]->Mask();
  _components[type]->Init();
}

template<typename T>
T* Entity::GetComponent(EComponent type)
{
	assert(_components[type]);
	return (T *)_components[type];
}