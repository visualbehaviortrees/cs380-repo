//	File name:		ConfrimationScreen.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//	Created 2/4/2015	
//
//	All content � 2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"

#include "ConfirmationScreen.h"

bool ConfirmScreenGS::last_confirm_result = false;

void ConfirmScreenGS::Button_Fn_Yes(void)
{
	// Find the current gamestate
	ConfirmScreenGS* cs = static_cast<ConfirmScreenGS*>(ENGINE->getCurrentGamestate());
	// Set the last confirm result
	cs->last_confirm_result = true;
	// And pop the gamestate
	ENGINE->popGamestate();
	// run our callback if we were given one
	if (cs->yes_callback != nullptr)
		cs->yes_callback();
}

void ConfirmScreenGS::Button_Fn_No(void)
{
	// Find the current gamestate
	ConfirmScreenGS* cs = static_cast<ConfirmScreenGS*>(ENGINE->getCurrentGamestate());
	// Set the last confirm result
	cs->last_confirm_result = false;
	// run our callback if we were given one
	if (cs->no_callback != nullptr)
		cs->no_callback();
	// And pop the gamestate
	ENGINE->popGamestate();
}

ConfirmScreenGS::ConfirmScreenGS()
	: Gamestate(sys_ConfirmScreenGS, sys_ConfirmScreenGS, std::string("Confirm Screen Gamestate")),
	button_yes("Yes", Button_Fn_Yes, Point2D(-7, 0)),
	button_no("No", Button_Fn_No, Point2D(7, 0)),
	yes_callback(nullptr), no_callback(nullptr), current_selection(false)
{

}

void ConfirmScreenGS::Init()
{

	REGISTER_PRESYS(Input);
	REGISTER_POSTSYS(AudioSystem);
	REGISTER_POSTSYS(Graphics);

	CreateBackgroundEntity();

	button_yes.Create_Entity();
	button_no.Create_Entity();

	// Make our cursor
	Transform* cursorTran = new Transform();
	cursorTran->scale = Vector2D(2.0f, 2.0f);
	cursorTran->rotation = 90.0f;
	Drawable* cursordraw = new Drawable();
	cursordraw->layer = 100;
	Sprite* cursorsprite = new Sprite("mainmenu_forkarrow_360.png");
	cursor_ent = ENGINE->Factory->Compose(3,
		cursorTran,
		cursordraw,
		cursorsprite
		);
	cursor_ent->SetName("Cursor");


	// If we're in debug, then call our yes function directly
#if defined _DEBUG && defined DEBUG_NO_CONFIRM
	Button_Fn_Yes();
	return;
#endif
}

void ConfirmScreenGS::Shutdown()
{
	//ENGINE->OM->DestroyEntity(background_ent);
	//ENGINE->OM->DestroyEntity(button_yes.Get_Entity());
	//ENGINE->OM->DestroyEntity(button_no.Get_Entity());
	//ENGINE->OM->DestroyEntity(cursor_ent);
}

void ConfirmScreenGS::PoppedTo(systype)
{
}

void ConfirmScreenGS::Update(float dt)
{ 
	// Since we're checking controller sticks for input, we don't get a "triggered",
	// So we're going to fake it
	static bool triggered = false;

	// Then we wait for no input before we accept new ones
	if (triggered)
		triggered = (CheckAllInputRight() || CheckAllInputLeft());

	if (CheckAllInputEnter())
	{
		// Activate the highlighted button
		if (current_selection)
			button_yes.Click();
		else
			button_no.Click();
		// Play a sound
		AudioSystem *audioSys = static_cast<AudioSystem *>(ENGINE->GetSystem(sys_AudioSystem));
		audioSys->PlaySound("axeHit");
		//audioSys->PlaySound(SFX::AXE_HIT);

	}
	// Move the cursor right
	if ((CheckAllInputRight() || CheckAllInputLeft()) && !triggered)
	{
		// Deselect our currently highlighted button
		(current_selection ? button_yes : button_no).Deselect();
		// Change our selection
		current_selection = !current_selection;
		// Select our new button
		(current_selection ? button_yes : button_no).Select();
		// play a sound
		AudioSystem *audioSys = static_cast<AudioSystem *>(ENGINE->GetSystem(sys_AudioSystem));
		audioSys->PlaySound("axeMiss");
		//audioSys->PlaySound(SFX::AXE_MISS);
		// Then we wait for no input before we accept new ones
		triggered = true;
	}

	// Update the cursors position
	Entity* button = (current_selection? button_yes: button_no).Get_Entity();
	Entity* selectionArrow = ENGINE->OM->GetEntityByName("Cursor").front();

	selectionArrow->GET_COMPONENT(Transform)->Position = button->GET_COMPONENT(Transform)->Position + Vector2D(5.0f, 0.0f);

	// Update the buttons
	button_yes.Update(dt);
	button_no.Update(dt);
}

// Sets the functions to be run before the gamestate is popped from the stack (if any)
void ConfirmScreenGS::SetYesCallback(void(*fn)(void))
{
	yes_callback = fn;
}
void ConfirmScreenGS::SetNoCallback(void(*fn)(void))
{
	no_callback = fn;
}


// returns the result of the last finished ConfirmationScreen,
// so you can easily access it when the gamestate pops
bool ConfirmScreenGS::GetConfirmResult(void)
{
	return last_confirm_result;
}

void ConfirmScreenGS::CreateBackgroundEntity(void)
{
	Transform* bgTran = new Transform();
	bgTran->scale = Vector2D(15.0f, 10.0f);
	Drawable* bgdraw = new Drawable();
	bgdraw->layer = 100;
	Sprite* bgsprite = new Sprite("main_menu_confirmationscreen_720.png");
	background_ent = ENGINE->Factory->Compose(3,
		bgTran,
		bgdraw,
		bgsprite
		);

	background_ent->SetName("Background");
}
