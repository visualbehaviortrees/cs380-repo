//	File name:		ConfrimationScreen.h
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//	Created 2/4/2015	
//
//	All content � 2015 DigiPen (USA) Corporation, all rights reserved.
#pragma once

#include "System.hpp"
#include "MenuButton.h" // Menubutton

// In debug, always confirm automagically
// #define DEBUG_NO_CONFIRM

class ConfirmScreenGS : public Gamestate
{
public:
	ConfirmScreenGS();
	virtual void Init();
	virtual void Shutdown();
	virtual void PoppedTo(systype);
	virtual void Update(float dt);

	// Sets the functions to be run before the gamestate is popped from the stack (if any)
	void SetYesCallback(void(*fn)(void));
	void SetNoCallback(void(*fn)(void));


	// returns the result of the last finished ConfirmationScreen,
	// so you can easily access it when the gamestate pops
	static bool GetConfirmResult(void);

private:
	// The result of the last confirm screen result
	static bool last_confirm_result;

	// static functions for the buttons to access
	static void Button_Fn_Yes(void);
	static void Button_Fn_No(void);

	// Our callback function pointers
	void(*yes_callback)(void);
	void(*no_callback)(void);

	MenuButton button_yes, button_no;
	bool current_selection; // Are we pointing at "yes"?

	// A pointer to our background
	Entity* background_ent;
	Entity* cursor_ent;

	// Creates our background sprite
	void CreateBackgroundEntity(void);

};
