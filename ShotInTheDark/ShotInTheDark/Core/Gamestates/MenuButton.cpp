//	File name:		MenuButton.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//  Created 2/4/2015
//	
//	All content � 2015 DigiPen (USA) Corporation, all rights reserved.
#include "stdafx.h"

#include "MenuButton.h"

MenuButton::MenuButton(std::string basename, void(*action)(), Point2D pos)
	: name(basename), click_action(action), click_counter(0.f), is_clicked(false), position(pos), ent(nullptr)
{
	// Load our base/highlighted/clicked textures
	highlighted = prefix + name + post_highlighted;
	clicked = prefix + name + post_clicked;
	base = prefix + name + post_base;
}

MenuButton::MenuButton(std::string base_, std::string highlighted_, std::string clicked_, void(*action)(), Point2D pos) 
	: name(base_), base(base_), highlighted(highlighted_), 
	clicked(clicked_), click_action(action), click_counter(0.f), 
	is_clicked(false), position(pos), ent(nullptr)
{

}

// Places this button into the game
void MenuButton::Create_Entity()
{


	// And create our object
	Transform* btran = new Transform();
	btran->scale = scale;
	btran->Position = position;
	Drawable* bdraw = new Drawable();
	bdraw->layer = 100;
	Sprite* bsprite = new Sprite(base);
	ent = ENGINE->Factory->Compose(3,
		btran,
		bdraw,
		bsprite
		);
	ent->SetName(std::string("Button ") + name);
}

// Finds ourself
Entity* MenuButton::Get_Entity() const
{
	return ent;
}

void MenuButton::Update(float dt)
{
	if (ent == nullptr)
		return;
	if (is_clicked)
	{
		click_counter += dt;
		if (click_counter > click_time)
		{
			is_clicked = false;
			click_action(); // After this call, we might not even exist...
		}
	}
}

void MenuButton::Select()
{
	ent->GET_COMPONENT(Sprite)->ChangeTexture(highlighted);
}

void MenuButton::Deselect()
{
	is_clicked = false;
	click_counter = 0.0f;

	ent->GET_COMPONENT(Sprite)->ChangeTexture(base);
}

void MenuButton::Click()
{
	is_clicked = true;
	ent->GET_COMPONENT(Sprite)->ChangeTexture(clicked);

}
