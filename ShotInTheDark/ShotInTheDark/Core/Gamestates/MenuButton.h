//	File name:		MenuButton.h
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//	Created 2/4/2015	
//
//	All content � 2015 DigiPen (USA) Corporation, all rights reserved.
#pragma once

#include "System.hpp"

class MenuButton
{
public:
	/* ie, to load menus/buttons/Menu_Button_Credits_Clicked.png, pass in
	"Credits"*/
	MenuButton(std::string basename, void(*action)(), Point2D pos);
	MenuButton(std::string base, std::string highlighted, std::string clicked, void(*action)(), Point2D pos);

	// Places this button into the game
	void Create_Entity();
	// Finds ourself
	Entity* Get_Entity() const;

	void Select();
	void Deselect();
	void Click();
	void Update(float dt);
private:
	std::string base;
	std::string highlighted;
	std::string clicked;

	Entity* ent;

	Point2D position;
	void(*click_action)();

	std::string name;

	bool is_clicked;
	float click_counter;
	const float click_time = 0.33f;
	const std::string prefix = std::string("Menu_Button_");
	const std::string post_base = std::string(".png");
	const std::string post_highlighted = std::string(".png");
	const std::string post_clicked = std::string("_Clicked.png");
	const Vector2D scale = Vector2D(3.0f, 3.0f);
};
