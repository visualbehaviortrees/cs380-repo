//	File name:		MenuGamestate.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//  Created 1/19/2015
//	
//	All content � 2015 DigiPen (USA) Corporation, all rights reserved.
#include "stdafx.h"

#include "MenuGamestate.h"

#include "../Systems/Gameplay/DungeonGameplay.h"
#include "../Systems/Gameplay/DMGameplay.hpp"
#include "OptionsMenu.h"
#include "ConfirmationScreen.h"

// Force debug skip splash and skip menu off in release modes
#ifndef _DEBUG
#undef DEBUG_SKIP_SPLASH
#undef DEBUG_QUICK_LOAD
#endif


void LoadDM(void)
{
	//ENGINE->pushGamestate(new DMGameplay());
}

void LoadDungeon(void)
{
	ENGINE->OM->DestroyAll();
	ENGINE->pushGamestate(new DungeonGameplay());
}

void QuitGame(void)
{
	std::cout << "Quitting the game ... \n";
	ENGINE->_running = false;
	std::cout << "bye!";
}

void ConfirmQuitGame(void)
{
	ConfirmScreenGS *confirm = new ConfirmScreenGS();
	confirm->SetYesCallback(QuitGame);
	ENGINE->pushGamestate(confirm);
}

void HowToPlay(void)
{
	static_cast<MenuGS*>(ENGINE->getCurrentGamestate())->AddSplash(60.0f, "HowToPlayScreen.png");
}

void ShowCredits(void)
{
	static_cast<MenuGS*>(ENGINE->getCurrentGamestate())->AddSplash(60.0f, "credits.png", false);
	static_cast<MenuGS*>(ENGINE->getCurrentGamestate())->AddSplash(60.0f, "credits2.png", false);
}

void Resume(void)
{
	std::cout << "Resuming game...\n";
	ENGINE->popGamestate();
}

void Reset(void)
{
	std::cout << "Resetting game...\n";
	// Pop menu, then call DungeonGameplay's init
	ENGINE->popGamestate();
	ENGINE->popGamestate();
	LoadDungeon();
}



void ConfirmResetGame(void)
{
	ConfirmScreenGS *confirm = new ConfirmScreenGS();
	confirm->SetYesCallback(Reset);
	ENGINE->pushGamestate(confirm);
}



void OptionsMenu(void)
{
	ENGINE->pushGamestate(new OptionsGS());
}

MenuGS::MenuGS(bool splash, bool pause)
	: Gamestate(sys_MenuGS, sys_MenuGS, std::string("Menu Gamestate")),
	current_button_selection(0),
	show_splashes(splash), show_pause(pause)
{

}

void MenuGS::AddButton(std::string button, void(*buttonFn)(void), Point2D pos)
{
	button_list.push_back(MenuButton(button, buttonFn, pos));
}

void MenuGS::AddButton(std::string base, std::string highlight, std::string click, void(*buttonFn)(void), Point2D pos)
{
	button_list.push_back(MenuButton(base, highlight, click, buttonFn, pos));
}


void MenuGS::Init(void)
{
	// Add Basic systems
	REGISTER_PRESYS(Input);
	REGISTER_POSTSYS(AudioSystem);
	REGISTER_POSTSYS(Graphics);

	GETSYS(Graphics)->SetLighting(false);


#ifndef DEBUG_SKIP_SPLASH
	if (show_splashes)
	{
		// Splash screens go here
		std::cout << "Splash screens\n";

		AddSplash(5.0f, "DigiPen_Splash.png");
		AddSplash(3.0f, "credits2.png");
		AddSplash(3.0f, "HowToPlayScreen.png");

	}
#else
		std::cout << "Skipping splash screens ... \n";
#endif

#ifndef DEBUG_QUICK_LOAD
		button_list.clear();
#else
		if (!show_pause)
		{
			std::cout << "Skipping menues ... \n";
			ENGINE->pushGamestate(new DungeonGameplay());
			return;
		}
#endif
		GETSYS(Graphics)->SetCameraPosition(Point2D(0.0f, 0.0f));

		GETSYS(AudioSystem)->PlayMusic("Menu");
}

void MenuGS::Update(float dt)
{
	if (!splash_queue.empty())
	{
		UpdateSplashes(dt);
	}
	else if (!button_list.empty())
	{
		UpdateButtons(dt);
	}
	else
	{
		CreateMenuObjects();
		InitializeButtons();
	}
}

void MenuGS::AddSplash(float SplashTime, std::string filename, bool skip_through)
{
	// If this is the first, make it an entity
	if (splash_queue.empty())
	{
		splash_queue.push({ SplashTime, filename, skip_through });
		CreateSplashEntity(splash_queue.front().filename);
	}
	else
	{
		splash_queue.push({ SplashTime, filename, skip_through });
	}
}

void MenuGS::CreateSplashEntity(std::string filename)
{
	Transform* stran = new Transform();
  stran->scale = Vector2D(18.0f, 10.125f);
	Drawable* sdraw = new Drawable();
	sdraw->layer = static_cast<unsigned>(-1);
	sdraw->useSprite = true;
	Sprite* ssprite = new Sprite(filename);
	GUIItem* sgui = new GUIItem();
	Entity * splashEnt = ENGINE->Factory->Compose(4,
		stran,
		sdraw,
		ssprite,
		sgui
		);
	splashEnt->SetName("Splash");
}

void MenuGS::UpdateSplashes(float dt)
{
	dt = std::min(dt, 1.0f);

	if (CheckAllInputEnter() || CheckMouseTriggered(MOUSE_LEFT) || CheckKeyTriggered(KEY_ESC))
	{
		if (splash_queue.front().skip_through)
		{
			// Remove all splashes from the queue
			while (!splash_queue.empty())
				splash_queue.pop();
    }
    else
    {
			splash_queue.pop();
    }

		// Destroy the in game objects
		std::vector<Entity*> splashes = ENGINE->OM->GetEntityByName("Splash");
		if (!splashes.empty())
		{
			// Remove the old splash screens
			for (Entity * e : splashes)
				ENGINE->OM->DestroyEntity(e);
		}

    // Add the next in game object if there is one
    if (!splash_queue.empty())
      CreateSplashEntity(splash_queue.front().filename);

    return;
	}

	// If a splash screen is up and running, reduce its time
	if (splash_queue.front().time > 0.0f)
	{
		splash_queue.front().time -= dt;
	}
	// If its time is up, switch splash screens
	else
	{
		// Remove it from the queue
		splash_queue.pop();
		// Destroy the in game object
		std::vector<Entity*> splashes = ENGINE->OM->GetEntityByName("Splash");
		if (!splashes.empty())
		{
			// Remove the old splash screens
			for (Entity * e : splashes)
				ENGINE->OM->DestroyEntity(e);
		}

		// Add the next in game object if there is one
		if (!splash_queue.empty())
			CreateSplashEntity(splash_queue.front().filename);
	}

}

void MenuGS::UpdateButtons(float dt)
{
	// Since we're checking controller sticks for input, we don't get a "triggered",
	// So we're going to fake it
	static bool triggered = false;

  bool up = CheckAllInputUp();
  bool down = CheckAllInputDown();
  bool right = CheckAllInputRight();
  bool left = CheckAllInputLeft();
  //std::cout << "MenuGS::UpdateButtons: " << up << down << left << right << std::endl;
	// Then we wait for no input before we accept new ones
	if (triggered)
		triggered = (right || left || down || up);


	if (CheckAllInputEnter())
	{
		// Activate the highlighted button
		button_list[current_button_selection].Click();
		// Play a sound
		AudioSystem *audioSys = static_cast<AudioSystem *>(ENGINE->GetSystem(sys_AudioSystem));
		//audioSys->PlaySound(SFX::AXE_HIT);
		audioSys->PlaySound("menu/select");
	}
	else
	{
		// Move the cursor right
		if ((down || right) && !triggered)
		{
			// Move our selection to the next button
			button_list[current_button_selection].Deselect();
			current_button_selection = (current_button_selection + 1) % button_list.size();
			button_list[current_button_selection].Select();
			// play a sound
			AudioSystem *audioSys = static_cast<AudioSystem *>(ENGINE->GetSystem(sys_AudioSystem));
			//audioSys->PlaySound(SFX::AXE_MISS);		
			audioSys->PlaySound("menu/move");

			// Then we wait for no input before we accept new ones
			triggered = true;
		}
		// Move the cursor left
		if ((up || left ) && !triggered)
		{
			// Move our selection to the next button
			button_list[current_button_selection].Deselect();

			--current_button_selection;
			if (current_button_selection < 0)
				current_button_selection = button_list.size() - 1; 

			button_list[current_button_selection].Select();
			// play a sound
			AudioSystem *audioSys = GETSYS(AudioSystem);
			//audioSys->PlaySound(SFX::AXE_MISS);
			audioSys->PlaySound("menu/move");
			// Then we wait for no input before we accept new ones
			triggered = true;
		}

		// Update its position
		Entity* button = button_list[current_button_selection].Get_Entity();
		Entity* selectionArrow = ENGINE->OM->GetEntityByName("Cursor").front();

		selectionArrow->GC(Transform)->Position = button->GC(Transform)->Position + Vector2D(5.0f, 0.0f);
	}

	button_list[current_button_selection].Update(dt);
}

void MenuGS::CreateMenuObjects(void)
{
	// Make our menu background
	Transform* menuTran = new Transform();
	menuTran->scale = Vector2D(18.0f, 10.125f);
	Drawable* menudraw = new Drawable();
	menudraw->layer = 0;
	Sprite* menusprite = new Sprite("MainMenu.png");

	Entity * bgEnt = ENGINE->Factory->Compose(3,
		menuTran,
		menudraw,
		menusprite
		);

	bgEnt->SetName("Background");

	const float button_x_align = -10.0f;

	// Add our buttons
	if (show_pause)
	{
		AddButton("Menu_Button_Resume.png", "Menu_Button_Resume_Selected.png", "Menu_Button_Clicked.png",
			Resume, Point2D(button_x_align, 6.f));
		AddButton("Menu_Button_Reset.png", "Menu_Button_Reset_Selected.png", "Menu_Button_Clicked.png",
			ConfirmResetGame, Point2D(button_x_align, 3.f));
	}
	else
	{
		AddButton("Menu_Button_Play.png", "Menu_Button_Play_Selected.png", "Menu_Button_Clicked.png",
			LoadDungeon, Point2D(button_x_align, 6.f));
	}
	AddButton("Menu_Button_HowTo.png", "Menu_Button_HowTo_Selected.png", "Menu_Button_Clicked.png",
		HowToPlay, Point2D(button_x_align, 0.f));

	AddButton("Menu_Button_Credits.png", "Menu_Button_Credits_Selected.png", "Menu_Button_Clicked.png",
		ShowCredits, Point2D(button_x_align, -3.f));


	AddButton("Menu_Button_Options.png", "Menu_Button_Options_Selected.png", "Menu_Button_Clicked.png",
		OptionsMenu, Point2D(button_x_align, -6.f));

	AddButton("Menu_Button_Quit.png", "Menu_Button_Quit_Selected.png", "Menu_Button_Clicked.png",
		ConfirmQuitGame, Point2D(button_x_align, -9.f));


	// Make our cursor
	Transform* cursorTran = new Transform();
	cursorTran->scale = Vector2D(2.0f, 2.0f);
	cursorTran->rotation = 90.0f;
	Drawable* cursordraw = new Drawable();
		cursordraw->layer = 100;
	Sprite* cursorsprite = new Sprite("mainmenu_forkarrow_360.png");
	Entity * cursorEnt = ENGINE->Factory->Compose(3,
		cursorTran,
		cursordraw,
		cursorsprite
		);
	cursorEnt->SetName("Cursor");
}

void MenuGS::InitializeButtons(void)
{
	for (MenuButton& b : button_list)
		b.Create_Entity();

	button_list.front().Select();
}

void MenuGS::Shutdown(void)
{
	button_list.clear();
}

void MenuGS::PoppedTo(systype system)
{
	if (system == sys_DungeonGameplay)
	{
		// Check if they beat the game
#ifndef DEBUG_SKIP_SPLASH
		if (GameComplete)
		{
			GameComplete = false;
			AddSplash(10.0f, "ThanksForPlaying.png");
		}
#endif
		// Add our stuff again without the splash screens
		// And we're a main menu, not a pause menu
		Init();

		// Reset the camera
		static_cast<Graphics*>(ENGINE->GetSystem(sys_Graphics))->SetCameraPosition(Point2D(0.0f, 0.0f));
	}

}