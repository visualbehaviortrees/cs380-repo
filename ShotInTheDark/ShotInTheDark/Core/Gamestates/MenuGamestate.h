//	File name:		MenuGamestate.h
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//	Created 1/19/2015	
//
//	All content � 2015 DigiPen (USA) Corporation, all rights reserved.
#pragma once

#include "System.hpp"
#include "MenuButton.h"
// Do we want to skip splash screens in (only) debug mode?
// #define DEBUG_SKIP_SPLASH

// Do we want to skip menus entirely?
// #define DEBUG_QUICK_LOAD

#include <queue>

class MenuGS : public Gamestate
{
public:
	MenuGS(bool show_splashes = true, bool show_pause = false);
	virtual void Init();
	virtual void Shutdown();
	virtual void PoppedTo(systype);
	virtual void Update(float dt);
	void AddSplash(float SplashTime, std::string filename, bool skip_through = true);


protected:
	virtual void CreateMenuObjects(void);
	void AddButton(std::string filename, void(*buttonFn)(void), Point2D pos);
	void AddButton(std::string base, std::string highlight, std::string click, void(*buttonFn)(void), Point2D pos);


private:
	void UpdateSplashes(float dt);
	void CreateSplashEntity(std::string filename);

	void InitializeButtons(void);
	void UpdateButtons(float dt);


	friend class MenuButton;

	// Button functions return true if the menu needs to clean up

	struct SplashScreen
	{
		float time;
		std::string filename;
		bool skip_through;
	};


	// Do we show any splash screens?
	bool show_splashes;

	// Do we show resume & restart instead of play?
	bool show_pause;
	int current_button_selection;
	std::vector<MenuButton> button_list;
	std::queue < SplashScreen > splash_queue;
};


