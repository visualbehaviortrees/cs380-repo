//	File name:		OptionsMenu.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//	Created 3/25/2015	
//
//	All content � 2015 DigiPen (USA) Corporation, all rights reserved.
#include "stdafx.h"

#include "OptionsMenu.h"

void ToggleFullscreen(void)
{
	std::cout << "Toggling fullscreen.\n";
	GETSYS(Graphics)->Toggle_Fullscreen();
}

void ToggleMusic(void)
{
	std::cout << "Toggling Music.\n";
	((AudioSystem *)(ENGINE->GetSystem(sys_AudioSystem)))->ToggleMuteMusic();

}

void ToggleAudio(void)
{
	std::cout << "Toggling All Audio.\n";
	((AudioSystem *)(ENGINE->GetSystem(sys_AudioSystem)))->ToggleMuteAll();
}

void ReturnToMainMenu(void)
{
	ENGINE->popGamestate();
}

void OptionsGS::CreateMenuObjects(void)
{
	// Make our menu background
	Transform* menuTran = new Transform();
	menuTran->scale = Vector2D(18.0f, 10.125f);
	Drawable* menudraw = new Drawable();
	menudraw->layer = 0;
	Sprite* menusprite = new Sprite("OptionsMenu.png");
	Entity * bgEnt = ENGINE->Factory->Compose(3,
		menuTran,
		menudraw,
		menusprite
		);

	bgEnt->SetName("Background");

	const float button_x_align = 10.0f;


	AddButton("Menu_Button_Return.png", "Menu_Button_Return_Selected.png", "Menu_Button_Clicked.png",
		ReturnToMainMenu, Point2D(button_x_align, 6.0f));
	AddButton("Menu_Button_Fullscreen.png", "Menu_Button_Fullscreen_Selected.png", "Menu_Button_Clicked.png",
		ToggleFullscreen, Point2D(button_x_align, 0.f));
	AddButton("Menu_Button_Music.png", "Menu_Button_Music_Selected.png","Menu_Button_Clicked.png",
		ToggleMusic, Point2D(button_x_align, -3.f));
	AddButton("Menu_Button_Sounds.png", "Menu_Button_Sounds_Selected.png", "Menu_Button_Clicked.png",
		ToggleAudio, Point2D(button_x_align, -6.f));

	// Make our cursor
	Transform* cursorTran = new Transform();
	cursorTran->scale = Vector2D(2.0f, 2.0f);
	cursorTran->rotation = 90.0f;
	Drawable* cursordraw = new Drawable();
	cursordraw->layer = 100;
	Sprite* cursorsprite = new Sprite("mainmenu_forkarrow_360.png");
	Entity * cursorEnt = ENGINE->Factory->Compose(3,
		cursorTran,
		cursordraw,
		cursorsprite
		);
	cursorEnt->SetName("Cursor");

}
