//	File name:		OptionsMenu.h
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//	Created 3/25/2015	
//
//	All content � 2015 DigiPen (USA) Corporation, all rights reserved.


#include "MenuGamestate.h"

class OptionsGS : public MenuGS
{
public:
	OptionsGS() : MenuGS(false, false) {}

protected:
	void CreateMenuObjects(void) override;

};

// Button Functions
void ToggleFullscreen(void);
void ToggleMusic(void);
void ToggleAudio(void);
void ReturnToMainMenu(void);