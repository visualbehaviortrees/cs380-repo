#include "stdafx.h"
#include <random>
#include <ctime>

namespace ADLib
{

bool seeded = false;
// Rand functions
void seed()
{
  if(!seeded)
  {
    srand((unsigned)time(NULL));
    seeded = true;
  }
}


void seed(unsigned Seed)
{
  if (!seeded)
  {
    srand(Seed);
    seeded = true;
  }
}

int randInt()
{
  seed();
  return rand();
}

int randInt(int max)
{
  seed();
  return rand() % max;
}

int randInt(int min, int max)
{
  seed();
  return (rand() % (max-min)) + min;
}

bool XinYodds(unsigned x, unsigned y)
{
  seed();
  return ((rand() % y) < x);
}

float randFloat()
{
  seed();
  return static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
}

float randFloat(float max)
{
  seed();
  return (float)((rand() % (int)(max * 1000)) / 1000.f);
}

float randFloat(float min, float max)
{
  seed();
  return ((float)((rand() % (int)((max - min) * 1000)) / 1000.f)) + min;
}

} // namespace ADLib

