#pragma once

namespace ADLib
{

#define EPSILON 0.00001f

template <typename T>
T clamp(T& lower, T& upper, T& value)
{
  if(value > upper)
    return upper;
  else if(value < lower)
    return lower;
  return value;
}

template < typename T>
T clamp(T lower, T upper, T value)
{
  if(value > upper)
    return upper;
  else if(value < lower)
    return lower;
  return value;
}



// Rand functions
void seed();
// allows custom seeding
void seed(unsigned Seed);

// allows seed retrieval (for replays etc)
unsigned getSeed(void);

int randInt();

int randInt(int max);

int randInt(int min, int max);

bool XinYodds(unsigned x, unsigned y);

float randFloat();

float randFloat(float max);

float randFloat(float min, float max);


// Flag control
#define FlagSet(Value, BitFlag)          (((Value) & (BitFlag)) == (BitFlag))
#define FlagOn(Value, BitFlag)           ((Value) |= (BitFlag))
#define FlagOff(Value, BitFlag)          ((Value) &= ~(BitFlag))
#define FlagZero(Value)               ((Value) = 0)
#define FlagOnExclusive(Value, BitFlag)  FlagZero(Value); \
                                      FlagOn((Value),(BitFlag) );
#define FlagToggle(Value, BitFlag)       ((Value) ^= (BitFlag))


// Float operations
#define isZero(x)     ((x < EPSILON) && (x > -EPSILON)
#define isEqual(x,y)  (((x >= y) ? (x-y) : (y-x)) < EPSILON)

} // namespace ADLib


#include "Allanptr.h"
