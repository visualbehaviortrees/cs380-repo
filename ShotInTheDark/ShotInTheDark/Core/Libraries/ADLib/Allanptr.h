// Author: Allan Deutsch
// purpose: RAII automatic resource management
// 
/* changelog:
              2/5/2015 - initial implementation.
                       - Allanptr (single memory managed pointer)
                       - RefCounter - tracks number of Allanptr instances attached to a memory block.
                       -

*/
#pragma once
class RefCounter
{
public:
  RefCounter() : counter(0) { }
  unsigned up        ()     { return ++counter; }
  unsigned down      ()     { return --counter; }
  unsigned operator++()     { return ++counter; }
  unsigned operator--()     { return --counter; }
  unsigned operator()()     { return   counter; }
private:
  unsigned counter;
};

template < typename T >
class Allanptr
{
public:
  Allanptr();
  Allanptr(T *unmanaged) : 
                         instance(unmanaged) 
                         { 
                         }
  ~Allanptr();

  T& operator*  ( )        { return *instance; }
  T* operator-> ( )        { return  instance; }
  Allanptr<T>& operator=   ( const   Allanptr <T> & );
private:
  T* instance;
  RefCounter *count;
};

template < typename T >
Allanptr<T>::Allanptr()
{
  instance = new T();
  count = new RefCounter;
  (*count)++;
}

template < typename T >
Allanptr<T>& Allanptr<T>::operator=(const Allanptr<T> & rhs)
{
  if (this != &rhs)
  {
    if (count->down() == 0)
    {
      delete instance;
      delete count;
    }
    instance = rhs.instance;
    count = rhs.count;
    count->up();
  }
  return *this;
}

template < typename T >
Allanptr<T>::~Allanptr()
{
  if(!count->down())
  {
    delete instance;
    delete count;
  }

}
