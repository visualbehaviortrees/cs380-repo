// Author: Allan Deutsch
// All content copyright (C) Allan Deutsch 2015. All rights reserved.

#pragma once

namespace ADL
{

  class IEventObject
  {
  public:
    virtual void notify() = 0;
    virtual ~IEventObject() = 0;
  };

} // ADL
