// Author: Allan Deutsch
// All content copyright (C) Allan Deutsch 2015. All rights reserved.

#pragma once
#include "stdafx.h"
#include "ITimedLogic.h"

namespace ADL
{


  ITimedLogic::ITimedLogic(float time, IEventObject * eventLogic)
                          :
                          _totalTime(time),
                          _remainingTime(time),
                          _eventLogic(eventLogic)
  {
  }


  bool ITimedLogic::tick(float dt)
  {
    _remainingTime -= dt;

    // This if check is here explicitly for branch prediction.
    // Usually the remaining time will be >0.f.
    if (_remainingTime > 0.f)
      return false;

    timeEnd();
    return true;
  }

  inline float ITimedLogic::progress() const
  {
    return _remainingTime / _totalTime;
  }

} // ADL
