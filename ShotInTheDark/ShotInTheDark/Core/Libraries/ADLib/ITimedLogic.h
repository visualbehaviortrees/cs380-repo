// Author: Allan Deutsch
// All content copyright (C) Allan Deutsch 2015. All rights reserved.

#pragma once
#include "IEventObject.hpp"

namespace ADL
{

  class ITimedLogic
  {
  public:

    ITimedLogic(float time, IEventObject * eventLogic = nullptr);

    virtual ~ITimedLogic() {};

    /*!
    * @brief Moves the timer forward by one tick.
    *
    * @return Returns true when the timer hits 0 or less.
    */
    virtual bool tick(float dt);

    /*!
    * @brief Reports the current progress in percentage (0-100) of the timed logic.
    *
    * @return returns the percent of total initial time elapsed so far.
    */
    inline float progress() const;

  protected:
    /*!
    * @brief Notifies the derived object that the time has reached zero. 
    * Called once when the timer reaches 0.f.
    */
    virtual void timeEnd() = 0;
  
    float _totalTime;
    float _remainingTime;
    IEventObject * _eventLogic;
  };

} // ADL
