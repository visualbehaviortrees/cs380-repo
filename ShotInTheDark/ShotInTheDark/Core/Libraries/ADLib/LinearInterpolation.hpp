// Author: Allan Deutsch
// All content copyright (C) Allan Deutsch 2015. All rights reserved.
#pragma once

#include "ITimedLogic.h"
namespace ADL
{

  class ILerp : public ITimedLogic
  {
  public:
    ILerp(float duration, IEventObject * eventLogic) 
          : 
          ITimedLogic(duration, eventLogic)
    {
    }

    virtual bool tick(float dt) = 0;
    virtual void cancel()       = 0;
    virtual ~ILerp() {}


  };

  template<typename T>
  class Lerp : public ILerp
  {
  public:
    Lerp(T &begin, T end, float duration, bool autoInterrupt = true, IEventObject *eventLogic = nullptr);
    
    ~Lerp() override;

    void interrupt(T end, float duration);
    void interrupt(T end);
    
    void cancel() override;

    bool tick(float dt) override;

    void timeEnd() override;

  private:
    T _previous;
    T _end;
    T &_value;
    T _stepsize;
    const bool _checkInterrupt;


    auto isLerpable(T value, float check) -> decltype(value * check)
    {
      return value;
    };
    

  };

  template<typename T>
  ITimedLogic* createLERP(T &begin, T end, float duration, bool autoInterrupt = true, IEventObject *eventLogic = nullptr)
  {
    return new Lerp<T>(begin, end, duration, autoInterrupt, eventLogic);
  }

#include "LinearInterpolation.inl"

} // ADL
