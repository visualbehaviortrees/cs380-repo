
template<typename T>
inline Lerp<T>::Lerp(T & begin, T end, float duration, bool autoInterrupt, IEventObject *eventLogic)
               : 
               ILerp(duration, eventLogic),
               _value(begin),
               _stepsize((end-begin) * (1.f / duration)),
               _checkInterrupt(autoInterrupt)
{
}

template<typename T>
inline Lerp<T>::~Lerp()
{
  if (_eventLogic != nullptr)
    delete _eventLogic;
}


template<typename T>
inline void Lerp<T>::interrupt(T end, float duration)
{
  _end = end;
  _totalTime = duration;
  _remainingTime = duration;
  _previous = _value;

  _stepsize = (end - _value) * (1.f / duration);

}

template<typename T>
inline void Lerp<T>::interrupt(T end)
{
  _end = end;

  _stepsize = (end - _previous) * (1.f / (_remainingTime));
}

template<typename T>
inline void Lerp<T>::cancel()
{
  _remainingTime = 0.f;
}

template<typename T>
inline bool Lerp<T>::tick(float dt)
{
  if (!(_checkInterrupt && (_value != _previous )) && _remainingTime > 0.f)
  {

    // We deal with time first so we can end precisely.
    _remainingTime -= dt;

    if (_remainingTime >= dt)
    {
      _value += _stepsize * dt;
      _previous = _value;
      return false;
    }

    // this is encountered when the remaining duration is less than 0.f
    // It snaps the value to it's end value to prevent over-shooting.
    _value = _end;
    _previous = _end;
    _remainingTime = 0.f;
    return true;
  }

  _remainingTime = 0.f;
  return true;
  timeEnd();
}

template<typename T>
inline void Lerp<T>::timeEnd()
{
  if (_eventLogic != nullptr)
  {
    _eventLogic->notify();
  }
}
