// Author: Allan Deutsch
// purpose: RAII automatic resource management
// 
/* changelog:
              2/ 5/2015 - initial implementation.

              2/25/2015 - Added AutoSList with constructor and destructor.


*/
#pragma once
#include "Allanptr.h"
namespace ADLib
{
  template < typename T >
  class Singleton
  {

    static Singleton < T > & instance()
    {
      if (_instance != NULL)
      {
        // do stuff
      }
      return *_instance;
    }

  protected:
    static Allanptr< Singleton<T> > _instance;

  };

}
