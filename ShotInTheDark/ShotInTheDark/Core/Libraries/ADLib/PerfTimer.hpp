

#pragma once
#include <ostream>
#include <chrono>
#include <string>
#include "TimeDefines.hpp"
namespace ADLib
{

  struct performanceData
  {
    void Compute() 
    { 
      frameAverage =  myTime / static_cast<float>(updates);
      adjustedAverage = (myTime - slowFrame) / (updates - 1.f);
      runtimePercentage = myTime / totalTime * 100.f;
    }
    float initTime;
    float frameAverage;
    float adjustedAverage;
    float slowFrame = 0.0f;
    float fastFrame = 100.f;
    float myTime = 0.0f;
    float totalTime = 0.0f;
    float lastFrame = 0.0f;
    float runtimePercentage;
    unsigned updates = 0;
    std::string name;
    float logData(std::ostream& os)
    {
      os.precision(6);
      Compute();
      // implement me!
      os
        << std::endl 
        << "/*****************************************************/\n"
        << name << " Performance Data\n"
        << "/*****************************************************/\n"

        << "Total runtime:      " << myTime << "s ("
        << static_cast<int>(myTime) / 3600 << "h "
        << (static_cast<int>(myTime) / 60) % 60 << "m " 
        << (static_cast<int>(myTime) % 60) << "s " 
        << (myTime - (static_cast<int>(myTime))) * 1000 << "ms)" // 1000 ms per second
        << std::endl

        << "Init time:          "
        << initTime
        << std::endl

        << "Slowest frame:      "
        << slowFrame
        << std::endl

        << "Fastest Frame:      "
        << fastFrame
        << std::endl

        << "Average time/frame: "
        << frameAverage
        << std::endl

        << "Adjusted Average:   "
        << adjustedAverage
        << std::endl

        << "Runtime Percentage: "
        << runtimePercentage
        << "  ("
        << (initTime + myTime) / totalTime * 100
        << "% including init time)"
        << std::endl
        << std::endl;

      return myTime + initTime;
    }
  };


  class PerfTimer
  {
  public: 
    PerfTimer(performanceData *perfData, float dt) : start(std::chrono::high_resolution_clock::now()), _perfData(perfData) { perfData->totalTime += dt; }

    ~PerfTimer()
    {
      timeDelta dt = std::chrono::high_resolution_clock::now() - start;

      _perfData->lastFrame = dt.count();

      _perfData->myTime += _perfData->lastFrame;

      if (_perfData->lastFrame < _perfData->fastFrame)
        _perfData->fastFrame = _perfData->lastFrame;

      else if (_perfData->lastFrame > _perfData->slowFrame)
        _perfData->slowFrame = _perfData->lastFrame;
    }


  private:
    instant start;
    performanceData *_perfData;
  };

}