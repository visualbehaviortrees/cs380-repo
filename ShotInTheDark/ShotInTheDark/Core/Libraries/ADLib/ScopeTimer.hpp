

#pragma once

#include <chrono>
#include "TimeDefines.hpp"
namespace ADLib
{

  class ScopeTimer
  {
  public: 
    ScopeTimer(float *dt) : start(std::chrono::high_resolution_clock::now()), delta(dt) {}

    ~ScopeTimer()
    {
      end = std::chrono::high_resolution_clock::now();
      timeDelta dt = end - start;
      *delta = dt.count();
    }


  private:
    instant start;
    instant end;
    float *delta;
  };

}