

#pragma once

#include <chrono>
#include "TimeDefines.hpp"
namespace ADLib
{

  class StopWatch
  {
  public: 
    StopWatch() : start(std::chrono::high_resolution_clock::now()) {}

    void Reset() { start = std::chrono::high_resolution_clock::now(); lap = start; }

    float Check()
    {
      instant now = std::chrono::high_resolution_clock::now();
      timeDelta dt = now - start;
      return dt.count();
    }

    void Lap() { lap = std::chrono::high_resolution_clock::now(); }

    float CheckLap()
    {
      instant now = std::chrono::high_resolution_clock::now();
      timeDelta dt = now - lap;
      return dt.count();
    }

    ~StopWatch()
    {
      lap = std::chrono::high_resolution_clock::now();
      timeDelta dt = lap - start;
    }


  private:
    instant start;
    instant lap;
  };

}