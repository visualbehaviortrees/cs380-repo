
#pragma once
#include <chrono>

namespace ADLib
{

  typedef std::chrono::milliseconds ms;
  typedef std::chrono::high_resolution_clock hrclock;
  typedef std::chrono::time_point<std::chrono::high_resolution_clock> instant;
  typedef std::chrono::duration<float> timeDelta;

}