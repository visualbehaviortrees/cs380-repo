#include "stdafx.h"
#include "AntTweakBar.hpp"
#include <stdlib.h>
#include <math.h>
#include <string>
#include <vector>


#include <Windows.h>


#define BEHAVIORTREE_DIRECTOR "BehaviorTrees/"

TwType vectorType;
std::vector<string> behaviorTreeFiles;

bool Toggle_AntTweakBar = true;

#define CHAR_BUFFER_SIZE 100

Entity *selectedEntity = 0;

struct TWEntityBarVars
{
	TWEntityBarVars() : health(0), current_BehaviourTree(0), Position(0, 0), scale(1.0, 1.0){}

	char entityName[CHAR_BUFFER_SIZE];

	float health;
	int current_BehaviourTree;

	Vector2D Position;
	Vector2D scale;
}entityBarVars;

void UppercaseCopy(char *dest, const char *src, size_t length)
{
	unsigned int i = 0;
	for (i = 0; i < length - 1; i++)
	{
		dest[i] = (char)toupper(src[i]);
	}
	dest[i] = '\0'; // ensure that dest is null-terminated
}

void TW_CALL SetPropertyNameCallBack(const void *value, void *clientData)
{
	const char *valueCString = (const char *)value;
	char temp[CHAR_BUFFER_SIZE];
	UppercaseCopy(temp, valueCString, CHAR_BUFFER_SIZE * sizeof(char));
	char *data = (char*)clientData;
	char oldClientData[CHAR_BUFFER_SIZE];
	strcpy_s(oldClientData, CHAR_BUFFER_SIZE, data);
	strcpy_s(data, CHAR_BUFFER_SIZE, temp);
}
void TW_CALL GetPropertyNameCallBack(void *value, void *clientData)
{
	char *dest = (char *)value;
	char *data = (char*)clientData;
	UppercaseCopy(dest, data, CHAR_BUFFER_SIZE * sizeof(char));
}

void TW_CALL SetIntCallBack(const void *value, void *clientData)
{
	int* clientIntData = (int*)clientData;
	int* valueInt = (int*)value;
	if (*valueInt == *clientIntData)
	{
		return;
	}

	*clientIntData = *valueInt;

	if (clientIntData == &entityBarVars.current_BehaviourTree)
	{
		if (entityBarVars.current_BehaviourTree)
		{ 
			(selectedEntity)->GET_COMPONENT(Behavior)->GenerateTree(std::string(BEHAVIORTREE_DIRECTOR + behaviorTreeFiles[entityBarVars.current_BehaviourTree]));
			(selectedEntity)->GET_COMPONENT(Behavior)->SetFileId(entityBarVars.current_BehaviourTree);
		}
		else
		{
			(selectedEntity)->GET_COMPONENT(Behavior)->FreeTree();
		}
	}
}
void TW_CALL GetIntCallBack(void *value, void *clientData)
{
	int* clientIntData = (int*)clientData;
	int *ptr = (int*)value;
	*ptr = *clientIntData;
}

void TW_CALL GetVector2CallBack(void *value, void *clientData)
{
	Vector2D* clientVec2Data = (Vector2D*)clientData;
	Vector2D *ptr = (Vector2D*)value;
	*ptr = *clientVec2Data;
}

void TW_CALL SetVector2CallBack(const void *value, void *clientData)
{
	Vector2D* clientVec2Data = (Vector2D*)clientData;
	Vector2D* valueVec2 = (Vector2D*)value;
	if (*valueVec2 == *clientVec2Data)
	{
		return;
	}

	*clientVec2Data = *valueVec2;

	if (clientData == &entityBarVars.Position)
	{
		(selectedEntity)->GET_COMPONENT(Transform)->Position = entityBarVars.Position;
	}
	else if (clientData == &entityBarVars.scale)
	{
		(selectedEntity)->GET_COMPONENT(Transform)->scale = entityBarVars.scale;
	}
}

void TW_CALL GetFloatCallBack(void *value, void *clientData)
{
	float* clientFloatData = (float*)clientData;
	float *ptr = (float*)value;
	*ptr = *clientFloatData;
}
void TW_CALL SetFloatCallBack(const void *value, void *clientData)
{
	float* clientFloatData = (float*)clientData;
	float* valueFloat = (float*)value;
	if (*valueFloat == *clientFloatData)
	{
		return;
	}

	float tempClientData = *clientFloatData;
	*clientFloatData = *valueFloat;

	if (clientFloatData == &entityBarVars.health)
	{
		(selectedEntity)->GET_COMPONENT(Health)->health = entityBarVars.health;
	}
}

/* Returns a list of files in a directory (except the ones that begin with a dot) */
void GetAllFilesInDirectory(vector<string> & out, string const & directory, bool fullPath)
{
	HANDLE dir;
	WIN32_FIND_DATA fileData;
	if ((dir = FindFirstFile((directory + "/*").c_str(), &fileData)) == INVALID_HANDLE_VALUE)
		return; // No files found
	do
	{
		if ((fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0) // Is directory
			continue;

		string filename = fileData.cFileName;
		if (filename[0] == '.')
			continue;
		if (fullPath)
		{
			out.push_back(directory + "/" + filename);
		}
		else
		{
			out.push_back(filename);
		}
	} while (FindNextFile(dir, &fileData));

	FindClose(dir);
}

std::string GetFileExtension(std::string path){
	int indDot = path.find_last_of(".");
	if (indDot == -1){
		return "";
	}
	else{
		return path.substr(indDot + 1, path.size() - indDot - 1);
	}
}

void GetAllTxtFilesInDirectory(std::vector<std::string> &out, const std::string &directory, bool fullPath)
{
	std::vector<std::string> allFiles;
	GetAllFilesInDirectory(allFiles, directory, fullPath);
	for (auto it = allFiles.begin(); it != allFiles.end(); ++it)
	{
		std::string ext = GetFileExtension(*it);
		for (unsigned int i = 0; i < ext.size(); i++)
		{
			ext[i] = tolower(ext[i]);
		}
		if (ext == "bhv")
		{
			out.push_back(*it);
		}
	}
}

TwType behaviorTreeType;

void TW_CALL ReloadButtonCallBack(void *clientData)
{
	behaviorTreeFiles.clear();
	behaviorTreeFiles.push_back("NULL");

	GetAllTxtFilesInDirectory(behaviorTreeFiles, BEHAVIORTREE_DIRECTOR, false);

	TwEnumVal * ev = new TwEnumVal[behaviorTreeFiles.size()];

	for (unsigned int i = 0; i < behaviorTreeFiles.size(); i++)
	{
		ev[i].Value = i;
		ev[i].Label = behaviorTreeFiles[i].c_str();
	}

	behaviorTreeType = TwDefineEnum("behaviorTrees", ev, behaviorTreeFiles.size());
	delete ev;
}

void SelectEntityAntTweakBar(Entity* init)
{
	selectedEntity = init;
	
	mask entity_mask = selectedEntity->Mask();

	SetPropertyNameCallBack(selectedEntity->GetName().c_str(), entityBarVars.entityName);

	if((entity_mask & MC_Health) == MC_Health)
	{
		entityBarVars.health = (selectedEntity)->GET_COMPONENT(Health)->GetHealthValue2();

		TwDefine("VBTD/health visible=true ");
	}
	else
	{
		TwDefine("VBTD/health visible=false ");
	}

	if ((entity_mask & MC_Transform) == MC_Transform)
	{
		entityBarVars.Position = (selectedEntity)->GC(Transform)->Position;
		entityBarVars.scale = (selectedEntity)->GET_COMPONENT(Transform)->scale;

		TwDefine("VBTD/Position visible=true ");
		TwDefine("VBTD/Scale visible=true ");
	}
	else
	{
		TwDefine("VBTD/Position visible=false ");
		TwDefine("VBTD/Scale visible=false ");
	}

	if ((entity_mask & MC_Behavior) == MC_Behavior)
	{
		TwDefine("VBTD/behaviorTrees visible=true ");

		entityBarVars.current_BehaviourTree = (selectedEntity)->GET_COMPONENT(Behavior)->GetFileId();
	}
	else
	{
		TwDefine("VBTD/behaviorTrees visible=false ");
	}
}

TwBar* InitMyAntweakBar()
{
  TwBar *bar;

  if(!TwInit(TW_OPENGL_CORE, NULL))
    return 0;
  
  bar = TwNewBar("VBTD");

  TwDefine("VBTD label='Visual Behavior Tree Demo'");

  if(!bar)
    return 0; // something failed

  Graphics* GS = (Graphics*)(ENGINE->GetSystem(sys_Graphics));

  int width = GS->GetWindowWidth();  // initiliaze to the current window width
  int height = GS->GetWindowHeight(); // initiliaze to the current window height

  // Tell the window size to AntTweakBar
  if(!TwWindowSize(width, height))
    return 0; // something failed

  TwStructMember vector2Members[] =
  {
	  { "X", TW_TYPE_FLOAT, 0, "Step=0.01" },
	  { "Y", TW_TYPE_FLOAT, sizeof(float), "Step=0.01" },
  };

  vectorType = TwDefineStruct("Vector2", vector2Members, 2, 2 * sizeof(float), NULL, NULL);

  TwAddVarCB(bar, "entityName", TW_TYPE_CSSTRING(CHAR_BUFFER_SIZE), SetPropertyNameCallBack, 
	  GetPropertyNameCallBack, entityBarVars.entityName, " label='Entity Name' readonly = true");

  SetPropertyNameCallBack("NULL", entityBarVars.entityName);

  TwAddVarCB(bar, "health", TW_TYPE_FLOAT, SetFloatCallBack, GetFloatCallBack, &entityBarVars.health, " label='Health' ");

  TwAddVarCB(bar, "Position", vectorType, SetVector2CallBack, GetVector2CallBack, &entityBarVars.Position, " label='Position' group='Transform'");
  TwAddVarCB(bar, "Scale", vectorType, SetVector2CallBack, GetVector2CallBack, &entityBarVars.scale, " label='Scale' group='Transform'");

  ReloadButtonCallBack(nullptr);

  TwAddVarCB(bar, "behaviorTrees", behaviorTreeType, SetIntCallBack, GetIntCallBack, &entityBarVars.current_BehaviourTree, "label='Behavior Tree'");

  TwAddButton(bar, "reloadButton", ReloadButtonCallBack, nullptr, "label='Reload Behavior Trees'");

  return bar;
}

int UpdateAntTweakBar()
{
	if (Toggle_AntTweakBar)
	{
		TwDraw();
	}

	if(CheckKeyTriggered(KEY_BACKSLASH) )
	{
		Toggle_AntTweakBar = !Toggle_AntTweakBar;
	}

	return 1;
}