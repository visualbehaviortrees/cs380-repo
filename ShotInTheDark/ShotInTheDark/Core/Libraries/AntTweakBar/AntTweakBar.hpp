
#pragma once

#include <../ShotInTheDark/ShotInTheDark/AntTweakBar/include/AntTweakBar.h>
#include "../../Systems/Graphics/OpenGL.hpp"
#include "../../Systems/Graphics/Graphics.hpp"
#include "../../Engine.hpp"
#include "../Core/Systems/Input/InputSystem.hpp"
#include "../../Components/Components.h"
#include "../Core/Systems/Physics/Collision.hpp"

#include <iostream>

#define KEY_ACCENT 96

using namespace Math;

extern bool Toggle_AntTweakBar;

TwBar* InitMyAntweakBar(void);
int UpdateAntTweakBar(void);

void SelectEntityAntTweakBar(Entity *init);
