#pragma once
#pragma warning( disable : 4715 )
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>



// DO NOT USE THIS CONTAINER! NOT FULLY TESTED AND I SPECIFICALLY MADE IT
// FOR THE MEMORY MANAGER. I NEEDED A CONTAINER THAT DIDN'T USE REGULAR NEW or NEW[]
template <typename T>
class KPVec
{
public:
  KPVec() : size_(0), capacity_(0), array_(0) {}
  ~KPVec()
  {
    if (array_ == nullptr)
      return;

    clear();
  }

  KPVec& operator=(const KPVec& rhs)
  {
    T* temp_hold = static_cast<T*>(malloc(sizeof(T)* rhs.capacity_));

    for (unsigned i = 0; i < rhs.size(); ++i)
      temp_hold[i] = rhs[i];

    this->array_ = temp_hold;
    this->size_ = rhs.size_;
    this->capacity_ = rhs.capacity_;

    return *this;
  }

  unsigned size(void) const { return size_; }
  bool empty(void) const
  {
    if (size_)
      return false;

    return true;
  }

  void push_back(T value)
  {
    if (!capacity_ || size_ == capacity_)
      grow();

    array_[size_++] = value;
  }

  T operator[](unsigned index) const
  {
    if (check_bounds(index))
      return array_[index];

    assert(false && "INDEX OUT OF BOUNDS");
  }
  T& operator[](unsigned index)
  {
    if (check_bounds(index))
      return array_[index];

    assert(false && "INDEX OUT OF BOUNDS");
  }

  void clear(void)
  {
    if (!array_)
      return;

    array_ = nullptr;  // I know this is wrong but I needed it to work for now
    size_ = 0;
  }

  void pop_back(void)
  {
    if (!size_)
      return;
    array_[--size_] = 0;
  }

  T& back(void)
  {
    if (check_bounds(size_ - 1))
      return array_[size_ - 1];

    assert(false && "BAD BOUNDARY!");
    return array_[size_ - 1];
  }

private:
  unsigned size_;
  unsigned capacity_;
  T* array_;

  bool check_bounds(unsigned index) const
  {
    if (index > size_)
      return false;

    return true;
  }

  void grow(void)
  {
    capacity_ = (capacity_) ? capacity_ * 2 : 1;
    bytes ptr = (bytes)malloc(sizeof(T)* capacity_);
    T* temp_hold = new(ptr)T();

    for (unsigned i = 0; i < size_; ++i)
      temp_hold[i] = array_[i];

    free(array_);
    array_ = temp_hold;
  }
};