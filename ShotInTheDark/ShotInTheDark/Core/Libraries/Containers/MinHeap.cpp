#include "stdafx.h"
#include "MinHeap.h"

namespace PathFind
{
	void MinHeap::insert(HeapNode& hNode)
	{
		heap[firstOpen] = hNode;

		int key = hNode.key;

		//Heap is non-empty
		if (firstOpen)
		{
			int child = firstOpen;
			int parent = (child - 1) / 2;

			while (parent >= 0 && heap[parent].key > key)
			{
				std::swap(heap[parent], heap[child]);

				child = parent;
				parent = (child - 1) / 2;
			}
		}

		firstOpen++;
	}

	void MinHeap::Pop()
	{
		assert(firstOpen > 0);

		firstOpen--;

		if (firstOpen > 0)
		{
			heap[0] = heap[firstOpen];
			int nodeKey = heap[0].key;

			int parent = 0;
			int child1 = 1;
			int child2 = 2;

			while (true)
			{
				//Both children don't exist, heap is fixed
				if (child1 >= firstOpen)
				{
					break;
				}

				//child1 exists but child2 does not exist
				if (child2 >= firstOpen)
				{
					if (heap[child1].key < nodeKey)
					{
						std::swap(heap[parent], heap[child1]);
					}

					break;
				}
				else
				{
					int minChild = heap[child1].key < heap[child2].key ? child1 : child2;

					if (heap[minChild].key < nodeKey)
					{
						std::swap(heap[parent], heap[minChild]);
					}

					parent = minChild;
					child1 = 2 * parent + 1;
					child2 = 2 * parent + 2;
				}
			}
		}
	}
}