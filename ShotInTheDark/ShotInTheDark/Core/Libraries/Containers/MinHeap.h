#pragma once

#define MAX_WIDTH 40

#include "Node.h"
#include <algorithm>
#include <utility>

namespace PathFind
{
	struct HeapNode
	{
		HeapNode() : node(0), key(0){}

		HeapNode(Node* node, int key) : node(node), key(key){}

		Node* node;
		int key;
	};

	class MinHeap
	{
	public:
		MinHeap() : firstOpen(0){}

		void insert(HeapNode& hNode);

		void Pop();

		inline HeapNode const& MinElement();

		inline int size();

		inline void Clear();

	private:
		HeapNode heap[MAX_WIDTH * MAX_WIDTH];
		int firstOpen;
	};

	inline int MinHeap::size()
	{
		return firstOpen;
	}

	inline void MinHeap::Clear()
	{
		firstOpen = 0;
	}

	inline HeapNode const& MinHeap::MinElement()
	{
		return heap[0];
	}
}