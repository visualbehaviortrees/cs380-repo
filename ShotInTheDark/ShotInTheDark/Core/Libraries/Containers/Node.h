#pragma once

namespace PathFind
{
	struct Node
	{
		Node() : parentRow(-1), parentCol(-1), row(-1), cost(0),
			total(0), onOpen(false), onClosed(false), iteration(-1)
		{
		}

		int parentRow; //Parent Node (-1 is start node)
		int parentCol; //Parent Node (-1 is start node)

		int row;		//row it is on
		int col;		//column it is on

		int cost;		//Cost to get to this node, g(x)
		int total;	//Total cost, g(x) + h(x)

		bool onOpen;	//Is on the open list
		bool onClosed;	 //Is on the closed list

		int iteration;
	};
}