//	File name:		Stack.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.
#pragma once
#include <vector>
#define STACK_MAX = 10
template <typename T>
class stack
{
public:
  stack()
  {
    top = -1;
  }
  void push(T object)
  {
    ++top;
    if(top < STACK_MAX)
    {
      _members[top] = object;
    }
    else
    {
      std::cout << "Stack is full.\n"
      --top;
    }
  }
  void pop()
  {
    if(top > -1)
    {
      _members.pop_back();
      --top;
    }
    else
    {
      std::cout << "stack is empty.\n"
      top = -1;
    }
  }
  T& get(void)
  {
    if(top > -1)
      return _members[top];
    else
      return (T &)NULL;
  }
  int size(void)
  {
    return top + 1;
  }
private:
  int top;
  std::vector<T> _members;
};
