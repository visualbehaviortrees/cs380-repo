//	File name:		JSONReader.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Kyle Philistine
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "JSONReader.hpp"
#include "../Math/Math2D.hpp"
#include "../../Components/Mesh.hpp"
#include "../../ObjectFactory.h"
#include "../../Engine.hpp"
#include "../../Components/Components.h"


#include <cassert>
#include <array>
#include <sstream>

extern Engine* ENGINE;

using namespace Math;

static bool check_name = false;
static int closed = 0;

enum Qualifier
{
  Continue,
  Value,
  Object,
  CloseChildObject,
  Stop
};

enum Comps
{
  _NONE,
  _Parent,         
  _Powerup,        
  _Mesh,          
  _RigidBody,      
  _Transform,      
  _Player,         
  _Team,           
  _Audio,          
  _Bullet,         
  _Base,          
  _Drawable,       
  _Sprite,         
  _SpriteText,     
  _ParticleEmitter,
  _Flag,           
  _Health,        
  _Score,          
  _PlayerAI,       
  _Turret,         
  _PowerupSpawner, 
  _ParticleEffect, 
  _GUIItem,      
  _Light
};


static struct Flags
{
  bool parent_flag;
  bool child_flag;
  bool more_children_flag;
  int num_children;
}Flags;

std::string SetLevel(const int level)
{
  switch(level)
  {
    case 1:
      return std::string("Level1.json");
    case 2:
      return std::string("Level2.json");
    default: 
      return std::string("Level1.json");
  }
}


void JSONReader::Component_LookUp_Table(const std::string name)
{
  std::vector<JSONReader::ValueType> result;
  if(strcmp(name.c_str(), "Audio") == 0)
  {
  }

  else if(strcmp(name.c_str(), "Base") == 0)
  {
    result.push_back(JSONReader::ValueType::NONE);
  }

  else if(strcmp(name.c_str(), "Bullet") == 0)
  {
    result.push_back(JSONReader::ValueType::NONE);
  }

  else if(strcmp(name.c_str(), "Drawable") == 0)
  {
    result.push_back(JSONReader::ValueType::Bool);   //bool visible;
    result.push_back(JSONReader::ValueType::Bool);   //bool useSprite;
    result.push_back(JSONReader::ValueType::Bool);   //bool drawFromCenter;
    result.push_back(JSONReader::ValueType::Bool);   //bool startPointColor;
    result.push_back(JSONReader::ValueType::Float);  //float r;
    result.push_back(JSONReader::ValueType::Float);  //float g;
    result.push_back(JSONReader::ValueType::Float);  //float b;
    result.push_back(JSONReader::ValueType::Float);  //float a;
    result.push_back(JSONReader::ValueType::Float);  //float beginR;
    result.push_back(JSONReader::ValueType::Float);  //float beginG;
    result.push_back(JSONReader::ValueType::Float);  //float beginB;
    result.push_back(JSONReader::ValueType::Float);  //float beginA;
    result.push_back(JSONReader::ValueType::INT);    //unsigned layer;
  }

  else if(strcmp(name.c_str(), "Flag") == 0)
  {
    result.push_back(JSONReader::ValueType::NONE);
  }

  else if(strcmp(name.c_str(), "Health") == 0)
  {
    result.push_back(JSONReader::ValueType::INT);  //int health;
    result.push_back(JSONReader::ValueType::INT);  //int maxhealth;
  }

  else if(strcmp(name.c_str(), "Light") == 0)
  {
    result.push_back(JSONReader::ValueType::NONE);
  }

  else if(strcmp(name.c_str(), "Mesh") == 0)
  {
    result.push_back(JSONReader::ValueType::String);
  }

  else if(strcmp(name.c_str(), "Parent") == 0)
    result.push_back(JSONReader::ValueType::NONE);

  else if(strcmp(name.c_str(), "ParticleEmitter") == 0)
  {
  }

  else if(strcmp(name.c_str(), "PowerupSpawner") == 0)
  {
    result.push_back(JSONReader::ValueType::String);    //string power;
    result.push_back(JSONReader::ValueType::Bool);   //bool alive;
    result.push_back(JSONReader::ValueType::Float);  //float cooldown;
    result.push_back(JSONReader::ValueType::Float);  //float value;
  }

  else if(strcmp(name.c_str(), "Player") == 0)
  {
    result.push_back(JSONReader::ValueType::INT);    //int? ControllerNum
    result.push_back(JSONReader::ValueType::Bool);   //bool alive
    result.push_back(JSONReader::ValueType::Bool);   //bool hasSecondart
    result.push_back(JSONReader::ValueType::Bool);   //bool hasMG
    result.push_back(JSONReader::ValueType::INT);    //unsigned score;
    result.push_back(JSONReader::ValueType::Float);  //float counter
    result.push_back(JSONReader::ValueType::Float);  //float reloadCounter
    result.push_back(JSONReader::ValueType::Float);  //float reloadSpeed
    result.push_back(JSONReader::ValueType::INT);    //KEYS up;
    result.push_back(JSONReader::ValueType::INT);    //KEYS down;
    result.push_back(JSONReader::ValueType::INT);    //KEYS left;
    result.push_back(JSONReader::ValueType::INT);    //KEYS right;
    result.push_back(JSONReader::ValueType::INT);    //KEYS shoot;
    result.push_back(JSONReader::ValueType::INT);    //KEYS shootSecondary;
    result.push_back(JSONReader::ValueType::INT);    //KEYS shootMG;
    result.push_back(JSONReader::ValueType::INT);    //KEYS turnLeft;
    result.push_back(JSONReader::ValueType::INT);    //KEYS turnRight;
    result.push_back(JSONReader::ValueType::INT);    //KEYS lightUp;
    result.push_back(JSONReader::ValueType::INT);    //KEYS lightDown;
  }

  else if(strcmp(name.c_str(), "RigidBody") == 0)
  {
    result.push_back(JSONReader::ValueType::Vector2D);   //Vector2D vel;
    result.push_back(JSONReader::ValueType::Vector2D);   //Vector2D direction;
    result.push_back(JSONReader::ValueType::Float);      //float speed;
    result.push_back(JSONReader::ValueType::Bool);       //bool isStatic;
    result.push_back(JSONReader::ValueType::Bool);       //bool rotationLocked;
    result.push_back(JSONReader::ValueType::Float);      //float density;
    result.push_back(JSONReader::ValueType::Float);      //float area;
    result.push_back(JSONReader::ValueType::Bool);       //bool ghost;
  }     

  else if(strcmp(name.c_str(), "Score") == 0)
  {
    result.push_back(JSONReader::ValueType::INT);
  }

  else if(strcmp(name.c_str(), "Sprite") == 0)
  {
    result.push_back(JSONReader::ValueType::String);
    //result.push_back(JSONReader::ValueType::Bool);
    //result.push_back(JSONReader::ValueType::Bool);
    //result.push_back(JSONReader::ValueType::Bool);
    //result.push_back(JSONReader::ValueType::Float);
    //result.push_back(JSONReader::ValueType::INT);
    //result.push_back(JSONReader::ValueType::INT);
  }

  else if(strcmp(name.c_str(), "SpriteText") == 0)
  {
  }

  else if(strcmp(name.c_str(), "Team") == 0)
  {
    result.push_back(JSONReader::ValueType::INT);
  }

  else if(strcmp(name.c_str(), "Transform") == 0)
  {
    result.push_back(JSONReader::ValueType::Vector2D);
    result.push_back(JSONReader::ValueType::Float);
    result.push_back(JSONReader::ValueType::Point2D);
    result.push_back(JSONReader::ValueType::Affine);
  }
 else if(strcmp(name.c_str(), "Turret") == 0)
 {
   result.push_back(JSONReader::ValueType::Float);
   result.push_back(JSONReader::ValueType::Float);
   result.push_back(JSONReader::ValueType::Bool);
   result.push_back(JSONReader::ValueType::Float);
 }
  else
    result.push_back(JSONReader::ValueType::NONE);

  current_components_type = result;
  ResetComp();
}



int CheckName(std::string name)
{

  if(strcmp(name.c_str(), "Audio") == 0)
  {
    return 1;
  }

  else if(strcmp(name.c_str(), "Base") == 0)
  {
    return 1;
  }

  else if(strcmp(name.c_str(), "Bullet") == 0)
  {
    return 1;
  }

  else if(strcmp(name.c_str(), "Drawable") == 0)
  {                                                 
    return 1;
  }

  else if(strcmp(name.c_str(), "Flag") == 0)
  {
    return 1;
  }

  else if(strcmp(name.c_str(), "Health") == 0)
  {
    return 1;
  }

  else if(strcmp(name.c_str(), "Mesh") == 0)
  {
    return 1;
  }

  else if(strcmp(name.c_str(), "ParticleEmitter") == 0)
  {
    return 1;
  }

  else if(strcmp(name.c_str(), "PowerupSpawner") == 0)
  {
    return 1;
  }

  else if(strcmp(name.c_str(), "Parent") == 0)
  {
    return 1;
  }

  else if(strcmp(name.c_str(), "Player") == 0)
  {
    return 1;
  }

  else if(strcmp(name.c_str(), "RigidBody") == 0)
  {
    return 1;
  }     

  else if(strcmp(name.c_str(), "Score") == 0)
  {
    return 1;
  }

  else if(strcmp(name.c_str(), "Sprite") == 0)
  {
    return 1;
  }

  else if(strcmp(name.c_str(), "SpriteText") == 0)
  {
    return 1;
  }

  else if(strcmp(name.c_str(), "Team") == 0)
  {
    return 1;
  }

  else if(strcmp(name.c_str(), "Transform") == 0)
  {
    return 1;
  }

  //else if(strcmp(name.c_str(), "Turret") == 0)
  //{
  //  return 1;
  //}

  return 0;
}


int JSONReader::GetCurrentComp()
{
  return comp_ref;
}

void JSONReader::UpdateComp()
{
  ++comp_ref;
}

void JSONReader::ResetComp()
{
  comp_ref = 0;
}


JSONReader::ValueType JSONReader::CheckType(const std::string check)
{
  ValueType result = ValueType::NONE;

  if(strcmp(check.c_str(), "Hcoords") == 0)
    result = ValueType::Hcoords;

  else if(strcmp(check.c_str(), "Point2D") == 0)
    result = ValueType::Point2D;

  else if(strcmp(check.c_str(), "Vector2D") == 0)
    result = ValueType::Vector2D;

  else if(strcmp(check.c_str(), "Affine") == 0)
    result = ValueType::Affine;

  else if(strcmp(check.c_str(), "map") == 0)
    result = ValueType::Map;

  else if(strcmp(check.c_str(), "Bool") == 0)
    result = ValueType::Bool;

  else if(strcmp(check.c_str(), "Float") == 0)
    result = ValueType::Float;

  else if(strcmp(check.c_str(), "String") == 0)
    result = ValueType::String;

  else if(strcmp(check.c_str(), "Int") == 0)
    result = ValueType::INT;

  return result; 
}



const std::string RemoveChars(std::string line)
{
  const char chars[] = {' ', '\t', '"', '\n'};

      // gets rid of any unwanted characters
  for(unsigned int i = 0; i < 4; ++i)
    line.erase(std::remove(line.begin(), line.end(), chars[i]), line.end());


  return line;
}


bool CheckParentFlag() { return Flags.parent_flag;}
bool CheckChildFlag() { return Flags.child_flag;}
bool CheckAnotherChild() {return Flags.more_children_flag;}

void SetParentFlagTrue() { Flags.parent_flag = true;}
void SetChildFlagTrue() { Flags.child_flag = true;}
void SetAnotherChildTrue() {Flags.more_children_flag = true;}

void SetParentFlagFalse() { Flags.parent_flag = false;}
void SetChildFlagFalse() { Flags.child_flag = false;}
void SetAnotherChildFalse() {Flags.more_children_flag = false;}



void AnotherChild() {++Flags.num_children;}
int GetChildNumber() {return Flags.num_children;}





Qualifier ParseCharacter(const char line, const char nextchar, std::string objectname)
{
  switch(line)
  {
    case '{':
      if(CheckParentFlag() == false)
        SetParentFlagTrue();
      closed = 0;
      return Qualifier::Continue;
  

    case '}':
      if(CheckChildFlag() == true)
      {
        SetChildFlagFalse();
        ++closed;
        return Qualifier::CloseChildObject;
      }
      else
        return Qualifier::Stop;
    break;

    case ':':
      if(CheckChildFlag() == false && nextchar == '{')
      {
        if(CheckAnotherChild() == true)
          AnotherChild();

        if(GetChildNumber() >= 1)
        {
          SetChildFlagTrue();
          return Qualifier::Object;
        }
        closed = 0;
        SetChildFlagTrue();
        SetAnotherChildTrue();
        return Qualifier::Object;
      }
      else if(CheckChildFlag() == true && nextchar == '{')
      {
        closed = 0;
        return Qualifier::Object;
      }
      else
      {
        closed = 0;
        return Qualifier::Value;
      }
    break;

    default:
      return Qualifier::Continue;
  }
}
/*
  if(CheckChildFlag() == false && nextchar == '{')
  {
    if(CheckAnotherChild() == true)
      AnotherChild();

    if(GetChildNumber() >= 1)
    {
      SetChildFlagTrue();
      return Qualifier::Object;
    }

    SetChildFlagTrue();
    SetAnotherChildTrue();
    return Qualifier::Object;
  }
*/

const std::string Extract_Value(const std::string value, unsigned int& pos)
{
  std::string result;
  bool hcoords_point_flag = false;

  for(pos; pos < value.length(); ++pos)
    result.push_back(value[pos]);

  return result;
}


void JSONReader::Set_Value(Array& arr, JSONReader::ValueType Valtype, const std::string value)
{
  switch(Valtype)
  {
    case JSONReader::ValueType::Poly:
    {
      //arr.push_back((int)stoi(value));
      //
      //for(int i = 0; i < arr.begin()[0].GetInt(); ++i)
      //  current_components_type.push_back(JSONReader::ValueType::Point2D);
      //
      //current_components_type.push_back(JSONReader::ValueType::String);
      break;
    }

    case JSONReader::ValueType::INT:
    {
      arr.push_back((int)stoi(value));
      break;
    }

    case JSONReader::ValueType::Hcoords:
    {
      arr.push_back(string_to_Hcoords(value));
      break;
    }

    case JSONReader::ValueType::Point2D:
    {
      Math::Point2D result = string_to_Point2D(value);
      ValType result1(result);
      arr.push_back(result);
      break;
    }

    case JSONReader::ValueType::Vector2D:
    {
      arr.push_back(string_to_Vector2D(value));
      break;
    }

    case JSONReader::ValueType::Affine:
    {
      arr.push_back(string_to_Affine(value));
      break;
    }

    case JSONReader::ValueType::Map:
    {
      arr.push_back(string_to_Map_arr(value));
      // give this to connor
      // _rawMap = string_to_Map_vector(value);
      break;
    }

    case JSONReader::ValueType::Bool:
    {
      if(value == "true")
        arr.push_back(true);
      else
        arr.push_back(false);
      break;
    }

    case JSONReader::ValueType::Float:
    {
      float f = (float)stof(value);
      arr.push_back(f);
      break;
    }

    case JSONReader::ValueType::String:
    {
      arr.push_back(value);
      break;
    }
  }

}



JSONReader::JSONReader()
{
  comp_ref = 0;
}

JSONReader::~JSONReader()
{
}

bool JSONReader::JSONLoad(const char *filepath)
{
  bool parent_object = false;
  bool comp_flag = false;

  myfile.open(filepath);
  if(myfile.is_open())
  {  
    Array values;
    std::string object, real_name;
    DynamicElement component;

    while(!myfile.eof())
    {
      std::string currentline, objectname;
      getline(myfile, currentline);
      currentline = RemoveChars(currentline);
      DynamicElement child;
      for(unsigned int i = 0; i < currentline.length(); ++i)
      {
        objectname.push_back(currentline[i]);
        Qualifier type = ParseCharacter(objectname[i], currentline[i+1], objectname);

        if(current_components_type.empty() || current_components_type[0] == ValueType::NONE)
          Component_LookUp_Table(objectname);

        if(type  == Qualifier::Object)
        {
          if(check_name == false)
          {
            objectname.pop_back(); // object name was found
            real_name = objectname;
            check_name = true;
          }
          else
          {
            objectname.pop_back();
            object = objectname;
          }

          values.clear();
        }
        if(type == Qualifier::Value)
        {
          std::string value = Extract_Value(currentline, i += 1); // gets the value after the : as a string
          objectname.pop_back();

          if(!CheckName(objectname))
          {
            // if its not a component type probably not ever going to be called unless someone made a .json file incorrectly
            if(current_components_type[0] == JSONReader::ValueType::NONE)
            {
              current_components_type.push_back(CheckType(objectname));

              if(comp_flag == false)
                UpdateComp();
              comp_flag = true;
            }

            Set_Value(values, current_components_type[GetCurrentComp()], value);
            UpdateComp();
          }
        }
        if(type == Qualifier::CloseChildObject)
        {
          current_components_type.clear();
          
          component[object.c_str()] = values;
         
        }
        if(type == Qualifier::Stop)
        {
          parent[real_name.c_str()] = component; 
          component.clear();
          check_name = false;
           
        }
      }
      //parent[objectname.c_str()] = child;
    }
  }
  else
  {
    std::cout << "COULD NOT OPEN JSON FILE\n";
    assert("COULD NOT OPEN JSON FILE");
    return false;
  }

  myfile.close();
  return true;
}





std::vector<Entity *> JSONReader::SerializeParent(void)
{
  std::vector<Entity *> entities;
  std::vector<Component *> components;
  int k = 0;

  //std::vector<std::string> dont_destroy;
  //dont_destroy.push_back(std::string("DigiPen_Splash"));
  //dont_destroy.push_back(std::string("Blue Player"));
  //dont_destroy.push_back(std::string("HUD"));
  //dont_destroy.push_back(std::string("turret0"));
  //dont_destroy.push_back(std::string("turret1"));
  //dont_destroy.push_back(std::string("turret2"));
  //dont_destroy.push_back(std::string("turret3"));
  //dont_destroy.push_back(std::string("turret4"));
  //dont_destroy.push_back(std::string("Credits"));
  //dont_destroy.push_back(std::string("HowToPlayScreen"));
  //dont_destroy.push_back(std::string("Red Player"));
  //dont_destroy.push_back(std::string("QuitMenu"));
  //
  //GridMap* GS = ((GridMap*)ENGINE->GetSystem(sys_GridMap));
  //for(auto &it : GS->_map)
  //{
  //  for(auto &it2 : it)
  //  {
  //    it2.collision = false;
  //  }
  //}
  //ENGINE->OM->DestroyAll(ENGINE->OM->GetEntitiesWithoutName(dont_destroy));

  for(auto it : parent)
  {
    if(it.first == "BlueFlag" || it.first == "RedFlag" || it.first == "NeutralFlag" 
        || it.first == "Flag" || it.first == "default" || it.first == "BluePlayer"
        || it.first == "DigiPen_Splash" || it.first == "Pause_Menu" || it.first == "turret0" || it.first.empty() == true
        || it.first == "turret1" || it.first == "turret2" || it.first == "turret3" || it.first == "turret4" || it.first == "Credits"
        || it.first == "HUD" || it.first == "HowToPlayScreen" || it.first == "RedPlayer" || it.first == "QuitMenu")
    {
      continue; 
    }

    for(auto id : it.second.GetDynamicElement())
    {
      if(strcmp(id.first.c_str(), "Audio") == 0)
      {
      }

      else if(strcmp(id.first.c_str(), "Base") == 0)
      {
        Base* base = new Base();
        components.push_back(base);
      }

      else if(strcmp(id.first.c_str(), "Bullet") == 0)
      {
      }

      else if(strcmp(id.first.c_str(), "Drawable") == 0)
      {
        Drawable* player = new Drawable();
		    player->visible = id.second.GetArray()[0];
		    player->useSprite = id.second.GetArray()[1];
		    player->drawFromCenter = id.second.GetArray()[2];
		    player->startPointColor = id.second.GetArray()[3];
		    player->r = id.second.GetArray()[4];
        player->g = id.second.GetArray()[5];
        player->b = id.second.GetArray()[6];
        player->a = id.second.GetArray()[7];
        player->beginR = id.second.GetArray()[8];
        player->beginG = id.second.GetArray()[9];
        player->beginB = id.second.GetArray()[10];
        player->beginA = id.second.GetArray()[11];
        components.push_back(player);
      }
/*
      else if(strcmp(id.first.c_str(), "Flag") == 0)
      {
        Flag* flag = new Flag();
        components.push_back(flag);
      }
      */
      else if(strcmp(id.first.c_str(), "Health") == 0)
      {
        Health * hp = new Health();
        hp->health = id.second.GetArray()[0];
        hp->maxHealth = id.second.GetArray()[1];
        components.push_back(hp);
      }

      else if(strcmp(id.first.c_str(), "Light") == 0)
      {
        Light* light = new Light(100.0f);
        components.push_back(light);
      }

      //else if(strcmp(id.first.c_str(), "Mesh") == 0)
      //{
      //  Mesh* mesh = new Mesh();
      //  
      //  mesh = Mesh::Get_Mesh(id.second.GetArray()[0]);
      //  components.push_back(mesh);

      //}

      else if(strcmp(id.first.c_str(), "ParticleEmitter") == 0)
      {

      }

      else if(strcmp(id.first.c_str(), "PowerupSpawner") == 0)
      {
        //PowerupSpawner *powerup = new PowerupSpawner();
        //
        //powerup->power = id.second.GetArray()[0];
        //powerup->hasCooldown = id.second.GetArray()[1];
        //powerup->cooldown = id.second.GetArray()[2];
        //powerup->value = id.second.GetArray()[3];
        //components.push_back(powerup);
      }

      else if(strcmp(id.first.c_str(), "Parent") == 0)
      {
       Parent *parent = new Parent();
       components.push_back(parent);
      }

      else if(strcmp(id.first.c_str(), "Player") == 0)
      {
        Player* player = new Player();
//        player->ControllerNum = (JOYSTICKS)id.second.GetArray()[0];
		    //player->alive = id.second.GetArray()[1];
		    //player->hasSecondary = id.second.GetArray()[2];
		    //player->hasMG = id.second.GetArray()[3];
		    //player->score = 0;
		    //player->counter = id.second.GetArray()[5];
        player->reloadCounter = id.second.GetArray()[6];
        player->reloadSpeed = id.second.GetArray()[7];

        //player->up = id.second.GetArray()[8];               //KEYS up;
        //player->down = id.second.GetArray()[9];             //KEYS down;
        //player->left = id.second.GetArray()[10];            //KEYS left;
        //player->right = id.second.GetArray()[11];           //KEYS right;
        //player->shoot = id.second.GetArray()[12];           //KEYS shoot;
        //player->shootSecondary = id.second.GetArray()[13];  //KEYS shootSecondary;
        //player->shootMG = id.second.GetArray()[14];         //KEYS shootMG;
        //player->turnLeft = id.second.GetArray()[15];        //KEYS turnLeft;
        //player->turnRight = id.second.GetArray()[16];       //KEYS turnRight;
        //player->lightUp = id.second.GetArray()[17];         //KEYS lightUp;
        //player->lightDown = id.second.GetArray()[18];       //KEYS lightDown;

        components.push_back(player);
      }

      else if(strcmp(id.first.c_str(), "RigidBody") == 0)
      {
        RigidBody* rigid      = new RigidBody();            //
        rigid->vel            = id.second.GetArray()[0];    //
        rigid->direction      = id.second.GetArray()[1];    //
        rigid->speed          = id.second.GetArray()[2];    //
        rigid->isStatic       = id.second.GetArray()[3];    //
        rigid->rotationLocked = id.second.GetArray()[4];    //
        rigid->density        = id.second.GetArray()[5];    //
        rigid->area           = id.second.GetArray()[6];    //
        //rigid->ghost          = id.second.GetArray()[7];    //
        components.push_back(rigid);
      }     

      else if(strcmp(id.first.c_str(), "Score") == 0)
      {
        Score* score = new Score();
        score->score = (int)id.second.GetArray()[0];
        components.push_back(score);
      }

      else if(strcmp(id.first.c_str(), "Sprite") == 0)
      {
        Sprite* sprite = new Sprite();

        sprite->ChangeTexture(id.second.GetArray()[0]);
        //sprite->flipX = id.second.GetArray()[0];
        //sprite->flipY = id.second.GetArray()[1];
        //sprite->animationActive = id.second.GetArray()[2];
        //sprite->animationSpeed = id.second.GetArray()[3];
        //sprite->startFrame = (int)id.second.GetArray()[4];
        //sprite->currentFrame = (int)id.second.GetArray()[5];
        components.push_back(sprite);
      }

      else if(strcmp(id.first.c_str(), "SpriteText") == 0)
      {
      }

      else if(strcmp(id.first.c_str(), "Team") == 0)
      {
        Team* team = new Team();
        // team->teamID = (Teams)id.second.GetArray()[0];
        components.push_back(team);
      }

      else if(strcmp(id.first.c_str(), "Transform") == 0)
      {
        Transform* trans = new Transform();
        trans->scale = id.second.GetArray()[0];
        trans->rotation =  id.second.GetArray()[1];
        trans->Position =  id.second.GetArray()[2];
        trans->transform = id.second.GetArray()[3];

        if(it.first.compare(0,4,"Wall") == 0)
        {
          //ridMap* GS = ((GridMap*)ENGINE->GetSystem(sys_GridMap));
          //S->GetPlot(trans->Position)->collision = true;
        }
        components.push_back(trans);
      }
      else
        continue;
    }

    entities.push_back(ENGINE->Factory->Compose(components));
    components.clear();
    entities[k++]->SetName(std::string(it.first));
    
  }
  return entities;
}

DynamicElement const* JSONReader::GetRoot(void)
{
  return &parent;
}

