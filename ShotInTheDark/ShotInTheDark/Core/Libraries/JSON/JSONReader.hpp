//	File name:		JSONReader.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Kyle Philistine
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "JSonParser.hpp"
#include <fstream>


using namespace JSON;

class JSONReader
{
  public:

    enum ValueType
    {
      Poly,
      INT,
      Hcoords,
      Point2D,
      Vector2D,
      Affine,
      Map,
      Bool,
      Float,
      String,
      NONE
    };
    ValueType CheckType(std::string check);
    void Set_Value(Array& arr, JSONReader::ValueType Valtype, std::string value);
    void JSONReader::Component_LookUp_Table(std::string name);
    void SetObject(std::string name, ValueType, Array& childrenlist, int child_num);
    JSONReader(); // creates the parent
    ~JSONReader(); // deletes the parent
	  bool JSONLoad(const char *filepath = 0);	//open the file, load a JSON file into a DynamicElement structure, close the file
	  DynamicElement const * GetRoot(void);		//return the root of the DynamicElement structure that the JSON file was loaded into
    std::vector<Entity*> SerializeParent(void);

    int JSONReader::GetCurrentComp();
    void JSONReader::UpdateComp();
    void JSONReader::ResetComp();

  private:
    std::ifstream myfile;
    DynamicElement parent;
    std::vector<JSONReader::ValueType> current_components_type;
    int comp_ref;
};

std::string SetLevel(const int level);