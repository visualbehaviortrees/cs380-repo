//	File name:		JSONSaver.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Kyle Philistine
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include <assert.h>
#include "JSONSaver.hpp"
#include "JSonParser.hpp"
#include "../Math/Math2D.hpp"
#include "../../Components/Components.h"
#include "Engine.hpp"


using namespace Math;


JSONSaver::JSONSaver()
{
  object.first = false;
  object.second = false;
}

bool JSONSaver::LoadFile(const char *filepath)
{
  jsonfile.open(filepath);

  if(jsonfile.is_open())
    return true;

  assert("!!JSON FILE COULD NOT BE OPENED / WAS NOT FOUND!!");
  return false;
}

void JSONSaver::CloseFile(void)
{
  jsonfile.close();
  object.first = false;
  object.second = false;
}

void JSONSaver::ReadObject(const char *name)
{
  if(name == 0 && object.first == false)
  {
    jsonfile << "{\n";
    num_into.push_back(1);
    object = make_pair(true, false);
  }
  else if(object.first == true && name == 0)
  {
    for(unsigned int i = 0; i < num_into.size(); ++i)
      jsonfile << "\t";

    num_into.push_back(1);
    jsonfile << "{\n";
    object.second = false;
  }
  else
  {
    for(unsigned int i = 0; i < num_into.size(); ++i)
      jsonfile << "\t";

    num_into.push_back(1);
    jsonfile << "\"" << name << "\":" << " {\n";
    object.second = false;
  }
}


void JSONSaver::EndObject(void)
{
  if(num_into.size() != 0)
    num_into.pop_back();
    
  for(unsigned int i = 0; i < num_into.size(); ++i)
    jsonfile << "\t";

  jsonfile << "}\n";
  object.second = true;
}


const std::string ArchetypeSaver(std::string filename, Entity *entity)
{
  mask entity_mask = entity->Mask();
  JSONSaver saver;

  if(!entity_mask)
    return NULL;


  saver.LoadFile(filename.c_str());

  while (filename[filename.size() - 1] != '.')
  {
    if (filename.empty())
      assert("THIS IS NOT A FILE! / NO EXTENSION GIVEN!");

    filename.pop_back();
  }
  filename.pop_back();


  saver.ReadObject(filename.c_str());
  {
    // power ups
    //if((entity_mask & Power) == MC_Transform)
    //{
    //  saver.ReadObject("Transform");
    //  {
    //    saver.Field("scale", (entity)->GET_COMPONENT(Transform)->scale);
    //    saver.Field("rotation", (entity)->GET_COMPONENT(Transform)->rotation);
    //    saver.Field("Position", (entity)->GET_COMPONENT(Transform)->Position);
    //    saver.Field("transform", (entity)->GET_COMPONENT(Transform)->transform);
    //  }
    //  saver.EndObject();
    //}


    // audio
    //if((entity_mask & MC_Audio) == MC_Audio)
    //{
    //  saver.ReadObject("Audio");
    //  {
    //    saver.Field("scale", (entity)->GET_COMPONENT(Audio)->audioFlags);
    //  }
    //  saver.EndObject();
    //}

    if((entity_mask & MC_Bullet) == MC_Bullet)
    {
      saver.ReadObject("Bullet");
      {
        saver.Field("damage", (entity)->GET_COMPONENT(Bullet)->damage);
        saver.Field("hitSound", (entity)->GET_COMPONENT(Bullet)->hitSound);
        saver.Field("lifetime", (entity)->GET_COMPONENT(Bullet)->lifetime);
      }
      saver.EndObject();
    }


    if((entity_mask & MC_Drawable) == MC_Drawable)
    {
      saver.ReadObject("Drawable");
      {
        saver.Field("visible", (entity)->GET_COMPONENT(Drawable)->visible);
        saver.Field("useSprite", (entity)->GET_COMPONENT(Drawable)->useSprite);
        saver.Field("drawFromCenter", (entity)->GET_COMPONENT(Drawable)->drawFromCenter);
        saver.Field("startPointColor", (entity)->GET_COMPONENT(Drawable)->startPointColor);
        saver.Field("r", (entity)->GET_COMPONENT(Drawable)->r);
        saver.Field("g", (entity)->GET_COMPONENT(Drawable)->g);
        saver.Field("b", (entity)->GET_COMPONENT(Drawable)->b);
        saver.Field("a", (entity)->GET_COMPONENT(Drawable)->a);
        saver.Field("beginR", (entity)->GET_COMPONENT(Drawable)->beginR);
        saver.Field("beginG", (entity)->GET_COMPONENT(Drawable)->beginG);
        saver.Field("beginB", (entity)->GET_COMPONENT(Drawable)->beginB);
        saver.Field("beginA", (entity)->GET_COMPONENT(Drawable)->beginA);
        saver.Field("layer", (entity)->GET_COMPONENT(Drawable)->layer);
      }
      saver.EndObject();
    }

    if((entity_mask & MC_Health) == MC_Health)
    {
      saver.ReadObject("Health");
      {
        saver.Field("health", (entity)->GET_COMPONENT(Health)->health);
        saver.Field("maxHealth", (entity)->GET_COMPONENT(Health)->maxHealth);
      }
      saver.EndObject();
    }

  /*  if((entity_mask & MC_Mesh) == MC_Mesh)
    {
      saver.ReadObject("Mesh");
      {
        for(unsigned i = 0; i < entity->GET_COMPONENT(Mesh)->Poly.GetVertNum(); ++i)
        {
          std::string vertex;
          vertex = "vertice";
          vertex.append(to_string(i));
          saver.Field(vertex.c_str(), entity->GET_COMPONENT(Mesh)->Poly.GetVert(i));
        }
      }
      saver.EndObject();
    } Meshes are no longer components
*/
    if((entity_mask & MC_Player) == MC_Player)
    {
      saver.ReadObject("Player");
      {
        //saver.Field("ControllerNum", entity->GET_COMPONENT(Player)->ControllerNum);
        //saver.Field("alive", entity->GET_COMPONENT(Player)->alive);
        //saver.Field("score", entity->GET_COMPONENT(Player)->score);
        //saver.Field("counter", entity->GET_COMPONENT(Player)->counter);
        saver.Field("reloadCounter", entity->GET_COMPONENT(Player)->reloadCounter);
        saver.Field("reloadSpeed", entity->GET_COMPONENT(Player)->reloadSpeed);
      }
      saver.EndObject();
    }

    // PathFinding
    //
    //

    if((entity_mask & MC_RigidBody) == MC_RigidBody)
    {
      saver.ReadObject("RigidBody");
      {
        saver.Field("vel", (entity)->GET_COMPONENT(RigidBody)->vel);
        saver.Field("direction", (entity)->GET_COMPONENT(RigidBody)->direction);
        saver.Field("speed", (entity)->GET_COMPONENT(RigidBody)->speed);
        saver.Field("isStatic", (entity)->GET_COMPONENT(RigidBody)->isStatic);
        saver.Field("rotationLocked", (entity)->GET_COMPONENT(RigidBody)->rotationLocked);
        saver.Field("density", (entity)->GET_COMPONENT(RigidBody)->density);
        saver.Field("area", (entity)->GET_COMPONENT(RigidBody)->area);
      }
      saver.EndObject();
    }

    if((entity_mask & MC_Score) == MC_Score)
    {
      saver.ReadObject("Score");
      {
        saver.Field("score", (entity)->GET_COMPONENT(Score)->score);
      }
      saver.EndObject();
    }


    if((entity_mask & MC_Sprite) == MC_Sprite)
    {
      saver.ReadObject("Sprite");
      {
        saver.Field("mpTextureData", (entity)->GET_COMPONENT(Sprite)->mpTextureData->Name);
        //saver.Field("mpTextureData" (entity)->GET_COMPONENT(Sprite))
        //saver.Field("flipX", (entity)->GET_COMPONENT(Sprite)->flipX);
        //saver.Field("flipY", (entity)->GET_COMPONENT(Sprite)->flipY);
        //saver.Field("animationActive", (entity)->GET_COMPONENT(Sprite)->animationActive);
        //saver.Field("animationSpeed", (entity)->GET_COMPONENT(Sprite)->animationSpeed);
        //saver.Field("startFrame", (entity)->GET_COMPONENT(Sprite)->startFrame);
        //saver.Field("currentFrame", (entity)->GET_COMPONENT(Sprite)->currentFrame);
      }
      saver.EndObject();
    }

    if((entity_mask & MC_SpriteText) == MC_SpriteText)
    {
      saver.ReadObject("SpriteText");
      {
        saver.Field("boxPos", (entity)->GET_COMPONENT(SpriteText)->boxPos);
        saver.Field("hAlign", (entity)->GET_COMPONENT(SpriteText)->hAlign);
        saver.Field("vAlign", (entity)->GET_COMPONENT(SpriteText)->vAlign);
        saver.Field("fontSize", (entity)->GET_COMPONENT(SpriteText)->fontSize);
        saver.Field("text", (entity)->GET_COMPONENT(SpriteText)->text.c_str());
      }
      saver.EndObject();
    }
 
    if((entity_mask & MC_Team) == MC_Team)
    {
      saver.ReadObject("Team");
      {
        saver.Field("teamID", (entity)->GET_COMPONENT(Team)->teamID);
      }
      saver.EndObject();
    }


    if((entity_mask & MC_Transform) == MC_Transform)
    {
      saver.ReadObject("Transform");
      {
        saver.Field("scale", (entity)->GET_COMPONENT(Transform)->scale);
        saver.Field("rotation", (entity)->GET_COMPONENT(Transform)->rotation);
        saver.Field("Position", (entity)->GET_COMPONENT(Transform)->Position);
        saver.Field("transform", (entity)->GET_COMPONENT(Transform)->transform);
      }
      saver.EndObject();
    }

    /*
    if((entity_mask & MC_Turret) == MC_Turret)
    {
      saver.ReadObject("Turret");
      {
        saver.Field("timeLastShot", (entity)->GET_COMPONENT(Turret)->timeLastShot);
      }
      saver.EndObject();
    }
    */
    if ((entity_mask & MC_Light) == MC_Light)
    {
      saver.ReadObject("Light");
      {
        saver.Field("intensity", (entity)->GET_COMPONENT(Light)->intensity);
      }
      saver.EndObject();
    }
  }
  saver.EndObject();
  saver.CloseFile();
  return filename;
}

const std::string SaveAllEntities(const std::string filename)
{
  JSONSaver saver;
  saver.LoadFile(filename.c_str());
  for(unsigned i = 0; i < ENGINE->OM->EntityCount(); ++i)
  {
    Entity* entity = ENGINE->OM->GetEntity(i);
    mask entity_mask = ENGINE->OM->GetEntity(i)->Mask();

    if(entity->GetName() == "Speed powerup instance")
      continue;

    saver.ReadObject(entity->GetName().c_str());

    if((entity_mask & MC_Base) == MC_Base)
    {
      saver.ReadObject("Base");
      {
      }
      saver.EndObject();
    }

    if((entity_mask & MC_Bullet) == MC_Bullet)
    {
      saver.ReadObject("Bullet");
      {
        saver.Field("damage", (entity)->GET_COMPONENT(Bullet)->damage);
        saver.Field("hitSound", (entity)->GET_COMPONENT(Bullet)->hitSound);
        saver.Field("lifetime", (entity)->GET_COMPONENT(Bullet)->lifetime);
      }
      saver.EndObject();
    }


    if((entity_mask & MC_Drawable) == MC_Drawable)
    {
      saver.ReadObject("Drawable");
      {
        saver.Field("visible", (entity)->GET_COMPONENT(Drawable)->visible);
        saver.Field("useSprite", (entity)->GET_COMPONENT(Drawable)->useSprite);
        saver.Field("drawFromCenter", (entity)->GET_COMPONENT(Drawable)->drawFromCenter);
        saver.Field("startPointColor", (entity)->GET_COMPONENT(Drawable)->startPointColor);
        saver.Field("r", (entity)->GET_COMPONENT(Drawable)->r);
        saver.Field("g", (entity)->GET_COMPONENT(Drawable)->g);
        saver.Field("b", (entity)->GET_COMPONENT(Drawable)->b);
        saver.Field("a", (entity)->GET_COMPONENT(Drawable)->a);
        saver.Field("beginR", (entity)->GET_COMPONENT(Drawable)->beginR);
        saver.Field("beginG", (entity)->GET_COMPONENT(Drawable)->beginG);
        saver.Field("beginB", (entity)->GET_COMPONENT(Drawable)->beginB);
        saver.Field("beginA", (entity)->GET_COMPONENT(Drawable)->beginA);
        saver.Field("layer", (entity)->GET_COMPONENT(Drawable)->layer);
      }
      saver.EndObject();
    }

    if((entity_mask & MC_Flag) == MC_Flag)
    {
      saver.ReadObject("Flag");
      {
      }
      saver.EndObject();
    }

    if((entity_mask & MC_Health) == MC_Health)
    {
      saver.ReadObject("Health");
      {
        saver.Field("health", (entity)->GET_COMPONENT(Health)->health);
        saver.Field("maxHealth", (entity)->GET_COMPONENT(Health)->maxHealth);
      }
      saver.EndObject();
    }

    if((entity_mask & MC_Light) == MC_Light)
    {
      saver.ReadObject("Light");
      {
      }
      saver.EndObject();
    }

    //if((entity_mask & MC_Mesh) == MC_Mesh)
    //{
    //  saver.ReadObject("Mesh");
    //  {
    //    saver.Field("meshName", entity->GET_COMPONENT(Mesh)->meshName);
    //  }
    //  saver.EndObject();
    //}
    if((entity_mask & MC_Parent) == MC_Parent)
    {
      saver.ReadObject("Parent");
      {
      }
      saver.EndObject();
    }

    if((entity_mask & MC_Player) == MC_Player)
    {
      saver.ReadObject("Player");
      {
        //saver.Field("ControllerNum", entity->GET_COMPONENT(Player)->ControllerNum);
        //saver.Field("alive", entity->GET_COMPONENT(Player)->alive);
        //saver.Field("hasSecondary", entity->GET_COMPONENT(Player)->hasSecondary);
        //saver.Field("hasMG", entity->GET_COMPONENT(Player)->hasMG);
        //saver.Field("score", entity->GET_COMPONENT(Player)->score);
        //saver.Field("counter", entity->GET_COMPONENT(Player)->counter);
        saver.Field("reloadCounter", entity->GET_COMPONENT(Player)->reloadCounter);
        saver.Field("reloadSpeed", entity->GET_COMPONENT(Player)->reloadSpeed);
        //saver.Field("up", entity->GET_COMPONENT(Player)->up);    //KEYS up;
        //saver.Field("down", entity->GET_COMPONENT(Player)->down);   //KEYS down;
        //saver.Field("left", entity->GET_COMPONENT(Player)->left);   //KEYS left;
        //saver.Field("right", entity->GET_COMPONENT(Player)->right);    //KEYS right;
        //saver.Field("shoot", entity->GET_COMPONENT(Player)->shoot);    //KEYS shoot;
        //saver.Field("shootSecondary", entity->GET_COMPONENT(Player)->shootSecondary);   //KEYS shootSecondary;
        //saver.Field("shootMG", entity->GET_COMPONENT(Player)->shootMG);     //KEYS shootMG;
        //saver.Field("turnLeft", entity->GET_COMPONENT(Player)->turnLeft);    //KEYS turnLeft;
        //saver.Field("turnRight", entity->GET_COMPONENT(Player)->turnRight);     //KEYS turnRight;
        //saver.Field("lightUp", entity->GET_COMPONENT(Player)->lightUp);     //KEYS lightUp;
        //saver.Field("lightDown", entity->GET_COMPONENT(Player)->lightDown);     //KEYS lightDown;
      }
      saver.EndObject();
    }
    if((entity_mask & MC_PowerupSpawner) == MC_PowerupSpawner)
    {
      saver.ReadObject("PowerupSpawner");
      {
        //saver.Field("power", entity->GET_COMPONENT(PowerupSpawner)->power);
        //saver.Field("hasCooldown", entity->GET_COMPONENT(PowerupSpawner)->hasCooldown);
        //saver.Field("cooldown", entity->GET_COMPONENT(PowerupSpawner)->cooldown);
        //saver.Field("value", entity->GET_COMPONENT(PowerupSpawner)->value);
      }
      saver.EndObject();
    }
    // PathFinding
    //
    //

    if((entity_mask & MC_RigidBody) == MC_RigidBody)
    {
      saver.ReadObject("RigidBody");
      {
        saver.Field("vel", (entity)->GET_COMPONENT(RigidBody)->vel);
        saver.Field("direction", (entity)->GET_COMPONENT(RigidBody)->direction);
        saver.Field("speed", (entity)->GET_COMPONENT(RigidBody)->speed);
        saver.Field("isStatic", (entity)->GET_COMPONENT(RigidBody)->isStatic);
        saver.Field("rotationLocked", (entity)->GET_COMPONENT(RigidBody)->rotationLocked);
        saver.Field("density", (entity)->GET_COMPONENT(RigidBody)->density);
        saver.Field("area", (entity)->GET_COMPONENT(RigidBody)->area);
        saver.Field("ghost", (entity)->GET_COMPONENT(RigidBody)->ghost);
      }
      saver.EndObject();
    }

    if((entity_mask & MC_Score) == MC_Score)
    {
      saver.ReadObject("Score");
      {
        saver.Field("score", (entity)->GET_COMPONENT(Score)->score);
      }
      saver.EndObject();
    }


    if((entity_mask & MC_Sprite) == MC_Sprite)
    {
      saver.ReadObject("Sprite");
      {
        saver.Field("flipX", (entity)->GET_COMPONENT(Sprite)->flipX);
        saver.Field("flipY", (entity)->GET_COMPONENT(Sprite)->flipY);
        saver.Field("animationActive", (entity)->GET_COMPONENT(Sprite)->animationActive);
        saver.Field("animationSpeed", (entity)->GET_COMPONENT(Sprite)->animationSpeed);
        saver.Field("startFrame", (entity)->GET_COMPONENT(Sprite)->startFrame);
        saver.Field("currentFrame", (entity)->GET_COMPONENT(Sprite)->currentFrame);
      }
      saver.EndObject();
    }

    if((entity_mask & MC_SpriteText) == MC_SpriteText)
    {
      saver.ReadObject("SpriteText");
      {
        saver.Field("boxPos", (entity)->GET_COMPONENT(SpriteText)->boxPos);
        saver.Field("hAlign", (entity)->GET_COMPONENT(SpriteText)->hAlign);
        saver.Field("vAlign", (entity)->GET_COMPONENT(SpriteText)->vAlign);
        saver.Field("fontSize", (entity)->GET_COMPONENT(SpriteText)->fontSize);
        saver.Field("text", (entity)->GET_COMPONENT(SpriteText)->text.c_str());
      }
      saver.EndObject();
    }
 
    if((entity_mask & MC_Team) == MC_Team)
    {
      saver.ReadObject("Team");
      {
        saver.Field("teamID", (entity)->GET_COMPONENT(Team)->teamID);
      }
      saver.EndObject();
    }


    if((entity_mask & MC_Transform) == MC_Transform)
    {
      saver.ReadObject("Transform");
      {
        saver.Field("scale", (entity)->GET_COMPONENT(Transform)->scale);
        saver.Field("rotation", (entity)->GET_COMPONENT(Transform)->rotation);
        saver.Field("Position", (entity)->GET_COMPONENT(Transform)->Position);
        saver.Field("transform", (entity)->GET_COMPONENT(Transform)->transform);
      }
      saver.EndObject();
    }

    /*
    if((entity_mask & MC_Turret) == MC_Turret)
    {
      saver.ReadObject("Turret");
      {
        saver.Field("timeLastShot", (entity)->GET_COMPONENT(Turret)->timeLastShot);
        saver.Field("timeStartTracking", (entity)->GET_COMPONENT(Turret)->timeStartTracking);
        saver.Field("tracking", (entity)->GET_COMPONENT(Turret)->tracking);
        saver.Field("timeDead", (entity)->GET_COMPONENT(Turret)->timeDead);
      }
      saver.EndObject();
    }
    */
    saver.EndObject();
     
  }
  saver.CloseFile();

  return filename;
}


void JSONSaver::Field(const char *name, std::string value)
{
  for(unsigned int i = 0; i < num_into.size(); ++i)
    jsonfile << "\t";
  jsonfile << "\"" << name << "\": " << "\""<< value << "\"" << "\n";
}

void JSONSaver::Field(const char *name, int value)
{
  for(unsigned int i = 0; i < num_into.size(); ++i)
    jsonfile << "\t";
  jsonfile << "\"" << name << "\": " << value << "\n";
}

void JSONSaver::Field(const char *name, unsigned int value)
{
  for(unsigned int i = 0; i < num_into.size(); ++i)
    jsonfile << "\t";
  jsonfile << "\"" << name << "\": " << value << "\n";
}

void JSONSaver::Field(const char *name, float value)
{
  for(unsigned int i = 0; i < num_into.size(); ++i)
    jsonfile << "\t";
  jsonfile << "\"" << name << "\": " << std::fixed << value << "\n";
}

void JSONSaver::Field(const char *name, double value)
{
  for(unsigned int i = 0; i < num_into.size(); ++i)
    jsonfile << "\t";
  jsonfile << "\"" << name << "\": " << std::fixed << value << "\n";
}

void JSONSaver::Field(const char *name, bool value)
{
  for(unsigned int i = 0; i < num_into.size(); ++i)
    jsonfile << "\t";

  if(value)
    jsonfile << "\"" << name << "\": " << "true" << "\n";
  else
    jsonfile << "\"" << name << "\": " << "false" << "\n";
}

void JSONSaver::Field(const char *name, const char *value)
{
  for(unsigned int i = 0; i < num_into.size(); ++i)
    jsonfile << "\t";

  jsonfile << "\"" << name << "\": " << "\"" << value << "\"" << "\n";
}

void JSONSaver::Field(const char *name, const Point2D value)
{
  for(unsigned int i = 0; i < num_into.size(); ++i)
    jsonfile << "\t";

  jsonfile << "\"" << name << "\": " << value << "\n";
}

void JSONSaver::Field(const char *name, const Vector2D value)
{
  for(unsigned int i = 0; i < num_into.size(); ++i)
    jsonfile << "\t";

  jsonfile << "\"" << name << "\": " << value << "\n";
}

void JSONSaver::Field(const char *name, const Hcoords value)
{
  for(unsigned int i = 0; i < num_into.size(); ++i)
    jsonfile << "\t";

  jsonfile << "\"" << name << "\": " << value << "\n";
}

void JSONSaver::Field(const char *name, const Affine value)
{
  for(unsigned int i = 0; i < num_into.size(); ++i)
    jsonfile << "\t";

  jsonfile << "\"" << name << "\": ";

  for(int i = 0; i < 3; ++i)
  {
    jsonfile << "[";
    for(int j = 0; j < 3; ++j)
    {
      jsonfile << value.matrix[i][j];

      if(j < 2)
        jsonfile << ",";
    }
    if(i < 2)
      jsonfile << "]" << ", ";
    else
      jsonfile << "]";
  }
  jsonfile << "\n";
}
void JSONSaver::FieldArr(const char *name, int **map, int Height, int Width)
{
  if(map == 0)
  {
    assert("MAP POINTER IS NULL!");
    return;
  }

  for(unsigned int i = 0; i < num_into.size(); ++i)
    jsonfile << "\t";
  jsonfile << "\"" << name << "\": ";

  for(int i = 0; i < Height; ++i)
  {
    jsonfile << "[";
    for(int j = 0; j < Width; ++j)
    {
      jsonfile << map[i][j];

      if(j < Width - 1)
        jsonfile << ",";
    }
    if(i < Height - 1)
      jsonfile << "]" << ", ";
    else
      jsonfile << "]";
  }
  jsonfile << "\n";
}


