//	File name:		JSONSaver.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Kyle Philistine
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include "../Math/Math2D.hpp"
#include "../../Entity.hpp"
using namespace std;
using namespace Math;


class JSONSaver
{

  public:
    JSONSaver();
	  bool LoadFile(const char *filepath);
	  void CloseFile(void);
	  void ReadObject(const char *name = 0);
	  void EndObject(void);


    void Field(const char *name, std::string value);
    void Field(const char *name, int value);
    void Field(const char *name, unsigned int value);
    void Field(const char *name, const char *value);
    void Field(const char *name, bool value);
    void Field(const char *name, float value);
    void Field(const char *name, double value);
    void Field(const char *name, const Point2D);
    void Field(const char *name, const Vector2D);
    void Field(const char *name, const Hcoords);
    void Field(const char *name, const Affine);
    void Field(const char *name, const Matrix4);
    void FieldArr(const char *name, int **map, int Height, int Width);
 


    void TestDriverJSON(void);
  private:
    ofstream jsonfile;
    std::pair<bool, bool> object;
    std::vector<int> num_into;
};

const std::string ArchetypeSaver(std::string filename, Entity *entity);
const std::string SaveAllEntities(const std::string filename);