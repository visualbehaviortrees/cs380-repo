//	File name:		JSonParser.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Kyle Philistine
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "JSonParser.hpp"

using namespace std;
using namespace JSON;


void ValType::Init()
{
  _int_value = 0;
  _float_value = 0.0;
  _bool_value = false;
  _Map_value = 0; 
  _type_t = TYPE_NIL;
}

ValType::ValType() : _type_t(TYPE_NIL) 
{
  Init();
}

//Poly p(9);
//for(unsigned i = 0; i < vertex_number; ++i)
//{
//  vector = serializevector(vertexNum);
//  p.setVert(i) = vector;
//}


ValType::ValType(const ValType& Val)
{
  Init();
  switch(Val._type_t)
  {
    case TYPE_INT:
      _int_value = Val._int_value;
      _type_t = TYPE_INT;
      break;

    case TYPE_FLOAT:
      _float_value = Val._float_value;
      _type_t = TYPE_FLOAT;
      break;

    case TYPE_BOOL:
      _bool_value = Val._bool_value;
      _type_t = TYPE_BOOL;
      break;

    case TYPE_STRING:
      _string_value = Val._string_value;
      _type_t = TYPE_STRING;
      break;

    case TYPE_ARRAY:
      _array_value = Val._array_value;
      _type_t = TYPE_ARRAY;
      break;

    case TYPE_OBJECT:
      _DE_value = Val._DE_value;
      _type_t = TYPE_OBJECT;
      break;

    case TYPE_HCOORD:
      _Hcoords_value = Val._Hcoords_value;
      _type_t = TYPE_HCOORD;
      break;

    case TYPE_POINT2D:
      _Point2D_value = Val._Point2D_value;
      _type_t = TYPE_POINT2D;
      break;

    case TYPE_VECTOR2D:
      _Vector2D_value = Val._Vector2D_value;
      _type_t = TYPE_VECTOR2D;
      break;

    case TYPE_AFFINE:
      _Affine_value = Val._Affine_value;
      _type_t = TYPE_AFFINE;
      break;

    case TYPE_MATRIX4:
      _Matrix4_value = Val._Matrix4_value;
      _type_t = TYPE_MATRIX4;
      break;

    case TYPE_MAP:
      _Map_value = Val._Map_value;
      _type_t = TYPE_MAP;
      break;

    case TYPE_NIL:
      break;
  }
}


ValType::ValType(const int value)
{
  Init();
  _int_value = value;
  _type_t = TYPE_INT;
}

ValType::ValType(const float value)
{
  Init();
  _float_value = value;
  _type_t = TYPE_FLOAT;
}

ValType::ValType(const bool value)
{
  Init();
  _bool_value = value;
  _type_t = TYPE_BOOL;
}

ValType::ValType(const char* value)          { Init(); _string_value  = value; _type_t = TYPE_STRING;}
ValType::ValType(const std::string& value)   { Init(); _string_value  = value; _type_t = TYPE_STRING;}
ValType::ValType(const DynamicElement& value){ Init(); _DE_value      = value; _type_t = TYPE_OBJECT;}
ValType::ValType(const Array& value)         { Init(); _array_value   = value; _type_t = TYPE_ARRAY; }

ValType::ValType(const Hcoords& value)       { Init(); _Hcoords_value  = (value); _type_t = TYPE_HCOORD;  }
ValType::ValType(const Point2D& value)       { Init(); _Point2D_value  = (value); _type_t = TYPE_POINT2D; }
ValType::ValType(const Vector2D& value)      { Init(); _Vector2D_value = (value); _type_t = TYPE_VECTOR2D;}
ValType::ValType(const Affine& value)        { Init(); _Affine_value   = (value); _type_t = TYPE_AFFINE;  }
ValType::ValType(const Matrix4& value)       { Init(); _Matrix4_value  = (value); _type_t = TYPE_MATRIX4; }
ValType::ValType(int** value)                { Init(); _Map_value      = (value); _type_t = TYPE_MAP;     }



ValType::ValType(const ValType&& Val)
{
  Init();
  switch(Val._type_t)
  {
    case TYPE_INT:
      _int_value = move(Val._int_value);
      _type_t = TYPE_INT;
      break;

    case TYPE_FLOAT:
      _float_value = move(Val._float_value);
      _type_t = TYPE_FLOAT;
      break;

    case TYPE_BOOL:
      _bool_value = move(Val._bool_value);
      _type_t = TYPE_BOOL;
      break;

    case TYPE_STRING:
      _string_value = move(Val._string_value);
      _type_t = TYPE_STRING;
      break;

    case TYPE_ARRAY:
      _array_value = move(Val._array_value);
      _type_t = TYPE_ARRAY;
      break;

    case TYPE_OBJECT:
      _DE_value = move(Val._DE_value);
      _type_t = TYPE_OBJECT;
      break;

    case TYPE_HCOORD:
      _Hcoords_value = move(Val._Hcoords_value);
      _type_t = TYPE_HCOORD;
      break;

    case TYPE_POINT2D:
      _Point2D_value = move(Val._Point2D_value);
      _type_t = TYPE_POINT2D;
      break;

    case TYPE_VECTOR2D:
      _Vector2D_value = move(Val._Vector2D_value);
      _type_t = TYPE_VECTOR2D;
      break;

    case TYPE_AFFINE:
      _Affine_value = move(Val._Affine_value);
      _type_t = TYPE_AFFINE;
      break;

    case TYPE_MATRIX4:
      _Matrix4_value = move(Val._Matrix4_value);
      _type_t = TYPE_MATRIX4;
      break;

    case TYPE_MAP:
      _Map_value = move(Val._Map_value);
      _type_t = TYPE_MAP;
      break;

    case TYPE_NIL:
      _type_t = TYPE_NIL;
      break;
  }
}

ValType::ValType(const Array&& value)          { Init(); _array_value =    (move(value));  _type_t = (TYPE_ARRAY);   }
ValType::ValType(const std::string&& value)    { Init(); _string_value =   (move(value));  _type_t = (TYPE_STRING);  }
ValType::ValType(const DynamicElement&& value) { Init(); _DE_value =       (move(value));  _type_t = (TYPE_OBJECT);  }
ValType::ValType(const Hcoords&& value)        { Init(); _Hcoords_value =  (move(value));  _type_t = (TYPE_HCOORD);  }
ValType::ValType(const Point2D&& value)        { Init(); _Point2D_value =  (move(value));  _type_t = (TYPE_POINT2D); }
ValType::ValType(const Vector2D&& value)       { Init(); _Vector2D_value = (move(value));  _type_t = (TYPE_VECTOR2D);}
ValType::ValType(const Affine&& value)         { Init(); _Affine_value =   (move(value));  _type_t = (TYPE_AFFINE);  }
ValType::ValType(const Matrix4&& value)        { Init(); _Matrix4_value =  (move(value));  _type_t = (TYPE_MATRIX4); }

const ValType& ValType::operator[] (const std::string& index) const
{
    if (type() != TYPE_OBJECT)
        throw std::logic_error("Value not an object");
    return _DE_value[index];
}


ValType& ValType::operator[](const size_t index)
{
    if (type() != TYPE_ARRAY)
      throw std::logic_error("Value not an array");
    return _array_value[index];
}


const ValType& ValType::operator[](const size_t index) const
{
    if (type() != TYPE_ARRAY)
      throw std::logic_error("Value not an array");
    return _array_value[index];
}


ValType& ValType::operator=(const ValType& Val)
{
  this->Init();
  switch(Val._type_t)
  {
    case TYPE_INT:
      _int_value = Val._int_value;
      _type_t = TYPE_INT;
      break;

    case TYPE_FLOAT:
      _float_value = Val._float_value ;
      _type_t = TYPE_FLOAT;
      break;

    case TYPE_BOOL:
      _bool_value = Val._bool_value;
      _type_t = TYPE_BOOL;
      break;

    case TYPE_STRING:
      _string_value = Val._string_value;
      _type_t = TYPE_STRING;
      break;

    case TYPE_ARRAY:
      _array_value = Val._array_value;
      _type_t = TYPE_ARRAY;
      break;

    case TYPE_OBJECT:
      _DE_value = Val._DE_value;
      _type_t = TYPE_OBJECT;
      break;

    case TYPE_HCOORD:
      _Hcoords_value = Val._Hcoords_value;
      _type_t = TYPE_HCOORD;
      break;

    case TYPE_POINT2D:
      _Point2D_value = Val._Point2D_value;
      _type_t = TYPE_POINT2D;
      break;

    case TYPE_VECTOR2D:
      _Vector2D_value = Val._Vector2D_value;
      _type_t = TYPE_VECTOR2D;
      break;

    case TYPE_AFFINE:
      _Affine_value = Val._Affine_value;
      _type_t = TYPE_AFFINE;
      break;

    case TYPE_MATRIX4:
      _Matrix4_value = Val._Matrix4_value;
      _type_t = TYPE_MATRIX4;
      break;

    case TYPE_MAP:
      _Map_value = Val._Map_value;
      _type_t = TYPE_MAP;
      break;

    case TYPE_NIL:
      _type_t = TYPE_NIL;
      break;
  }
  return *this;
}


ValType& ValType::operator=(ValType&& Val)
{
  this->Init();
  switch(Val._type_t)
  {
    case TYPE_INT:
      _int_value = move(Val._int_value);
      _type_t = TYPE_INT;
      break;

    case TYPE_FLOAT:
      _float_value = move(Val._float_value);
      _type_t = TYPE_FLOAT;
      break;

    case TYPE_BOOL:
      _bool_value = move(Val._bool_value);
      _type_t = TYPE_BOOL;
      break;

    case TYPE_STRING:
      _string_value = move(Val._string_value);
      _type_t = TYPE_STRING;
      break;

    case TYPE_ARRAY:
      _array_value = move(Val._array_value);
      _type_t = TYPE_ARRAY;
      break;

    case TYPE_OBJECT:
      _DE_value = move(Val._DE_value);
      _type_t = TYPE_OBJECT;
      break;

    case TYPE_HCOORD:
      _Hcoords_value = move(Val._Hcoords_value);
      _type_t = TYPE_HCOORD;
      break;

    case TYPE_POINT2D:
      _Point2D_value = move(Val._Point2D_value);
      _type_t = TYPE_POINT2D;
      break;

    case TYPE_VECTOR2D:
      _Vector2D_value = move(Val._Vector2D_value);
      _type_t = TYPE_VECTOR2D;
      break;

    case TYPE_AFFINE:
      _Affine_value = move(Val._Affine_value);
      _type_t = TYPE_AFFINE;
      break;

    case TYPE_MATRIX4:
      _Matrix4_value = move(Val._Matrix4_value);
      _type_t = TYPE_MATRIX4;
      break;

    case TYPE_MAP:
      _Map_value = move(Val._Map_value);
      _type_t = TYPE_MAP;
      break;

    case TYPE_NIL:
      _type_t = TYPE_NIL;
      break;
  }
  return *this;
}






DynamicElement::DynamicElement() {}
DynamicElement::~DynamicElement() {}
DynamicElement::DynamicElement(const DynamicElement& DE) : _object(DE._object) {}
DynamicElement::DynamicElement(DynamicElement&& DE): _object(move(_object)) {}

void DynamicElement::clear()
{
  _object.clear();
}

bool DynamicElement::AddObject(Array member, std::string parentname, std::string objectname)
{
  if(_object[parentname].type() != TYPE_OBJECT)
      return false;
    
  _object.insert(_object.begin(), _object.find(objectname));

  return true;
}

DynamicElement& DynamicElement::operator=(const DynamicElement& DE)
{
  _object = DE._object;
  return *this;
}

DynamicElement& DynamicElement::operator=(DynamicElement&& DE)
{
    _object = move(DE._object);
    return *this;
}

// gets the object from the array using DE_name
ValType& DynamicElement::operator[] (const std::string& DE_name)
{
  return _object[DE_name];
}


// gets the object from the array using DE_name
const ValType& DynamicElement::operator[] (const std::string DE_name) const
{
  return _object.at(DE_name);
}

// gets the tail iterator of the map (const)
std::map<std::string, ValType>::const_iterator DynamicElement::begin() const
{
  return _object.begin();
}

// gets the tail iterator of the map (const)
std::map<std::string, ValType>::const_iterator DynamicElement::end() const
{
  return _object.end();
}

// gets the tail iterator of the map
std::map<std::string, ValType>::iterator DynamicElement::begin()
{
  return _object.begin();
}


// gets the tail iterator of the map
std::map<std::string, ValType>::iterator DynamicElement::end()
{
  return _object.end();
}

// inserts a entry into the objects field 
std::pair<std::map<std::string, ValType>::iterator, bool> DynamicElement::insert(const std::pair<std::string, ValType>& insert)
{
  return _object.insert(insert);
}



// size of the current object
size_t DynamicElement::size() const
{
  return _object.size();
}








Array::Array() {}
Array::~Array() {}


// copy constructor
Array::Array(const Array& A) : _array(A._array) {}

// assignment overload
Array& Array::operator=(const Array& A)
{
  _array = A._array;
  return *this;
}

// move constructor / copy constructor
Array::Array(Array&& A) : _array(move(A._array)){}


// assignment overload
Array& Array::operator=(const Array&& A)
{
  _array = move(A._array);
  return *this;
}


// overload [] for going through the array by index
ValType& Array::operator[] (size_t index)
{
  return _array.at(index);
}


// overload[] to go through the array by an index
const ValType& Array::operator[] (size_t index) const
{
  return _array.at(index);
}

// the beginning of the array (const)
std::vector<ValType>::const_iterator Array::begin() const
{
  return _array.begin();
}

// the end of the array (const)
std::vector<ValType>::const_iterator Array::end() const
{
  return _array.end();
}

// the beginning of the array
std::vector<ValType>::iterator Array::begin()
{
  return _array.begin();
}

// the beginning of the array
std::vector<ValType>::iterator Array::end()
{
  return _array.end();
}


// pushes a val back in the array
void Array::push_back(const ValType& Val)
{
  return _array.push_back(Val);
}


// size of the array
size_t Array::size() const
{
  return _array.size();
}

void Array::pop_back()
{
  return _array.pop_back();
}

void Array::clear(void)
{
  for(unsigned int i = _array.size(); i > 0; --i)
    _array.pop_back();
}

void JSON::indent(std::ostream& os)
{
    for (unsigned int i  = 0; i < index; i++)
        os << "\t";
}


// output for DynamicElements
std::ostream& operator<<(std::ostream& os, const JSON::DynamicElement& DE)
{
  os << "{" << endl;
  ++index;

  for (auto i = DE.begin(); i != DE.end();)
  {
      JSON::indent(os);
      os << '"' << i->first << '"' << ": " << i->second;

      if (++i != DE.end())
          os << ",";
      os << endl;
  }    
  --index;
  JSON::indent(os);
  os << "}";
    
  return os;
}


// output for Values
std::ostream& operator<<(std::ostream& os, const JSON::ValType& Val_T)
{
  switch(Val_T.type())
  {
    case TYPE_INT:
        os << (int)Val_T;
        break;
        
    case TYPE_FLOAT:
        os << (double)Val_T;
        break;
        
    case TYPE_BOOL:
        os << ((bool)Val_T ? "true" : "false");
        break;
        
    case TYPE_STRING:
        os << '"' << (string)Val_T << '"';                
        break;
        
    case TYPE_ARRAY:
        os << (Array)Val_T;                
        break;
        
    case TYPE_OBJECT:
        os << (DynamicElement)Val_T;                
        break;

    case TYPE_NIL:
        os << "null";
        break;

  }
  return os;
}


// output for Arrays
std::ostream& operator<<(std::ostream& os, const JSON::Array& Array_A)
{
  os << "[" << endl;
  index++;
  for (auto i = Array_A.begin(); i != Array_A.end();)
  {
      JSON::indent(os);
      os << (*i);
      if (++i != Array_A.end())
          os << ",";
      os << endl;
  }    
  index--;
  JSON::indent(os);
  os << "]";
    
  return os;
}