//	File name:		JSonParser.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Kyle Philistine
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include <iostream>
#include <map>
#include <vector>
#include <stack>
#include <string>
#include "Systems\Input\InputSystem.hpp"
#include "../Math/Math2D.hpp"

using namespace Math;

namespace JSON
{
  enum Type
  {
    TYPE_INT,
    TYPE_FLOAT,
    TYPE_BOOL,
    TYPE_STRING,
    TYPE_OBJECT,
    TYPE_ARRAY,
    TYPE_HCOORD,
    TYPE_POINT2D,
    TYPE_VECTOR2D,
    TYPE_AFFINE,
    TYPE_MATRIX4,
    TYPE_MAP,
    TYPE_NIL
  };

  // forward declaraction
  class ValType;
  class Array;

  class DynamicElement
  {
    public:
      // default constructor
      DynamicElement();

      // Destructor
      ~DynamicElement();

      // non-default constructor
      DynamicElement(const DynamicElement& DE);

      // move constructor
      DynamicElement(DynamicElement&& DE);

      bool AddObject(Array member, std::string parentname, std::string objectname);

      // assignment from one DynamicElement to another DynamicElement
      DynamicElement& operator=(const DynamicElement& DE);

      DynamicElement& operator=(DynamicElement&& DE);

      // gets the object from the array using DE_name
      ValType& operator[] (const std::string& DE_name);

      // gets the object from the array using DE_name
      const ValType& operator[] (const std::string DE_name) const;

      // gets the tail iterator of the map (const)
      std::map<std::string, ValType>::const_iterator begin() const;

      // gets the tail iterator of the map (const)
      std::map<std::string, ValType>::const_iterator end() const;

      // gets the tail iterator of the map
      std::map<std::string, ValType>::iterator begin();

      // gets the tail iterator of the map
      std::map<std::string, ValType>::iterator end();

      void clear(void);

      // inserts a entry into the objects field 
      std::pair<std::map<std::string, ValType>::iterator, bool> insert(const std::pair<std::string, ValType>& insert);

      // size of the current object
      size_t size() const;

      

    protected:
      std::map<std::string, ValType> _object;
  };

  class Array
  {
    public:
      // default constructor
      Array();
 
      // desctructor
      ~Array();
 
      // copy constructor
      Array(const Array& A);
 
      // assignment overload
      Array& operator=(const Array& A);
 
      // move constructor / copy constructor
      Array(Array&& A);
 
      // assignment overload
      Array& operator=(const Array&& A);
 
      // overload [] for going through the array by index
      ValType& operator[] (size_t index);
 
      // overload[] to go through the array by an index
      const ValType& operator[] (size_t index) const;
 
      // the beginning of the array (const)
      std::vector<ValType>::const_iterator begin() const;
 
      // the end of the array (const)
      std::vector<ValType>::const_iterator end() const;
 
      // the beginning of the array
      std::vector<ValType>::iterator begin();
 
      // the end of the array
      std::vector<ValType>::iterator end();
 
      // pushes a val back in the array
      void push_back(const ValType& Val);

 
      // destroys the last value of the array
      void pop_back(void);

      void clear(void);

      // size of the array
      size_t size() const;
 
   private:
     std::vector<ValType> _array;

  };


  class ValType
  {
    public:
      ValType(const ValType& Val);
      ValType();
      void Init();

      ValType(const int value);
      ValType(const float value);
      ValType(const bool value);
      ValType(const char* value);
      ValType(const std::string& value);
      ValType(const DynamicElement& value);
      ValType(const Array& value);
      ValType(const Hcoords& value);
      ValType(const Point2D& value);
      ValType(const Vector2D& value);
      ValType(const Affine& value);
      ValType(const Matrix4& value);
      ValType(int** value);



      ValType(const ValType&& Val);
      ValType(const Array&& value);
      ValType(const std::string&& value);
      ValType(const DynamicElement&& value);

      ValType(const Hcoords&& value);
      ValType(const Point2D&& value);
      ValType(const Vector2D&& value);
      ValType(const Affine&& value);
      ValType(const Matrix4&& value);



      inline
      Type type() const
      {
        return _type_t;
      }

      ValType& operator[] (const std::string& index);
      const ValType& operator[] (const std::string& index) const;
      ValType& operator[](const size_t index);
      const ValType& operator[](const size_t index) const;
      ValType& operator=(const ValType& Val);
      ValType& operator=(ValType&& Val);


      operator JOYSTICKS()   const {return (JOYSTICKS)_int_value;} // yolo
      operator KEYS()   const {return (KEYS)_int_value;} // yolo
      //operator double()      const {return _float_value;}
      operator float()       const {return _float_value;}
      operator bool()        const {return _bool_value;}
      operator int()         const {return _int_value;}
      operator std::string() const {return _string_value;}
      //explicit operator const char*() const {return _string_value.c_str();}
      operator DynamicElement () const {return _DE_value;}
      operator Array () const {return _array_value;}

      operator Hcoords()  const {return _Hcoords_value;}
      operator Point2D()  const {return _Point2D_value;}
      operator Vector2D() const {return _Vector2D_value;}
      operator Affine()   const {return _Affine_value;}
      operator Matrix4()  const {return _Matrix4_value;}
      operator int **()   const {return _Map_value;}

      double GetDouble() const { return _float_value;}
      float GetFloat() const { return _float_value;}
      int GetInt() const { return _int_value;}
      bool GetBool() const { return _bool_value;}
      std::string GetString() const { return _string_value;}

      DynamicElement GetDynamicElement() const { return _DE_value;}
      Array GetArray() const { return _array_value;}
      Hcoords GetHcoords() const { return _Hcoords_value;}
      Point2D GetPoint() const { return _Point2D_value;}
      Vector2D GetVector2D() const { return _Vector2D_value;}
      Affine GetAffine() const { return _Affine_value;}
      Matrix4 GetMatrix4() const { return _Matrix4_value;}
      int ** GetMap() const { return _Map_value;}

    private:

      int            _int_value;
      float          _float_value;
      bool           _bool_value;

      std::string    _string_value;
      DynamicElement _DE_value;
      Array          _array_value;
      Hcoords        _Hcoords_value;
      Point2D        _Point2D_value;
      Vector2D       _Vector2D_value;
      Affine         _Affine_value;
      Matrix4        _Matrix4_value;
      int **         _Map_value;

      Type           _type_t;
  };

  static unsigned int index;

  static void indent(std::ostream& os = std::cout);
}



// output for DynamicElements
std::ostream& operator<<(std::ostream& os, const JSON::DynamicElement& DE);


// output for Values
std::ostream& operator<<(std::ostream& os, const JSON::ValType& Val_T);

// output for Arrays
std::ostream& operator<<(std::ostream& os, const JSON::Array& Array_A);

