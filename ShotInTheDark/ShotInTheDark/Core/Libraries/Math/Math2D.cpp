//	File name:		Math2D.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Kyle Philistine
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "Math2D.hpp"
#include <vector>
#include <string>

using namespace Math;

Hcoords::Hcoords(void) : x(0), y(0), z(0), w(0) {}
Hcoords::Hcoords(const float X, const float Y, const float Z, const float W) : x(X), y(Y), z(Z), w(0){}

Vector2D::Vector2D(void)  { x = 0.0f; y = 0.0f;  z = 0.0f; w = 0.0f; }
Vector2D::Vector2D(const float X, const float Y, const float Z, const float W)  { x = X; y = Y; z = 0; w = 0; }

Vector2D::Vector2D(const glm::vec2& v)
{
  x = v.x;
  y = v.y;
}

Vector2D::Vector2D(const Hcoords& H) { x = H.x; y = H.y; z = 0; w = 0; }

void Vector2D::Zero(void) { x = 0; y = 0; z = 0; }

bool Vector2D::Normalize()
{
  if (x == 0 && y == 0)
    return false;

  float normal;

  normal = std::sqrt((x * x) + (y * y));

  x = (x / normal);
  y = (y / normal);

  return true;
}

Vector2D Vector2D::Normalize(void) const
{
	if (x == 0 && y == 0)
		return *this;

	float normal;

	normal = std::sqrt((x * x) + (y * y));

	return Vector2D((x / normal), (y / normal));
}

void Hcoords::Set(const float X, const float Y, const float Z){ x = X; y = Y; z = Z; }

void Vector2D::Scale(float c)
{
  x *= c;
  y *= c;
}

void Vector2D::ScaleAdd(const Vector2D &v, const float c)
{
  this->Scale(c);
  x += v.x;
  y += v.y;
}

void Vector2D::ScaleSub(const Vector2D &v, const float c)
{
  this->Scale(c);
  x -= v.x;
  y -= v.y;
}


float Vector2D::Angle(const Vector2D &v) const
{
  return atan2f(y - v.y, x - v.x);
}


float Hcoords::Length(void)
{
  return std::sqrt(this->SquareLength());
}

float Hcoords::Length(void) const
{
	return std::sqrt(this->SquareLength());
}

float Hcoords::SquareLength(void)
{
  return ((x * x) + (y * y));
}

float Hcoords::SquareLength(void) const
{
	return ((x * x) + (y * y));
}

float Point2D::Distance(const Point2D &v)
{
  return sqrt(this->SquareDistance(v));
}

float Point2D::SquareDistance(const Point2D &v)
{
  return (v.y - y) * (v.y - y) + (v.x - x) * (v.x - x);
}

float Hcoords::DotProduct(const Hcoords &v)
{
  return (x * v.x) + (y * v.y);
}


float  Vector2D::X(void) const { return x; }
float  Vector2D::Y(void) const { return y; }



Vector2D Vector2D::operator-() const
{
  Vector2D v(-x, -y);
  return v;
}


Vector2D Vector2D::operator-(const Vector2D &rhs) const
{
  Vector2D retval = *this;
  retval -= rhs;
  return retval;
}


Vector2D Vector2D::operator+(const Vector2D &rhs) const
{
  return Vector2D(x + rhs.x, y + rhs.y, z + rhs.z);
}

bool Vector2D::operator==(const Vector2D &rhs) const
{
  if (x == rhs.x && y == rhs.y)
    return true;
  return false;
}
// rotates the Vector2D by given degrees
Vector2D Vector2D::operator%(const float degrees) const
{
  float cs = cos(degrees*_DEG2RAD_);
  float sn = sin(degrees*_DEG2RAD_);
  float rx = x*cs - y * sn;
  float ry = x*sn + y * cs;
  Vector2D v(rx, ry);
  return v;
}

Vector2D Vector2D::operator*(const float scalar) const
{
  Vector2D v(x*scalar, y*scalar);
  return v;
}

float Vector2D::operator*(const Vector2D &rhs) const
{
  return x * rhs.x + y * rhs.y;
}

Vector2D Vector2D::operator/(const float rhs) const
{
  Vector2D retval(x / rhs, y / rhs);
  return retval;
}



/*****************************************************************************/
/*********************** Assignment Operators ********************************/
/*****************************************************************************/

Vector2D& Vector2D::operator+=(const Vector2D &rhs)
{
  x += rhs.x;
  y += rhs.y;
  return *this;
}

Vector2D& Vector2D::operator+=(const float rhs)
{
  x += rhs;
  y += rhs;
  return *this;
}

Vector2D& Vector2D::operator-=(const Vector2D &rhs)
{
  x -= rhs.x;
  y -= rhs.y;
  return *this;
}

Vector2D& Vector2D::operator-=(const float rhs)
{
  x -= rhs;
  y -= rhs;
  return *this;
}

Vector2D& Vector2D::operator*=(const float scalar)
{
  x *= scalar;
  y *= scalar;
  return *this;
}

Vector2D& Vector2D::operator/=(const float divisor)
{
  x /= divisor;
  y /= divisor;
  return *this;
}

Vector2D& Vector2D::operator%=(const float degrees)
{
  *this = *this % degrees;
  return *this;
}



Point2D::Point2D(const float X, const float Y, const float Z, const float W){ x = X; y = Y; z = Z; w = W; }

Point2D::Point2D(const Hcoords& Point2D){ x = Point2D.x; y = Point2D.y; z = Point2D.z; w = Point2D.w; }


Point2D::Point2D(const glm::vec2& v)
{
  x = v.x;
  y = v.y;
}

Point2D::Point2D(const Point2D& P) { x = P.x; y = P.y; z = P.z; w = P.w; }


float Point2D::X(void) const
{
  return x;
}

float Point2D::Y(void) const
{
  return y;
}

float Point2D::operator[](int i) const
{
  if (i == 0)
    return x;
  else
    return y;
}

Point2D& Point2D::operator=(const Point2D& C)
{
  x = C.x;
  y = C.y;
  return *this;
}

Point2D& Point2D::operator+=(const Point2D& C)
{
  x += C.x;
  y += C.y;

  return *this;
}

Point2D& Point2D::operator-=(const Point2D& C)
{
  x -= C.x;
  y -= C.y;

  return *this;
}

Point2D& Point2D::operator*=(const float S)
{
  x *= S;
  y *= S;
  return *this;
}

Point2D Point2D::operator+(const Point2D& v)
{
  return Point2D(x + v.x, y + v.y);
}


Point2D Point2D::operator-(void)
{
  return Point2D(-x, -y);
}

Point2D Math::operator*(const float r, const Point2D& u)
{
  Point2D Scale = u;
  Scale *= r;

  return Scale;
}

Point2D Point2D::operator*(const float r)
{
  Point2D Scale(x, y);
  Scale *= r;

  return Scale;
}


Vector2D Point2D::operator-(const Point2D& v)
{
  return Vector2D(x - v.x, y - v.y);
}

float Point2D::operator*(const Point2D& v)
{
  return x * v.x + y * v.y;
}


bool Point2D::operator==(const Point2D& v)
{
  if( x == v.x
   && y == v.y)
    return true;
  else
    return false;
}

bool Point2D::operator!=(const Point2D& v)
{
  if (x != v.x
    || y != v.y)
    return false;
  else
    return true;
}

Point2D Point2D::BarScale(float r)
{
  return(Point2D(x *= r, y *= r, w *= r));
}

Point2D Math::GetMousePosition(void) // Gets the mouse position in WindowToWorld Coordinates
{
  double xpos = 0, ypos = 0;
  Graphics* GS = ((Graphics*)ENGINE->GetSystem(sys_Graphics));
  glfwGetCursorPos(GS->GetCurrentWindow()->window, &xpos, &ypos);
  Point2D Start((float)xpos, (float)ypos);
  Math::Affine WindowToWorld = *(GS->GetCameraToWorld()) * Math::Inverse(GS->GetCameraToNDC()) * GS->GetWindowToNDC();
  Point2D Finish = WindowToWorld * Start;
  return Finish;
}

Affine::Affine(void)
{
  memset(matrix, 0, sizeof(matrix));
  matrix[2][2] = 1;
}

Affine::Affine(const Vector2D& LeftCol, const Vector2D& MiddleCol, const Point2D& RightCol)
{
  matrix[0][0] = LeftCol.x;
  matrix[1][0] = LeftCol.y;
  matrix[2][0] = 0;

  matrix[0][1] = MiddleCol.x;
  matrix[1][1] = MiddleCol.y;
  matrix[2][1] = 0;

  matrix[0][2] = RightCol.x;
  matrix[1][2] = RightCol.y;
  matrix[2][2] = 1;
}


/*****************************************************************************/
/***********************  End Of Constructors ********************************/
/*****************************************************************************/





/*****************************************************************************/
/***********************  Operator Overloads  ********************************/
/*****************************************************************************/


/*****************************************************************************/
/***********************  Hcoords Overloads   ********************************/
/*****************************************************************************/


Hcoords Hcoords::operator+(const Hcoords& rhs)
{
  return Hcoords(this->x + rhs.x, this->y + rhs.y);
}

Hcoords Hcoords::operator+(const Hcoords& rhs) const
{
	return Hcoords(this->x + rhs.x, this->y + rhs.y);
}

Hcoords Hcoords::operator-(const Hcoords& rhs)
{
  return Hcoords(this->x - rhs.x, this->y - rhs.y, this->z - rhs.z);
}

Hcoords Hcoords::operator*(const float r)
{
  return Hcoords(this->x * r, this->y * r);
}

Hcoords Hcoords::operator-(void)
{
  return Hcoords(-this->x, -this->y);
}

/*******************************************************************************/
/***********************End Of Hcoords Overloads********************************/
/*******************************************************************************/



/********************************************************************/
/***********************   Affine Overloads   ***********************/
/********************************************************************/

Affine Math::Rot(float t)
{

  Affine Rot;

  Rot.matrix[0][0] = std::cos(t*_DEG2RAD_);
  Rot.matrix[0][1] = -std::sin(t*_DEG2RAD_);
  Rot.matrix[1][0] = std::sin(t*_DEG2RAD_);
  Rot.matrix[1][1] = std::cos(t*_DEG2RAD_);

  return Rot;
}

Affine Math::Trans(const Point2D& p)
{
  Affine X(Vector2D(1.0f, 0.0f), Vector2D(0.0f, 1.0f), p);
  return X;
}

Affine Math::Scale(float r)
{
  return Affine(Vector2D(r, 0), Vector2D(0, r), Point2D());
}

Affine Math::Scale(float rx, float ry)
{
  return Affine(Vector2D(rx, 0), Vector2D(0, ry), Point2D());
}



Affine Affine::operator+(const Affine& B) const
{
  return Affine(Vector2D(this->matrix[0][0] + B.matrix[0][0], this->matrix[1][0] + B.matrix[1][0]),
    Vector2D(this->matrix[0][1] + B.matrix[0][1], this->matrix[1][1] + B.matrix[1][1]),
    Point2D(this->matrix[0][2] + B.matrix[0][2], this->matrix[1][2] + B.matrix[1][2]));
}

Affine Affine::operator*(const Affine& B) const
{
  int i, j, k;
  Affine Multiply;

  for (i = 0; i < 3; ++i)
  {
    for (j = 0; j < 3; ++j)
    {
      for (k = 0; k < 3; ++k)
      {
        if (!(i == 2 && j == 2))
          Multiply.matrix[i][j] += this->matrix[i][k] * B.matrix[k][j];
      }
    }
  }
  return Multiply;
}

Hcoords Affine::operator*(const Hcoords& B)
{
  Hcoords multiply;

  multiply.x = this->matrix[0][0] * B.x + this->matrix[0][1] * B.y + this->matrix[0][2] * B.w;
  multiply.y = this->matrix[1][0] * B.x + this->matrix[1][1] * B.y + this->matrix[1][2] * B.w;
  multiply.w = this->matrix[2][0] * B.x + this->matrix[2][1] * B.y + this->matrix[2][2] * B.w;

  return multiply;
}



Matrix4::Matrix4(void)
{
  memset(matrix4, 0, sizeof(matrix4));
  matrix4[3][3] = 1;
}

Matrix4::Matrix4(const Affine& A)
{
  *this = A;
}

Matrix4::Matrix4(const Vector2D& LeftCol, const Vector2D& MiddleCol, const Point2D& P)
{
  matrix4[0][0] = LeftCol.x;
  matrix4[1][0] = LeftCol.y;
  matrix4[2][0] = LeftCol.z;
  matrix4[3][0] = 0.0f;

  matrix4[0][1] = MiddleCol.x;
  matrix4[1][1] = MiddleCol.y;
  matrix4[2][1] = MiddleCol.z;
  matrix4[3][1] = 0.0f;

  matrix4[0][2] = 0.0f;
  matrix4[1][2] = 0.0f;
  matrix4[2][2] = 0.0f;
  matrix4[3][2] = 0.0f;

  matrix4[0][3] = P.x;
  matrix4[1][3] = P.y;
  matrix4[2][3] = P.z;
  matrix4[3][3] = 1.0f;
}

Matrix4::Matrix4(const Vector2D& LeftCol, const Vector2D& MiddleCol, const Vector2D& RightCol, const Point2D& P)
{
  matrix4[0][0] = LeftCol.x;
  matrix4[1][0] = LeftCol.y;
  matrix4[2][0] = LeftCol.z;
  matrix4[3][0] = 0.0f;

  matrix4[0][1] = MiddleCol.x;
  matrix4[1][1] = MiddleCol.y;
  matrix4[2][1] = MiddleCol.z;
  matrix4[3][1] = 0.0f;


  matrix4[0][2] = RightCol.x;
  matrix4[1][2] = RightCol.y;
  matrix4[2][2] = RightCol.z;
  matrix4[3][2] = 0.0f;

  matrix4[0][3] = P.x;
  matrix4[1][3] = P.y;
  matrix4[2][3] = P.z;
  matrix4[3][3] = 1.0f;
}

Matrix4 Matrix4::operator+(const Matrix4& m4) const
{
  return Matrix4(Vector2D(this->matrix4[0][0] + m4.matrix4[0][0], this->matrix4[1][0] + m4.matrix4[1][0], this->matrix4[3][0] + m4.matrix4[3][0]),
    Vector2D(this->matrix4[0][1] + m4.matrix4[0][1], this->matrix4[1][1] + m4.matrix4[1][1], this->matrix4[3][1] + m4.matrix4[3][1]),
    Vector2D(this->matrix4[0][2] + m4.matrix4[0][2], this->matrix4[1][2] + m4.matrix4[1][2], this->matrix4[3][2] + m4.matrix4[3][2]),
    Point2D(this->matrix4[0][3] + m4.matrix4[0][3], this->matrix4[1][3] + m4.matrix4[1][3]));
}

Matrix4 Matrix4::operator*(const Matrix4& m4) const
{
  return Matrix4(Vector2D(this->matrix4[0][0] * m4.matrix4[0][0], this->matrix4[1][0] * m4.matrix4[1][0], this->matrix4[3][0] * m4.matrix4[3][0]),
    Vector2D(this->matrix4[0][1] * m4.matrix4[0][1], this->matrix4[1][1] * m4.matrix4[1][1], this->matrix4[3][1] * m4.matrix4[3][1]),
    Vector2D(this->matrix4[0][2] * m4.matrix4[0][2], this->matrix4[1][2] * m4.matrix4[1][2], this->matrix4[3][2] * m4.matrix4[3][2]),
    Point2D(this->matrix4[0][3] * m4.matrix4[0][3], this->matrix4[1][3] * m4.matrix4[1][3]));
}

Matrix4& Matrix4::operator=(const Affine& A)
{/*
 this->matrix4[0][0] = A.matrix[0][0];
 this->matrix4[0][1] = A.matrix[0][1];
 this->matrix4[0][2] = 0.0f;
 this->matrix4[0][3] = A.matrix[0][2];
 this->matrix4[1][0] = A.matrix[1][0];
 this->matrix4[1][1] = A.matrix[1][1];
 this->matrix4[1][2] = 0.0f;
 this->matrix4[1][3] = A.matrix[1][2];
 this->matrix4[2][0] = 0.0f;
 this->matrix4[2][1] = 0.0f;
 this->matrix4[2][2] = 0.0f;
 this->matrix4[2][3] = 0.0f;
 this->matrix4[3][0] = A.matrix[2][0];
 this->matrix4[3][1] = A.matrix[2][1];
 this->matrix4[3][2] = 0.0f;
 this->matrix4[3][3] = A.matrix[2][2];
 */
  this->matrix4[0][0] = A.matrix[0][0];
  this->matrix4[0][1] = A.matrix[1][0];
  this->matrix4[0][2] = 0.0f;
  this->matrix4[0][3] = A.matrix[2][0];
  this->matrix4[1][0] = A.matrix[0][1];
  this->matrix4[1][1] = A.matrix[1][1];
  this->matrix4[1][2] = 0.0f;
  this->matrix4[1][3] = A.matrix[2][1];
  this->matrix4[2][0] = 0.0f;
  this->matrix4[2][1] = 0.0f;
  this->matrix4[2][2] = 0.0f;
  this->matrix4[2][3] = 0.0f;
  this->matrix4[3][0] = A.matrix[0][2];
  this->matrix4[3][1] = A.matrix[1][2];
  this->matrix4[3][2] = 0.0f;
  this->matrix4[3][3] = A.matrix[2][2];

  return *this;
}


float Math::dot(const Hcoords& u, const Hcoords& v)
{
  return (u.x * v.x) + (u.y * v.y);
}

float Math::abs(const Vector2D& v)
{
  return std::sqrt((v.x*v.x) + (v.y*v.y));
}

float Math::abs(const float &f)
{
  if (f < 0.f)  return f * -1.f;
  return f;
}

Affine Math::Inverse(const Affine& A)
{
  float detDenom = ((A.matrix[0][0] * A.matrix[1][1]) - (A.matrix[1][0] * A.matrix[0][1]));
  assert(detDenom);
  if (!detDenom)
  {
    std::cout << "DIVIDING BY ZERO IN AFFINE INVERSE" << std::endl;
    return A;
  }

  float det = 1 / detDenom;

  Affine Inverse;

  Inverse.matrix[0][0] = A.matrix[1][1] * det;
  Inverse.matrix[0][1] = -A.matrix[0][1] * det;
  Inverse.matrix[1][0] = -A.matrix[1][0] * det;
  Inverse.matrix[1][1] = A.matrix[0][0] * det;

  Inverse.matrix[0][2] = (-A.matrix[0][2] * Inverse.matrix[0][0]) + (-A.matrix[1][2] * Inverse.matrix[0][1]);
  Inverse.matrix[1][2] = (-A.matrix[0][2] * Inverse.matrix[1][0]) + (-A.matrix[1][2] * Inverse.matrix[1][1]);

  return Inverse;
}

Matrix4 Math::Inverse(const Matrix4& A)
{
  float det = 1 / ((A.matrix4[0][0] * A.matrix4[1][1]) - (A.matrix4[1][0] * A.matrix4[0][1]));

  Matrix4 Inverse;

  Inverse.matrix4[0][0] = A.matrix4[1][1] * det;
  Inverse.matrix4[0][1] = -A.matrix4[0][1] * det;
  Inverse.matrix4[1][0] = -A.matrix4[1][0] * det;
  Inverse.matrix4[1][1] = A.matrix4[0][0] * det;

  Inverse.matrix4[0][3] = (-A.matrix4[0][3] * Inverse.matrix4[0][0]) + (-A.matrix4[1][3] * Inverse.matrix4[0][1]);
  Inverse.matrix4[1][3] = (-A.matrix4[0][3] * Inverse.matrix4[1][0]) + (-A.matrix4[1][3] * Inverse.matrix4[1][1]);

  return Inverse;
}



Hcoords Math::string_to_Hcoords(const std::string  &value)
{
  std::vector<float> values;
  std::string number;
  for (unsigned int i = 0; i < value.length(); ++i)
  {
    if (value[i] == '(')
      continue;
    else if (value[i] == ',' || value[i] == ')')
    {
      values.push_back((float)std::stod(number.c_str()));
      number.clear();
      continue;
    }
    number.push_back(value[i]);
  }
  Hcoords result(values[0], values[1], values[2], values[3]);
  return result;
}

Point2D Math::string_to_Point2D(const std::string &value)
{
  std::vector<float> values;
  std::string number;
  for (unsigned int i = 0; i < value.length(); ++i)
  {
    if (value[i] == '(')
      continue;
    else if (value[i] == ',' || value[i] == ')')
    {
      values.push_back((float)std::stod(number.c_str()));
      number.clear();
      continue;
    }
    number.push_back(value[i]);
  }
  Point2D result(values[0], values[1], values[2], values[3]);
  return result;
}

Vector2D Math::string_to_Vector2D(const std::string &value)
{
  std::vector<float> values;
  std::string number;
  for (unsigned int i = 0; i < value.length(); ++i)
  {
    if (value[i] == '<')
      continue;
    else if (value[i] == ',' || value[i] == '>')
    {
      values.push_back((float)std::stod(number.c_str()));
      number.clear();
      continue;
    }
    number.push_back(value[i]);
  }
  Vector2D result(values[0], values[1], values[2], values[3]);
  return result;
}


Affine Math::string_to_Affine(const std::string &value)
{
  std::vector<float> values;
  std::string number;
  for (unsigned int i = 0; i < value.length(); ++i)
  {
    if (value[i] == '[')
      continue;
    else if (value[i] == ',' || value[i] == ']')
    {
      if ((value[i] == ',' && value[i + 1] == '['))
        continue;
      else if (value[i] == ']' && value[i + 1] == ',')
      {
        values.push_back((float)std::stod(number.c_str()));
        number.clear();
        continue;
      }
      values.push_back((float)std::stoi(number.c_str()));
      number.clear();
      continue;
    }
    number.push_back(value[i]);
  }
  return Affine(Vector2D(values[0], values[1]), Vector2D(values[3], values[4]), Point2D(values[6], values[7]));
}

std::vector<int> Math::string_to_Map_vector(const std::string &value)
{
  std::vector<int> values;
  std::string number;
  int Width = 0;
  int Height = 0;


  for (unsigned int i = 0; i < value.length(); ++i)
  {
    if (value[i] == '[')
      continue;
    else if (value[i] == ',' || value[i] == ']')
    {
      if ((value[i] == ',' && value[i + 1] == '['))
      {
        ++Height;
        continue;
      }
      else if (value[i] == ']' && value[i + 1] == ',')
      {
        continue;
      }
      continue;
    }
    ++Width;
  }

  ++Height;
  Width = Width / Height;
  values.push_back(Width);
  values.push_back(Height);


  for (unsigned int i = 0; i < value.length(); ++i)
  {
    if (value[i] == '[')
      continue;
    else if (value[i] == ',' || value[i] == ']')
    {
      if ((value[i] == ',' && value[i + 1] == '['))
      {
        ++Height;
        continue;
      }
      else if (value[i] == ']' && value[i + 1] == ',')
      {
        values.push_back(std::stoi(number.c_str()));
        number.clear();
        continue;
      }
      values.push_back(std::stoi(number.c_str()));
      number.clear();
      continue;
    }
    ++Width;
    number.push_back(value[i]);
  }

  return values;
}




int** Math::string_to_Map_arr(const std::string &value)
{
  std::vector<int> values;
  std::string number;
  int **map;
  int Width = 0;
  int Height = 0;


  for (unsigned int i = 0; i < value.length(); ++i)
  {
    if (value[i] == '[')
      continue;
    else if (value[i] == ',' || value[i] == ']')
    {
      if ((value[i] == ',' && value[i + 1] == '['))
      {
        ++Height;
        continue;
      }
      else if (value[i] == ']' && value[i + 1] == ',')
      {
        values.push_back(std::stoi(number.c_str()));
        number.clear();
        continue;
      }
      values.push_back(std::stoi(number.c_str()));
      number.clear();
      continue;
    }
    ++Width;
    number.push_back(value[i]);
  }

  ++Height;
  Width = Width / Height;

  map = 0;
  return map;
}


std::ostream& Math::operator<<(std::ostream &out, const Vector2D &rhs)
{
  return  out << '<' << rhs.x << ',' << rhs.y << ',' << rhs.z << ',' << rhs.w << '>';
}

std::ostream& Math::operator<<(std::ostream& out, const Point2D& rhs)
{
  return (out << '(' << rhs.x << ',' << rhs.y << ',' << rhs.z << ',' << rhs.w << ')');
}

std::ostream& Math::operator<<(std::ostream& out, const Hcoords &rhs)
{
  return (out << '(' << rhs.x << ',' << rhs.y << ',' << rhs.z << ',' << rhs.w << ')');
}

std::ostream& Math::operator<<(std::ostream& out, const Affine &rhs)
{
  for (int i = 0; i < 3; ++i)
  {
    out << "[";
    for (int j = 0; j < 3; ++j)
    {
      out << rhs.matrix[i][j];

      if (j < 2)
        out << ",";
    }
    out << "]" << "\n";
  }
  return out;
}

std::ostream& Math::operator<<(std::ostream& out, const Matrix4 &rhs)
{
  for (int i = 0; i < 4; ++i)
  {
    out << "[";
    for (int j = 0; j < 4; ++j)
    {
      out << rhs.matrix4[i][j];

      if (j < 3)
        out << ",";
    }
    out << "]" << "\n";
  }
  return out;
}

void Math::VectorToDegree(const float& x, const float& y, float& degree)
{
  degree = std::atan2f(y, x)*_RAD2DEG_;
}

float Math::VectorToDegree(const Vector2D &vec)
{
  return std::atan2f(vec.y, vec.x) * _RAD2DEG_;
}

Vector2D Math::DegreeToVector(const float &deg)
{
  float rad = deg * _DEG2RAD_;
  return Vector2D(cosf(rad), sinf(rad));
}

/*
Clamps value between min and max.
guaranteed to return within [min, max] inclusively
*/
float Math::clamp(float value, float min, float max)
{
  if (value > max)
    return max;
  else if (value < min)
    return min;
  else
    return value;
}

/*
Wraps value around min and max. Useful for degrees.
guaranteed to return within [min, max)
*/
float Math::wrap(float value, float min, float max)
{
  // sanity checks
  if (min > max)
    std::swap(min, max);
  if (min == max)
    return min;

  float range = max - min;

  // too big
  while (value > max)
  {
    value -= range;
  }
  // too small
  while (value < min)
  {
    value += range;
  }

  return value;
}

// find angle between angle0 & angle1
float Math::DegreeBetween(float angle0, float angle1)
{
  const float minAngle = -180.0f, maxAngle = 180.0f;
  float angleDifference = angle1 - angle0;

  return wrap(angleDifference, minAngle, maxAngle);
}


// adds given amount of rotation to a vector, returns the degrees Vector is now at
float Math::RotateVector(Vector2D &vec, float deg)
{
  float endDeg;
  Math::VectorToDegree(vec.x, vec.y, endDeg);
  endDeg += deg;
  vec = Vector2D(cosf(endDeg * _DEG2RAD_), sinf(endDeg * _DEG2RAD_));
  return endDeg;
}