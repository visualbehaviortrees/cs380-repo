//	File name:		Math2D.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Kyle Philistine
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include <string.h>
#include <iostream>
#include <cmath>
#include <cassert>
#include <vector>
/*** NOTE FROM ALLAN: Always wrap math defines in parenthesis to prevent silly evaluation issues *****/
#define _PI_ (4.0f*atan(1.0f))
#define _DEG2RAD_ (4.0f*atan(1.0f)/180.0f)

#define _RAD2DEG_ (180.f / (4.0f*atan(1.0f)))

namespace Math
{
  struct Hcoords
  {
    float x, y, z, w;
    Hcoords(void);
    Hcoords(const float X, const float Y, const float Z = 0, const float W = 0);
    Hcoords(std::string value);

    static bool Near(float x, float y) { return std::abs(x - y) < 1e-5f; }
    void Set(const float X, const float Y, const float Z = 0.0f);

    // Hcoords + Hcoords
    Hcoords operator+(const Hcoords& rhs);

	// Hcoords + Hcoords
	Hcoords operator+(const Hcoords& rhs) const;

    // Hcoords - Hcoords
    Hcoords operator-(const Hcoords& rhs);
    // Hcoords * float
    Hcoords operator*(const float r);
    // -Hcoords (Negates it)
    Hcoords operator-(void);

    float DotProduct(const Hcoords &v);
    float Length(void);
	float Length(void) const;
    float SquareLength(void);
	float SquareLength(void) const;
  };

  struct Vector2D : Hcoords
  {
    Vector2D(void);
    Vector2D(const float X, const float Y, const float Z = 0, const float W = 0);
    Vector2D(const glm::vec2& v);
    Vector2D(const Hcoords& H);
    Vector2D(std::string value);
    void Zero(void);
    bool Normalize(void);
	Vector2D Normalize(void) const;


    void Scale(const float c);
    void ScaleAdd(const Vector2D &v, const float c);
    void ScaleSub(const Vector2D &v, const float c);

    float Angle(const Vector2D &v) const;

    float X(void) const;
    float Y(void) const;
    /*****************************************************************************/
    /************************* Operator overloads ********************************/
    /*****************************************************************************/
    Vector2D operator-() const;
    Vector2D operator-(const Vector2D &rhs) const;
    Vector2D operator+(const Vector2D &rhs) const;
    bool operator==(const Vector2D &rhs) const;
    // rotates the Vector2D by given degrees
    Vector2D operator%(const float degrees) const;
    Vector2D operator*(const float multiplier) const;
    float operator*(const Vector2D &rhs) const;
    Vector2D operator/(const float rhs) const;

    /*****************************************************************************/
    /*********************** Assignment Operators ********************************/
    /*****************************************************************************/

    Vector2D& operator+=(const Vector2D &rhs);
    Vector2D& operator+=(const float rhs);
    Vector2D& operator-=(const Vector2D &rhs);
    Vector2D& operator-=(const float rhs);
    Vector2D& operator*=(const float scalar);
    Vector2D& operator/=(const float divisor);
    Vector2D& operator%=(const float degrees);
  };

  struct Point2D : Hcoords
  {
    Point2D(const float X = 0.0f, const float Y = 0.0f, const float Z = 0.0f, const float W = 1.0f);
    Point2D(const Hcoords& Point2D);

    Point2D(const Point2D&);
    Point2D(const glm::vec2& v);

    float X(void) const;
    float Y(void) const;

    float operator[](int i) const;
    Point2D& operator=(const Point2D&);
    Point2D& operator+=(const Point2D&);
    Point2D& operator-=(const Point2D&);
    Point2D& operator*=(const float);

    float Distance(const Point2D &v);
    float SquareDistance(const Point2D &v);

    Point2D BarScale(float r);
    Point2D operator+(const Point2D& v);
    Point2D operator-(void);
    Point2D operator*(const float r);
    Vector2D operator-(const Point2D& v);
    float operator*(const Point2D& v);
    bool operator==(const Point2D& v);
    bool operator!=(const Point2D& v);
  };

  struct Affine
  {
    float matrix[3][3];
    Affine(void); // set everything to zero expect for the bottom left
    Affine(const Vector2D& LeftCol, const Vector2D& MiddleCol, const Point2D& RightCol);
    Affine(std::string value);

    Affine operator+(const Affine& B) const;
    Affine operator*(const Affine& B) const;
    // Affine * Hcoords
    Hcoords operator*(const Hcoords& B);
  };





  struct Matrix4
  {
    float matrix4[4][4];

    Matrix4(void);
    Matrix4(const Affine&);
    Matrix4(const Vector2D& LeftCol, const Vector2D& MiddleCol, const Point2D& P);
    Matrix4(const Vector2D& LeftCol, const Vector2D& MiddleCol, const Vector2D& RightCol, const Point2D& P);
    Matrix4(std::string value);

    Matrix4 operator+(const Matrix4& m4) const;
    Matrix4 operator*(const Matrix4& m4) const;
    Matrix4& operator=(const Affine& A);
  };

  struct Vector2DInt
  {
    int x, y;

    bool operator==(const Math::Vector2DInt& other) const
    {
      return x == other.x && y == other.y;
    }

    Vector2DInt(int x_ = 0, int y_ = 0){
      x = x_; y = y_;
    }

    Vector2DInt operator*(const Math::Vector2DInt& other) const
    {
      return Vector2DInt(x * other.x, y * other.y);
    }

    Vector2DInt operator-(const Math::Vector2DInt& other) const
    {
      return Vector2DInt(x - other.x, y - other.y);
    }

  };

  void VectorToDegree(const float& x, const float& y, float& degree);
  float VectorToDegree(const Vector2D &vec);
  Vector2D DegreeToVector(const float &deg);
  float DegreeBetween(float angle0, float angle1);
  float RotateVector(Vector2D &vec, float deg);

  float dot(const Hcoords& u, const Hcoords& v);
  float abs(const Vector2D& v);
  float abs(const float &f);

  float clamp(float value, float min, float max);
  float wrap(float value, float min, float max);

  Affine Rot(float t);
  Affine Trans(const Point2D& p);
  Affine Scale(const float r);
  Affine Scale(const float rx, const float ry);
  Affine Inverse(const Affine& A);
  Matrix4 Inverse(const Matrix4& A);
  Point2D operator*(float r, const Point2D& u);
  Point2D GetMousePosition(void);

  Hcoords string_to_Hcoords(const std::string  &value);
  Point2D string_to_Point2D(const std::string &value);
  Vector2D string_to_Vector2D(const std::string &value);
  Affine string_to_Affine(const std::string &value);
  int** string_to_Map_arr(const std::string &value);
  std::vector<int> string_to_Map_vector(const std::string &value);

  std::ostream& operator<<(std::ostream& out, const Point2D& rhs);
  std::ostream& operator<<(std::ostream& out, const Matrix4 &rhs);
  std::ostream& operator<<(std::ostream& out, const Affine &rhs);
  std::ostream& operator<<(std::ostream& out, const Hcoords &rhs);
  std::ostream& operator<<(std::ostream& out, const Vector2D &rhs);
};