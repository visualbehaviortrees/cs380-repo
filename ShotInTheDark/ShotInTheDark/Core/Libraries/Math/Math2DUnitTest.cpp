//	File name:		Math2DUnitTest.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Kyle Philistine
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include <fstream>
#include <sstream>
#include <cstring>

#include <iostream>
#include <iomanip>
#include "Math2D.hpp"

using namespace Math;
using namespace std;

static ofstream myfile;


void TestHcoords(void)
{
    myfile << "\n********** TestHcoords **********\n";
  {
    myfile << "|###########################################|\n";
    myfile << "Hcoords Test #1 (Default Construct)" << "\n";

    Hcoords H1;
    myfile << "H1 = " << H1 << "\n";
  }

  {
    myfile << "\n|###########################################|\n";
    myfile << "Hcoords Test #2 (Non-Default Construct)" << "\n";
    Hcoords H1(1, 2), H2 (-3, 4), H3 (-5, -6), H4 (7, -8);

    myfile << "H1 = " << H1 << "\n";
    myfile << "H2 = " << H2 << "\n";
    myfile << "H3 = " << H3 << "\n";
    myfile << "H4 = " << H4 << "\n";
  }

  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Hcoords Test #3 (Hcoords + Hcoords) PART1" << "\n";

    Hcoords H1(50, 100),  H2(25, 25);


    myfile << "\n" << "H1 = " << H1 << "\n" << "H2 = " << H2 << "\n";
    myfile << "\n" << "H3 = " << "H1" << H1 << " + " << "H2" << H2 << "\n";

    Hcoords H3 = H1 + H2;

    myfile << "H3 = " << H3 << "\n\n";

    myfile << "PART2 Hcoords Test #3 (Hcoords + Hcoords) PART2" << "\n";


    Hcoords H4(-25,-25);

    myfile << "\n" << "H3 = " << H3 << "\n" << "H4 = " << H4 << "\n";
    myfile << "\n" << "H5 = " << "H3" << H3 << " + " << "H4" << H4 << "\n";


    Hcoords H5 = H3 + H4;
    myfile << "H5 = " << H5 << "\n\n";

  }

  {
    myfile << "|###########################################|\n";
    myfile << "PART1 Hcoords Test #4 (Hcoords - Hcoords) PART1" << "\n";

    Hcoords H1(50, 100), H2(25, 25);

    myfile << "\n" << "H1 = " << H1 << "\n" << "H2 = " << H2 << "\n";
    myfile << "\n" << "H3 = " << "H1" << H1 << " - " << "H2" << H2 << "\n";

    Hcoords H3 = H1 - H2;
    myfile << "H3 = " << H3 << "\n";

    myfile << "\nPART2 Hcoords Test #4 (Hcoords - Hcoords) PART2" << "\n";

    Hcoords H4(-25,-25);

    myfile << "\n" << "H3 = " << H3 << "\n" << "H4 = " << H4 << "\n";
    myfile << "\n" << "H5 = " << "H3" << H3 << " - " << "H4" << H4 << "\n";

    Hcoords H5 = H3 - H4;
    myfile << "H5 = " << H5 << "\n";
  }
 
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Hcoords Test #5 (Hcoords * float) PART1" << "\n";

    Hcoords H1(1,1);
    float r = 2.00f;

    myfile << "\n" << "H1 = " << H1 << "\n" << "r = " << r << "\n";
    myfile << "\n" << "H2 = " << "H1" << H1 << " * " << fixed << r << "\n";

    Hcoords H2 = H1 * r;
    myfile << "H2 = " << H2 << "\n";


    myfile << "\nPART2 Hcoords Test #5 (Hcoords * float) PART2" << "\n";

    H1 = Hcoords(5,2);
    r = 3.14f;

    myfile << "\n" << "H1 = " << H1 << "\n" << "r = " << r << "\n";
    myfile << "\n" << "H2 = " << "H1" << H1 << " * " << fixed << r << "\n";

    H2 = H1 * r;
    myfile << "H2 = " << H2 << "\n";
  }

  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Hcoords Test #6 (Hcoord Negated) PART1" << "\n";

    Hcoords H1(1,1);
    myfile << "\n" << "H1 = " << H1 << "\n";
    myfile << "H1 Negated = " << -H1 << "\n";

    myfile << "\nPART2 Hcoords Test #6 (Hcoord Negated) PART2" << "\n";

    H1 = Hcoords(-1,-1);
    myfile << "\n" << "H1 = " << H1 << "\n";
    myfile << "H1 Negated = " << -H1 << "\n";
  }
}









void TestVector2Ds(void)
{
  myfile << "\n********** TestVector2Ds **********\n";

  {
    myfile << "\n|###########################################|\n";
    myfile << "Vector Test #1 (Default Constructor)" << "\n";

    Vector2D X;
    myfile << "X = " << X << "\n";
  }

  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Vector Test #1 (Non-Default Constructor) PART1" << "\n";

    Vector2D X(1,1), Y(1,1,1), Z(1,1,1,1);

    myfile << "X(1,1)" << "\n";
    myfile << "X = " << X << "\n\n";

    myfile << "Y(1,1,1)" << "\n";
    myfile << "Y = " << Y << "\n\n";

    myfile << "Z(1,1,1,1)" << "\n";
    myfile << "Z = " << Z << "\n\n";

    myfile << "PART2 Vector Test #1 (Non-Default Constructor (HCOORDS)) PART2" << "\n\n";
    X = Vector2D(Hcoords(1,1));
    Y = Vector2D(Hcoords(1,1,1));
    Z = Vector2D(Hcoords(1,1,1,1));

    myfile << "X(1,1)" << "\n";
    myfile << "X = " << X << "\n\n";

    myfile << "Y(1,1,1)" << "\n";
    myfile << "Y = " << Y << "\n\n";

    myfile << "Z(1,1,1,1)" << "\n";
    myfile << "Z = " << Z << "\n";
  }

  {
    myfile << "\n|###########################################|\n";
    myfile << "Vector Test #1 (Zero Vector)" << "\n\n";

    Vector2D X(1,1), Y(2,2), Z(3,3);

    myfile << "X = " << X << "\n";
    X.Zero();
    myfile << "Zero X = " << X << "\n\n";

    myfile << "Y = " << Y << "\n";
    Y.Zero();
    myfile << "Zero Y = " << Y << "\n\n";

    myfile << "Z = " << Z << "\n";
    Z.Zero();
    myfile << "Zero Z = " << Z << "\n";
  }

  {
    myfile << "\n|###########################################|\n";
    myfile << "Vector Test #1 (Normalize Vector)" << "\n\n";

    Vector2D X(2,1), Y(2,-1), Z(-2,-1), W(-2, 1);

    myfile << "X = " << X << "\n";
    X.Normalize();
    myfile << "Normalized X = " << X << "\n\n";

    myfile << "Y = " << Y << "\n";
    Y.Normalize();
    myfile << "Normalized Y = " << Y << "\n\n";

    myfile << "Z = " << Z << "\n";
    Z.Normalize();
    myfile << "Normalized Z = " << Z << "\n\n";

    myfile << "W = " << W << "\n";
    W.Normalize();
    myfile << "Normalized W = " << W << "\n\n";
  }

  {
    myfile << "\n|###########################################|\n";
    myfile << "Vector Test #1 (Set Vector)" << "\n\n";
    Vector2D X, Y, Z, W;

    myfile << "X = " << X << "\n";
    X.Set(1,1);
    myfile << "Set X = " << X << "\n\n";

    myfile << "Y = " << Y << "\n";
    Y.Set(2,2);
    myfile << "Set Y = " << Y << "\n\n";

    myfile << "Z = " << Z << "\n";
    Z.Set(3,3);
    myfile << "Set Z = " << Z << "\n\n";

    myfile << "W = " << W << "\n";
    W.Set(4,4);
    myfile << "Set W = " << W << "\n\n";
  }

  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Vector Test #1 (Vector Scale(float)) PART1" << "\n\n";

    Vector2D X, Y, Z, W;

    myfile << "X = " << X << "\n";
    X.Scale(2.0f);
    myfile << "Scale X = " << X << "\n\n";

    myfile << "Y = " << Y << "\n";
    Y.Scale(3.0f);
    myfile << "Scale Y = " << Y << "\n\n";

    myfile << "Z = " << Z << "\n";
    Z.Scale(4.0f);
    myfile << "Scale Z = " << Z << "\n\n";

    myfile << "W = " << W << "\n";
    W.Scale(5.0);
    myfile << "Scale W = " << W << "\n\n";


    myfile << "PART2 Vector Test #1 (Vector Scale(float)) PART2" << "\n\n";

    X = Vector2D(1,1);
    Y = Vector2D(2,2);
    Z = Vector2D(3,3);
    W = Vector2D(4,4);

    myfile << "X = " << X << "\n";
    X.Scale(2.0f);
    myfile << "Scale X = " << X << "\n\n";

    myfile << "Y = " << Y << "\n";
    Y.Scale(3.0f);
    myfile << "Scale Y = " << Y << "\n\n";

    myfile << "Z = " << Z << "\n";
    Z.Scale(4.0f);
    myfile << "Scale Z = " << Z << "\n\n";

    myfile << "W = " << W << "\n";
    W.Scale(5.0);
    myfile << "Scale W = " << W << "\n\n";
  }

  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Vector Test #1 (Vector ScaleAdd) PART1" << "\n\n";

    Vector2D X, Y, Z, W;

    myfile << "X = " << X << "\n";
    X.ScaleAdd(X, 2.0f);
    myfile << "Scale X = " << X << "\n\n";

    myfile << "Y = " << Y << "\n";
    Y.ScaleAdd(Y, 2.0f);
    myfile << "Scale Y = " << Y << "\n\n";

    myfile << "Z = " << Z << "\n";
    Z.ScaleAdd(Z, 2.0f);
    myfile << "Scale Z = " << Z << "\n\n";

    myfile << "W = " << W << "\n";
    W.ScaleAdd(W, 2.0f);
    myfile << "Scale W = " << W << "\n\n";


    myfile << "PART2 Vector Test #1 (Vector ScaleAdd) PART2" << "\n\n";

    X = Vector2D(1,1);
    Y = Vector2D(2,2);
    Z = Vector2D(3,3);
    W = Vector2D(4,4);

    myfile << "X = " << X << "\n";
    X.ScaleAdd(X, 2.0f);
    myfile << "Scale X = " << X << "\n\n";

    myfile << "Y = " << Y << "\n";
    Y.ScaleAdd(Y, 2.0f);
    myfile << "Scale Y = " << Y << "\n\n";

    myfile << "Z = " << Z << "\n";
    Z.ScaleAdd(Z, 2.0f);
    myfile << "Scale Z = " << Z << "\n\n";

    myfile << "W = " << W << "\n";
    W.ScaleAdd(W, 2.0f);
    myfile << "Scale W = " << W << "\n\n";


    myfile << "PART3 Vector Test #1 (Vector ScaleAdd) PART3" << "\n\n";

    Vector2D A(-3,-2), B(-3, 2), C(3,-2), D(3,2);

    X = Vector2D(1,1);
    Y = Vector2D(2,2);
    Z = Vector2D(3,3);
    W = Vector2D(4,4);

    myfile << "X = " << X << "\n";
    X.ScaleAdd(A, 2.0f);
    myfile << "Scale X = " << X << "\n\n";

    myfile << "Y = " << Y << "\n";
    Y.ScaleAdd(B, 2.0f);
    myfile << "Scale Y = " << Y << "\n\n";

    myfile << "Z = " << Z << "\n";
    Z.ScaleAdd(C, 2.0f);
    myfile << "Scale Z = " << Z << "\n\n";

    myfile << "W = " << W << "\n";
    W.ScaleAdd(D, 2.0f);
    myfile << "Scale W = " << W << "\n\n";
  }

  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Vector Test #1 (Vector ScaleSub) PART1" << "\n\n";


    Vector2D X, Y, Z, W;

    myfile << "X = " << X << "\n";
    X.ScaleSub(X, 2.0f);
    myfile << "Scale X = " << X << "\n\n";

    myfile << "Y = " << Y << "\n";
    Y.ScaleSub(Y, 2.0f);
    myfile << "Scale Y = " << Y << "\n\n";

    myfile << "Z = " << Z << "\n";
    Z.ScaleSub(Z, 2.0f);
    myfile << "Scale Z = " << Z << "\n\n";

    myfile << "W = " << W << "\n";
    W.ScaleSub(W, 2.0f);
    myfile << "Scale W = " << W << "\n\n";


    myfile << "PART2 Vector Test #1 (Vector ScaleSub) PART2" << "\n\n";

    X = Vector2D(1,1);
    Y = Vector2D(2,2);
    Z = Vector2D(3,3);
    W = Vector2D(4,4);

    myfile << "X = " << X << "\n";
    X.ScaleSub(X, 2.0f);
    myfile << "Scale X = " << X << "\n\n";

    myfile << "Y = " << Y << "\n";
    Y.ScaleSub(Y, 2.0f);
    myfile << "Scale Y = " << Y << "\n\n";

    myfile << "Z = " << Z << "\n";
    Z.ScaleSub(Z, 2.0f);
    myfile << "Scale Z = " << Z << "\n\n";

    myfile << "W = " << W << "\n";
    W.ScaleSub(W, 2.0f);
    myfile << "Scale W = " << W << "\n\n";


    myfile << "PART3 Vector Test #1 (Vector ScaleSub) PART3" << "\n\n";

    Vector2D A(-3,-2), B(-3, 2), C(3,-2), D(3,2);

    X = Vector2D(1,1);
    Y = Vector2D(2,2);
    Z = Vector2D(3,3);
    W = Vector2D(4,4);


    myfile << "X = " << X << "\n";
    X.ScaleSub(A, 2.0f);
    myfile << "Scale X = " << X << "\n\n";

    myfile << "Y = " << Y << "\n";
    Y.ScaleSub(B, 2.0f);
    myfile << "Scale Y = " << Y << "\n\n";

    myfile << "Z = " << Z << "\n";
    Z.ScaleSub(C, 2.0f);
    myfile << "Scale Z = " << Z << "\n\n";

    myfile << "W = " << W << "\n";
    W.ScaleSub(D, 2.0f);
    myfile << "Scale W = " << W << "\n\n";
  }

  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Vector Test #1 (Vector Length) PART1" << "\n\n";

    Vector2D X, Y;
    float temp = 0;

    myfile << "X = " << X << "\n";
    temp = X.Length();
    myfile << "Lenght of X = " << temp << "\n\n";

    myfile << "Y = " << Y << "\n";
    temp = Y.Length();
    myfile << "Length of Y = " << temp << "\n\n";


    myfile << "PART2 Vector Test #1 (Vector Length) PART2" << "\n\n";

    X = Vector2D(2,3);
    Y = Vector2D(4,1);

    myfile << "X = " << X << "\n";
    temp = X.Length();
    myfile << "Lenght of X = " << temp << "\n\n";

    myfile << "Y = " << Y << "\n";
    temp = Y.Length();
    myfile << "Lenght Y = " << temp << "\n\n";


    myfile << "PART3 Vector Test #3 (Vector Length) PART3" << "\n\n";

    X = Vector2D(-2,-3);
    Y = Vector2D(-4,1);
    Vector2D Z(3, -1);


    myfile << "X = " << X << "\n";
    temp = X.Length();
    myfile << "Lenght of X = " << temp << "\n\n";

    myfile << "Y = " << Y << "\n";
    temp = Y.Length();
    myfile << "Lenght Y = " << temp << "\n\n"; 

    myfile << "Z = " << Z << "\n";
    temp = Z.Length();
    myfile << "Lenght Z = " << temp << "\n\n";
  }

  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Vector Test #1 (Vector SquareLength) PART1" << "\n\n";

    Vector2D X, Y;
    float temp = 0;

    myfile << "X = " << X << "\n";
    temp = X.SquareLength();
    myfile << "SquareLength of X = " << temp << "\n\n";

    myfile << "Y = " << Y << "\n";
    temp = Y.SquareLength();
    myfile << "SquareLength of Y = " << temp << "\n\n";

    myfile << "PART2 Vector Test #1 (Vector SquareLength) PART2" << "\n\n";

    X = Vector2D(2,3);
    Y = Vector2D(4,1);

    myfile << "X = " << X << "\n";
    temp = X.SquareLength();
    myfile << "SquareLength of X = " << temp << "\n\n";

    myfile << "Y = " << Y << "\n";
    temp = Y.SquareLength();
    myfile << "SquareLength Y = " << temp << "\n\n";

    myfile << "PART3 Vector Test #1 (Vector SquareLength) PART3" << "\n\n";

    X = Vector2D(-2,-3);
    Y = Vector2D(-4,1);
    Vector2D Z(3, -1);


    myfile << "X = " << X << "\n";
    temp = X.SquareLength();
    myfile << "SquareLength of X = " << temp << "\n\n";

    myfile << "Y = " << Y << "\n";
    temp = Y.SquareLength();
    myfile << "SquareLength Y = " << temp << "\n\n"; 

    myfile << "Z = " << Z << "\n";
    temp = Z.SquareLength();
    myfile << "SquareLength Z = " << temp << "\n\n";
  }

  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 POINT!!! Test #1 (POINT!!! Distance) PART1" << "\n\n";

    Point2D X, Y;
    float temp = 0;

    myfile << "X = " << X << "\n";
    temp = X.Distance(X);
    myfile << "Distance of X to X = " << temp << "\n\n";

    myfile << "Y = " << Y << "\n";
    temp = Y.Distance(Y);
    myfile << "Distance of Y to Y = " << temp << "\n\n";


    myfile << "PART2 Vector Test #1 (Vector Distance) PART2" << "\n\n";

    X = Point2D(-2, -3);
    Y = Point2D(-4, 1);
    Point2D Z(3, -1);

    myfile << "X = " << X << "\n" << "Y = " << Y << "\n";
    temp = X.Distance(Y);
    myfile << "Distance from X to Y = " << temp << "\n\n";

    myfile << "X = " << X << "\n" << "Z = " << Z << "\n";
    temp = X.Distance(Z);
    myfile << "Distance from X to Z = " << temp << "\n\n";

    myfile << "Y = " << Y << "\n" << "X = " << X << "\n";
    temp = Y.Distance(X);
    myfile << "Distance from Y to X = " << temp << "\n\n";

    myfile << "Y = " << Y << "\n" << "Z = " << Z << "\n";
    temp = Y.Distance(Z);
    myfile << "Distance from Y to Z = " << temp << "\n\n";

    myfile << "Z = " << Z << "\n" << "Y = " << Y << "\n";
    temp = Z.Distance(Y);
    myfile << "Distance from Z to Y = " << temp << "\n\n";

    myfile << "Z = " << Z << "\n" << "X = " << X << "\n";
    temp = Z.Distance(X);
    myfile << "Distance from Z to X = " << temp << "\n\n";
  }


  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Vector Test #1 (Vector SquareDistance) PART1" << "\n\n";

    Point2D X, Y;
    float temp = 0;

    myfile << "X = " << X << "\n";
    temp = X.SquareDistance(X);
    myfile << "SquareDistance of X to X = " << temp << "\n\n";

    myfile << "Y = " << Y << "\n";
    temp = Y.SquareDistance(Y);
    myfile << "SquareDistance of Y to Y = " << temp << "\n\n";


    myfile << "PART3 Vector Test #1 (Vector SquareDistance) PART3" << "\n\n";

    X = Point2D(-2, -3);
    Y = Point2D(-4, 1);
    Point2D Z(3, -1);

    myfile << "X = " << X << "\n" << "Y = " << Y << "\n";
    temp = X.SquareDistance(Y);
    myfile << "SquareDistance from X to Y = " << temp << "\n\n";

    myfile << "X = " << X << "\n" << "Z = " << Z << "\n";
    temp = X.SquareDistance(Z);
    myfile << "SquareDistance from X to Z = " << temp << "\n\n";

    myfile << "Y = " << Y << "\n" << "X = " << X << "\n";
    temp = Y.SquareDistance(X);
    myfile << "SquareDistance from Y to X = " << temp << "\n\n";

    myfile << "Y = " << Y << "\n" << "Z = " << Z << "\n";
    temp = Y.SquareDistance(Z);
    myfile << "SquareDistance from Y to Z = " << temp << "\n\n";

    myfile << "Z = " << Z << "\n" << "Y = " << Y << "\n";
    temp = Z.SquareDistance(Y);
    myfile << "SquareDistance from Z to Y = " << temp << "\n\n";

    myfile << "Z = " << Z << "\n" << "X = " << X << "\n";
    temp = Z.SquareDistance(X);
    myfile << "SquareDistance from Z to X = " << temp << "\n\n";
  }

  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Vector Test #1 (Vector DotProduct) PART1" << "\n\n";

    Vector2D X, Y;
    float temp = 0;

    myfile << "X = " << X << "\n";
    X.DotProduct(X);
    myfile << "X Dot X = " << temp << "\n\n";

    myfile << "Y = " << Y << "\n";
    temp = Y.DotProduct(Y);
    myfile << "Y Dot Y = " << temp << "\n\n";


    myfile << "PART2 Vector Test #1 (Vector DotProduct) PART2" << "\n\n";

    X = Vector2D(-1,-1);
    Y = Vector2D(2,2);
    Vector2D Z(3,-3), W(-4,4);

    myfile << "X = " << X << "\n" << "Y = " << Y << "\n";
    temp = X.DotProduct(Y);
    myfile << "X Dot Y = " << temp << "\n\n";

    myfile << "X = " << X << "\n" << "Z = " << Z << "\n";
    temp = X.DotProduct(Z);
    myfile << "X Dot Z = " << temp << "\n\n";

    myfile << "Y = " << Y << "\n" << "X = " << X << "\n";
    temp = Y.DotProduct(X);
    myfile << "Y Dot to X = " << temp << "\n\n";

    myfile << "Y = " << Y << "\n" << "Z = " << Z << "\n";
    temp = Y.DotProduct(Z);
    myfile << "Y Dot Z = " << temp << "\n\n";

    myfile << "W = " << W << "\n" << "Z = " << Z << "\n";
    temp = W.DotProduct(Z);
    myfile << "W Dot Z = " << temp << "\n\n";

    myfile << "Z = " << Z << "\n" << "W = " << W << "\n";
    temp = Z.DotProduct(W);
    myfile << "Z Dot W = " << temp << "\n\n";
  }

  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Vector Test #1 (Vector Gettors) PART1" << "\n\n";

    Vector2D X;
    float temp = 0;

    myfile << "X = " << X << "\n";
    temp = X.X();
    myfile << "Vector2D x value = " << temp << "\n\n";
    temp = X.Y();
    myfile << "Vector2D y value = " << temp << "\n\n";


    myfile << "PART2 Vector Test #1 (Vector Gettors) PART2" << "\n\n";

    Vector2D Y(3, 5);


    myfile << "Y = " << Y << "\n";
    temp = Y.X();
    myfile << "Vector2D x value = " << temp << "\n\n";
    temp = Y.Y();
    myfile << "Vector2D y value = " << temp << "\n\n";
  }
  
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Vector Test #1 (Vector Negating) PART1" << "\n\n";

    Vector2D X;

    myfile << "X = " << X << "\n" << "Negated X = " << -X << "\n\n";

    myfile << "PART2 Vector Test #1 (Vector Negating) PART2" << "\n\n";
    Vector2D Y(3, 5), W(-3, -5), Z(-3, 5), A(3, -5);

    myfile << "Y = " << Y << "\n" << "Negated Y = " << -Y << "\n\n";
    myfile << "W = " << W << "\n" << "Negated W = " << -W << "\n\n";
    myfile << "Z = " << Z << "\n" << "Negated W = " << -Z << "\n\n";
    myfile << "A = " << A << "\n" << "Negated W = " << -A << "\n\n";
  }

  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Vector Test #1 (Vector - Vector) PART1" << "\n\n";

    Vector2D A, B;

    myfile << "A = " << A << "\n" << "B = " << B << "\n";
    myfile << "A - B = " << A - B << "\n\n" << "B - A = " << B - A << "\n\n";
    myfile << "A - A = " << A - A << "\n\n" << "B - B = " << B - B << "\n\n";

    myfile << "PART2 Vector Test #1 (Vector - Vector) PART2" << "\n\n";

    Vector2D X(2,1), Y(3,2), Z(-5, -5), W(-2, 3);




    myfile << "X = " << X << "\n" << "Y = " << Y << "\n" << "Z = " << Z << "\n" << "W = " << W << "\n\n";
    myfile << "X - Y = " << X - Y << "\n\n" << "Y - X = " << Y - X << "\n\n";
    myfile << "Z - W = " << Z - W << "\n\n" << "W - Z = " << W - Z << "\n\n";
    myfile << "W - X = " << W - X << "\n\n" << "W - Y = " << W - Y << "\n\n";
    myfile << "Z - X = " << Z - X << "\n\n" << "Y - W = " << Y - W << "\n\n";
  }

  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Vector Test #1 (Vector + Vector) PART1" << "\n\n";

    Vector2D A, B;

    myfile << "A = " << A << "\n" << "B = " << B << "\n";
    myfile << "A + B = " << A + B << "\n\n" << "B + A = " << B + A << "\n\n";
    myfile << "A + A = " << A + A << "\n\n" << "B + B = " << B + B << "\n\n";

    myfile << "PART2 Vector Test #1 (Vector + Vector) PART2" << "\n\n";

    Vector2D X(2,1), Y(3,2), Z(-5, -5), W(-2, 3);

    myfile << "X = " << X << "\n" << "Y = " << Y << "\n" << "Z = " << Z << "\n" << "W = " << W << "\n\n";
    myfile << "X + Y = " << X + Y << "\n\n" << "Y + X = " << Y + X << "\n\n";
    myfile << "Z + W = " << Z + W << "\n\n" << "W + Z = " << W + Z << "\n\n";
    myfile << "W + X = " << W + X << "\n\n" << "W + Y = " << W + Y << "\n\n";
    myfile << "Z + X = " << Z + X << "\n\n" << "Y + W = " << Y + W << "\n\n";
  }

  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Vector Test #1 (Vector == Vector?) PART1" << "\n\n";

    Vector2D X(1,1), Y(1,1), Z(2,2);

    myfile << "X = " << X << "\n" << "Y = " << Y << "\n" <<"Z = " << Z << "\n";
    myfile << "Does X == Y? ";

    if(X == Y)
      myfile << "YES\n";
    else
      myfile << "NO\n";

    myfile << "Does X == Z? ";

    if(X == Z)
      myfile << "YES\n";
    else
      myfile << "NO\n";
  }

  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Vector Test #1 (Rotating a Vector by Degrees) PART1" << "\n\n";

    Vector2D X(2,3);  Vector2D Y(-2, 1); Vector2D Z(3,-2);

    float temp = 90; float temp1 = 45; float temp2 = 25;

    myfile << "X = " << X << "\n" << "Y = " << Y << "\n" <<"Z = " << Z << "\n";
    myfile << "X rotated 90 deg = " << X % temp << "\n" << "X rotated 45 deg = " << X % temp1 << "\n" << "X rotated 25 deg = " << X % temp2 << "\n\n";
    myfile << "Y rotated 90 deg = " << Y % temp << "\n" << "Y rotated 45 deg = " << Y % temp1 << "\n" << "Y rotated 25 deg = " << Y % temp2 << "\n\n";
    myfile << "Z rotated 90 deg = " << Z % temp << "\n" << "Z rotated 45 deg = " << Z % temp1 << "\n" << "Z rotated 25 deg = " << Z % temp2 << "\n\n";


    myfile << "PART2 Vector Test #1 (Rotating a Vector by Degrees) PART2" << "\n\n";

    temp = -90; temp1 = -45; temp2 = -25;

    myfile << "X rotated -90 deg = " << X % temp << "\n" << "X rotated -45 deg = " << X % temp1 << "\n" << "X rotated -25 deg = " << X % temp2 << "\n\n";
    myfile << "Y rotated -90 deg = " << Y % temp << "\n" << "Y rotated -45 deg = " << Y % temp1 << "\n" << "Y rotated -25 deg = " << Y % temp2 << "\n\n";
    myfile << "Z rotated -90 deg = " << Z % temp << "\n" << "Z rotated -45 deg = " << Z % temp1 << "\n" << "Z rotated -25 deg = " << Z % temp2 << "\n\n";
                       
  }

  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Vector Test #1 (Vector * scalar) PART1" << "\n\n";

    Vector2D X(1,1), Y(2,2), Z(3,3), W(4,4);
    float r = 2.0f;

    myfile << "X = " << X << "\n" << "Y = " << Y << "\n" << "Z = " << Z << "\n" << "W = " << W << "\n\n";

    myfile << "X * r = " << (X * r) << "\n\n" << "Y * r = " << (Y * r) << "\n\n";
    myfile << "Z * r = " << (Z * r) << "\n\n" << "W * r = " << (W * r) << "\n\n";

  }


  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Vector Test #1 (scalar * Vector) PART1" << "\n\n";

    Vector2D X(1,1), Y(2,2), Z(3,3), W(4,4);
    float r = 2.0f;

    myfile << "X = " << X << "\n" << "Y = " << Y << "\n" << "Z = " << Z << "\n" << "W = " << W << "\n\n";

    myfile << "r * X = " << (r * X) << "\n\n" << "r * Y = " << (r * Y) << "\n\n";
    myfile << "r * Z = " << (r * Z) << "\n\n" << "r * W = " << (r * W) << "\n\n";
  }


  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Vector Test #1 (Vector / float) PART1" << "\n\n";

    Vector2D X(1,1), Y(2,2), Z(3,3), W(4,4);
    float r = 2.0f;

    myfile << "X = " << X << "\n" << "Y = " << Y << "\n" << "Z = " << Z << "\n" << "W = " << W << "\n\n";

    myfile << "X / r = " << (X / r) << "\n\n" << "Y / r = " << (Y / r) << "\n\n";
    myfile << "Z / r = " << (Z / r) << "\n\n" << "W / r = " << (W / r) << "\n\n";
  }

  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Vector Test #1 (Vector += Vector) PART1" << "\n\n";

    Vector2D X(1,1), Y(2,2), Z(3,3), W(4,4);

    myfile << "X = " << X << "\n" << "Y = " << Y << "\n" << "Z = " << Z << "\n" << "W = " << W << "\n\n";

    myfile << "X += X = " << (X += X) << "\n\n" << "Y += Y = " << (Y += Y) << "\n\n";
    myfile << "Z += Z = " << (Z += Z) << "\n\n" << "W += W = " << (W += W) << "\n\n";

    myfile << "X = " << X << "\n" << "Y = " << Y << "\n" << "Z = " << Z << "\n" << "W = " << W << "\n\n";
  }

  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Vector Test #1 (Vector += float) PART1" << "\n\n";

    Vector2D X(1,1), Y(2,2), Z(3,3), W(4,4);
    float r = 2.0f;

    myfile << "X = " << X << "\n" << "Y = " << Y << "\n" << "Z = " << Z << "\n" << "W = " << W << "\n\n";

    myfile << "X += r = " << (X += r) << "\n\n" << "Y += r = " << (Y += r) << "\n\n";
    myfile << "Z += r = " << (Z += r) << "\n\n" << "W += r = " << (W += r) << "\n\n";

    myfile << "X = " << X << "\n" << "Y = " << Y << "\n" << "Z = " << Z << "\n" << "W = " << W << "\n\n";
  }

  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Vector Test #1 (Vector -= Vector) PART1" << "\n\n";

    Vector2D X(1,1), Y(2,2), Z(3,3), W(4,4);

    myfile << "X = " << X << "\n" << "Y = " << Y << "\n" << "Z = " << Z << "\n" << "W = " << W << "\n\n";

    myfile << "X -= X = " << (X -= X) << "\n\n" << "Y -= Y = " << (Y -= Y) << "\n\n";
    myfile << "Z -= Z = " << (Z -= Z) << "\n\n" << "W -= W = " << (W -= W) << "\n\n";

    myfile << "X = " << X << "\n" << "Y = " << Y << "\n" << "Z = " << Z << "\n" << "W = " << W << "\n\n";

  }


  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Vector Test #1 (Vector -= float) PART1" << "\n\n";

    Vector2D X(1,1), Y(2,2), Z(3,3), W(4,4);
    float r = 2.0f;

    myfile << "X = " << X << "\n" << "Y = " << Y << "\n" << "Z = " << Z << "\n" << "W = " << W << "\n\n";

    myfile << "X -= r = " << (X -= r) << "\n\n" << "Y -= r = " << (Y -= r) << "\n\n";
    myfile << "Z -= r = " << (Z -= r) << "\n\n" << "W -= r = " << (W -= r) << "\n\n";

    myfile << "X = " << X << "\n" << "Y = " << Y << "\n" << "Z = " << Z << "\n" << "W = " << W << "\n\n";
  }


  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Vector Test #1 (Vector *= float) PART1" << "\n\n";

    Vector2D X(1,1), Y(2,2), Z(3,3), W(4,4);
    float r = 2.0f;

    myfile << "X = " << X << "\n" << "Y = " << Y << "\n" << "Z = " << Z << "\n" << "W = " << W << "\n\n";

    myfile << "X *= r = " << (X *= r) << "\n\n" << "Y *= r = " << (Y *= r) << "\n\n";
    myfile << "Z *= r = " << (Z *= r) << "\n\n" << "W *= r = " << (W *= r) << "\n\n";

    myfile << "X = " << X << "\n" << "Y = " << Y << "\n" << "Z = " << Z << "\n" << "W = " << W << "\n\n";
  }

  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Vector Test #1 (Vector /= float) PART1" << "\n\n";

    Vector2D X(1,1), Y(2,2), Z(3,3), W(4,4);
    float r = 2.0f;

    myfile << "X = " << X << "\n" << "Y = " << Y << "\n" << "Z = " << Z << "\n" << "W = " << W << "\n\n";

    myfile << "X /= r = " << (X /= r) << "\n\n" << "Y /= r = " << (Y /= r) << "\n\n";
    myfile << "Z /= r = " << (Z /= r) << "\n\n" << "W /= r = " << (W /= r) << "\n\n";

    myfile << "X = " << X << "\n" << "Y = " << Y << "\n" << "Z = " << Z << "\n" << "W = " << W << "\n\n";
  }


  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Vector Test #1 (Vector %= Degrees) PART1" << "\n\n";

    Vector2D X(1,1), Y(2,2), Z(3,3), W(4,4);

    float temp = 90;

    myfile << "X = " << X << "\n" << "Y = " << Y << "\n" << "Z = " << Z << "\n";
    myfile << "X rotated 90 deg = " << (X %= temp) << "\n";
    myfile << "Y rotated 90 deg = " << (Y %= temp) << "\n";
    myfile << "Z rotated 90 deg = " << (Z %= temp) << "\n";

    myfile << "PART2 Vector Test #1 (Rotating a Vector by Degrees) PART2" << "\n\n";

    temp = -90;

    myfile << "X rotated -90 deg = " << (X %= temp) << "\n";
    myfile << "Y rotated -90 deg = " << (Y %= temp) << "\n";
    myfile << "Z rotated -90 deg = " << (Z %= temp) << "\n";
  }
}









void TestPoints(void)
{
  myfile << "\n********** TestPoint2Ds **********\n";
  //Point2D(const float X = 0, const float Y = 0, const float Z = 0, const float W = 1);
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Point2D Test #1 (Default Constructor) PART1" << "\n";

    Point2D X;
    myfile << "X = " << X << "\n\n";

    
    myfile << "PART2 Point2D Test #1 (Non-Default Constructor) PART2" << "\n";

    X = Point2D(1,1);
    myfile << "X = " << X << "\n";

  }

  //Point2D(const Hcoords& Point2D);
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Point2D Test #1 (Point(Hcoords) Constructor) PART1" << "\n";

    Point2D X(Hcoords(3,2));
    myfile << "X = " << X << "\n";
  }

  //Point2D(const Point2D&);
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Point2D Test #1 (Point(Point) constructor) PART1" << "\n";

    Point2D X(Point2D(3,2));
    myfile << "X = " << X << "\n";
  }


  //float X(void) const;
	//float Y(void) const;
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Point2D Test #1 (Gettors) PART1" << "\n";
    Point2D X(3,2);
    float temp;

    myfile << "X = " << X << "\n";
    temp = X.X();
    myfile << "X Gettor Got = " << temp << "\n";
    temp = X.Y();
    myfile << "Y Gettor Got = " << temp << "\n\n";
  }

  //float operator[](int i) const;
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Point2D Test #1 (Gettors) PART1" << "\n";
    Point2D X(3,2);
    float temp = 0;

    myfile << "X = " << X << "\n";
    temp = X[0];
    myfile << "X[0] Got = " << temp << "\n";
    temp = X[1];
    myfile << "X[1] Got = " << temp << "\n\n";
  }


  //Point2D& operator=(const Point2D&);
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Point2D Test #1 (Point = Point) PART1" << "\n";
    Point2D X(3,2), Y(1,1);

    myfile << "X = " << X << "\n" << "Y = " << Y << "\n";
    Y = X;
    myfile << "Y = X" << "\n" << "Y now = " << Y << "\n\n";
  }



  //Point2D& operator+=(const Point2D&);
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Point2D Test #1 (Point += Point) PART1" << "\n";
    Point2D X(3,2), Y(1,1);

    myfile << "X = " << X << "\n" << "Y = " << Y << "\n";
    Y += X;
    myfile << "Y += X" << "\n" << "Y now = " << Y << "\n\n";

  }


  //Point2D& operator-=(const Point2D&);
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Point2D Test #1 (Point -= Point) PART1" << "\n";
    Point2D X(3,2), Y(1,1);

    myfile << "X = " << X << "\n" << "Y = " << Y << "\n";
    Y -= X;
    myfile << "Y -= X" << "\n" << "Y now = " << Y << "\n\n";   
  }


  //Point2D& operator*=(const float);
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Point2D Test #1 (Point *= float) PART1" << "\n";
    Point2D X(3,2);
    float temp = 2.0f;

    myfile << "X = " << X << "\n" << "temp = " << temp << "\n";
    X *= temp;
    myfile << "X *= temp" << "\n" << "X now = " << X << "\n\n"; 

  }


  //float length(void);
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Point2D Test #1 (Length of Point) PART1" << "\n";
    Vector2D X(3,2);
    float temp = 2.0f;

    myfile << "X = " << X << "\n";
    temp = X.Length();
    myfile << "length of X = " << temp << "\n\n"; 
  }



  //float length2(void);
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Point2D Test #1 (SquareLength) PART1" << "\n";
    Vector2D X(3, 2);
    float temp = 2.0f;

    myfile << "X = " << X << "\n";
    temp = X.SquareLength();
    myfile << "length2 of X = " << temp << "\n\n"; 
  }


  //Point2D BarScale(float r);
  {
    myfile << "\n|###########################################|\n";
    myfile << "Point2D Test #1 (BarScale)" << "\n\n";

    Point2D X(1,1);
    Point2D Y(2,2);

    myfile << "X = " << X << "\n";
    X.BarScale(2.0);
    myfile << "X.BarScale = " << X << "\n\n"; 

    myfile << "Y = " << Y << "\n";
    Y.BarScale(2.0);
    myfile << "Y.BarScale = " << Y << "\n\n"; 
  }



  //Point2D operator+(const Point2D& v);
  {
    myfile << "\n|###########################################|\n";
    myfile << "Point2D Test #1 (Point + Point)" << "\n\n";
    Point2D X(1,1), Y(2,2), Z(3,3);

    myfile << "X = " << X << "\n";
    Z = X + Y;
    myfile << "Z = X + Y" << "\n" << "Z = " << Z << "\n\n"; 

    myfile << "X = " << X << "\n";
    X = Z + Y;
    myfile << "X = Z + Y" << "\n" << "X = " << X << "\n\n";  
  }
	 
	 
	//Point2D operator-(void);
  {
    myfile << "\n|###########################################|\n";
    myfile << "Point2D Test #1 (-Point)" << "\n\n";
    Point2D X(1,1), Y(2,2), Z(3,3);

    myfile << "X = " << X << "\n";
    -X;
    myfile << "-X = " << X << "\n\n"; 

    myfile << "Y = " << Y << "\n";
    -Y;
    myfile << "-Y = " << Y << "\n\n"; 

    myfile << "Z = " << Z << "\n";
    -Z;
    myfile << "-Z = " << Z << "\n\n"; 
  }
	  
  

  //Point2D operator*(const float r);
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Point2D Test #1 (Point * r) PART1" << "\n";
    Point2D X(3,2), Y;
    float r = 2.0f;

    myfile << "X = " << X << "\n" << "r = " << r <<  "\n";
    Y = X * r;
    myfile << "Y = X * r = " << "\n" << "Y = " << Y << "\n\n"; 

  }


  //Vector2D operator-(const Point2D& v);
  {
    myfile << "\n|###########################################|\n";
    myfile << "Point2D Test #1 (Point - Point = Vector)" << "\n\n";
    Point2D X(1,1), Y(2,2), Z(3,3);
    Vector2D W;


    myfile << "X = " << X << "\n" << "Y = " << Y << "\n";
    W = X - Y;
    myfile << "W = X - Y" << "\n" << W << "\n\n"; 

    myfile << "Y = " << Y << "\n" << "Z = " << Z << "\n";
    W = Y - Z;
    myfile << "W = Y - Z" << "\n" << W << "\n\n"; 

    myfile << "Z = " << Z << "\n" << "X = " << X << "\n";
    W = Z - X;
    myfile << "W = Z - X" << "\n" << W << "\n\n";  
  }


  //float operator*(const Point2D& v);
  {
    myfile << "\n|###########################################|\n";
    myfile << "Point2D Test #1 (Point * Point = float)" << "\n\n";
    Point2D X(1,1), Y(2,2), Z(3,3);
    float temp = 0;

    myfile << "X = " << X << "\n";
    temp = X * X;
    myfile << "temp = X * X = " << temp << "\n\n"; 


    myfile << "Y = " << Y << "\n";
    temp = Y * Y;
    myfile << "temp = Y * Y = " << temp << "\n\n"; 

    myfile << "Z = " << Z << "\n";
    temp = Z * Z;
    myfile << "temp = Z * Z = " << temp << "\n\n"; 
  }


  //bool operator==(const Point2D& v);
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Point2D Test #1 (Point == Point?) PART1" << "\n\n";

    Point2D X(1,1), Y(1,1), Z(2,2);

    myfile << "X = " << X << "\n" << "Y = " << Y << "\n" <<"Z = " << Z << "\n";
    myfile << "Does X == Y? ";

    if(X == Y)
      myfile << "YES\n";
    else
      myfile << "NO\n";

    myfile << "Does X == Z? ";

    if(X == Z)
      myfile << "YES\n";
    else
      myfile << "NO\n";
  }
}











void TestAffines(void)
{
  myfile << "\n********** TestAffines **********\n";


  //Affine(void); // set everything to zero expect for the bottom left
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Affine Test #1 (Default Constructor) PART1" << "\n\n";

    Affine X;

    myfile << "X = " << "\n" << X << "\n\n";
  }

  //Affine(const Vector2D& LeftCol, const Vector2D& MiddleCol, const Point2D& RightCol);
  //Affine(const Vector2D& LeftCol, const Vector2D& MiddleCol, const Hcoords& RightCol);
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Affine Test #1 (Non-Default Constructor) PART1" << "\n\n";

    Affine X(Vector2D(1,1), Vector2D(2,2), Point2D(3,3));
    Affine Y(Vector2D(1,1), Vector2D(2,2), Hcoords(3,3));
    myfile << "X = " << "\n" << X << "\n\n";
    myfile << "Y = " << "\n" << Y << "\n\n";
  }


  //Affine Inverse(const Affine& A);
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Affine Test #1 (Affine Inverse) PART1" << "\n\n";

    Affine X(Vector2D(1,1), Vector2D(2,2), Point2D(3,3));
    Affine Y(Vector2D(2,1), Vector2D(3,4), Hcoords(6,3));
    Affine iX;
    Affine iY;
    myfile << "X = " << "\n" << X << "\n\n";
    iX = Inverse(X);
    myfile << "X Inverse " << "\n" << iX << "\n\n";

    myfile << "Y = " << "\n" << Y << "\n\n";
    iY = Inverse(Y);
    myfile << "Y Inverse " << "\n" << iY << "\n\n";
  }


  //Affine& operator+(const Affine& B);
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Affine Test #1 (Affine + Affine) PART1" << "\n\n";

    Affine X(Vector2D(1,1), Vector2D(2,2), Point2D(3,3));
    Affine Y(Vector2D(2,1), Vector2D(3,4), Hcoords(6,3));
    myfile << "X = " << "\n" << X << "\n" << "Y = " << "\n" << Y << "\n\n";
    myfile << "X + Y " << "\n" << X + Y << "\n\n";
  }

  //Affine& operator*(const Affine& B);
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Affine Test #1 (Affine * Affine) PART1" << "\n\n";

    Affine X(Vector2D(1,1), Vector2D(2,2), Point2D(3,3));
    Affine Y(Vector2D(2,1), Vector2D(3,4), Hcoords(6,3));
    myfile << "X = " << "\n" << X << "\n" << "Y = " << "\n" << Y << "\n\n";
    myfile << "X * Y " << "\n" << X * Y << "\n\n";
  }



    //  // Affine * Hcoords
    //Hcoords operator*(const Hcoords& B);
  {

    myfile << "\n|###########################################|\n";
    myfile << "PART1 Affine Test #1 (Hcoords = Affine * Hcoords) PART1" << "\n\n";

    Affine X(Vector2D(1,1), Vector2D(2,2), Point2D(3,3));
    Hcoords Y(3,3);
    myfile << "X = " << "\n" << X << "\n" << "Y = " << Y << "\n\n";
    myfile << "X * Y " << "\n" << X * Y << "\n\n";
  }
}






void TestMatrix4s(void)
{
  myfile << "\n********** TestMatrix4s **********\n";

  //Matrix4(void);
  {

    myfile << "\n|###########################################|\n";
    myfile << "PART1 Matrix4 Test #1 (Default Constructor) PART1" << "\n\n";

    Matrix4 X;

    myfile << "X = " << "\n" << X << "\n\n";
  }


   //Matrix4(const Vector2D& LeftCol, const Vector2D& MiddleCol, const Vector2D& RightCol, const Point2D& P);
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Matrix4 Test #1 (Default Constructor) PART1" << "\n\n";

    Matrix4 X(Vector2D(1,1), Vector2D(2,2), Vector2D(3,3), Point2D(3,3));

    myfile << "X = " << "\n" << X << "\n\n";
  }

  /*
  //Matrix4& operator+(const Matrix4& m4);
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 Matrix4 Test #1 (Matrix4 + Matrix4) PART1" << "\n\n";

    Matrix4 X(Vector2D(1,1), Vector2D(2,2), Vector2D(3,3), Point2D(4,4));
    Matrix4 Y(Vector2D(2,2), Vector2D(3,3), Vector2D(4,4), Point2D(5,5));

    myfile << "X = " << "\n" << X << "\n";
    myfile << "Y = " << "\n" << Y << "\n\n";
    myfile << "X + Y = " << "\n" << X + Y << "\n\n";
    myfile << "X = " << "\n" << X << "\n";
    myfile << "Y = " << "\n" << Y << "\n\n";
  }*/
}








void TestMath(void)
{
  myfile << "\n************** TestMath **************\n";

  //float dot(const Vector2D& u, const Vector2D& v);
  {

    myfile << "\n|###########################################|\n";
    myfile << "PART1 TestMath Test #1 (Vector dot Vector) PART1" << "\n\n";

    Vector2D X(1,1), Y(2,2);
    float temp = 0;

    myfile << "X = " << X << "\n";
    myfile << "Y = " << Y << "\n\n";
    temp = dot(X, Y);
    myfile << "dot product of X and Y = " << temp << "\n\n";
  }


  //float abs(const Vector2D& v);
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 TestMath Test #1 () PART1" << "\n\n";

    Vector2D X(1,1), Y(2,2);
    float temp = 0;

    myfile << "X = " << X << "\n";
    myfile << "Y = " << Y << "\n\n";
    temp = abs(X);
    myfile << "length of X = " << temp << "\n\n";
  }



  //Affine Rot(float t);
  // Rots in Radians
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 TestMath Test #1 (Rototation by Radians) PART1" << "\n\n";

    float temp = 3.14f;

    myfile << "Rotation Affine = " << "\n" << Rot(temp) << "\n";
  }

  //Affine Trans(const Vector2D& v);
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 TestMath Test #1 (Affine Translation) PART1" << "\n\n";
    Vector2D Y(3,3);

    myfile << "Y = " << Y << "\n\n" ;
    myfile << "Translation Affine of Y = " << "\n" << Trans(Y);
  }


  //Affine Scale(const float r);
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 TestMath Test #1 (Affine Scale) PART1" << "\n\n";
    float temp = 2.0f;

    myfile << "Scale Affine of temp = " << "\n" << Scale(temp);
  }

  //Affine Scale(const float rx, const float ry);
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 TestMath Test #1 (Affine Scale(x,y)) PART1" << "\n\n";
    float temp = 2.0f;
    float temp1 = 5.0f;

    myfile << "Scale Affine of temp = " << "\n" << Scale(temp, temp1);
  }


  //Point2D operator*(float r, const Point2D& u);
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 TestMath Test #1 (Point = r * Point) PART1" << "\n\n";
    Point2D X(1,1);
    float temp1 = 5.0f;

    myfile << "X = " << X << "\n" << "temp1 * X = " << (temp1 * X) << "\n\n";
  }



  //Point2D Normalize(Point2D V);
  {
    myfile << "\n|###########################################|\n";
    myfile << "PART1 TestMath Test #1 (Point Normal) PART1" << "\n\n";
    Vector2D X(3,1);

    myfile << "X = " << X << "\n" << "Normal of X = " << X.Normalize() << "\n\n";
    myfile << "\n\n\n\n";
  }
}


void Math2DUnitTest(void)
{
  const char* filename = "TestMyMath.txt";
  const char* comparefile;
  myfile.open(filename);

  if(myfile.is_open())
  {
    int numTests = 0;


    switch(numTests)
    {
      case 0:
        comparefile = "TestMasterMath.txt";
        break;

      case 1:
        comparefile = "TestHcoords.txt";
        break;

      case 2:
        comparefile = "TestVector2Ds.txt";
        break;

      case 3:
        comparefile = "TestPoints.txt";
        break;

      case 4:
        comparefile = "TestAffines.txt";
        break;

      case 5:
        comparefile = "TestMatrix4s.txt";
        break;

      case 6:
        comparefile = "TestMath.txt";
        break;
    }

    typedef void (*Test)(void);

    Test MathTests[] = 
    {
      TestHcoords,      // 1
      TestVector2Ds,    // 2
      TestPoints,       // 3
      TestAffines,      // 4
      TestMatrix4s,     // 5
      TestMath,         // 6
    };

    int tests = sizeof(MathTests) / sizeof(*MathTests);
    if(numTests == 0)
    {
      for(int i = 0; i < tests; ++i)
        MathTests[i]();
    }
    else if(numTests > 0 && numTests <= tests)
      MathTests[numTests - 1]();


    myfile.close();

    //std::stringstream stream;
    //stream << "WinMergeU" << " " << filename << " " << comparefile;
    //system(stream.str().c_str());
  }
  else
    assert("FILE NOT FOUND!");
  
}