#include "stdafx.h"
#include "Page_Allocator.hpp"




Page_Allocator::Page_Allocator(size_t low_obj_size, size_t medium_obj_size, size_t high_obj_size, const MMConfig& config) : config(config)
{
  allocate_page(low_obj_size);    // can be hyper optimized once performance tracking is made
  allocate_page(medium_obj_size); // can be hyper optimized once performance tracking is made
  allocate_page(high_obj_size);   // can be hyper optimized once performance tracking is made
}

Page_Allocator& Page_Allocator::operator=(Page_Allocator* rhs)
{
  Page_Allocator* temp_PA = static_cast<Page_Allocator*>(malloc(sizeof(Page_Allocator)));

  temp_PA->free_list = rhs->free_list;

  return *this;
}


void Page_Allocator::allocate_page(size_t obj_size)
{
  unsigned total_size = obj_size * config.ObjectsPerPage_ + sizeof(void*)* config.ObjectsPerPage_ +
    config.ObjectsPerPage_ * config.PaddingSize * 2;

  bytes page = (bytes)malloc(total_size);

  if (page == NULL)
    assert(false && "NO MEMORY LEFT SOMETHING BAD HAPPENED!");

  memset(page, UNALLOCATED_PATTERN, total_size); // setting all the memory to AA's
  ++stats.PagesInUse_;

#if _DEBUG
  config.DebugOn_ = false;
  if (config.PaddingSize != 0)
    Add_Padding(page, obj_size);
#else
  config.DebugOn_ = false;

#endif

  Add_Headers(page, obj_size);

  // adjust pointers to point at the correct blocks on free_list
  KPVec<bytes> obj_ptrs;


  size_t pos = config.PaddingSize + sizeof(void *);
  for (unsigned i = 0; i < config.ObjectsPerPage_; ++i)
  {
    ++stats.FreeObjects_;
    bytes temp = reinterpret_cast<char* >(page + pos);
    obj_ptrs.push_back(temp);
    pos += obj_size + config.PaddingSize * 2 + sizeof(void*);
  }

  List_Info data(obj_size, config.ObjectsPerPage_, page);

  free_list.push_back(make_pair(data, obj_ptrs));
}



void* Page_Allocator::Allocate(size_t size)
{
  if (free_list.size() == 0)
    allocate_page(size);

  unsigned i = 0;
  bool flag = false;
  unsigned similar_size = 0;
  for (; i < free_list.size(); ++i)
  {
    if (free_list[i].first.max_obj_size >= size && similar_size == 0)
      similar_size = free_list[i].first.max_obj_size;

    if (!free_list[i].second.size())
      continue;

    if (free_list[i].first.max_obj_size >= size && free_list[i].second.size() != 0)
    {
      flag = true;
      break;
    }
  }

  if (flag == false)
  {
    if (similar_size != 0)
      allocate_page(similar_size); // can be hyper optimized with data collection
    else
      allocate_page(size);
  }


  bytes temp = free_list[i].second.back();
  free_list[i].second.pop_back();

  if (size != free_list[i].first.max_obj_size)
    Update_Header_Info(temp, true, free_list[i].first.max_obj_size - size, size, free_list[i].first.max_obj_size);
  else
    Update_Header_Info(temp, true, 0, size, free_list[i].first.max_obj_size);
  --stats.FreeObjects_;
  ++stats.total_alloc_;
  return temp;
}

void Page_Allocator::Free(void* obj)
{
  if (!obj)
    return;

  if (config.DebugOn_)
  {
    // checks double delete / wrong ptr
    Double_Del_BadPtr(obj);

    // checks for memory overwrites (checks the padding)
    Check_Buffer_Flows(obj);
  }

  Header_Info** header_block = reinterpret_cast<Header_Info**>((bytes)obj - config.PaddingSize - sizeof(void*));

  // put on free_list
  free_list[(*header_block)->Page_Num - 1].second.push_back((bytes)obj);
  ++stats.total_frees_;
  ++stats.FreeObjects_;
  // gets rid of any extra pad bytes / changes header information to inactive
  Update_Header_Info((bytes)obj, false);  // testing
}

void Page_Allocator::Update_Header_Info(bytes block, bool allocate, size_t extra_padding, size_t obj_size, size_t list_size)
{
  Header_Info** header_block = reinterpret_cast<Header_Info**>(block - config.PaddingSize - sizeof(void*));
  if (allocate)
  {
    (*header_block)->Is_Allocated = allocate;
    (*header_block)->Obj_Size = obj_size;
    (*header_block)->Extra_Padding = extra_padding;
    (*header_block)->List_Size = list_size;


    if (extra_padding && config.DebugOn_)
      memset(block + obj_size, PAD_PATTERN, extra_padding);

    return;
  }

  // clear extra padding
  if (config.DebugOn_)
    memset(block + (*header_block)->Obj_Size, UNALLOCATED_PATTERN, (*header_block)->Extra_Padding);

  (*header_block)->Is_Allocated = false;
  (*header_block)->Obj_Size = 0;
  (*header_block)->Extra_Padding = 0;
  (*header_block)->List_Size = 0;
}

void Page_Allocator::Add_Headers(bytes& page, size_t obj_size)
{
  unsigned pos = 0;
  for (unsigned i = 0; i < config.ObjectsPerPage_; ++i)
  {
    Header_Info* header = static_cast<Header_Info*>(malloc(sizeof(Header_Info)));

    header->Is_Allocated = false;
    header->Obj_Size = obj_size;
    header->Pad_Size = config.PaddingSize;
    header->Page_Num = stats.PagesInUse_;
    header->Alloc_Num = stats.total_alloc_;
    Header_Info** mem_block_info = reinterpret_cast<Header_Info**>(page + pos);
    *mem_block_info = header;
    pos += obj_size + config.PaddingSize * 2 + sizeof(void *);
  }
}

void Page_Allocator::Add_Padding(bytes& page, size_t obj_size)
{
  unsigned pos = sizeof(void*);
  for (unsigned i = 0; i < config.ObjectsPerPage_; ++i) // padding
  {
    memset(page + pos, PAD_PATTERN, config.PaddingSize);
    pos += obj_size + config.PaddingSize;
    memset(page + pos, PAD_PATTERN, config.PaddingSize);
    pos += sizeof(void*)+config.PaddingSize;
  }
}



void Page_Allocator::Convert_Extra_Bytes(char* start, unsigned size)
{
  memset(start, PAD_PATTERN, size);
}




// Debugging Tool
void Page_Allocator::Double_Del_BadPtr(void *obj)
{
  for (unsigned i = 0; i < free_list.size(); ++i)
  {
    bytes start = free_list[i].first.page + config.PaddingSize + sizeof(void*);
    for (unsigned j = 0; j < free_list[i].first.max_objs; ++j)
    {
      if (start == obj)
      {
        if ((*reinterpret_cast<Header_Info**>(start - config.PaddingSize - sizeof(void*)))->Is_Allocated == true)
          return;

        assert(false && "DOUBLE DELETE FIX YOUR MEMORY!"); // change this to line number and file when overloading new / delete
      }
      start += config.PaddingSize * 2 + sizeof(void*)+free_list[i].first.max_obj_size;
    }
  }
  assert(false && "WRONG DELETE ADDRESS, FIX YOUR MEMORY!"); // change this once I get the systems
}


void Page_Allocator::Check_Buffer_Flows(void *obj)
{
  Header_Info** h_block = reinterpret_cast<Header_Info**>((bytes)obj - config.PaddingSize - sizeof(void*));
  bytes left_pad_start = (bytes)obj - config.PaddingSize;
  bytes right_pad_start = (bytes)obj + (*h_block)->Obj_Size;

  for (unsigned i = 0; i < config.PaddingSize; ++i)
  {
    if ((unsigned char)left_pad_start[i] != PAD_PATTERN)
      assert(false && "UNDERFLOW HAPPENED, SOMEONE OVERWROTE MEMORY!"); // CHANGE THIS TO THE SYSTEM THAT WAS RESPONSIBLE
  }

  for (unsigned i = 0; i < (*h_block)->Extra_Padding + config.PaddingSize; ++i)
  {
    if ((unsigned char)right_pad_start[i] != PAD_PATTERN)
      assert(false && "OVERFLOW HAPPENED, SOMEONE OVERWROTE MEMORY!"); // CHANGE THIS TO THE SYSTEM THAT WAS RESPONSIBLE
  }
}


#if USE_PAGE_ALLOCATOR
void* operator new(size_t size)
{
  if (!P_A)
  {
    MMConfig settings;
    settings.ObjectsPerPage_ = 1000;
    P_A = static_cast<Page_Allocator*>(malloc(sizeof(Page_Allocator)));
    P_A->Set_Config_Settings(settings);
    P_A->Set_Stats(MMStats());
    P_A->Get_Free_List() = KPVec<pair<List_Info, KPVec<bytes>>>();
  }

  return P_A->Allocate(size);
}

void* operator new[](size_t size)
{
  if (!P_A)
  {
    MMConfig settings;
    settings.ObjectsPerPage_ = 1000;
    P_A = static_cast<Page_Allocator*>(malloc(sizeof(Page_Allocator)));
    P_A->Set_Config_Settings(settings);
    P_A->Set_Stats(MMStats());



    P_A->Get_Free_List() = KPVec<pair<List_Info, KPVec<bytes>>>();
  }

  return P_A->Allocate(size);
}

void operator delete(void* del_ptr)
{
  P_A->Free(del_ptr);
}

void operator delete[](void* del_ptr) throw()
{
  P_A->Free(del_ptr);
}
#endif