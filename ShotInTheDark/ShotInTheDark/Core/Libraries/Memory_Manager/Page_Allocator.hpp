#pragma once
#pragma warning( disable : 4290 )

#include <vector>
#include "Libraries\Containers\KPVec.hpp"

#define USE_PAGE_ALLOCATOR 1

typedef char* bytes;

using namespace std;

static const int DEFAULT_OBJECTS_PER_PAGE = 10;
static const int DEFAULT_MAX_PAGES = 1;

struct MMStats
{
  MMStats(void) : FreeObjects_(0), ObjectsInUse_(0), PagesInUse_(0), most_objs_(0), total_alloc_(0), total_frees_(0) {};

  unsigned FreeObjects_;   // number of objects on the free list
  unsigned ObjectsInUse_;  // number of objects in use by client
  unsigned PagesInUse_;    // number of pages allocated
  unsigned most_objs_;     // most objects in use by client at one time
  unsigned total_alloc_;   // total requests to allocate memory
  unsigned total_frees_;   // total requests to free memory
};

struct Header_Info
{
  bool Is_Allocated;      // is this object allocated currently
  int Pad_Size;           // ??? not sure if this is needed
  unsigned Obj_Size;      // size of the object that was requested to be allocated
  unsigned Page_Num;      // figure out what page this is on
  unsigned Extra_Padding; // if any extra padding was added
  unsigned List_Size;     // the size of each obj that this obj was assigned to (for placing it back onto the freelist more easily)
  unsigned Alloc_Num;
  // Allocation # for debugging broken memory
  // The System that allocated this memory, for debugging
};


struct MMConfig
{
  static const size_t EXTERNAL_HEADER_SIZE = sizeof(void*);     // just a pointer


  MMConfig(
    unsigned ObjectsPerPage = DEFAULT_OBJECTS_PER_PAGE,
    unsigned MaxPages = DEFAULT_MAX_PAGES,
    bool DebugOn = false,
    unsigned Padding = 6,
    bool extra_pad = false,
    const Header_Info &HBInfo = Header_Info())
    : ObjectsPerPage_(ObjectsPerPage),
    DebugOn_(DebugOn),
    PaddingSize(Padding),
    HBlockInfo_(HBInfo)
  {
    HBlockInfo_ = HBInfo;
  }
  unsigned ObjectsPerPage_; // number of objects on each page
  bool DebugOn_;            // enable/disable debugging code (signatures, checks, etc.)
  unsigned PaddingSize;         // size of the left/right padding for each block
  Header_Info HBlockInfo_;  // size of the header for each block (0=no headers)
};


struct List_Info
{
  unsigned max_obj_size;
  unsigned max_objs;
  bytes page;
  List_Info() : max_obj_size(0), max_objs(0), page(nullptr) {}
  List_Info(unsigned obj_size, unsigned max_objs, bytes full_page) : max_obj_size(obj_size), max_objs(max_objs), page(full_page) {}
};

class Page_Allocator
{
public:
  static const unsigned char UNALLOCATED_PATTERN = 0xAA;
  static const unsigned char ALLOCATED_PATTERN = 0xBB;
  static const unsigned char FREED_PATTERN = 0xCC;
  static const unsigned char PAD_PATTERN = 0xDD;

  Page_Allocator& operator=(Page_Allocator* rhs);
  Page_Allocator(size_t low_obj_size, size_t medium_obj_size, size_t high_obj_size, const MMConfig& config);
  void *Allocate(size_t size);
  void Free(void* obj);

  void Set_Config_Settings(MMConfig configs) { config = configs; }
  void Set_Stats(MMStats stats_) { stats = stats_; }
  void Reset_Page_Allocator(void) { free_list.clear(); }
  KPVec<pair<List_Info, KPVec<bytes>>>& Get_Free_List(void) { return free_list; }

private:

  KPVec<pair<List_Info, KPVec<bytes>>> free_list; // small, medium, large ... etc. also the page_list
  MMConfig config;
  MMStats stats;

  // HELPER FUNCTIONS:

  // Convert Extra Bytes into Padding
  void Convert_Extra_Bytes(char* start, unsigned size);

  // Adds the pointers for the all the headers
  void Add_Headers(bytes& page, size_t obj_size);

  // Turns header information on / updates header information
  void Update_Header_Info(bytes block, bool allocate, size_t extra_padding = 0, size_t obj_size = 0, size_t list_size = 0);

  // add Padding to the page, only if debug is on
  void Add_Padding(bytes& page, size_t obj_size);

  // Allocate Page
  void allocate_page(size_t obj_size);

  // Debugging Tools:
  //{
  // Memory Leaks function

  // Buffer Overflows function
  // detect when it happens
  // detect read overflows

  // Buffer access after deletion

  // Double deletes
  void Double_Del_BadPtr(void *obj);
  void Check_Buffer_Flows(void *obj);
  // mismatched new / delete[], new[]/delete 

  // Bad pointer deletes / wrong pointer O(N) algorithm
  //}
};

#if USE_PAGE_ALLOCATOR
  void* operator new(size_t size) throw(std::bad_alloc);
  void* operator new[](size_t size) throw(std::bad_alloc);
  void operator delete(void* del_ptr) throw();
  void operator delete[](void* del_ptr) throw();
#endif
