#include "stdafx.h"
#include "RNG.hpp"
#include <stdio.h>

string RNG::seed_to_string(int value)
{
  const int seedBufferSize = 30;
  char seed[seedBufferSize] = { 0 };

  sprintf_s(seed, seedBufferSize, "%i", value);
  char *temp = seed;
  while (*temp != '\0')
  {
    *temp += 'A' - '0';
    ++temp;
  }

  return std::string(seed);


  /*
  string string_of_numbers = to_string(value);
  string char_seed;

  for (unsigned i = 0; i < string_of_numbers.size(); ++i)
  {
    char letter = string_of_numbers[i];
    int letter_value = atoi(&letter);

    char_seed.push_back(seed_letters[letter_value].first);
  }
  return char_seed;
  */
}

int RNG::seed_to_int(string string_seed)
{
  int int_seed = 0;
  string all_numbers;

  for (unsigned i = 0; i < string_seed.size(); ++i)
  {
    int letter_number = string_seed[i] - 'A';
    all_numbers.push_back(seed_letters[letter_number].second + '0');
  }

  int_seed = stoi(all_numbers);
  return int_seed;
}


RNG::RNG(void)
{
  number_seed = 1;
  string_seed.push_back('A');
  eng1.seed(1);
  rand_calls = 0;
}

RNG::RNG(int num_seed)
{
  number_seed = num_seed;

  for (int i = 0; i < 10; ++i)
    seed_letters.push_back(make_pair('A' + i, i));

  eng1.seed(num_seed);

  string_seed = seed_to_string(num_seed);
}

RNG::RNG(string char_seed)
{
  string_seed = char_seed;

  for (int i = 0; i < 9; ++i)
    seed_letters.push_back(make_pair('A' + i, i));

  number_seed = seed_to_int(string_seed);
  eng1.seed(number_seed);
}


void RNG::Seed(string seed)
{
  number_seed = seed_to_int(seed);
  string_seed = seed;
  eng1.seed(seed_to_int(seed));
}



bool RNG::OneIn(int x_chance)
{
  ++rand_calls;
  if (eng1() % x_chance == 0)
    return true;
  
  return false;
}

bool RNG::OneIn(float percent_chance)
{
  ++rand_calls;

  if (percent_chance > 1.0f)
    percent_chance = 1.0f;

  unsigned percent = (unsigned)(percent_chance * 100);

  if (percent == 0)
	  return false;

  unsigned value = eng1() % 100;
  if (value <= percent)
    return true;

  return false;
}


int RNG::range(int low, int high)
{
  ++rand_calls;
  int value = (eng1() % ((high + 1) - low)) + low; // for debugging
  return value;
}


float RNG::range(float low, float high)
{
  ++rand_calls;
  return ((float)((eng1() % (int)((high - low) * 1000)) / 1000.f)) + low;
}


/*same as Seed(GetSeed())*/
void RNG::reset()
{
  eng1.seed(number_seed);
}


/*Same as range(1, x+1)*/
int RNG::Roll(int x)
{
	if (x <= 0)
		return 0;
  return range(1, x);
}
