#include "prng_engine.hpp"
#pragma once

using namespace std;




class RNG
{
  public:
    RNG(void); // default seed
    RNG(int given_seed);
    RNG(string char_seed);

    void Seed(string);
    string GetStringSeed(void) { return string_seed; }
    int GetNumberSeed(void) { return number_seed; }
    bool OneIn(int);
    bool OneIn(float percent_chance);
    int range(int low, int high);
    float range(float low, float high);
    void reset(); /*same as Seed(GetSeed())*/
    int Roll(int x); /*Same as range(1, x)*/
    int TimesUsed() const { return rand_calls; }
    string seed_to_string(int value);
    int seed_to_int(string string_seed);
    


  private:
    sitmo::prng_engine eng1;
    string string_seed;
    int number_seed;
    vector<pair<char, int>> seed_letters;
    int rand_calls;
};