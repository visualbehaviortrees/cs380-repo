//	File name:		ObjectFactory.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include <cstdarg>
#include "Engine.hpp"
#include "ObjectFactory.h"
#include "ObjectManager.hpp"
#include "Components/Components.h"
#include <cassert>
#include "Systems/Physics/PhysicsSystem.hpp"
#include "Systems/Graphics/ResourceManager.h"

Entity* ObjectFactory::Compose(Component **components)
{
  Entity *composition = ENGINE->OM->AddEntity();
  while(*components != NULL)
  {
    composition->AttachComponent(*components);
    ++components;
  }
  return composition;
}

Entity* ObjectFactory::Compose(std::vector<Component *> components)
{
  Entity *composition = ENGINE->OM->AddEntity();
  for(auto it: components)
  {
    composition->AttachComponent(it);
  }
  return composition;
}

Entity* ObjectFactory::Compose(unsigned count...)
{
  Entity *composition = ENGINE->OM->AddEntity();
  va_list args;
  va_start(args,count);
  Component *component;
  for(unsigned i = 0; i < count; ++i)
  {
		component = va_arg(args, Component *);
    assert(component != NULL);
    composition->AttachComponent(component);
  }
  va_end(args);
  return composition;
}
Component* ObjectFactory::CreateComponent(const std::string& ComponentName)
{
		return Components[ComponentName]();
}

/*
Entity* ObjectFactory::Compose(const char **components)
{

}
*/

void ObjectFactory::AddComponent(const std::string &ComponentName, ComponentCreateFunction CCF)
{
		Components[ComponentName] = CCF;
}

EComponent ObjectFactory::ComponentStrToEnum(std::string& name)
{
  Component *temp = CreateComponent(name);
  EComponent type = temp->Type();
  delete temp;
  return type;
}

Entity* ObjectFactory::Compose(std::vector<std::string>& components)
{
  std::vector<Component *> createdComponents;
  for(auto it : components)
  {
    createdComponents.push_back(CreateComponent(it));
  }

  return Compose(createdComponents);
  
}

Entity *ObjectFactory::CreatePauseMenuBox(Math::Vector2D pos, Math::Vector2D scale, std::string texture)
{
  Drawable* draw        = new Drawable();
          * draw        = Drawable::GrayScheme;
            draw->layer = 20;
  Transform* trans      = new Transform();
  trans->Position       = pos;
  trans->scale          = scale;
  GUIItem *pause      = new GUIItem();
  pause->screenPos = Point2D(0.0f, 0.0f);
  ObjectToWorld(trans);

  Sprite *sprite = new Sprite(texture, true);

  return ENGINE->Factory->Compose(4,draw,trans,pause,sprite);
}

void ObjectFactory::Init()
{
 // AddComponent("Mesh",            &Mesh::CreateComponent            ); Meshes are no longer components
  AddComponent("Transform",       &Transform::CreateComponent           );
  AddComponent("Player",          &Player::CreateComponent              );
  AddComponent("RigidBody",       &RigidBody::CreateComponent           );
  AddComponent("Team",            &Team::CreateComponent                );
  AddComponent("Audio",           &Audio::CreateComponent               );
  AddComponent("Bullet",          &Bullet::CreateComponent              );
  AddComponent("Base",            &Base::CreateComponent                );
  AddComponent("Drawable",        &Drawable::CreateComponent            );
  AddComponent("Sprite",          &Sprite::CreateComponent              );
  AddComponent("SpriteText",      &Sprite::CreateComponent              );
  //AddComponent("Flag",            &Flag::CreateComponent                );
  AddComponent("Health",          &Health::CreateComponent              );
  AddComponent("Score",           &Score::CreateComponent               );
  AddComponent("Parent",          &Parent::CreateComponent              );
  //AddComponent("Turret",          &Turret::CreateComponent              );
  AddComponent("PowerupSpawner",  &PowerupSpawner::CreateComponent      );
  AddComponent("ParticleEffect",  &ParticleEffect::CreateComponent      );
  AddComponent("GUIItem",         &GUIItem::CreateComponent             );
  AddComponent("Light",           &Light::CreateComponent               );
  AddComponent("Behavior",        &Behavior::CreateComponent            );
  AddComponent("Door",            &Door::CreateComponent                );
  AddComponent("Death",           &Death::CreateComponent               );
  AddComponent("Minimap",         &Minimap::CreateComponent             );
  AddComponent("BulletEffect",    &BulletEffect::CreateComponent        );
  AddComponent("HUDElement",      &HUDElement::CreateComponent          );
  AddComponent("Key",             &Key::CreateComponent                 );
  AddComponent("MenuButton",      &MenuButtonComponent::CreateComponent );
  AddComponent("Script",          &Script::CreateComponent              );
}