//	File name:		ObjectFactory.h
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "Component.hpp"
#include "Entity.hpp"
#include <map>
#include <string>
#include "Libraries/Math/Math2D.hpp"
class ObjectFactory
{
public:
		ObjectFactory(){Init();}
		void Init();
  /**
   * @brief Builds an entity with components
   * @details Takes an array of pointers to components, and builds an entity with them.
   *
   * @param components An array of pointers to components to be added to the Entity.
   * @return Returns a pointer to the Entity that was composed.
   */
  Entity* Compose(Component **components);

  /**
   * @brief Builds an entity with components
   * @details Takes a count and that number of component pointers.
   *
   * @param count The number of components being added.
   * @param ... Component pointers. There should be exactly count components.
   * @return Returns a pointer to the Entity that was composed.
   */
  Entity* Compose(unsigned count...);
  /**
   * @brief Creates a component corresponding to the type named in the string.
   * @details Makes a component of the type named in the string. Useful for deserializing.
   *
   * @param[in] ComponenName The name of the type of component to create.
   * @return Returns a base component pointer to a component of the specified derived type.
   */
	Component* CreateComponent(const std::string& ComponentName);
  
  /**
   * @brief Creates an Entity from a list of derived Component typename strings.
   * @details Allows the user to create an Entity with components named by string.
   *
   * @param[in] components A NULL-terminated array of pointers to characters that contain
   *  strings naming types of components.
   * @return Returns a base component pointer to a component of the specified derived type.
   */
  //Entity* Compose(const char **components);

  Entity* Compose(std::vector<Component *> components);

  Entity* Compose(std::vector<std::string>& components);

  EComponent ComponentStrToEnum(std::string& name);
  
  Entity *CreatePauseMenuBox(Math::Vector2D pos, Math::Vector2D scale, std::string texture);

private:
  void AddComponent(const std::string &ComponentName, ComponentCreateFunction);
  typedef std::map<std::string, ComponentCreateFunction> ComponentMap;
  ComponentMap Components;
};