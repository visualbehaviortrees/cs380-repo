//	File name:		ObjectManager.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "ObjectManager.hpp"
#include <cassert>
#include "Components/Components.h"



ObjectManager::~ObjectManager()
{
  for (auto it : _registry)
  {
    delete it;
  }
  _registry.clear();
  DestroyAll();
  PurgeObjects();
}

Entity* ObjectManager::GetEntity(unsigned index)
{
	if (index <= _entities.size())
	{
		assert(index <= _entities.size());
		return _entities[index]; // this line should never be reached.
	}
	else
	{
		return _entities[index];
	}
	return _entities[index];
}

Entity* ObjectManager::AddEntity(void)
{
	_shouldUpdateRegistry = true;

  _entities.push_back(new Entity());
	return _entities.back();

}

std::vector<Entity *>& ObjectManager::FilterEntities(mask Mask)
{
  Mask |= MC_Alive;
	_filteredEntities.clear();
	for (std::vector<Entity *>::iterator it = _entities.begin(); it != _entities.end(); ++it)
	{
		if ((*it)->GetActive() && (Mask & (*it)->Mask()) == Mask)
		{
			_filteredEntities.push_back((*it));
		}
	}
	return _filteredEntities;
}

// god this is horrendous. Good version is much better.
void ObjectManager::DestroyEntity(Entity* e)
{
  if((e->Mask() & MC_Alive) == MC_Alive && e->GetActive())
  {
  	e->GetMask() &= ~(MC_Alive);
    e->Active(false);
  }
  _shouldUpdateRegistry = true;
}

void ObjectManager::FilterEntities(System *system)
{
  /*
	system->_entities.clear();
	for (auto it : _entities)
	{
		if (it->GetActive() && (system->_requirements & (it)->Mask()) == system->_requirements)
		{
			system->_entities.push_back(it);
		}
	}
  */
}

void ObjectManager::DestroyAll(void)
{
  for (auto it : _entities)
    DestroyEntity(it);
}

void ObjectManager::DestroyAll(std::vector<Entity *> &e)
{
  _shouldUpdateRegistry = true;
  for(auto it: e)
    DestroyEntity(it);
}

void ObjectManager::PurgeObjects(void)
{
  PurgePowerupInstances();
  // kill objects that are no longer alive
  std::vector<Entity *> objectsToPurge;
  for(auto it : _entities)
  {
    mask itsMask = it->Mask();
    mask isitAlive = it->Mask() & MC_Alive;

    if(!(it->GetActive()) && (it->Mask() & MC_Alive) != MC_Alive)
      objectsToPurge.push_back(it);
  }
  if(!objectsToPurge.empty())
    for(auto it : objectsToPurge)
      PurgeEntity(it);
}
void ObjectManager::PurgeEntity(Entity *e)
{
  

  // Loop through all the registry entries, and if the entity fits the mask, find it in the vector and remove it.
	for(auto &it : _registry)
  {
    if(FlagSet(e->GetMask(), it->_mask))
    {
      for(auto ents = it->_entities->begin(); ents != it->_entities->end(); ++ents)
      {
        if((*ents) == e)
        {
          *ents = it->_entities->back();
          it->_entities->pop_back();
          break;
        }
      } // loop through registry entry
    }
  }
  
  e->Free();
  
  // Find and delete the object to be destroyed.
  for(auto it = _entities.begin(); it != _entities.end(); ++it)
  {
    if(*it == e)
    {
      delete e;
      // store the last entity in the deleted object's place, then pop the old pointer to the last object since it took e's place.
      *it = _entities.back();
      _entities.pop_back();
      return;
    }
  }
}

void ObjectManager::PurgePowerupInstances(void)
{
  // kill powerup actors with no live parent
  std::vector<Entity *> children = FilterEntities(MC_Parent | MC_Powerup);
  for(auto it : children)
  {
    Entity * parent = it->GET_COMPONENT(Parent)->parent;
    bool found = false;
    for(auto it2 : _entities)
    {
      if(it2 == parent)
      {
        found = true;
        break;
      }
    }
    if(!found)
      DestroyEntity(it);
  }
}
bool ObjectManager::isAlive(Entity *e)
{
  for(auto it : _entities)
  {
    if(it == e)
      return true;
  }
  return false;
}

std::vector<Entity *> ObjectManager::GetEntityByName(const std::string &name)
{
  std::vector<Entity *> retVal;
  for(auto it : _entities)
  {
    if (it->GetName() == name)
    {
        retVal.push_back(it);
    }
  }

  return retVal;
}

std::vector<Entity *> ObjectManager::GetEntitiesWithoutName(std::vector<std::string> &name)
{
  std::vector<Entity *> retVal;
  bool flag = true;


  for(auto it : _entities)
  {
    for(auto id : name)
    {
      if (it->GetName() == id)
        flag = false;
    }
    if(flag == true)
      retVal.push_back(it);
    else
      flag = true;
  }

  return retVal;
}

void ObjectManager::_UpdateRegistryEntry(OMRegistryEntry *entry)
{
  entry->_entities->clear();
  entry->updated() = true;

  for (auto &entity : _entities)
  {
    if (FlagSet(entity->Mask(), entry->_mask))
      entry->_entities->push_back(entity);
  }
}
void ObjectManager::UpdateRegistry()
{
  if (_shouldUpdateRegistry == true)
  {
    ++_rebuilds;

    for (auto &it : _registry)
    {
      _UpdateRegistryEntry(it);
    }

    assert(_registry.size() == _threadpool.size());
    for (unsigned i = 0; i < _registry.size(); ++i)
    {
      _threadpool[i] = std::thread(&ObjectManager::_UpdateRegistryEntry, std::ref(*this), _registry[i]);
    }

    for (unsigned i = 0; i < _threadpool.size(); ++i)
      if (_threadpool[i].joinable())
        _threadpool[i].join();

    _shouldUpdateRegistry = false;
  }
  else
    for (auto &it : _registry)
      it->updated() = false;
}

void ObjectManager::RemoveRegistry(OMRegistryEntry *entry)
{
  if (entry == nullptr)
    return;
  for (auto it = _registry.begin(); it != _registry.end(); ++it)
  {
    if (entry == *it)
    {
      *it = _registry.back();
      _registry.pop_back();
      delete entry;
      if (_threadpool.back().joinable())
        _threadpool.back().join();
      _threadpool.pop_back();
      return;
    }
  }
  throw ("Attempted to remove a non-existent registry entry.");
}

