#include "stdafx.h"
#include "System.hpp"
#include "ObjectManager.hpp"

Gamestate::Gamestate(systype type,
                     systype parent,
                     std::string name)
                    :
                     System(type, name),
                     OM(new ObjectManager()),
                     _parent(parent),
                     _PreSystems(0),
                     _PostSystems(0)
{
}

void System::operator()(float dt)
{
  ENGINE->SetActiveSystem(_type);
  ADLib::PerfTimer perfTimer(&perfData, dt);
  Update(dt);
}

void System::operator()()
{
  ENGINE->SetActiveSystem(_type);
  ADLib::ScopeTimer initTimer(&perfData.initTime);
  Init();
}

Gamestate::~Gamestate()
{
  delete OM;
}