﻿//	File name:		System.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content © 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "Component.hpp"
#include "Entity.hpp"
#include <vector>
#include "Libraries/ADLib/PerfTimer.hpp"
#include "Libraries/ADLib/ScopeTimer.hpp"
struct OMRegistryEntry;
#pragma region SysEnums
enum sysRuntime
{
  srt_pre,
  srt_post,
  srt_any
};
enum systype
{
  sys_Test                = 1,
  sys_NetworkClient	      = 1 << 1,
  sys_Input               = 1 << 2,
  sys_AudioSystem         = 1 << 3,
  sys_CollisionDetection  = 1 << 4,
  sys_DoorSystem          = 1 << 5,
  sys_CollisionResolution = 1 << 6,
  sys_Graphics            = 1 << 7,
  sys_PlayerHandler       = 1 << 8,
  sys_Physics             = 1 << 9,
  sys_Particle            = 1 << 10,
  sys_Networking          = 1 << 11,
  sys_DungeonBuilder      = 1 << 12,
  sys_GridMap             = 1 << 13,
  sys_PlayerAI            = 1 << 14,
  sys_NarratorSystem      = 1 << 15,
  sys_ChestSystem         = 1 << 16,
  sys_PowerupHandler      = 1 << 17,
  sys_ObjectToWorldSystem = 1 << 18,
  sys_HUDsystem           = 1 << 19,
  sys_DungeonGameplay     = 1 << 20,
  sys_MenuGS              = 1 << 21,
  sys_BroadPhase          = 1 << 22,
  sys_EditorSystem        = 1 << 23,
  sys_EnemyAISystem       = 1 << 24,
  sys_ConfirmScreenGS     = 1 << 25,
  sys_DebugTools          = 1 << 26,
  sys_EnemyFactory        = 1 << 27,
  sys_BulletSystem        = 1 << 28,
  sys_TorchSystem         = 1 << 29,
  sys_ScriptSystem        = 1 << 30
};
typedef unsigned systems;
#pragma endregion

#pragma region Messaging

typedef unsigned message;

enum MessageDetails
{
  MSG_Collision = 1,
  
  MSG_Damage = 1 << 1,

  /* Sent when a player enters/exits a door.
  First entity is the player,
  second is the door*/
  MSG_Door = 1 << 2,

  /*Sent when a static tile is added to the map.
  First entity is the tile added,
  second is null*/
  MSG_Add_Tile = 1 << 3,

  /*Sent when the dungeon is cleared, and all walls/entities are destroyed
  Both entities are null*/
  MSG_Clear_Dungeon = 1 << 4,

  /*Sent when the dungeon is finished being built, and all walls/entities
  Both entities are null*/
  MSG_Built_Dungeon = 1 << 5,

  /*Sent when the camera switched rooms.
  The both entities are null*/
  MSG_Room_Change = 1 << 6,

  /*Sent when the engine is put into screenshot mode.
  The hud isn't drawn and only graphics runs so it is easy to capture beautiful screenshots*/
  MSG_Screenshot_Mode = 1 << 7,


  /*************
  These messages are for window events. They will never include entities.
  **************/
  /*Sent when the game window loses focus*/
  MSG_LoseFocus = 1 << 8,

  /*Sent when the game is minimized */
  MSG_Minimized = 1 << 9,

  /*Sent when the window regains focus*/
  MSG_GainFocus = 1 << 10,

  /*Sent when the window returns from being minimized*/
  MSG_Unminimized = 1 << 11
};

#pragma endregion
/**
* @brief This class defines the base interface used by the engine for all systems.
* @details Each system operates on any Entity with the components necessary for that system.
* The Register function tells the engine which components it needs to work.
* The engine determines which entities the system can perform data transformations on.
*/
// this class is probably what you're looking for.
// Sorry I don't have the system for getting valid components yet. this was a 2 hour crapshoot.
class System
{
public:


  System(systype type, std::string name = std::string("Unnamed System")) : _type(type), sysName(name), _requirements(MC_Alive),runWhilePaused(false),_registryEntry(nullptr) 
  {
    perfData.name = name;
  }
  //~System() { if (_registryEntry) delete _registryEntry; }
	/**
	 * @brief Registers a component dependency for the System with the Engine.
	 * @details Use this inside the System Init() function once for each component the System needs to function.
	 * This tells the engine how to filter
	 *
	 * @param C The typename of the derived component type needed by your system.
	 */
#define REGISTER_COMPONENT( C ) \
	Register_Component(MC_##C)

	// Don't use this use the macro.
	void Register_Component(MComponent mc){ _requirements |= mc; }

	/**
	 * @brief Function called to initialize system.
	 * @details Run any necessary start up and initialization code necessary for the system to function.
	 * This should include the REGISTER_COMPONENT() calls, any initialize code the system needs,
	 * and absolutely no actual logic as this will only be called once.
	 */
	virtual void Init(void) = 0;

	/**
	* @brief Contains all the runtime logic for the System.
	* @details Performs data transformations on all applicable objects
	* Updates should be dependent on the frame time, not a preset value like 1/60.
	* @param dt The time elapsed since the previous update call.
	*/
	virtual void Update(float dt){ if (dt) return; return; /* warning suppression.*/ }

	/**
	 * @brief Runs any terminating code, typically the inverse of Init().
	 * @details This will be the final function called before a System is destroyed.
	 * Use it to deallocate any memory, clear global/static variables, and any other
	 * housekeeping tasks necessary.
	 */
	virtual void Shutdown(void) = 0;


  virtual void SendMsg(Entity *, Entity *, message) {}

  void operator()(float dt);

  void operator()();

  systype _type;
	mask _requirements;
	std::vector<Entity *> _entities;
  bool runWhilePaused;
  float dt;
  OMRegistryEntry *_registryEntry;

  // performance benchmarking
  ADLib::performanceData perfData;


  std::string sysName;

};
/**
* @brief This class is a container for entries in the Entity Registry in the object manager.
*/
struct OMRegistryEntry
{
public:
  /*OMRegistryEntry(mask m, std::string& name, std::vector<Entity *> *entities)
  : _updated(false),
  _mask(m),
  _name(name),
  _entities(entities)
  {}*/
  OMRegistryEntry(System *sys)
    : _updated(false),
    _mask(sys->_requirements),
    _name(sys->sysName),
    _entities(&sys->_entities)
  {
    sys->_registryEntry = this;
  }

  OMRegistryEntry(std::vector<Entity *> *entities, mask Mask, std::string name)
    : _updated(false),
      _mask(Mask),
      _name(name),
      _entities(entities)
  {

  }

  ~OMRegistryEntry() { clear(); }

  friend class ObjectManager;
  /**
  * @brief checks if the registry has been updated
  */
  bool updated() const { return _updated; }
private:
  void clear() { _entities->clear(); }
  bool& updated() { return _updated; }
  bool _updated;
  mask _mask;
  std::string _name;
  std::vector<Entity *> *_entities;
};
class ObjectManager;
class Gamestate : public System
{
public:
  Gamestate(systype type, 
            systype parent, 
            std::string name = std::string("Unnamed Gamestate"));
  virtual ~Gamestate();
  virtual void Init() = 0;
  virtual void Shutdown() = 0;
  virtual void PoppedTo(systype) = 0;
  virtual void Update(float dt) = 0;
  
#define REGISTER_PRESYS( SYS ) \
  Register_PreSys(sys_##SYS)
#define REGISTER_POSTSYS( SYS) \
  Register_PostSys(sys_##SYS)
  void Register_PreSys(systype type){ _PreSystems |= type; }
  void Register_PostSys(systype type){ _PostSystems |= type; }
  mask PreSystems(void) {return _PreSystems;}
  mask PostSystems(void){return _PostSystems;}

  ObjectManager *OM;

private:
  const systype _parent;
  mask _PreSystems;
  mask _PostSystems;
};
