//	File name:		Actions.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Connor Drose
//	
//	All content � 2014 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../../Entity.hpp"
#include <vector>

class Action
{
  float _time;
  float _duration;
  float _percentDone;
  Entity *_gameObj;

  void Update();
};


