#include "stdafx.h"
#include "Astar.h"

#include "../../Systems/Gameplay/DungeonGameplay.h"

#define IsWall2(x, y) IsWall(x, y, true)

namespace PathFind
{
	const int diagonalCost = (int)(FIXED_POINT_FLOAT * sqrt(2.0f));
	float weight;

	int octileHeuristicCosts[MAX_WIDTH + 1][MAX_WIDTH + 1];

	int OctileHeuristic(int curR, int curC, int goalR, int goalC)
	{
		assert(goalR != -1);
		assert(goalC != -1);
		assert(curR != -1);
		assert(curC != -1);

		int dr = abs(goalR - curR);
		int dc = abs(goalC - curC);

		return octileHeuristicCosts[dr][dc];
	}

	void Astar::Reset()
	{
		inProgress = false;
		goalC = -1;
		goalR = -1;
		iteration++;
	}

	void Astar::Initialize(int(*h)(int, int, int, int), float hWeight, bool singleStep)
	{
		heuristic = h;

		width = g_terrain->GetWidth();
		height = g_terrain->GetHeight();

		inProgress = false;

		for (int dr = 0; dr < MAX_WIDTH + 1; dr++)
		{
			for (int dc = 0; dc < MAX_WIDTH + 1; dc++)
			{
				octileHeuristicCosts[dr][dc] = (int)((hWeight * ((float)min(dr, dc) * sqrt(2.0f) + (float)std::max(dr, dc) - (float)min(dr, dc))) * FIXED_POINT_FLOAT);

				if (dr < width && dc < height)
				{
					//flip y to account for map being upside down
					positionInformation[dr][dc] = g_terrain->GetCoordinates(dr, height - dc - 1);
				}
			}
		}

		weight = hWeight;

		this->singleStep = singleStep;
	}

	void Astar::VisitNodeDiagonal(int r, int c, Node* parent)
	{
		int g = parent->cost + diagonalCost;

		int total = g + heuristic(r, c, goalR, goalC);

		Node* child = &allNodes[r][c];

		bool curIt = child->iteration == iteration;

		if (curIt && child->onOpen)
		{
			//Child is already on the open list
			if (total < child->total)
			{
				child->cost = g;
				child->total = total;
				child->parentRow = parent->row;
				child->parentCol = parent->col;

				openList.insert(HeapNode(child, total));
			}
		}
		//Check if child is already on closed list
		else if (curIt && child->onClosed)
		{
			if (total < child->total)
			{
				child->cost = g;
				child->total = total;
				child->parentRow = parent->row;
				child->parentCol = parent->col;
				child->onClosed = false;
				child->onOpen = true;

				openList.insert(HeapNode(child, total));
			}
		}
		//child is not on openlist or closed list, add to openlist
		else
		{
			child->row = r;
			child->col = c;

			child->cost = g;
			child->total = total;
			child->parentRow = parent->row;
			child->parentCol = parent->col;
			child->onClosed = false;
			child->onOpen = true;
			child->iteration = iteration;

			openList.insert(HeapNode(child, total));
		}
	}

	void Astar::VisitNodeNonDiagonal(int r, int c, Node* parent)
	{
		int g = parent->cost + FIXED_POINT_INT;

		int total = g + heuristic(r, c, goalR, goalC);

		Node* child = &allNodes[r][c];

		bool curIt = child->iteration == iteration;

		if (curIt && child->onOpen)
		{
			//Child is already on the open list
			if (total < child->total)
			{
				child->cost = g;
				child->total = total;
				child->parentRow = parent->row;
				child->parentCol = parent->col;

				openList.insert(HeapNode(child, total));
			}
		}
		//Check if child is already on closed list
		else if (curIt && child->onClosed)
		{
			if (total < child->total)
			{
				child->cost = g;
				child->total = total;
				child->parentRow = parent->row;
				child->parentCol = parent->col;
				child->onClosed = false;
				child->onOpen = true;

				openList.insert(HeapNode(child, total));
			}
		}
		//child is not on openlist or closed list, add to openlist
		else
		{
			child->row = r;
			child->col = c;

			child->cost = g;
			child->total = total;
			child->parentRow = parent->row;
			child->parentCol = parent->col;
			child->onClosed = false;
			child->onOpen = true;
			child->iteration = iteration;

			openList.insert(HeapNode(child, total));
		}
	}

	void Astar::VisitAllNeighbors(Node * currentNode)
	{
		int curR = currentNode->row;
		int curC = currentNode->col;

		if (!g_terrain->IsWall2(curR + 1, curC))
		{
			if (!g_terrain->IsWall2(curR + 1, curC - 1) && !g_terrain->IsWall2(curR, curC - 1))
			{
				VisitNodeDiagonal(curR + 1, curC - 1, currentNode);
			}

			VisitNodeNonDiagonal(curR + 1, curC, currentNode);

			if (!g_terrain->IsWall2(curR + 1, curC + 1) && !g_terrain->IsWall2(curR, curC + 1))
			{
				VisitNodeDiagonal(curR + 1, curC + 1, currentNode);
			}
		}

		if (!g_terrain->IsWall2(curR, curC + 1))
		{
			VisitNodeNonDiagonal(curR, curC + 1, currentNode);

			if (!g_terrain->IsWall2(curR - 1, curC + 1) && !g_terrain->IsWall2(curR - 1, curC))
			{
				VisitNodeDiagonal(curR - 1, curC + 1, currentNode);
			}
		}

		if (!g_terrain->IsWall2(curR - 1, curC))
		{
			VisitNodeNonDiagonal(curR - 1, curC, currentNode);

			if (!g_terrain->IsWall2(curR - 1, curC - 1) && !g_terrain->IsWall2(curR, curC - 1))
			{
				VisitNodeDiagonal(curR - 1, curC - 1, currentNode);
			}
		}

		if (!g_terrain->IsWall2(curR, curC - 1))
		{
			VisitNodeNonDiagonal(curR, curC - 1, currentNode);
		}
	}

	bool Astar::Run(Point2D &start, Point2D& goal)
	{
		if (!inProgress)
		{
			g_terrain->GetRowColumn(goal, &goalR, &goalC);


			//Flip y to account for the map being upside down
			goalC = height - goalC - 1;

			goalNode = &allNodes[goalR][goalC];

			openList.Clear();

			int curR, curC;

			g_terrain->GetRowColumn(start, &curR, &curC);

			//Flip y to account for the map being upside down
			curC = height - curC - 1;

			//Initialize Start Node
			Node *startNode = &allNodes[curR][curC];
			startNode->parentCol = -1;
			startNode->parentRow = -1;
			startNode->row = curR;

			startNode->col = curC;
			startNode->cost = 0;
			startNode->total = heuristic(curR, curC, goalR, goalC);
			startNode->iteration = iteration;

			openList.insert(HeapNode(startNode, startNode->total));

			startNode->onOpen = true;
			startNode->onClosed = false;

			inProgress = true;
		}

		while (openList.size())
		{
			Node* cheapestNode = 0;

			do
			{
				if (!openList.size())
				{
					goto noOpenList;
				}

				cheapestNode = openList.MinElement().node;
				openList.Pop();
			} while (cheapestNode->onClosed);

			if (cheapestNode == goalNode)
			{
				return true;
			}

			VisitAllNeighbors(cheapestNode);

			cheapestNode->onOpen = false;
			cheapestNode->onClosed = true;

			if (singleStep)
			{
				return false;
			}
		}

	noOpenList:
		inProgress = false;
		return false;
	}
}
