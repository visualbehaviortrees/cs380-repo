#pragma once

#define FIXED_POINT_INT 10000
#define FIXED_POINT_FLOAT 10000.0f

typedef std::list<Point2D> WaypointList;

#include "../../Libraries/Containers/MinHeap.h"
#include "../../Libraries/Containers/Node.h"

//#include "OpenList.h"

namespace PathFind
{
	int OctileHeuristic(int curR, int curC, int goalR, int goalC);

	class Astar
	{
	public:
		Astar() : inProgress(false) {}

		void Initialize(int(*h)(int, int, int, int), float hWeight, bool singleStep);

		bool Run(Point2D &start, Point2D &goal);

		void Reset();

		inline bool InProgress();

		inline void PopulatePath(WaypointList& path);

	private:
		void VisitNodeNonDiagonal(int r, int c, Node* parent);

		void VisitNodeDiagonal(int r, int c, Node* parent);

		void VisitAllNeighbors(Node * currentNode);

		void VisitNeighborsParentBelowLeft(Node* currentNode);

		void VisitNeighborsParentBelow(Node* currentNode);

		void VisitNeighborsParentBelowRight(Node* currentNode);

		void VisitNeighborsParentRight(Node* currentNode);

		void VisitNeighborsParentAboveRight(Node* currentNode);

		void VisitNeighborsParentAbove(Node* currentNode);

		void VisitNeighborsParentAboveLeft(Node* currentNode);

		void VisitNeighborsParentLeft(Node* currentNode);

		void SetSolution();

		int goalR = -1;
		int goalC = -1;
		Node *goalNode;

		Node allNodes[MAX_WIDTH][MAX_WIDTH];

		Point2D positionInformation[MAX_WIDTH][MAX_WIDTH];

		int(*heuristic)(int, int, int, int);

		Node* solution[MAX_WIDTH * MAX_WIDTH];
		int solutionSize = 0;

		bool inProgress;

		int width;
		int height;

		int iteration = 1;

		bool singleStep;

		MinHeap openList;
	};

	inline bool Astar::InProgress()
	{
		return inProgress;
	}

	inline void Astar::PopulatePath(WaypointList& path)
	{
		Node *node = goalNode;

		while (node->parentRow != -1 && node->parentCol != -1)
		{
			path.push_front(positionInformation[node->row][node->col]);

			node = &allNodes[node->parentRow][node->parentCol];
		}

		path.push_front(positionInformation[node->row][node->col]);
	}
}