//	File name:		AttackState.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Connor Drose
//	
//	All content � 2014 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "../../Components/PlayerAI.h"
#include "../../Components/Components.h"
#include "AIPlayerSystem.hpp"
#include "../../Engine.hpp"
#include "../Input/InputSystem.hpp"

#define DEBUG 1


static const float orbitRadius = 3.f;  // how far away from target to orbit


using namespace Math;


// HELPER FUNCTIONS
/////////////////////////////////////////////

// set the AI's destination
void AIPlayerSystem::FindNewAttackDestination()
{
  // returning to base takes precidence
  if(DecideReturnToBase())
    return;


  Point2D dest = ai->enemyTarget->GET_COMPONENT(Transform)->Position;
  // get somewhat random vector pointing away from target
  Vector2D offset = trans->Position - dest;
  const float rLim = 70.f; 
  float randDegree = float(rand() % int(rLim * 2)) - rLim;  // between -rD and +rD
  offset.Normalize();

  Math::RotateVector(offset, randDegree);
  offset *= orbitRadius;

  dest += offset;
  ai->Move.SetDestination(dest);
}


// ATACK STATE
///////////////////////////////////////

void AIPlayerSystem::AttackStateInit()
{
  #if DEBUG 
  std::cout << "AI Attack State\n"; 
  #endif
  FindNewAttackDestination();

}


// Chase and move around enemy player
void AIPlayerSystem::AttackState(float dt)
{
  // when to set new destination  ////////
  // reached destination
  if(CloseEnough(trans->Position, ai->destination)){
    FindNewAttackDestination();
  }
  
  // LOTS MORE STUFF HERE



}

