//	File name:		Turret.h
//	Project name:	Shot In The Dark
//	Author(s):		Connor Drose
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once
#include "stdafx.h"
#include <unordered_map>
#include <fstream>
#include "BehaviorTree.h"

#define ShortTextOn 1

void PushText(Entity *e, string text)
{
  if (!e->HasComponent(EC_SpriteText))
    e->AttachComponent(new SpriteText);

  e->GC(SpriteText)->aibehavior << ">" << text;

  if (ShortTextOn)
  {
	  // get index of cutoff point
	  string aistring = "...";
	  string test = e->GC(SpriteText)->aibehavior.str();
	  int length = e->GC(SpriteText)->aibehavior.str().length();
	  int delimitcount = 0;
	  for (int i = length - 1; i >= 0; --i)
	  {
		  if (e->GC(SpriteText)->aibehavior.str()[i] == '>')
		  {
			  ++delimitcount;
		  }

		  if (delimitcount == 2 && i != 0) // crop remaining string at beginning
		  {
			  aistring += e->GC(SpriteText)->aibehavior.str().substr(i);
			  e->GC(SpriteText)->aibehavior.str("");
			  e->GC(SpriteText)->aibehavior << aistring;
			  break;
		  }
	  }
  }
}

std::unordered_map<std::string, LogicNodeCreateFn> logicNodeMapGenerator()
{
  std::unordered_map<std::string, LogicNodeCreateFn> logicNodes;
#undef BEHAVIOR_TREE_NODES_H
#undef DEFINE_LEAF_NODE 
#undef DEFINE_SELECTOR_NODE
#undef DEFINE_DECORATOR_NODE
#define DEFINE_LEAF_NODE(X, Y, Z) 
#define DEFINE_SELECTOR_NODE(X, Y, Z) logicNodes.emplace(#X, &X::CreateInstance);
#define DEFINE_DECORATOR_NODE(X, Y, Z) logicNodes.emplace(#X, &X::CreateInstance);

#include "BehaviorTreeNodes.h"

#undef BEHAVIOR_TREE_NODES_H
#undef DEFINE_SELECTOR_NODE
#undef DEFINE_DECORATOR_NODE
#undef DEFINE_LEAF_NODE 

  return logicNodes;
}

std::unordered_map<std::string, LeafNodeCreateFn> leafNodeMapGenerator()
{
  std::unordered_map<std::string, LeafNodeCreateFn> leafNodes;

#undef BEHAVIOR_TREE_NODES_H
#undef DEFINE_LEAF_NODE 
#undef DEFINE_SELECTOR_NODE
#undef DEFINE_DECORATOR_NODE
#define DEFINE_SELECTOR_NODE(X, Y, Z) 
#define DEFINE_DECORATOR_NODE(X, Y, Z) 
#define DEFINE_LEAF_NODE(X, Y, Z) leafNodes.emplace(#X, &X::CreateInstance);

#include "BehaviorTreeNodes.h"

#undef BEHAVIOR_TREE_NODES_H
#undef DEFINE_SELECTOR_NODE
#undef DEFINE_DECORATOR_NODE
#undef DEFINE_LEAF_NODE 
  
  
  return leafNodes;
}

void GenerateNodesFiles()
{
  std::ofstream NodesFile("nodes.vbt");

  // First, output the selector nodes:
  NodesFile << "Selectors\n";
#undef BEHAVIOR_TREE_NODES_H
#undef DEFINE_LEAF_NODE 
#undef DEFINE_SELECTOR_NODE
#undef DEFINE_DECORATOR_NODE
#define DEFINE_SELECTOR_NODE(X, Y, Z) NodesFile << "\t" #X " " #Y "\n";
#define DEFINE_DECORATOR_NODE(X, Y, Z) 
#define DEFINE_LEAF_NODE(X, Y, Z)

#include "BehaviorTreeNodes.h"

#undef BEHAVIOR_TREE_NODES_H
#undef DEFINE_SELECTOR_NODE
#undef DEFINE_DECORATOR_NODE
#undef DEFINE_LEAF_NODE 

  // second, output the decorator nodes:
  NodesFile << "Decorators\n";
#undef BEHAVIOR_TREE_NODES_H
#undef DEFINE_LEAF_NODE 
#undef DEFINE_SELECTOR_NODE
#undef DEFINE_DECORATOR_NODE
#define DEFINE_SELECTOR_NODE(X, Y, Z) 
#define DEFINE_DECORATOR_NODE(X, Y, Z) NodesFile << "\t" #X " " #Y "\n";
#define DEFINE_LEAF_NODE(X, Y, Z)

#include "BehaviorTreeNodes.h"

#undef BEHAVIOR_TREE_NODES_H
#undef DEFINE_SELECTOR_NODE
#undef DEFINE_DECORATOR_NODE
#undef DEFINE_LEAF_NODE 

  // second, output the decorator nodes:
  NodesFile << "Leaves\n";
#undef BEHAVIOR_TREE_NODES_H
#undef DEFINE_LEAF_NODE 
#undef DEFINE_SELECTOR_NODE
#undef DEFINE_DECORATOR_NODE
#define DEFINE_SELECTOR_NODE(X, Y, Z) 
#define DEFINE_DECORATOR_NODE(X, Y, Z) 
#define DEFINE_LEAF_NODE(X, Y, Z) NodesFile << "\t" #X " " #Y "\n";

#include "BehaviorTreeNodes.h"

#undef BEHAVIOR_TREE_NODES_H
#undef DEFINE_SELECTOR_NODE
#undef DEFINE_DECORATOR_NODE
#undef DEFINE_LEAF_NODE 
}



std::unordered_map<std::string, LogicNodeCreateFn>& LogicNodesMap()
{
  static std::unordered_map<std::string, LogicNodeCreateFn> nodes{ logicNodeMapGenerator() };
  return nodes;
}

std::unordered_map<std::string, LeafNodeCreateFn>& LeafNodesMap()
{
  static std::unordered_map<std::string, LeafNodeCreateFn> nodes{ leafNodeMapGenerator() };
  return nodes;
}

IBTLogicNode * createLogicNode(std::string name)
{
  auto nodes = LogicNodesMap();
  auto it = nodes.find(name);
  if (it != nodes.end())
    return (*it).second();
  else
    return nullptr;

}


IBTNode * createLeafNode(std::string name)
{
  auto nodes = LeafNodesMap();
  auto it = nodes.find(name);
  if (it != nodes.end())
    return (*it).second();
  else
    return nullptr;
}

IBTLogicNode * createLogicNode(std::string name, std::string varname)
{
  auto nodes = LogicNodesMap();
  auto it = nodes.find(name);
  if (it != nodes.end())
  {
    IBTLogicNode * node = (*it).second();
    *const_cast<std::string *>(&(node->m_name)) = varname;
    return node;
  }
  else
    return nullptr;
}

IBTNode * createLeafNode(std::string name, std::string varname)
{
  auto nodes = LeafNodesMap();
  auto it = nodes.find(name);
  if (it != nodes.end())
  {
    IBTNode * node = (*it).second();
    *const_cast<std::string *>(&(node->m_name)) = varname;
    return node;
  }
  else
    return nullptr;
}
