//	File name:		Turret.h
//	Project name:	Shot In The Dark
//	Author(s):		Connor Drose
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include <string>
#include <vector>
#include <iostream>

using std::vector;
using std::string;

void PushText(Entity *, string);

enum NodeStatus
{
  NS_Failed,
  NS_Completed,
  NS_Running,
  NS_OnEnter
};

enum NodeType
{
  NT_Selector = 0,
  NT_Decorator = 1,
  NT_Leaf = 2
};

class Entity;

#define DEBUG_BT 1

typedef Entity* EntityPtr;

struct IBTNode
{
  virtual ~IBTNode() {}
  IBTNode(string name, string description, NodeType type) : m_name(name), m_description(description), 
	  m_type(type), currentStatus(NS_OnEnter) {}
  const string m_name;
  const string m_description;
  IBTNode *p_parent;
  virtual NodeStatus Update(EntityPtr, float dt) = 0;
  const NodeType m_type;

  void ResetStatus()
  { 
	  currentStatus = NS_OnEnter;
  }

  protected:
	  NodeStatus currentStatus;
};

struct IBTLogicNode : IBTNode
{
  virtual ~IBTLogicNode() { freeKids(); }
  IBTLogicNode(string name, string description, NodeType type) : IBTNode(name, description, type), childStatus(NS_OnEnter){}
  vector<IBTNode *> m_children;
  virtual NodeStatus Update(EntityPtr, float dt) = 0;
  int m_currentChildIndex = -1;

  void ResetLogicStatus()
  {
	  ResetStatus();
	  m_currentChildIndex = -1;

	  for (auto it = m_children.begin(); it != m_children.end(); ++it)
	  {
		  IBTLogicNode *logic = dynamic_cast<IBTLogicNode*>(*it);

		  if (logic)
		  {
			  logic->ResetLogicStatus();
		  }
		  else
		  {
			  (*it)->ResetStatus();
		  }
	  }
  }

protected:
  void freeKids()
  {
    for (auto& it : m_children)
    {
      if (it != nullptr)
      {
        delete it;
        it = nullptr;
      }
    }
  }

  NodeStatus childStatus;
};

typedef IBTNode * (*LeafNodeCreateFn)();
typedef IBTLogicNode * (*LogicNodeCreateFn)();

#define DEFINE_LEAF_NODE(NAME, DESCRIPTION, VARS) \
  struct NAME : public IBTNode\
  { \
    static IBTNode *CreateInstance() { return new NAME; } \
    ~##NAME() override { } \
    NAME() : IBTNode(#NAME, DESCRIPTION, NT_Leaf){} \
    NAME(std::string name) : IBTNode(name, DESCRIPTION, NT_Leaf){} \
    NodeStatus Update(EntityPtr self, float dt) override; \
	VARS \
  };

#define DEFINE_SELECTOR_NODE(NAME, DESCRIPTION, VARS) \
  struct NAME : public IBTLogicNode\
  { \
    static IBTLogicNode *CreateInstance() { return new NAME; } \
    ~##NAME() override { freeKids(); } \
  	NAME() : IBTLogicNode(#NAME, DESCRIPTION, NT_Selector){} \
    NAME(std::string name) : IBTLogicNode(name, DESCRIPTION, NT_Selector){} \
    NodeStatus Update(EntityPtr self, float dt) override; \
	VARS \
  };

#define DEFINE_DECORATOR_NODE(NAME, DESCRIPTION, VARS) \
  struct NAME : public IBTLogicNode\
  { \
    static IBTLogicNode *CreateInstance() { return new NAME; } \
    ~##NAME() override { freeKids(); } \
  	NAME() : IBTLogicNode(#NAME, DESCRIPTION, NT_Decorator){} \
    NAME(std::string name) : IBTLogicNode(name, DESCRIPTION, NT_Decorator){} \
    NodeStatus Update(EntityPtr self, float dt) override; \
    VARS \
  };

#define MEMBER_VAR(TYPE, NAME) TYPE NAME;
#undef BEHAVIOR_TREE_NODES_H
#include "BehaviorTreeNodes.h"


#undef LOGIC_UPDATE_FUNC
#undef LEAF_UPDATE_FUNC
#undef MEMBER_VAR
#define MEMBER_VAR(X, Y)

#define LOGIC_UPDATE_FUNC( CLASS ) \
  NodeStatus CLASS::Update(EntityPtr self, float dt) \
{ \
	if(DEBUG_BT) \
		PushText(self, m_name); \
if(m_currentChildIndex != -1) \
{\
	 childStatus = m_children[m_currentChildIndex]->Update(self, dt); \
}\

#define END_LOGIC_UPDATE_FUNC \
if (currentStatus == NS_Completed || currentStatus == NS_Failed)\
{\
	NodeStatus retval = currentStatus; \
	m_currentChildIndex = -1;\
	currentStatus = NS_OnEnter; \
	for (IBTNode * node : m_children)\
	{\
		IBTLogicNode *logic = dynamic_cast<IBTLogicNode*>(node);\
		if (logic)\
		{\
			logic->ResetLogicStatus();\
		}\
		else\
		{\
			node->ResetStatus(); \
		}\
	}\
	return retval; \
}\
return currentStatus; \
}

#define LEAF_UPDATE_FUNC( X ) \
  NodeStatus X::Update(EntityPtr self, float dt) \
{ \
if(currentStatus == NS_Completed || currentStatus == NS_Failed)\
{\
	currentStatus = NS_OnEnter; \
}\
if(DEBUG_BT) \
		PushText(self, m_name); \

#define END_LEAF_UPDATE_FUNC \
return currentStatus; \
}


IBTLogicNode * createLogicNode(std::string name);
IBTNode * createLeafNode(std::string name);
IBTLogicNode * createLogicNode(std::string name, std::string varname);
IBTNode * createLeafNode(std::string name, std::string varname);



void GenerateNodesFiles();

/*
#undef BEHAVIOR_TREE_NODES_H
#undef DEFINE_LEAF_NODE 
#undef DEFINE_SELECTOR_NODE
#undef DEFINE_DECORATOR_NODE
#undef LOGIC_UPDATE_FUNC
#undef LEAF_UPDATE_FUNC
#undef MEMBER_VAR
#define LOGIC_UPDATE_FUNC( X )
#define LEAF_UPDATE_FUNC( X )
#define MEMBER_VAR( X, Y )
*/
