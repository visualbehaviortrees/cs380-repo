//	File name:		GatherState.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Connor Drose
//	
//	All content � 2014 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "AIPlayerSystem.hpp"

#define DEBUG 1

extern const float sightRange;


bool AIPlayerSystem::DecideReturnToBase()
{
    // when to return to base
  float returnToBaseDist = 0.f;
  std::vector<Entity*> flags = ENGINE->OM->FilterEntities(MC_Flag);
  for(auto it : flags)
  {
    if(it->GET_COMPONENT(Parent)->parent == owner)
      returnToBaseDist += 15.f + rand() % 10; // how badly each flag makes me want to return to base
  }
  if(trans->Position.Distance(basePos) < returnToBaseDist){
    ai->move.SetDestination(basePos);
    return true;
  }
  return false;
}


// sets a different ai->destination 
void AIPlayerSystem::FindNewGatherDestination()
{
  // if targetting flag
  /*if(ai->objTarget){
    SetAIDestination(ai->objTarget->GET_COMPONENT(Transform)->Position);
    return;
  }*/


  // when to return to base
  if(DecideReturnToBase())
    return;


  ai->move.SetDestination(glm::vec2(0.f, -5.f));

  // go to enemy flag
  std::vector<Entity*> flags = ENGINE->OM->FilterEntities(MC_Flag);
  for(auto it : flags)
  {
    if(it->GC(Team)->teamID == team->teamID)
      continue;
    if(it->GC(Parent)->parent == owner)
      continue;

    glm::vec2 pos(it->GC(Transform)->Position.x, it->GC(Transform)->Position.y);
    ai->move.SetDestination(pos);
    return;
  }


  // if all else fails go to random position
  float wMax = 15.f;
  float yMax = 10.f;
  float randX = rand() % int(wMax * 2) - wMax;
  float randY = rand() % int(yMax * 2) - yMax;
  //ai->Move.SetDestination(Point2D(randX, randY));
}


void AIPlayerSystem::GatherStateInit()
{
  #if DEBUG // TEMPORARY
  std::cout << "AI Gather State\n";
  #endif

  // set search direction
  ai->searchDir.x = float(rand() % 2) - 1.f;
  ai->searchDir.y = float(rand() % 2) - 1.f;

  FindNewGatherDestination();
}


// sets ai->target if an object is found
//void LookForObjects(PlayerAI *ai, Transform *trans, Entity* me, Team *team, Vector2D &basePos)
//{
//
//
//}


// sets ai->destination: looks for objectives, also determines when to return to base
void AIPlayerSystem::GatherState(float dt) 
{
  //LookForObjects(ai, trans, owner, team, basePos);


  // if we are targeting an objective, follow objective
  //if(ai->objTarget)
  //{
  //  SetAIDestination(ai->objTarget->GET_COMPONENT(Transform)->Position);
  //  // when to stop targeting object
  //  if(ai->objTarget->GET_COMPONENT(Parent)->parent == owner)
  //    ai->objTarget = NULL;

  //  return;
  //}
  // if we are searching randomly
  /*else*/
  {
    // if reached destination, make new destination
    if(CloseEnough(trans->Position, ai->destination))
      FindNewGatherDestination();
  }

}
