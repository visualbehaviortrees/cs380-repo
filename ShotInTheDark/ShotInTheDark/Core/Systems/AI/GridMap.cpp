//	File name:		GridMap.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Connor Drose
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include <vector>
#include "GridMap.hpp"
#include "../../Engine.hpp"
#include "../../Components/Components.h"
#include "../../Libraries/Math/Math2D.hpp"
#include "../../Components/Mesh.hpp"

#include "../Gameplay/DungeonGameplay.h"

extern Engine *ENGINE;
static int Wall_Num;


// SET SIZE OF TILES IN GridMap::Init


// SET _width and _height before initializing Gridmap!!!
void GridMap::Init()
{
  //// SET SIZE OF TILES HERE
  //tileSize = 1.4f; // Default
  //_worldCenter = Math::Point2D(0,0);

  //std::vector<int> rawMap(_width * _height, 0); // create empty raw map

  //_worldWidth = _width * tileSize;
  //_worldHeight = _height * tileSize;

  //// allocate 2D vector
  //_map.resize(_width);
  //for(int i = 0; i < _width; i++)
  //  _map[i].resize(_height);

  ////initialize map values
  //for(int x = 0; x < _width; x++){
  //  for(int y = 0; y < _height; y++)
  //  {
  //    Plot &p = _map[x][y];

  //    switch(rawMap[y*_width + x]){  // SHOULD BE _rawMap
  //      case 0: p.collision = false; break;
  //      case 1: p.collision = true; break;
  //    }
  //    const float halfWidth = static_cast<float>(_width / 2);
  //    const float halfHeight = static_cast<float>(_height / 2);
  //    p.worldPos.x = _worldCenter.x + (x - halfWidth) * tileSize + (tileSize / 2);
  //    p.worldPos.y = _worldCenter.y + (y - halfHeight) * tileSize + (tileSize / 2);

  //    p.plotNum.x = x;
  //    p.plotNum.y = y;
  //  }
  //}
}

void GridMap::Update(float dt)
{

}

void GridMap::ClampToMap(Vector2DInt& index, std::vector < std::vector<Plot>> map)
{


	// Clamp x
	if (index.x < 0)
		index.x = 0;
	if ((unsigned) index.x >= map.size())
		index.x = map.size() - 1;

	// Clamp y
	if (index.y < 0)
		index.y = 0;
	if ((unsigned) index.y >= map.at(0).size())
		index.y = map.at(0).size() - 1;

}


void GridMap::ClampToMap(Vector2DInt& index)
{
	// Use the static function with our map
	ClampToMap(index, _map);
}

// give me a world position, I'll return a pointer to the corresponding plot data
Plot* GridMap::GetPlot(Math::Vector2D pos)
{
	//Use the gameplay system's getIndex for dimensional consistiency
	Vector2DInt index;
	DungeonGameplay* d = dynamic_cast<DungeonGameplay*>(ENGINE->getCurrentGamestate());
	if (d != 0)
	{
		// Offset to the center of the grid, instead of the intersection
		index = d->WorldToDungeon(pos);
		// Clamp it to our size
		//ClampToMap(index);
		return &_map[index.x][index.y];
	}
	else
	{
	  const float halfWorldWidth = _width / 2 * tileSize;
	  const float halfWorldHeight = _height / 2 * tileSize;

	  // error checking : if position is out of bounds
	  if(pos.x >= halfWorldWidth || pos.x <= -halfWorldWidth ||
	  pos.y >= halfWorldHeight || pos.y <= -halfWorldHeight){
	    //std::cout << "Position :" << pos.x << ", " << pos.y << "is out of grid area\n";
	    return NULL;
	  }

	  float scaledPosX = (pos.x + halfWorldWidth);
	  float scaledPosY = (pos.y + halfWorldHeight);

	  int x = (int)(scaledPosX / tileSize);
	  int y = (int)(scaledPosY / tileSize);
	  return &_map[x][y];
	}

}

bool GridMap::WithinMap(Math::Vector2D pos)
{
  if(pos.x < -_worldWidth || pos.x > _worldWidth)
    return false;
  if(pos.y < -_worldHeight || pos.y > _worldHeight)
    return false;
  return true;
}

void GridMap::Reinitialize(std::vector<int> CollisionInfo, int w, int h, float tilesize_)
{
	Reset();
	tileSize = tilesize_;
	_width = w;
	_height = h;

	_rawMap = CollisionInfo;

	_worldWidth = _width * tileSize;
  _worldHeight = _height * tileSize;

	// allocate 2D vector
  _map.resize(_width);
  for(int i = 0; i < _width; i++)
    _map[i].resize(_height);

  //initialize map values
  for(int x = 0; x < _width; x++){
    for(int y = 0; y < _height; y++)
    {
      Plot &p = _map[x][y];

      const float halfWidth = static_cast<float>(_width / 2);
      const float halfHeight = static_cast<float>(_height / 2);

	  //Use the gameplay system's getIndex for dimensional consistiency
	  DungeonGameplay* d = dynamic_cast<DungeonGameplay*>(ENGINE->getCurrentGamestate());
	  if (d != 0)
	  {
		  // Offset to the center of the grid, instead of the intersection
		  p.worldPos = d->DungeonToWorld(x, y);
	  }
	  else
	  {
		  p.worldPos.x = (x - halfWidth) * tileSize + (tileSize / 2);
		  p.worldPos.y = (y - halfHeight) * tileSize + (tileSize / 2);
	  }

      p.plotNum.x = x;
      p.plotNum.y = y;

	    // Set collision and build tiles
      if (_rawMap[y*_width + x]){
        p.collision = true;
        //PlaceWall(_map, x, y);
      }
      else
        p.collision = false;
    }
  }

}


void GridMap::Reset()
{
  for(int x = 0; x < _width; x++)
    for(int y = 0; y < _height; y++)
      _map[x][y].collision = false;

  //_width = mapWidth;
  //_height = mapHeight;

  //std::vector<int> rawMap(_width * _height, 0); // create empty raw map

  //_worldWidth = _width * tileSize;
  //_worldHeight = _height * tileSize;

  //// allocate 2D vector
  //_map.resize(_width);
  //for (int i = 0; i < _width; i++)
  //  _map[i].resize(_height);

  ////initialize map values
  //for (int x = 0; x < _width; x++){
  //  for (int y = 0; y < _height; y++)
  //  {
  //    Plot &p = _map[x][y];

  //    switch (rawMap[y*_width + x]){  // SHOULD BE _rawMap
  //    case 0: p.collision = false; break;
  //    case 1: p.collision = true; break;
  //    }
  //    const float halfWidth = static_cast<float>(_width / 2);
  //    const float halfHeight = static_cast<float>(_height / 2);
  //    p.worldPos.x = _worldCenter.x + (x - halfWidth) * tileSize + (tileSize / 2);
  //    p.worldPos.y = _worldCenter.y + (y - halfHeight) * tileSize + (tileSize / 2);

  //    p.plotNum.x = x;
  //    p.plotNum.y = y;
  //  }
  //}
}


void GridMap::Shutdown()
{
  for(int x = 0; x < _width; x++)
    _map[x].clear();
  _map.clear();
}

void GridMap::SendMsg(Entity* a, Entity* b, message m)
{
	Point2D pos;
	switch (m)
	{
		// A collision tile has been added, and we need to 
		// mark it's plot as collided
	case MSG_Add_Tile:
		pos = a->GET_COMPONENT(Transform)->Position;
		if (WithinMap(pos))
		{
			GetPlot(pos)->collision = true;
			GetPlot(pos)->worldPos = pos;
		}
		break;
		// The dungeon has been destroyed, and we need to
		// mark all tiles as open
	case MSG_Clear_Dungeon:
		Reset();
		break;
	}
}



