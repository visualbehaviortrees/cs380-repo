//	File name:		GridMap.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Connor Drose
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../../System.hpp"
#include "../../Libraries/Math/Math2D.hpp"

static float tileSize;  // set me under GridMap::Init()

struct Plot
{
  // constructor
  Plot() {
    parent.x = 0; parent.y = 0; collision = false; moveScore = 0;
    parentFound = false;
  }

  bool collision;
  Math::Vector2D worldPos;  // set in gridmap init
  Math::Vector2DInt plotNum;// set in gridmap init
  int moveScore;
  Math::Vector2DInt parent;
  bool parentFound;
};


class GridMap : public System
{
public:
	GridMap(void) : System(sys_GridMap, "Grid Map System"), _width(0), _height(0){}
	void Init(void);
  void Update(float dt);
	void Shutdown(void);

  // TEMPORARY
	// Resets the map with a raw map of collision info
	void Reinitialize(std::vector<int> CollisionInfo, int w, int h, float tilesize); 


	// Clamps a point to within our searchable spaces 
	void ClampToMap(Math::Vector2DInt& index);
	static void ClampToMap(Math::Vector2DInt& index, std::vector < std::vector<Plot>> map);

  // returns plot corresponding to world position
  Plot* GetPlot(Math::Vector2D pos);
  bool WithinMap(Math::Vector2D pos);
  // sets all map plots to uncollidable
  void Reset();
  
  int ReturnHeight(){ return _height; };
  int ReturnWidth(){ return _width; };

  void SendMsg(Entity* a, Entity* b, message m);

  // data
  std::vector<int> _rawMap;
  std::vector<std::vector<Plot>> _map;

private:
  int _width;
  int _height;

  float _worldWidth;
  float _worldHeight;
  Math::Point2D _worldCenter;
};

