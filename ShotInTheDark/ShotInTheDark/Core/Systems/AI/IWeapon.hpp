#pragma once

#include "IWeaponComponent.hpp"

namespace Weapon
{

  class IWeapon
  {
  public:
    IWeapon();
    ~IWeapon();

    bool CanFire() const;
    bool IsFiring() const;

  private:
    bool fIsAttacking = false;
    std::string weaponName = "GUN WITH NO NAME";
    std::vector<IWeaponComponent*> weaponComponents;

    unsigned attackMode;
    float attackSpeed = 1.0f;       //Speed at which the player can attack. Lower value means less attacks per second
    float attackDamage = 1.0f;      //Base damage each attack causes
    float attackRange = 1.0f;       //Range at which the target has to be within in order to be hit
    unsigned attackPenetration = 0; //Does attack go through walls/enemies;

    float critMultiplier = 1.5f;

    Entity* owner;
  };

}
