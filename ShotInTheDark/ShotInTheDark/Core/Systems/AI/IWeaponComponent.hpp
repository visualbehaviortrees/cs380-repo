#pragma once
#include "Component.hpp"

namespace Weapon
{
  class IWeapon;

  class IWeaponComponent : public Component
  {
    public:
      IWeapon* owner;
      std::string nameWC = "Weapon Component has no name!!!";
  };
}
