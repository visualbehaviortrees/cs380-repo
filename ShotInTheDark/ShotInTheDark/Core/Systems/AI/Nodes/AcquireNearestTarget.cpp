#include "stdafx.h"
#include "../BehaviorTree.h"
#include "../Core/Systems/Gameplay/DungeonGameplay.h"

LEAF_UPDATE_FUNC(AcquireNearestTarget)
{
	if (currentStatus == NS_OnEnter)
	{
		time = 0;

		currentStatus = NS_Running;
	}
	else
	{
		time += dt;

		if (time > 0.25f)
		{
			const std::vector<Entity*>& enemies = g_terrain->ViewEnemies();

			float minDistance = FLT_MAX;
			Entity *target = 0;

			auto pos = self->GET_COMPONENT(Transform)->Position;

			for (Entity* e : enemies)
			{
				if (self == e || self->GC(Team)->teamID == e->GC(Team)->teamID)
				{
					continue;
				}

				auto pos2 = e->GET_COMPONENT(Transform)->Position;

				float dis2 = (pos2 - pos).SquareLength();

				if (dis2 < minDistance)
				{
					minDistance = dis2;
					target = e;
				}
			}

			if (target)
			{
				self->GET_COMPONENT(Behavior)->targetID = target->GetId();
				self->GET_COMPONENT(Behavior)->targetLocation = target->GET(Transform)->Position;
				currentStatus = NS_Completed;
			}
			else
			{
				self->GET_COMPONENT(Behavior)->targetID = -1;
				currentStatus = NS_Failed;
			}
		}
	}
}
END_LEAF_UPDATE_FUNC