#include "stdafx.h"
#include "../BehaviorTree.h"
#include "../Core/Systems/Gameplay/DungeonGameplay.h"
#include <cstdlib>

LEAF_UPDATE_FUNC(AcquireRandomTarget)
{
  if (currentStatus == NS_OnEnter)
  {
    time = 0;

    currentStatus = NS_Running;
  }
  else
  {
    time += dt;

    if (time > 0.25f)
    {
      const std::vector<Entity*>& enemies = g_terrain->ViewEnemies();
      float minDistance = FLT_MAX;
      Entity *target{ nullptr };
      int tries = 0;
      int maxtries = enemies.size() * 2;
      while (!target && (tries < maxtries))
      {
        ++tries;
        int entityid = rand() % enemies.size();
        target = g_terrain->GetEnemy(entityid);
        if (target)
        {
          self->GET_COMPONENT(Behavior)->targetID = target->GetId();
          self->GET_COMPONENT(Behavior)->targetLocation = target->GET(Transform)->Position;
          currentStatus = NS_Completed;
        }
        
      }
      if (!target)
      {
        for (unsigned i = 0; (i < enemies.size()) && currentStatus == NS_Running ; ++i)
        {
          target = g_terrain->GetEnemy(i);
          if (target)
          {
            self->GET_COMPONENT(Behavior)->targetID = target->GetId();
            self->GET_COMPONENT(Behavior)->targetLocation = target->GET(Transform)->Position;
            currentStatus = NS_Completed;
            break;
          }
        }
        if (!target)
        {
          currentStatus = NS_Failed;
        }
      }
    }
  }
}
END_LEAF_UPDATE_FUNC