#include "stdafx.h"
#include "../BehaviorTree.h"
#include "../Core/Systems/Gameplay/DungeonGameplay.h"

// most of this is totally not copied from AcquireNearestTarget...
LEAF_UPDATE_FUNC(AcquireNearestUnlitTorch)
{
	if (currentStatus == NS_OnEnter)
	{
		time = 0;
		currentStatus = NS_Running;
	}
	else
	{
		time += dt;

		if (time > 0.5f)
		{
			const std::vector<Entity*>& torches = g_terrain->ViewTorches(); // get torches

			float minDistance = FLT_MAX;
			Entity *target = 0;

			auto pos = self->GET(Transform)->Position;

			for (Entity* t : torches)
			{
				if (self == t || t->GC(ParticleEffect)->OnOrOff())
				{
					continue;
				}

				auto pos2 = t->GET(Transform)->Position;

				float dis2 = (pos2 - pos).SquareLength();

				if (dis2 < minDistance)
				{
					minDistance = dis2;
					target = t;
				}
			}

			if (target)
			{
				self->GET(Behavior)->targetID = -2;
				self->GET(Behavior)->targetLocation = target->GET(Transform)->Position;
				currentStatus = NS_Completed;
			}
			else
			{
				currentStatus = NS_Failed;
			}
		}
	}
}
END_LEAF_UPDATE_FUNC