#include "stdafx.h"
#include "../BehaviorTree.h"
#include "../Core/Systems/Gameplay/DungeonGameplay.h"
LOGIC_UPDATE_FUNC(DecoratorRunWhileTarget)
{
	if (currentStatus == NS_OnEnter)
	{
		currentStatus = NS_Running;
		m_currentChildIndex = 0;
	}
	else
	{
		if (g_terrain->GetEnemy(self->GET(Behavior)->targetID) != nullptr)
		{


			if (!g_terrain->GetEnemy(self->GET(Behavior)->targetID)->HasComponent(EC_Health)
				||  g_terrain->GetEnemy(self->GET(Behavior)->targetID)->GET(Health)->health <= 0.f)
			{
				currentStatus = NS_Completed;
			}
		}
		else
		{
			currentStatus = NS_Completed;
		}
	}
}
END_LOGIC_UPDATE_FUNC
