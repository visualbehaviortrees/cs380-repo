#include "stdafx.h"
#include "../BehaviorTree.h"
#include "../Core/Systems/Gameplay/DungeonGameplay.h"

LEAF_UPDATE_FUNC(LineOfSight)
{
	if (currentStatus == NS_OnEnter)
	{
		auto targetID = self->GET(Behavior)->targetID;

		// invalid enemy target case
		if ((targetID >= 0) && !g_terrain->GetEnemy(targetID)) 
		{ 
			currentStatus = NS_Failed; 
		}
		// Valid enemy target case
		else if ((targetID >= 0) && g_terrain->GetEnemy(targetID))
		{
			self->GET(Behavior)->targetLocation = g_terrain->GetEnemy(targetID)->GET(Transform)->Position;
			bool LoS = g_terrain->IsClearPath(self->GET(Behavior)->targetLocation, self->GET(Transform)->Position);
			
			if (LoS) 
			{ 
				currentStatus = NS_Completed; 
			}
			else
			{ 
				currentStatus = NS_Failed; 
			}
		}
		// No target case
		else if (targetID == -1) 
		{ 
			currentStatus = NS_Failed; 
		}
		// manually selected target position case
		else if (targetID == -2)
		{
			bool LoS = g_terrain->IsClearPath(self->GET(Behavior)->targetLocation, self->GET(Transform)->Position);
			if (LoS)
			{ 
				currentStatus = NS_Completed; 
			}
			else
			{ 
				currentStatus = NS_Failed; 
			}
		}
	}
		
      /*
    if (g_terrain->IsClearPath(self->GET(Behavior)->targetLocation, self->GET(Transform)->Position))
		{
			currentStatus = NS_Completed;
		}
		else 
		{
			if (targetID >= 0 || targetID == -2)
			{
				currentStatus = NS_Running;
			}
			else
			{
				currentStatus = NS_Failed;
			}
		}
	}
	else
	{
		auto targetID = self->GET(Behavior)->targetID;

		if (targetID >= 0)
		{
			auto entity = g_terrain->GetEnemy(targetID);

			if (entity)
			{
				self->GET(Behavior)->targetLocation = entity->GET(Transform)->Position;

				if (g_terrain->IsClearPath(self->GET(Behavior)->targetLocation, self->GET(Transform)->Position))
				{
					currentStatus = NS_Completed;
				}
			}
			else
			{
				currentStatus = NS_Failed;
			}
		}
		else if (targetID == -2)
		{
			if (g_terrain->IsClearPath(self->GET(Behavior)->targetLocation, self->GET(Transform)->Position))
			{
				currentStatus = NS_Completed;
			}
		}
		else
		{
			currentStatus = NS_Failed;
		}
	}
      */
}
END_LEAF_UPDATE_FUNC