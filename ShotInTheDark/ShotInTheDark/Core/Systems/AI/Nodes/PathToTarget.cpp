#include "stdafx.h"
#include "../BehaviorTree.h"
#include "../Core/Systems/Gameplay/DungeonGameplay.h"

LEAF_UPDATE_FUNC(PathToTarget)
{
	//Add ability to change path if target is moving
	if (currentStatus == NS_OnEnter)
	{
		auto targetID = self->GET(Behavior)->targetID;

		self->GET(Movement)->SetDestination(self->GET(Behavior)->targetLocation);

		currentStatus = NS_Running;
	}
	else
	{
		if (self->GET(Movement)->Size() == 0)
		{
			currentStatus = NS_Completed;
		}
	}
}
END_LEAF_UPDATE_FUNC



