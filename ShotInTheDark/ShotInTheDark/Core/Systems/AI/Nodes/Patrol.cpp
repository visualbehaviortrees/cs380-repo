#include "stdafx.h"
#include "../BehaviorTree.h"
#include "../Core/Systems/Gameplay/DungeonGameplay.h"

const int PATROL_DISTANCE = 4;

void AddPatrolPoint(Vector2DInt p, std::list<Point2D> &patrolPoints, int dx, int dy)
{
	if (g_terrain->IsWall(p.x, p.y))
	{
		if (g_terrain->IsWall(p.x + dx, p.y))
		{
			if (!g_terrain->IsWall(p.x, p.y + dy))
			{
				p.y += dy;
				patrolPoints.push_back(g_terrain->GetCoordinates(p.x, p.y));
			}
		}
		else
		{
			p.x += dx;
			patrolPoints.push_back(g_terrain->GetCoordinates(p.x, p.y));
		}
	}
	else
	{
		patrolPoints.push_back(g_terrain->GetCoordinates(p.x, p.y));
	}
}

LEAF_UPDATE_FUNC(Patrol)
{
	auto move = self->GET_COMPONENT(Movement);

	if (currentStatus == NS_OnEnter)
	{
		if (move->Size() > 0)
		{
			currentStatus = NS_Failed;
		}
		else
		{
			currentStatus = NS_Running;

			int curX, curY;

			auto pos = self->GET_COMPONENT(Transform)->Position;

			g_terrain->GetRowColumn(pos, &curX, &curY);

			Vector2DInt p1(curX - PATROL_DISTANCE, curY + PATROL_DISTANCE);
			Vector2DInt p2(curX + PATROL_DISTANCE, curY + PATROL_DISTANCE);
			Vector2DInt p3(curX + PATROL_DISTANCE, curY - PATROL_DISTANCE);
			Vector2DInt p4(curX - PATROL_DISTANCE, curY - PATROL_DISTANCE);

			AddPatrolPoint(p1, patrolPoints, 1, -1);
			AddPatrolPoint(p2, patrolPoints, -1, -1);
			AddPatrolPoint(p3, patrolPoints, -1, 1);
			AddPatrolPoint(p4, patrolPoints, 1, 1);
			patrolPoints.push_back(pos);
		}
	}
	else
	{
		if (!move->Size())
		{
			if (patrolPoints.size() > 0)
			{
				move->SetDestination(patrolPoints.front());
				patrolPoints.pop_front();
			}
			else
			{
				currentStatus = NS_Completed;
			}
		}
	}
}
END_LEAF_UPDATE_FUNC