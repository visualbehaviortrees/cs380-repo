#include "stdafx.h"
#include "../BehaviorTree.h"
#include "../Core/Systems/Gameplay/DungeonGameplay.h"

LEAF_UPDATE_FUNC(StopPath)
{
	self->GET(Movement)->Stop();
	currentStatus = NS_Completed;
}
END_LEAF_UPDATE_FUNC