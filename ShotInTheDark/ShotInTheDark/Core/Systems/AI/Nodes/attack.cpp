#include "stdafx.h"
#include "../BehaviorTree.h"
#include "../Core/Systems/Gameplay/DungeonGameplay.h"


LEAF_UPDATE_FUNC(Attack)
{
	if (currentStatus == NS_OnEnter)
	{
		time = 0.0f;
		targetID = self->GET_COMPONENT(Behavior)->targetID;

		if (targetID == -1)				// fail if no target set
		{
			currentStatus = NS_Failed;
		}
		else
		{
			currentStatus = NS_Running;
		}
	}
	else
	{
		time += dt;

		if (time > 0.25f)
		{
			if (targetID >= 0) // target is an enemy
			{
				Entity* enemy = g_terrain->GetEnemy(targetID);

				if (enemy != NULL)
				{
					self->GET_COMPONENT(Behavior)->targetLocation = enemy->GET_COMPONENT(Transform)->Position;
				}
				else
				{
					currentStatus = NS_Failed;
					self->GET_COMPONENT(Behavior)->targetID = -1;
					targetID = -1;
				}
			}

			if (targetID >= 0 || targetID == -2) // targetLocation is set to valid enemy or valid location
			{
				if (!g_terrain->IsClearPath(self->GET(Transform)->Position, self->GET(Behavior)->targetLocation))
				{
					currentStatus = NS_Failed;
				}
				else
				{
					Vector2D vel_attack = self->GET_COMPONENT(Behavior)->targetLocation
						- self->GET_COMPONENT(Transform)->Position;
					vel_attack.Normalize();
					vel_attack *= 3;
					ShootBullet(self, vel_attack);
					currentStatus = NS_Completed;
				}
			}
			else
			{
				currentStatus = NS_Failed;
			}
		}
	}

	if (currentStatus == NS_Completed || currentStatus == NS_Failed)
	{
		time = 0;
	}
}
END_LEAF_UPDATE_FUNC
