DEFINE_LEAF_NODE(Attack, "Attacks a specific target",
	MEMBER_VAR(int, targetID)
	MEMBER_VAR(float, time)
)
