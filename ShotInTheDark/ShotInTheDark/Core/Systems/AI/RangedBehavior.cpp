//	File name:		EnemyRangedAI.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Connor Drose
//	
//	All content � 2014 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "RangedBehavior.hpp"

#include "../Gameplay/DungeonGameplay.h"
// SET STATS HERE
static const float sightRange = 40.0f;




namespace Enemies
{


Ranged::Ranged()
{
  _sightRange = sightRange;
  shotTime = 0.0f;
  reloadTime = 2.0f;
  retreatRad = 10.0f;
  SetState(BS_WAIT);


}


/* This is big and ugly and we should just use inherited states 
with a queue or somthing */
void Ranged::StateMachine(float dt)
{
  assert(_curState != BS_INVALID);

  if (_curState != _prevState)
  {
    switch (_curState)
    {
      case BS_ATTACK:
			  AttackStateInit();
        break;

      case BS_CHARGE:
			  ChargeStateInit();
        break;

      case BS_GUARD:
        break;

      case BS_RETREAT:
			  RetreatStateInit();
        break;

      case BS_WAIT:
			  WaitStateInit();
        break;
    }
  }

	// EXECUTE THE STATE
	switch (_curState)
	{
	  case BS_ATTACK:
		  AttackStateUpdate(dt);
		  break;
	
    case BS_WAIT:
		  WaitStateUpdate(dt);
		  break;
  
    case BS_CHARGE:
		  ChargeStateUpdate(dt);
		  break;

    case BS_RETREAT:
		  RetreatStateUpdate(dt);
		  break;
	}

	_prevState = _curState;
}


std::vector<Point2D> IBehavior::GetPlayerInRoomPositions()
{
	DungeonGameplay* dungeon = dynamic_cast<DungeonGameplay*>(ENGINE->getCurrentGamestate());
	if (!dungeon) return std::vector<Point2D>();

	std::vector<Entity *> players = dungeon->ViewPlayers();
	std::vector<Point2D> player_positions;

	// Find our room's center
	Point2D room_center = 0.0f;
	if (dungeon)
		room_center = dungeon->GetRoomCenter(_me->GET_COMPONENT(Transform)->Position);

	for (Entity* plr : players)
	{
		if (!plr->GET_COMPONENT(Player)->IsAlive())
			continue;

		Point2D player_pos = plr->GET_COMPONENT(Transform)->Position;

		// Check if the player is in the same room as us
		if (dungeon)
			if (Hcoords::Near((dungeon->GetRoomCenter(player_pos) - room_center).SquareLength(), 0.0f))
			{
				player_positions.push_back(player_pos);
			}
	}
	return player_positions;
}

Point2D IBehavior::GetClosestPlayerPosition()
{
	std::vector<Point2D> player_pos = GetPlayerInRoomPositions();
	float best_sqdist = _sightRange * sightRange;
	Point2D best_pos = Point2D(0,0);

	for (Point2D p : player_pos)
	{
		Vector2D dist = _trans->Position - p;
		if (dist.SquareLength() < best_sqdist)
		{
			best_pos = p;
			best_sqdist = dist.SquareLength();
		}
	}

	return best_pos;
}

void Ranged::AttackStateInit()
{

}

void Ranged::AttackStateUpdate(float dt)
{
	// go to players
	std::vector<Point2D> player_pos = GetPlayerInRoomPositions();

	// Find the closest alive player in the room
	_Move.SetDestination(GetClosestPlayerPosition());

	// If we can shoot, save the direction we need to shoot 
	// and go into the charge state
	if (_Move.LineOfSightToDest() && shotTime > reloadTime)
	{
		shotDir = _me->GC(RigidBody)->direction;
		SetState(BS_CHARGE);
		return;
	}
	else
	{
		shotTime += dt;
	}

	// If we can't shoot, and we're too close to the player, get some distance
	//Vector2D dist = GetClosestPlayerPosition() - _me->GC(Transform)->Position;
	//if (dist.SquareLength() < retreatRad * retreatRad)
	//	SetState(RETREAT);

}

void Ranged::WaitStateInit()
{
	_Move.SetDestination(_trans->Position);


	if (_sleeptime <= 0.0f)
		_sleeptime = 0.0f;
}

void Ranged::WaitStateUpdate(float dt)
{
	_sleeptime -= dt;

	if (_sleeptime <= 0.0f)
	{
		if (GetPlayerInRoomPositions().size() == 0)
			_sleeptime = 1.0f;
		else
			SetState(BS_ATTACK);
	}
}

void Ranged::ChargeStateInit()
{
	_Move.SetDestination(_trans->Position);
	chargeTime = 1.0f;
}

void Ranged::ChargeStateUpdate(float dt)
{
	chargeTime -= dt;
	
	// Brighten & clamp our light
	_me->GC(Light)->r += 1.0f * dt;
	if (_me->GC(Light)->r > 1.0f)
		_me->GC(Light)->r = 1.0f;

	// Shoot our bullet and go back to chasing the player
	if (chargeTime <= 0.0f)
	{
		CreateBullet(_me, shotDir);
		_sleeptime = reloadTime;
		shotTime = 0;
		_me->GC(Light)->r = 0.0f;
		SetState(BS_WAIT);
	}
}

void Ranged::RetreatStateInit()
{
	// Find the player and go away
	Vector2D dir = _Move.GetDestination() - _me->GET_COMPONENT(Transform)->Position;
	dir.Normalize();
	Point2D away_pos = _me->GET_COMPONENT(Transform)->Position + (dir *retreatRad * 2.0f);

	_Move.SetDestination(away_pos);
}

void Ranged::RetreatStateUpdate(float dt)
{
	// If we're far enough away, go back to attacking
	Vector2D dist = GetClosestPlayerPosition() - _me->GC(Transform)->Position;
	if (dist.SquareLength() < retreatRad * retreatRad)
	{
		SetState(BS_ATTACK);
	}
}

void Ranged::Update(float dt, Entity *me)
{
	if (!me->GetActive())
		return;
  // assigns component pointer members 
  AssignComponentPointers(me);            /// REDUNDANT?????  only do once


  // Decide what state to be in
  DecideState();


  // Update state machine
  StateMachine(dt);


  // moves towards the set destination
  _Move.Update(dt, me);
}

// Literally copy pasted from playerhandler (but red)
Entity* Ranged::CreateBullet(Entity * enemy, Vector2D direction)
{
	Transform *Trans = (enemy)->GET_COMPONENT(Transform);
	
	Team *playerTeam = 0;
	if (enemy->HasComponent(EC_Team))
	{
		playerTeam = (enemy)->GET_COMPONENT(Team);
	}
	else
		return 0;

	Bullet *bullet = new Bullet();
	bullet->damage = 0.5f;
	bullet->pierces = false;
	Parent *parent = new Parent(enemy);
	Transform *trans = new Transform();
	trans->scale.Set(.4f, .4f);
	trans->rotation = Trans->rotation;
	trans->Position = Trans->Position;
	RigidBody *rigid = new RigidBody();
	rigid->isStatic = false;
	rigid->ghost = true;
	rigid->direction = direction;
	rigid->speed = 10.f;
	rigid->isStatic = false;
	Audio *audio = new Audio();
	Drawable *draw = new Drawable();
	draw->layer = 100;
	Sprite* sprite = new Sprite();
	sprite->ChangeTexture("muffin_projectile.png");

	Team *team = new Team();
	team->teamID = playerTeam->teamID;

	Light* light = new Light(200, 0.9f, 0.0f, 0.0f);


	draw->mpMesh = Mesh::Get_Mesh("BlueBullet");
	Entity * theBullet = ENGINE->Factory->Compose(9,
		light,
		parent,
		team,
		bullet,
		rigid,
		trans,
		sprite,
		audio,
		draw
		);
	theBullet->SetName(std::string("Player Bullet"));
	return theBullet;
}

}

