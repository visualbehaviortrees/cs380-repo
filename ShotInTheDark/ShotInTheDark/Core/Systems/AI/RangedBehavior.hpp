#pragma once
#include "IBehavior.hpp"

namespace Enemies
{

class Ranged : public IBehavior
{
public:
  Ranged();
  void Update(float dt, Entity *me);

private:
  void StateMachine(float dt);
  void AttackStateInit();
  void AttackStateUpdate(float dt);
  void WaitStateInit();
  void WaitStateUpdate(float dt);
  void ChargeStateInit();
  void ChargeStateUpdate(float dt);
  void RetreatStateInit();
  void RetreatStateUpdate(float dt);

  Entity* CreateBullet(Entity * enemy, Vector2D dir);

  Vector2D shotDir;
  float shotTime;
  float chargeTime;
  float reloadTime;
  float retreatRad;
};

} // namespace Enemies
