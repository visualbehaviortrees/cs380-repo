//	File name: System.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Connor Drose
//
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.


#include "stdafx.h"
#include <vector>
#include "TurretSystem.hpp"
#include "../../Engine.hpp"
#include "../../Components/Components.h"
#include "../../Libraries/Math/Math2D.hpp"
#include "Systems/Physics/Collision.hpp"


#define STOP_SHOOTING_ME 0

#define DEBUG 1


using namespace Math;

static Poly railBullet;
extern Engine *ENGINE;

static const float reloadSpeed = .75f;  // time between shots
static const float maxSightAngle = 70.f; // peripheral vision of turret (angles)
static const unsigned int bulletDamage = 4;
static const float maxSightDist = 5.f; // sight distance
static const float trackSpeed = 100.f;   // degrees per second when locked on enemy
static const float searchSpeed = 70.f;  // degrees per second when not locked on
static const float deathTime = 6.f; // seconds the turret remains dead
static int turret_num;


void CreateTurret()
{
  Transform *trans = new Transform;
  trans->Position = Vector2D(0.f, -.3f);
  trans->scale = Vector2D(1.f, 1.f);

  Drawable *drawable = new Drawable();
  *drawable = Drawable::BlueScheme;
  drawable->layer = 6;

  Team *team = new Team(Team_Player_1);

  Turret *turretAI = new Turret;

  if (team->teamID == Team_Player_2)
  {
    drawable->mpMesh = Mesh::Get_Mesh("turret");
    ENGINE->Factory->Compose(4, trans, drawable, team, turretAI)->SetName(std::string("turret").append(std::to_string(turret_num++)));
  }
  else if (team->teamID == Team_Player_1)
  {
    drawable->mpMesh = Mesh::Get_Mesh("redturret");
    ENGINE->Factory->Compose(4, trans, drawable, team, turretAI)->SetName(std::string("turret").append(std::to_string(turret_num++)));
  }
  else
  {
    drawable->mpMesh = Mesh::Get_Mesh("neutralturr");
    ENGINE->Factory->Compose(4, trans, drawable, team, turretAI)->SetName(std::string("turret").append(std::to_string(turret_num++)));
  }
}

void CreateTurret(Math::Vector2D &pos,Math::Vector2D &scale = Math::Vector2D(1.0f,1.0f), Teams TurTeam = Team_NONE)
{
  Transform *trans = new Transform;
  trans->Position = pos;
  trans->scale = scale;

  Drawable *drawable = new Drawable();
  *drawable = Drawable::BlueScheme;
  drawable->layer = 6;

  Team *team = new Team(TurTeam);

  Turret *turretAI = new Turret;

  if (team->teamID == Team_Player_1)
  {
    drawable->mpMesh = Mesh::Get_Mesh("turret");
    ENGINE->Factory->Compose(4, trans, drawable, team, turretAI)->SetName(std::string("turret").append(std::to_string(turret_num++)));
  }
  else if (team->teamID == Team_Player_1)
  {
    drawable->mpMesh = Mesh::Get_Mesh("redturret");
    ENGINE->Factory->Compose(4, trans, drawable, team, turretAI)->SetName(std::string("turret").append(std::to_string(turret_num++)));
  }
  else
  {
    drawable->mpMesh = Mesh::Get_Mesh("neutralturr");
    ENGINE->Factory->Compose(4, trans, drawable, team, turretAI)->SetName(std::string("turret").append(std::to_string(turret_num++)));
  }
}


void TurretSystem::Init()
{
  REGISTER_COMPONENT(Turret);
  REGISTER_COMPONENT(Team);
  REGISTER_COMPONENT(Turret);
  REGISTER_COMPONENT(Drawable);

  CreateTurret(Math::Vector2D(0, 0), Math::Vector2D(1.f,1.f));
  CreateTurret(Math::Vector2D(-10.f, 5.f), Math::Vector2D(1.f,1.f));
  CreateTurret(Math::Vector2D(10.f,-5.f), Math::Vector2D(1.f,1.f));
  gridMap = static_cast<GridMap*>(ENGINE->GetSystem(sys_GridMap));
  CreateTurret(Math::Vector2D(-10.f, -5.f), Math::Vector2D(1.f, 1.f), Team_Player_1);
  CreateTurret(Math::Vector2D(10.f, 5.f), Math::Vector2D(1.f, 1.f), Team_Player_1);

  railBullet = Poly(5);
  railBullet.SetVert(0) = Math::Point2D(-1.0f,0.f);
  railBullet.SetVert(1) = Math::Point2D(0.f,-0.3f);
  railBullet.SetVert(2) = Math::Point2D(1.0f,0.f);
  railBullet.SetVert(3) = Math::Point2D(0.0f,0.3f);
  railBullet.SetVert(4) = railBullet.GetVert(0);
}


// spawns bullet in given direction
void TurretSystem::Shoot()
{
  // create bullet
  Transform *trans = new Transform();
  trans->rotation = trans->rotation + 90.f;
  trans->scale.Set(.5f, .5f);
  trans->Position = trans->Position;
  Bullet *bullet = new Bullet();
  bullet->damage = bulletDamage;
  bullet->lifetime = 1.5f;
  Parent *parent = new Parent(owner);
  RigidBody *rigid = new RigidBody();
  rigid->direction = Math::Vector2D(0,1) % trans->rotation;
  rigid->speed = 5.f;
  rigid->isStatic = false;
  Audio *audio = new Audio();
  //audio->PlaySound(SFX::FIRE_SPREAD);
  Drawable *draw = new Drawable();
  draw->r = 1.f;
  draw->g = 1.f;
  draw->layer = 2;
  draw->mpMesh = Mesh::Get_Mesh("turretbullet");
  Team *team = new Team();
  team->teamID = team->teamID;

  ENGINE->Factory->Compose(7, parent, team, bullet, rigid, trans, audio, draw);

  // change color
  Drawable *turDraw = owner->GET_COMPONENT(Drawable);
  turDraw->beginR = 0.0f;
  turDraw->beginG = 0.0f;
  turDraw->beginB = 0.0f;

}


float DegreeBetweenTurretAndTarget(float turRot, Vector2D turPos, Vector2D targetPos)
{
  float targetAngle;
  Vector2D vecToTar = targetPos - turPos;
  Math::VectorToDegree(vecToTar.x, vecToTar.y, targetAngle);
  float degToTar = DegreeBetween(turRot + 90.f, targetAngle);

  return degToTar;
}


// turn smoothly towards enemy
void TurretSystem::TrackEnemy(float dt)
{
  _drawable->r = 1.f;
  _drawable->b = 0.f;

  _turret->trackingTime += dt;

  // find degrees to target
  float degToTar = DegreeBetweenTurretAndTarget(trans->rotation, trans->Position, _targetTrans->Position);

  float turnDir = 1.f;
  if(degToTar < 0.f)
    turnDir = -1.f;

  //float turnRate = (float)(sqrt(timeSpentTurning) * turnDir * 80.f * dt);
  trans->rotation += turnDir * trackSpeed * dt;

  // WHEN TO STOP TRACKING
  if(Math::abs(degToTar) > maxSightAngle)
    _turret->tracking = false;
  float dist = _targetTrans->Position.Distance(trans->Position);
  if(dist > maxSightDist)
    _turret->tracking = false;
}


// determine if we should track any enemies
void TurretSystem::FindEnemy(float dt)
{
  _drawable->r = 0.f;
  _drawable->b = 1.f;

  // searching rotation
  if(_turret->tracking == false)
    trans->rotation += searchSpeed * dt;

  // find targets
  std::vector<Entity*> players = ENGINE->OM->FilterEntities(MC_Player);
  for(auto player : players)
  {
    // too far
    Transform *plrTrans = player->GET_COMPONENT(Transform);
    float dist = plrTrans->Position.Distance(trans->Position);
    if(dist > maxSightDist)
      continue;
    // same team
    if(team->teamID == player->GET_COMPONENT(Team)->teamID)
      continue;
    // not in field of vision
    float degToTar = DegreeBetweenTurretAndTarget(trans->rotation, trans->Position, plrTrans->Position);
    if(Math::abs(degToTar) > maxSightAngle)
      continue;

    _turret->tracking = true;
    _targetTrans = plrTrans;
  }
}


bool TurretSystem::DetectDeath(float dt)
{
  _drawable->g = 0.f;

  if(_turret->timeDead > 0.f)  // if dead and when to come back
  {
    _turret->timeDead += dt;
    if(_turret->timeDead > deathTime){
      _turret->timeDead = 0.f;
      return false;
    }
    _drawable->g = 1.f;
    return true;
  }

  std::vector<Entity*> bullets = ENGINE->OM->FilterEntities(MC_Bullet);
  for(auto it : bullets)   // if not dead, detect bullets
  {
    if(it->GET_COMPONENT(Parent)->parent == owner)
      continue;
    if(CollisionCheck(it, owner))
      continue;//_turret->timeDead += dt;   // TEMPORARY   disabling death
  }
  return false;
}


void TurretSystem::Update(float dt)
{
  for(auto it : _entities)
  {
    // Get turret components
    owner = it;
    trans = it->GET_COMPONENT(Transform);
    team = it->GET_COMPONENT(Team);
    _turret = it->GET_COMPONENT(Turret);
    _drawable = it->GET_COMPONENT(Drawable);

    if(DetectDeath(dt))
      continue;

    if(_turret->tracking == false){
      _turret->trackingTime = 0.f;
      FindEnemy(dt);
    }
    else
    {
      _turret->trackingTime += dt;
      TrackEnemy(dt);
      #if !STOP_SHOOTING_ME
      _turret->cooldownTime += dt;
      if(_turret->cooldownTime > reloadSpeed){
        Shoot();
        _turret->cooldownTime = 0;
      }
      #endif
    }
  }

}

