//	File name:		TurretSystem.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Connor Drose
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../../System.hpp"
#include "../../Systems/AI/GridMap.hpp"
#include "../../Components/Components.h"
#include "../../Entity.hpp"

class TurretSystem : public System
{
public:
	TurretSystem(void) : System(sys_TurretAI, "Turret AI System"){}
	void Init(void);
  void Update(float dt);
	void Shutdown(void){}

  void Shoot();
  void FindEnemy(float dt);
  void TrackEnemy(float dt);
  bool DetectDeath(float dt);

  GridMap *gridMap;
  Entity *owner; // turret being updated
  Transform *trans;
  //Turret *_turret;
  Team *team;
  Drawable *_drawable;

  Transform *_targetTrans;

};


