#pragma once
#include "IWeaponComponent.hpp"

namespace Weapon
{
  class IWCAmmo : public IWeaponComponent
  {
    virtual bool CanFire() const = 0;
  };

#define INFINITE_AMMO -1
  class WCBulletAmmo : IWCAmmo
  {
  public:
    virtual bool CanFire() const
    {
      if (rounds == INFINITE_AMMO) //INFINITE AMMO
        return true;
      else
        return rounds > 0;
    }
  private:
    int rounds;
    int clips;
  };

  class WCBatteryAmmo : public IWCAmmo
  {
  public:
    virtual bool CanFire() const
    {
      if (chargeRemaining == INFINITE_AMMO)
        return true;
      else
        return chargePerShot <= chargeRemaining;
    }

  private:
    int chargePerShot;
    int chargeRemaining;
  };

  class WCOverheatAmmo : public IWCAmmo
  {
  public:
    virtual bool CanFire() const
    {
      if (maxCharge == INFINITE_AMMO)
        return true;
      else
        return currentCharge + chargePerShot <= maxCharge;
    }

  private:
    int chargePerShot;
    int currentCharge;
    int maxCharge;
  };

  class IWCCharge : public IWCAmmo
  {
  public:
    virtual bool CanFire() const
    {
      if (minChargeTime == INFINITE_AMMO)
        return true;
      else
        return currentChargeTime >= minChargeTime;
    }

    int currentChargeTime = 0;

  protected:
    int minChargeTime = 1;
  };

  class WCDamageCharge : public IWCCharge
  {
  public:
    virtual bool CanFire() const
    {
      if (minChargeTime == INFINITE_AMMO)
        return true;
      else
        return currentChargeTime > minChargeTime;
    }

    float GetChargeMultiplier() const //wherever this is called needs to check that currentCharge >= minCharge
    {
      float s = clamp(currentChargeTime / maxChargeTime, 0.0f, 1.0f);
      return baseMultiplier * (1.0f - s) + maxMultiplier * s;
    }

  private:
    float maxChargeTime = 3.0f;
    float baseMultiplier = 1.0f;
    float maxMultiplier = 2.0f;
  };
}