#pragma once
#include "IWeaponComponent.hpp"
namespace Weapon
{

class Projectile : public IWeaponComponent
{
public:
  Projectile()
  {
    nameWC = "Projectile has no name.";
  }

  virtual void OnCollision(){ /*apply bullet damage*/__debugbreak(); }

  int damage = 1.0f;
};

class RichochetProjectile : public Projectile
{
  virtual void OnCollision() 
  {
    if (bounces == -1)
      return;

    if (bounces > 0)
      --bounces;
    //else
      //destroy bullet
  }

private:
  int bounces;
};

class PenetratingProjectile : public Projectile
{
  public:
    virtual void OnCollision() 
    {
      if (penetration == -1)
        return;

      if (penetration > 0)
        --penetration;
      else
        //destroy bullet
    };

  private:
    int penetration;
};

class Explosive : public Projectile
{
public:
  virtual void OnCollision(){}
  float areaOfEffect = 1.0f;
};

class TimedExplosive : public Explosive
{
  public:
    virtual void Update(float dt)
    {
      detonationTimer -= dt;
      if (detonationTimer > 0)
        return;
      else
        Explosive::OnCollision();
    }

  private:
    float detonationTimer = 1.0f;
};

}