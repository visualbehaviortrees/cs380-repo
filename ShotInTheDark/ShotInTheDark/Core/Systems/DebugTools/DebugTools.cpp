//	File name:		DebugTools.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include <vector>
#include "DebugTools.hpp"
#include "../../Engine.hpp"
#include "../../Components/Components.h"
#include "../Graphics/Graphics.hpp"

extern Engine *ENGINE;


void DebugTools::Init()
{
  drawnFPS = 0;
}

void DebugTools::Update(float dt)
{
  dt = ENGINE->GetTime();
  if (CheckKeyTriggered(KEY_D, MOD_CTRL))
  {
    useDebugDraw = !useDebugDraw;
    if (WireFrames)
    {
      WireFrames = !WireFrames;
    }
  }

  if (useDebugDraw && CheckKeyTriggered(KEY_W, MOD_CTRL))
  {
    WireFrames = !WireFrames;
  }

  if (useDebugDraw && CheckKeyTriggered(KEY_C, MOD_CTRL))
  {
    clipDebug = !clipDebug;
  }

  if (CheckKeyTriggered(KEY_F1))
  {
    ++drawFPS;
    if (drawFPS == 3)
      drawFPS = 0;
  }

  switch(drawFPS)
  {
  case(0) :
    break;
  case(1):
    DrawFPS(false, dt);
    break;
  case(2) :
    DrawFPS(true, dt);
    break;
  default:
    break;
  }

  if (useDebugDraw)
  {
    DebugDraw();
  }
}


void DebugTools::DebugDraw()
{
  Window* CurrentWindow = GETSYS(Graphics)->GetCurrentWindow();
  Point2D point;

  glUseProgram(0);

  // Projection Matrix
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  // Orthographic Projection - Camera Box
  glOrtho
    (
    -CurrentWindow->main_Cam->width / 2,
    +CurrentWindow->main_Cam->width / 2,
    -CurrentWindow->main_Cam->height / 2,
    CurrentWindow->main_Cam->height / 2,
    -1.0f,
    1.0f
    );

  // Camera View Matrix
  glMatrixMode(GL_MODELVIEW);

  Matrix4 Matrix = Math::Inverse(*(GETSYS(Graphics)->GetCurrentWindow()->Current_CameraToWorld));

  glLoadMatrixf(&Matrix.matrix4[0][0]);

  glColor4fv(&debugDrawColor.r);

  // Loop Through All Entities And Draw Bounding Box Of All Which Have Mesh
  for (auto& go : _entities)
  {
    if (clipDebug && go->GC(Drawable)->clipped)
      continue;
    Transform* transform = go->GET_COMPONENT(Transform);
    Drawable* draw = go->GET_COMPONENT(Drawable);
    Mesh* mesh = draw->mpMesh;

    if (go->HasComponent(EC_RigidBody) && mesh != nullptr)
    {
      RigidBody * rb = go->GC(RigidBody);
      // Push Camera Matrix To Stack And Apply Model Transformations
      glPushMatrix();
      glTranslatef(transform->Position.x, transform->Position.y, 0.0f);
      glScalef(transform->scale.x, transform->scale.y, 1.0f);
      glBegin(GL_LINE_LOOP);
      {
        glColor3f(0.f, 1.f, 0.f);
        for (unsigned i = 0; i < mesh->Poly.GetVertNum(); ++i)
        {
          point = mesh->Poly.GetVert(i);
          glVertex2f(point.x, point.y);
        }
        glPopAttrib();
      }
      glEnd();

      glBegin(GL_LINES);
      {
        glColor3f(1.f, 0.f, 0.f);
        glVertex2f(0.f, 0.f);
        glVertex2f(rb->direction.x * rb->speed * .25f,
                   rb->direction.y * rb->speed * .25f);
        glPopAttrib();
      }
      glEnd();

      // Pop The Modified Matrix
      glPopMatrix();
      if (go->HasComponent(EC_Player) && WireFrames)
      {
        // draw lines from the players
        glColor4fv(&debugConnectColor.r);
        std::vector<Entity *> bullets = ENGINE->OM->FilterEntities(MC_Bullet | MC_Transform);
        std::vector<Entity *> pups = ENGINE->OM->FilterEntities(MC_PowerupSpawner | MC_Transform);
        glPushMatrix();
        glTranslatef(transform->Position.x, transform->Position.y, 0.0f);
        for (auto it : bullets)
        {
          // set the color to red if it's an enemy
          if (it->HasComponent(EC_Behavior))
            glColor4fv(&enemyConnectorColor.r);
          glBegin(GL_LINE_LOOP);
          //glVertex2f(draw->mpMesh->Poly.CENTER().X(), draw->mpMesh->Poly.CENTER().Y());
          glVertex2f(0.f, 0.f);
          glVertex2f(it->GC(Transform)->Position.x - transform->Position.x,
            it->GC(Transform)->Position.y - transform->Position.y);
          glEnd();
          if (it->HasComponent(EC_Behavior))
            glColor4fv(&debugConnectColor.r);

        }

        // draw lines to the powerups
        glColor4fv(&powerupColors.r);
        for (auto it : pups)
        {
          glBegin(GL_LINE_LOOP);
          //glVertex2f(draw->mpMesh->Poly.CENTER().X(), draw->mpMesh->Poly.CENTER().Y());
          glVertex2f(0.f, 0.f);

          glVertex2f(it->GC(Transform)->Position.x - transform->Position.x,
            it->GC(Transform)->Position.y - transform->Position.y);
          glEnd();

        }
        glPopMatrix();

        //glColor4fv(&debugDrawColor.r);
      }
    }
  }

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}
void DebugTools::WireFrameDraw()
{

}
void DebugTools::TraceDraw()
{

}

void DebugTools::SetFPSColor(float dt)
{
  if (dt > 1.f / 30.f)
  {
    _red = 1.f;
    _green = 0.f;
  }
  else if (dt < 0.0166f)
  {
    _red = 0.f;
    _green = 1.f;
  }
  else
  {
    _red = dt / 0.032f - 0.0166f / dt + 0.5f;
    _green = 0.0166f / dt - dt / 0.032f + 0.5f;


    if (_red < 0.f)
      _red = 0.f;
    else if (_red > 1.f)
      _red = 1.f;
    if (_green > 1.f)
      _green = 1.f;
    else if (_green < 0.f)
      _green = 0.f;
  }
}

void DebugTools::DrawFPS(bool fIncremental, float dt)
{
  timeAccumulator += dt;
  int currentFPS = static_cast<int>(ceil(1.0f/dt));

  if (currentFPS != drawnFPS)
  {
    if (timeAccumulator > 0.25f || timeAccumulator < 0.f)
    {
      timeAccumulator = 0.f;

      SetFPSColor(dt);
    }

    if (fIncremental)
    {
      if (currentFPS > drawnFPS)
        drawnFPS += 1;
      else if (currentFPS < drawnFPS)
        drawnFPS -= 1;
    }
    else
      drawnFPS = currentFPS;
  }

  glUseProgram(0);
  const unsigned fpsBufferSize = 20;
  char fpsstr[fpsBufferSize];
  sprintf_s(fpsstr, fpsBufferSize, "FPS: %03i", drawnFPS);

  // Create a pixmap font from a TrueType file.
  FTGLPixmapFont Font("Resources/Anonymous.ttf");

  Graphics* GS = GETSYS(Graphics);
  Font.FaceSize(static_cast<unsigned int>(24.0f * (static_cast<float>(GS->GetCurrentWindow()->WindowHeight) / static_cast<float>(GS->GetCurrentWindow()->Desktop->height))));
  // Set the font size and render a small text.
  glPushAttrib(GL_ALL_ATTRIB_BITS);
  // Current frame chunk FPS draw
  glColor3f(_red, _green, 0.f);
  glRasterPos3f(-0.99f, .95f, 1.f);
  //glColor3f(_red, _green, 0.f);
  Font.Render(fpsstr);
  glPopAttrib();
}

bool DebugTools::Get_WireFrames()
{
  return WireFrames;
}
