//	File name:		TestSystem.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../../System.hpp"

class DebugTools: public System
{
public:
  DebugTools(void):System(sys_DebugTools, "Debug Tools System"){}
	
  void Init(void);
  
  void Update(float dt);

  void Shutdown(void){}

  bool Get_WireFrames();

private:
  // Debug functions
  void DebugDraw();
  void WireFrameDraw();
  void TraceDraw();
  void SetFPSColor(float dt);
  void DrawFPS(bool fIncrement, float dt);
  int drawFPS = 0;
  bool useDebugDraw = false;
  bool clipDebug = false;
  bool WireFrames = false;

  // debug values

  float timeAccumulator = 0.f,  _red, _green;

  int drawnFPS = 0;

  glm::vec4 debugDrawColor = { 0.0f, 1.0f, 0.0f, 1.f };
  glm::vec4 debugConnectColor = { 1.0f, 0.0f, 0.0f, 1.f };
  glm::vec4 enemyConnectorColor = { 0.0f, 0.0f, 1.0f, 1.f };
  glm::vec4 powerupColors = { 1.0f, 1.0f, 0.0f, 1.f };
};
