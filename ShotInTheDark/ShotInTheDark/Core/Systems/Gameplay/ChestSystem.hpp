//	File name:		TestSystem.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../../System.hpp"

class ChestSystem : public System
{
public:
  ChestSystem(void) :System(sys_ChestSystem, "Chest Controller System") {}

  void Init(void);

  void Update(float dt);

  void Shutdown(void) {}

};
