//	File name:		DMGameplay.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "../Input/InputSystem.hpp"
#include "DMGameplay.hpp"
#include "../../Engine.hpp"
#include "../../Components/Components.h"
#include "../Input/InputSystem.hpp"
#include "../Physics/Collision.hpp"
#include "../Graphics/Graphics.hpp"
#include "../Graphics/ResourceManager.h"
//#include "../Graphics/Text.hpp"

#define AUTO_PLAY 1

extern Engine* ENGINE;
static Poly railBullet;
static const unsigned int PointsPerKill = 2;
static float current_time;
DMGameplay::DMGameplay() : Gamestate(sys_DMGameplay, sys_MenuGS, "Deathmatch Gameplay System")
{


}

DMGameplay::~DMGameplay()
{


}

void DMGameplay::Init(void)
{
  Transform* digi_tran = new Transform();
  digi_tran->Position.Set(0.0f, 0.0f);
  digi_tran->rotation = 0.0f;
  digi_tran->scale.Set(20.0f, 10.0f);

  Drawable *digi_drawable = new Drawable();
  digi_drawable->a = 1.0f;
  *digi_drawable = Drawable::BlueScheme;
  digi_drawable->layer = 100;
//  ParticleEffect* PE = new CircleEffect(2000,-1.0f);

  Sprite *digipen_logo_sprite = new Sprite("DigiPen_Splash.png", true);
  Light* digilight = new Light(5.0f);

  Entity *Splash = ENGINE->Factory->Compose(4, digi_drawable, digipen_logo_sprite, digi_tran, digilight);
  Splash->SetName(std::string("DigiPen_Splash"));


  Transform* credits_tran = new Transform();
  credits_tran->Position.Set(0.0f, 0.0f);
  credits_tran->rotation = 0.0f;
  credits_tran->scale.Set(18.0f, 10.0f);

  Drawable *credit_drawable = new Drawable();
  credit_drawable->a = 1.0f;
  *credit_drawable = Drawable::BlueScheme;
  credit_drawable->layer = 190;

  Sprite *credit_logo_sprite = new Sprite("credits.png", true);

  Entity *Credits_screen = ENGINE->Factory->Compose(3, credit_drawable, credit_logo_sprite, credits_tran);
  Credits_screen->SetName(std::string("Credits"));

  Transform* playing_tran = new Transform();
  playing_tran->Position.Set(0.0f, 0.0f);
  playing_tran->rotation = 0.0f;
  playing_tran->scale.Set(18.0f, 10.0f);

  Drawable *playing_drawable = new Drawable();
  playing_drawable->a = 1.0f;
  *playing_drawable = Drawable::BlueScheme;
  playing_drawable->layer = 180;

  Sprite *how_to_play_sprite = new Sprite("HowToPlayScreen.png", true);

  Entity *how_to_play = ENGINE->Factory->Compose(3, playing_drawable, how_to_play_sprite, playing_tran);
  how_to_play->SetName(std::string("HowToPlayScreen"));


  static bool bases = false;
  static bool players = false;
#pragma region  Bases
  if(!bases)
  {
    // Initialize the bases
    // Start with red base teamID = 0
    Team *redTeam = new Team();
	redTeam->teamID = Team_Player_1;
    Base *redBase = new Base();
      // Bases need all the relevant draw components
    Drawable *redBaseDraw = new Drawable();
    *redBaseDraw = Drawable::RedScheme;
    redBaseDraw->layer = 1;
    redBaseDraw->mpMesh = Mesh::Get_Mesh("RedBase");
        // A base needs a position
    Transform *redTransform = new Transform();
    redTransform->scale.Set(1.f,1.f);
    redTransform->Position.Set(-(16.f/9.f)*9.0f,-(9.f/16.f)*15.0f);
      // It doesn't move, but it can collide. Bases are heavy.
    RigidBody *redRigid = new RigidBody();
    redRigid->isStatic = true;
    redRigid->density = 100.0f;
    redRigid->rotationLocked = true;
    redRigid->vel.Set(0,0);


    Entity *theRedBase = ENGINE->Factory->Compose(5,redTeam,redBase,redBaseDraw,redTransform,redRigid);

    theRedBase->SetName(std::string("RedBase"));
      // Then do the blue team base teamID = 1
    Team * blueTeam = new Team();
    blueTeam->teamID = Team_Player_2;
    Base *blueBase = new Base();
      // Bases need all the relevant draw components
    Drawable *blueBaseDraw = new Drawable();
    *blueBaseDraw = Drawable::BlueScheme;
    blueBaseDraw->layer = 1;
    blueBaseDraw->mpMesh = Mesh::Get_Mesh("BlueBase");

        // A base needs a position
    Transform *blueTransform = new Transform();
    blueTransform->scale.Set(1.f,1.f);
    blueTransform->Position.Set((16.f/9.f)*9.0f,(9.f/16.f)*12.0f);
      // It doesn't move, but it can collide. Bases are heavy.
    RigidBody *blueRigid = new RigidBody();
    blueRigid->isStatic = true;
    blueRigid->density = 100.0f;
    blueRigid->rotationLocked = true;
    blueRigid->vel.Set(0,0);



      // Finally we must build the base into an Entity:

    Entity * theBase = ENGINE->Factory->Compose(5 ,blueTeam,blueBase,blueBaseDraw,blueTransform,blueRigid);
    theBase->SetName(std::string("BlueBase"));
    bases = true;
  }
#pragma endregion

#pragma region Players
  if(!players)
  {
    Player* player = new Player();
    //player->controls.ControllerNum = JOYSTICK_1;
    Transform* ObjTrans = new Transform();
    ObjTrans->Position.Set(-(16.f/9.f)*9.0f,-(9.f/16.f)*15.0f);
    ObjTrans->rotation = 0.0f;
    ObjTrans->scale.Set(0.75f,0.75f);
    RigidBody* Rigid = new RigidBody();
    Rigid->direction.Set(0.0f,0.0f);
    Rigid->isStatic = false;
    Rigid->speed = 7.5f;
    Drawable *drawable = new Drawable();
    *drawable = Drawable::RedScheme;
    drawable->layer = 6;
    drawable->mpMesh = Mesh::Get_Mesh("RedPlayer");
    Team *team = new Team();
	team->teamID = Team_Player_1;
    Health *redHP = new Health(10);
    Light *pLight = new Light();
 //   Mesh::Init_Mesh(Mesh_RedPlayer);

    ParticleEffect* pEffect = new CircleEffect(ObjTrans);

    if(AUTO_PLAY)
    {
      Behavior *playerredAI = new Behavior(AI::ET_PLAYER);

      Entity *redPlayer = ENGINE->Factory->Compose(9,ObjTrans,Rigid,player,drawable, playerredAI,team,redHP, pLight, pEffect);
      redPlayer->SetName(std::string("Red Player"));
    }
    else
    {
      Entity *redPlayer = ENGINE->Factory->Compose(8,ObjTrans,Rigid,player,drawable,team,redHP, pLight, pEffect);
      redPlayer->SetName(std::string("Red Player"));
    }

    Player* player2 = new Player();
    //player2->up = KEY_I;
    //player2->down = KEY_K;
    //player2->left = KEY_J;
    //player2->right = KEY_L;
    //player2->shoot = KEY_SEMICOLON;
    //player2->turnLeft = KEY_U;
    //player2->turnRight = KEY_O;
    //player2->shootSecondary = KEY_RIGHT_SHIFT;
    //player2->shootMG = KEY_RIGHT_CONTROL;
    //player2->ControllerNum = JOYSTICK_2;
    Transform* ObjTrans2 = new Transform();
    ObjTrans2->Position.Set((16.f/9.f)*9.0f,(9.f/16.f)*15.0f);
    ObjTrans2->rotation = 0.0f;
    ObjTrans2->scale.Set(0.75f,0.75f);
    RigidBody* Rigid2 = new RigidBody();
    Rigid2->direction.Set(0.0f,0.0f);
    Rigid2->isStatic = false;
    Rigid2->speed = 7.5f;
    Drawable *drawable2 = new Drawable();
    *drawable2 = Drawable::BlueScheme;
    drawable2->layer = 6;
    drawable2->mpMesh = Mesh::Get_Mesh("BluePlayer");
    Team *team2 = new Team();
    team2->teamID = Team_Player_2;
    Health *blueHP = new Health(10);
    Light *pLight2 = new Light();


    // TEMPORARY - add AI to blue
    Behavior *playerAI = new Behavior();
    Entity * bluePlayer = ENGINE->Factory->Compose(8,ObjTrans2,Rigid2,player2,drawable2,team2, blueHP, playerAI, pLight2);
    bluePlayer->SetName(std::string("Blue Player"));
  }
#pragma endregion
}

bool splash = true;
bool credits = true;
bool how_to_play_screen = true;

void DMGameplay::Update(float dt)
{
  //WriteText("asdf", 4, Math::Point2D(0,0));
#pragma region SplashScreen
  if(splash && ENGINE->GetTotalTime() >= 5.0f)
  {
    splash = false;
    ENGINE->OM->DestroyAll(ENGINE->OM->GetEntityByName(std::string("DigiPen_Splash")));
  }
  if(credits && ENGINE->GetTotalTime() >= 10.0f)
  {
    credits = false;
    ENGINE->OM->DestroyAll(ENGINE->OM->GetEntityByName(std::string("Credits")));
  }
  if(how_to_play_screen && ENGINE->GetTotalTime() >= 15.0f)
  {
    how_to_play_screen = false;
    ENGINE->OM->DestroyAll(ENGINE->OM->GetEntityByName(std::string("HowToPlayScreen")));
  }

#pragma endregion
  

#pragma region BulletHandling
  std::vector<Entity *> bullets = ENGINE->OM->FilterEntities(MC_Bullet | MC_RigidBody);
  for(auto bullet : bullets)
  {
    (bullet)->GET_COMPONENT(Bullet)->lifetime -= dt;
    if((bullet)->GET_COMPONENT(Bullet)->lifetime <= 0.0f)
    {
      ENGINE->OM->DestroyEntity(bullet);
    }
    else if(bullet->GET(RigidBody)->collisionList.size() == 0)
    {
      continue;
    }
    else
    {
      for(auto &collisions : bullet->GET_COMPONENT(RigidBody)->collisionList)
      {
        if(collisions.object->HasComponent(static_cast<mask>(MC_Player | MC_Health | MC_Team)))
        {
          if(collisions.object->GET_COMPONENT(Team)->teamID != bullet->GET_COMPONENT(Team)->teamID)
          {
            // logic for bullets hitting players
            continue;
          }
        }
        if(collisions.object->GET(RigidBody)->isStatic
       && !collisions.object->GET(RigidBody)->ghost
                  && !bullet->GET(RigidBody)->ghost)
        {
          if(collisions.object->HasComponent(EC_Team) && (collisions.object->GET_COMPONENT(Team)->teamID != bullet->GET_COMPONENT(Team)->teamID) )
          {
            CollisionResolved(bullet, collisions.object);
            ENGINE->OM->DestroyEntity(bullet);
            break;
          }
          else
          {
            CollisionResolved(bullet, collisions.object);
            ENGINE->OM->DestroyEntity(bullet);
            break;
          }
        }
       
      }
      /*
      std::vector<Entity *> staticObjects = ENGINE->OM->FilterEntities(MC_RigidBody | MC_Transform);
      for(auto objs : staticObjects)
      {
        if(objs->HasComponent(EC_RigidBody))
          if(!it->GET_COMPONENT(RigidBody)->ghost && objs->GET_COMPONENT(RigidBody)->isStatic && !objs->GET_COMPONENT(RigidBody)->ghost)
          {
            if(objs->HasComponent(EC_Team))
            {
              if(objs->GET_COMPONENT(Team)->teamID != it->GET_COMPONENT(Team)->teamID)
              {
                if( CollisionFound(it, objs))
                {
                  CollisionResolved(it, objs);
                  ENGINE->OM->DestroyEntity(it);
                  break;
                }
              }
            }
            else
            {
              if(CollisionFound(it, objs))
              {
                CollisionResolved(it, objs);
                ENGINE->OM->DestroyEntity(it);
                break;
              }
            }
          }
      }*/
    }
  }
#pragma endregion

#pragma region PlayerHandling
  std::vector<Entity *> players = ENGINE->OM->FilterEntities(MC_Player | MC_Team | MC_Transform | MC_Drawable | MC_Health);
  //for(auto it2 : players)
  //{
  //  if(it2->GET_COMPONENT(Player)->alive)
  //  {
  //    if(it2->GET_COMPONENT(RigidBody)->collisionList.size())
  //    {
  //      for(auto &it: it2->GET_COMPONENT(RigidBody)->collisionList)
  //      {
  //        if(it.object->HasComponent(EC_Bullet))
  //        {
  //          if((it.object->GET_COMPONENT(Team)->teamID != (it2)->GET_COMPONENT(Team)->teamID) && CollisionFound(it.object,it2) )
  //          {
  //            CollisionResolved(it.object, it2);
  //            it2->GET_COMPONENT(Drawable)->visible = false;
  //            it2->GET_COMPONENT(Player)->counter = 0.1f;
  //            it2->GET_COMPONENT(Health)->health -= it.object->GET_COMPONENT(Bullet)->damage;
  //            if(it2->GET_COMPONENT(Health)->health <= 0)
  //            {
  //              it2->GET_COMPONENT(Player)->alive = false;
  //              it2->GET_COMPONENT(Player)->counter = 2.0f;
  //              Entity *parent = it.object->GET_COMPONENT(Parent)->parent;
  //              if(parent->HasComponent(EC_Player))
  //              {
  //                Player *player = parent->GET_COMPONENT(Player);
  //                player->score += PointsPerKill;
  //                std::cout << player->score << std::endl;
  //              }
  //              it.object->GET_COMPONENT(Bullet)->lifetime = 0.0f;
  //              //break;
  //            }
  //            it.object->GET_COMPONENT(Bullet)->lifetime = 0.0f;
  //            //continue;
  //          }
  //        }
  //      }
  //    }
  //  } // player is alive
  //}
#pragma endregion

}

void DMGameplay::Shutdown(void)
{


}


