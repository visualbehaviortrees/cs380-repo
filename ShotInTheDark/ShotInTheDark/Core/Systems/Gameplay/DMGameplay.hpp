//	File name:		DMGameplay.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../../System.hpp"

class DMGameplay : public Gamestate
{
public:
  DMGameplay();
  ~DMGameplay();
  void Init(void);
  void Update(float dt);
  void Shutdown(void);
  void PoppedTo(systype) {};


private:

};