//	File name:		TestSystem.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../../System.hpp"

class DungeonBuilder : public System
{
public:
  DungeonBuilder(void) :System(sys_DungeonBuilder, "Dungeon Builder System") {}

  void Init(void);

  void Update(float dt);

  void Shutdown(void) {}

};
