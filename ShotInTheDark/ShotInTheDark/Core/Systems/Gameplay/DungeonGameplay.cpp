//	File name:		DungeonGameplay.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.


#include "stdafx.h"

#include "../../Engine.hpp"
#include "DungeonGameplay.h"

#include "Gamestates/MenuGamestate.h"

#include "../../Components/Components.h"
#include "PowerupHandler.hpp"
#include "../../Components/Player.hpp"

#include "../Physics/PhysicsSystem.hpp"
#include "../Physics/Resolution.hpp"
#include "../Graphics/Graphics.hpp"
#include "../AI/GridMap.hpp"
#include "../PlayerHandler/PlayerHandler.hpp"
#include "../HUD/HUD.hpp"
#include "Systems/LevelEditor/Level_Creator.hpp"
#include "DungeonGenerator.h"
#include "DungeonLayoutGenerator.h"
#include "EnemyFactory.h"
#include "../../Components/Minimap.h"
#include "../Core/Libraries/AntTweakBar/AntTweakBar.hpp"

extern Engine* ENGINE;

bool GameComplete = false;

static int currentLevel = 0;

Vector2D HUD_Offset_(0.0f, 1.25f);

Vector2D MiniMap_Offset(0.0f, 500.0f);

DungeonGameplay::DungeonGameplay(void) : Gamestate(sys_DungeonGameplay, sys_MenuGS, "Dungeon Gamestate"), dungeon_generator(), dungeon_scale(2.0f), target_camera_pos(0, 0), layout_(0)
{

}

DungeonGameplay::~DungeonGameplay(void)
{
  if(layout_ != 0)
	  delete layout_;
}

int DungeonGameplay::GetCurrentLevel()
{
	return currentLevel;
}

// Should be moved to a centralized Cheat system.... 
static void Cheat_DisableDarkness(void)
{
	static bool active = false;
	active = !active;
	Graphics* g = GETSYS(Graphics);//static_cast<Graphics* >(ENGINE->GetSystem(sys_Graphics));
	if (g)
		g->SetLighting(active);
}


void DungeonGameplay::Cheat_KillPlayers(void)
{
	if (CheckKeyTriggered(KEY_1, MOD_ALT) && players.size() >= 1)
		players.at(0)->GET_COMPONENT(Health)->health = 0.0f;
	if (CheckKeyTriggered(KEY_2, MOD_ALT) && players.size() >= 2)
		players.at(1)->GET_COMPONENT(Health)->health = 0.0f;
	if (CheckKeyTriggered(KEY_3, MOD_ALT) && players.size() >= 3)
		players.at(2)->GET_COMPONENT(Health)->health = 0.0f;
	if (CheckKeyTriggered(KEY_4, MOD_ALT) && players.size() >= 4)
		players.at(3)->GET_COMPONENT(Health)->health = 0.0f;

	// UNKILL PLAYERS
	if (CheckKeyTriggered(KEY_1, MOD_CTRL) && players.size() >= 1)
	{
		players.at(0)->GET_COMPONENT(Health)->health = 999.0f;
	}
	if (CheckKeyTriggered(KEY_2, MOD_CTRL) && players.size() >= 2)
	{
		players.at(1)->GET_COMPONENT(Health)->health = 999.0f;
	}
	if (CheckKeyTriggered(KEY_3, MOD_CTRL) && players.size() >= 3)
	{
		players.at(2)->GET_COMPONENT(Health)->health = 999.0f;
	}
	if (CheckKeyTriggered(KEY_4, MOD_CTRL) && players.size() >= 4)
	{
		players.at(3)->GET_COMPONENT(Health)->health = 999.0f;
	}
}

// DUNGEON NAVIGATION FUNCTIONS ===============================================
// Note -- dungeon creation & functions like this should be moved to an independent module so that people don't have to rely on this state...

Point2D DungeonGameplay::DungeonToWorld(int x, int y)
{
	Vector2D offset(x * dungeon_scale, y * dungeon_scale);

	return dungeon_origin + offset;
}

Vector2DInt DungeonGameplay::WorldToDungeon(Point2D pos)
{
	// Shift over to be relative to the bottom-left corner of the dungeon
	pos -= dungeon_origin;
	// Reduce our coordinates by the scale of the dungeon
	pos *= (1 / dungeon_scale);

	// Round into an int and return
	return Vector2DInt((int)(pos.x + 0.5f), (int)(pos.y + 0.5f));
}


Vector2DInt DungeonGameplay::WorldToDungeonMap(Point2D pos)
{
	pos.x -= -(GetWidth() - 1);
	pos.y -= (GetHeight() - 1);
	// Reduce our coordinates by the scale of the dungeon
	pos *= (1 / dungeon_scale);

	return Vector2DInt(static_cast<int>(pos.x), static_cast<int>(pos.y));
}

void DungeonGameplay::WorldToDungeonMap(Point2D pos, int* x, int *y)
{
	pos.x -= -(GetWidth() - 1);
	pos.y -= (GetHeight() - 1);
	// Reduce our coordinates by the scale of the dungeon
	pos *= (1 / dungeon_scale);

	*x = static_cast<int>(pos.x + 0.5f);
	*y = static_cast<int>(pos.y + 0.5f);
}

Point2D DungeonGameplay::DungeonMapToWorld(Vector2DInt index)
{
	return DungeonMapToWorld(index.x, index.y);
}

Point2D DungeonGameplay::DungeonMapToWorld(int x, int y)
{
	Point2D pos;
	pos.x = (float)x;
	pos.y = (float)y;

	pos *= dungeon_scale;

	pos.x += -(GetWidth() - 1);
	pos.y += (GetHeight() - 1);

	return pos;
}

Vector2DInt DungeonGameplay::WorldToLayout(Point2D pos)
{
  // Shift over to be relative to the bottom-left corner of the dungeon
  pos.x -= dungeon_origin.x;
  pos.y = dungeon_origin.y + (dungeon_dimensions.y + room_dimensions.y - 1) * 2.0f - pos.y;
  // Reduce our coordinates by the scale of the dungeon
  pos *= (1 / dungeon_scale);

  pos.x /= (room_dimensions.x - 1);
  pos.y /= (room_dimensions.y - 1);

  // Round into an int and return
  return Vector2DInt(static_cast<int>(pos.x), static_cast<int>(pos.y));
}

Point2D DungeonGameplay::LayoutToWorld(int x, int y)
{
  Point2D pos(static_cast<float>(x) + 0.5f, static_cast<float>(y) + 0.5f);

  pos.x *= (room_dimensions.x - 1);
  pos.y *= (room_dimensions.y - 1);

  pos *= dungeon_scale;

  pos.x += dungeon_origin.x;
  pos.y = dungeon_origin.y + (dungeon_dimensions.y + room_dimensions.y - 1) * 2.0f - pos.y;

  return pos;
}

Point2D DungeonGameplay::GetRoomCenter(Point2D pos)
{
	// translate the worldspace position into dungeon indices,
	// rounding by 1/2 to account for walls being shared between rooms
	Vector2DInt dungeon_index = WorldToDungeon(pos - (dungeon_scale * Vector2D(0.5f, 0.5f)));


	// To find the current room, we divide by the room size
	Vector2DInt current_room_index;
	current_room_index.x = dungeon_index.x / (room_dimensions.x - 1);
	current_room_index.y = dungeon_index.y / (room_dimensions.y - 1);

	// Offset into the middle of the room
	dungeon_index.x = current_room_index.x * (room_dimensions.x - 1) + ((room_dimensions.x - 1) / 2);
	dungeon_index.y = current_room_index.y * (room_dimensions.y - 1) + ((room_dimensions.y - 1) / 2);

	return DungeonToWorld(dungeon_index.x, dungeon_index.y);
}

Vector2D DungeonGameplay::GetRoomDimensions()
{
	float width = 2.0f * (room_dimensions.x - 1);
	float height = 2.0f * (room_dimensions.y - 1);

	return Vector2D(width, height);
}

void DungeonGameplay::SetDungeonColor(float r, float g, float b, float brightness)
{
	current_dungeon_color.x = r;
	current_dungeon_color.y = g;
	current_dungeon_color.z = b;

	// Update the ambient lighting
	// We want the lighting to be very unsaturated, so we will bring it closer to 1.0f
	const float desaturate = 5.0f;
	GETSYS(Graphics)->SetAmbientLighting(brightness, brightness, brightness,
		(r + desaturate) / (desaturate + 1.0f),
		(g + desaturate) / (desaturate + 1.0f),
		(b + desaturate) / (desaturate + 1.0f));
	// Set the background color
  // BACKGROUND COLOR NO LONGER NEEDED
	//*if (brightness > 0.001f)
		//GETSYS(Graphics)->SetBackgroundColor(r, g, b);
	//else
		GETSYS(Graphics)->SetBackgroundColor(0.0f, 0.0f, 0.0f);
}

Entity* DungeonGameplay::CreateBullet(Entity* player, Vector2D vel)
{
	const float base_speed = 7.5f;

	Transform *Trans = (player)->GET_COMPONENT(Transform);
	Team *playerTeam = (player)->GET_COMPONENT(Team);

	Bullet *bullet = new Bullet();
	bullet->damage = 1.0f;
	bullet->pierces = false;
	bullet->particle_info.count = 20;
	Parent *parent = new Parent(player);
	Transform *trans = new Transform();
	trans->scale.Set(.25f, .25f);
	trans->rotation = Trans->rotation;
	trans->Position = Trans->Position;
	RigidBody *rigid = new RigidBody();
	rigid->isStatic = false;
	rigid->ghost = true;
	rigid->direction = vel;
	rigid->speed = vel.Length() + base_speed; // previously 7.5f;
	rigid->isStatic = false;
	Audio *audio = new Audio();
	Drawable *draw = new Drawable();
	draw->layer = 100;
	Sprite* sprite = new Sprite();
	sprite->ChangeTexture("muffin_projectile.png");

	Team *team = new Team();
	team->teamID = playerTeam->teamID;

	Light* light = new Light(100.0f);

	//	ParticleEffect* effect = new CircleEffect(50, -1);


	draw->mpMesh = Mesh::Get_Mesh("BlueBullet");
	Entity * theBullet = ENGINE->Factory->Compose(9,
		light,
		parent,
		team,
		bullet,
		rigid,
		trans,
		sprite,
		audio,
		draw
		);
	theBullet->SetName(std::string("Bullet"));
	return theBullet;
}

// INIT FUNCTIONS =============================================================

void DungeonGameplay::PlaceWall(Point2D pos)
{
	Drawable *drawable = new Drawable();
	*drawable = Drawable::YellowScheme;
	drawable->layer = 3;
	drawable->mpMesh = Mesh::Get_Mesh("box");

	// set position
	Transform* transform = new Transform();
	transform->Position = pos;

	transform->rotation = 0.0f;
	transform->scale.Set(1.0f, 1.0f);

	RigidBody *rigid = new RigidBody;
	rigid->isStatic = true;
	rigid->speed = 0.00001f;

	Sprite *wallSprite = new Sprite();

	std::vector<std::string> wall_list;

	switch (currentLevel)
	{
	case 1:
		wall_list.push_back("wall_yellow.png");
		break;
	case 2:
		wall_list.push_back("wall_red.png");
		break;
	case 3:
	default:
		wall_list.push_back("wall_purple.png");
		break;
	}

	// Pick a random wall
	wallSprite->ChangeTexture(wall_list.at(ADLib::randInt(wall_list.size())));


	static unsigned DungeonWallCount = 0;

	Entity * wall = ENGINE->Factory->Compose(4,
		drawable,
		transform,
		rigid,
		wallSprite
		);
	wall->SetName(std::string("DungeonWall").append(std::to_string(DungeonWallCount++)));

	ENGINE->SendMsg(wall, 0, MSG_Add_Tile);
}

void DungeonGameplay::PlaceTorch(Point2D pos)
{
	Transform* torchTran = new Transform();
	torchTran->Position = pos;
	torchTran->rotation = 0.0f;
	torchTran->scale.Set(1.0f, 1.0f);
	RigidBody* torchBody = new RigidBody();
	torchBody->direction.Set(0.0f, 0.0f);
	torchBody->isStatic = true;
	torchBody->ghost = true;
	Drawable* torchDraw = new Drawable();
	*torchDraw = Drawable::RedScheme;
	torchDraw->layer = 10;
	torchDraw->mpMesh = Mesh::Get_Mesh("RedBase");
	Health* torchHealth = new Health();
	torchHealth->maxHealth = 1.0f;
	torchHealth->health = 1.0f;
	Light *torchLight = new Light(100.0f, current_dungeon_color);
	Team *torchTeam = new Team();
	torchTeam->teamID = Team_Environment;
	Sprite* torchSprite = new Sprite("firepit_unlit.png");
	Audio* torchAudio = new Audio();
	torchAudio->audioMap[Audio::AE_DIE] = "torchIgnite";

  Transform* temptrans = new Transform();
  Point2D temppos = Point2D();
  temptrans->Position = temppos;

  glm::vec4 start_min(1.0f, 0.0f, 0.0f, 1.0f);
  glm::vec4 start_max(1.0f, 1.0f, 0.0f, 1.0f);
  glm::vec4 end_min(0.5f, 0.5f, 0.0f, 0.0f);
  glm::vec4 end_max(0.75f, 0.5f, 0.0f, 0.2f);



  ParticleEffect *pEffect = new FireEffect(torchTran, 80, -1.0, start_min, start_max, end_min, end_max, 1);
 // ParticleEffect* pEffect = new GoToPosEffect(torchTran, temptrans);
  pEffect->SetBool(false);

	Entity *torch = ENGINE->Factory->Compose(9,
		torchTran,
		torchBody,
		torchDraw,
		torchHealth,
		torchLight,
		torchTeam,
		torchSprite,
		pEffect,
		torchAudio
		);
	torch->SetName(std::string("Torch"));
	torches.push_back(torch);
}

void DungeonGameplay::PlaceDestructibleWall(Point2D pos)
{
	Drawable *drawable = new Drawable();
	*drawable = Drawable::YellowScheme;
	drawable->layer = 3;
	drawable->mpMesh = Mesh::Get_Mesh("box");

	// set position
	Transform* transform = new Transform();
	transform->Position = pos;

	transform->rotation = 0.0f;
	transform->scale.Set(1.0f, 1.0f);

	RigidBody *rigid = new RigidBody;
	rigid->isStatic = true;
	rigid->speed = 0.00001f;

	Sprite *wallSprite = new Sprite();
	switch (currentLevel)
	{
	case 3:
		wallSprite->ChangeTexture("wall_breakable_purple_1.png");
		break;

	case 2:
	default:
		wallSprite->ChangeTexture("wall_breakable_1.png");
		break;
	}

	Health *wallHealth = new Health(3.0f);

	Team *wallTeam = new Team(Team_Environment);

	Audio* wallAudio = new Audio();
	wallAudio->audioMap[Audio::AE_HURT] = "stoneHit";
	wallAudio->audioMap[Audio::AE_DIE] = "stoneShatter";


	static unsigned DestructibleWallCount = 0;

	Entity * wall = ENGINE->Factory->Compose(7,
		drawable,
		transform,
		rigid,
		wallSprite,
		wallHealth,
		wallTeam,
		wallAudio
		);
	wall->SetName(std::string("DestructibleWall").append(std::to_string(DestructibleWallCount++)));

	destructible_walls.push_back(wall);
	ENGINE->SendMsg(wall, 0, MSG_Add_Tile);

}

void DungeonGameplay::BuildDungeon(DungeonMap& dungeon)
{
  Vector2DInt Map_Index;
	//Math::Vector2D dScale(2.0f, 2.0f);
	//// Calculate our bottom-left corner
	//Math::Point2D dStart(dScale.X() * -.5f * dungeon.GetWidth(),
	//				     dScale.Y() * -.5f * dungeon.GetHeight());

	// Tell the Grid map/AI system what just happened so it can build the map
	GridMap* grid = static_cast<GridMap*>(ENGINE->GetSystem(sys_GridMap));
	grid->Reinitialize(
		dungeon.GetRawCollisionMap(),
		dungeon.GetWidth(), dungeon.GetHeight(),
		dungeon_scale);

	// Place some torches at random
	for (unsigned i = 0; i < dungeon.GetWidth(); ++i)
		for (unsigned j = 0; j < dungeon.GetHeight(); ++j)
		{
			Point2D pos = DungeonToWorld(i, j);
			switch (dungeon.GetTile(i, j))
			{
				// Disabled for testing
			case TILE_TORCH:
				PlaceTorch(pos);
				break;

			case TILE_ENEMY:
				PlaceEnemy(pos);
				break;

			case TILE_WALL:
				PlaceWall(pos);
				break;

			case TILE_TREASURE:
				GETSYS(PowerupHandler)->CreateRandomEntity(pos, PowerupHandler::POWERUP_POWERUP);
				break;

			case TILE_PICKUP:
				GETSYS(PowerupHandler)->CreateRandomEntity(pos, PowerupHandler::POWERUP_PICKUP);
				break;


			case TILE_BREAKABLE_WALL:
				PlaceDestructibleWall(pos);
				break;

			case TILE_DOOR:
				PlaceDoor(pos);
				break;

			default:
				break;
			}
		}

	//Put the camera in the center
  Graphics* GS GETSYS(Graphics);
  GS->SetCameraPosition(DungeonToWorld(dungeon_generator.GetStartingRoomCenter().x, dungeon_generator.GetStartingRoomCenter().y), HUD_Offset_);
  target_camera_pos = GS->GetCameraPosition();

  Map_Index = WorldToLayout(target_camera_pos);
  layout_->SetRoomVisited(Map_Index.x, Map_Index.y);

  //GS->SetMinimapCameraPosition(target_camera_pos, MiniMap_Offset);

	// Tell everyone what we did
	ENGINE->SendMsg(0, 0, MSG_Built_Dungeon);
}

Math::Hcoords DungeonGameplay::GetRoomColor(LayoutTile RoomType)
{
  switch (RoomType)
  {
  case LayoutTile::LAYOUT_ROOM:

  case LayoutTile::LAYOUT_START_ROOM:
    return Hcoords(1.0f, 1.0f, 1.0f, 1.0f);
    break;

  case LayoutTile::LAYOUT_TREASURE_ROOM:
    return Hcoords(1.0f, 0.82f, 0.0f, 1.0f);
    break;

  case LayoutTile::LAYOUT_BOSS_ROOM:
    return Hcoords(1.0f, 0.0f, 0.0f, 1.0f);
    break;

  default:
    return Hcoords(0.0f, 0.0f, 0.0f, 0.0f);
    break;
  }
}

// Tiles the background 
void DungeonGameplay::PlaceBackground(void)
{
	const Vector2D bg_scale(14.0f, 8.0f);
	//Point2D bg_pos;
  unsigned int width = layout_->GetWidth(), height = layout_->GetHeight();
	// Set the background based on the level
	std::string bg;
  std::string mm;

	switch (currentLevel)
	{
	case 1:
		bg = "floortile_yellow.png";
		break;
	case 2:
		bg = "floortile_red.png";
		break;
	case 3:
	default:
		bg = "floortile_purple.png";
		break;
	}

  unsigned int x, y;

  for (x = 0; x < width; ++x)
	{
    for (y = 0; y < height; ++y)
    {
      if (layout_->GetRoom(x, y) != LAYOUT_NO_ROOM)
      {
        //Place Background
        Transform* bgTran = new Transform();
        bgTran->scale = bg_scale;
        bgTran->Position = LayoutToWorld(x, y);
        Drawable* bgDraw = new Drawable();
        bgDraw->layer = 1;
        bgDraw->clipped = false;
        bgDraw->visible = true;
        Sprite* bgSprite = new Sprite(bg);
        bgSprite->ChangeTexture(bg);

        Entity *bgEntity = ENGINE->Factory->Compose(3, bgTran, bgDraw, bgSprite);
        bgEntity->SetName(std::string("LevelBackground"));

        //Place MiniMap Image
        mm = "minimap_icon.png";
        Transform* mmTran = new Transform();
        mmTran->scale = bg_scale;
        mmTran->Position = LayoutToWorld(x, y) + MiniMap_Offset;
        Drawable* mmDraw = new Drawable();
        mmDraw->layer = 18;
        mmDraw->clipped = false;
        mmDraw->visible = false;
        Math::Hcoords color_ = GetRoomColor(layout_->GetRoom(x, y));
        mmDraw->r = color_.x;
        mmDraw->g = color_.y;
        mmDraw->b = color_.z;
        mmDraw->a = color_.w;
        Sprite* mmSprite = new Sprite(mm);
        mmSprite->ChangeTexture(mm);
        Minimap* mmMap = new Minimap();

        Entity *mmEntity = ENGINE->Factory->Compose(4, mmTran, mmDraw, mmSprite, mmMap);
        mmEntity->SetName(std::string("MiniMap"));
      }
    }
	}
}

void DungeonGameplay::PlaceEnemy(Point2D pos)
{
	Entity* enemy;
	
	enemy = GETSYS(EnemyFactory)->CreateRandomEnemy(pos);
}

void DungeonGameplay::PlaceDoor(Point2D pos)
{


	Drawable *drawable = new Drawable();
	*drawable = Drawable::YellowScheme;
	drawable->layer = 3;
	drawable->mpMesh = Mesh::Get_Mesh("box");

	// set position
	Transform* transform = new Transform();
	transform->Position = pos;

	transform->rotation = 0.0f;
	transform->scale.Set(1.0f, 1.0f);

	RigidBody *rigid = new RigidBody;
	rigid->speed = 0.00001f;

	Sprite *wallSprite = new Sprite();
	wallSprite->ChangeTexture("wall_topdown.png");

	static unsigned DungeonWallCount = 0;

	Entity * wall = ENGINE->Factory->Compose(4,
		drawable,
		transform,
		rigid,
		wallSprite
		);
	wall->SetName(std::string("Door").append(std::to_string(DungeonWallCount++)));

	Door d;
	d.ent = wall;
	d.exitDir = Vector2D(0.0f, 0.0f);
	doors.push_back(d);

	ENGINE->SendMsg(wall, 0, MSG_Add_Tile);

}

void DungeonGameplay::LevelUp()
{
  test_bug = true;

	// Delete everything that isn't a "safe" entity -- player, backround, hud stuff
	std::vector<std::string> safeEnts;
	safeEnts.push_back("HUD_Healthbar_bg");
	safeEnts.push_back("HUD_Healthbar_fg");
	safeEnts.push_back("HUD_PlayerIcon_bg");
	safeEnts.push_back("HUD_PlayerIcon_fg");
	safeEnts.push_back("HUD_Icon");
	safeEnts.push_back("Player");
	safeEnts.push_back("Enemy Counter");
	safeEnts.push_back("NavArrow");
	std::vector<Entity* > ents = ENGINE->OM->GetEntitiesWithoutName(safeEnts);
	for (Entity* e : ents)
		ENGINE->OM->DestroyEntity(e);
	// Torch, destructible wall pointers are no good now
	torches.clear();
	destructible_walls.clear();
	doors.clear();

	currentLevel++;

	ENGINE->SendMsg(0, 0, MSG_Clear_Dungeon);

	if (currentLevel > END_LEVEL)
	{
		GETSYS(AudioSystem)->NarrateSound("endVictory");
		ENGINE->popGamestate();
		GameComplete = true;
		return;
	}

	DM_Modifiers mods;

  // No darkness, enable ambient light
	mods.darken_chance = 1.0f;
  mods.darken_weight = 1.0f;
  mods.pickup_chance = 0.75f / (float)currentLevel;
  mods.pickup_max = 1;
  mods.treasure_size = 1 + currentLevel;
	
  room_dimensions = Vector2DInt(15, 9);

  switch (currentLevel)
	{

		// LEVEL 1 CONFIGURATION ==========================================
	case 1:
		// Yellow
		SetDungeonColor(1.0f, 0.914f, 0.239f, 0.012f);
		// Change the light of all players
		Player::base_light = 0.75f;

		mods.safe_chance = 0.66f;
		mods.safe_weight = .33f;

		num_rooms.x = 4;
		num_rooms.y = 5;

		dungeon_generator.AddRoomList("RoomList_Dungeon1.txt");

		GETSYS(AudioSystem)->SetTargetLevelParam(0.0f);

		break;

		// LEVEL 2 CONFIGURATION ==========================================
	case 2:
		// No darkness, enable ambient light
		// Red
		SetDungeonColor(1.0f, 0.278f, 0.094f, 0.01f);
		// Change the light of all players
		Player::base_light = 1.0f;

		mods.safe_chance = 0.33f;
		mods.safe_weight = 0.33f;

		room_dimensions.x = 15;
		room_dimensions.y = 9;
		num_rooms.x = 5;
		num_rooms.y = 4;


		dungeon_generator.AddRoomList("RoomList_Dungeon2.txt");
		GETSYS(AudioSystem)->SetTargetLevelParam(0.1f);
		break;

		// LEVEL 3 CONFIGURATION ==========================================
	default:
	case 3:
		//darkness, disable ambient light
		mods.darken_chance = 0.25f;
		mods.darken_weight = 0.8f;
		// Purple
		SetDungeonColor(0.686f, 0.145f, 1.0f, 0.0f);
		// Change the light of all players
		Player::base_light = 5.0f;

		num_rooms.x = 1;
		num_rooms.y = 1;


		dungeon_generator.AddRoomList("RoomList_Dungeon3.txt");
		GETSYS(AudioSystem)->SetTargetLevelParam(0.2f);
		break;

		//(The old way)
		//mods.darken_chance = .1f * currentLevel;
		//mods.darken_weight = 0.8f + 0.1f * currentLevel;
		//mods.safe_chance = .01f - 0.0005f * currentLevel;
		//mods.safe_weight = 0.5f;
		//mods.danger_chance = 0.02f + (0.01f * currentLevel);
		//mods.danger_weight = 0.1f + (0.01f * currentLevel);
		//mods.treasure_chance = .5f - (0.1f * currentLevel);
		//mods.treasure_max = players.size();
	}



  for (Entity* p : players)
    p->GET(Light)->intensity = Player::base_light;

  
  // Recalculate our dimensions and whatnot
	dungeon_dimensions = num_rooms * (room_dimensions - Vector2DInt(1, 1));
	dungeon_generator.SetRoomSize(room_dimensions.x, room_dimensions.y);
	dungeon_origin = Point2D(dungeon_scale * dungeon_dimensions.x * -.5f,
                           dungeon_scale * dungeon_dimensions.y * -.5f);

  if (layout_ != nullptr)
  {
    delete layout_;
  }

  layout_ = new DungeonLayout(layout_generator.MazeAlgorithm(num_rooms.x, num_rooms.y, true, 2, 0));

	// Generate and build the dungeon
    DungeonMap dungeon = dungeon_generator.GenerateDungeonFromLayout(d, *layout_, mods);
	BuildDungeon(dungeon);

	// Take our players and put them in the center
	for (Entity* p : players)
	{
    p->GET_COMPONENT(Transform)->Position = DungeonToWorld(dungeon_generator.GetStartingRoomCenter().x,
			dungeon_generator.GetStartingRoomCenter().y);
		p->GC(Health)->health = 9999.0f;
	}

	PlaceBackground();
}

void DungeonGameplay::Init(void)
{
	//// REQUIRED SYSTEMS
	REGISTER_PRESYS(Test);
	REGISTER_PRESYS(PowerupHandler);
	REGISTER_PRESYS(Input);
	REGISTER_PRESYS(PlayerHandler);
	REGISTER_PRESYS(PowerupHandler);
  REGISTER_PRESYS(GridMap); //CBA: Pathfinding enable/disable
  REGISTER_POSTSYS(ScriptSystem);
	REGISTER_POSTSYS(Physics);
	REGISTER_POSTSYS(CollisionResolution);
	REGISTER_POSTSYS(EnemyFactory);
	REGISTER_POSTSYS(HUDsystem);
	REGISTER_POSTSYS(AudioSystem);
	REGISTER_POSTSYS(EnemyAISystem);

	currentLevel = STARTING_LEVEL - 1;

  layout_ = nullptr;

	ENGINE->OM->AddRegistry(new OMRegistryEntry(&bullets, MC_Bullet, "Dungeon Bullet list"));

	GETSYS(Graphics)->SetLighting(true);
	LevelUp();
	// Build the HUD
	static_cast<HUDsystem*>(ENGINE->GetSystem(sys_HUDsystem))->BuildHudItems();

	// Start the audio only if we're not in debug or the skip is off
#ifdef _DEBUG
	if (!DEBUG_NO_MUSIC)
#endif
	{
		//static_cast<AudioSystem*>(ENGINE->GetSystem(sys_AudioSystem))->MusicOn(true);
		//static_cast<AudioSystem*>(ENGINE->GetSystem(sys_AudioSystem))->PlayMusic(Music::THEME_1);
	}

	// Place the player(s)
	CheckAndAddPlayers();


	// Play the nifty first audio queue
	GETSYS(AudioSystem)->NarrateSound("beginGame");

	// Start the music
	GETSYS(AudioSystem)->PlayMusic("Level");

}

// UPDATE FUNCTIONS ===========================================================

// Should probably be moved to playerhandler...
void DungeonGameplay::UpdateBullets(float dt)
{	
  if (bullets.size() == 0)
    return;

	for (auto bullet : bullets)
	{
		// Update the Bullet's lifetime
		(bullet)->GET_COMPONENT(Bullet)->lifetime -= dt;
		if ((bullet)->GET_COMPONENT(Bullet)->lifetime <= 0.0f)
		{
			// Make a particle effect
			//Bullet::CreateExplosionEffect(bullet);
			ENGINE->OM->DestroyEntity(bullet);
			continue;
		}

		// Skip bullets that aren't colliding with anything
		if (!bullet->HasComponent(EC_RigidBody))
			continue;

		RigidBody* bullet_rbody = bullet->GET_COMPONENT(RigidBody);
		bool Bullet_hit = false;
		for (auto &collisions : bullet_rbody->collisionList)
		{

			if (!Team::FriendlyTeams(bullet, collisions.object))
			{
				// Special case for players
				if (collisions.object->HasComponent(EC_Player))
				{
					if (!collisions.object->GET_COMPONENT(Player)->IsAlive())
						continue;

					if (collisions.object->GET_COMPONENT(Health)->safeTime > collisions.object->GET_COMPONENT(Health)->timeSinceLastHit)
						continue;
					
					// Tell the HUD we got hit if it was a player
					static_cast<HUDsystem*>(ENGINE->GetSystem(sys_HUDsystem))->OnPlayerHit(collisions.object);
					// Screen shake
					GETSYS(Graphics)->SetCameraPosition(
						GETSYS(Graphics)->GetCameraPosition()
            + Vector2D(ADLib::randFloat(-3.0f, 3.0f), ADLib::randFloat(-3.0f, 3.0f)), HUD_Offset_);

				}
				// logic for bullets hitting things
				// List the parent as the source if it has one
				bullet->GET_COMPONENT(Bullet)->Hit(collisions.object, dt, bullet);
				ENGINE->SendMsg(bullet, collisions.object, MessageDetails::MSG_Damage);

				// Play a sound
				if (collisions.object->HasComponent(EC_Audio))
				{
					if (collisions.object->HasComponent(EC_Health) && collisions.object->GET(Health)->GetHealthPercent() <= 0)
						collisions.object->GET(Audio)->Play(Audio::AE_DIE);
					else
						collisions.object->GET(Audio)->Play(Audio::AE_HURT);
				}


				Bullet_hit = true;
			}
		}

		if (!bullet->GET_COMPONENT(Bullet)->pierces && Bullet_hit)
		{
			bullet->GET_COMPONENT(Bullet)->lifetime = 0.0f;
			GETSYS(AudioSystem)->PlaySound("magicHit");

		}
	}
}

void DungeonGameplay::UpdateTorches(float dt)
{
	// Remember - lower intensity is brighter
	const float Max_Torch_Intensity = 2.0f;
	const float Min_Torch_Intensity = 150.0f;


	for (Entity* torch : torches)
	{
		Light* torchLight = torch->GET_COMPONENT(Light);
		Health* torchHealth = torch->GET_COMPONENT(Health);
		Drawable* torchDraw = torch->GET_COMPONENT(Drawable);
		RigidBody* torchBody = torch->GET_COMPONENT(RigidBody);
		Team* torchTeam = torch->GET_COMPONENT(Team);
		Sprite* torchSprite = torch->GET_COMPONENT(Sprite);
		ParticleEffect* torchParticles = torch->GET(ParticleEffect);
		Audio* torchAudio = torch->GET(Audio);

		const float color_slerp_speed = 5.0f;
		float flicker = ADLib::randFloat(-1.0f, 1.0f);
		// Torches are brighter the more hurt they are
		float brightnessScalar = 1.0f - (float)pow(torchHealth->GetHealth(), 2.0f);

		float target_torch_intensity = ((Max_Torch_Intensity - Min_Torch_Intensity) * brightnessScalar) + Min_Torch_Intensity + flicker;

		torchLight->intensity += (target_torch_intensity - torchLight->intensity) * (color_slerp_speed * dt);

    int hp = torchHealth->GetHealthPercent();
    if (torchDraw->mpMesh != Mesh::Get_Mesh("RedBase") &&  hp < 50)
		{
			torchSprite->ChangeTexture("firepit_lit.png");
			torchParticles->SetBool(true);
			torchDraw->mpMesh = Mesh::Get_Mesh("RedBase");
		}
    else if (torchDraw->mpMesh != Mesh::Get_Mesh("BlueBase") && hp >= 50)
		{
			torchSprite->ChangeTexture("firepit_unlit.png");
			torchParticles->SetBool(false);
			torchAudio->Stop();
			torchDraw->mpMesh = Mesh::Get_Mesh("BlueBase");
		}

		torchHealth->Update(dt);
	}
    
    

	// This needs to be somewhere else. 
  bool clear = RoomIsClear();
  // Hide powerups if all the lights aren't on in this room
  std::vector<Entity*> pup_list = ENGINE->OM->FilterEntities(MC_PowerupSpawner | MC_Transform);
  for (Entity* p : pup_list)
  {
	  p->GC(Drawable)->visible = clear;
  }
  
}

void DungeonGameplay::UpdateMovement(float dt)
{
	if (CheckMouseTriggered(MOUSE_RIGHT))
	{
		players[0]->GET_COMPONENT(Movement)->SetDestination(GetMousePosition());
	}

	for (Entity *e : enemies)
	{
		if (e->Mask() & MC_Movement)
		{
			auto movement = e->GET_COMPONENT(Movement);

			movement->Update(dt);
		}
	}
}

void DungeonGameplay::UpdateBehaviors(float dt)
{
	for (Entity *e : enemies)
	{
		if (e->Mask() & MC_Behavior)
		{
			auto behavior = e->GET_COMPONENT(Behavior);

			behavior->Update(e, dt);

		}
	}
}

bool DungeonGameplay::CameraMoving()
{
	return (target_camera_pos - GETSYS(Graphics)->GetCameraPosition()).SquareLength() > 0.01;
}


bool DungeonGameplay::RoomIsClear(void)
{
	return room_clear;
}

void DungeonGameplay::UpdateRoomClear(void)
{
	Point2D playerRoom = GetRoomCenter(GETSYS(Graphics)->GetCameraPosition());

	if (Team::pvp_enabled)
	{
		room_clear = false;
		return;
	}

	for (Entity* e : enemies)
	{
		Point2D enemyroom = GetRoomCenter(e->GC(Transform)->Position);
		if (Hcoords::Near((enemyroom - playerRoom).SquareLength(), 0.0f))
		{
			room_clear = false;
			return;
		}
	}

	for (Entity* torch : torches)
	{
		Point2D torchroom = GetRoomCenter(torch->GC(Transform)->Position);
		if (Hcoords::Near((torchroom - playerRoom).SquareLength(), 0.0f))
		{
			Health* torchHealth = torch->GET_COMPONENT(Health);
			if (torchHealth->GetHealthPercent() > 50)
			{
				room_clear = false;
				return;
			}
		}
	}

	room_clear = true;
}

// Should be moved to a camera controller module ... 
void DungeonGameplay::UpdateCamera(float dt)
{
	// This is physics stuff
	// Statics->PREVIOUSROOM() = Statics->GetCurrentRoom();
  Graphics* GS = GETSYS(Graphics);
	const float camera_lerp_percent = 5.0f;

  Point2D current_camera_pos = GS->GetCameraPosition();

	// Find any players outside the room
	for (Entity * p : players)
	{
		// Skip dead players
		if (!p->GET_COMPONENT(Player)->IsAlive() || p->GET_COMPONENT(Player)->InDoor())
			continue;
	}


	// Update our current camera position as a percentage of the target so we move smoothly
	Point2D next_camera_pos = current_camera_pos + (target_camera_pos - current_camera_pos) * camera_lerp_percent * dt;
	// If the points are in different rooms, send a message
	if (!Hcoords::Near((GetRoomCenter(next_camera_pos) - GetRoomCenter(current_camera_pos)).SquareLength(), 0.0f))
		ENGINE->SendMsg(0, 0, MSG_Room_Change);

  GS->SetCameraPosition(next_camera_pos, HUD_Offset_);
  //GS->SetMinimapCameraPosition(next_camera_pos, MiniMap_Offset);
}

void DungeonGameplay::UpdateDoors(float dt)
{
	static bool clear_last_frame = false;

	if (RoomIsClear() != clear_last_frame)
	{
		// Update the sprites
		for (Door& d : doors)
			d.ent->GET(Sprite)->ChangeTexture(RoomIsClear() ? "wall_topdown.png" : "wall_breakable_1.png");
		// Play an unlocking sound
		if (RoomIsClear())
		{
			GETSYS(AudioSystem)->PlaySound("chest/open");
		}
	}
	
	if (!CameraMoving() && RoomIsClear())
		CheckDoorEntry(dt);
	if (!CameraMoving())
		CheckDoorExit(dt);
	
	// Lock player positions
	for (Door& d : doors)
		for (auto it = d.players_in_door.begin(); it != d.players_in_door.end(); ++it)
			(*it)->GET(Transform)->Position = d.ent->GET(Transform)->Position;

	clear_last_frame = RoomIsClear();
}

void DungeonGameplay::CheckDoorEntry(float dt)
{
	const float EntryTolerance = 0.1f;

	for (Door &d : doors)
	{
		RigidBody* door_body = d.ent->GET_COMPONENT(RigidBody);
		// Check player entry
		Entity* player_ent = 0;
		for (auto hit : door_body->collisionList)
		{
			// If player hitting door
			if (hit.object->HasComponent(EC_Player) &&
				// And player not already in door
				(d.players_in_door.find(hit.object) == d.players_in_door.end()))
				player_ent = hit.object;
		}
		// No players, no problem
		if (player_ent != 0)
		{
			// and player moving towards door
			Vector2D player_dir = player_ent->GET_COMPONENT(RigidBody)->direction;
			Vector2D door_offset = GETSYS(Graphics)->GetCameraPosition() -
				d.ent->GET_COMPONENT(Transform)->Position;
			if (door_offset.Normalize() && Math::dot(player_dir, -door_offset) > EntryTolerance)
				// Add them to the door
			{
				// this makes the player dissapear
				ENGINE->SendMsg(player_ent, d.ent, MSG_Door);
				// Remember the player 
				d.players_in_door.insert(player_ent);
				// If its the first player, set our exit dir
				if (d.players_in_door.size() == 1)
					d.exitDir = door_offset;
				// If its the last player, flip our exit dir and move the camera
				if (d.players_in_door.size() == LivePlayers())
				{
          Vector2DInt Map_Index;
					d.exitDir = -d.exitDir;
					target_camera_pos = GetRoomCenter(d.ent->GET(Transform)->Position + d.exitDir);
          Map_Index = WorldToLayout(target_camera_pos);
          layout_->SetRoomVisited(Map_Index.x, Map_Index.y);
				}
			}
		}
	}
}

void DungeonGameplay::CheckDoorExit(float dt)
{
	const float ExitTolerance = 0.1f;

	Entity* player_ent = 0;
	for (Door& d : doors)
	{
		// If player in door
		for (auto it = d.players_in_door.begin(); it != d.players_in_door.end(); ++it)
		{
			player_ent = *it;
			// And move dir is along exit dir
			if (dot(player_ent->GET(RigidBody)->direction, d.exitDir) > ExitTolerance)
				// and has enough room
			{
				//Remove them from the door
				d.players_in_door.erase(d.players_in_door.find(player_ent));
				// Tell everyone a players leaving a door
				ENGINE->SendMsg(player_ent, d.ent, MSG_Door);
				// Place the player outside the door
				player_ent->GET_COMPONENT(Transform)->Position =
					d.ent->GET(Transform)->Position + d.exitDir * d.ent->GET(Transform)->scale.Length();
				break; // Our iterators no good any more... 
			}
		}
	}
}

void DungeonGameplay::UpdateDestructibleWalls(float dt)
{

	const int DROP_CHANCE = 15; // 1 in this number drops a pickup
	for (Entity* wall : destructible_walls)
	{
		Health* wallHealth = wall->GET_COMPONENT(Health);
		Sprite* wallSprite = wall->GET_COMPONENT(Sprite);
		Drawable* wallDraw = wall->GET_COMPONENT(Drawable);
			RigidBody* wallBody = wall->GET_COMPONENT(RigidBody);

		wallHealth->Update(dt);

	  int hp = wallHealth->GetHealthPercent();
		if (wallDraw->visible && !wallDraw->clipped && hp < 100)
		{
			// make it "invisible"
			if (hp < 10)
			{
				Transform* wallTrans = wall->GET_COMPONENT(Transform);
				// Turn off collisions, drawing, and bullet logic
				wallBody->ghost = true;
				wallDraw->visible = false;
				Team* wallTeam = wall->GET(Team);
				wallTeam->teamID = Team_ALL;
				// Tell AI what we did (message here would be cool
				glm::vec2 pos(wallTrans->Position.x, wallTrans->Position.y);
				GETSYS(GridMap)->GetPlot(pos)->collision = false;
				// Drop a powerup sometimes
				if(ADLib::XinYodds(1, DROP_CHANCE))
					GETSYS(PowerupHandler)->CreateRandomEntity(pos, PowerupHandler::POWERUP_PICKUP);

			}
			else if (hp < 50)
			{
				if (currentLevel >= 3)
					wallSprite->ChangeTexture("wall_breakable_purple_3.png");
				else
					wallSprite->ChangeTexture("wall_breakable_3.png");
			}
			else
			{
				if (currentLevel >= 3)
					wallSprite->ChangeTexture("wall_breakable_purple_2.png");
				else
					wallSprite->ChangeTexture("wall_breakable_2.png");
			}
		}
	}
}

void DungeonGameplay::UpdateMiniMap()
{
  for (auto it : _entities)
  {
    if (!(it)->HasComponent(EC_Minimap))
    {
      continue;
    }

    if (!it->GetActive())
    {
      continue;
    }

    if (test_bug)
    {
      test_bug = !test_bug;
    }

    Drawable* Draw_ = (it)->GET_COMPONENT(Drawable);
    Transform* Trans_ = (it)->GET_COMPONENT(Transform);
    Sprite* Sprite_ = (it)->GET_COMPONENT(Sprite);

    if ((Draw_ != nullptr) && (Trans_ != nullptr) && (Sprite_ != nullptr))
    {
      Vector2DInt pos = WorldToLayout(Trans_->Position - MiniMap_Offset);

      const Map_Layout* room = GetDungeonLayout()->GetRoomInfo(pos.x, pos.y);

      if (room->CurrentRoom)
      {
        Sprite_->Color.r = 0.0f;
        Sprite_->Color.g = 0.5f;
        Sprite_->Color.b = 1.0f;
        Sprite_->Color.a = 0.75f;
      }
      else
      {
        Sprite_->Color.r = Draw_->r * room->ColorMask.x;
        Sprite_->Color.g = Draw_->g * room->ColorMask.y;
        Sprite_->Color.b = Draw_->b * room->ColorMask.z;
        Sprite_->Color.a = 0.75f;
      }

      if (room->Visited == Visitation::VIS_NOT)
      {
        Draw_->visible = false;
      }
      else
      {
        Draw_->visible = true;
      }
    }
  }
}

void DungeonGameplay::Update(float dt)
{
	enemies = ENGINE->OM->FilterEntities(MC_Behavior | MC_RigidBody | MC_Sprite);

	// CHEAT for forcing PVP on/off
#ifdef _DEBUG
	if (CheckKeyTriggered(KEY_P, MOD_ALT))
		Team::pvp_enabled ? EndPvp() : StartPvp();
#endif

	if (CheckKeyTriggered(KEY_L))
		Cheat_DisableDarkness();

	if (CheckKeyTriggered(KEY_P))
		LevelUp();

	Cheat_KillPlayers();

	UpdateBehaviors(dt);
	UpdateMovement(dt);

	UpdateBullets(dt);
	UpdateRoomClear();

	UpdateTorches(dt);
	UpdateDestructibleWalls(dt);
	UpdateCamera(dt);
	UpdateDoors(dt);
	UpdateMiniMap();

	bool playersAdded = CheckAndAddPlayers();
	if (!playersAdded)
		CheckPause();

	// Check if we're fighting and if one player is left
	if (Team::pvp_enabled)
	{
		static float reset_time = 7.0f;
		int players_alive = 0;
		for (Entity * p : players)
			if (!p->GC(Player)->IsDead())
				players_alive++;
		if (players_alive <= 1)
		{
			if (reset_time <= 0.0f)
			{
				reset_time = 7.0f;
				EndPvp();
				LevelUp();
			}
			else
				reset_time -= dt;
		}
	}

	// Check if all the players are dead
	bool all_players_dead = true;
	for (Entity* p : players)
	{
		if (!p->GET_COMPONENT(Player)->IsDead())
			all_players_dead = false;
	}

	if (all_players_dead)
	{
		static float time_to_reset = 7.0f;
		if (time_to_reset == 7.0f)
		{
			std::cout << "Everyone died :(\n";
			GETSYS(AudioSystem)->NarrateSound("endDefeat");
			time_to_reset -= dt;
		}
		else if (time_to_reset < 0.0f)
		{
			time_to_reset = 7.0f;
			ENGINE->popGamestate();
			return;
		}
		else
		{

			time_to_reset -= dt;
		}
	}

  for (Entity * e : enemies)
  {
    // Check health
    if (e->GET_COMPONENT(Health)->health <= 0.0f)
    {
      if (e->HasComponent(EC_Audio))
        e->GET(Audio)->PlayNow(Audio::AudioEvent::AE_DIE);
      ENGINE->OM->DestroyEntity(e);
    }
  }
	if (Toggle_AntTweakBar && CheckMouseTriggered(MOUSE_LEFT))
	{
		Point2D position = GetMousePosition();

		for (Entity* p : players)
		{
			auto pPos = (p)->GET_COMPONENT(Transform)->Position;

			if (pPos.Distance(position) < 0.5f)
			{
				SelectEntityAntTweakBar(p);
				return;
			}
		}

		for (Entity* p : enemies)
		{
			auto pPos = (p)->GET_COMPONENT(Transform)->Position;

			if (pPos.Distance(position) < 0.5f)
			{
				SelectEntityAntTweakBar(p);
				return;
			}
		}
	}
}

void DungeonGameplay::CheckPause()
{
	bool pause = false;

	if (CheckKeyTriggered(KEY_SPACE) || CheckKeyTriggered(KEY_ESC) || CheckKeyTriggered(KEY_BACKSPACE))
	{
		pause = true;
	}

	for (Entity *p : players)
	{
		if (CheckJSButtonTriggered((JOYSTICKS)(p->GET(Player)->GetNum() - 1), XBOX_START))
		{
			pause = true;
			break;
		}
	}

	if (pause)
	{
#if (defined _DEBUG && defined DEBUG_QUICK_LOAD)
	ENGINE->_running = false;
#else
	ENGINE->pushGamestate(new MenuGS(false, true));
	return;
#endif
	}


}

void DungeonGameplay::Shutdown(void)
{
	// Destroy the HUD idtems
	static_cast<HUDsystem*>(ENGINE->GetSystem(sys_HUDsystem))->DestroyHudItems();
	// Stop the audio
	GETSYS(AudioSystem)->StopMusic(false);

	players.clear();
	ENGINE->SendMsg(0, 0, MSG_Clear_Dungeon);
}

void DungeonGameplay::PoppedTo(systype sys)
{
	GETSYS(Graphics)->SetLighting(true);
	GETSYS(AudioSystem)->PlayMusic("Level");

  
  if (sys == sys_MenuGS)
    paused = false;

}

void DungeonGameplay::SendMsg(Entity* a, Entity* b, message m)
{
	switch (m)
	{
    
	case MSG_Built_Dungeon:
		// Put the camera in the center
    GETSYS(Graphics)->SetCameraPosition(GetRoomCenter(DungeonToWorld(dungeon_generator.GetStartingRoomCenter().x,
      dungeon_generator.GetStartingRoomCenter().y)), HUD_Offset_);
		target_camera_pos = GETSYS(Graphics)->GetCameraPosition();
    break;

    // Window changes
  case MSG_Minimized:
    if (!paused)
    {
      ENGINE->pushGamestate(new MenuGS(false, true));
      paused = true;
    }
    break;
  case MSG_LoseFocus:
    if (!paused)
    {
      ENGINE->pushGamestate(new MenuGS(false, true));
      paused = true;
    }
    break;
	default:
		break;
	}
}



bool DungeonGameplay::CheckAndAddPlayers(void)
{
	// If we have all possible players, don't bother checking
	if (players.size() == NUMBEROFJOYSTICKS)
		return false;

	// Cheats to add players without controllers (control them with the keyboard)
	int CheatAdd = JOYSTICK_1 - 1;
	
	{
		if (CheckKeyTriggered(KEY_1, MOD_SHIFT))
			CheatAdd = JOYSTICK_1;
		else if (CheckKeyTriggered(KEY_2, MOD_SHIFT))
			CheatAdd = JOYSTICK_2;
		else if (CheckKeyTriggered(KEY_3, MOD_SHIFT))
			CheatAdd = JOYSTICK_3;
		else if (CheckKeyTriggered(KEY_4, MOD_SHIFT))
			CheatAdd = JOYSTICK_4;
	}

	// Loop through all possible controllers
	for (int i = JOYSTICK_1; i < NUMBEROFJOYSTICKS; ++i)
	{
		// Is the controller connected?
		if ((CheckJSActive((JOYSTICKS)i) && CheckJSButtonTriggered((JOYSTICKS)i, JOYSTICK_BUTTONS::XBOX_START)) || CheatAdd == i)
		{
			// Is this player already added?
			bool playerAdded = false;
			for (Entity* p : players)
				if (p->GET_COMPONENT(Player)->GetNum() == i + 1)
					playerAdded = true;
			// Add the player if they aren't already added
			if (!playerAdded)
			{
				// Find where we're gonna put em
				Point2D room_center = GetRoomCenter(GETSYS(Graphics)->GetCameraPosition());
				players.push_back(PlayerHandler::CreatePlayer(room_center, i + 1));
				return true;
			}
		}
	}

	// If no controllers are connected, then add player 1 for keyboard
	if (players.empty())
	{
		Point2D room_center = GetRoomCenter(GETSYS(Graphics)->GetCameraPosition());
		players.push_back(PlayerHandler::CreatePlayer(room_center, 1));

		SelectEntityAntTweakBar(players[0]);

		return true;
	}

	return false;
}

void DungeonGameplay::StartPvp()
{
	Team::pvp_enabled = true;
	GETSYS(AudioSystem)->NarrateSound("fight");

	Transform* stran = new Transform();
	stran->scale = Vector2D(5.0f, 5.0f);
	Drawable* sdraw = new Drawable();
	sdraw->layer = static_cast<unsigned>(-1);
	sdraw->useSprite = true;
	Sprite* ssprite = new Sprite("pvp_fight.png");
	GUIItem* sgui = new GUIItem();
	sgui->screenPos = Point2D(0.0f, -6.0f);
	Entity * splashEnt = ENGINE->Factory->Compose(4,
		stran,
		sdraw,
		ssprite,
		sgui
		);
	splashEnt->SetName("fight_splash");




}

void DungeonGameplay::EndPvp()
{
	Team::pvp_enabled = false;
	ENGINE->OM->DestroyAll(ENGINE->OM->GetEntityByName("fight_splash"));

	int winning_player_num = 0;
	for (Entity* p : players)
		if (p->GET(Player)->IsAlive())
			winning_player_num = p->GET(Player)->GetNum();

	switch (winning_player_num)
	{
	case 1:
		GETSYS(AudioSystem)->NarrateSound("purpleWin");
		break;
	case 2:
		GETSYS(AudioSystem)->NarrateSound("pinkWin");
		break;
	case 3:
		GETSYS(AudioSystem)->NarrateSound("tealWin");
		break;
	case 4:
		GETSYS(AudioSystem)->NarrateSound("orangeWin");
		break;
	default:
		break;
	}

}

int DungeonGameplay::LivePlayers()
{
	int live = 0;
	for (Entity* p : players)
		if (!p->GET(Player)->IsDead())
			live++;
	return live;
}