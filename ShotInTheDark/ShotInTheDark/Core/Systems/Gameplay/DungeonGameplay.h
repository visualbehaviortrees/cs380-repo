//	File name:		DungeonGameplay.h
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../../System.hpp"
#include "DungeonGenerator.h"
#include "DungeonLayoutGenerator.h"

#include <unordered_set>

#define DEBUG_NO_MUSIC false
#define STARTING_LEVEL 3
#define END_LEVEL 3

extern bool GameComplete;

#define GetRowColumn(Pos, RetX, RetY) WorldToDungeonMap(Pos, RetX, RetY)

#define GetCoordinates(x, y) DungeonMapToWorld(x, y)

#define g_terrain (static_cast<DungeonGameplay*>(ENGINE->getCurrentGamestate()))

#define ShootBullet(player, velocity) g_terrain->CreateBullet(player, velocity)

class DungeonGameplay : public Gamestate
{
public:
	// Gamestate functions
	DungeonGameplay();
	~DungeonGameplay();
	void Init(void);
	void Update(float dt);
	void Shutdown(void);
	void PoppedTo(systype);
	void SendMsg(Entity* a, Entity* b, message);

	int GetCurrentLevel();

	// Takes a dungeonmap index and scales it into world space
	Point2D DungeonToWorld(int x, int y);

	Vector2DInt WorldToDungeon(Point2D pos);

	// Finds the center of the room the current point is in
	Point2D GetRoomCenter(Point2D pos);

	// Returns the worldspace width and height of each room
	Vector2D GetRoomDimensions();

	const Map_Layout* GetRoomInfo(Vector2DInt &index)
	{
		return layout_->GetRoomInfo(index.x, index.y);
	}

	bool IsWall(int x, int y, bool fix = false)
	{
		if (fix)
		{
			//The map is upside down -- flip it to account for this
			y = GetHeight() - y - 1;
		}

		return x < 0 || x >= (int)GetWidth() || y < 0 || y >= (int)GetHeight() ||
			d.GetTile(x, y) == TILE_WALL;
	}

	DungeonTile GetTile(int x, int y)
	{
		return d.GetTile(x, y);
	}

	Entity* CreateBullet(Entity* player, Vector2D vel);

	const std::vector<Entity*>& ViewEnemies()
	{
		return enemies;
	}

	Entity* GetEnemy(int id)
	{
		for (Entity *e : enemies)
		{
			if (e->GetId() == id)
			{
				return e;
			}
		}

		return nullptr;
	}

	Entity* GetEnemy(Point2D &atPos)
	{
		for (Entity* e : enemies)
		{
			auto pos = e->GET_COMPONENT(Transform)->Position;

			if (std::abs(pos.x - atPos.x) < 0.01 && std::abs(pos.y - atPos.y) < 0.01)
			{
				return e;
			}
		}

		return 0;
	}

	const std::vector<Entity*>& ViewPlayers()
	{
		return players;
	}

	const std::vector<Entity*>& ViewTorches()
	{
		return torches;
	}

	Entity* GetTorch(int id)
	{
		for (Entity *e : torches)
		{
			if (e->GetId() == id)
			{
				return e;
			}
		}

		return 0;
	}

	Entity* GetTorch(Point2D &atPos)
	{
		for (Entity* e : torches)
		{
			auto pos = e->GET_COMPONENT(Transform)->Position;

			if (std::abs(pos.x - atPos.x) < 0.01 && std::abs(pos.y - atPos.y) < 0.01)
			{
				return e;
			}
		}

		return 0;
	}

	// line intersect text between lines p1-p2 and p3-p4
	// using Rabins algortithm
	#define ABS(a) (((a) < 0) ? -(a) : (a))
	bool LineIntersect(float x1, float y1, float x2, float y2,
					   float x3, float y3, float x4, float y4)
	{
		float y4y3 = y4 - y3;
		float y1y3 = y1 - y3;
		float y2y1 = y2 - y1;
		float x4x3 = x4 - x3;
		float x2x1 = x2 - x1;
		float x1x3 = x1 - x3;
		float denom = y4y3 * x2x1 - x4x3 * y2y1;
		float numera = x4x3 * y1y3 - y4y3 * x1x3;
		float numerb = x2x1 * y1y3 - y2y1 * x1x3;

		const float eps = 0.0001f;
		if (ABS(numera) < eps && ABS(numerb) < eps && ABS(denom) < eps)
		{	//Lines coincident (on top of each other)
			return true;
		}

		if (ABS(denom) < eps)
		{	//Lines parallel
			return false;
		}

		float mua = numera / denom;
		float mub = numerb / denom;
		if (mua < 0 || mua > 1 || mub < 0 || mub > 1)
		{	//No intersection
			return false;
		}
		else
		{	//Intersection
			return true;
		}
	}

	// checks if there is a clear path between p1 and p2
	// only checks tiles within a bound box around line p1-p2
	// Does NOT return false if target is a wall, in case you want to target a wall
	bool IsClearPath(Point2D const &p0, Point2D const &p1)
	{
		int x0, y0, x1, y1;
		GetRowColumn(p0, &x0, &y0);
		GetRowColumn(p1, &x1, &y1);

		float offset = 1.0f;

		// set i's and j's to loop through all viable grid points
		int i1, i2, j1, j2;
		if (x0 > x1)
		{
			i1 = x1;
			i2 = x0;
		}
		else
		{
			i1 = x0;
			i2 = x1;
		}

		if (y0 > y1)
		{
			j1 = y1;
			j2 = y0;
		}
		else
		{
			j1 = y0;
			j2 = y1;
		}

		for (int i = i1; i <= i2; ++i)
		{
			for (int j = j1; j <= j2; j++)
			{
				if (IsWall(i, j))
				{
					// check all sides of wall
					if (LineIntersect((float)x0, (float)y0, (float)x1, (float)y1,
						(float)i - offset, (float)j + offset, (float)i + offset, (float)j + offset)
						|| LineIntersect((float)x0, (float)y0, (float)x1, (float)y1,
						(float)i - offset, (float)j - offset, (float)i + offset, (float)j - offset)
						|| LineIntersect((float)x0, (float)y0, (float)x1, (float)y1,
						(float)i - offset, (float)j - offset, (float)i - offset, (float)j + offset)
						|| LineIntersect((float)x0, (float)y0, (float)x1, (float)y1,
						(float)i + offset, (float)j - offset, (float)i + offset, (float)j + offset))
					{
						return false;
					}
				}
			}
		}

		return true;
	}

	Vector2DInt WorldToDungeonMap(Point2D pos);

	void WorldToDungeonMap(Point2D pos, int* x, int *y);

	Point2D DungeonMapToWorld(Vector2DInt pos);

	Point2D DungeonMapToWorld(int x, int y);

	void MoveTile(int x1, int y1, int x2, int y2)
	{
		DungeonTile t = d.GetTile(x1, y1);
		DungeonTile t2 = d.GetTile(x2, y2);

		d.SetTile(x1, y1, TILE_OPEN);
		d.SetTile(x2, y2, t);
	}

	int GetWidth()
	{
		return d.GetWidth();
	}

	int GetHeight()
	{
		return d.GetHeight();
	}

private:
	Math::Hcoords GetRoomColor(LayoutTile RoomType);

	bool RoomIsClear(void);

	const DungeonLayout* GetDungeonLayout(void) const
	{
		return layout_;
	}

	Vector2DInt WorldToLayout(Point2D pos);
	Point2D LayoutToWorld(int x, int y);
	static void PlaceWall(Point2D pos);
	void PlaceTorch(Point2D pos);
	void PlaceBackground(void);
	void BuildDungeon(DungeonMap& dungeon);
	void PlaceEnemy(Point2D pos);
	void PlaceDestructibleWall(Point2D pos);
	void PlaceDoor(Point2D pos);
	int LivePlayers();
	// Destroys the dungeon and builds a harder one
	void LevelUp();
	
	// Adds players depending on how many controllers are connected
	bool CheckAndAddPlayers(void);
	void CheckPause(void);
	void UpdateTorches(float dt);
	void UpdateBullets(float dt);
	void UpdateDestructibleWalls(float dt);
	void UpdateDoors(float dt);
	void UpdateMiniMap(void);
	void UpdateBehaviors(float dt);
	void UpdateMovement(float dt);

	void UpdateRoomClear(void);
	bool room_clear;


	void CheckDoorEntry(float dt);
	void CheckDoorExit(float dt);
	void UpdateCamera(float dt);

	bool CameraMoving();
	bool paused = false;

	void SetDungeonColor(float r, float g, float b, float brightness);

	void StartPvp();
	void EndPvp();

	Point2D target_camera_pos;

	glm::vec4 current_dungeon_color; // For decoration :)

	DungeonLayout* layout_;
	DungeonLayoutGenerator layout_generator;
	DungeonGenerator dungeon_generator;
	// The total dimensions of our dungeon
	Vector2DInt dungeon_dimensions;
	// The dimensions of each room in the dungeon
	Vector2DInt room_dimensions;
	// How many rooms across are we
	Vector2DInt num_rooms;

	float dungeon_scale;
	Point2D dungeon_origin; // (0,0), the bottom/left of the dungon centered at 0,0
	DungeonMap d;

	std::vector<Entity* > players;
	std::vector<Entity* > torches;
	std::vector<Entity* > enemies;
	std::vector<Entity *> bullets;
	std::vector<Entity* > destructible_walls;

	struct Door
	{
		std::unordered_set<Entity*> players_in_door;
		float coolDown;
		Entity* ent;
		Vector2D exitDir;
	};

	std::vector<Door> doors;

	void Cheat_KillPlayers(void); 


	bool test_bug = false;
};