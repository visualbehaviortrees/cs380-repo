//	File name:		DungeonGenerator.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.
#include "stdafx.h"
#include <algorithm>
#include <ctime>

#include "DungeonGenerator.h"


Affine DungeonToWorld(int width, int height)
{
	return Affine(Scale(1 * 2.0f) * Trans(Point2D(-(width - 1) / 2.0f, -(height - 1) / 2.0f)));
}

Affine WorldToDungeonAffine(int width, int height)
{
	return Math::Inverse(DungeonToWorld(width, height));
}

DungeonGenerator::DungeonGenerator() : random_generator(), starting_room_center(0, 0)
{
  #ifdef _DEBUG
    //random_generator;//(int)time(0); //std::chrono::system_clock::now();
  #else
    random_generator = (int)time(0); //std::chrono::system_clock::now();
  #endif
}

void DungeonGenerator::SetRoomSize(unsigned width, unsigned height)
{
	roomWidth = width;
	roomHeight = height;
}

DungeonMap DungeonGenerator::GenerateEmptyRoom(unsigned width, unsigned height, int dir)
{
	DungeonMap map(width, height);

	for (unsigned i = 0; i < width; ++i)
	{
		for (unsigned j = 0; j < height; ++j)
		{
			if (i == 0 || j == 0 || i == (width - 1) || j == (height - 1))
				map.SetTile(i, j, TILE_WALL);
		}
	}

	// Then, carve openings if we need them

	CarveOpenings(map, dir);

	return map;
}

// Gets an random room with walls along the perimiter of the given size, and openings along the directions given
DungeonMap DungeonGenerator::GenerateRandomRoom(unsigned width, unsigned height, int openings, DM_Modifiers mods)
{
	DungeonMap map = GenerateEmptyRoom(width, height, openings);

	ApplyRoomModifiers(map, mods);

	return map;
}

void DungeonGenerator::ScatterTiles(DungeonMap& room, DungeonTile scatter, float frequency, DungeonTile replace_tile)
{
	if (frequency <= 0.0f)
		return;

	for (unsigned i = 0; i < room.GetWidth(); ++i)
	{
		for (unsigned j = 0; j < room.GetHeight(); ++j)
		{
			if (room.GetTile(i, j) == replace_tile)
			{
				if (random_generator.OneIn(frequency))
					room.SetTile(i, j, scatter);
			}
		}
	}
}

void DungeonGenerator::ScatterSingleTile(DungeonMap& room, DungeonTile scatter, DungeonTile replace_tile)
{
	// Guess until we get one
	unsigned i, j;
	unsigned tries = room.GetWidth() * room.GetHeight();
	while (tries-- >= 0)
	{
		i = random_generator.range(0, room.GetWidth());
		j = random_generator.range(0, room.GetHeight());

		while (i < room.GetWidth() && j < room.GetHeight())
		{
			if (room.GetTile(i, j) == replace_tile)
			{
				room.SetTile(i, j, scatter);
				return;
			}
			else
			{
				++i;
				++j;
			}
		}
	}

}


DungeonMap DungeonGenerator::CombineMaps(const DungeonMap& A, const DungeonMap& B, unsigned x, unsigned y)
{
	// Calculate the size of the final map
	unsigned width = std::max(A.GetWidth(), x + B.GetWidth());
	unsigned height = std::max(A.GetHeight(), y + B.GetHeight());

	DungeonMap D(width, height);

	// Place map A into D
	for (unsigned i = 0; i < A.GetWidth(); ++i)
		for (unsigned j = 0; j < A.GetHeight(); ++j)
		{
			DungeonTile tile = A.GetTile(i, j);
			D.SetTile(i, j, tile);
		}

	// Place map B offset into D - overwriting what was already there
	// Place map A into D
	for (unsigned i = 0; i < B.GetWidth(); ++i)
		for (unsigned j = 0; j < B.GetHeight(); ++j)
		{
			DungeonTile tile = B.GetTile(i, j);
			D.SetTile(i + x, j + y, tile);
		}

	return D;
}

void DungeonGenerator::CarveOpenings(DungeonMap& room, int openings)
{
	unsigned x_mid = room.GetWidth() / 2;
	unsigned y_mid = room.GetHeight() / 2;

	if (openings & DIR_NORTH)
		room.SetTile(x_mid, room.GetHeight() - 1, TILE_DOOR);
	if (openings & DIR_SOUTH)
		room.SetTile(x_mid, 0, TILE_DOOR);
	if (openings & DIR_EAST)
		room.SetTile(room.GetWidth() - 1, y_mid, TILE_DOOR);
	if (openings & DIR_WEST)
		room.SetTile(0, y_mid, TILE_DOOR);
}


DungeonMap DungeonGenerator::GenerateDungeon(unsigned width, unsigned height, DM_Modifiers mods)
{
	// Figure out how many rooms we need
	unsigned numRooms_x = width / (roomWidth - 1);
	unsigned numRooms_y = height / (roomHeight - 1);


	DungeonMap D(0, 0);
	AddRoomList("AllRooms.txt");


	// Append empty rooms together
	for (unsigned i = 0; i < numRooms_x; ++i)
	{
		unsigned x_offset = i * (roomWidth - 1);
		for (unsigned j = 0; j < numRooms_y; ++j)
		{
			unsigned y_offset = j * (roomHeight - 1);
			// Figure out what openings we want
			int openings = DIR_ALL; //random_generator.range(DIR_NONE + 1, DIR_ALL); <-- this sometimes blocks off rooms...

			// Turn off openings that would lead off of the map
			if (i == 0)
				openings &= ~DIR_WEST;
			if (j == 0)
				openings &= ~DIR_SOUTH;
			if (i == numRooms_x - 1)
				openings &= ~DIR_EAST;
			if (j == numRooms_y - 1)
				openings &= ~DIR_NORTH;
			// Make a new room
			DungeonMap room(0, 0);
			// Check if we need to make a safe room
			if (i == numRooms_x / 2 && j == numRooms_y / 2)
			{
				room = GenerateEmptyRoom(roomWidth, roomHeight, DIR_ALL);
			}
			else if (!room_pool.empty())
			{
				int room_pool_index = random_generator.range(0, room_pool.size() - 1);
				room = room_pool.at(room_pool_index);
				if (mods.no_repeats)
					room_pool.erase(room_pool.begin() + room_pool_index, room_pool.begin() + room_pool_index + 1);
				ApplyRoomModifiers(room, mods);
				CarveOpenings(room, openings);
			}
			else
			{
				room = GenerateRandomRoom(roomWidth, roomHeight, openings, mods);
			}

			D = CombineMaps(D, room, x_offset, y_offset);
		}
	}

	// Outline the walls of the outer dungeon to cover any gaps that were created
	for (unsigned i = 0; i < D.GetWidth(); ++i)
		for (unsigned j = 0; j < D.GetHeight(); ++j)
			if (i == 0 || j == 0 || j == D.GetHeight() - 1 || i == D.GetWidth() - 1)
				D.SetTile(i, j, TILE_WALL);
	// For some reason gaps still get through....

	return D;
}

DungeonMap DungeonGenerator::GenerateDungeonFromLayout(DungeonMap& initiald, DungeonLayout& map, DM_Modifiers mods)
{
	// Figure out how many rooms we need
	unsigned numRooms_x = map.GetWidth();
	unsigned numRooms_y = map.GetHeight();

	unsigned total_height = numRooms_y * (roomHeight - 1);


	DungeonMap D(0, 0);
	if (room_pool.empty())
		AddRoomList("All_Rooms.txt");

	// Append empty rooms together
	for (unsigned i = 0; i < numRooms_x; ++i)
	{
		unsigned x_offset = i * (roomWidth - 1);
		for (unsigned j = 0; j < numRooms_y; ++j)
		{
			// Account for Kyle being upside down
			unsigned y_offset = total_height - j * (roomHeight - 1);
			// Figure out what openings we want
			int openings = map.GetAdjacentTileDirs(i, j);
			// Make a new room
			initiald = DungeonMap(0, 0);
			// Check if we need to make a safe room
			if (map.GetRoom(i, j) == LAYOUT_START_ROOM)
			{
				int room_pool_index = random_generator.range(0, room_pool.size() - 1);
				initiald = room_pool.at(room_pool_index);
				ApplyRoomModifiers(initiald, mods);
				if (mods.no_repeats)
					room_pool.erase(room_pool.begin() + room_pool_index, room_pool.begin() + room_pool_index + 1);
				CarveOpenings(initiald, openings);

				starting_room_center.x = x_offset + roomWidth / 2;
				starting_room_center.y = y_offset + roomHeight / 2;

			}
			else if (map.GetRoom(i, j) == LAYOUT_ROOM)
			{
				int room_pool_index = random_generator.range(0, room_pool.size() - 1);
				initiald = room_pool.at(room_pool_index);
				ApplyRoomModifiers(initiald, mods);
				if (mods.no_repeats)
					room_pool.erase(room_pool.begin() + room_pool_index, room_pool.begin() + room_pool_index + 1);
				CarveOpenings(initiald, openings);
			}
			else if (map.GetRoom(i, j) == LAYOUT_TREASURE_ROOM)
			{
				initiald = GenerateEmptyRoom(roomWidth, roomHeight, openings);
				for (int t = 0; t < mods.treasure_size; ++t)
					ScatterSingleTile(initiald, TILE_TREASURE);
			}

			D = CombineMaps(D, initiald, x_offset, y_offset);
		}
	}

	return D;
}

Vector2DInt DungeonGenerator::GetStartingRoomCenter()
{
	return starting_room_center;
}

void DungeonGenerator::ApplyRoomModifiers(DungeonMap& d, const DM_Modifiers mods)
{
	// Apply removals before additions to maximize madness

	// DARKNESS
	if (random_generator.OneIn(mods.darken_chance))
		ScatterTiles(d, TILE_OPEN, mods.darken_weight, TILE_TORCH);

	// SAFE
	if (random_generator.OneIn(mods.safe_chance))
		ScatterTiles(d, TILE_OPEN, mods.safe_weight, TILE_ENEMY);

	// BRIGHTNESS
	if (random_generator.OneIn(mods.brighten_chance))
		ScatterTiles(d, TILE_TORCH, mods.brighten_weight, TILE_WALL);

	// TREASURE
	if (random_generator.OneIn(mods.pickup_chance))
		for (int t = random_generator.Roll(mods.pickup_max); t > 0; --t)
			ScatterSingleTile(d, TILE_PICKUP);


	// CHAOS
	//if (random_generator.OneIn(mods.chaos_chance))
	//{
	//	ScatterTiles(d, TILE_WALL, mods.chaos_density, TILE_ENEMY);
	//	ScatterTiles(d, TILE_WALL, mods.chaos_density, TILE_OPEN);
	//} -- Disabled due to creating unwinnable rooms

	// DANGER
	if (random_generator.OneIn(mods.danger_chance))
		ScatterTiles(d, TILE_ENEMY, mods.danger_weight, TILE_TORCH);
}

void DungeonGenerator::AddRoomList(const char* filename)
{
	std::string file;
	file = "Resources/Rooms/";
	file += filename;
	room_pool = LoadAllRooms(file.c_str());
}


std::vector<DungeonMap> DungeonGenerator::LoadAllRooms(const char* filename)
{
	std::ifstream all_rooms_file(filename);
	std::vector<DungeonMap> rooms;
	string roompath;

	if (all_rooms_file.is_open())
	{
		while (getline(all_rooms_file, roompath))
		{
			std::ifstream current_room(roompath);
			if (current_room.is_open())
			{
				DungeonMap D = LoadRoomFromFile(roompath.c_str());
				rooms.push_back(D);
				current_room.close();
			}
			else
				return std::vector<DungeonMap>();
		}

		all_rooms_file.close();
	}
	else
		return std::vector<DungeonMap>();

	return rooms;
}


DungeonMap DungeonGenerator::LoadRoomFromFile(const char* filename)
{
	std::ifstream file(filename);

	if (file.is_open())
	{
		int height, width;
		file >> width >> height;
		DungeonMap Map(width, height);

		for (int j = 0; j < height; ++j)
		{

			for (int i = 0; i < width; ++i)
			{
				int tile = 0;
				file >> tile;

				if (tile < MAX_TILE_TYPES)
				{
					Map.SetTile(i, j, static_cast<DungeonTile>(tile));
				}
				/*
						switch(tile)
						{
						case TILE_WALL:
						Map.SetTile(i, j, TILE_WALL);
						break;

						case TILE_OPEN:
						Map.SetTile(i, j, TILE_OPEN);
						break;

						default:
						break;
						}*/
			}
		}
		file.close();
		return Map;
	}
	else
		return DungeonMap(0, 0);
}

DungeonMap DungeonGenerator::LoadRoomFromJSON(const char * filename)
{
	return DungeonMap(0, 0);
}



