//	File name:		DungeonGenerator.h
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.
#pragma once

#include "Libraries\RNG\RNG.hpp"

#include "DungeonMap.h"
#include "DungeonLayout.h"


// Finds the world coordinates of a duneon index
Affine DungeonToWorld(int width, int height);
Affine WorldToDungeonAffine(int width, int height);


struct DM_Modifiers
{
	// Makes rooms darker by removing torches
	float darken_chance, darken_weight;

	// Adds some random torches
	float brighten_chance, brighten_weight;

	// Adds random room obstacles and remove random walls
	float chaos_chance, chaos_density;

	// Makes rooms safer by removing enemies
	float safe_chance, safe_weight;

	// Makes rooms more dangerous by adding enemies
	float danger_chance, danger_weight;

	// How much treasure is in each treasure room
	int treasure_size;

	// How often pickups spawn, and how many
	float pickup_chance; int pickup_max;

	// Removes a room from the room pool after using it
	bool no_repeats; 

	DM_Modifiers() :
		darken_chance(0.0f), darken_weight(0.0f),
		brighten_chance(0.0f), brighten_weight(0.0f),
		chaos_chance(0.0f), chaos_density(0.0f),
		safe_chance(0.0f), safe_weight(0.0f),
		danger_chance(0.0f), danger_weight(0.0f),
		treasure_size(0),
		pickup_chance(0.0f), pickup_max(0),
		no_repeats(true)
	{ }

	//DM_Modifiers(int difficulty_level);
};

class DungeonGenerator
{
public:


	DungeonGenerator();

  	// Assembles a generated dungeon of the given size
	DungeonMap GenerateDungeon(unsigned width, unsigned height, DM_Modifiers mods = DM_Modifiers());

	// Takes a map (where Wall = room and 0 = no room) and expands it to hold rooms
	DungeonMap GenerateDungeonFromLayout(DungeonMap& initiald, DungeonLayout& map, DM_Modifiers mods = DM_Modifiers());

	// Gets an empty room with walls along the perimiter of the given size, and openings along the directions given
	DungeonMap GenerateEmptyRoom(unsigned room_width, unsigned room_height, int openings = DIR_NONE);

	// Gets an random room with walls along the perimiter of the given size, and openings along the directions given
	DungeonMap GenerateRandomRoom(unsigned room_width, unsigned room_height,
		int openings = DIR_NONE, DM_Modifiers mods = DM_Modifiers());

	// Takes a dungeon and replaces 1 in frequency tiles with the given scatter tile
	void ScatterTiles(DungeonMap& room, DungeonTile scatter,
		float frequency = 0.2, DungeonTile replace_tile = TILE_OPEN);

	// Like above, but only replaces a single tile
	void ScatterSingleTile(DungeonMap& room, DungeonTile scatter, DungeonTile replace_tile = TILE_OPEN);

	// Takes a dungeon room and adds openings on the given openings
	void CarveOpenings(DungeonMap& room, int openings);

	// This takes two dungeon rooms and places the B _over_ A at the given position
	DungeonMap CombineMaps(const DungeonMap& A, const DungeonMap& B,
		unsigned x, unsigned y);


	void AddRoomList(const char* filename);

	void SetRoomSize(unsigned width, unsigned height);

	Vector2DInt GetStartingRoomCenter();
private:
	RNG random_generator;

	void ApplyRoomModifiers(DungeonMap& d, const DM_Modifiers);

	std::vector<DungeonMap> LoadAllRooms(const char* filename);
	DungeonMap LoadRoomFromFile(const char* filename);
	DungeonMap LoadRoomFromJSON(const char* filename);

	std::vector<DungeonMap> room_pool;

	Vector2DInt starting_room_center;

	unsigned roomWidth, roomHeight;
};

