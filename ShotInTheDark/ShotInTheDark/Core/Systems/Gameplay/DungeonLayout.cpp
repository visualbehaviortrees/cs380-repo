//	File name:		DungeonLayout.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.
#include "stdafx.h"

#include "DungeonLayout.h"

DungeonLayout::Map_Info DungeonLayout::Count_Adjacent_Rooms(int index, const vector<Map_Layout>& map, int width, int height)
{
	Map_Info room_info;
	int adjacent_rooms = 0;
	int total_spots = 0;
	int empty_room = 0;

	bool flag_left = false;
	bool flag_right = false;
	bool flag_top = false;
	bool flag_bottom = false;

	// looking below
	if (index + width < (width * height))
	{
    if (map[index + width].RoomType == LAYOUT_ROOM)
    {
      room_info.room_tiles[2] = 1;
      ++adjacent_rooms;
    }


    if (map[index + width].RoomType == LAYOUT_NO_ROOM)
		{
			room_info.open_tiles[2] = 1;
			++empty_room;
		}

		flag_top = true;
		++total_spots;
	}

	// looking above
	int value = index - width; // because unsigned
	if (value > -1)
	{
    if (map[value].RoomType == LAYOUT_ROOM)
    {
      ++adjacent_rooms;
      room_info.room_tiles[3] = 1;
    }

    if (map[value].RoomType == LAYOUT_NO_ROOM)
		{
			room_info.open_tiles[3] = 1;
			++empty_room;
		}

		flag_bottom = true;
		++total_spots;
	}

	// looking left
	if (index - 1 > -1 && (index) % width != 0)
	{
    if (map[index - 1].RoomType == LAYOUT_ROOM)
    {
      ++adjacent_rooms;
      room_info.room_tiles[0] = 1;
    }

    if (map[index - 1].RoomType == LAYOUT_NO_ROOM)
		{
			room_info.open_tiles[0] = 1;
			++empty_room;
		}

		flag_right = true;
		++total_spots;
	}

	// looking right
	if (index + 1 < (width * height) && (index + 1) % width != 0)
	{
    if (map[index + 1].RoomType == LAYOUT_ROOM)
    {
      ++adjacent_rooms;
      room_info.room_tiles[1] = 1;
    }

    if (map[index + 1].RoomType == LAYOUT_NO_ROOM)
		{
			room_info.open_tiles[1] = 1;
			++empty_room;
		}

		flag_left = true;
		++total_spots;
	}


	// top left
	if (index - width - 1 > -1 && (index - width) % width != 0)
	{
    if (map[index - width - 1].RoomType == LAYOUT_ROOM)
    {
      room_info.room_tiles[4] = 1;
      ++adjacent_rooms;
    }

    if (map[index - width - 1].RoomType == LAYOUT_NO_ROOM)
		{
			room_info.open_tiles[4] = 1;
			++empty_room;
		}

		++total_spots;
	}


	// top right
	if (index - width + 1 > -1 && (index - width + 1) % width != 0)
	{
    if (map[index - width + 1].RoomType == LAYOUT_ROOM)
    {
      room_info.room_tiles[5] = 1;
			++adjacent_rooms;
    }

    if (map[index - width + 1].RoomType == LAYOUT_NO_ROOM)
		{
			room_info.open_tiles[5] = 1;
			++empty_room;
		}

		++total_spots;
	}

	// bottom left
	if (index + width - 1 < (width * height) && (index + width) % width != 0)
	{
    if (map[index + width - 1].RoomType == LAYOUT_ROOM)
    {
      room_info.room_tiles[6] = 1;
			++adjacent_rooms;
    }

    if (map[index + width - 1].RoomType == LAYOUT_NO_ROOM)
		{
			room_info.open_tiles[6] = 1;
			++empty_room;
		}

		++total_spots;
	}

	// bottom right
	if (index + width + 1 < (width * height) && (index + width + 1) % width != 0)
	{
    if (map[index + width + 1].RoomType == LAYOUT_ROOM)
    {
      room_info.room_tiles[7] = 1;
			++adjacent_rooms;
    }

    if (map[index + width + 1].RoomType == LAYOUT_NO_ROOM)
		{
			room_info.open_tiles[7] = 1;
			++empty_room;
		}

		++total_spots;
	}


	room_info.adjacent_tiles = total_spots;
	room_info.num_adjacent_rooms = adjacent_rooms;
	room_info.num_empty_rooms = empty_room;


	// center piece:
	// 1 1 1
	// 1 0 1 
	// 1 1 1 

	// left facing side piece:
	// 1 1
	//   1 
	// 1 1 

	// right facing side piece:
	// 1 1
	// 1 
	// 1 1

	// bottom side piece:
	// 1   1
	// 1 1 1

	// top side piece:
	// 1 1 1
	// 1   1


	if (total_spots == 8)  // center pieces
		room_info.tile_type = Center_Piece;
	else if (total_spots == 5)  // side pieces
	{
		if (flag_left && flag_right && flag_top)
			room_info.tile_type = Top_Side_Piece;
		else if (flag_left && flag_right && flag_bottom)
			room_info.tile_type = Bottom_Side_Piece;
		else if (flag_left && flag_top && flag_bottom)
			room_info.tile_type = Left_Side_Piece;
		else if (flag_right && flag_top && flag_bottom)
			room_info.tile_type = Right_Side_Piece;
	}
	else // corner piece
		room_info.tile_type = Corner_Piece;

	return room_info;
}

int DungeonLayout::AdjacentTiles(int x, int y)
{
	int index = y * width + x;
	int room_count = 0;

	// looking left
	if (index - 1 > -1 && (index) % width != 0)
	{
    if (Layout_[index - 1].RoomType > 0)
			++room_count;
	}

	// looking right
	if (index + 1 < (int) (width * height) && (index + 1) % width != 0)
	{
    if (Layout_[index + 1].RoomType > 0)
			++room_count;
	}

	// looking below
	if (index + width < (width * height))
	{
    if (Layout_[index + width].RoomType > 0)
			++room_count;
	}

	// looking above
	int value = index - width;
	if (value > -1)
	{
    if (Layout_[value].RoomType > 0)
			++room_count;
	}

	return room_count;
}

void DungeonLayout::Reduce_Clumping(void)
{
  for (unsigned i = 0; i < Layout_.size(); ++i)
	{
    if (Layout_[i].RoomType == LAYOUT_NO_ROOM)
			continue; // do nothing with open tiles


		// first is the number of total adjacent open rooms or rooms are bordering
		// second is the number of rooms are bordering excluding 0's; only 1's
    Map_Info tile_info = Count_Adjacent_Rooms(i, Layout_, width, height);


    if (tile_info.num_adjacent_rooms > 4)
    {
      // looks for this:
      // 1 1 1     1 1 1
      // 1 1 1  -> 1 0 1
      // 0 0 0     0 0 0
      if (tile_info.room_tiles[0] && tile_info.room_tiles[4] &&
          tile_info.room_tiles[3] && tile_info.room_tiles[5] &&
          tile_info.room_tiles[1] && tile_info.room_tiles[2] != 1)
      {
        Layout_[i].RoomType = LAYOUT_NO_ROOM;
      }
      // looks for this:
      // 0 1 1     0 1 1
      // 0 1 1  -> 0 0 1
      // 0 1 1     0 1 1
      if (tile_info.room_tiles[3] && tile_info.room_tiles[5] &&
          tile_info.room_tiles[1] && tile_info.room_tiles[2] &&
          tile_info.room_tiles[7] && tile_info.room_tiles[0] != 1)
      {
        Layout_[i].RoomType = LAYOUT_NO_ROOM;
      }

      // looks for this:
      // 0 0 0     0 0 0
      // 1 1 1  -> 1 0 1
      // 1 1 1     1 1 1
      if (tile_info.room_tiles[2] && tile_info.room_tiles[6] &&
          tile_info.room_tiles[7] && tile_info.room_tiles[1] &&
          tile_info.room_tiles[0] && tile_info.room_tiles[3] != 1)
      {
        Layout_[i].RoomType = LAYOUT_NO_ROOM;
      }

      // looks for this:
      // 1 1 0     1 1 0
      // 1 1 0  -> 1 0 0
      // 1 1 0     1 1 0
      if (tile_info.room_tiles[0] && tile_info.room_tiles[4] &&
          tile_info.room_tiles[2] && tile_info.room_tiles[3] &&
          tile_info.room_tiles[6] && tile_info.room_tiles[1] != 1)
      {
        Layout_[i].RoomType = LAYOUT_NO_ROOM;
      }
    }


		if (tile_info.tile_type == Corner_Piece) // corner
		{
			if (tile_info.num_empty_rooms == 2)
        Layout_[i].RoomType = LAYOUT_NO_ROOM;  // during the algorithm a corner piece was left alone; this removes it
		}
		else if (tile_info.adjacent_tiles == 5)  // side piece
		{
			if (tile_info.tile_type == Right_Side_Piece)
			{
				if (tile_info.num_empty_rooms == 1 && tile_info.open_tiles[2] == 0 && tile_info.open_tiles[3] == 0)
				{
					continue;
				}

				//else if (tile_info.num_empty_rooms == 1 && tile_info.open_tiles[0] == 0 && tile_info.open_tiles[3] == 0)
				//{
				//  continue;
				//}
				//
				//else if (tile_info.num_empty_rooms == 1 && tile_info.open_tiles[0] == 0 && tile_info.open_tiles[3] == 0)
				//{
				//  continue;
				//}

				else if (tile_info.num_empty_rooms == 1)
          Layout_[i].RoomType = LAYOUT_NO_ROOM;
			}
			else if (tile_info.tile_type == Left_Side_Piece)
			{
				if (tile_info.num_empty_rooms == 1 && tile_info.open_tiles[2] == 0 && tile_info.open_tiles[3] == 0)
					continue;

				//else if (tile_info.num_empty_rooms == 1 && tile_info.open_tiles[1] == 0 && tile_info.open_tiles[3] == 0)
				//{
				//  continue;
				//}
				//
				//else if (tile_info.num_empty_rooms == 1 && tile_info.open_tiles[1] == 0 && tile_info.open_tiles[2] == 0)
				//{
				//  continue;
				//}

				else if (tile_info.num_empty_rooms == 1)
          Layout_[i].RoomType = LAYOUT_NO_ROOM;
			}
			else if (tile_info.tile_type == Top_Side_Piece)
			{
				if (tile_info.num_empty_rooms == 1 && tile_info.open_tiles[0] == 0 && tile_info.open_tiles[1] == 0)
					continue;


				else if (tile_info.num_empty_rooms == 1)
          Layout_[i].RoomType = LAYOUT_NO_ROOM;
			}
			else // Bottom_Side_Piece
			{
				if (tile_info.num_empty_rooms == 1 && tile_info.open_tiles[0] == 0 && tile_info.open_tiles[1] == 0)
					continue;

				else if (tile_info.num_empty_rooms == 1)
          Layout_[i].RoomType = LAYOUT_NO_ROOM;
			}
		}

		else if (tile_info.tile_type == Center_Piece)
		{
			if (tile_info.num_empty_rooms == 1)
        Layout_[i].RoomType = LAYOUT_NO_ROOM;
		}
	}
}

void DungeonLayout::Remove_Single_Rooms(void)
{
	for (unsigned i = 0; i < height; ++i)
	{
		for (unsigned j = 0; j < width; ++j)
		{
			if (GetRoom(j, i) == LAYOUT_ROOM)
				continue;
			else if (AdjacentTiles(j, i) == 0)
				SetRoom(j, i, LAYOUT_NO_ROOM);
		}
	}
}

Direction DungeonLayout::GetAdjacentTileDirs(int x, int y)
{
	int index = GetIndex(x, y);
	unsigned room_dirs = DIR_NONE;

	// looking left
	if (index - 1 > -1 && (index) % width != 0)
	{
    if (Layout_[index - 1].RoomType != LAYOUT_NO_ROOM)
			room_dirs |= DIR_WEST;
	}

	// looking right
	if (index + 1 < (int) (width * height) && (index + 1) % width != 0)
	{
    if (Layout_[index + 1].RoomType != LAYOUT_NO_ROOM)
			room_dirs |= DIR_EAST;
	}

	// looking below
	if (index + width < (width * height))
	{
    if (Layout_[index + width].RoomType != LAYOUT_NO_ROOM)
			room_dirs |= DIR_SOUTH;
	}

	// looking above
	int value = index - width; // because unsigned
	if (value > -1)
	{
    if (Layout_[value].RoomType != LAYOUT_NO_ROOM)
			room_dirs |= DIR_NORTH;
	}

	return (Direction)room_dirs;
}

void DungeonLayout::Print(void)
{
	cout << endl;
  for (unsigned i = 0; i < Layout_.size(); ++i)
	{
		if (i % width == 0)
			cout << endl;

    cout << Layout_[i].RoomType << ' ';
	}
}

void DungeonLayout::SetAdjacentRoomsVis(int x, int y)
{
  int left_x = x - 1, right_x = x + 1,
    up_y = y + 1, down_y = y - 1;
  Map_Layout* index;

  if (left_x > -1)
  {
    index = &Layout_[GetIndex(left_x, y)];
    index->CurrentRoom = false;
    if (index->Visited == VIS_NOT && index->RoomType != LAYOUT_NO_ROOM)
    {
      index->Visited = VIS_NEXT;
      index->ColorMask = { 0.75f, 0.75f, 0.75f, 1.0f };
    }
  }

  if (right_x < static_cast<signed int>(width))
  {
    index = &Layout_[GetIndex(right_x, y)];
    index->CurrentRoom = false;
    if (index->Visited == VIS_NOT && index->RoomType != LAYOUT_NO_ROOM)
    {
      index->Visited = VIS_NEXT;
      index->ColorMask = { 0.75f, 0.75f, 0.75f, 1.0f };
    }
  }

  if (up_y < static_cast<signed int>(height))
  {
    index = &Layout_[GetIndex(x, up_y)];
    index->CurrentRoom = false;
    if (index->Visited == VIS_NOT && index->RoomType != LAYOUT_NO_ROOM)
    {
      index->Visited = VIS_NEXT;
      index->ColorMask = { 0.75f, 0.75f, 0.75f, 1.0f};
    }
  }

  if (down_y > -1)
  {
    index = &Layout_[GetIndex(x, down_y)];
    index->CurrentRoom = false;
    if (index->Visited == VIS_NOT && index->RoomType != LAYOUT_NO_ROOM)
    {
      index->Visited = VIS_NEXT;
      index->ColorMask = { 0.75f, 0.75f, 0.75f, 1.0f};
    }
  }
}

void DungeonLayout::SetRoomVisited(int x, int y)
{
  Map_Layout* index = &Layout_[GetIndex(x, y)];
  index->Visited = VIS_SEEN;
  index->CurrentRoom = true;
  index->ColorMask = { 1.0f, 1.0f, 1.0f, 1.0f };
  SetAdjacentRoomsVis(x, y);
}



