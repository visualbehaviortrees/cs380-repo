//	File name:		DungeonLayout.h
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//
//
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.
#pragma once

enum Direction
{
	DIR_NONE  = 0,
	DIR_NORTH = 1 << 0,
	DIR_EAST  = 1 << 1,
	DIR_WEST  = 1 << 2,
	DIR_SOUTH = 1 << 3,
	DIR_ALL   = DIR_NORTH | DIR_EAST | DIR_WEST | DIR_SOUTH
};

enum LayoutTile
{
  LAYOUT_NO_ROOM = 0,
  LAYOUT_ROOM,
  LAYOUT_START_ROOM,
  LAYOUT_TREASURE_ROOM,
  LAYOUT_BOSS_ROOM
};

enum Visitation
{
  VIS_NOT = 0,
  VIS_NEXT,
  VIS_SEEN
};

struct Map_Layout
{
  Map_Layout() : RoomType(LAYOUT_NO_ROOM), ColorMask(0.0f, 0.0f, 0.0f), Visited(VIS_NOT), CurrentRoom(false)
  {}

  LayoutTile RoomType;
  Math::Hcoords ColorMask;
  Visitation Visited;
  bool CurrentRoom;
};

class DungeonLayout
{
public:
	DungeonLayout(unsigned width_, unsigned height_)
		: width(width_), height(height_)
	{
    Layout_.resize(width * height);
	}

	void SetRoom(int x, int y, LayoutTile t)
	{
    Layout_[GetIndex(x, y)].RoomType = t;
	}

	void SetRoom(int x, LayoutTile t)
	{
    Layout_[x].RoomType = t;
	}

	LayoutTile& operator[](unsigned int index)
	{
    return Layout_[index].RoomType;
	}

	LayoutTile GetRoom(int x, int y) const
	{
    return Layout_[GetIndex(x, y)].RoomType;
	}

  const Map_Layout* GetRoomInfo(int x, int y) const
  {
    return &Layout_[GetIndex(x, y)];
  }

  void SetRoomVisited(int x, int y);

  Visitation CheckRoomVisited(int x, int y)
  {
    return Layout_[GetIndex(x, y)].Visited;
  }

	unsigned Size(void)
	{
    return Layout_.size();
	}

	void Print_DungeonLayout(void);

	Direction GetAdjacentTileDirs(int x, int y);

  int AdjacentTiles(int x, int y);
	void Reduce_Clumping(void);

	// last step after reducing clumping, goes through the map one more time to clean up any no connected rooms
	void Remove_Single_Rooms(void);

	unsigned GetWidth() const { return width; }
	unsigned GetHeight() const { return height; }

 
	void Print();

private:
	enum Map_Piece
	{
		Corner_Piece = 0,
		Right_Side_Piece,
		Left_Side_Piece,
		Bottom_Side_Piece,
		Top_Side_Piece,
		Center_Piece,
		NO_Piece
	};

	struct Map_Info
	{
		Map_Piece tile_type;
		int adjacent_tiles;
		int num_adjacent_rooms;
		int num_empty_rooms;

		// [0] == left,     [1] == right,     [2] == bottom,      [3] == top
		// [4] == top left, [5] == top right, [6] == bottom left, [7] == bottom right
		int open_tiles[8];

    // [0] == left,     [1] == right,     [2] == bottom,      [3] == top
    // [4] == top left, [5] == top right, [6] == bottom left, [7] == bottom right
    int room_tiles[8];

		Map_Info()
		{
      for (int i = 0; i < 8; ++i)
      {
        open_tiles[i] = 0;
        room_tiles[i] = 0;
      }

			tile_type = NO_Piece;
			adjacent_tiles = 0;
			num_adjacent_rooms = 0;
			num_empty_rooms = 0;
		}
	};

  void SetAdjacentRoomsVis(int x, int y);

	unsigned GetIndex(unsigned x, unsigned y) const
	{
		return y * width + x;
	}

  Map_Info Count_Adjacent_Rooms(int index, const std::vector<Map_Layout>& map, int width, int height);

	unsigned width, height;
  std::vector<Map_Layout> Layout_;
};