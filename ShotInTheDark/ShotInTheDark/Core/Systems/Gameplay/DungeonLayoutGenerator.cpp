//	File name:		DungeonLayoutGenerator.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Kyle Philistine
//
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.
#include "stdafx.h"
#include "DungeonLayoutGenerator.h"


void DungeonLayoutGenerator::Check_For_Rooms_Around(vector<vector<int>>dung_map, vector<bool>& flags, unsigned y_starting_point, unsigned x_starting_point, unsigned dung_height, unsigned dung_width)
{
	if (y_starting_point == 0)
		flags[0] = true; // Can't place UP!

	if (y_starting_point + 1 >= dung_height)
		flags[1] = true; // Can't place DOWN!

	if (x_starting_point == 0)
		flags[2] = true; // Can't place LEFT!

	if (x_starting_point + 1 >= dung_width)
		flags[3] = true; // Can't place RIGHT!


	if (flags[0] == false) // checking if a room is already placed above
	{
		if (dung_map[y_starting_point - 1][x_starting_point] == 1)
			flags[0] = true;
	}

	if (flags[1] == false) // checking if a room is already placed below
	{
		if (dung_map[y_starting_point + 1][x_starting_point] == 1)
			flags[1] = true;
	}

	if (flags[2] == false) // checking if a room is already placed on the left
	{
		if (dung_map[y_starting_point][x_starting_point - 1] == 1)
			flags[2] = true;
	}

	if (flags[3] == false) // checking if a room can be placed on the right
	{
		if (dung_map[y_starting_point][x_starting_point + 1] == 1)
			flags[3] = true;
	}
}


// looks for rows and columns that aren't being used if this is true I
// run the algorithm again because the room will most likely be clumpy
bool DungeonLayoutGenerator::Check_Rows_Columns(DungeonLayout map)
{
	int zero_rooms = 0;
	// checks all rows
	for (unsigned i = 0; i < map.Size(); ++i)
	{

		if (map[i] == LAYOUT_NO_ROOM)
			++zero_rooms;

		if ((i + 1) % map.GetWidth() == 0 && i != 0)
		{
			if (zero_rooms == map.GetWidth())
				return true;
			zero_rooms = 0;
		}
	}

	unsigned column = 0;
	unsigned next_column = 0;
	// checks all columns
	for (unsigned i = 0; i < map.Size(); ++i)
	{

		if (map[column] == LAYOUT_NO_ROOM)
			++zero_rooms;

		column += map.GetWidth();

		if (column > map.Size() - 1)
		{
			++next_column;
			if (zero_rooms == map.GetHeight())
				return true;

			column = next_column;
			zero_rooms = 0;
		}
	}
	return false;
}

std::vector<vector<int>> DungeonLayoutGenerator::GenerateRandomDungeon(unsigned dung_width, unsigned dung_height, unsigned room_width, unsigned room_height)
{
  #define UP    1
  #define DOWN  2
  #define LEFT  3
  #define RIGHT 4

	vector<vector<int>> dung_map;
	dung_map.resize(dung_height);
	for (unsigned i = 0; i < dung_height; ++i)
		dung_map[i].resize(dung_width);

	unsigned num_rooms = dung_width + dung_height + random_generator.range(0, 3);
	unsigned y_starting_point = random_generator.range(0, dung_height - 1);
	unsigned x_starting_point = random_generator.range(0, dung_width - 1);

	std::vector<Point2D> last_spots;


	dung_map[y_starting_point][x_starting_point] = 1;


	vector<bool> flags(4);


	for (unsigned i = 1; i < num_rooms;)
	{
		flags[0] = false; flags[1] = false; flags[2] = false; flags[3] = false;
		Check_For_Rooms_Around(dung_map, flags, y_starting_point, x_starting_point, dung_height, dung_width);

		if (flags[0] && flags[1] && flags[2] && flags[3])
		{
			if (last_spots.empty())
			{
				flags[0] = false; flags[1] = false; flags[2] = false; flags[3] = false;

				y_starting_point = random_generator.range(0, dung_height - 1);
				x_starting_point = random_generator.range(0, dung_width - 1);
				continue;
			}

			y_starting_point = (unsigned)last_spots.back().y;
			x_starting_point = (unsigned)last_spots.back().x;

			if (!last_spots.empty())
				last_spots.pop_back();

			continue;
		}

		switch (random_generator.Roll(4))
		{
		  case UP:
		  {
			  if (flags[0] == false)
			  {
				  last_spots.push_back(Point2D((float)x_starting_point, (float)y_starting_point));
				  y_starting_point -= 1;

				  dung_map[y_starting_point][x_starting_point] = 1;
				  ++i;
				  continue;
			  }
		  } // these are supposed to fall throw

		  case DOWN:
		  {
			  if (flags[1] == false)
			  {
				  last_spots.push_back(Point2D((float)x_starting_point, (float)y_starting_point));
				  y_starting_point += 1;

				  dung_map[y_starting_point][x_starting_point] = 1;
				  ++i;
				  continue;
			  }

		  } // these are supposed to fall throw

		  case LEFT:
		  {
			  if (flags[2] == false)
			  {
				  last_spots.push_back(Point2D((float)x_starting_point, (float)y_starting_point));
				  x_starting_point -= 1;

				  dung_map[y_starting_point][x_starting_point] = 1;
				  ++i;
				  continue;
			  }
		  } // these are supposed to fall throw

		  case RIGHT:
		  {
			  if (flags[3] == false)
			  {
				  last_spots.push_back(Point2D((float)x_starting_point, (float)y_starting_point));
				  x_starting_point += 1;

				  dung_map[y_starting_point][x_starting_point] = 1;
				  ++i;
				  continue;
			  }
		  }
		}
	}
	return dung_map;
}

/*
// the return is the actual dungeon layout not the room.
// 1's are rooms, 0's are non rooms
DungeonLayout DungeonLayoutGenerator::GenerateDungMaze(unsigned dung_width, unsigned dung_height)
{
	DungeonLayout Dung_Layout(dung_width, dung_height);

	int num_rooms = ((dung_width * dung_height) / 2) + random_generator.range(1, 3);

	int num_rooms_placed = 0;

	for (unsigned int i = 0; i < Dung_Layout.Size(); ++i)
	{
		int roll = random_generator.Roll(4);

		if (roll != 4)
		{
			Dung_Layout[i] = LAYOUT_ROOM;
			++num_rooms_placed;
		}

		if (num_rooms_placed > num_rooms)
			break;


		// back up incase the chance I don't get a dungeon with enough rooms
		// or I get the chance no rooms are added highly unlikely its a 1 in 4 chance a room isn't placed
		// keep placing random rooms until I finally get enough rooms
		if (i == Dung_Layout.Size() - 1 && num_rooms_placed < num_rooms)
		{
			while (num_rooms_placed < num_rooms)
			{
				int position = random_generator.range(0, (dung_height * dung_width) - 1);

				if (Dung_Layout[position] == LAYOUT_ROOM)
					continue;
				else
					Dung_Layout[position] = LAYOUT_ROOM;

				++num_rooms_placed;
			}
		}
	}



	Dung_Layout.Print();
	return Dung_Layout;
}
*/

void DungeonLayoutGenerator::Place_Starting_Room(DungeonLayout& map)
{
    // first = position in map, second = number of rooms it borders
  vector<pair<int, int>> rooms;
  for (int adj_rooms = 4; adj_rooms > 1; --adj_rooms)
  {
    for (unsigned i = 0; i < map.GetHeight(); ++i)
    {
      for (unsigned j = 0; j < map.GetWidth(); ++j)
      {
        if (map.GetRoom(j, i) != LAYOUT_ROOM)
          continue;

        int room_count = map.AdjacentTiles(j, i);
        if (adj_rooms == room_count)
          rooms.push_back(make_pair(i*map.GetWidth() + j, room_count));
      }
    }
    if (rooms.size() != 0)
      break;
  }

  int pick_starting_pos = random_generator.range(0, (int)(rooms.size() - 1));
  map.SetRoom(rooms[pick_starting_pos].first, LAYOUT_START_ROOM);
}

  // this can potentially fail if the rooms are really bad
bool DungeonLayoutGenerator::Place_Unique_Rooms(DungeonLayout& map, unsigned treasure_rooms, unsigned boss_rooms)
{
  // first = position in map, second = number of rooms it borders
  vector<pair<int, int>> rooms;
  for (int adj_rooms = 4; adj_rooms > 1; --adj_rooms)
  {
    for (unsigned i = 0; i < map.GetHeight(); ++i)
    {
      for (unsigned j = 0; j < map.GetWidth(); ++j)
      {
        if (map.GetRoom(j, i) != LAYOUT_ROOM)
          continue;

        int room_count = map.AdjacentTiles(j, i);
        if (adj_rooms == room_count)
          rooms.push_back(make_pair(i*map.GetWidth() + j, room_count));
      }
    }
    if (rooms.size() != 0)
      break;
  }
  if (rooms.size() == 0)
    return false;

  int pick_starting_pos = random_generator.range(0, (int)(rooms.size() - 1));
  map.SetRoom(rooms[pick_starting_pos].first, LAYOUT_START_ROOM);

  rooms.clear();



    // treasure room / boss room
  for (int adj_rooms = 1; adj_rooms < 2; ++adj_rooms)
  {
    for (unsigned i = 0; i < map.GetHeight(); ++i)
    {
      for (unsigned j = 0; j < map.GetWidth(); ++j)
      {
        if (map.GetRoom(j, i) != LAYOUT_ROOM)
          continue;

        int room_count = map.AdjacentTiles(j, i);
        if (adj_rooms == room_count)
          rooms.push_back(make_pair(i*map.GetWidth() + j, room_count));
      }
    }
    if (rooms.size() != 0)
      break;
  }
  if (rooms.size() < 3)
    return false;

  unsigned num_treasure = treasure_rooms;
  unsigned num_boss = boss_rooms;

    // too many uniques for the number of rooms
  if ((int)rooms.size() - (int)num_treasure - (int)num_boss < 0)
  {
    num_treasure = 0;
    num_boss = 0;
    for (int i = 0; i < (int)(rooms.size() / 2); ++i)
    {
      if (num_treasure < treasure_rooms)
        ++num_treasure;

      if (num_boss < boss_rooms)
        ++num_boss;
    }
      // left overs
    if (num_treasure + num_boss < rooms.size())
    {

      if (treasure_rooms > boss_rooms && treasure_rooms != num_treasure)
      {
        num_treasure += rooms.size() - num_treasure - num_boss;
      }
      else
      {
        num_boss += rooms.size() - num_treasure - num_boss;
      }
    }
  }

  for (unsigned i = 0; i < num_treasure; ++i)
  {
    int pick_treasure_room = random_generator.range(0, (int)(rooms.size() - 1));
    map.SetRoom(rooms[pick_treasure_room].first, LAYOUT_TREASURE_ROOM);
    rooms.erase(rooms.begin() + pick_treasure_room);
  }

  for (unsigned i = 0; i < num_boss; ++i)
  {
    int num_retries = 0;
    pick_again:
      int pick_boss_room = random_generator.range(0, (int)(rooms.size() - 1));

      // looking left
      if (rooms[pick_boss_room].first - 1 > -1 && (rooms[pick_boss_room].first) % map.GetWidth() != 0)
      {
        if (map[rooms[pick_boss_room].first - 1] == LAYOUT_START_ROOM)
        {
          if (rooms.size() == 1)
            return false;
          else
          {
            if (num_retries == 3)
              return false;
            ++num_retries;
            goto pick_again;
          }
        }
      }

      // looking right
      if (rooms[pick_boss_room].first + 1 < (int)(map.GetWidth() * map.GetHeight()) && (rooms[pick_boss_room].first + 1) % map.GetWidth() != 0)
      {
        if (map[rooms[pick_boss_room].first + 1] == LAYOUT_START_ROOM)
        {
          if (rooms.size() == 1)
            return false;
          else
          {
            if (num_retries == 3)
              return false;
            ++num_retries;
            goto pick_again;
          }
        }
      }

      // looking below
      if (rooms[pick_boss_room].first + map.GetWidth() < (map.GetWidth() * map.GetHeight()))
      {
        if (map[rooms[pick_boss_room].first + map.GetWidth()] == LAYOUT_START_ROOM)
        {
          if (rooms.size() == 1)
            return false;
          else
          {
            if (num_retries == 3)
              return false;
            ++num_retries;
            goto pick_again;
          }
        }
      }

      // looking above
      int value = rooms[pick_boss_room].first - map.GetWidth();
      if (value > -1)
      {
        if (map[value] == LAYOUT_START_ROOM)
        {
          if (rooms.size() == 1)
            return false;
          else
          {
            if (num_retries == 3)
              return false;
            ++num_retries;
            goto pick_again;
          }
        }
      }

      map.SetRoom(rooms[pick_boss_room].first, LAYOUT_BOSS_ROOM);
      rooms.erase(rooms.begin() + pick_boss_room);
  }

  return true;
}


DungeonLayout DungeonLayoutGenerator::MazeAlgorithm(unsigned dung_width, unsigned dung_height, bool reduce_clumping, 
                                                    unsigned treasure_rooms, unsigned boss_rooms)
{
	if ((dung_width == 1 && dung_height == 1) || (dung_width == 0 && dung_height == 0) ||
	   	 dung_width == 1 && dung_height == 0  ||  dung_width == 0 && dung_height == 1)
	{
		if (dung_width == 1 && dung_height == 1)
		{
			DungeonLayout map(dung_width, dung_height);

			map[0] = LAYOUT_START_ROOM;
			return map;
		}
		else
			assert(!"MAZE ALGORITHM, DUNGEON DIMENSIONS ARE WRONG");
	}

restart:
	DungeonLayout Dung_Layout(dung_width, dung_height);
	int starting_location_x = random_generator.range(0, dung_width - 1);
	int starting_location_y = random_generator.range(0, dung_height - 1);
	int num_rooms = ((dung_width * dung_height) / 2) + random_generator.range(1, 3);
	Dung_Layout.SetRoom(starting_location_x, starting_location_y, LAYOUT_ROOM);


	for (int i = 0; i < num_rooms; ++i)
	{
		while (1)
		{
			int rand_room_location_x = random_generator.range(0, dung_width - 1);
			int rand_room_location_y = random_generator.range(0, dung_height - 1);

			if (Dung_Layout.GetRoom(rand_room_location_x, rand_room_location_y) == LAYOUT_NO_ROOM)
			{
				int adjacent_rooms = Dung_Layout.AdjacentTiles(rand_room_location_x, rand_room_location_y);
				if (adjacent_rooms < 3 && adjacent_rooms > 0)
				{
					Dung_Layout.AdjacentTiles(rand_room_location_x, rand_room_location_y);
					Dung_Layout.SetRoom(rand_room_location_x, rand_room_location_y, LAYOUT_ROOM);
					break;
				}
			}
		}
	}

	if (reduce_clumping)
	{
		Dung_Layout.Reduce_Clumping();
		Dung_Layout.Remove_Single_Rooms();

      // makes sure that ever row and column has a room on it, makes it less clumpy sometimes; it has a better chance
		if (Check_Rows_Columns(Dung_Layout))
		{
			goto restart;
		}
    //Place_Starting_Room(Dung_Layout);
    if (!Place_Unique_Rooms(Dung_Layout, treasure_rooms, boss_rooms))
      goto restart;

    Dung_Layout.Print();
		cout << endl;
		return Dung_Layout;
	}
  else
    return Dung_Layout;
}
