//	File name:		DungeonLayoutGenerator.h
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.
#pragma once
#include "DungeonLayout.h"
#include "Libraries\RNG\RNG.hpp"
#include "../../Libraries/ADLib/StopWatch.hpp"
#include <ctime>

class DungeonLayoutGenerator
{
public:
  DungeonLayoutGenerator() : random_generator()
  {
    #ifdef _DEBUG
      //random_generator;//(int)time(0); //std::chrono::system_clock::now();
    #else
      random_generator = (int)time(0); //std::chrono::system_clock::now();
    #endif
  };

	// somewhat clumpy
	// makes a random dungeon layout that isn't square, kinda like a maze layout
	// takes the size of the dungeon height and width to make randomly scaling dungeons
	// room_width and height are for the actual sizes of each room
	std::vector<vector<int>> GenerateRandomDungeon(unsigned dung_width, unsigned dung_height, unsigned room_width, unsigned room_height);

	//DungeonLayout GenerateDungMaze(unsigned dung_width, unsigned dung_height);
	DungeonLayout MazeAlgorithm(unsigned dung_width, unsigned dung_height, bool reduce_clumping = true, unsigned treasure_rooms = 1, unsigned boss_rooms = 1);

private:
  void Place_Starting_Room(DungeonLayout& map);
  bool Place_Unique_Rooms(DungeonLayout& map, unsigned treasure_rooms = 1, unsigned boss_rooms = 1);
	void Check_For_Rooms_Around(vector<vector<int>>dung_map, vector<bool>& flags, unsigned y_starting_point, unsigned x_starting_point,
                              unsigned dung_height, unsigned dung_width);
	bool Check_Rows_Columns(DungeonLayout map);
	RNG random_generator;
};