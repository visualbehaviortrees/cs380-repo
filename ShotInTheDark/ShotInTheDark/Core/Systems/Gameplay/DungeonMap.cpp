//	File name:		DungeonMap.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "DungeonMap.h"

// Tiles that the AI will pathfind around
const DungeonTile COLLISION_TILES[] = {
	TILE_WALL,
	TILE_TORCH,
	TILE_PIT,
	TILE_BREAKABLE_WALL,
	TILE_DOOR
};


std::vector<int> DungeonMap::GetRawCollisionMap() const
{
	std::vector<int> collision_tiles(width * height, 0);
	for (size_t i = 0; i < mapData.size(); ++i)
	{
		// Check if the tile is a collision tile
		for (size_t tile = 0; tile < sizeof(COLLISION_TILES) / sizeof(*COLLISION_TILES); ++tile)
		{
			if (mapData[i] == tile)
			{
				collision_tiles[i] = 1;
			}
			else
			{
				collision_tiles[i] = 0;
			}
		}
	}

	return collision_tiles;
}

void DungeonMap::Print_DungeonMap(void)
{

	cout << endl;
	for (unsigned i = 0; i < mapData.size(); ++i)
	{
		if (i % width == 0)
			cout << endl;

		cout << mapData[i] << ' ';
	}
}