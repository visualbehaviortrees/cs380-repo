//	File name:		DungeonMap.h
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.
#pragma once

enum DungeonTile
{
	TILE_OPEN = 0,
	TILE_WALL = 1,
	TILE_TORCH = 2,
	TILE_ENEMY = 3,
	TILE_BREAKABLE_WALL = 4,
	TILE_TREASURE = 5,
	TILE_PICKUP,
	TILE_DOOR,
	TILE_PLAYER,
	MAX_TILE_TYPES,
	// NOT YET USED
	TILE_PIT
};

class DungeonMap
{
public:
	DungeonMap(unsigned width_ = 0, unsigned height_ = 0)
		: width(width_), height(height_)
	{
		mapData.resize(width * height);
	}

	void SetTile(int x, int y, DungeonTile t)
	{
		mapData[GetIndex(x, y)] = t;
	}

	void SetTile(int x, DungeonTile t)
	{
		mapData[x] = t;
	}

	DungeonTile& operator[](unsigned int index)
	{
		return mapData[index];
	}

	DungeonTile GetTile(int x, int y) const
	{
		return mapData[GetIndex(x, y)];
	}

	unsigned Size(void)
	{
		return mapData.size();
	}

	void Print_DungeonMap(void);

	// last step after reducing clumping, goes through the map one more time to clean up any no connected rooms
	void Remove_Single_Rooms(void);

	unsigned GetWidth() const { return width; }
	unsigned GetHeight() const { return height; }

	std::vector<int> GetRawCollisionMap() const;

private:
	unsigned GetIndex(unsigned x, unsigned y) const
	{
		return y * width + x;
	}
	unsigned width, height;
	std::vector<DungeonTile> mapData;
};