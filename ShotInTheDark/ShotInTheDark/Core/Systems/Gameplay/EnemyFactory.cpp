//	File name:		EnemyFactory.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include <stdafx.h>
#include "EnemyFactory.h"
#include "DungeonGameplay.h"

void EnemyFactory::Init(void)
{
	REGISTER_COMPONENT(Behavior);
	REGISTER_COMPONENT(Sprite);
	REGISTER_COMPONENT(RigidBody);


	RegisterEnemy("Strawberry", CreateStrawberry);
	RegisterEnemy("BigStrawberry", CreateBigStrawberry);
	RegisterEnemy("Bacon", CreateBacon);
	RegisterEnemy("Muffin", CreateZombie);

	// SetSpawnList(1);
}

void EnemyFactory::RegisterEnemy(std::string EnemyName, CreateEnemyFunction fn)
{
	EnemyCreationMap[EnemyName] = fn;
}

Entity* EnemyFactory::CreateEnemy(std::string name, Point2D position)
{
	// Check for a valid string
	if (EnemyCreationMap.find(name) == EnemyCreationMap.end())
	{
		std::cout << "Failed to create enemy " << name << "!!! \n";
		return 0;
	}
	else
		return EnemyCreationMap[name](position);
}

Entity* EnemyFactory::CreateRandomEnemy(Point2D position)
{
	return CreateEnemy(spawnList.Spawn(random_generator), position);
	//int enemy_number = random_generator.Roll(EnemyCreationMap.size());
	//auto it = EnemyCreationMap.begin();
	//for (int i = 0; i < enemy_number; ++i)
	//	++it;

	//return it->second(position);
}

void EnemyFactory::SendMsg(Entity * a, Entity * b, message m)
{
	DungeonGameplay* dm;
	switch (m)
	{
	case MSG_Clear_Dungeon:
		dm = dynamic_cast<DungeonGameplay*>(ENGINE->getCurrentGamestate());
		if (dm != nullptr)
			SetSpawnList(dm->GetCurrentLevel());
	default:
		break;
	}
}

void EnemyFactory::Update(float dt)
{
	for (Entity * e : _entities)
	{
		UpdateAnimations(e);
	}
}

void EnemyFactory::UpdateAnimations(Entity * e)
{
	Sprite* sprite = e->GET_COMPONENT(Sprite);
	Behavior* behavior = e->GET_COMPONENT(Behavior);

	Vector2D dir = e->GET_COMPONENT(RigidBody)->direction;

	if (dir.SquareLength() <= 1.0f)
		return;

	switch (behavior->enemyType)
	{
	case ET_MAGE:
		// Right
		if (dir.x > 0.0f && dir.x > fabs(dir.y))
			sprite->ChangeTexture("Strawberry_Move_Right_1.png");
		// Left
		else if (dir.x < 0.0f && -dir.x > fabs(dir.y))
			sprite->ChangeTexture("Strawberry_Move_Left_1.png");
		// Back/Up
		else if (dir.y > 0.0f)
			sprite->ChangeTexture("Strawberry_Move_Backward_1.png");
		// Forward/Down
		else if (dir.y < 0.0f)
			sprite->ChangeTexture("Strawberry_Move_Forward_1.png");
		break;

	case ET_THEGUY:
		// Right
		if (dir.x > 0.0f && dir.x > fabs(dir.y))
			sprite->ChangeTexture("Guy_Move_Right_Base.png");
		// Left
		else if (dir.x < 0.0f && -dir.x > fabs(dir.y))
			sprite->ChangeTexture("Guy_Move_Left_Base.png");
		// Back/Up
		else if (dir.y > 0.0f)
			sprite->ChangeTexture("Guy_Move_Backward_Base.png");
		// Forward/Down
		else if (dir.y < 0.0f)
			sprite->ChangeTexture("Guy_Move_Forward_Base.png");
		break;

	case ET_JUGGERNAUT:
		// Right
		if (dir.x > 0.0f && dir.x > fabs(dir.y))
			sprite->ChangeTexture("Muffin_Move_Right.png");
		// Left
		else if (dir.x < 0.0f && -dir.x > fabs(dir.y))
			sprite->ChangeTexture("Muffin_Move_Left.png");
		// Back/Up
		else if (dir.y > 0.0f)
			sprite->ChangeTexture("Muffin_Move_Backward.png");
		// Forward/Down
		else if (dir.y < 0.0f)
			sprite->ChangeTexture("Muffin_Move_Forward.png");
		break;


	default:
		break;
	}
}

std::string EnemyFactory::EnemySpawnList::Spawn(RNG& gen)
{
	// Calculate the total weight of all our enemies
	int totalWeight = 0;
	for (EnemySpawnOdds s : spawnList)
		totalWeight += s.weight;

	// Roll a number somewhere in that total weight
	int roll = gen.Roll(totalWeight);
	
	// Go through our spawn list, decreasing our roll until we find the corresponding enemy
	for (EnemySpawnOdds s : spawnList)
	{
		roll -= s.weight;
		if (roll <= 0)
			return s.name;
	}

	return "BAD ROLL???";
}

EnemyFactory::EnemySpawnList EnemyFactory::SetSpawnList(int level)
{
	EnemySpawnList spawn;
	switch (level)
	{
	case 1:
		spawn.AddSpawn("Strawberry", 10);
		spawn.AddSpawn("BigStrawberry", 1);
		break;

	case 2:
		spawn.AddSpawn("Strawberry", 5);
		spawn.AddSpawn("BigStrawberry", 1);
		spawn.AddSpawn("Muffin", 2);
		break;
	default:
	case 3:
		spawn.AddSpawn("Strawberry", 3);
		spawn.AddSpawn("BigStrawberry", 1);
		spawn.AddSpawn("Muffin", 3);
		spawn.AddSpawn("Bacon", 1);
		break;
	}

	spawnList = spawn;
	return spawn;
}

void EnemyFactory::EnemySpawnList::AddSpawn(std::string name, int weight)
{
	spawnList.push_back(EnemySpawnOdds{ name, weight });
}

Entity* CreateStrawberry(Point2D pos)
{
	// Add an enemy at a random location on the map
	Transform* enemyTran = new Transform();
		enemyTran->Position = pos;
		enemyTran->rotation = 0.0f;
		enemyTran->scale.Set(0.5f, 0.5f);
	RigidBody* enemyRBody = new RigidBody();
	enemyRBody->direction.Set(0.0f, 0.0f);
	enemyRBody->isStatic = false;
	enemyRBody->speed = 3.0f;
	enemyRBody->ghost = false;
	Drawable* enemyDraw = new Drawable();
	*enemyDraw = Drawable::RedScheme;
	enemyDraw->layer = 3;
	enemyDraw->mpMesh = Mesh::Get_Mesh("Strawberry");
	Sprite* enemySprite = new Sprite("Strawberry_Move_Right_1.png");
	Team *enemyTeam = new Team();
	enemyTeam->teamID = Team_Enemy;
	enemyTeam->specialFriends.push_back(Team_Environment);

	Behavior *enemyBehavior = new Behavior(ET_MAGE);
	Movement *enemyMovement = new Movement;

	Health *enemyHP = new Health();
	enemyHP->maxHealth = 3.0f;
	enemyHP->health = enemyHP->maxHealth;
	enemyHP->regen = 0.1f;
	Bullet* enemyBullet = new Bullet();
	enemyBullet->lifetime = 999999.0f;
	enemyBullet->damage = 1.5f;
	enemyBullet->pierces = true;
	enemyBullet->effects.push_back(Bullet_Effect_DestroyOnDamage);
	enemyBullet->particle_info.count = 200;
	enemyBullet->particle_info.color = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);

	Light* enemyLight = new Light(5.0f, Color( 0.0f, 0.0f, 0.0f , 1.0f));

	Audio* enemyAudio = new Audio();
	// enemyAudio->audioMap[Audio::AE_ATTACK] = "torchExtinguish";
	enemyAudio->audioMap[Audio::AE_HURT] = "strawberry/pain";
	enemyAudio->audioMap[Audio::AE_DIE] = "strawberry/death";

	Entity *enemyEntity = ENGINE->Factory->Compose(11,
		enemyTran,
		enemyRBody,
		enemyDraw,
		enemyHP,
		enemyBehavior,
		enemyMovement,
		enemyTeam,
		enemySprite,
		enemyBullet,
		enemyLight,
		enemyAudio
		);
	enemyEntity->SetName(std::string("Enemy"));

	enemyMovement->PreInit(enemyEntity);

	return enemyEntity;
}

Entity* CreateBigStrawberry(Point2D pos)
{
	// Add an enemy at a random location on the map
	Transform* enemyTran = new Transform();
	enemyTran->Position = pos;
	enemyTran->rotation = 0.0f;
	enemyTran->scale.Set(1.0f, 1.0f);
	RigidBody* enemyRBody = new RigidBody();
	enemyRBody->direction.Set(0.0f, 0.0f);
	enemyRBody->isStatic = false;
	enemyRBody->speed = 1.0f;
	enemyRBody->ghost = false;
	Drawable* enemyDraw = new Drawable();
	*enemyDraw = Drawable::RedScheme;
	enemyDraw->layer = 3;
	enemyDraw->mpMesh = Mesh::Get_Mesh("Strawberry");
	Sprite* enemySprite = new Sprite("Strawberry_Move_Right_1.png");
	Team *enemyTeam = new Team();
	enemyTeam->teamID = Team_Enemy;
	enemyTeam->specialFriends.push_back(Team_Environment);
	Behavior *enemyBehavior = new Behavior(ET_MAGE);
	Movement *enemyMovement = new Movement;

	Health *enemyHP = new Health();
	enemyHP->maxHealth = 5.0f;
	enemyHP->health = enemyHP->maxHealth;
	enemyHP->regen = 0.1f;
	Bullet* enemyBullet = new Bullet();
	enemyBullet->lifetime = 999999.0f;
	enemyBullet->damage = 3.0f;
	enemyBullet->pierces = true;
	enemyBullet->particle_info.count = 200;
	enemyBullet->particle_info.color = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);

	Light* enemyLight = new Light(5.0f, Color(0.0f, 0.0f, 0.0f, 1.0f));

	Audio* enemyAudio = new Audio();
	enemyAudio->audioMap[Audio::AE_HURT] = "bigStrawberry/pain";
	enemyAudio->audioMap[Audio::AE_DIE] = "bigStrawberry/death";

	Entity *enemyEntity = ENGINE->Factory->Compose(11,
		enemyTran,
		enemyRBody,
		enemyDraw,
		enemyHP,
		enemyBehavior,
		enemyMovement,
		enemyTeam,
		enemySprite,
		enemyBullet,
		enemyLight,
		enemyAudio
		);
	enemyEntity->SetName(std::string("Enemy"));

	enemyMovement->PreInit(enemyEntity);

	return enemyEntity;
}

Entity* CreateBacon(Point2D pos)
{
	// Add an enemy at a random location on the map
	Transform* enemyTran = new Transform();
	enemyTran->Position = pos;
	enemyTran->rotation = 0.0f;
	enemyTran->scale.Set(0.75f, 0.75f);
	RigidBody* enemyRBody = new RigidBody();
	enemyRBody->direction.Set(0.0f, 0.0f);
	enemyRBody->isStatic = false;
	enemyRBody->speed = 6.0f;
	enemyRBody->ghost = false;
	Drawable* enemyDraw = new Drawable();
	*enemyDraw = Drawable::RedScheme;
	enemyDraw->layer = 3;
	enemyDraw->mpMesh = Mesh::Get_Mesh("Strawberry");
	Sprite* enemySprite = new Sprite("Guy_Move_Forward_Base.png");
	Team *enemyTeam = new Team();
	enemyTeam->teamID = Team_Enemy;
	enemyTeam->specialFriends.push_back(Team_Environment);


	Behavior *enemyAI = new Behavior(ET_THEGUY);
	Movement* enemyMovement = new Movement;

	Health *enemyHP = new Health();
	enemyHP->maxHealth = 5.0f;
	enemyHP->health = enemyHP->maxHealth;
	enemyHP->regen = 0.0f;
	Bullet* enemyBullet = new Bullet();
	enemyBullet->lifetime = 999999.0f;
	enemyBullet->damage = 1.0f;
	enemyBullet->pierces = true;
	enemyBullet->particle_info.count = 200;
	enemyBullet->particle_info.color = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);

	Light* enemyLight = new Light(5.0f, Color(0.0f, 0.0f, 0.0f, 1.0f));

	Audio* enemyAudio = new Audio();
	enemyAudio->audioMap[Audio::AE_ATTACK] = "torchExtinguish";
	enemyAudio->audioMap[Audio::AE_DIE] = "bacon/death";

	Entity *enemyEntity = ENGINE->Factory->Compose(11,
		enemyTran,
		enemyRBody,
		enemyDraw,
		enemyHP,
		enemyAI,
		enemyMovement,
		enemyTeam,
		enemySprite,
		enemyBullet,
		enemyLight,
		enemyAudio
		);
	enemyEntity->SetName(std::string("Bacon Enemy"));

	enemyMovement->PreInit(enemyEntity);

	return enemyEntity;
}

Entity* CreateZombie(Point2D pos)
{
	// Add an enemy at a random location on the map
	Transform* enemyTran = new Transform();
	enemyTran->Position = pos;
	enemyTran->rotation = 0.0f;
	enemyTran->scale.Set(0.5f, 0.5f);
	RigidBody* enemyRBody = new RigidBody();
	enemyRBody->direction.Set(0.0f, 0.0f);
	enemyRBody->isStatic = false;
	enemyRBody->speed = 1.0f;
	enemyRBody->ghost = false;
	Drawable* enemyDraw = new Drawable();
	*enemyDraw = Drawable::RedScheme;
	enemyDraw->layer = 3;
	enemyDraw->mpMesh = Mesh::Get_Mesh("Strawberry");
	Sprite* enemySprite = new Sprite("Muffin_Move_Right.png");
	Team *enemyTeam = new Team();
	enemyTeam->teamID = Team_Enemy;

	Behavior *enemyAI = new Behavior(ET_JUGGERNAUT);
	Movement* enemyMovement = new Movement;

	Health *enemyHP = new Health();
	enemyHP->maxHealth = 7.0f;
	enemyHP->health = enemyHP->maxHealth;
	enemyHP->regen = 1.0f;
	Bullet* enemyBullet = new Bullet();
	enemyBullet->lifetime = 999999.0f;
	enemyBullet->damage = 3.0f;
	enemyBullet->pierces = true;
	enemyBullet->particle_info.count = 200;
	enemyBullet->particle_info.color = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);

	Light* enemyLight = new Light(5.0f, Color(0.0f, 0.0f, 0.0f, 1.0f));

	Audio* enemyAudio = new Audio();
	enemyAudio->audioMap[Audio::AE_HURT] = "muffin/pain";
	enemyAudio->audioMap[Audio::AE_ATTACK] = "muffin/attack";
	enemyAudio->audioMap[Audio::AE_DIE] = "muffin/death";

	Entity *enemyEntity = ENGINE->Factory->Compose(11,
		enemyTran,
		enemyRBody,
		enemyDraw,
		enemyHP,
		enemyAI,
		enemyMovement,
		enemyTeam,
		enemySprite,
		enemyBullet,
		enemyLight,
		enemyAudio
		);
	enemyEntity->SetName(std::string("Enemy"));

	enemyMovement->PreInit(enemyEntity);

	return enemyEntity;
}
