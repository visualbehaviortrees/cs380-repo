//	File name:		EnemyFactory.h
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.


#pragma once
#include "../Core/Libraries/RNG/RNG.hpp"

class EnemyFactory : public System
{
public:
	EnemyFactory(void) : System(sys_EnemyFactory, "EnemyFactory") {}
	void Init(void);
	void Update(float dt);
	void Shutdown(void){};
	
	void SendMsg(Entity * a, Entity * b, message m);


	Entity* CreateEnemy(std::string name, Point2D position);
	Entity* CreateRandomEnemy(Point2D position);

	typedef Entity*(*CreateEnemyFunction)(Point2D pos);
	void RegisterEnemy(std::string EnemyName, CreateEnemyFunction fn);

private:
	struct EnemySpawnList
	{
		std::string Spawn(RNG& gen);
		void AddSpawn(std::string name, int weight);
		struct EnemySpawnOdds
		{
			std::string name;
			int weight;
		};
	private:
		std::vector<EnemySpawnOdds> spawnList;
	};

	void UpdateAnimations(Entity* e);
	EnemySpawnList SetSpawnList(int level);

	RNG random_generator;
	EnemySpawnList spawnList;
	std::map <std::string, CreateEnemyFunction > EnemyCreationMap;
};


// Enemy Creation Functions
Entity* CreateStrawberry(Point2D pos);
Entity* CreateBigStrawberry(Point2D pos);
Entity* CreateZombie(Point2D pos);
Entity* CreateBacon(Point2D pos);
