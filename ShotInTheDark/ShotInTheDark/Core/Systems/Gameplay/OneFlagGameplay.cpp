//	File name:		OneFlagGameplay.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "../Input/InputSystem.hpp"
#include "OneFlagGameplay.hpp"
#include "../../Engine.hpp"
#include "../../Components/Components.h"
#include "../Input/InputSystem.hpp"
#include "../Physics/Collision.hpp"
extern Engine* ENGINE;
static bool flagExists = false;
static const unsigned OneFlagCapturePoints = 5;

OneFlagGameplay::OneFlagGameplay(): System(sys_OneFlagGameplay, "OneFlag Gameplay System")
{


}

OneFlagGameplay::~OneFlagGameplay()
{


}

void OneFlagGameplay::Init(void)
{
  REGISTER_COMPONENT(Flag);
  REGISTER_COMPONENT(Transform);
  REGISTER_COMPONENT(Drawable);

  if(!flagExists)
  {
   

    Team* team = new Team(Team_NONE);
    Drawable *DrawFlag = new Drawable(true,false,0.f,0.f,0.f,1.0f,10,true,true,0.8f,0.8f,0.8f,1.0f);
    DrawFlag->mpMesh = Mesh::Get_Mesh("Flag");
    Transform *flagTrans = new Transform();
    flagTrans->scale.Set(1.4f,1.4f);
    flagTrans->Position.Set(0.0f,0.0f);
    RigidBody *flagRigid = new RigidBody();
    flagRigid->isStatic = false;
    flagRigid->rotationLocked = true;
    flagRigid->vel.Set(0,0);
    Flag* flagcmp = new Flag();
    Parent *parent = new Parent();
    Entity * theFlag = ENGINE->Factory->Compose(6,parent,team,DrawFlag,flagTrans,flagRigid,flagcmp);
    theFlag->SetName(std::string("Neutral Flag"));

  }

}

void OneFlagGameplay::Update(float dt)
{
  for(auto it : _entities)
  {
    std::vector<Entity *> objects = ENGINE->OM->FilterEntities(MC_Player | MC_Transform);

    // First we set the position of the flag to be either at the flag stand or the player holding it.
    {
      if(it->GET_COMPONENT(Team)->teamID == Team_NONE)
      {
        if(it->GET_COMPONENT(Parent)->parent == NULL)
        {
          // @@TODO: set it to a flag base
          //it->GET_COMPONENT(Transform)->Position.Set(0.f,0.f);
          for(auto player : objects) 
          {
           /* if(player->GET_COMPONENT(Player)->alive)
            {
              if(CollisionCheck(it,player,Math::Vector2D()))
              {
                it->GET_COMPONENT(Parent)->parent = player;
                it->GET_COMPONENT(Transform)->Position = player->GET_COMPONENT(Transform)->Position;
                break;
              }
            }-- Players are always alive now -- Luc */ 
          }
        }
        else
        {
          it->GET_COMPONENT(Transform)->Position = it->GET_COMPONENT(Parent)->parent->GET_COMPONENT(Transform)->Position;
           // next we reset the flag and add to the correct score if the holder returns the flag to his base!
          objects = ENGINE->OM->FilterEntities(MC_Base);
          for(auto bases: objects)
          {
            if(CollisionCheck(it, bases, Math::Vector2D()))
            {
              if(it->GET_COMPONENT(Parent)->parent->GET_COMPONENT(Team)->teamID == bases->GET_COMPONENT(Team)->teamID)
              {
                // @@TODO: some logic with scores
                // @@PAST ME: Thanks for pointing this out clearly for me, you're a cool guy.
                // it->GET_COMPONENT(Parent)->parent->GET_COMPONENT(Player)->score += OneFlagCapturePoints;
                // std::cout << it->GET_COMPONENT(Parent)->parent->GET_COMPONENT(Player)->score << std::endl;
                
                //reinterpret_cast<AudioSystem *>(ENGINE->GetSystem(sys_AudioSystem))->PlaySound(SFX::FLAG_CAP);
                /*********************************/
                it->GET_COMPONENT(Parent)->parent = NULL;
                it->GET_COMPONENT(Transform)->Position.Set(0.0f,-.02f);
              }
            }
          }
          if(it->GET_COMPONENT(Parent)->parent)
          {
            //if(!it->GET_COMPONENT(Parent)->parent->GET_COMPONENT(Player)->alive)
            //{
            //  it->GET_COMPONENT(Parent)->parent = NULL;
            //  reinterpret_cast<AudioSystem *>(ENGINE->GetSystem(sys_AudioSystem))->PlaySound(SFX::FLAG_DROP);
            //  //it->GET_COMPONENT(Transform)->Position.Set(0.0f,0.0f);
            //}
          }
        }
      } // if no team
    }
  }
}

void OneFlagGameplay::Shutdown(void)
{
  auto flags = ENGINE->OM->FilterEntities(MC_Flag | MC_Team);
  for(auto it : flags)
    if(it->GET_COMPONENT(Team)->teamID == Team_NONE)
      ENGINE->OM->DestroyEntity(it);
  flagExists = false;

}

