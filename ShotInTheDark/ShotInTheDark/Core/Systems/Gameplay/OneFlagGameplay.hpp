//	File name:		OneFlagGameplay.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../../System.hpp"

class OneFlagGameplay : public System
{
public:
  OneFlagGameplay();
  ~OneFlagGameplay();
  void Init(void);
  void Update(float dt);
  void Shutdown(void);

private:

};