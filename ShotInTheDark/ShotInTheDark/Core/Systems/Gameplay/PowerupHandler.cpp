//	File name:		Powerups.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "../Input/InputSystem.hpp"
#include "PowerupHandler.hpp"
#include "../../Engine.hpp"
#include "../../Components/Components.h"
#include "../Input/InputSystem.hpp"
#include "../Physics/Collision.hpp"

extern Engine* ENGINE;

PowerupHandler::PowerupHandler() : System(sys_PowerupHandler, "Powerup System")
{


}

PowerupHandler::~PowerupHandler()
{

}

void PowerupHandler::Init(void)
{

	REGISTER_COMPONENT(PowerupSpawner);
	REGISTER_COMPONENT(RigidBody);

	RegisterLink("Powerup_Coffee", "powerup_coffee_360.png",
		{new Powerup_MoveSpeed(2.0f), new Powerup_AttackSpeed(0.25f)});

	RegisterLink("Powerup_Banana", "powerup_bagel.png",
	{ new Powerup_Range(0.5f), new Powerup_BouncyBullets(), new Powerup_Damage(-.25f) });

	RegisterLink("Powerup_Ham", "powerup_syrup.png",
		{new Powerup_MultiShot(3)});

	RegisterLink("Powerup_Oatmeal", "powerup_oatmeal.png",
		{ new Powerup_Health(3.0f)});

	RegisterLink("Powerup_OrangeJuice", "powerup_orangejuice.png",
	{ new Powerup_Damage(1.5f) });

	/*RegisterPowerupLink("Powerup_Flamethrower", "PLACEHOLDER_Powerup_Oatmeal.png",
	{ new Powerup_Range(-1.0f), new Powerup_AttackSpeed(0.2f), new Powerup_Damage(-0.9f) });*/

	RegisterLink("Health", "HealthBar_Egg1.png",
	{ new Powerup_HealOnce(3.0f) },
	POWERUP_PICKUP);

	RegisterLink("HalfHealth", "HealthBar_Egg2.png",
	{ new Powerup_HealOnce(1.5f) },
	POWERUP_PICKUP);
	// CHEATS -- won't be picked up by random powerups

	RegisterLink("Cheat_Invulnerability", "powerup_coffee_360.png",
	{ new Powerup_Invulnerability(9999.0f) },
	POWERUP_CHEAT);

	RegisterLink("Cheat_Bouncy", "powerup_coffee_360.png",
	{ new Powerup_BouncyBullets() },
	POWERUP_CHEAT);

	RegisterLink("Cheat_Espresso", "powerup_coffee_360.png",
	{ new Powerup_MoveSpeed(10.0f) },
	POWERUP_CHEAT);


	RegisterLink("Cheat_GodMode", "powerup_coffee_360.png",
	{ new Powerup_Invulnerability(999999.0f), new Powerup_Damage(3.0f), new Powerup_MoveSpeed(4.0f) }, POWERUP_CHEAT);

}


Entity* PowerupHandler::CreateRandomEntity(Point2D pos, POWERUP_TYPE type)
{
	return link_maps[type].CreateRandomEntity(pos);
}

Entity* PowerupHandler::CreatePowerupEntity(std::string effect, Point2D pos, POWERUP_TYPE type)
{
	return link_maps[type].CreatePowerupEntity(effect, pos);
}

Entity* PowerupHandler::CreatePowerupEntityFromEffect(PowerupEffect * effect, Point2D pos)
{
	// Search through all our maps to find the name of the effect
	for (int i = 0; i < POWERUP_TYPES_MAX; ++i)
	{
		PowerupLinkMap& p = link_maps[i];
		for (auto e : p.link_map)
		if (e.second.second == effect)
			return CreatePowerupEntity(e.first, pos, (POWERUP_TYPE)i);
	}

	return 0;
}

void PowerupHandler::Update(float dt)
{
	// find all spawners and check collisions with things that can take them
	for (Entity* pspawn_ent : _entities)
	{
		RigidBody* body = pspawn_ent->GET_COMPONENT(RigidBody);
		PowerupSpawner* pspawn = pspawn_ent->GET_COMPONENT(PowerupSpawner);
		
		if (pspawn_ent->GC(Drawable)->visible == false)
			continue;

		// bodies still collide with themselves, so look for any collisisons past that
		if (body->collisionList.size())
		{
			// Check all of the collisions to see if we hit anything with a powerup (container)
			for (CollisionInfo& c: body->collisionList)
			{
				if (c.object->HasComponent(EC_Powerup))
				{
					// If the player is alive (or it isn't a player
					if (!c.object->HasComponent(EC_Player) || c.object->GET(Player)->IsAlive())
					{
						c.object->GET_COMPONENT(Powerup)->AddEffect(pspawn->effect, c.object);
						ENGINE->OM->DestroyEntity(pspawn_ent);

						// Play the audio effect
						GETSYS(AudioSystem)->PlaySound("powerup/collect");

						break;
					}
				}
			}
		}
	}


}

void PowerupHandler::RegisterLink(std::string effect_name, std::string spawn_sprite, std::initializer_list<PowerupEffect*> effects, POWERUP_TYPE type)
{
	link_maps[type].RegisterLink(effect_name, spawn_sprite, effects);
}

std::string PowerupHandler::GetPowerupSprite(std::string effect_name, POWERUP_TYPE type)
{
	return link_maps[type].GetPowerupSprite(effect_name);
}

PowerupEffect* PowerupHandler::GetPowerupEffect(std::string effect_name, POWERUP_TYPE type)
{
	return link_maps[type].GetPowerupEffect(effect_name);
}


void PowerupHandler::Shutdown(void)
{
	// Delete all our powerups
	for (PowerupLinkMap m :link_maps)
	for (auto p : m.link_map)
		delete p.second.second;
}

std::vector<std::string> PowerupHandler::GetAllPowerupEffectNames()
{
	std::vector<std::string> list;

	for (PowerupLinkMap p : link_maps)
	for (auto it : p.link_map)
		list.push_back(it.first);

	return list;
}





