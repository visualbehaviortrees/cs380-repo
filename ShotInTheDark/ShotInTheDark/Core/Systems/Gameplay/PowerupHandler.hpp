//	File name:		Powerups.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include <map>

#include "../../System.hpp"
#include "../../Components/Powerup.h"
#include "PowerupLinkMap.h"
#include "Libraries\RNG\RNG.hpp"

class PowerupHandler : public System
{
public:
	PowerupHandler();
	~PowerupHandler();
  void Init(void);
  void Update(float dt);
  void Shutdown(void);

  enum POWERUP_TYPE
  {
	  POWERUP_POWERUP, // Actual powerups, like ham, bananas, etc..
	  POWERUP_PICKUP, // Single use things like eggs, (experience) etc...
	  POWERUP_CHEAT, // Things you shouldn't ever get in the game

	  POWERUP_TYPES_MAX
  };

  Entity* CreatePowerupEntity(std::string effect, Point2D pos, POWERUP_TYPE type);
  Entity* CreateRandomEntity(Point2D pos, POWERUP_TYPE type = POWERUP_POWERUP);

  std::string GetPowerupSprite(std::string effect_name, POWERUP_TYPE type);
  PowerupEffect* GetPowerupEffect(std::string effect_name, POWERUP_TYPE type);

	Entity* CreatePowerupEntityFromEffect(PowerupEffect * effect, Point2D pos);

	std::vector<std::string> GetAllPowerupEffectNames();

	friend struct PowerupLinkMap;
private:
	RNG random_generator;

	void RegisterLink(std::string effect_name, std::string spawn_sprite, std::initializer_list<PowerupEffect*> effects, POWERUP_TYPE type = POWERUP_POWERUP);
	PowerupLinkMap link_maps[POWERUP_TYPES_MAX];
};