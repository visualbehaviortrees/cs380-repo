//	File name:		PowerupLinkMap.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "PowerupHandler.hpp"
#include "../../Engine.hpp"
#include "../../Components/Components.h"
#include "../Input/InputSystem.hpp"
#include "Libraries\RNG\RNG.hpp"

extern Engine* ENGINE;

// Registers a Powerup "entity" from an arbitrary effect name, a sprite, and a list of effects
void PowerupLinkMap::RegisterLink(std::string effect_name, std::string spawn_sprite, std::initializer_list<PowerupEffect*> effects)
{
	link_map[effect_name] = std::make_pair(spawn_sprite, new PowerupCompound(effects));
}

// Creates an entity at random from our map
Entity* PowerupLinkMap::CreateRandomEntity(Point2D pos)
{
	int index = GETSYS(PowerupHandler)->random_generator.range(0, link_map.size() - 1);
	auto it = link_map.begin();
	for (int i = 0; i < index; ++i)
		it++;

	return CreatePowerupEntity(it->first, pos);
}
// Gets a powerup's sprite from an effect name
std::string PowerupLinkMap::GetPowerupSprite(std::string effect_name)
{
	return link_map[effect_name].first;
}

// Gets a powerup's effect pointer from an effect name
PowerupEffect* PowerupLinkMap::GetPowerupEffect(std::string effect_name)
{
	return link_map[effect_name].second;
}

// Creates a powerup entity from the effect pointer, assuming it exists
Entity* PowerupLinkMap::CreatePowerupEntityFromEffect(PowerupEffect * effect, Point2D pos)
{
	// Search through our map to find the name of the effect
	for (auto e : link_map)
		if (e.second.second == effect)
			return CreatePowerupEntity(e.first, pos);

	return 0;
}

Entity* PowerupLinkMap::CreatePowerupEntity(std::string effect, Point2D pos)
{
	Drawable *drawable = new Drawable();
	drawable->layer = 3;
	drawable->mpMesh = Mesh::Get_Mesh("box");

	// set position
	Transform* transform = new Transform();
	transform->Position = pos;
	transform->rotation = 0.0f;
	transform->scale.Set(1.0f, 1.0f);

	RigidBody *rigid = new RigidBody;
	rigid->isStatic = false;
	rigid->speed = 0.001f;
	rigid->ghost = true;

	Sprite *sprite = new Sprite();
	sprite->ChangeTexture(GetPowerupSprite(effect));

	PowerupSpawner* pSpawn = new PowerupSpawner();
	pSpawn->effect = GetPowerupEffect(effect);

	Team* pTeam = new Team(Team_ALL);

	Light* pLight = new Light(100.0f);


	Entity * pEnt = ENGINE->Factory->Compose(7,
		drawable,
		transform,
		rigid,
		sprite,
		pSpawn,
		pTeam,
		pLight
		);
	pEnt->SetName(effect);
	return pEnt;
}
