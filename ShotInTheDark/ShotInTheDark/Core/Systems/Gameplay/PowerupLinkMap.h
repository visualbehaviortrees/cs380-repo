//	File name:		Powerups.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include <map>

#include "../../Components/Powerup.h"
#include "PowerupHandler.hpp"

/*
This class is like a simple archetype system for powerups/health pickups/cheat powerups
*/
struct PowerupLinkMap
{
	// Registers a Powerup "entity" from an arbitrary effect name, a sprite, and a list of effects
	void RegisterLink(std::string effect_name, std::string spawn_sprite, std::initializer_list<PowerupEffect*> effects);
	// Creates an entity at random from our map
	Entity* CreateRandomEntity(Point2D pos);
	// Gets a powerup's sprite from an effect name
	std::string GetPowerupSprite(std::string effect_name);
	// Gets a powerup's effect pointer from an effect name
	PowerupEffect* GetPowerupEffect(std::string effect_name);
	// Creates a powerup entity from the effect pointer, assuming it exists
	Entity* CreatePowerupEntityFromEffect(PowerupEffect * effect, Point2D pos);
	// Creates a powerup entity from the effects name, at the given position
	Entity* CreatePowerupEntity(std::string effect, Point2D pos);

	friend class PowerupHandler;
private:
	std::map<std::string, std::pair<std::string, PowerupEffect*>> link_map;
};