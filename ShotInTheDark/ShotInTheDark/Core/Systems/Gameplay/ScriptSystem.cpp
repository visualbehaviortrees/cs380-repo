//	File name:		TwoFlagGameplay.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "../Input/InputSystem.hpp"
#include "ScriptSystem.hpp"
#include "../../Engine.hpp"
#include "../../Components/Components.h"
#include "../Input/InputSystem.hpp"
#include "../Physics/Collision.hpp"
extern Engine* ENGINE;

ScriptSystem::ScriptSystem() : System(sys_ScriptSystem, "Script System")
{


}

ScriptSystem::~ScriptSystem()
{


}

void ScriptSystem::Init(void)
{
	REGISTER_COMPONENT(Script);
	REGISTER_COMPONENT(Transform);
	REGISTER_COMPONENT(Drawable);

}



void ScriptSystem::Update(float dt)
{
	for (auto entity : _entities)
	{
		Script *script = entity->GC(Script);
	}
}

void ScriptSystem::Shutdown(void)
{


}
