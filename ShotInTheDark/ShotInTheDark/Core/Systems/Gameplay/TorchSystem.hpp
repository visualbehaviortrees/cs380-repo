//	File name:		TestSystem.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../../System.hpp"

class TorchSystem : public System
{
public:
  TorchSystem(void) :System(sys_TorchSystem, "Torch Controller System") {}

  void Init(void);

  void Update(float dt);

  void Shutdown(void) {}

};
