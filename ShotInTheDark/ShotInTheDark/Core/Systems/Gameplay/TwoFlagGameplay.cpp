//	File name:		TwoFlagGameplay.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "../Input/InputSystem.hpp"
#include "TwoFlagGameplay.hpp"
#include "../../Engine.hpp"
#include "../../Components/Components.h"
#include "../Input/InputSystem.hpp"
#include "../Physics/Collision.hpp"
extern Engine* ENGINE;
static Poly flag(4);
static bool teamFlagsExist = false;
static const unsigned TeamFlagCapturePoints = 10;

TwoFlagGameplay::TwoFlagGameplay(): System(sys_TwoFlagGameplay, "Team CTF System")
{


}

TwoFlagGameplay::~TwoFlagGameplay()
{


}

void TwoFlagGameplay::Init(void)
{
  REGISTER_COMPONENT(Flag);
  REGISTER_COMPONENT(Transform);
  REGISTER_COMPONENT(Drawable);
  REGISTER_COMPONENT(Team);
  REGISTER_COMPONENT(Parent);

  if(!teamFlagsExist)
  {
    auto bases = ENGINE->OM->FilterEntities(MC_Base | MC_Team);
    flag.SetVert(0) = Math::Point2D(-0.375f,-0.25f);
    flag.SetVert(1) = Math::Point2D(0.375f, 0.0f);
    flag.SetVert(2) = Math::Point2D(-0.375f,0.25f);
    flag.SetVert(3) = Math::Point2D(-0.375f,-0.25f);

    for(auto it : bases)
    {
      Team* team = new Team((Teams)(it->GET_COMPONENT(Team)->teamID));
      Drawable *DrawFlag = new Drawable();
      *DrawFlag = *(it->GET_COMPONENT(Drawable));
      DrawFlag->layer = 10;
      Transform *flagTrans = new Transform();
      flagTrans->scale.Set(1.4f,1.4f);
      flagTrans->Position = it->GET_COMPONENT(Transform)->Position;
      RigidBody *flagRigid = new RigidBody();
      flagRigid->isStatic = false;
      flagRigid->rotationLocked = true;
      flagRigid->vel.Set(0,0);
      Flag* flagcmp = new Flag();
      Parent *parent = new Parent();
      Entity * theFlag = nullptr;
      if(team->teamID == Team_Player_1)
      {
        DrawFlag->mpMesh = Mesh::Get_Mesh("BlueFlag");
        theFlag = ENGINE->Factory->Compose(6,parent,team,DrawFlag,flagTrans,flagRigid,flagcmp);
      }
      else if(team->teamID == Team_Player_1)
      {
        DrawFlag->mpMesh = Mesh::Get_Mesh("RedFlag");
        theFlag = ENGINE->Factory->Compose(6,parent,team,DrawFlag,flagTrans,flagRigid,flagcmp);
      }

	  if ((it->GET_COMPONENT(Team)->teamID) == Team_Player_1)
        theFlag->SetName(std::string("Red Flag"));
	  else if ((it->GET_COMPONENT(Team)->teamID) == Team_Player_1)
        theFlag->SetName(std::string("Blue Flag"));

    }

  }

}



void TwoFlagGameplay::Update(float dt)
{
  std::vector<Entity *> objects = ENGINE->OM->FilterEntities(MC_Player | MC_Transform | MC_Team);
  std::vector<Entity *> bases = ENGINE->OM->FilterEntities(MC_Base | MC_Team | MC_Transform);
  for(auto it : _entities)
  {
    // First we set the position of the flag to be either at the flag stand or the player holding it.
    {
      if(it->GET_COMPONENT(Team)->teamID != Team_NONE)
      {
        if(it->GET_COMPONENT(Parent)->parent == NULL)
        {
          
          for(auto player : objects) 
          {
            if(player->GET_COMPONENT(Team)->teamID != it->GET_COMPONENT(Team)->teamID)
            {
              if(CollisionCheck(it,player,Math::Vector2D()))
              {
                it->GET_COMPONENT(Parent)->parent = player;
                it->GET_COMPONENT(Transform)->Position = player->GET_COMPONENT(Transform)->Position;
                break;
              }
            }
          }
          // @@TODO: set it to a flag base
          for(auto base: bases)
          {
            if(base->GET_COMPONENT(Team)->teamID == it->GET_COMPONENT(Team)->teamID)
            {
              it->GET_COMPONENT(Transform)->Position = base->GET_COMPONENT(Transform)->Position;
              break;
            }
          }
        }
        else
        {
          it->GET_COMPONENT(Transform)->Position = it->GET_COMPONENT(Parent)->parent->GET_COMPONENT(Transform)->Position;
           // next we reset the flag and add to the correct score if the holder returns the flag to his base!
          for(auto base: bases)
          {
            if(it->GET_COMPONENT(Parent)->parent)
            {
              if(base->GET_COMPONENT(Team)->teamID == it->GET_COMPONENT(Parent)->parent->GET_COMPONENT(Team)->teamID)
              {
                if(CollisionCheck(it, base, Math::Vector2D()))
                {
                  if(it->GET_COMPONENT(Parent)->parent->GET_COMPONENT(Team)->teamID == base->GET_COMPONENT(Team)->teamID)
                  {
                    // @@TODO: some logic with scores
                    // @@PAST ME: Thanks for pointing this out clearly for me, you're a cool guy.
                    //it->GET_COMPONENT(Parent)->parent->GET_COMPONENT(Player)->score += TeamFlagCapturePoints;
                    //std::cout << it->GET_COMPONENT(Parent)->parent->GET_COMPONENT(Player)->score << std::endl;

                    //reinterpret_cast<AudioSystem *>(ENGINE->GetSystem(sys_AudioSystem))->PlaySound(SFX::FLAG_CAP); DOESN:T EXIST ANYMORE
                    /*********************************/
                    it->GET_COMPONENT(Parent)->parent = NULL;
                    for(auto base: bases)
                    {
                      if(base->GET_COMPONENT(Team)->teamID == it->GET_COMPONENT(Team)->teamID)
                      {
                        it->GET_COMPONENT(Transform)->Position = base->GET_COMPONENT(Transform)->Position;
                        break;
                      }
                    }
                  }
                }
              }
            }
          }
          if(it->GET_COMPONENT(Parent)->parent)
          {
            // Check if the parent is dead, and drop the flag.
           /* if(!it->GET_COMPONENT(Parent)->parent->GET_COMPONENT(Player)->alive)
            {
              it->GET_COMPONENT(Parent)->parent = NULL;
              for(auto base: bases)
              {
                if(base->GET_COMPONENT(Team)->teamID == it->GET_COMPONENT(Team)->teamID)
                {
                  it->GET_COMPONENT(Transform)->Position = base->GET_COMPONENT(Transform)->Position;
                  
                    reinterpret_cast<AudioSystem *>(ENGINE->GetSystem(sys_AudioSystem))->PlaySound(SFX::FLAG_DROP);
                  break;
			}
            }
				} -- Players are always alive now -- Luc*/
          }
        }
      } // if no team
    }
  }
}

void TwoFlagGameplay::Shutdown(void)
{


}
