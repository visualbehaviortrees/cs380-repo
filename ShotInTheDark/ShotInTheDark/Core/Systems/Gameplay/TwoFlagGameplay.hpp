//	File name:		TwoFlagGameplay.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../../System.hpp"

class TwoFlagGameplay : public System
{
public:
  TwoFlagGameplay();
  ~TwoFlagGameplay();
  void Init(void);
  void Update(float dt);
  void Shutdown(void);

private:

};