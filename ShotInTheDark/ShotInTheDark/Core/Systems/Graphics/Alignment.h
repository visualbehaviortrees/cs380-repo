//	File name:		Alignment.h
//	Project name:	Shot In The Dark
//	Author(s):		(Insert Name Here)
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

enum Alignment
{
  Alignment_TopLeft,
  Alignment_Left,
  Alignment_BottomLeft,
  Alignment_TopCenter,
  Alignment_Center,
  Alignment_BottomCenter,
  Alignment_TopRight,
  Alignment_Right,
  Alignment_BottomRight
};
