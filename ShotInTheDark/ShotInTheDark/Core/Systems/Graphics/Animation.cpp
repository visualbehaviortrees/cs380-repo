//	File name:		Animation.h
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.
#include "stdafx.h"
#include "Animation.h"

Animation::Animation(std::initializer_list<Frame> frames, bool repeats)
	: frameList(frames), currFrame(0), currFrameTime(0.0f), repeating(repeats), active(true)
{
	
}

Animation::Animation(std::string filename, bool repeats)
	: currFrame(0), currFrameTime(0.0f), repeating(repeats), active(true)
{
	std::ifstream animFile;
	static const std::string resource_loc("Resources/");
	animFile.open(resource_loc + filename);
	if (!animFile.good())
	{
		std::cout << "Problem loading animation: " << filename << std::endl;
		return;
	}

	while (!animFile.eof())
	{
		Frame f;
		animFile >> f.frameSprite;
		animFile >> f.frameTime;
		frameList.push_back(f);
	}
}

Animation::Animation()
	: currFrame(0), currFrameTime(0), repeating(false)
{

}


void Animation::Update(float dt)
{
	if (!active)
		return;
	// Increment the time of the current frame
	currFrameTime += dt;
	// If we went over the frame's time, increment our counter and move to the next one
	while (currFrameTime > frameList[currFrame].frameTime)
	{
		currFrameTime -= frameList[currFrame].frameTime;
		currFrame++;
		// Did we end our animation/loop?
		if (currFrame >= frameList.size())
			if (!repeating)
			{
				currFrame--;
				active = false;
			}
			else
				currFrame = 0;
	}
}

void Animation::Reset()
{
	currFrame = 0;
	currFrameTime = 0.0f;
}

std::string Animation::CurrentSprite()
{
	return frameList[currFrame].frameSprite;
}


AnimationMap::AnimationMap()
{

}

void AnimationMap::RegisterAnimation(std::string s, Animation a)
{
	animation_map[s] = a;
}

void AnimationMap::Update(float dt)
{
	current_animation.Update(dt);
}

void AnimationMap::StartAnimation(std::string s)
{
	current_animation = animation_map[s];
}

std::string AnimationMap::CurrentSprite()
{
	return "";
}

void AnimationMap::Reset()
{
	current_animation.Reset();
}