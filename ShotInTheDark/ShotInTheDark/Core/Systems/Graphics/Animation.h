//	File name:		Animation.h
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.
#pragma once

class Animation
{
public:
	struct Frame
	{
		std::string frameSprite;
		float frameTime;
	};

	Animation(std::initializer_list<Frame> frames, bool repeats = true);
	Animation(const std::string filename, bool repeats = true);
	Animation();

	void Update(float dt);
	std::string CurrentSprite();
	void Reset();

private:
	std::vector<Frame> frameList;
	unsigned currFrame;
	float currFrameTime;
	bool repeating;
	bool active;
};


class AnimationMap
{
public:

	AnimationMap();

	void RegisterAnimation(std::string, Animation);

	void Update(float dt);
	void StartAnimation(std::string);
	std::string CurrentSprite();
	void Reset();

private:
	Animation current_animation;
	std::map<std::string, Animation> animation_map;
};
