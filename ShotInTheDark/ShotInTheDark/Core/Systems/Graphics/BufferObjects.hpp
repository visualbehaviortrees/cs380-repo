//	File name:		BufferObjects.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Gonzalo Rojo
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "OpenGL.hpp"

class VertexArrayObject
{
public:
  VertexArrayObject(void)
  {
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
  }
  ~VertexArrayObject(void)
  {
    glDeleteVertexArrays(1, &vao);
    vao = 0;
  }

  void bind ()
  {
    glBindVertexArray(vao);
  }
  void unBind ()
  {
    glBindVertexArray(0);
  }

private:
  GLuint vao;
};


class VertexBufferObject
{
public:
  VertexBufferObject()
  {
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, 0, 0, GL_STREAM_DRAW);
  }
  VertexBufferObject (int size, GLfloat* data)
  {
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData (GL_ARRAY_BUFFER, size, (const void*)data, GL_STATIC_DRAW);

    numVertices = (size / 2) / sizeof (float);
  }
  ~VertexBufferObject()
  {
    glDeleteBuffers(1, &vbo);
    vbo = 0;
  }

  void AddData(float* data, GLuint size)
  {
    mData.insert(mData.end(), data, data + size);
  }

  void UploadData(GLenum mode)
  {
    bind();
    glBufferData(GL_ARRAY_BUFFER, mData.size() * sizeof (float), mData.data(), mode);
    mData.clear();
    unBind();
  }

  void bind ()
  {
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
  }
  void unBind ()
  {
    glBindBuffer(GL_ARRAY_BUFFER, 0);
  }

  unsigned getDataSize()
  {
    return mData.size();
  }

  int getVerticeCount ()
  {
    return numVertices;
  }

private:
  int numVertices;
  GLuint vbo;
  std::vector <float> mData;
};


class ElementBufferObject
{
public:
  ElementBufferObject(int size, GLuint* data)
  {
    glGenBuffers(1, &ebo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, (const void*)data, GL_STATIC_DRAW);
  }
  ~ElementBufferObject()
  {
    glDeleteBuffers(1, &ebo);
    ebo = 0;
  }
  void bind ()
  {
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  }
  void unBind ()
  {
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
  }

private:
  GLuint ebo;
};


class FrameBufferObject
{
public:
  FrameBufferObject()
  {
    glGenFramebuffers(1, &fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
  }
  ~FrameBufferObject()
  {
    glDeleteFramebuffers(1, &fbo);
    fbo = 0;
  }
  void bind ()
  {
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
  }
  void unBind ()
  {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
  }
  void attachTexture (GLenum attachment, GLuint texture)
  {
    bind();
    glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, texture, 0);
  }

private:
  GLuint fbo;
};


typedef VertexArrayObject VAO;
typedef VertexBufferObject VBO;
typedef ElementBufferObject EBO;
typedef FrameBufferObject FBO;

