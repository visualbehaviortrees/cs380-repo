//	File name:		Camera.cpp
//	Project name:	Shot In The Dark
//	Author(s):		James Schmidt
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "Camera.hpp"

Camera::Camera(void)
{
  size = STARTING_CAM_SIZE;
  height = size;
  aspect = 16.0f/9.0f;
  width = height * aspect;
  pos = Point2D(0.0f, 0.0f);
}

Camera::~Camera(void)
{
}



void Camera::Resize (float win_aspect)
{
  if (win_aspect < aspect) // Window Width gets smaller so adjust camera height
  {
    width = size * aspect;
    height = width * 1 / win_aspect;
  }
  else  // Window Height gets smaller so adjust camera width
  {
    height = size;
    width = height * win_aspect;
  }
}
