//	File name:		Camera.hpp
//	Project name:	Shot In The Dark
//	Author(s):		James Schmidt
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#define STARTING_CAM_SIZE 20.25f

using namespace Math;

class Camera
{
  public:
    Camera(void);
    ~Camera(void);

    void Resize (float win_aspect);
    
    float size;
    float width, height;
    float aspect;

    Point2D pos;
};
