//	File name:		Graphics.cpp
//	Project name:	Shot In The Dark
//	Author(s):		(Insert Name Here)
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"

// DON'T TAKE OUT!!
// Stops the compiler from having issues with redefinitions because of windows.h
#ifdef APIENTRY
#undef APIENTRY
#endif
#ifdef MOD_SHIFT
#undef MOD_SHIFT
#endif
#ifdef MOD_ALT
#undef MOD_ALT
#endif

#include <windows.h>

// Stops the compiler from having issues with redefinitions because of windows.h
#ifdef MOD_SHIFT
#undef MOD_SHIFT
#endif
#ifdef MOD_ALT
#undef MOD_ALT
#endif

#include <vector>
#include <algorithm>
#include "Graphics.hpp"
#include "../../Engine.hpp"
#include "LoadShader.hpp"
#include "soil/SOIL.h"
#include "../Input/InputSystem.hpp"
#include "../../Components/Components.h"
#include "../../Libraries/AntTweakBar/AntTweakBar.hpp"
#include "ResourceManager.h"
#include "../../Components/Sprite.h"
#include "../../Components/Mesh.hpp"
#include "Libraries/JSON/JSONHeaders.hpp"
#include "SpriteBatcher.h"
#include "Systems/DebugTools/DebugTools.hpp"
#include "Systems/Gameplay/DungeonGameplay.h"
#include "../../Components/GUIItem.h"

#define GLFW_EXPOSE_NATIVE_WIN32
#define GLFW_EXPOSE_NATIVE_WGL
#include "../../Libraries/glfw/glfw3native.h"

#define PI 3.1415926535f

#define TEST_CENTER 0

bool FullScreen;

static float DrawLights;

static bool Grid;

float t = 0.0f;
float Change_Time = 60.0f;

Math::Affine ObjectToWorld;
Math::Matrix4 ObjectToWindow;

float rot = 0.0f;

GLuint m_Matrix = 0;
GLuint LightPos_Array = 0;
GLuint m_Instensity = 0;
GLuint m_Specular = 0;
GLuint m_Lights = 0;
GLuint m_Sample = 0;
GLuint m_NormalSample = 0;
GLuint m_DrawOrNot = 0;
GLuint m_pos = 0;
GLuint m_tex = 0;
GLuint m_ambient_intens = 0;
GLuint l_ambient_color = 0;

GLfloat projMatrix[16];
GLfloat modelMatrix[16];
GLuint spriteShaderProgram;
GLuint uniMVPMatrix;
GLuint MaxFrames;
GLuint CurrentFrame;
Texture textures[3];
bool debugDraw;

glm::vec3 lightPos[150];
float intensity[150];
glm::vec3 specular[150];

VAO* spriteVAO;
VBO* spriteVBO;
EBO* spriteEBO;

VAO* vao1;
VBO* vbo1;
FBO* fbo;
Texture* renderTexture;
Texture* normalTexture;
GLuint sceneShaderProgram;
enum COLOR_STATE { NOTHING, INTERPOLATE };
//COLOR_STATE BackColor;
glm::vec4 startColor;
glm::vec4 endColor;
glm::vec4 color;

glm::mat4 MyMatrix(1.0f);
glm::mat4 modelToWorld(1.0f);
glm::vec3 light(0.0f, 0.0f, 0.0f);
glm::vec3 ambientLight(0.1f, 0.1f, 0.1f);

GLfloat vertices[] = {
  //  Vertices                           Color                          UV Coord.
  -1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, // Top-left
  1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, // Top-right
  1.0f, -1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, // Bottom-right
  -1.0f, -1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f  // Bottom-left
};

GLfloat verticearray[] = {
  -1.0f, 1.0f, 0.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, -1.0f, 1.0f, 0.0f,

  1.0f, -1.0f, 1.0f, 0.0f,
  -1.0f, -1.0f, 0.0f, 0.0f,
  -1.0f, 1.0f, 0.0f, 1.0f
};

GLuint indices[] = {
  0, 1, 2,
  2, 3, 0
};

int test = 0;

int ___TEST = 0;

static bool NoParticles;

int uniValue = 1;

bool Show_MiniMap;

/************************************************************************/
// GLEW Context
/************************************************************************/

std::map <std::thread::id, GLEWContext*> Graphics::m_ContextMap;

GLEWContext* glewGetContext ()
{
  return Graphics::GetCurrentContext ();
}

GLEWContext* Graphics::GetCurrentContext ()
{
  std::thread::id threadId = std::this_thread::get_id ();
  return m_ContextMap [threadId];
}

void Graphics::MakeContextCurrent(GLEWContext* context, GLFWwindow* window)
{
  if (context != nullptr && window != nullptr)
  {
    m_ContextMap [std::this_thread::get_id ()] = context;
    glewInit ();
    glfwMakeContextCurrent (window);
  }
}

void Graphics::RemoveContext(GLEWContext* context)
{
  m_ContextMap.erase (std::this_thread::get_id ());
}

static GLFWwindow* InitWindow (GLEWContext** context, GLFWwindow* shared /* = nullptr*/, bool visible /* = true*/)
{
  std::thread::id threadId = std::this_thread::get_id ();
  GLEWContext* prevContext = Graphics::GetCurrentContext();
  GLFWwindow* prevWindow = shared;

  int width = 800, height = 600;
  GLFWwindow* win;
#ifdef GL33
  glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
  glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
#endif
  if (visible)
    glfwWindowHint (GLFW_VISIBLE, GL_TRUE);
  else
    glfwWindowHint (GLFW_VISIBLE, GL_FALSE);

  if (FullScreen)
  {
    GLFWmonitor* monitor = 0;
    if (visible) //Don't create fullscreen window for offscreen contexts
      monitor = glfwGetPrimaryMonitor ();
    win = glfwCreateWindow (width, height, "LoadingContext", monitor, shared);
  }
  else
  {
    win = glfwCreateWindow (width, height, "LoadingContext", 0, shared);
  }

  *context = new GLEWContext ();

  glfwMakeContextCurrent (win);
  Graphics::MakeContextCurrent (*context, win);

  Graphics::MakeContextCurrent (prevContext, prevWindow);

  return win;
}

/************************************************************************/
// Resource Loading
/************************************************************************/
// Thread used for loading graphics resources
static std::thread resourceLoadingThread;
// GLEW Context for gl Function calls on the thread
static GLEWContext* resourceLoadingGLEWContext = nullptr;
// Window Context for gl function calls
static GLFWwindow* resourceLoadingContext = nullptr;

/***************************************************************************/
/*!
\brief
Allocate Graphics Objects On A Different Thread
*/
/***************************************************************************/
void Graphics::AllocateResources ()
{
  // Flag To Stop Loading Screen Animation
  m_Loading = true;
  // Make the Resource Loading Context the current context on this thread
  MakeContextCurrent (resourceLoadingGLEWContext, resourceLoadingContext);
  // Load the Texture Objects
  RESOURCES->Load_Resources ();
  // Destroy the context as we no longer need it
  // Note: This can be used as a context for loading levels if not destroyed
  glfwDestroyWindow (resourceLoadingContext);
  // Remove the GLEW Context from the map
  RemoveContext (resourceLoadingGLEWContext);
  // Deallocate memory
  delete resourceLoadingGLEWContext;
  // Don't return until all the opengl commands are completed
  glFinish ();
  // Reset the flag to stop the loading animation
  m_Loading = false;

  resourceLoadingThread.detach ();
}

/************************************************************************/
// IMPORTANT: HARD-CODED COMPLETELY
/************************************************************************/
void Graphics::DrawLoadingScreen ()
{
  bool mybool = false;
  unsigned count = 0;
  std::vector <std::string> loadingText;
  loadingText.push_back ("BreakFasT ApoCAlypSE");
  loadingText.push_back("FoOd EveRyWheRe");
  loadingText.push_back("LoAdiNg GaMe");


  std::vector <Entity*> weirdtextObjects;
  int randomNumber = rand () % 10 + 10;
  for (int i = 0; i < randomNumber; ++i)
  {
    Transform* tform = new Transform ();
    tform->Position.x = glm::linearRand (-2.0f, 1.5f);
    tform->Position.y = glm::linearRand (-1.5f, 1.5f);
    SpriteText* spriteText = new SpriteText ();
    spriteText->text = loadingText [rand () % loadingText.size()];
    Entity* loadingScreen = ENGINE->Factory->Compose (2, tform, spriteText);
    weirdtextObjects.push_back (loadingScreen);
  }

  int counter = 0;

  // Loop through until all resources are loaded
  // Note: This can be used for Level Loading by changing the flag variable
  while (m_Loading)
  {
    /************************************************************************/
    /************************************************************************/
    // Hard-Code Graphics Loop
    /************************************************************************/
    /************************************************************************/
    glfwPollEvents ();
    glClear (GL_COLOR_BUFFER_BIT);

    for (auto& it : weirdtextObjects)
    {
      Transform* tform = it->GET_COMPONENT (Transform);
      SpriteText* spriteText = it->GET_COMPONENT (SpriteText);
      // Show Loading Screen / Splash Screen / Weird Stuff
      if (count== 0)
      {
        tform->Position.x = glm::linearRand(-1.0f, 1.5f);
        tform->Position.y = glm::linearRand(-1.5f, 1.5f);
        spriteText->text = loadingText[rand() % loadingText.size()];
        spriteText->fontSize = ADLib::randInt(100, 150);
        spriteText->color = glm::linearRand
          (
          glm::linearRand(glm::vec4(1.0f, 0.0f, 0.0f, 1.0f), glm::vec4(0.0f, 1.0f, 0.0f, 1.0f)),
          glm::linearRand(glm::vec4(0.0f, 1.0f, 0.0f, 1.0f), glm::vec4(0.0f, 0.0f, 1.0f, 1.0f))
          );

        count = 50;
      }
      --count;
      spriteText->Draw (tform->Position);
    }

    glfwSwapBuffers(CurrentWindow->window);
    ++counter;
    /************************************************************************/
    /************************************************************************/
  }

  for (auto& it : weirdtextObjects)
  {
    delete it;
  }
  glClear (GL_COLOR_BUFFER_BIT);
  glfwSwapBuffers(CurrentWindow->window);
}

/************************************************************************/
/************************************************************************/

Window::Window(bool full_screen)
{
  Create_Window(full_screen);
}

void Window::Create_Window(bool full_screen)
{
  //glfwWindowHint(GLFW_DECORATED, 0);
  glfwWindowHint(GLFW_RESIZABLE, 1);
  glfwWindowHint(GLFW_SAMPLES, 4); //antialiazing 4x
  //glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //OpenGL 3.3
  //glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); //To make MacOS happy
  //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //Don't want the old OpenGL
  //glfwWindowHint(GLFW_VISIBLE, GL_FALSE); // Starts all Windowed windows as hidden
  glEnable(GL_MULTISAMPLE);

  Monitor = glfwGetPrimaryMonitor();
  Desktop = glfwGetVideoMode(Monitor);

  WindowWidth = Desktop ? (full_screen ? Desktop->width : Desktop->width / 2) : 600;
  WindowHeight = Desktop ? (full_screen ? Desktop->height : Desktop->height / 2) : 400;

  previousWidth = Desktop ? Desktop->width / 2 : 600;
  previoisHeight = Desktop ? Desktop->height / 2 : 400;

  Window_aspect = (float)Desktop->width / (float)Desktop->height;

  NDC_width = NDC_height = 2.0f;

 
  if (full_screen)
  {
    window = glfwCreateWindow(WindowWidth, WindowHeight, "Breakfast Apocalypse", Monitor, NULL);
  }
  else
  {
    window = glfwCreateWindow(WindowWidth, WindowHeight, "Breakfast Apocalypse", NULL, NULL);
  }

  // GLEW Context: This will be used by all gl Calls
  // Example: glBindFrameBuffer calls glewGetContext which returns the active
  // context on that thread
  // Same applies to all other gl Functions
  glewContext = new GLEWContext ();
  Graphics::MakeContextCurrent (glewContext, window);

  // hides the cursor.
#ifndef _DEBUG
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
#endif
  glfwSwapInterval(1);

  previousPos.x = Desktop->width / 2 - WindowWidth / 2;
  previousPos.y = Desktop->height / 2 - WindowHeight / 2;

  if (!full_screen)
  {
    glfwSetWindowPos(window, previousPos.x, previousPos.y);
  }

  glfwSwapBuffers(window);
  glfwPollEvents();

  //Set Callbacks
  Graphics* GS = (Graphics*)ENGINE->GetSystem(sys_Graphics);

  glfwSetWindowSizeCallback(window, GS->GLFWResizeWindow);
  glfwSetFramebufferSizeCallback(window, GS->GLFWFrameBufferResize);
  glfwSetKeyCallback(window, KeyboardHandler);

  // Window focus callbacks
  glfwSetWindowFocusCallback(window, WindowFocusChange);
  glfwSetWindowIconifyCallback(window, WindowMinimized);

#ifdef _DEBUG
  glfwSetScrollCallback(window, MouseWheelHandler);
  glfwSetDropCallback(window, DragNDropHandler);
  glfwSetCursorPosCallback(window, MouseCurserHandler);
#endif
  glfwSetMouseButtonCallback(window, MouseButtonHandler);
}

Window::~Window()
{
  glfwDestroyWindow(window);
  if (main_Cam)
  {
    delete main_Cam;
  }

  GETSYS(Graphics)->RemoveContext(glewContext);
}

void Window::Init()
{
  main_Cam = new Camera;
  main_Cam->Resize(Window_aspect);


  WindowToNDC = Math::Trans(Math::Vector2D(-1.0f, 1.0f)) * Math::Scale(NDC_width / WindowWidth, -NDC_height / WindowHeight);
  CameraToNDC = Math::Scale(NDC_width / main_Cam->width, NDC_height / main_Cam->height);
  CameraToWorld = Math::Scale(1.0f);

  Current_CameraToWorld = &CameraToWorld;

  //////// MINIMAP CAMERA ////////
  Show_MiniMap = false;

  minimap_Cam = new Camera;
  minimap_Cam->size = 3.0f;
  minimap_Cam->Resize(Window_aspect);

  minimap_CameraToNDC = Math::Scale(NDC_width / minimap_Cam->width, NDC_height / minimap_Cam->height);
  minimap_CameraToWorld = Trans(Point2D(0.0f, 516.0f)) * Math::Scale(50.0f);

  /////////////////////////////////

  glEnable(GL_POINT_SMOOTH);
  glEnable(GL_LINE_SMOOTH);
  glEnable(GL_POLYGON_SMOOTH);

}

void Graphics::Change_Back_Time(float t)
{
  Change_Time = t;
}

Graphics::Graphics() : System(sys_Graphics, "Graphics System")
{
  if (!glfwInit())
  {
    throw("Could not create GLFW Context");
  }

#ifdef _DEBUG  
  FullScreen = false;
#else
  FullScreen = true;
#endif

  windows.push_back(new Window(FullScreen));

  if (windows.empty())
  {
    throw "Failed to create window";
  }

  vao1 = NULL;
  vbo1 = NULL;
  fbo = NULL;
  spriteVAO = NULL;
  spriteVBO = NULL;
  spriteEBO = NULL;
  debugDraw = false;
  runWhilePaused = true;
}

void Graphics::SpecifyScreenVertexAttributes(GLuint shaderProgram)
{
  GLint posAttrib = glGetAttribLocation(shaderProgram, "position");
  glEnableVertexAttribArray(posAttrib);
  glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);

  GLint texAttrib = glGetAttribLocation(shaderProgram, "texcoord");
  glEnableVertexAttribArray(texAttrib);
  glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (void*)(2 * sizeof(GLfloat)));
}

void Graphics::Specify_Texture_Attributes(GLuint spriteShaderProgram)
{
  glUseProgram(spriteShaderProgram);

  GLint posAttrib = glGetAttribLocation(spriteShaderProgram, "position");
  glEnableVertexAttribArray(posAttrib);
  glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), 0);

  GLint colAttrib = glGetAttribLocation(spriteShaderProgram, "color");
  glEnableVertexAttribArray(colAttrib);
  glVertexAttribPointer(colAttrib, 4, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)(3 * sizeof(float)));

  GLint texAttrib = glGetAttribLocation(spriteShaderProgram, "texcoord");
  glEnableVertexAttribArray(texAttrib);
  glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)(7 * sizeof(float)));

  glUseProgram(0);
}


void Graphics::Init()
{
  //Create_Window();
#ifndef _DEBUG  
#pragma comment( linker, "/subsystem:\"windows\" /entry:\"mainCRTStartup\"" )
#endif

  glewExperimental = true;
  if (glewInit() != GLEW_OK)
  {
    throw "Glew failed to initialize";
  }

  std::cout << "OpenGL Version " << glGetString(GL_VERSION) << "\n";
  
  for (unsigned int i = 0; i < windows.size(); ++i)
  {
    windows[i]->Init();
  }

  CurrentWindow = windows[0];

  sceneShaderProgram = LoadShaders("Core/Systems/Graphics/Shaders/SceneVertexShader.glsl", "Core/Systems/Graphics/Shaders/SceneFragmentShader.glsl");
  spriteShaderProgram = LoadShaders("Core/Systems/Graphics/Shaders/QuadVertexShader.glsl", "Core/Systems/Graphics/Shaders/QuadFragmentShader.glsl");

  vao1 = new VAO();
  vbo1 = new VBO(sizeof verticearray, verticearray);
  fbo = new FBO();
  renderTexture = new Texture();
  normalTexture = new Texture();
  renderTexture->createTexture(CurrentWindow->WindowWidth, CurrentWindow->WindowHeight);
  normalTexture->createTexture(CurrentWindow->WindowWidth, CurrentWindow->WindowHeight);
  fbo->attachTexture(GL_COLOR_ATTACHMENT0, renderTexture->Get_ID());
  fbo->attachTexture(GL_COLOR_ATTACHMENT1, normalTexture->Get_ID());

  GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
  glDrawBuffers (2, drawBuffers);

  SpecifyScreenVertexAttributes(sceneShaderProgram);
  vao1->unBind();
  fbo->unBind();

  spriteVAO = new VAO();
  spriteVBO = new VBO(sizeof vertices, vertices);
  spriteEBO = new EBO(sizeof indices, indices);
  Specify_Texture_Attributes(spriteShaderProgram);
  spriteVAO->unBind();

  REGISTER_COMPONENT(Transform);
  REGISTER_COMPONENT(Drawable);

  /************************************************************************/
  /************************************************************************/
  // Multi-Threaded Resource Loading
  /************************************************************************/
  /************************************************************************/

  RESOURCES = new Resource_Manager;

  // Create Resource Loading Context
  // Share the current context with the newly created context so we can
  // share the gl Objects - Texture, Shader, Buffers
  // Note: Cannot Share Vertex Arrays
  resourceLoadingContext = InitWindow(&resourceLoadingGLEWContext, CurrentWindow->window, false);
  // Spawn the Resource Loading Thread
  resourceLoadingThread = std::thread (&Graphics::AllocateResources, this);
  // Make the default context current for rendering on this thread
  MakeContextCurrent(CurrentWindow->glewContext, CurrentWindow->window);

  DrawLoadingScreen ();

  /************************************************************************/
  /************************************************************************/

  startColor = glm::vec4(0.2f, 0.0f, 0.1f, 1.0f);
  endColor = glm::vec4(1.0f, 1.0f, 1.0f, 0.0f);
  color = startColor;

  Grid = false;

  glfwShowWindow(CurrentWindow->window);
  Mesh::Init_All_Meshes();

  SpriteBatcher::Initialize();

  m_Matrix = glGetUniformLocation(sceneShaderProgram, "Matrix");
  LightPos_Array = glGetUniformLocation(sceneShaderProgram, "lightpos");
  m_Instensity = glGetUniformLocation(sceneShaderProgram, "intensity");
  m_Specular = glGetUniformLocation(sceneShaderProgram, "lspecular");
  m_Lights = glGetUniformLocation(sceneShaderProgram, "numLights");
  m_Sample = glGetUniformLocation(sceneShaderProgram, "image");
  m_NormalSample = glGetUniformLocation(sceneShaderProgram, "uniNormalMap");
  m_DrawOrNot = glGetUniformLocation(sceneShaderProgram, "drawornot");
  m_pos = glGetAttribLocation(sceneShaderProgram, "position");
  m_tex = glGetAttribLocation(sceneShaderProgram, "texcoord");
  m_ambient_intens = glGetUniformLocation(sceneShaderProgram, "mambient");
  l_ambient_color = glGetUniformLocation(sceneShaderProgram, "lambient");

  SetBackgroundColor(0.f, 0.f, 0.f);
}

bool SortEntitiesByLayer(Entity *lhs, Entity *rhs)
{
  return lhs->GET_COMPONENT(Drawable)->layer < rhs->GET_COMPONENT(Drawable)->layer;
}


void Graphics::DrawBackground(const Math::Affine& WorldToWindow)
{
  bool Debug_WireFrames = GETSYS(DebugTools)->Get_WireFrames();

  DungeonGameplay* DGP = dynamic_cast<DungeonGameplay*>(ENGINE->getCurrentGamestate());

  for (auto it : _entities)
  {
    if (it->GetName() != "Background" && it->GetName() != "LevelBackground")
    {
      continue;
    }
    
    Drawable *Draw = (it)->GET_COMPONENT(Drawable);
    
    Transform *Trans = (it)->GET_COMPONENT(Transform);
    
    if (it->GetName() == "LevelBackground")
    {
      if (Debug_WireFrames || DGP == nullptr)
      {
        continue;
      }

      if (DGP->GetRoomCenter(GetCurrentWindow()->main_Cam->pos) == (DGP->GetRoomCenter(Trans->Position)))
      {
        Draw->visible = true;
      }
      else
      {
        if (Draw->visible)
        {
          Draw->visible = false;
        }
      }
    }

    if (!Draw->visible || Draw->clipped)
    {
      continue;
    }

    ObjectToWindow = WorldToWindow * Trans->transform;

    if (it->HasComponent(EC_Sprite))
    {
      ++___TEST;
      Sprite* sprite = it->GET_COMPONENT(Sprite);
      if (sprite->NoParticles)
        NoParticles = true;
      if (!it->HasComponent(EC_GUIItem))
        SpriteBatcher::UploadSprite(sprite, ObjectToWindow);
    }

  }
  SpriteBatcher::Batch();
}


void Graphics::Update(float dt)
{
  bool Debug_WireFrames = GETSYS(DebugTools)->Get_WireFrames();
  NoParticles = false;
  if (!glfwWindowShouldClose(CurrentWindow->window))
  {
#pragma region INPUT STUFF

    if (CheckKeyTriggered(KEY_F11))
    {
		  Toggle_Fullscreen();
    }

    if (CheckKeyTriggered(KEY_5))
    {
      if (Grid)
      {
        Grid = false;
      }
      else
      {
        Grid = true;
      }
    }

    if (CheckAllInputSelect())
    {
      Show_MiniMap = true;
    }
    else
    {
      Show_MiniMap = false;
    }
#pragma endregion


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                    MESHES AND TEXTURES DRAW HERE
#pragma region OBJECTS

    CurrentWindow->WorldToNDC = CurrentWindow->CameraToNDC * Math::Inverse(*CurrentWindow->Current_CameraToWorld);

    fbo->bind();
    glClearColor(color.r, color.g, color.b, 1.0f); // Background color
    glClear(GL_COLOR_BUFFER_BIT);

    std::sort(_entities.begin(), _entities.end(), &SortEntitiesByLayer);

    DrawBackground(CurrentWindow->WorldToNDC);

    for (auto it : _entities)
    {
      Drawable *Draw = (it)->GET_COMPONENT(Drawable);

      if (Debug_WireFrames)
      {
        break;
      }

      if (!Draw->visible || Draw->clipped || it->HasComponent(EC_Minimap) || it->HasComponent(EC_GUIItem) || it->GetName() == "LevelBackground" || it->GetName() == "Background")
      {
        continue;
      }

      Transform *Trans = (it)->GET_COMPONENT(Transform);

      ObjectToWindow = CurrentWindow->WorldToNDC * Trans->transform;

      if (it->HasComponent(EC_Sprite))
      {
        ++___TEST;
        Sprite* sprite = it->GET_COMPONENT(Sprite);
        if (sprite->NoParticles)
          NoParticles = true;
        if (!it->HasComponent(EC_GUIItem))
          SpriteBatcher::UploadSprite(sprite, ObjectToWindow);
      }
      if (Draw->mpMesh && !it->HasComponent(EC_Sprite) && !it->HasComponent(EC_GUIItem))
      {
        Draw->mpMesh->Draw(Draw, ObjectToWindow);
      }
    }

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    SpriteBatcher::Batch();

#pragma endregion
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                LIGHTS DRAW HERE
#pragma region LIGHTS

    glUseProgram(sceneShaderProgram);
    vao1->bind();
    fbo->unBind();
    glClear(GL_COLOR_BUFFER_BIT);

    for (int i = 0; i < sizeof(intensity) / sizeof(float); ++i)
    {
      intensity[i] = 0.0f;
      lightPos[i] = glm::vec3(0.0f, 0.0f, 0.0f);
    }

    int count = 0;

    auto litObjects = ENGINE->OM->FilterEntities(MC_Drawable | MC_Transform | MC_Light);

    for (auto it : litObjects)
    {
      if (Debug_WireFrames)
      {
        break;
      }

      // Don't do too many lights
		  if (count >= 150)
		  {
			  std::cout << "TOO MANY LIGHTS :(\n";
			  break;
		  }

      if (it->GET_COMPONENT(Drawable)->visible && !Debug_WireFrames && !it->GET_COMPONENT(Drawable)->clipped)
      {
        Light* light = it->GET_COMPONENT(Light);
        Transform *Trans = it->GET_COMPONENT(Transform);

        intensity[count] = light->intensity;

        specular[count] = light->GetCurrentColorRGB();

        lightPos[count] = glm::vec3(
          Trans->Position.x,
          Trans->Position.y,
          0.0f
          );
        ++count;
      }
    }

    ObjectToWindow = CurrentWindow->WorldToNDC;

    glm::vec3 ambient = GetAmbientIntensity();
    glm::vec3 color = GetAmbientColor();

    if (CheckKeyTriggered(KEY_G))
    {
      uniValue = 0;
    }
    else if (CheckKeyTriggered(KEY_H))
    {
      uniValue = 1;
    }


    glUniform1i(glGetUniformLocation(sceneShaderProgram, "uniValue"), uniValue);
    glUniformMatrix4fv(m_Matrix, 1, GL_FALSE, &ObjectToWindow.matrix4[0][0]);
    glUniform3fv(LightPos_Array, sizeof lightPos / sizeof lightPos[0], glm::value_ptr(lightPos[0]));
    glUniform1fv(m_Instensity, sizeof intensity / sizeof intensity[0], intensity);
    glUniform3fv(m_Specular, sizeof specular / sizeof specular[0], glm::value_ptr(specular[0]));
    glUniform1i(m_Lights, count);

    glActiveTexture(GL_TEXTURE0);
    renderTexture->bind();
    glUniform1i(m_Sample, 0);

    glActiveTexture(GL_TEXTURE1);
    normalTexture->bind();
    glUniform1i(m_NormalSample, 1);

    glActiveTexture(GL_TEXTURE0);

    glUniform3fv(m_ambient_intens, 1, glm::value_ptr(ambient));
    glUniform3fv(l_ambient_color, 1, glm::value_ptr(color));
    glUniform1f(m_DrawOrNot, DrawLights);
    glEnableVertexAttribArray(m_pos);
    glEnableVertexAttribArray(m_tex);

    glDrawArrays(GL_TRIANGLES, 0, 6);

    glUseProgram(0);
    vao1->unBind();
    renderTexture->unBind();
#pragma endregion
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                              PARTICLES GO HERE
#pragma region PARTICLES

    for (auto it : _entities)
    {
      if (it->HasComponent(EC_ParticleEffect) && !NoParticles && !(it)->GET_COMPONENT(Drawable)->clipped && !Debug_WireFrames)
      {
        if(!_screenshotMode)
        it->GET_COMPONENT(ParticleEffect)->Update(dt);
        if (it->GET_COMPONENT(ParticleEffect)->OnOrOff())
          it->GET_COMPONENT(ParticleEffect)->Draw();
      }
    }

#pragma endregion
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                               HUD DRAWN HERE
#pragma region HUD
    // transparency here
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    auto HUDObjects = ENGINE->OM->FilterEntities(MC_Drawable | MC_Transform | MC_GUIItem);
    std::sort(HUDObjects.begin(), HUDObjects.end(), &SortEntitiesByLayer);
    if (!_screenshotMode)
    for (auto it : HUDObjects)
    {
      Drawable *Draw = (it)->GET_COMPONENT(Drawable);
      if (!Draw->visible || Draw->clipped)
      {
        continue;
      }

      Transform *Trans = (it)->GET_COMPONENT(Transform);
      ObjectToWindow = CurrentWindow->WorldToNDC * Trans->transform;

      if (it->HasComponent(EC_Sprite))
      {
        ++___TEST;
        Sprite* sprite = it->GET_COMPONENT(Sprite);
        SpriteBatcher::UploadSprite(sprite, ObjectToWindow);
      }

      else if (Draw->mpMesh != nullptr)
      {
        Draw->mpMesh->Draw(Draw, ObjectToWindow);
      }
    }

    SpriteBatcher::Batch();

#pragma endregion

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                               TEXT

    auto textobjects = ENGINE->OM->FilterEntities(MC_Transform | MC_SpriteText);
    if(!_screenshotMode)
    for (auto it : textobjects)
    {
      SpriteText* Text = it->GET_COMPONENT(SpriteText);
      Transform* Trans = it->GET_COMPONENT(Transform);

      Point2D Pos = CurrentWindow->WorldToNDC * (Trans->Position + Text->offset);
      if (it->HasComponent(EC_Behavior))
      {
        Text->text = Text->aibehavior.str();
        Text->aibehavior.str("");
        Text->fontSize = 36;
        Text->offset = { -7.f, 1.f };
      }
      Text->Draw(Pos);
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                               MINIMAP

    if (Show_MiniMap)
    {

      auto minimap_Objects = ENGINE->OM->FilterEntities(MC_Minimap | MC_Drawable | MC_Transform);

      CurrentWindow->minimap_WorldToNDC = CurrentWindow->minimap_CameraToNDC * Math::Inverse(CurrentWindow->minimap_CameraToWorld);

      for (auto it : minimap_Objects)
      {
        Drawable *Draw = (it)->GET_COMPONENT(Drawable);
        Transform *Trans = (it)->GET_COMPONENT(Transform);

        ObjectToWindow = CurrentWindow->minimap_WorldToNDC * Trans->transform;
        
        if (!Draw->visible)
        {
          continue;
        }
        
        if (it->HasComponent(EC_Sprite))
        {
          Sprite* sprite = it->GET_COMPONENT(Sprite);
          SpriteBatcher::UploadSprite(sprite, ObjectToWindow);
        }
        else if(Draw->mpMesh)
        {
          Draw->mpMesh->Draw(Draw, ObjectToWindow);
        }
      }

      SpriteBatcher::Batch();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

#if(TEST_CENTER)
    Math::Point2D mesh_temp_verts[3];

    ObjectToWorld = Math::Rot(100.f*(float)glfwGetTime());

    mesh_temp_verts[0] = WorldToWindow * ObjectToWorld * Math::Point2D(0.0f, 0.0f);
    mesh_temp_verts[1] = WorldToWindow * ObjectToWorld * Math::Point2D(2.0f, 1.0f);
    mesh_temp_verts[2] = WorldToWindow * ObjectToWorld * Math::Point2D(1.0f, 2.0f);

    glBegin(GL_TRIANGLES);
    glColor3f(0.7f, 1.0f, 0.7f);
    glVertex2f(mesh_temp_verts[0].x, mesh_temp_verts[0].y);
    glVertex2f(mesh_temp_verts[1].x, mesh_temp_verts[1].y);
    glVertex2f(mesh_temp_verts[2].x, mesh_temp_verts[2].y);
    glEnd();
#endif

    UpdateAntTweakBar(); // runs the update of antTweak after graphics has drawn to go over top of graphics
    glDisable(GL_ALPHA);
    glDisable(GL_BLEND);
  }
  else
  {
    ENGINE->_running = false;
  }
}

void Graphics::Shutdown(void)
{
  delete vao1;
  delete vbo1;
  delete fbo;
  delete renderTexture;

  delete spriteVAO;
  delete spriteVBO;
  delete spriteEBO;

  if (!windows.empty())
  {
    for (unsigned int i = 0; i < windows.size(); ++i)
    {
      delete windows[i];
    }
  }

  RESOURCES->Unload_Resources();

  while (MeshList.size())
  {
    MeshList.front().second->Free();
  }
  MeshList.clear();

  //TwTerminate(); // terminates AntTweakBar when the windows closes
  glfwTerminate();
}

void Graphics::SendMsg(Entity*, Entity *, message msg)
{
  if (msg == MSG_Screenshot_Mode)
    _screenshotMode = !_screenshotMode;

}

void Graphics::SetLighting(bool on)
{
  if (on)
    DrawLights = 1.0f;
  else
    DrawLights = 0.0f;
}

void Graphics::SetBackgroundColor(float r, float g, float b)
{
  color.r = r;
  color.g = g;
  color.b = b;
}

Window* Graphics::GetCurrentWindow(void)
{
  return CurrentWindow;
}

GLFWwindow* Graphics::GetGLFWWindow(int index)
{
  return GETSYS(Graphics)->windows[index]->window;
}

int Graphics::GetWindowWidth()
{
  return ((Graphics*)ENGINE->GetSystem(sys_Graphics))->CurrentWindow->WindowWidth;
}

int Graphics::GetWindowHeight()
{
  return ((Graphics*)ENGINE->GetSystem(sys_Graphics))->CurrentWindow->WindowHeight;
}

void Graphics::SetWindowWidth(int width)
{
  ((Graphics*)ENGINE->GetSystem(sys_Graphics))->CurrentWindow->WindowWidth = width;
}

void Graphics::SetWindowHeight(int height)
{
  ((Graphics*)ENGINE->GetSystem(sys_Graphics))->CurrentWindow->WindowHeight = height;
}

void Graphics::GLFWResizeWindow(GLFWwindow* window, int Window_W, int Window_H)
{
  if (Window_W == 0 || Window_H == 0)
  {
    return;
  }

  Window* win = GETSYS(Graphics)->GetCurrentWindow();
  win->WindowWidth = Window_W;
  win->WindowHeight = Window_H;
  win->WindowToNDC = Math::Trans(Math::Point2D(-1.0f, 1.0f)) * Math::Scale(win->NDC_width / win->WindowWidth,
    -win->NDC_height / win->WindowHeight);
  //float prev_aspect = win->main_Cam->aspect;
  win->Window_aspect = ((float) Window_W) / ((float) Window_H);
  //win->main_Cam->aspect = ((float) Window_W) / ((float) Window_H);

  win->main_Cam->Resize(win->Window_aspect);
  win->CameraToNDC = Math::Scale(win->NDC_width / win->main_Cam->width, win->NDC_height / win->main_Cam->height);
  TwWindowSize(Window_W, Window_H);
}

void Graphics::GLFWFrameBufferResize(GLFWwindow*, int w, int h)
{
  if (w == 0 || h == 0)
  {
    return;
  }

  glViewport(0, 0, w, h);
  fbo->bind();
  delete renderTexture;
  renderTexture = new Texture();
  renderTexture->createTexture(w, h);
  normalTexture->createTexture(w, h);
  fbo->attachTexture(GL_COLOR_ATTACHMENT0, renderTexture->Get_ID());
  fbo->attachTexture(GL_COLOR_ATTACHMENT1, normalTexture->Get_ID());

  GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
  glDrawBuffers(2, drawBuffers);

  fbo->unBind();
}

const Math::Affine& Graphics::GetWindowToNDC()
{
  return CurrentWindow->WindowToNDC;
}

const Math::Affine& Graphics::GetCameraToNDC()
{
  return CurrentWindow->CameraToNDC;
}

const Math::Affine* Graphics::GetCameraToWorld()
{
  return CurrentWindow->Current_CameraToWorld;
}

void Graphics::SetCameraPosition(Point2D pos, Vector2D offset)
{
  // We need a vector displacement (from origin), regardless of whether we got a point or a vector
  CurrentWindow->main_Cam->pos = pos;
  // Create an affine transformation from this displacement
  Math::Affine trans = Math::Trans(pos + offset) * Scale(1.0f);

  CurrentWindow->CameraToWorld = trans;
}

void Graphics::SetMinimapCameraPosition(Point2D pos, Vector2D offset)
{
  // We need a vector displacement (from origin), regardless of whether we got a point or a vector
  CurrentWindow->minimap_Cam->pos = pos;
  // Create an affine transformation from this displacement
  Math::Affine trans = Math::Trans(pos + offset) * Scale(50.0f);

  CurrentWindow->minimap_CameraToWorld = trans;
}

Point2D Graphics::GetCameraPosition()
{
  return CurrentWindow->main_Cam->pos;
}


void Graphics::SetAmbientLighting(float x, float y, float z, float r, float g, float b)
{
  AmbientIntensity.x = x;
  AmbientIntensity.y = y;
  AmbientIntensity.z = z;

  AmbientColor.x = r;
  AmbientColor.y = g;
  AmbientColor.z = b;
}

glm::vec3 Graphics::GetAmbientIntensity(void)
{
  return AmbientIntensity;
}

glm::vec3 Graphics::GetAmbientColor(void)
{
  return AmbientColor;
}

bool Graphics::IsLoading () const
{
  return m_Loading;
}

#define CS380
//GLFW Window Callback functions:
// GL_TRUE if focus was gained, GL_FALSE if focus was lost
void WindowFocusChange(GLFWwindow *, int focus)
{
#ifndef CS380
  if (GETSYS(Graphics)->IsLoading())
    return;
  if (focus == GL_TRUE)
    ENGINE->SendMsg(nullptr, nullptr, MSG_GainFocus);
  if (focus == GL_FALSE)
    ENGINE->SendMsg(nullptr, nullptr, MSG_LoseFocus);
#endif
}

// GL_TRUE if minimized, GL_FALSE if returning from minimize
void WindowMinimized(GLFWwindow *, int minimized)
{
#ifndef CS380
  if (GETSYS(Graphics)->IsLoading())
    return;
  if (minimized == GL_TRUE)
    ENGINE->SendMsg(nullptr, nullptr, MSG_Minimized);
  if (minimized == GL_FALSE)
    ENGINE->SendMsg(nullptr, nullptr, MSG_Unminimized);
#endif
}

void Graphics::Toggle_Fullscreen()
{
  FullScreen = !FullScreen;

  if (FullScreen)
  {
    glfwGetWindowSize(CurrentWindow->window, &CurrentWindow->previousWidth, &CurrentWindow->previoisHeight);
    glfwGetWindowPos(CurrentWindow->window, &CurrentWindow->previousPos.x, &CurrentWindow->previousPos.y);
    CurrentWindow->WindowHeight = CurrentWindow->Desktop->height;
    CurrentWindow->WindowWidth = CurrentWindow->Desktop->width;

    SetWindowLongPtr(glfwGetWin32Window(CurrentWindow->window), GWL_EXSTYLE, WS_EX_APPWINDOW | WS_EX_TOPMOST);
    SetWindowLongPtr(glfwGetWin32Window(CurrentWindow->window), GWL_STYLE, WS_POPUP | WS_VISIBLE);
    SetWindowPos(glfwGetWin32Window(CurrentWindow->window), HWND_TOPMOST, 0, 0, CurrentWindow->WindowWidth, CurrentWindow->WindowHeight, SWP_SHOWWINDOW);
    ShowWindow(glfwGetWin32Window(CurrentWindow->window), SW_MAXIMIZE);
  }

  else
  {
    CurrentWindow->WindowHeight = CurrentWindow->previoisHeight + 38;
    CurrentWindow->WindowWidth = CurrentWindow->previousWidth + 16;

    RECT rect;
    rect.left = 0;
    rect.top = 0;
    rect.bottom = CurrentWindow->WindowHeight;
    rect.right = CurrentWindow->WindowWidth;
    
    SetWindowLongPtr(glfwGetWin32Window(CurrentWindow->window), GWL_STYLE, WS_OVERLAPPEDWINDOW | WS_VISIBLE);
    AdjustWindowRect(&rect, WS_OVERLAPPEDWINDOW, FALSE);    
    MoveWindow(glfwGetWin32Window(CurrentWindow->window), 0, 0, CurrentWindow->WindowWidth, CurrentWindow->WindowHeight, TRUE);
    glfwSetWindowPos(CurrentWindow->window, CurrentWindow->previousPos.x, CurrentWindow->previousPos.y);
  }
}