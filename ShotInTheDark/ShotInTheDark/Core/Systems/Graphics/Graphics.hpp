//	File name:		Graphics.hpp
//	Project name:	Shot In The Dark
//	Author(s):		(Insert Name Here)
//	
//	All content � 2014 DigiPen (USA) Corporation, all rights reserved


#pragma once

#include "../../Systems/Graphics/OpenGL.hpp"
#include "../../System.hpp"

#include "Texture.hpp"
#include "Camera.hpp"
#include "BufferObjects.hpp"
#include <vector>

struct Window
{
  Window(bool full_screen = false);
  ~Window();

  void Create_Window(bool full_screen);
  void Init();

  GLFWmonitor* Monitor;
  const GLFWvidmode* Desktop;
  GLFWwindow* window;
  GLEWContext* glewContext;
  Camera* main_Cam;

  Math::Affine WorldToNDC;
  Math::Affine WindowToNDC;
  Math::Affine CameraToNDC;
  Math::Affine CameraToWorld;
  Math::Affine* Current_CameraToWorld;

  Camera* minimap_Cam;

  Math::Affine minimap_CameraToNDC;
  Math::Affine minimap_CameraToWorld;
  Math::Affine minimap_WorldToNDC;

  int WindowHeight, WindowWidth;
  int previoisHeight, previousWidth;
  Math::Vector2DInt previousPos;
  float Window_aspect;
  float NDC_width, NDC_height;
};

class Graphics : public System
{
public:
  Graphics();
  ~Graphics();
  void Init(void);
  void Update(float dt);
  void Shutdown(void);

  //void Create_Window();
  void Window_Init();

  void Change_Back_Time(float t); 

  void SpecifyScreenVertexAttributes (GLuint shaderProgram);
  void Specify_Texture_Attributes(GLuint spriteShaderProgram);

  GLFWwindow* GetGLFWWindow(int index);
  Window* GetCurrentWindow(void);

  static void GLFWResizeWindow(GLFWwindow* window, int width, int height);
  static void GLFWFrameBufferResize (GLFWwindow*, int, int);

  int GetWindowWidth();
  int GetWindowHeight();

  // Sets the current window's moving camera position to be centered at the given world coordinates
  void SetCameraPosition(Point2D pos, Vector2D offset = Vector2D(0.0f, 0.0f));

  void SetMinimapCameraPosition(Point2D pos, Vector2D offset = Vector2D(0.0f, 0.0f));

  Point2D GetCameraPosition();



  // Turns all lighting on or off
  void SetLighting(bool on);

  void SetAmbientLighting(float x, float y, float z, float r = 0.6f, float g = 0.6f, float b = 0.6f);
  glm::vec3 GetAmbientIntensity();
  glm::vec3 GetAmbientColor();
  void SetBackgroundColor(float r, float g, float b);

  void SetWindowWidth(int width);
  void SetWindowHeight(int height);

  const Math::Affine& GetWindowToNDC();
  const Math::Affine& GetCameraToNDC();
  const Math::Affine* GetCameraToWorld();

  void DrawBackground(const Math::Affine& WorldToWindow);

  void SendMsg(Entity*, Entity *, message);

  /************************************************************************/
  // GLEW Context - Required For All gl Calls
  /************************************************************************/
  static GLEWContext* GetCurrentContext ();
  static void MakeContextCurrent (GLEWContext* context, GLFWwindow* window);
  static void RemoveContext (GLEWContext* context);
  /************************************************************************/
  /************************************************************************/
  void Toggle_Fullscreen();
  bool IsLoading () const;

private:
  std::vector<Window*> windows; //Would only be used if we had multiple windows
  Window* CurrentWindow;
  glm::vec3 AmbientIntensity;
  glm::vec3 AmbientColor;

  GLEWContext* m_pPrimaryContext;

  bool _screenshotMode = false;

  bool FullScreen = false;

  /************************************************************************/
  // Context, Resource Loading Variables
  /************************************************************************/
  static std::map <std::thread::id, GLEWContext*> m_ContextMap;
  bool m_Loading;
  void AllocateResources ();
  void DrawLoadingScreen ();
  /************************************************************************/
  /************************************************************************/
  //bool estopit = false;
};

//GLFW Window Callback functions:
// GL_TRUE if focus was gained, GL_FALSE if focus was lost
void WindowFocusChange(GLFWwindow *, int focus);

// GL_TRUE if minimized, GL_FALSE if returning from minimize
void WindowMinimized (GLFWwindow *, int minimized);