//	File name:		GraphicsMath.hpp
//	Project name:	Shot In The Dark
//	Author(s):		(Insert Name Here)
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

//include GLM
#define GLM_FORCE_RADIANS
#include "glm/glm.hpp"
#include "glm/gtx/transform.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/random.hpp"
#include "../../Libraries/Math/Math2D.hpp"

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795f
#endif // !M_PI