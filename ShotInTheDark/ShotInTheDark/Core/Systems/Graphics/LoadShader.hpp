//	File name:		Graphics.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Gonzalo Rojo
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "OpenGL.hpp"

template <class T> const T& max (const T& a, const T& b) {
  return (a<b)?b:a;     // or: return comp(a,b)?b:a; for version (2)
}

GLuint LoadShaders(const char * vertex_file_path, const char * fragment_file_path, const char* geom_file_path = 0);
