//	File name:		OpenGL.hpp
//	Project name:	Shot In The Dark
//	Author(s):		(Insert Name Here)
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../../Libraries/glew/glew.h"
#include "../../Libraries/glfw/glfw3.h"

GLEWContext* glewGetContext ();