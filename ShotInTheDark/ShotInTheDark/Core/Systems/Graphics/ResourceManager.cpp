#include "stdafx.h"
#include "ResourceManager.h"
#include <fstream>
#include <iostream>
#include <string>

void Resource_Manager::Load_Resources ()
{
  Load_Textures();
  Load_Texture_Data();
}

void Resource_Manager::Load_Textures()
{
  std::ifstream MyFile;
  std::string currTexture;

  MyFile.open(TexturePath + "Textures.txt");
  if(!(MyFile.is_open()))
  {
    std::cout<<"Error Openning Textures.txt"<<std::endl;
    return;
  }
  while (!(MyFile.eof()))
  {
    MyFile >> currTexture;
    TexMap [currTexture] = new Texture ( (TexturePath + currTexture).c_str() );
  }
}

void Resource_Manager::Load_Texture_Data(void)
{
  std::ifstream MyFile;
  std::string currFile;
  std::string Data;

  MyFile.open(TexturePath + "Atlas.txt");
  if (!(MyFile.is_open()))
  {
    std::cout << "Error Openning Atlas.txt" << std::endl;
    return;
  }
  std::vector <std::string> files;

  while (!(MyFile.eof()))
  {
    MyFile >> currFile;
    files.push_back(currFile);
  }

  for (auto& atlasFile : files)
  {
    std::ifstream MyFile (TexturePath + atlasFile);

    if (!MyFile.good())
    {
		std::string error("Problem loading atlas: ");
		error += atlasFile;
      throw (error.c_str());
    }
    else
    {
      std::string AtlasName = "";
      MyFile >> AtlasName;
      while (!MyFile.eof())
      {
        MyFile >> Data;

        TextureData* tex = new TextureData();
        tex->Name = Data;
        tex->mpAtlas = Get_Texture(AtlasName);

        MyFile >> Data;
        Data.pop_back();
        tex->bounds.Min.x = stof(Data);
        MyFile >> Data;
        tex->bounds.Min.y = 1 - stof(Data);

        MyFile >> Data;
        Data.pop_back();
        tex->bounds.Max.x = stof(Data);
        MyFile >> Data;
        tex->bounds.Max.y = 1 - stof(Data);

        TexDataMap[tex->Name] = tex;
      }
    }
  }
}

Texture* Resource_Manager::Get_Texture(std::string filename)
{
  for (auto& it : TexMap)
  {
    if (it.first == filename)
    {
      return it.second;
    }
  }

  std::cout << "Wrong texture name: " << filename << std::endl;

  return nullptr;
}

void Resource_Manager::Unload_Resources()
{
  for (auto& it : TexMap)
  {
    it.second->~Texture();
    delete it.second;
  }
}

TextureData* Resource_Manager::Get_TextureData(std::string filename)
{
  for (auto& i : TexDataMap) // pls no more breakerino
  {
    if (i.first == filename)
    {
      return i.second;//''''''90909:(
    }
  }

  return nullptr;
}

Resource_Manager* RESOURCES;
