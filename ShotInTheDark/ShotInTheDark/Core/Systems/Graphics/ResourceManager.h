#pragma once

#include <unordered_map>
#include <map>
#include <string>
#include "Texture.hpp"

class Resource_Manager {
public:
  Resource_Manager()
  {
    TexturePath = "Resources/";
  }
  ~Resource_Manager()
  {
    Unload_Resources();
  }
  //Resource_Manager* RESOURCES;
  std::string TexturePath;
  std::map <std::string, Texture*> TexMap;
  std::map <std::string, TextureData*> TexDataMap;
  void Load_Resources();
  void Load_Textures();
  void Load_Texture_Data();
  void Unload_Resources();
  Texture* Get_Texture(std::string filename);
  TextureData* Get_TextureData(std::string filename);
};

extern Resource_Manager* RESOURCES;