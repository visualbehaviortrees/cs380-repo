#version 330

in vec4 Color;
in vec2 Texcoord;

out vec4 outColor;

uniform sampler2D image;

void main ()
{
  outColor = texture2D (image, Texcoord) * Color;
}