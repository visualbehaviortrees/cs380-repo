#version 330

in vec3 position;
in vec2 texcoord;

out vec3 Eye;
out vec2 Texcoord;

void main ()
{
  gl_Position = vec4 (position.x, position.y, 1.0, 1.0);
  Eye = vec3(position.x, position.y, -1.0);
  Texcoord = texcoord;
}