#version 330

layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

in vec4 gColor [];
in vec4 gTexcoord [];
in mat4 gModelMatrix [];

out vec4 Color;
out vec2 Texcoord;

void main()
{
  Color = gColor [0];

  gl_Position = gModelMatrix [0] * vec4 (-1.0, -1.0, 0.0, 1.0);
  Texcoord = vec2 (gTexcoord [0].x, gTexcoord [0].y);
  EmitVertex ();

  gl_Position = gModelMatrix [0] * vec4 (1.0, -1.0, 0.0, 1.0);
  Texcoord = vec2 (gTexcoord [0].z, gTexcoord [0].y);
  EmitVertex ();

  gl_Position = gModelMatrix [0] * vec4 (-1.0, 1.0, 0.0, 1.0);
  Texcoord = vec2 (gTexcoord[0].x, gTexcoord [0].w);
  EmitVertex ();

  gl_Position = gModelMatrix [0] * vec4 (1.0, 1.0, 0.0, 1.0);
  Texcoord = vec2 (gTexcoord [0].z, gTexcoord [0].w);
  EmitVertex ();

  EndPrimitive();
}
