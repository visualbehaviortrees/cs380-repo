#version 330


in vec3 position;
in vec4 color;
in vec2 texcoord;
out vec4 Color;


uniform ivec2 currentframe;
uniform ivec2 maxframes;

uniform mat4 matrix;

out vec2 Texcoord;

void main()
{
   float U = 0;
   float V = 0;
   vec2 framedivision = vec2(1.0/float(maxframes.x), 1.0/float(maxframes.y));

   Color = color;

  if(position.x == -0.5 && position.y == 0.5)
  {
    U = framedivision.x * float(currentframe.x);
    V = framedivision.y * (float(currentframe.y) + 1.0);
  }
  else if(position.x == -0.5 && position.y == -0.5)
  {
    U = framedivision.x * float(currentframe.x);
    V = framedivision.y * float(currentframe.y);
  }
  else if(position.x == 0.5 && position.y == -0.5)
  {
    U = framedivision.x * float(currentframe.x + 1.0);
    V = framedivision.y * float(currentframe.y);
  }
  else if(position.x == 0.5 && position.y == 0.5)
  {
    U = framedivision.x * float(currentframe.x + 1.0);
    V = framedivision.y * float(currentframe.y + 1.0);
  }

  Texcoord = texcoord;//vec2(U, V);
  gl_Position =  matrix *
				  vec4 (position.x,position.y, position.z, 1.0);
}