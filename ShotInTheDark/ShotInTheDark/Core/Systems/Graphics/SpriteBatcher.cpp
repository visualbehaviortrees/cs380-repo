// Author: Darthvader


#include "stdafx.h"
#include "SpriteBatcher.h"
#include <algorithm>

struct buffers
{
  GLuint mVaoMap;
  GLuint mMatrixVboMap;
  GLuint mColorVbo;
  GLuint mTexVbo;
  GLuint mShader;
};


std::vector <Sprite*>  SpriteBatcher::mSpriteMap;
VAO* SpriteBatcher::mVaoMap;
VBO* SpriteBatcher::mMatrixVboMap;
VBO* SpriteBatcher::mColorVbo;
VBO* SpriteBatcher::mTexVbo;
GLuint SpriteBatcher::mShader;

GLuint SpriteBatcher::m_SamplerLocation;
GLuint SpriteBatcher::m_NormalSamplerLocation;
static GLuint m_UseNormalLocation;


void SpriteBatcher::Initialize()
{
  mShader = LoadShaders("Core/Systems/Graphics/Shaders/Sprite.vs.glsl",
    "Core/Systems/Graphics/Shaders/Sprite.fs.glsl",
    "Core/Systems/Graphics/Shaders/Sprite.gs.glsl");

  m_SamplerLocation = glGetUniformLocation(mShader, "uniTexture");
  m_NormalSamplerLocation = glGetUniformLocation(mShader, "uniNormalTexture");
  m_UseNormalLocation = glGetUniformLocation(mShader, "uniUseNormal");

  mSpriteMap.reserve(500);
  Specify_Attributes(0);
}

void SpriteBatcher::Specify_Attributes(unsigned key)
{
  VAO* vao = new VAO();
  mVaoMap = vao;
  VBO* mvbo = new VBO();
  mMatrixVboMap = mvbo;

  GLuint location = glGetAttribLocation(mShader, "modelMatrix");

  for (int i = 0; i < 4; ++i)
  {
    glEnableVertexAttribArray(location + i);
    glVertexAttribPointer(location + i, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (const void*)(i * sizeof(glm::vec4)));
  }
  mColorVbo = new VBO();
  location = glGetAttribLocation(mShader, "color");
  glEnableVertexAttribArray(location);
  glVertexAttribPointer(location, 4, GL_FLOAT, GL_FALSE, 4 * sizeof GLfloat, 0);

  mTexVbo = new VBO();
  location = glGetAttribLocation(mShader, "texcoord");
  glEnableVertexAttribArray(location);
  glVertexAttribPointer(location, 4, GL_FLOAT, GL_FALSE, 4 * sizeof GLfloat, 0);

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  mVaoMap->unBind();
}

void SpriteBatcher::AddSprite(Sprite* sprite)
{
  //auto key = mSpriteMap.find(sprite->mpTextureData->mpAtlas->Get_ID());
  //if (key == mSpriteMap.end())
  //{
  //  Specify_Attributes(sprite->mpTextureData->mpAtlas->Get_ID());
  //}
  //mSpriteMap.push_back(sprite);
}

void SpriteBatcher::RemoveSprite(Sprite* sprite)
{
  auto it = std::find(mSpriteMap.begin(), mSpriteMap.end(), sprite);
  if (it != mSpriteMap.end())
  {
    mSpriteMap.erase(std::remove(mSpriteMap.begin(), mSpriteMap.end(), sprite), mSpriteMap.end());
  }
}

void SpriteBatcher::ChangeSpriteTexture(Sprite* sprite, Texture* newTexture)
{
  RemoveSprite(sprite);
  sprite->mpTextureData->mpAtlas = newTexture;
  AddSprite(sprite);
}

void SpriteBatcher::UploadSprite(Sprite* sprite, Math::Matrix4& matrix)
{
  unsigned key = sprite->mpTextureData->mpAtlas->Get_ID();
  //sprite->Draw(matrix, mMatrixVboMap, mColorVbo, mTexVbo);
  sprite->m_MVP = matrix;
  mSpriteMap.push_back(sprite);
}

void SpriteBatcher::Batch()
{
  // std::sort(mSpriteMap.begin(), mSpriteMap.end(), Sprite::SortFunction); -- oops
  glUseProgram(mShader);
  mVaoMap->bind();

  for (auto it = mSpriteMap.begin(); it != mSpriteMap.end();)
  {
    auto jt = it;
    int numSprites = 0;
    while (jt != mSpriteMap.end() && (*it)->mpTextureData->mpAtlas->Get_ID() == (*jt)->mpTextureData->mpAtlas->Get_ID())
    {
      ++jt;
    }

    for (auto kt = it; kt != jt; ++kt)
    {
      (*kt)->Draw(mMatrixVboMap, mColorVbo, mTexVbo);
      ++numSprites;
    }

    mMatrixVboMap->UploadData(GL_STREAM_DRAW);
    mColorVbo->UploadData(GL_STREAM_DRAW);
    mTexVbo->UploadData(GL_STREAM_DRAW);

    glActiveTexture(GL_TEXTURE0);
    (*it)->mpTextureData->mpAtlas->bind();
    glUniform1i(m_SamplerLocation, 0);

    glActiveTexture(GL_TEXTURE1);
    if ((*it)->mpNormalTextureData != nullptr)
    {
      (*it)->mpNormalTextureData->mpAtlas->bind();
      glUniform1i(m_UseNormalLocation, 1);
    }
    else
    {
      glBindTexture(GL_TEXTURE_2D, 0);
      glUniform1i(m_UseNormalLocation, 0);
    }
    glUniform1i(m_NormalSamplerLocation, 1);

    glActiveTexture(GL_TEXTURE0);

    glDrawArrays(GL_POINTS, 0, numSprites);
    it = jt;
  }

  mVaoMap->unBind();

  mSpriteMap.clear();
}

void SpriteBatcher::Free()
{
  delete mVaoMap, mMatrixVboMap, delete mColorVbo, delete mTexVbo;
}