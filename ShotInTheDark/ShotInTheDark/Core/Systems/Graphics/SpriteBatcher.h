// Author: Darthvader

#pragma once
struct Sprite;
class Texture;
class VertexArrayObject;
class VertexBufferObject;

class SpriteBatcher
{
public:
  static void Initialize();
  static void AddSprite (Sprite* sprite);
  static void RemoveSprite (Sprite* sprite);
  static void ChangeSpriteTexture(Sprite* sprite, Texture* newTexture);
  static void UploadSprite(Sprite* sprite, Math::Matrix4& matrix);
  static void Batch();
  static void Free();

private:
  static std::vector<Sprite*> mSpriteMap;
  static VAO* mVaoMap;
  static VBO* mMatrixVboMap;
  static VBO* mColorVbo;
  static VBO* mTexVbo;

  static GLuint mShader;
  static GLuint m_SamplerLocation;
  static GLuint m_NormalSamplerLocation;
  static void Specify_Attributes(unsigned key);

};