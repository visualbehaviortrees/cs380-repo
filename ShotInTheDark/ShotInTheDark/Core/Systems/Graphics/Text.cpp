//	File name:		Text.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Dereck Crouse
//	
//	All content � 2014 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "Text.hpp"
#include <string>
#include "../../Libraries/Math/Math2D.hpp"
#include "../../Components/Components.h"
#include "ResourceManager.h"

//std::map<char, Texture*> Font;
std::map<char, std::string> Font;

void InitializeText(void)
{
  std::string fontfile = ".png";

  for(char i = 'A'; i < 'Z'; ++i)
  {
    //std::cout << PutTogether(i,fontfile).c_str() << std::endl;

    //Font[i] = new Texture(PutTogether(i,fontfile).c_str());
    Font[i] = PutTogether(i,fontfile);
  }

  //Font[' '] = new Texture("_.png");
  Font[' '] = std::string("_.png");

  for(char i = '0'; i < '9'; ++i)
  {
    //Font[i] = new Texture(PutTogether(i,fontfile).c_str();
    Font[i] = PutTogether(i,fontfile);
  }
}

std::string PutTogether(char a, std::string b)
{
  std::string newstring;
  newstring += a;
  newstring += b;

  return newstring;
}

void WriteText(const std::string& Words, const Math::Point2D& Position, unsigned Size)
{
  if(ENGINE->OM->GetEntityByName(std::string("DigiPen_Splashes")).size())
    ENGINE->OM->DestroyAll(ENGINE->OM->GetEntityByName(std::string("DigiPen_Splashes")));

  Transform* digi_tran = new Transform();
  digi_tran->Position.Set(0.0f, 0.0f);
  digi_tran->rotation = 0.0f;
  digi_tran->scale.Set(9.0f, 5.0f);

  Drawable *digi_drawable = new Drawable();
  digi_drawable->a = 1.0f;
  *digi_drawable = Drawable::BlueScheme;
  digi_drawable->layer = 10;

  Sprite *digipen_logo_sprite = new Sprite(RESOURCES->Get_Texture("DigiPen_Splash.png"));

  Entity *Splash = ENGINE->Factory->Compose(3, digi_drawable, digipen_logo_sprite, digi_tran);
  Splash->SetName(std::string("DigiPen_Splashes"));
}

void WriteText(const std::string& Words, unsigned Size, const Math::Point2D& Position)
{


  Transform* digi_tran = new Transform();
  digi_tran->Position.Set(0.0f, 0.0f);
  digi_tran->rotation = 0.0f;
  digi_tran->scale.Set(9.0f, 5.0f);

  Drawable *digi_drawable = new Drawable();
  digi_drawable->a = 1.0f;
  *digi_drawable = Drawable::BlueScheme;
  digi_drawable->layer = 10;
  
  Mesh *digi_mesh = new Mesh();
  digi_mesh->Poly = Poly(5);
  digi_mesh->Poly.SetVert(0) = Math::Point2D(-1.0f, 1.0f);
  digi_mesh->Poly.SetVert(1) = Math::Point2D(-1.0f, -1.0f);
  digi_mesh->Poly.SetVert(2) = Math::Point2D(1.0f, -1.0f);
  digi_mesh->Poly.SetVert(3) = Math::Point2D(1.0f, 1.0f);
  digi_mesh->Poly.SetVert(4) = digi_mesh->Poly.GetVert(0);

  Sprite *digipen_logo_sprite = new Sprite(RESOURCES->Get_Texture("DigiPen_Splash.png"));

  Entity *Splash = ENGINE->Factory->Compose(4, digi_drawable, digi_mesh, digipen_logo_sprite, digi_tran);
  Splash->SetName(std::string("DigiPen_Splashes"));
}