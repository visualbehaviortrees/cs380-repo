//	File name:		Text.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Dereck Crouse
//	
//	All content � 2014 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include <map>
#include <string>
#include "Texture.hpp"
#include "../../Libraries/Math/Math2D.hpp"
#include "../../ObjectManager.hpp"

void InitializeText(void);
std::string PutTogether(char, std::string);

void WriteText(const std::string& Words, const Math::Point2D& Position, unsigned Size);
void WriteText(const std::string& Words, unsigned Size, const Math::Point2D& Position);