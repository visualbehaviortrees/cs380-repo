//	File name:		Texture.cpp
//	Project name:	Shot In The Dark
//	Author(s):		(Insert Name Here)
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "Texture.hpp"
#include "soil/SOIL.h"
#include <iostream>


Texture::Texture(const char* filename)
{
  Load_Texture (filename);
}


Texture::~Texture(void)
{
  const GLuint Tex[1] = {texture};
  glDeleteTextures(1,Tex);
}


void Texture::Load_Texture (const char* filename)
{
      // Load textures
    GLuint tex;
    glGenTextures (1, &tex);
    glBindTexture (GL_TEXTURE_2D, tex);
  
    int width, height;
    unsigned char* image = SOIL_load_image (filename, &width, &height, 0, SOIL_LOAD_RGBA);
    glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);

    if(image == NULL)
      std::cout<<"Error loading texture: "<<filename<<std::endl;
    else
      std::cout<<"Texture loaded correctly: "<<filename<<std::endl;
    SOIL_free_image_data (image);
  
    //glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    texture = tex;
}

