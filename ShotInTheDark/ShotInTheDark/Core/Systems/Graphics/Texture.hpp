//	File name:		Texture.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Gonzalo Rojo
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "OpenGL.hpp"
#include "GraphicsMath.hpp"

struct Bounds
{
  Bounds() : Min({0,1}), Max({ 1, 0 })
  {}
  Bounds(glm::vec2 min, glm::vec2 max) : Max(max), Min(min)
  {}
  glm::vec2 Min;
  glm::vec2 Max;
};

class Texture
{
public:
  Texture(void) {}
  Texture (const char* filename);
  ~Texture(void);
  void Load_Texture (const char*);
  inline GLuint Get_ID ()
  {
    return texture;
  }
  void bind ()
  {
    glBindTexture(GL_TEXTURE_2D, texture);
  }
  void unBind ()
  {
    glBindTexture(GL_TEXTURE_2D, 0);
  }
  void createTexture (int w, int h, bool isDepth = false)
  {
    glGenTextures (1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  }

  Bounds bounds;

private:
  GLuint texture;
};

struct TextureData
{
  std::string Name;
  Texture* mpAtlas;
  Bounds bounds;
};
