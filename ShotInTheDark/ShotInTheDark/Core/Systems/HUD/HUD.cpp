//	File name:		HUD.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Dereck Crouse
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include <vector>
#include "HUD.hpp"
#include "../../Engine.hpp"
#include "../../Components/Components.h"
#include "Text.hpp"
#include "../Graphics/ResourceManager.h"
#include "../Graphics/Graphics.hpp"
#include "../Gameplay/DMGameplay.hpp"
#include "../Input/InputSystem.hpp"
#include "../Physics/PhysicsSystem.hpp"

const unsigned HUD_BACK_LAYER = static_cast<unsigned>(-100);

// Hud layers are in front of almost everything else
const unsigned hud_layer_back = static_cast<unsigned>(-90);
const unsigned hud_layer_mid = static_cast<unsigned>(-50);
const unsigned hud_layer_front = static_cast<unsigned>(-30);

  // Takes a screen position and centers it to the camera
  Point2D ScreenToWorld(Hcoords screenPos)
  {
	  // Find out where our camera is
	  Point2D CameraPos = static_cast<Graphics*>(ENGINE->GetSystem(sys_Graphics))->GetCameraPosition();

	  return Point2D(CameraPos.x + screenPos.x, CameraPos.y + screenPos.y);

  }

void HUDsystem::Init()
{
  //REGISTER_COMPONENT(Player);


}

void HUDsystem::Shutdown()
{
	healthbars.clear();
}

void HUDsystem::Update(float dt)
{
	std::vector<Entity *> players = ENGINE->OM->FilterEntities(MC_Player | MC_Health);

	// Make or destroy healthbars if we need to
	if (players.size() != healthbars.size())
	{
		// Destroy all existing healthbars
		healthbars.clear();
		for (HUD_PlayerIcon p : playericons)
			p.DestroyEntitites();
		playericons.clear();
		// and add new ones
		Point2D player_hud_start = Point2D(-11.0f, 10.0f);
		for (Entity * p : players)
		{
			Vector2D player_hud_offset(5.8f, 0.0f);

			player_hud_offset = (float)(p->GET_COMPONENT(Player)->num - 1) * player_hud_offset;

      if (p->GET_COMPONENT(Player)->num > 2)
      {
        player_hud_offset += Vector2D(6.8f, 0.0f, 0.0f);
      }

			healthbars.push_back(std::make_tuple(
				HUD_Bar(),
				HUD_EggHealthBar(player_hud_start + player_hud_offset + Vector2D(0.0f, 0.25f, 0.0f), p->GET_COMPONENT(Health)->maxHealth), p));

			//std::get<0>(healthbars.back()).Create_Entity(player_hud_offset);

			playericons.push_back(HUD_PlayerIcon());
			playericons.back().Create_Entity(p->GET_COMPONENT(Player)->tex_icon, player_hud_offset);
		}

	}

	if (!players.empty())
	{
		// Update our health bar positions
		for (auto& hbar_player_pair : healthbars)
		{
			Health* player_health = std::get<2>(hbar_player_pair)->GET_COMPONENT(Health);
			//// BAR
			//std::get<0>(hbar_player_pair).Set_Percent(player_health->GetHealthPercent());
			//std::get<0>(hbar_player_pair).Update(dt);
			// EGGS
			std::get<1>(hbar_player_pair).SetHealth(player_health->health, player_health->maxHealth);
			std::get<1>(hbar_player_pair).Update(dt);
		}

		for (auto& p_icon : playericons)
		{
			p_icon.Update(dt);
		}
	}

	CheckAddPressStarts();
	// Update our enemy counter
	UpdateEnemyCounter();

	//std::vector<Entity*> enemy_list = ENGINE->OM->FilterEntities(MC_Behavior | MC_Sprite);
	//std::vector<std::string> enemy_icons;
	//for (Entity* e : enemy_list)
	//{
	//	enemy_icons.push_back("Strawberry_Move_Forward_1.png");
	//}
	//enemycounter.Refresh_Icons(enemy_icons);


	// Moves all our items to their screen position
	std::vector<Entity*> hud_list = ENGINE->OM->FilterEntities(MC_GUIItem | MC_Transform);
	for (Entity* hud_ent : hud_list)
	{
		Transform* hud_trans = hud_ent->GET_COMPONENT(Transform);
		GUIItem* hud_item = hud_ent->GET_COMPONENT(GUIItem);

		Point2D worldpos = ScreenToWorld(hud_item->screenPos);
		hud_trans->Position.Set(worldpos.x, worldpos.y);

		ObjectToWorld(hud_trans);
	}

	UpdateNavArrow(dt);
}

void HUDsystem::CheckAddPressStarts()
{
	// How many do we need?
	int icons_required = 0;
	for (int i = healthbars.size(); i < NUMBEROFJOYSTICKS; ++i)
	{
		if (CheckJSActive((JOYSTICKS)i))
			icons_required++;
	}

	// DEBUG
	//icons_required = 4 - healthbars.size();

	// Do we need to refresh em?
	if (icons_required == press_starts.size())
		return;

	ENGINE->OM->DestroyAll(press_starts);
	press_starts.clear();

	Point2D player_hud_start = Point2D(-11.0f, 10.0f);
	Vector2D player_hud_offset(5.0f, 0.0f);
	for (int i = 0; i < icons_required; ++i)
	{
		Transform* bgTran = new Transform();
		bgTran->scale = Vector2D(3.0f, 3.0f);
		Drawable* bgDraw = new Drawable();
		bgDraw->layer = hud_layer_back - 1;
		Sprite* bgSprite = new Sprite("PressStart.png");
		GUIItem* bgPause = new GUIItem();
		bgPause->screenPos = player_hud_start + (float)(healthbars.size() + i) * player_hud_offset;

		Entity *bgEntity = ENGINE->Factory->Compose(4,
			bgTran,
			bgDraw,
			bgSprite,
			bgPause
			);
		bgEntity->SetName(std::string("Background"));

		press_starts.push_back(bgEntity);
	
	}
}

void HUDsystem::CreateEnemyCounter()
{
	// Build the enemy counter text
	Transform* transform = new Transform();
	GUIItem* gui = new GUIItem();
	gui->screenPos = Point2D(-12.0f, -8.25f);
	Drawable* draw = new Drawable();
	SpriteText* text = new SpriteText("Enemies remaining: ");
	text->fontSize = 64;
	text->hAlign = Alignment_BottomRight;


	enemycountertext = ENGINE->Factory->Compose(4,
		transform, draw, gui, text);
}

void HUDsystem::UpdateEnemyCounter()
{
	if (enemycountertext == nullptr)
		return;

	int enemies_remaining = ENGINE->OM->FilterEntities(MC_Behavior | MC_Sprite).size();
	
	std::string counter_text;
	counter_text = "Enemies remaining: " + std::to_string(enemies_remaining);

	enemycountertext->GET(SpriteText)->text = counter_text;

	enemycountertext->SetName("Enemy Counter");
}

void HUDsystem::BuildHudItems(void)
{
	//healthbars.push_back(HUD_Bar(), );
	//healthbars.front().first.Create_Entity();
	
	// Make the background
  
	Transform* bgTran = new Transform();
	bgTran->scale = Vector2D(15.0f, 9.0f);
	Drawable* bgDraw = new Drawable();
	bgDraw->layer = hud_layer_back-1;
	Sprite* bgSprite = new Sprite("hud_base.png");
	GUIItem* bgPause = new GUIItem();
	bgPause->screenPos = Point2D(0.0f, 2.5f);

	Entity *bgEntity = ENGINE->Factory->Compose(4,
		bgTran,
		bgDraw,
		bgSprite,
		bgPause
		);
	bgEntity->SetName(std::string("Background"));


	//bgTran = new Transform();
	//bgTran->scale = Vector2D(16.0f, 10.0f);
	//bgTran->rotation = 180.0f;
	//bgDraw = new Drawable();
	//bgDraw->layer = hud_layer_back - 1;
	//bgSprite = new Sprite("base_main.png");
	//bgPause = new GUIItem();
	//bgPause->screenPos = Point2D(0.0f, 0.0f);

	//bgEntity = ENGINE->Factory->Compose(4,
	//	bgTran,
	//	bgDraw,
	//	bgSprite,
	//	bgPause
	//	);
	//bgEntity->SetName(std::string("Background"));

	// Create the navigation arrow
	Transform* arrowTran = new Transform();
	arrowTran->scale = Vector2D(1.0f, 1.0f);
	arrowTran->Position = Point2D(0.0f, 0.0f);
	Drawable* arrowDraw = new Drawable();
	*arrowDraw = Drawable::RedScheme;
	//arrowDraw->layer = hud_layer_back - 1;
	arrowDraw->mpMesh = Mesh::Get_Mesh("Strawberry");
	GUIItem* arrowPause = new GUIItem();
	arrowPause->screenPos = Point2D(-14.0f, -8.0f);

	navArrow = ENGINE->Factory->Compose(3,
		arrowTran,
		arrowDraw,
		arrowPause
		);
	navArrow->SetName(std::string("NavArrow"));

	CreateEnemyCounter();

}



void HUDsystem::DestroyHudItems(void)
{
	//for (auto it : playericons)
	//{
	//	it.Destroy_Entities();
	//}
	healthbars.clear();
	press_starts.clear();
}

void HUDsystem::OnPlayerHit(Entity* player)
{
	float shakefactor = 90.0f * (player->GET_COMPONENT(Health)->GetHealth());
	float shaketime = player->GET_COMPONENT(Health)->safeTime * 2.0f;
	playericons.at(player->GET_COMPONENT(Player)->num - 1).Shake(shakefactor, shaketime);
}

void HUDsystem::UpdateNavArrow(float dt)
{
	std::vector<Entity*> enemies = ENGINE->OM->FilterEntities(MC_Transform | MC_Behavior);
	// Use the center of the arrow for angle calculations
	Point2D arrowCenter = navArrow->GET(Transform)->Position;
	// And the center of the camera (room center) for distance calculations
	Point2D camCenter = GETSYS(Graphics)->GetCameraPosition();
	float current_angle = navArrow->GET_COMPONENT(Transform)->rotation;
	float new_angle = current_angle;
	float best_dist = -1.0f;
	// Go through the enemies, and rotate the cursor based on how close each one is
	for (Entity* e : enemies)
	{
		float dist = (e->GET(Transform)->Position - camCenter).SquareLength();
		if (best_dist < 0.0f || dist <= best_dist)
		{
			best_dist = dist;
			float direction = Math::VectorToDegree(e->GET(Transform)->Position - arrowCenter);
			new_angle = direction + 90.0f;
		}
	}

	navArrow->GET_COMPONENT(Transform)->rotation = current_angle + (new_angle - current_angle) * dt;

}


void HUDsystem::SendMsg(Entity* a, Entity* b, message m)
{
	switch (m)
	{
	case MSG_Clear_Dungeon:
		break;
	case MSG_Built_Dungeon:
		//BuildHudItems();
		break;
	case MSG_Room_Change:
		break;
	default:
		break;
	}
}

