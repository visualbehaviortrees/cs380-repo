//	File name:		HUD.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Dereck Crouse
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../../System.hpp"

/* All HUD and GUI items are guaranteed to be on or above( >=) this layer.
 Currently, HUD and GUI items are forced to the camera's position, but ideally 
 you can just not apply the camera mtx to avoid unnecesary computations.

 When you stop applying the camera mtx, change Update to stop object's moving 
 their world space unnecesarily
*/
extern const unsigned HUD_BACK_LAYER;
extern const unsigned hud_layer_back;
extern const unsigned hud_layer_mid;
extern const unsigned hud_layer_front;


// Forward declarations
#include "HUDBar.h"
#include "HUDPlayerIcon.h"
#include "HUDIconStack.h"
#include "HUDEggHealthBar.h"

class HUDsystem : public System
{
public:
	HUDsystem(void) :System(sys_HUDsystem, "HUD System"),
		enemycountertext(0),
		enemycounter(Point2D(5.0f, -8.0f), 10.0f, "Strawberry_Move_Forward_1.png", 0)
	{}
	void Init(void);
  void Update(float dt);
	void Shutdown(void);
	void BuildHudItems(void);
	void DestroyHudItems(void);
	void SendMsg(Entity* a, Entity* b, message);


	void OnPlayerHit(Entity* player);
private:
	void UpdateNavArrow(float dt);
	void CreateEnemyCounter();
	void UpdateEnemyCounter();
	void CheckAddPressStarts();
	//First is the bar, the second is the pointer to the player to check health for
	std::vector < std::tuple<HUD_Bar, HUD_EggHealthBar, Entity*> > healthbars;

	std::vector<HUD_PlayerIcon> playericons;

	std::vector<Entity*> press_starts;

	Entity* navArrow;

	Entity* enemycountertext;
	// Deprecated
	HUD_IconStack enemycounter;
};



// Takes a screen position and centers it to the camera
Point2D ScreenToWorld(Hcoords screenPos);