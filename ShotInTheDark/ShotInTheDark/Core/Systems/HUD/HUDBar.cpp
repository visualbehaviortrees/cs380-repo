//	File name:		HUDBar.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "HUDBar.h"
#include "HUD.hpp"

void HUD_Bar::Create_Entity(Vector2D offset)
{
	// Make the background
	Transform* hud_bg_trans = new Transform();
	hud_bg_trans->scale = Vector2D(width, 2.0f);
	GUIItem* hud_bg_item = new GUIItem();
	hud_bg_item->screenPos = Point2D(-8.0f, 7.5f) + offset;
	Drawable* hud_bg_draw = new Drawable();
	hud_bg_draw->layer = hud_layer_back;
	Sprite* hud_bg_sprite = new Sprite("HealthBar_Border.png");
	back_ent = ENGINE->Factory->Compose(4,
		hud_bg_trans,
		hud_bg_item,
		hud_bg_draw,
		hud_bg_sprite
		);

	back_ent->SetName("HUD_Healthbar_bg");

	// Then make the actual bar
	Transform* hud_fg_trans = new Transform();
	hud_fg_trans->scale = Vector2D(width, 2.0f);
	GUIItem* hud_fg_item = new GUIItem();
	hud_fg_item->screenPos = Point2D(5.0f, 5.0f) + offset;;
	Drawable* hud_fg_draw = new Drawable();
	hud_fg_draw->layer = hud_layer_mid;
	//	hud_fg_draw->b = 0.0f;
	Sprite* hud_fg_sprite = new Sprite("HealthBar_Base.png");

	front_ent = ENGINE->Factory->Compose(4,
		hud_fg_trans,
		hud_fg_item,
		hud_fg_draw,
		hud_fg_sprite
		);

	front_ent->SetName("HUD_Healthbar_fg");
}

void HUD_Bar::Set_Percent(float percent)
{
	target_percent = clamp(percent, 0.0f, 1.0f);
}


Entity* HUD_Bar::Get_Front_Entity(void)
{
	return front_ent;
}

Entity* HUD_Bar::Get_Background_Entity(void)
{
	return back_ent;
}

void HUD_Bar::Update(float dt)
{
	current_percent += (target_percent - current_percent) *(lerp_speed * dt);

	// Move and scale our bar
	front_ent->GET_COMPONENT(Transform)->scale.x = width * current_percent;
	Point2D front_pos = back_ent->GET_COMPONENT(GUIItem)->screenPos;
	front_pos.x -= width;
	front_pos.x += width * current_percent;
	front_pos.x += (1.0f - current_percent) * sprite_slop;

	front_ent->GET_COMPONENT(GUIItem)->screenPos = front_pos;
}
