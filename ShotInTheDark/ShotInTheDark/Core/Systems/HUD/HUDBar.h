//	File name:		HUDBar.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.
#pragma once

#include "../../System.hpp"

class HUD_Bar
{
public:

	HUD_Bar(float w = 2.0f)
		: width(w), current_percent(1.0f), target_percent(1.0f), front_ent(nullptr), back_ent(nullptr) {};

	void Create_Entity(Vector2D offset = Vector2D( 0.0f, 0.0f) ); // technically makes two entities (bg & bar)...
	Entity* Get_Front_Entity(void);
	Entity* Get_Background_Entity(void);

	void Destroy_Entities(void);

	void Update(float dt);
	void Set_Percent(float percent);

private:
	Entity* front_ent;
	Entity* back_ent;

	const float lerp_speed = 10.0f;
	const float sprite_slop = 0.25f; // How far off the image is from the actual bar
	float width;


	float current_percent;
	float target_percent;
};
