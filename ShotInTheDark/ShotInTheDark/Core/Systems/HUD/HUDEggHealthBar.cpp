//	File name:		HUDEggHealthBar.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include <stdafx.h>
#include "HUDEggHealthBar.h"

std::vector<std::string> HUD_EggHealthBar::GetEggList()
{
	std::vector<std::string> eggList;
	float hp = current_hp;
	float m_hp = max_hp;

	while (m_hp >= half_egg_health_value * 2.0f)
	{
		// Decide what egg to add
		if (hp >= 2.0f * half_egg_health_value)
			eggList.push_back(egg_full);
		else if (hp >= half_egg_health_value)
			eggList.push_back(egg_half);
		else
			eggList.push_back(egg_empty);

		//reduce our health and iterate
		hp -= 2.0f * half_egg_health_value;
		m_hp -= 2.0f * half_egg_health_value;
	}

	return eggList;
}


void HUD_EggHealthBar::SetHealth(float current, float max)
{
	// Is the difference enough to rebuild our entities?
	current_hp = current;
	max_hp = max;
	eggs.Refresh_Icons(GetEggList());
}

void HUD_EggHealthBar::Update(float dt)
{
	// Placeholder
}