//	File name:		HUDEggHealthBar.h
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../../System.hpp"
#include "HUDIconStack.h"

class HUD_EggHealthBar
{
public:
	HUD_EggHealthBar(Point2D position, float max_health = 0.0f)
		: max_hp(max_health), current_hp(max_health),
		eggs(position, 3.0f, egg_full, (int)(max_health / half_egg_health_value))
	{
		eggs.Create_Entities();
		//bar.Create_Entity();
	}

	~HUD_EggHealthBar()
	{
		eggs.Destroy_Entities();
	}

	void SetHealth(float current, float max);
  void Update(float dt);



private:
	std::vector<std::string> GetEggList();

	const float half_egg_health_value = 1.5f;
	float max_hp;
	float current_hp;

	const std::string egg_full = "HealthBar_Egg1.png";
	const std::string egg_half = "HealthBar_Egg2.png";
	const std::string egg_empty = "HealthBar_Egg3.png";

	HUD_IconStack eggs;
};
