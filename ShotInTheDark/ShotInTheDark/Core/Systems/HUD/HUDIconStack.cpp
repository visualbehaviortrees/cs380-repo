//	File name:		HUDIconStack.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Dereck Crouse
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.
#include <stdafx.h>

#include "HUDIconStack.h"
#include "HUD.hpp"

// For drawing many icons
HUD_IconStack::HUD_IconStack(Point2D c, float w, std::initializer_list<std::string> icons)
	:start(c - Vector2D(w * 0.5f, 0)), width(w)
{
	for (std::string s : icons)
		icon_list.push_back(s);

	Update_Increment();
}

// For drawing one icon repeatedly
HUD_IconStack::HUD_IconStack(Point2D c, float w, std::string icon, int num)
	:start(c - Vector2D(w * 0.5f, 0)), width(w)
{
	for (int i = 0; i < num; ++i)
		icon_list.push_back(icon);
	Update_Increment();
}

void HUD_IconStack::Refresh_Icons(std::initializer_list<std::string> icons)
{


	icon_list = icons;
	Update_Increment();
}

void HUD_IconStack::Refresh_Icons(std::vector<std::string> icons)
{
	if (icons != icon_list)
	{
		Destroy_Entities();
		icon_list = icons;
		Update_Increment();
		Create_Entities();
	}
}

void HUD_IconStack::Update_Increment(void)
{
	increment = Vector2D(width / icon_list.size(), 0.0f);
}


std::vector<Entity*>& HUD_IconStack::Get_Entities(void)
{
	return icon_ents;
}

void HUD_IconStack::Create_Entities(void)
{
	Point2D currentPos = start;
	for (std::string filename : icon_list)
	{
		Transform* icon_trans = new Transform();
		icon_trans->scale = Vector2D(0.75f, 0.75f);
		GUIItem* icon_pause = new GUIItem();
		icon_pause->screenPos = currentPos;
		currentPos += increment;
		Drawable* icon_draw = new Drawable();
		icon_draw->layer = hud_layer_mid;
		Sprite* icon_sprite = new Sprite(filename);
		icon_ents.push_back(ENGINE->Factory->Compose(4,
			icon_trans,
			icon_pause,
			icon_draw,
			icon_sprite
			));
		icon_ents.back()->SetName("HUD_Icon");
	}
}

void HUD_IconStack::Destroy_Entities(void)
{
	for (Entity* e : icon_ents)
		ENGINE->OM->DestroyEntity(e);
	icon_ents.clear();
}
