//	File name:		HUDIconStack.h
//	Project name:	Shot In The Dark
//	Author(s):		Dereck Crouse
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../../System.hpp"

// Draws a series of sprites evenly spaced in a line
class HUD_IconStack
{
public:
	// For drawing many icons
	HUD_IconStack(Point2D center, float width, std::initializer_list<std::string> icons);
	// For drawing one icon repeatedly
	HUD_IconStack(Point2D center, float width, std::string icon, int num);

	void Refresh_Icons(std::initializer_list<std::string> icons);
	void Refresh_Icons(std::vector<std::string> icons);

	std::vector<Entity*>& Get_Entities(void);
	void Create_Entities(void);
	void Destroy_Entities(void);

	std::vector<std::string>& GetIconList(void) { return icon_list; }
private:
	void Update_Increment(void);
	// The leftmost point on the list (in screenspace)
	Point2D start;
	float width;
	// How far each item moves over (in screenspace
	Vector2D increment;

	std::vector<std::string> icon_list;
	std::vector<Entity*> icon_ents;
};
