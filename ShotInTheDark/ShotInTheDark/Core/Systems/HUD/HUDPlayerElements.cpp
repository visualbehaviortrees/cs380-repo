//	File name:		HUDPlayerElements.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.
#include <stdafx.h>

#include "HUDPlayerElements.h"



HUD_PlayerElements::HUD_PlayerElements(Entity * p, Point2D pos)
	: player(p), healtheggs(pos, p->GET_COMPONENT(Health)->maxHealth),
	powerups(pos, 2.0f, {}), healthbar(), icon()
{

	
}

HUD_PlayerElements::~HUD_PlayerElements()
{

}

void HUD_PlayerElements::Update(float dt)
{

}