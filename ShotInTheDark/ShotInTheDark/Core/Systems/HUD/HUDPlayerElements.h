//	File name:		HUDPlayerElements.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.
#pragma once

#include "HUD.hpp"
#include "HUDBar.h"
#include "HUDEggHealthBar.h"
#include "HUDIconStack.h"
#include "HUDPlayerIcon.h"
#include "../../System.hpp"

class HUD_PlayerElements
{
	HUD_PlayerElements(Entity* player, Point2D pos);
	~HUD_PlayerElements();
	void Update(float dt);

private:
	friend class HUDsystem;

	const Vector2D eggs_offset		= Vector2D(2.0f,1.0f);
	const Vector2D healthbar_offset = Vector2D(0.0f, 0.0f);
	const Vector2D powerups_offset	= Vector2D(0.0f, 0.0f);
	const Vector2D icon_offset		= Vector2D(0.0f, 0.0f);


	Entity* player;
	HUD_Bar healthbar;
	HUD_EggHealthBar healtheggs;
	HUD_IconStack powerups;
	HUD_PlayerIcon icon;
};
