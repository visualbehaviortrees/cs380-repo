//	File name:		HUD.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include <stdafx.h>

#include "HUDPlayerIcon.h"
#include "HUD.hpp"

void HUD_PlayerIcon::Create_Entity(std::string icon_filename, Vector2D offset)
{
	// Create the frying pan
	Transform* hud_bg_trans = new Transform();
	hud_bg_trans->scale = Vector2D(2.0f, 2.0f);
	GUIItem* hud_bg_item = new GUIItem();
	hud_bg_item->screenPos = Point2D(-13.5f, 9.0f) + offset;
	Drawable* hud_bg_draw = new Drawable();
	hud_bg_draw->layer = hud_layer_back;
	Sprite* hud_bg_sprite = new Sprite("Character_Icon_Base_Pan.png");
	back_ent = ENGINE->Factory->Compose(4,
		hud_bg_trans,
		hud_bg_item,
		hud_bg_draw,
		hud_bg_sprite
		);
	back_ent->SetName("HUD_PlayerIcon_bg");

	// Create the player icon
	Transform* hud_fg_trans = new Transform();
	hud_fg_trans->scale = Vector2D(1.5f, 1.5f);
	GUIItem* hud_fg_item = new GUIItem();
	hud_fg_item->screenPos = hud_bg_item->screenPos;
	// Move down a certain offset
	hud_fg_item->screenPos.y -= (fg_offset* hud_bg_trans->scale.y);
	Drawable* hud_fg_draw = new Drawable();
	hud_fg_draw->layer = hud_layer_front;
	Sprite* hud_fg_sprite = new Sprite(icon_filename);

	front_ent = ENGINE->Factory->Compose(4,
		hud_fg_trans,
		hud_fg_item,
		hud_fg_draw,
		hud_fg_sprite
		);
	front_ent->SetName("HUD_PlayerIcon_fg");
}

Entity* HUD_PlayerIcon::Get_Front_Entity(void)
{
	return front_ent;
}

Entity* HUD_PlayerIcon::Get_Background_Entity(void)
{
	return back_ent;
}

void HUD_PlayerIcon::Update(float dt)
{
	if (!swinging)
		return;

	if (shake_duration > 0.0f)
		// Update our shake duration
		shake_duration -= dt;
	else
		target_angle = 0.0f;

	Transform* bg_trans = back_ent->GET_COMPONENT(Transform);
	Transform* fg_trans = front_ent->GET_COMPONENT(Transform);
	GUIItem* bg_hudinfo = back_ent->GET_COMPONENT(GUIItem);
	GUIItem* fg_hudinfo = front_ent->GET_COMPONENT(GUIItem);

	// Update our angle
	if (target_angle - current_angle > 0.0f)
		current_angle = current_angle + shake_speed * dt;
	else
		current_angle = current_angle - shake_speed * dt;


	// Stop swinging if both current and target angles are near zero
	if (std::abs(target_angle) == 0.0f && std::abs(current_angle) <= shake_speed * dt)
	{
		swinging = false;
		// reset our location
		bg_trans->rotation = 0.0f;
		fg_trans->rotation = 0.0f;
		bg_hudinfo->screenPos = rest_screen_location;
		fg_hudinfo->screenPos = rest_screen_location;
		fg_hudinfo->screenPos.y -= (fg_offset * bg_trans->scale.y);
		return;
	}

	// Swing in the different direction if we've made it
	if (target_angle != 0.0f && std::abs(target_angle) < std::abs(current_angle))
	{
		current_angle = target_angle;
		target_angle = -target_angle;
	}

	// Rotate our sprites about the top of the bg image		
	/* Ideally, the sprite rotates around the top point of the handle, but this is taking too long for a minor detail.
	Once you figure out the affine, uncomment the commented lines below and all of the other logic is in place
	(including error removal)*/

	Vector2D rot_point = bg_hudinfo->screenPos;
	//rot_point.y += 0.5f * bg_trans->scale.y;

	//Affine A = Rot (_DEG2RAD_ * current_angle);

	//fg_hudinfo->screenPos = A * fg_hudinfo->screenPos;
	fg_trans->rotation = current_angle;

	//bg_hudinfo->screenPos = A * bg_hudinfo->screenPos;
	bg_trans->rotation = current_angle;

}

void HUD_PlayerIcon::Shake(float ammount, float time)
{
	// Only one at a time please
	if (swinging)
		return;

	// Store our starting point so we can revert to it later to remove sliding error
	rest_screen_location = back_ent->GET_COMPONENT(GUIItem)->screenPos;

	target_angle = ammount;
	shake_duration = time;
	swinging = true;
}

void HUD_PlayerIcon::DestroyEntitites()
{
	ENGINE->OM->DestroyEntity(front_ent);
	ENGINE->OM->DestroyEntity(back_ent);
}
