//	File name:		HUD.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../../System.hpp"

class HUD_PlayerIcon
{
public:
	HUD_PlayerIcon() : shake_duration(0.0f), current_angle(0.0f), target_angle(0.0f), swinging(false), front_ent(nullptr), back_ent(nullptr){  }

	// technically makes two entities (bg & icon)...
	void Create_Entity(std::string icon_filename, Vector2D offset = Vector2D{ 0.0f, 0.0f });
	Entity* Get_Front_Entity(void);
	Entity* Get_Background_Entity(void);
	void DestroyEntitites(void);
	void Update(float dt);
	void Shake(float amount, float time);

private:
	Entity* front_ent;
	Entity* back_ent;

	// How fast we swing from one side to the other, in deg/s
	const float shake_speed = 360.0f;
	// How far down (y-axis) we have to offset our front image
	const float fg_offset = 0.51f;
	// Our original point, to reset to to correct swinging errors
	Point2D rest_screen_location;

	bool swinging;
	float shake_duration;
	float current_angle;
	float target_angle;
};
