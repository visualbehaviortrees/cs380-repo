////	File name:		Text.cpp
////	Project name:	Shot In The Dark
////	Author(s):		Dereck Crouse
////	
////	All content � 2014 DigiPen (USA) Corporation, all rights reserved.
//
#include "stdafx.h"
//#include "Text.hpp"
//#include <string>
//#include "../../Libraries/Math/Math2D.hpp"
//#include "../../Components/Components.h"
//#include "../Graphics/ResourceManager.h"
//
////std::map<char, Texture*> Font;
//std::map<char, std::string> Font;
//
//void InitializeText(void)
//{
//  // std::string fontfile = ".png";
//  // 
//  // for(char i = 'A'; i < 'Z'; ++i)
//  // {
//  //   //std::cout << PutTogether(i,fontfile).c_str() << std::endl;
//  // 
//  //   //Font[i] = new Texture(PutTogether(i,fontfile).c_str());
//  //   Font[i] = PutTogether(i,fontfile);
//  // }
//  // 
//  // //Font[' '] = new Texture("_.png");
//  // Font[' '] = std::string("_.png");
//  // 
//  // for(char i = '0'; i < '9'; ++i)
//  // {
//  //   //Font[i] = new Texture(PutTogether(i,fontfile).c_str();
//  //   Font[i] = PutTogether(i,fontfile);
//  // }
//}
//
//std::string PutTogether(char a, std::string b)
//{
//  std::string newstring;
//  newstring += a;
//  newstring += b;
//  
//  return newstring;
//}
//
//void WriteText(const std::string& Words, const Math::Point2D& Position, unsigned Size)
//{
//  WriteText(Words, Size, Position);
//}
//
//void WriteText(const std::string& Words, unsigned Size, const Math::Point2D& Position)
//{
//  for(unsigned i = 0; i < Words.size(); ++i)
//  {
//    Transform* TextTransform = new Transform();
//    TextTransform->Position.Set(0.0f, 0.0f);
//    TextTransform->rotation = 0.0f;
//    TextTransform->scale.Set(9.0f, 5.0f);
//    
//    Drawable* TextDrawable = new Drawable();
//    TextDrawable->a = 1.0f;
//    *TextDrawable = Drawable::BlueScheme;
//    TextDrawable->layer = 10;
//    
//    Sprite *TextSprite = new Sprite(RESOURCES->Get_Texture(Font[Words[i]]));
//    
//    Entity *TextEntity = ENGINE->Factory->Compose(3, TextTransform, TextDrawable, TextSprite);
//    TextEntity->SetName(std::string("Text"));
//  }
//}