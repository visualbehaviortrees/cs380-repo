//	File name:		InputSystem.cpp
//	Project name:	Shot In The Dark
//	Author(s):		James Schmidt
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "../Input/InputSystem.hpp"
#include "../Graphics/Graphics.hpp"
#include "../../Engine.hpp"
#include "Joystick.hpp"
#include "../../Libraries/AntTweakBar/AntTweakBar.hpp"
#include "Systems/LevelEditor/Level_Creator.hpp"
#define CONTROLLERDEBUG 0

KEYSTATE currKeys;
KEYSTATE lastKeys;
MOUSESTATE currMouse;
MOUSESTATE lastMouse;
JOYSTICKBUTTONSTATE currJSButtons;
JOYSTICKBUTTONSTATE lastJSButtons;
JOYSTICKBUTTONSTATE bolleanJSCheck;
JOYSTICKAXISSTATE currJSAxis;
//JOYSTICKAXISSTATE lastJSAxis;
int currMods;

Joystick joysticks[JOYSTICKS::NUMBEROFJOYSTICKS - JOYSTICKS::JOYSTICK_1];

InputSystem::InputSystem() : System(sys_Input, "Input System")
{
  runWhilePaused = true;
}

InputSystem::~InputSystem()
{
  
}

void InputSystem::Init(void)
{
  for(int i = 0; i < NUMBEROFJOYSTICKS; ++i)
  {
    for(int j = 0; j < NUMBEROFJSBUTTONS; ++j)
    {
      bolleanJSCheck.state[i][j] = 0;
    }
  }
}

void InputSystem::Update(float dt)
{
  lastKeys = currKeys;
  lastMouse = currMouse;
  lastJSButtons = currJSButtons;

  for (int i = 0; i < NUMBEROFJOYSTICKS; ++i)
  {
    for (int j = 0; j < NUMBEROFJSBUTTONS; ++j)
    {
      currJSButtons.state[i][j] = 0;
    }
  }

  for (int i = 0; i < NUMBEROFJOYSTICKS; ++i)
  {
    for (int j = 0; j < NUMBEROFJSAXIS; ++j)
    {
      currJSAxis.state[i][j] = 0;
    }
  }

  glfwPollEvents();

  if(windowIsActive)
    PollJoyStickEvents();
}

void InputSystem::SendMsg(Entity *, Entity*, message msg)
{
  switch (msg)
  {
    case MSG_LoseFocus:
      windowIsActive = false;
      break;

    case MSG_GainFocus:
      windowIsActive = true;
      break;

    default:
      break;
  }
}

void InputSystem::Shutdown(void)
{
  
}

void DragNDropHandler(GLFWwindow* window, int file_count, const char** file)
{
  if (ENGINE->sysActive(sys_EditorSystem))
  {
    if (file_count > 2)
    {
      std::cout << "CANNOT HANDLE MULTIPLE FILES";
      return;
    }

    const char* filename = file[0];

    int position = 0;
    while (filename)
    {
      if (*filename == '.')
      {
        while (*filename != '\\')
        {
          if (position == 0)
            return;

          --filename;
          --position;
        }


        ++filename;
        string filepath("Core/Systems/Gameplay/Rooms/");
        filepath.append(filename);
        
        EditorSystem* ES = dynamic_cast<EditorSystem*>(ENGINE->getCurrentGamestate());

        if (ES)
        {
          ES->Load_Level(filepath);
          return;
        }
      }
      ++position;
      ++filename;
    }
  }
}

void KeyboardHandler(GLFWwindow* window, int Button, int Scancode, int Action, int Mods)
{
  if (Action == GLFW_REPEAT)
  {
    return;
  }

  if(Toggle_AntTweakBar)
  {
    int handled = TwEventKeyGLFW(Button, Action);

	if (!handled)
	{
		switch (Action)
		{
		case GLFW_RELEASE:
			currKeys.state[Button] = GLFW_RELEASE;
			break;
		case GLFW_PRESS:
			currKeys.state[Button] = GLFW_PRESS;
			break;
		}
	}
  }

  else
  {
    if(Button != GLFW_KEY_UNKNOWN)
    {
      switch(Action)
      {
        case GLFW_RELEASE:
          currKeys.state[Button] = GLFW_RELEASE;
          break;
        case GLFW_PRESS:
          currKeys.state[Button] = GLFW_PRESS;
          break;
      }
    }
  }

  currMods = Mods;
}

void MouseButtonHandler(GLFWwindow* window, int Button, int Action, int Mods)
{
  TwEventMouseButtonGLFW(Button, Action);
  switch(Action)
  {
    case GLFW_RELEASE:
      currMouse.state[Button] = GLFW_RELEASE;
      break;
    case GLFW_PRESS:
      currMouse.state[Button] = GLFW_PRESS;
      break;
  }

  currMods = Mods;
}

void MouseCurserHandler(GLFWwindow* window, double x, double y)
{
  TwMouseMotion(int(x), int(y));
}

void MouseWheelHandler(GLFWwindow* window, double xoffset, double yoffset)
{
  Window* getting_main_cam = GETSYS(Graphics)->GetCurrentWindow();

  if (yoffset < 0)
  {
    getting_main_cam->main_Cam->size *= 1.10f;
    getting_main_cam->main_Cam->Resize(getting_main_cam->Window_aspect);
    getting_main_cam->CameraToNDC = Math::Scale(getting_main_cam->NDC_width / getting_main_cam->main_Cam->width, getting_main_cam->NDC_height / getting_main_cam->main_Cam->height);
  }
  else
  {
    getting_main_cam->main_Cam->size /= 1.10f;
    getting_main_cam->main_Cam->Resize(getting_main_cam->Window_aspect);
    getting_main_cam->CameraToNDC = Math::Scale(getting_main_cam->NDC_width / getting_main_cam->main_Cam->width, getting_main_cam->NDC_height / getting_main_cam->main_Cam->height);
  }
  TwEventMouseWheelGLFW((int)(yoffset));
}

bool CheckAllInputRight()
{
	// Check all possible joysticks
	for (int i = JOYSTICK_1; i < NUMBEROFJOYSTICKS; ++i)
	{
		// Check the D pad
		if (CheckJSButtonTriggered((JOYSTICKS)i, JOYSTICK_BUTTONS::XBOX_DPAD_RIGHT))
			return true;
		// Check the Left stick
		if (CheckJSAxis((JOYSTICKS)i, LEFT_X_AXIS) > LEFT_AXIS_THRESHOLD)
			return true;
		// Check the right stick
		if (CheckJSAxis((JOYSTICKS)i, RIGHT_X_AXIS) > RIGHT_AXIS_THRESHOLD)
			return true;
	}

	// Check Keyboard
	return
		CheckKeyTriggered(KEY_D) ||
		CheckKeyTriggered(KEY_RIGHT);
}

bool CheckAllInputDown()
{
  // Check all possible joysticks
  for (int i = JOYSTICK_1; i < NUMBEROFJOYSTICKS; ++i)
  {
    // Check the D pad
    if (CheckJSButtonTriggered((JOYSTICKS)i, JOYSTICK_BUTTONS::XBOX_DPAD_DOWN))
      return true;
    // Check the Left stick
    if (-CheckJSAxis((JOYSTICKS)i, LEFT_Y_AXIS) > LEFT_AXIS_THRESHOLD)
      return true;
    // Check the right stick
    if (-CheckJSAxis((JOYSTICKS)i, RIGHT_Y_AXIS) > RIGHT_AXIS_THRESHOLD)
      return true;
  }

  // Check Keyboard
  return
    CheckKeyTriggered(KEY_S) ||
    CheckKeyTriggered(KEY_DOWN);
}

bool CheckAllInputUp()
{
  // Check all possible joysticks
  for (int i = JOYSTICK_1; i < NUMBEROFJOYSTICKS; ++i)
  {
    // Check the D pad
    if (CheckJSButtonTriggered((JOYSTICKS)i, JOYSTICK_BUTTONS::XBOX_DPAD_UP))
      return true;
    // Check the Left stick
    if (CheckJSAxis((JOYSTICKS)i, LEFT_Y_AXIS) > LEFT_AXIS_THRESHOLD)
      return true;
    // Check the right stick
    if (CheckJSAxis((JOYSTICKS)i, RIGHT_Y_AXIS) > RIGHT_AXIS_THRESHOLD)
      return true;
  }

  // Check Keyboard
  return
    CheckKeyTriggered(KEY_W) ||
    CheckKeyTriggered(KEY_UP);
}

bool CheckAllInputLeft()
{
	// Check all possible joysticks
	for (int i = JOYSTICK_1; i < NUMBEROFJOYSTICKS; ++i)
	{
		// Check the D pad
		if (CheckJSButtonTriggered((JOYSTICKS)i, JOYSTICK_BUTTONS::XBOX_DPAD_LEFT))
			return true;
		// Check the Left stick
		if (-CheckJSAxis((JOYSTICKS)i, LEFT_X_AXIS) > LEFT_AXIS_THRESHOLD)
			return true;
		// Check the right stick
		if (-CheckJSAxis((JOYSTICKS)i, RIGHT_X_AXIS) > RIGHT_AXIS_THRESHOLD)
			return true;
	}

	// Check Keyboard
	return
		CheckKeyTriggered(KEY_A) ||
		CheckKeyTriggered(KEY_LEFT);
}

bool CheckAllInputEnter()
{
	// Check all possible joysticks
	for (int i = JOYSTICK_1; i < NUMBEROFJOYSTICKS; ++i)
	{
		// Check the start button
		if (CheckJSButtonTriggered((JOYSTICKS)i, JOYSTICK_BUTTONS::XBOX_START))
			return true;
		// Check the A button
		if (CheckJSButtonTriggered((JOYSTICKS)i, JOYSTICK_BUTTONS::XBOX_A))
			return true;
	}

	// Check Keyboard
	return
		CheckKeyTriggered(KEY_SPACE) ||
		CheckKeyTriggered(KEY_ENTER);
}



bool CheckAllInputSelect()
{
  // Check all possible joysticks
  for (int i = JOYSTICK_1; i < NUMBEROFJOYSTICKS; ++i)
  {
    // Check the start button
    if (CheckJSButtonPressed((JOYSTICKS) i, JOYSTICK_BUTTONS::XBOX_BACK))
      return true;
  }

  // Check Keyboard
  return CheckKeyPressed(KEY_TAB);
}

bool CheckAllInputPause()
{
	// Check all possible joysticks
	for (int i = JOYSTICK_1; i < NUMBEROFJOYSTICKS; ++i)
	{
		// Check the start button
		if (CheckJSButtonTriggered((JOYSTICKS)i, JOYSTICK_BUTTONS::XBOX_START))
			return true;
	}

	// Check Keyboard
	return
		CheckKeyTriggered(KEY_BACKSPACE) ||
		CheckKeyTriggered(KEY_ESC);

}




void PollJoyStickEvents()
{
#if(CONTROLLERDEBUG)
  system("cls");
#endif
  poll_joysticks();

  for(int i = 0; i < NUMBEROFJOYSTICKS; ++i)
  {
    joysticks[i].refresh_joystick();

    if(joysticks[i].present)
    {
      if(joysticks[i].button_count > NUMBEROFJSBUTTONS)
      {
        for(int j = 0; j < NUMBEROFJSBUTTONS; ++j)
        {
          currJSButtons.state[i][j] = (int)(*(joysticks[i].buttons + j));
        }
      }
      else
      {
        for(int j = 0; j < joysticks[i].button_count; ++j)
        {
          currJSButtons.state[i][j] = (int)(*(joysticks[i].buttons + j));
        }
      }
      if(joysticks[i].axis_count > NUMBEROFJSAXIS)
      {
        for(int j = 0; j < NUMBEROFJSAXIS; ++j)
        {
          if(j == LEFT_Y_AXIS)
          {
            currJSAxis.state[i][j] = -*(joysticks[i].axes + j);
          }
          else if(j == RIGHT_Y_AXIS)
          {
            currJSAxis.state[i][j] = -*(joysticks[i].axes + j);
          }
          else
          {
            currJSAxis.state[i][j] = *(joysticks[i].axes + j);
          }
        }
      }
      else
      {
        for(int j = 0; j < joysticks[i].axis_count; ++j)
        {
          if(j == LEFT_Y_AXIS)
          {
            currJSAxis.state[i][j] = -*(joysticks[i].axes + j);
          }
          else if(j == RIGHT_Y_AXIS)
          {
            currJSAxis.state[i][j] = -*(joysticks[i].axes + j);
          }
          else
          {
            currJSAxis.state[i][j] = *(joysticks[i].axes + j);
          }
        }
      }
    }
    /*
    if(glfwJoystickPresent(i))
    {
      int nbuttons, naxes;
      const unsigned char *pressed = glfwGetJoystickButtons(i, &nbuttons);
      const float *axes = glfwGetJoystickAxes(i, &naxes);
      
      if(nbuttons > NUMBEROFJSBUTTONS)
      {
        for(int j = 0; j < NUMBEROFJSBUTTONS; ++j)
        {
          currJSButtons.state[i][j] = *(pressed + j);
        }
      }
      else
      {
        for(int j = 0; j < nbuttons; ++j)
        {
          currJSButtons.state[i][j] = *(pressed + j);
        }
      }

      if(naxes > NUMBEROFJSAXIS)
      {
        for(int j = 0; j < NUMBEROFJSAXIS; ++j)
        {
          currJSAxis.state[i][j] = *(axes + j);
        }
      }
      else
      {
        for(int j = 0; j < naxes; ++j)
        {
          currJSAxis.state[i][j] = *(axes + j);
        }
      }*/
#if(CONTROLLERDEBUG)
    joysticks[i].print();
    //PrintJoyStickEvents(i, &nbuttons, &naxes, pressed, axes);
#endif
    //}
  }
}
/*
void PrintJoyStickEvents(int joy, int* nbuttons, int* naxes, const unsigned char* pressed, const float* axes)
{
  std::cout << "Controller ID: " << CheckJoyStickName(joy) << std::endl;
  std::cout << "Number of Buttons: " << *nbuttons << std::endl;
  std::cout << "Number of Axes:    " << *naxes << std::endl;

  std::cout << "Buttons Pressed:   ";
  for(int i = 0; i < *nbuttons; ++i)
  {
    if(*(pressed + i) != '\0')
    {
      std::cout << i;
    }
    std::cout << ", ";
  }
  std::cout << std::endl;

  std::cout << "Buttons Pressed:   " << std::endl;
  for(int i = 0; i < *naxes; ++i)
  {
    std::cout << *(axes + i) << std::endl;
  }
}
*/
/*
bool CheckKeyPressed(KEYS Key)
{
  if(currKeys.state[Key])
    return true;
  else
    return false;
}

bool CheckKeyTriggered(KEYS Key)
{
 if(currKeys.state[Key] && !lastKeys.state[Key])
   return true;
 else
   return false;
}

bool CheckKeyHeld(KEYS Key)
{
  if(currKeys.state[Key] && lastKeys.state[Key])
  {
    currKeys.state[Key] += (int)(ENGINE->GetTime() * 10000);
    if(currKeys.state[Key] > KEY_HELD)
    {
      currKeys.state[Key] = KEY_HELD;
    }
  }
  if(currKeys.state[Key] == KEY_HELD)
    return true;
  else
    return false;
}

bool CheckKeyReleased(KEYS Key)
{
  if(!currKeys.state[Key] && lastKeys.state[Key])
    return true;
  else
    return false;
}

bool CheckMousePressed(MOUSE_KEYS M_Key)
{
  if(currMouse.state[M_Key])
    return true;
  else
    return false;
}

bool CheckMouseTriggered(MOUSE_KEYS M_Key)
{
 if(currMouse.state[M_Key] && !lastMouse.state[M_Key])
   return true;
 else
   return false;
}

bool CheckMouseHeld(MOUSE_KEYS M_Key)
{
  if(currMouse.state[M_Key] && lastMouse.state[M_Key])
  {
    currMouse.state[M_Key] += (int)(ENGINE->GetTime() * 10000);
    if(currMouse.state[M_Key] > KEY_HELD)
    {
      currMouse.state[M_Key] = KEY_HELD;
    }
  }
  if(currMouse.state[M_Key] == KEY_HELD)
    return true;
  else
    return false;
}

bool CheckMouseReleased(MOUSE_KEYS M_Key)
{
  if(!currMouse.state[M_Key] && lastMouse.state[M_Key])
  {
    return true;
  }
  return false;
}
*/
const char* CheckJSName(int joy)
{
  return glfwGetJoystickName(joy);
}

float& CheckJSAxis(JOYSTICKS joy, JOYSTICK_AXIS JS_Axis)
{
  return currJSAxis.state[joy][JS_Axis];
}

bool CheckJSButtonPressed(JOYSTICKS joy, JOYSTICK_BUTTONS Button)
{
  if(currJSButtons.state[joy][Button])
    return true;
  else
    return false;
}

bool CheckJSButtonTriggered(JOYSTICKS joy, JOYSTICK_BUTTONS Button)
{
 if(currJSButtons.state[joy][Button] && !lastJSButtons.state[joy][Button])
   return true;
 else
   return false;
}

bool CheckJSButtonHeld(JOYSTICKS joy, JOYSTICK_BUTTONS Button)
{
  if(currJSButtons.state[joy][Button] && lastJSButtons.state[joy][Button])
  {
    currJSButtons.state[joy][Button] += (int)(ENGINE->GetTime() * 10000) + lastJSButtons.state[joy][Button];
    if(currJSButtons.state[joy][Button] > KEY_HELD)
    {
      currJSButtons.state[joy][Button] = KEY_HELD;
    }
  }
  if(currJSButtons.state[joy][Button] == KEY_HELD)
    return true;
  else
    return false;
}

bool CheckJSButtonReleased(JOYSTICKS joy, JOYSTICK_BUTTONS Button)
{
  if(!currJSButtons.state[joy][Button] && lastJSButtons.state[joy][Button])
  {
    return true;
  }
  return false;
}

void SetJSButtonPressed(JOYSTICKS joy, JOYSTICK_BUTTONS Button)
{
  currJSButtons.state[joy][Button] = 1;
}

void SetJSButtonTriggered(JOYSTICKS joy, JOYSTICK_BUTTONS Button)
{
  if(bolleanJSCheck.state[joy][Button])
  {
    currJSButtons.state[joy][Button] = 0;
    bolleanJSCheck.state[joy][Button] = 0;
  }
  else
  {
    currJSButtons.state[joy][Button] = 1;
    bolleanJSCheck.state[joy][Button] = 1;
  }
}

void SetJSButtonReleased(JOYSTICKS joy, JOYSTICK_BUTTONS Button)
{
  currJSButtons.state[joy][Button] = 0;
}

void JSAxisSetValue(JOYSTICKS joy, JOYSTICK_AXIS JS_Axis, float value)
{
  currJSAxis.state[joy][JS_Axis] = value;
}

bool CheckJSActive(JOYSTICKS joy)
{
  return joysticks[joy].present;
}

