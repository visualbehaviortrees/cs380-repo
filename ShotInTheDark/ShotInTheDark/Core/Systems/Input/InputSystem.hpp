//	File name:		InputSystem.hpp
//	Project name:	Shot In The Dark
//	Author(s):		James Schmidt
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../../Systems/Graphics/OpenGL.hpp"
#include "../../System.hpp"

#define LEFT_AXIS_THRESHOLD 0.4f  //Leave otherwise used Joysticks will drift
#define RIGHT_AXIS_THRESHOLD 0.4f //Leave otherwise used Joysticks will drift
#define KEY_HELD 2500
#define MOD_NONE 0
#define MOD_SHIFT GLFW_MOD_SHIFT
#define MOD_CTRL GLFW_MOD_CONTROL
#define MOD_ALT GLFW_MOD_ALT
#define MOD_SUPER GLFW_MOD_SUPER

class InputSystem : public System
{
public:
  InputSystem();
  ~InputSystem();
  void Init(void);
  void Update(float dt);
  void Shutdown(void);

  void SendMsg(Entity *, Entity*, message);

private:

  bool windowIsActive = true;

};

typedef enum
{
  KEY_SPACE             = GLFW_KEY_SPACE,       /*   */
  KEY_APOSTROPHE        = GLFW_KEY_APOSTROPHE,  /* ' */
  KEY_COMMA             = GLFW_KEY_COMMA,       /* , */
  KEY_MINUS             = GLFW_KEY_MINUS,       /* - */
  KEY_PERIOD            = GLFW_KEY_PERIOD,      /* . */
  KEY_SLASH             = GLFW_KEY_SLASH,       /* / */
  KEY_0                 = GLFW_KEY_0,
  KEY_1                 = GLFW_KEY_1,
  KEY_2                 = GLFW_KEY_2,
  KEY_3                 = GLFW_KEY_3,
  KEY_4                 = GLFW_KEY_4,
  KEY_5                 = GLFW_KEY_5,
  KEY_6                 = GLFW_KEY_6,
  KEY_7                 = GLFW_KEY_7,
  KEY_8                 = GLFW_KEY_8,
  KEY_9                 = GLFW_KEY_9,
  KEY_SEMICOLON         = GLFW_KEY_SEMICOLON,   /* ; */
  KEY_EQUAL             = GLFW_KEY_EQUAL,       /* = */
  KEY_A                 = GLFW_KEY_A,
  KEY_B                 = GLFW_KEY_B,
  KEY_C                 = GLFW_KEY_C,
  KEY_D                 = GLFW_KEY_D,
  KEY_E                 = GLFW_KEY_E,
  KEY_F                 = GLFW_KEY_F,
  KEY_G                 = GLFW_KEY_G,
  KEY_H                 = GLFW_KEY_H,
  KEY_I                 = GLFW_KEY_I,
  KEY_J                 = GLFW_KEY_J,
  KEY_K                 = GLFW_KEY_K,
  KEY_L                 = GLFW_KEY_L,
  KEY_M                 = GLFW_KEY_M,
  KEY_N                 = GLFW_KEY_N,
  KEY_O                 = GLFW_KEY_O,
  KEY_P                 = GLFW_KEY_P,
  KEY_Q                 = GLFW_KEY_Q,
  KEY_R                 = GLFW_KEY_R,
  KEY_S                 = GLFW_KEY_S,
  KEY_T                 = GLFW_KEY_T,
  KEY_U                 = GLFW_KEY_U,
  KEY_V                 = GLFW_KEY_V,
  KEY_W                 = GLFW_KEY_W,
  KEY_X                 = GLFW_KEY_X,
  KEY_Y                 = GLFW_KEY_Y,
  KEY_Z                 = GLFW_KEY_Z,
  KEY_LEFT_BRACKET      = GLFW_KEY_LEFT_BRACKET,
  KEY_BACKSLASH         = GLFW_KEY_BACKSLASH,
  KEY_RIGHT_BRACKET     = GLFW_KEY_RIGHT_BRACKET,
  KEY_GRAVE_ACCENT      = GLFW_KEY_GRAVE_ACCENT,
  KEY_ESC               = GLFW_KEY_ESCAPE,
  KEY_ENTER             = GLFW_KEY_ENTER,
  KEY_TAB               = GLFW_KEY_TAB,
  KEY_BACKSPACE         = GLFW_KEY_BACKSPACE,
  KEY_INSERT            = GLFW_KEY_INSERT,
  KEY_DELETE            = GLFW_KEY_DELETE,
  KEY_RIGHT             = GLFW_KEY_RIGHT,
  KEY_LEFT              = GLFW_KEY_LEFT,
  KEY_DOWN              = GLFW_KEY_DOWN,
  KEY_UP                = GLFW_KEY_UP,
  KEY_PAGE_UP           = GLFW_KEY_PAGE_UP,
  KEY_PAGE_DOWN         = GLFW_KEY_PAGE_DOWN,
  KEY_HOME              = GLFW_KEY_HOME,
  KEY_END               = GLFW_KEY_END,
  KEY_CAPS_LOCK         = GLFW_KEY_CAPS_LOCK,
  KEY_SCROLL_LOCK       = GLFW_KEY_SCROLL_LOCK,
  KEY_NUM_LOCK          = GLFW_KEY_NUM_LOCK,
  KEY_PRINT_SCREEN      = GLFW_KEY_PRINT_SCREEN,
  KEY_PAUSE             = GLFW_KEY_PAUSE,
  KEY_F1                = GLFW_KEY_F1,
  KEY_F2                = GLFW_KEY_F2,
  KEY_F3                = GLFW_KEY_F3,
  KEY_F4                = GLFW_KEY_F4,
  KEY_F5                = GLFW_KEY_F5,
  KEY_F6                = GLFW_KEY_F6,
  KEY_F7                = GLFW_KEY_F7,
  KEY_F8                = GLFW_KEY_F8,
  KEY_F9                = GLFW_KEY_F9,
  KEY_F10               = GLFW_KEY_F10,
  KEY_F11               = GLFW_KEY_F11,
  KEY_F12               = GLFW_KEY_F12,
  KEY_F13               = GLFW_KEY_F13,
  KEY_F14               = GLFW_KEY_F14,
  KEY_F15               = GLFW_KEY_F15,
  KEY_F16               = GLFW_KEY_F16,
  KEY_F17               = GLFW_KEY_F17,
  KEY_F18               = GLFW_KEY_F18,
  KEY_F19               = GLFW_KEY_F19,
  KEY_F20               = GLFW_KEY_F20,
  KEY_F21               = GLFW_KEY_F21,
  KEY_F22               = GLFW_KEY_F22,
  KEY_F23               = GLFW_KEY_F23,
  KEY_F24               = GLFW_KEY_F24,
  KEY_F25               = GLFW_KEY_F25,
  KEY_PAD_0             = GLFW_KEY_KP_0,
  KEY_PAD_1             = GLFW_KEY_KP_1,
  KEY_PAD_2             = GLFW_KEY_KP_2,
  KEY_PAD_3             = GLFW_KEY_KP_3,
  KEY_PAD_4             = GLFW_KEY_KP_4,
  KEY_PAD_5             = GLFW_KEY_KP_5,
  KEY_PAD_6             = GLFW_KEY_KP_6,
  KEY_PAD_7             = GLFW_KEY_KP_7,
  KEY_PAD_8             = GLFW_KEY_KP_8,
  KEY_PAD_9             = GLFW_KEY_KP_9,
  KEY_PAD_DECIMAL       = GLFW_KEY_KP_DECIMAL,
  KEY_PAD_DIVIDE        = GLFW_KEY_KP_DIVIDE,
  KEY_PAD_MULTIPLY      = GLFW_KEY_KP_MULTIPLY,
  KEY_PAD_SUBTRACT      = GLFW_KEY_KP_SUBTRACT,
  KEY_PAD_ADD           = GLFW_KEY_KP_ADD,
  KEY_PAD_ENTER         = GLFW_KEY_KP_ENTER,
  KEY_PAD_EQUAL         = GLFW_KEY_KP_EQUAL,
  KEY_LEFT_SHIFT        = GLFW_KEY_LEFT_SHIFT,
  KEY_LEFT_CONTROL      = GLFW_KEY_LEFT_CONTROL,
  KEY_LEFT_ALT          = GLFW_KEY_LEFT_ALT,
  KEY_LEFT_SUPER        = GLFW_KEY_LEFT_SUPER,
  KEY_RIGHT_SHIFT       = GLFW_KEY_RIGHT_SHIFT,
  KEY_RIGHT_CONTROL     = GLFW_KEY_RIGHT_CONTROL,
  KEY_RIGHT_ALT         = GLFW_KEY_RIGHT_ALT,
  KEY_RIGHT_SUPER       = GLFW_KEY_RIGHT_SUPER,
  KEY_MENU              = GLFW_KEY_MENU,
  NUMBEROFKEYS
}KEYS;

typedef enum
{
  MOUSE_LEFT     = GLFW_MOUSE_BUTTON_1,
  MOUSE_RIGHT    = GLFW_MOUSE_BUTTON_2,
  MOUSE_MIDDLE   = GLFW_MOUSE_BUTTON_3,
  MOUSE_BUTTON_4 = GLFW_MOUSE_BUTTON_4,
  MOUSE_BUTTON_5 = GLFW_MOUSE_BUTTON_5,
  MOUSE_BUTTON_6 = GLFW_MOUSE_BUTTON_6,
  MOUSE_BUTTON_7 = GLFW_MOUSE_BUTTON_7,
  MOUSE_BUTTON_8 = GLFW_MOUSE_BUTTON_8,
  NUMBEROFMOUSEBUTTONS
}MOUSE_KEYS;

typedef enum
{
  JOYSTICK_1  = GLFW_JOYSTICK_1,
  JOYSTICK_2  = GLFW_JOYSTICK_2,
  JOYSTICK_3  = GLFW_JOYSTICK_3,
  JOYSTICK_4  = GLFW_JOYSTICK_4,

  //Please leave these commented unless we need to use more than four controllers.

  /*JOYSTICK_5  = GLFW_JOYSTICK_5,
    JOYSTICK_6  = GLFW_JOYSTICK_6,
    JOYSTICK_7  = GLFW_JOYSTICK_7,
    JOYSTICK_8  = GLFW_JOYSTICK_8,
    JOYSTICK_9  = GLFW_JOYSTICK_9,
    JOYSTICK_10 = GLFW_JOYSTICK_10,
    JOYSTICK_11 = GLFW_JOYSTICK_11,
    JOYSTICK_12 = GLFW_JOYSTICK_12,
    JOYSTICK_13 = GLFW_JOYSTICK_13,
    JOYSTICK_14 = GLFW_JOYSTICK_14,
    JOYSTICK_15 = GLFW_JOYSTICK_15,
    JOYSTICK_16 = GLFW_JOYSTICK_16,*/

  NUMBEROFJOYSTICKS
}JOYSTICKS;

typedef enum
{
  JS_BUTTON_1     = GLFW_JOYSTICK_1,
  XBOX_A          = GLFW_JOYSTICK_1,
  JS_BUTTON_2     = GLFW_JOYSTICK_2,
  XBOX_B          = GLFW_JOYSTICK_2,
  JS_BUTTON_3     = GLFW_JOYSTICK_3,
  XBOX_X          = GLFW_JOYSTICK_3,
  JS_BUTTON_4     = GLFW_JOYSTICK_4,
  XBOX_Y          = GLFW_JOYSTICK_4,
  JS_BUTTON_5     = GLFW_JOYSTICK_5,
  XBOX_LB         = GLFW_JOYSTICK_5,
  JS_BUTTON_6     = GLFW_JOYSTICK_6,
  XBOX_RB         = GLFW_JOYSTICK_6,
  JS_BUTTON_7     = GLFW_JOYSTICK_7,
  XBOX_BACK       = GLFW_JOYSTICK_7,
  JS_BUTTON_8     = GLFW_JOYSTICK_8,
  XBOX_START      = GLFW_JOYSTICK_8,
  JS_BUTTON_9     = GLFW_JOYSTICK_9,
  XBOX_LEFT_JS    = GLFW_JOYSTICK_9,
  JS_BUTTON_10    = GLFW_JOYSTICK_10,
  XBOX_RIGHT_JS   = GLFW_JOYSTICK_10,
  JS_BUTTON_11    = GLFW_JOYSTICK_11,
  XBOX_DPAD_UP    = GLFW_JOYSTICK_11,
  JS_BUTTON_12    = GLFW_JOYSTICK_12,
  XBOX_DPAD_RIGHT = GLFW_JOYSTICK_12,
  JS_BUTTON_13    = GLFW_JOYSTICK_13,
  XBOX_DPAD_DOWN  = GLFW_JOYSTICK_13,
  JS_BUTTON_14    = GLFW_JOYSTICK_14,
  XBOX_DPAD_LEFT  = GLFW_JOYSTICK_14,
  JS_BUTTON_15    = GLFW_JOYSTICK_15,
  JS_BUTTON_16    = GLFW_JOYSTICK_16,
  NUMBEROFJSBUTTONS
}JOYSTICK_BUTTONS;

typedef enum
{
  LEFT_X_AXIS,
  LEFT_Y_AXIS,

  TRIGGER_AXIS,

  RIGHT_Y_AXIS,
  RIGHT_X_AXIS,

  //D_PAD_X_AXIS,
  //D_PAD_Y_AXIS,

  NUMBEROFJSAXIS
}JOYSTICK_AXIS;

typedef struct
{
  int state[NUMBEROFKEYS];
}KEYSTATE;

typedef struct
{
  int state[NUMBEROFMOUSEBUTTONS];
}MOUSESTATE;

typedef struct
{
  int state[NUMBEROFJOYSTICKS][NUMBEROFJSBUTTONS];
}JOYSTICKBUTTONSTATE;

typedef struct
{
  float state[NUMBEROFJOYSTICKS][NUMBEROFJSAXIS];
}JOYSTICKAXISSTATE;

//Need these Externed for the inline Templates to properly work
extern KEYSTATE currKeys;
extern KEYSTATE lastKeys;
extern MOUSESTATE currMouse;
extern MOUSESTATE lastMouse;
extern int currMods;

void KeyboardHandler(GLFWwindow* window, int Button, int Scancode, int Action, int Mods);
void MouseButtonHandler(GLFWwindow* window, int Button, int Action, int Mods);
void MouseCurserHandler(GLFWwindow* window, double x, double y);
void DragNDropHandler(GLFWwindow* window, int file_count, const char** file);
void MouseWheelHandler(GLFWwindow* window, double xoffset, double yoffset);
void PollJoyStickEvents();
//void PrintJoyStickEvents(int joy, int* nbuttons, int* naxes, const unsigned char* pressed, const float* axes);

/* Moved below into inline Templates
bool CheckKeyPressed(KEYS Key);
bool CheckKeyHeld(KEYS Key);
bool CheckKeyTriggered(KEYS Key);
bool CheckKeyReleased(KEYS Key);
bool CheckMousePressed(MOUSE_KEYS M_Key);
bool CheckMouseTriggered(MOUSE_KEYS M_Key);
bool CheckMouseHeld(MOUSE_KEYS M_Key);
bool CheckMouseReleased(MOUSE_KEYS M_Key);
*/

const char* CheckJSName(int joy);
float& CheckJSAxis(JOYSTICKS joy, JOYSTICK_AXIS JS_Axis);
bool CheckJSButtonPressed(JOYSTICKS joy, JOYSTICK_BUTTONS Button);
bool CheckJSButtonTriggered(JOYSTICKS joy, JOYSTICK_BUTTONS Button);
bool CheckJSButtonHeld(JOYSTICKS joy, JOYSTICK_BUTTONS Button);
bool CheckJSButtonReleased(JOYSTICKS joy, JOYSTICK_BUTTONS Button);
void SetJSButtonPressed(JOYSTICKS joy, JOYSTICK_BUTTONS Button);
void SetJSButtonTriggered(JOYSTICKS joy, JOYSTICK_BUTTONS Button);
void SetJSButtonReleased(JOYSTICKS joy, JOYSTICK_BUTTONS Button);
void JSAxisSetValue(JOYSTICKS joy, JOYSTICK_AXIS JS_Axis, float value);
bool CheckJSActive(JOYSTICKS joy);

// Checks all keys and joysticks for universal key presses (useful for menus)
bool CheckAllInputRight();
bool CheckAllInputLeft();
bool CheckAllInputUp();
bool CheckAllInputDown();
bool CheckAllInputEnter();
bool CheckAllInputPause();
bool CheckAllInputSelect();

template <typename T> inline
bool CheckModKey(T const& Mod)
{
  if (Mod & currMods)
  {
    return true;
  }

  return false;
}

template <typename T, typename ... T2> inline
bool CheckModKey(T const& Mod_First, T2 const& ... Mod_Rest)
{
  return (CheckModKey(Mod_First) && CheckModKey(Mod_Rest ...));
}

template <typename T> inline
bool CheckKeyPressed(T const& Key)
{
  if (currMods == MOD_NONE)
  {
    if (currKeys.state[Key])
    {
      return true;
    }
  }

  return false;
}

template <typename T, typename ... T2> inline
bool CheckKeyPressed(T const& Key, T2 const& ... Mod)
{
  if (currKeys.state[Key])
  {
    return CheckModKey(Mod ...);
  }

  return false;
}

template <typename T> inline
bool CheckKeyTriggered(T const& Key)
{
  if (currMods == MOD_NONE)
  {
    if (currKeys.state[Key] && !lastKeys.state[Key])
    {
      return true;
    }
  }
  return false;
}

template <typename T, typename ... T2> inline
bool CheckKeyTriggered(T const& Key, T2 const& ... Mod)
{
  if (currKeys.state[Key] && !lastKeys.state[Key])
  {
    return CheckModKey(Mod ...);
  }

  return false;
}

template <typename T> inline
bool CheckKeyHeld(T const& Key)
{
  if (currMods == MOD_NONE)
  {
    if (currKeys.state[Key] && lastKeys.state[Key])
    {
      currKeys.state[Key] += (int)(ENGINE->GetTime() * 10000);
      if (currKeys.state[Key] > KEY_HELD)
      {
        currKeys.state[Key] = KEY_HELD;
      }
    }

    if (currKeys.state[Key] == KEY_HELD)
    {
      currKeys.state[Key] -= (int)(ENGINE->GetTime() * 200000);
      return true;
    }
  }

  return false;
}

template <typename T, typename ... T2> inline
bool CheckKeyHeld(T const& Key, T2 const& ... Mod)
{
  if (currKeys.state[Key] && lastKeys.state[Key])
  {
    currKeys.state[Key] += (int)(ENGINE->GetTime() * 10000);
    if (currKeys.state[Key] > KEY_HELD)
    {
      currKeys.state[Key] = KEY_HELD;
    }
  }

  if (currKeys.state[Key] == KEY_HELD)
  {
    currKeys.state[Key] -= (int)(ENGINE->GetTime() * 80001);
    return CheckModKey(Mod ...);
  }

  return false;
}

template <typename T> inline
bool CheckKeyReleased(T const& Key)
{
  if (currMods == MOD_NONE)
  {
    if (!currKeys.state[Key] && lastKeys.state[Key])
    {
      return true;
    }
  }

  return false;
}

template <typename T, typename ... T2> inline
bool CheckKeyReleased(T const& Key, T2 const& ... Mod)
{
  if (!currKeys.state[Key] && lastKeys.state[Key])
  {
    return CheckModKey(Mod ...);
  }

  return false;
}

template <typename T> inline
bool CheckMousePressed(T const& M_Key)
{
  if (currMods == MOD_NONE)
  {
    if (currMouse.state[M_Key])
    {
      return true;
    }
  }
  
  return false;
}

template <typename T, typename ... T2> inline
bool CheckMousePressed(T const& M_Key, T2 const& ... Mod)
{
  if (currMouse.state[M_Key])
  {
    return CheckModKey(Mod ...);
  }

  return false;
}

template <typename T> inline
bool CheckMouseTriggered(T const& M_Key)
{
  if (currMods == MOD_NONE)
  {
    if (currMouse.state[M_Key] && !lastMouse.state[M_Key])
    {
      return true;
    }
  }

  return false;
}

template <typename T, typename ... T2> inline
bool CheckMouseTriggered(T const& M_Key, T2 const& ... Mod)
{
  if (currMouse.state[M_Key] && !lastMouse.state[M_Key])
  {
    return CheckModKey(Mod ...);
  }

  return false;
}

template <typename T> inline
bool CheckMouseHeld(T const& M_Key)
{
  if (currMods == MOD_NONE)
  {
    if (currMouse.state[M_Key] && lastMouse.state[M_Key])
    {
      currMouse.state[M_Key] += (int)(ENGINE->GetTime() * 10000);
      if (currMouse.state[M_Key] > KEY_HELD)
      {
        currMouse.state[M_Key] = KEY_HELD;
      }
    }

    if (currMouse.state[M_Key] == KEY_HELD)
    {
      //currMouse.state[M_Key] -= (int)(ENGINE->GetTime() * 200000);
      return true;
    }
  }

  return false;
}

template <typename T, typename ... T2> inline
bool CheckMouseHeld(T const& M_Key, T2 const& ... Mod)
{
  if (currMouse.state[M_Key] && lastMouse.state[M_Key])
  {
    currMouse.state[M_Key] += (int)(ENGINE->GetTime() * 10000);
    if (currMouse.state[M_Key] > KEY_HELD)
    {
      currMouse.state[M_Key] = KEY_HELD;
    }
  }

  if (currMouse.state[M_Key] == KEY_HELD)
  {
    //currMouse.state[M_Key] -= (int)(ENGINE->GetTime() * 200000);
    return CheckModKey(Mod ...);
  }

  return false;
}

template <typename T> inline
bool CheckMouseReleased(T const& M_Key)
{
  if (currMods == MOD_NONE)
  {
    if (!currMouse.state[M_Key] && lastMouse.state[M_Key])
    {
      return true;
    }
  }

  return false;
}

template <typename T, typename ... T2> inline
bool CheckMouseReleased(T const& M_Key, T2 const& ... Mod)
{
  if (!currMouse.state[M_Key] && lastMouse.state[M_Key])
  {
    return CheckModKey(Mod ...);
  }

  return false;
}
