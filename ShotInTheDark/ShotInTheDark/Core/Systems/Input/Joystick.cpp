//	File name:		Joystick.cpp
//	Project name:	Shot In The Dark
//	Author(s):		James Schmidt
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "Joystick.hpp"
#include "InputSystem.hpp"

static int joystick_count = 0;

int connected[NUMBEROFJOYSTICKS];

Joystick::Joystick()
{
  joy_num = joystick_count++;
  present = GL_FALSE;
  name = nullptr;
  buttons = nullptr;
  axes = nullptr;
  axis_count = 0;
  button_count = 0;
}

Joystick::~Joystick()
{
  delete[] buttons;
  delete[] axes;
}

void Joystick::refresh_joystick()
{
  if(joy_connected(joy_num))
  {
    //if(!present)
    //{
      const float* _axes;
      const unsigned char* _buttons;
      int _axis_count, _button_count;

      name = _strdup(glfwGetJoystickName(joy_num));

      _axes = glfwGetJoystickAxes(joy_num, &_axis_count);

      if(_axis_count != axis_count)
      {
        axis_count = _axis_count;
        delete[] axes;
        axes = new float[axis_count];
      }

      memcpy(axes, _axes, axis_count * sizeof(float));

      _buttons = glfwGetJoystickButtons(joy_num, &_button_count);

      if(_button_count != button_count)
      {
        button_count = _button_count;
        delete[] buttons;
        buttons = new unsigned char[button_count];
      }
    
      memcpy(buttons, _buttons, button_count * sizeof(unsigned char));

      if (!present)
      {
          printf("Found joystick %i named \'%s\' with %i axes, %i buttons\n",
                  joy_num, name, axis_count, button_count);
      }

      present = GL_TRUE;
    //}
  }
  else
  {
    if(present)
    {
      printf("Lost joystick %i named \'%s\'\n", joy_num, name);
      present = GL_FALSE;
    }
  }
}

void Joystick::print(void)
{
  if(present)
  {
    std::cout << "Controller ID: " << name << std::endl;
    std::cout << "Controller Number: " << joy_num << std::endl;
    std::cout << "Active: " << present << std::endl;
    std::cout << "Number of Buttons: " << button_count << std::endl;
    std::cout << "Number of Axes:    " << axis_count << std::endl;

    std::cout << "Buttons Pressed:   ";
    for(int i = 0; i < button_count; ++i)
    {
      if(*(buttons + i) != '\0')
      {
        std::cout << i;
      }
      std::cout << ", ";
    }
    std::cout << std::endl;

    std::cout << "Axes:   " << std::endl;
    for(int i = 0; i < axis_count; ++i)
    {
      std::cout << *(axes + i) << std::endl;
    }
  }
}


bool Joystick::joy_connected(int num)
{
  if (num > NUMBEROFJOYSTICKS)
  {
		return false;
  }
  else
  {
    return connected[num] == GL_TRUE;
  }

}

void poll_joysticks(void)
{
	for (int i = 0; i < NUMBEROFJOYSTICKS; ++i)
	{
		connected[i] = glfwJoystickPresent(i);
	}
}