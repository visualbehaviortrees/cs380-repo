//	File name:		Joystick.hpp
//	Project name:	Shot In The Dark
//	Author(s):		James Schmidt
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../Graphics/OpenGL.hpp"

struct Joystick
{
public:
  Joystick();
  ~Joystick();
  void refresh_joystick(void);


  void print(void);

  int joy_num;
  bool present;
  char* name;
  unsigned char* buttons;
  float* axes;
  int axis_count;
  int button_count;


private:
	// Updates connected joysticks every k frames
	bool joy_connected(int num);


	//static int connected[NUMBEROFJOYSTICKS];

};

void poll_joysticks(void);
