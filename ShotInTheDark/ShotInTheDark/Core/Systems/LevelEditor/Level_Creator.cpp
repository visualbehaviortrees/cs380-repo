#include "stdafx.h"
#include <algorithm>
#include "Level_Creator.hpp"
#include "Libraries\AntTweakBar\AntTweakBar.hpp"
#include "Systems\Physics\Collision.hpp"
#include "Libraries\RNG\RNG.hpp"
#include "../Gameplay/DungeonGenerator.h"
#include "../AI/GridMap.hpp"
#include "../Gameplay/DungeonGameplay.h"

extern Engine* ENGINE;


void EditorSystem::Editor_Background(void)
{
  const Vector2D bg_scale(20.0f, 20.0f);
  const int num_x = 5, num_y = 5;
  // Start at the bottom left and go right and up
  const Point2D bg_pos_start(-39.0f, -39.0f);
  Point2D bg_pos = bg_pos_start;

  bool flop_x = true, flop_y = true;
  for (int x = 0; x < num_x; ++x)
  {
    flop_x = !flop_x;

    for (int y = 0; y < num_y; ++y)
    {
      flop_y = !flop_y;

      Transform* bgTran = new Transform();
      bgTran->scale = Vector2D(20.0f, 20.0f);
      bgTran->Position = bg_pos;
      // Rotate since sprite flipping doesn't work
      bgTran->rotation = 180.0f * (flop_y ? 1.0f : 0.0f) + 90.0f * (flop_x ? 1.0f : 0.0f);
      Drawable* bgDraw = new Drawable();
      bgDraw->layer = 1;
      Sprite* bgSprite = new Sprite("PLACEHOLDER_background_toast.png");

      Entity *bgEntity = ENGINE->Factory->Compose(3,
        bgTran,
        bgDraw,
        bgSprite
        );
      bgEntity->SetName(std::string("Background"));

      // Move up vertically
      bg_pos.y += bg_scale.y;
    }
    // Reset our horizontal position and move right
    bg_pos.y = bg_pos_start.y;
    bg_pos.x += bg_scale.x;
  }


}


void EditorSystem::Create_Default_Room(int Width, int Height)
{
  int x = width / 2;
  x *= 2;
  x *= -1; // since im starting from the left, its on the negative side;


  int y = height / 2;
  y *= 2;

  Point2D top_left((float)x, (float)y);

  int xoffset = 0;
  int yoffset = 0;

  for (int i = 0; i < width; ++i)
  {
    Create_Tile(Point2D(top_left.x + xoffset, top_left.y + yoffset), Tiles::Tiles_wall, true, false);
    if (!action_list.empty())
      action_list.pop_back();
    //undo_list_ptr = action_list.back();
    xoffset += 2;
  }
  yoffset = (height - 1) * -2;
  xoffset = 0;


  for (int i = 0; i < width; ++i)
  {
    Create_Tile(Point2D(top_left.x + xoffset, top_left.y + yoffset), Tiles::Tiles_wall, true, false);
    if (!action_list.empty())
      action_list.pop_back();
    //undo_list_ptr = action_list.back();
    xoffset += 2;
  }

  xoffset = 0;
  yoffset = 0;

  for (int j = 1; j < height - 1; ++j)
  {
    yoffset -= 2;
    for (int i = 0; i < 2; ++i)
    {
      Create_Tile(Point2D(top_left.x + xoffset, top_left.y + yoffset), Tiles::Tiles_wall, true, false);
      if (!action_list.empty())
        action_list.pop_back();
      //undo_list_ptr = action_list.back();
      xoffset += (width * 2) - 2;
    }
    xoffset = 0;
  }
}


EditorSystem::EditorSystem(int Width, int Height) : Gamestate(sys_EditorSystem, sys_EditorSystem, "Level Editor")
{
  width = Width;
  height = Height;
  current_room.close();
  filepath.clear();
  undo_list_ptr = NULL;

  //Create_Default_Room(width, height);
}


EditorSystem::EditorSystem(const char* file) : Gamestate(sys_EditorSystem, sys_EditorSystem, "Level Editor")
{
  filepath = file;
  current_room.open(file);
  undo_list_ptr = NULL;

  if (!current_room.is_open())
  {
    cout << "EDITOR COULD NOT OPEN THIS FILE: " << file << endl;
    return;
  }
  else
  {
    current_room >> width;
    current_room >> height;

    int x = width / 2;
    x *= 2;
    x *= -1; // since im starting from the left, its on the negative side;


    int y = height / 2;
    y *= 2;

    Point2D top_left((float)x, (float)y);
    Create_Default_Room(width, height);


    int xoffset = 0;
    int yoffset = 0;

    int tile = 0;

    for (int j = 0; j < height; ++j)
    {
      if (j == 0 || j == height - 1)
      {
        current_room >> tile;
        continue;
      }

      for (int i = 0; i < width; ++i)
      {
        if (i == 0 || i == width - 1)
        {
          current_room >> tile;
          continue;
        }

        current_room >> tile;

        if (tile)
        {
          Create_Tile(Point2D(top_left.x + xoffset, top_left.y + yoffset), static_cast<Tiles>(tile), true);

          if (!action_list.empty())
            action_list.pop_back();

        }
        xoffset += 2;
      }
      xoffset = 0;
      yoffset -= 2;
    }
    current_room.close();
  }
}


bool Snap_To_Grid(Entity* object)
{
  Point2D Object_Pos = Point2D(object->GET_COMPONENT(Transform)->Position.x, object->GET_COMPONENT(Transform)->Position.y);
  
  Point2D Grid_pos = WorldToDungeonAffine(15, 7) * Object_Pos;

  int Pos_x = (Grid_pos.x < 0.0f) ? (int)(Grid_pos.x - 0.5f) : (int)(Grid_pos.x + 0.5);

  int Pos_y = (Grid_pos.y < 0.0f) ? (int)(Grid_pos.y - 0.5f) : (int)(Grid_pos.y + 0.5);

  Vector2DInt Dungeon_Pos = Vector2DInt(Pos_x, Pos_y);

  Point2D pos = DungeonToWorld(15, 7) * Point2D((float)Dungeon_Pos.x, (float)Dungeon_Pos.y);

  object->GET_COMPONENT(Transform)->Position.x = pos.x;
  object->GET_COMPONENT(Transform)->Position.y = pos.y;

  return 1;
}


bool Snap_To_Grid(Point2D& Position)
{
  Point2D Object_Pos = Point2D(Position.x, Position.y);

  Point2D Grid_pos = WorldToDungeonAffine(15, 7) * Object_Pos;

  int Pos_x = (Grid_pos.x < 0.0f) ? (int)(Grid_pos.x - 0.5f) : (int)(Grid_pos.x + 0.5);

  int Pos_y = (Grid_pos.y < 0.0f) ? (int)(Grid_pos.y - 0.5f) : (int)(Grid_pos.y + 0.5);

  Vector2DInt Dungeon_Pos = Vector2DInt(Pos_x, Pos_y);

  Point2D pos = DungeonToWorld(15, 7) * Point2D((float)Dungeon_Pos.x, (float)Dungeon_Pos.y);

  Position.x = pos.x;
  Position.y = pos.y;

  return 1;
}


void EditorSystem::Destroy_Object(Entity* destroy_this)
{
  Editor_Actions* editor_action = new Editor_Actions;
  editor_action->action = Editor_Actions::DESTROYED;
  editor_action->node_position = action_list.size();

  editor_action->position = destroy_this->GET_COMPONENT(Transform)->Position;


  for (vector<Editor_Object*>::iterator i = object_list.begin(); i != object_list.end(); ++i)
  {
    if ((*i)->selected_tile->GET_COMPONENT(Transform)->Position == editor_action->position)
    {
      if ((*i)->can_destroy == false)
        return;

      editor_action->tile_type = (*i)->tile;
      ENGINE->OM->DestroyEntity(destroy_this);
      object_list.erase(i);
      break;
    }
  }

 
  action_list.push_back(editor_action);
  undo_list_ptr = action_list.back();
}


void EditorSystem::Go_Back()
{

  if (undo_list_ptr->action == Editor_Actions::CREATE)
  {
    for (vector<Editor_Object*>::iterator i = object_list.begin(); i != object_list.end(); ++i)
    {
      if ((*i)->selected_tile->GET_COMPONENT(Transform)->Position == undo_list_ptr->position)
      {
        ENGINE->OM->DestroyEntity((*i)->selected_tile);  // destroys the entity in the engine
        object_list.erase(i);
        redo_list.push_back(action_list.back());
        break;
      }
    }
  }
  else if (undo_list_ptr->action == Editor_Actions::DESTROYED)// Editor_Actions::Destroy
  {
    redo_list.push_back(undo_list_ptr);
    Create_Tile(undo_list_ptr->position, undo_list_ptr->tile_type);
    action_list.pop_back();
  }
  else if (undo_list_ptr->action == Editor_Actions::REPLACRED)
  {
    redo_list.push_back(undo_list_ptr);
    
    for (vector<Editor_Object*>::iterator i = object_list.begin(); i != object_list.end(); ++i)
    {
      if ((*i)->selected_tile->GET_COMPONENT(Transform)->Position == undo_list_ptr->position)
      {
        ENGINE->OM->DestroyEntity((*i)->selected_tile);  // destroys the entity in the engine
        object_list.erase(i);
        action_list.pop_back();
        break;
      }
    }
    Create_Tile(undo_list_ptr->position, undo_list_ptr->replaced);
    
  }


  if (undo_list_ptr->node_position != 0)
  {
    undo_list_ptr = action_list[undo_list_ptr->node_position - 1];  // the next action on the list
    action_list.pop_back();
  }
  else
  {
    action_list.pop_back();
    undo_list_ptr = NULL; // no more actions left to undo on the list
  }
}


void EditorSystem::Go_Forward()
{
  if (redo_list.size() == 0)
    return;

  if (redo_list.back()->action == Editor_Actions::CREATE)
  {
    Create_Tile(redo_list.back()->position, redo_list.back()->tile_type);
    undo_list_ptr = action_list.back();
  }

  else if (redo_list.back()->action == Editor_Actions::REPLACRED)
  {
    Create_Tile(redo_list.back()->position, redo_list.back()->tile_type);
    undo_list_ptr = action_list.back();
  }
  redo_list.pop_back();
}


EditorSystem::~EditorSystem()
{

}


void EditorSystem::Init(void)
{
  //REGISTER_PRESYS(Input);
  REGISTER_POSTSYS(Graphics);

  ENGINE->OM->DestroyAll();

  Create_Default_Room(width, height);

  //Editor_Background(); //doesn't work because gonzalo's batching
  current_tile = Tiles::Tiles_wall;

  current_room.close();
  filepath.clear();
  undo_list_ptr = NULL;
  ENGINE->OM->UpdateRegistry();
  GETSYS(Graphics)->SetCameraPosition(Point2D(0, 0));
  GETSYS(Graphics)->SetLighting(false);
}


void EditorSystem::CheckKeyEvents(void)
{
  if (CheckKeyTriggered(KEY_ACCENT))
  {
//    ENGINE->popGamestate();
    EditorSystem* es = new EditorSystem(15, 9);
    ENGINE->pushGamestate(es);
  }

  if (CheckKeyTriggered(KEY_1))
  {
    current_tile = Tiles::Tiles_wall;
  }

  if (CheckKeyTriggered(KEY_2))
  {
    current_tile = Tiles::Tiles_torch;
  }
  if (CheckKeyTriggered(KEY_3))
  {
    current_tile = Tiles::Tiles_enemy;
  }
  if (CheckKeyTriggered(KEY_4))
  {
	  current_tile = Tiles::Tiles_destructable;
  }

  if (CheckKeyTriggered(KEY_S, MOD_CTRL))
  {
    Save_Level();
  }

  if (CheckKeyTriggered(KEY_Z, MOD_CTRL) || CheckKeyHeld(KEY_Z, MOD_CTRL))
  {
    if (undo_list_ptr == NULL)
      return;

    if (undo_list_ptr->node_position != -1)
      Go_Back();
  }

  if (CheckKeyTriggered(KEY_Y, MOD_CTRL) || CheckKeyHeld(KEY_Y, MOD_CTRL))
    Go_Forward();

  if (CheckMouseTriggered(MOUSE_LEFT) || CheckMouseHeld(MOUSE_LEFT))
  {
    if (!Create_Tile(GetMousePosition(), current_tile))
      return;

    // update the position of the undo_list_ptr to make sure it points at the last action taken
    undo_list_ptr = action_list.back();
    redo_list.clear();
  }

  if (CheckMouseTriggered(MOUSE_RIGHT) || CheckMouseHeld(MOUSE_RIGHT))
  {
    Entity* selected_object = SomethingPhysicsy(GetMousePosition());
    if (selected_object)
      Destroy_Object(selected_object);
  }

  
  if (CheckKeyTriggered(KEY_N, MOD_CTRL))
  {
    if (object_list.size() != 0)
    {
      ENGINE->OM->DestroyAll();
      action_list.clear();
      object_list.clear();
      undo_list_ptr = NULL;
      redo_list.clear();
      edges.clear();
    }
   
    Create_Default_Room(width, height);
    filepath.clear();
  }
}


void EditorSystem::Update(float dt)
{
  ENGINE;
  CheckKeyEvents();
  int wait = 0;
}


void EditorSystem::Shutdown(void)
{

}


void EditorSystem::PoppedTo(systype sys)
{

}



bool EditorSystem::Create_Tile(Point2D position, Tiles tile, bool is_loading, bool Can_Destroy_This) // position in screen cordinates
{
   // check if outside the height and width passed in if so don't make the tile
  if (position.x < -width + 0.5f || position.x > width - 0.5f || position.y > height - 0.5f || position.y < -height + 0.5f)
    return false;


  JSONReader level;
  Editor_Actions* editor_action = new Editor_Actions;
  editor_action->action = Editor_Actions::CREATE;
  editor_action->node_position = action_list.size();
  editor_action->tile_type = tile;
  std::vector<Entity*> object;
  

  switch (tile)
  {
    case Tiles::Tiles_wall:
    {
        level.JSONLoad("Core/Systems/LevelEditor/Archetypes/Wall.json");
        object = level.SerializeParent();
        object[0]->GET_COMPONENT(Transform)->Position = position;
        object[0]->GET_COMPONENT(Transform)->scale = Vector2D(1,1);
        object[0]->GET_COMPONENT(Drawable)->mpMesh = Mesh::Get_Mesh("box");
        //object[0]->GET_COMPONENT(Mesh)->Update_Vertices(object[0]);
        break;
    }

	case Tiles::Tiles_destructable:
	{
		level.JSONLoad("Core/Systems/LevelEditor/Archetypes/WallDestructable.json");
		object = level.SerializeParent();
		object[0]->GET_COMPONENT(Transform)->Position = position;
		object[0]->GET_COMPONENT(Transform)->scale = Vector2D(1, 1);
		object[0]->GET_COMPONENT(Drawable)->mpMesh = Mesh::Get_Mesh("box");
		//object[0]->GET_COMPONENT(Mesh)->Update_Vertices(object[0]);
		break;
	}

    case Tiles::Tiles_torch:
    {
      level.JSONLoad("Core/Systems/LevelEditor/Archetypes/Torch.json");
      object = level.SerializeParent();
      object[0]->GET_COMPONENT(Transform)->Position = position;
      object[0]->GET_COMPONENT(Transform)->scale = Vector2D(1, 1);
      object[0]->GET_COMPONENT(Drawable)->mpMesh = Mesh::Get_Mesh("RedBase");
      //object[0]->GET_COMPONENT(Mesh)->Update_Vertices(object[0]);
      break;
    }

    case Tiles::Tiles_enemy:
    {
      level.JSONLoad("Core/Systems/LevelEditor/Archetypes/Enemy.json");
      object = level.SerializeParent();
      object[0]->GET_COMPONENT(Transform)->Position = position;
      object[0]->GET_COMPONENT(Transform)->scale = Vector2D(1, 1);
      object[0]->GET_COMPONENT(Drawable)->mpMesh = Mesh::Get_Mesh("box");
      //object[0]->GET_COMPONENT(Mesh)->Update_Vertices(object[0]);
      break;
    }

    default:
      delete editor_action;
      return false;
      break;
  }

  Editor_Object* insert_object = new Editor_Object;
  insert_object->tile = tile;
  insert_object->can_destroy = Can_Destroy_This;


  Snap_To_Grid(object[0]);
   
  if (!is_loading)
  {
    for (vector<Editor_Object*>::iterator i = object_list.begin(); i != object_list.end(); ++i)
    {
      if ((*i)->selected_tile->GET_COMPONENT(Transform)->Position == object[0]->GET_COMPONENT(Transform)->Position)
      {
        if ((*i)->can_destroy == false)
        {
          ENGINE->OM->DestroyEntity(object[0]);
          delete insert_object;
          delete editor_action;
          return false;
        }
        if ((*i)->tile == current_tile)
        {
          ENGINE->OM->DestroyEntity(object[0]);
          return false;
        }
        else
        {
          Editor_Actions* action = new Editor_Actions;
          action->action = Editor_Actions::REPLACRED;
          action->node_position = action_list.size();
          action->replaced = (*i)->tile;
          action->tile_type = tile;
          action->position = object[0]->GET_COMPONENT(Transform)->Position;

          ENGINE->OM->DestroyEntity((*i)->selected_tile);
          Destroy_Object((*i)->selected_tile);
          action_list.pop_back();

          insert_object->selected_tile = object[0];
          object_list.push_back(insert_object);


          action_list.push_back(action);
          return true;
          break;
        }
      }
    }
  }

  insert_object->selected_tile = object[0];
  object_list.push_back(insert_object);

  editor_action->position = object[0]->GET_COMPONENT(Transform)->Position;
  action_list.push_back(editor_action);
  return true;
}


void EditorSystem::Set_Width(void)
{
  std::vector<float> all_sizes;

  for (unsigned i = 0; i < object_list.size(); ++i)
    all_sizes.push_back(object_list[i]->selected_tile->GET_COMPONENT(Transform)->Position.y);

  sort(all_sizes.begin(), all_sizes.end());

  int Width = std::count(all_sizes.begin(), all_sizes.end(), all_sizes[0]);
  int count = Width;

  for (unsigned i = count; i < all_sizes.size(); i+=count)
  {
    count = std::count(all_sizes.begin(), all_sizes.end(), all_sizes[i]);

    if (count > Width)
      Width = count;
  }

  width = Width;
}


void EditorSystem::Set_Height(void)
{
  std::vector<float> all_sizes;

  for (unsigned i = 0; i < object_list.size(); ++i)
    all_sizes.push_back(object_list[i]->selected_tile->GET_COMPONENT(Transform)->Position.x);

  sort(all_sizes.begin(), all_sizes.end());

  int Height = std::count(all_sizes.begin(), all_sizes.end(), all_sizes[0]);
  int count = Height;

  for (unsigned i = count; i < all_sizes.size(); i += count)
  {
    count = std::count(all_sizes.begin(), all_sizes.end(), all_sizes[i]);

    if (count > Height)
      Height = count;
  }

  height = Height;
}


void EditorSystem::Find_Edges(void)
{
  std::vector<float> all_widths, all_heights;


  for (unsigned i = 0; i < object_list.size(); ++i)
  {
    all_widths.push_back(object_list[i]->selected_tile->GET_COMPONENT(Transform)->Position.x);
    all_heights.push_back(object_list[i]->selected_tile->GET_COMPONENT(Transform)->Position.y);
  }

  sort(all_widths.begin(), all_widths.end());
  reverse(all_widths.begin(), all_widths.end());
  sort(all_heights.begin(), all_heights.end());
  reverse(all_heights.begin(), all_heights.end());


  edges.push_back(Point2D(all_widths[all_widths.size() - 1], all_heights[0]));
  edges.push_back(Point2D(all_widths[0], all_heights[0]));
  edges.push_back(Point2D(all_widths[all_widths.size() - 1], all_heights[all_heights.size() - 1]));
  edges.push_back(Point2D(all_widths[0], all_heights[all_heights.size() - 1]));
}


const char* EditorSystem::Make_Next_Room(void)
{
  std::fstream level_files("Core/Systems/Gameplay/Rooms/All_Rooms.txt");

  if (level_files.is_open())
  {
    string rooms;
    int num_rooms = 0;
    while (std::getline(level_files, rooms))
    {
      ++num_rooms;
    }
    ++num_rooms;
    level_files.close();

    std::ofstream adding_level("Core/Systems/Gameplay/Rooms/All_Rooms.txt",std::ios::app);

    string next_room("\nCore/Systems/Gameplay/Rooms/Room");
    next_room.append(std::to_string(num_rooms));
    next_room.append(".txt");

    adding_level << next_room;
    adding_level.close();


    string* level = new string("Core/Systems/Gameplay/Rooms/Room");
    level->append(std::to_string(num_rooms));
    level->append(".txt");

    return level->c_str();
  }
  return NULL;
}


void EditorSystem::Save_To_File(std::vector<vector<int>> map)
{
  if (filepath.empty())
  {
    filepath = Make_Next_Room();
  }
  
  std::fstream level_file(filepath, std::ios::out);

  if (level_file.is_open())
  {
    level_file << width << "\n" << height << "\n";

    for (int i = 0; i < height; ++i)
    {
      for (int j = 0; j < width; ++j)
      {
        level_file << map[i][j];

        if (j + 1 < width)
          level_file << " ";
      }
      if (i + 1 < height)
        level_file << "\n";
    }

    level_file.close();
  }
  else
    return;

}


void EditorSystem::Save_Level()
{
  //Set_Width();
  //Set_Height();
  Find_Edges();


  vector<vector<int>> DungMap2D;
  Point2D trans(edges[0]);


  DungMap2D.resize(height);
  for (int i = 0; i < height; ++i)
    DungMap2D[i].resize(width);

  for (unsigned i = 0; i < object_list.size(); ++i)
  {
    float x = (object_list[i]->selected_tile->GET_COMPONENT(Transform)->Position.x - trans.x) / (object_list[i]->selected_tile->GET_COMPONENT(Transform)->scale.x * 2);
    float y = (object_list[i]->selected_tile->GET_COMPONENT(Transform)->Position.y - trans.y) / (object_list[i]->selected_tile->GET_COMPONENT(Transform)->scale.x * 2);

    if ((int)x < 0)
      x *= -1;
    if ((int)y < 0)
      y *= -1;

    DungMap2D[(int)y][(int)x] = object_list[i]->tile;
  }

  Save_To_File(DungMap2D);
}


void EditorSystem::Load_Level(string level_file)
{

  current_room.close();
  current_room.open(level_file);

  if (!current_room.is_open())
  {
    cout << "EDITOR COULD NOT OPEN THIS FILE: " << level_file << endl;
    return;
  }
  else
  {
    for (unsigned i = 0; i < level_file.size(); ++i)
      filepath.push_back(level_file[i]);

    
    if (object_list.size() != 0)
    {
      ENGINE->OM->DestroyAll();
      action_list.clear();
      object_list.clear();
      undo_list_ptr = NULL;
      redo_list.clear();
      height = 0;
      width = 0;
      edges.clear();
    }

    current_room >> width;
    current_room >> height;

    int x = width / 2;
    x *= 2;
    x *= -1; // since im starting from the left, its on the negative side;


    int y = height / 2;
    y *= 2;

    Point2D top_left((float)x, (float)y);

    int xoffset = 0;
    int yoffset = 0;

    for (int j = 0; j < height; ++j)
    {
      for (int i = 0; i < width; ++i)
      {
        int tile = 0;
        current_room >> tile;

        if (tile)
        {
          Create_Tile(Point2D(top_left.x + xoffset, top_left.y + yoffset), static_cast<Tiles>(tile), true);
          undo_list_ptr = action_list.back();
        }
        xoffset += 2;
      }
      xoffset = 0;
      yoffset -= 2;
    }
    current_room.close();
  }
}