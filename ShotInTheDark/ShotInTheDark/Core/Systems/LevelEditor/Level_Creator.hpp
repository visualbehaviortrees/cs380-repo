#include <list>
#include "Libraries/Math/Math2D.hpp"
#include "../../System.hpp"

using namespace Math;

enum Tiles
{
  Tiles_player = 0,
  Tiles_wall,
  Tiles_torch,
  Tiles_enemy,
  Tiles_destructable
};



class EditorSystem : public Gamestate
{
  public:

    struct Editor_Object
    {
      Tiles tile;
      Entity* selected_tile;
      bool can_destroy;
    };

    struct Editor_Actions
    {
      enum ACTION
      {
        CREATE = 0,
        DESTROYED,
        REPLACRED,
      };

      ACTION action;     // the action taken, either create or destroy
      Point2D position;  // the actual entity location
      Tiles tile_type;
      int node_position; // location of the node currently on the action list used for the undo_list_ptr to keep track of what node its on
      Tiles replaced;
    };


    EditorSystem(int Width, int Height);
    EditorSystem(const char* file);
    ~EditorSystem();
    void Init(void);
    void Update(float dt);
    void Shutdown(void);
    void PoppedTo(systype);
    
    void Load_Level(string level_file);
  private:
    void Set_Width(void);
    void Set_Height(void);
    void Find_Edges(void);
    void Save_To_File(std::vector<vector<int>> map);
    void CheckKeyEvents(void);
    bool Create_Tile(Point2D position, Tiles tile, bool is_loading = false, bool Can_Destroy_This = true); // position in screen cordinates
    void Destroy_Object(Entity* destroy_this);
    void Go_Back();
    void Go_Forward();
    void Save_Level();
    void Create_Default_Room(int Width, int Height);
    const char* Make_Next_Room(void);
    void Editor_Background(void);

    Tiles current_tile; // the current tile that is selected
    std::vector<Editor_Actions*> action_list;  // list of all the actions taken
    std::vector<Editor_Object*> object_list;  // the actual list of all the objects
    std::vector<Editor_Actions*> redo_list; // ctrl-y redo
    Editor_Actions* undo_list_ptr; // ctrl-z undo
    Editor_Object* object_copy; // ctrl-c


    string filepath;
    std::fstream current_room;
    int width, height;
    std::vector<Point2D> edges;
};




// CTRL-Z functionality:

// 1. if the user ctrl-z's an object then the action will be recorded on the redo_list
//
// 2. if the user ctrl-z's N number of objects and that number is not less than the number of objects
//    currently on the screen and proceeds to place an object the action list must be fixed to where the
//    undo_list_ptr points to the last action taken and the action list will end at the new object that was
//    placed. 
//
// EX: C's are creates, D's are destroys 
//     CCCCC - Action List
//         ^
//    undo_list_ptr 
//
//     Ctrl-z happens, opposite of C is Destroy
//     CCCC
//        ^
//   undo_list_ptr
//
//     User adds an object:
//     CCCCC
//         ^
//    undo_list_ptr
//
//    undo_list_ptr points at the last action done


// CTRL-Y functionality:

// 1. anytime the user ctrl-z's the action will be recorded on the redo_list
// 2. the redo_list is cleared when the user places a new object