//	File name:		NetworkClient.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Connor Drose
//	
//	All content � 2014 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "NetworkClient.h"
#include "../../Systems/Graphics/GraphicsSystem.hpp"

#include "../../Engine.hpp"
#include "../../../AntTweakBar/include/AntTweakBar.h"

#include <iostream>

// pointer to client to be used in threads
static NetworkClient *client;

// connect to server
bool NetworkClient::connect()
{
  const int timeout = 500000;   // how long to wait before giving up connecting
  try { sock_.connect(sa_, timeout); }
    
  catch (Poco::Exception error) {
    std::cout << "Connection failed (Error: " << error.displayText() << ')' << std::endl;
    return false;
  }
  std::cout << "Connection successful...\n";

  return true;
}


// USED TO CREATE THREAD 
class ListenClass: public Poco::Runnable
{
  virtual void run()
  {
    std::cout << "Client listen thread running...\n";
    while(1)
      client->listen();

    client = NULL;  // null terminate static global

    // sleeps this thread
    //Poco::Thread::sleep(1);

  }

};

// USED TO CREATE THREAD
class MessageClass: public Poco::Runnable
{
  virtual void run()
  {
    for(;;)
    {
      Sleep(20);
      std::cout << "Send message: \t";
      NetPacket sendPacket;
      std::cin >> sendPacket.msg;
      
      // detect if shutdown
      if(client == NULL){
        // shut down this thread
        return;
      }

      client->sendPacket(&sendPacket);

      Sleep(20);
    }

    //client = NULL;
  }
};

// listens for messages from server and handles them (running in a seperate thread)
// returns false if connection dies
bool NetworkClient::listen()
{
  // receive message back from server
  NetPacket recPackets[MAX_CLIENTS];

  int bytesRec = sock_.receiveBytes(recPackets, sizeof(recPackets));
  //recPackets = (NetPacket*)recPackets;
  std::cout << "BYTES RECEIVED:\t" << bytesRec << std::endl;

  // if server disconnects
  if(bytesRec == 0){
    client = NULL;  // tells other threads to shut down
    std::cout << "Server disconnected, client shutting down\n";
    return false;
  }

  // detect if anything is different
  //std::cout << "Received a packet from server\n";
  std::cout << "Received: \t" << recPackets[0].msg << std::endl << std::endl;

  return true;
}


// creates another thread that listens for messages from server
void NetworkClient::createThreads()
{
  ListenClass listenObj;
  MessageClass messageObj;

  listenThread.start(listenObj);
  messageThread.start(messageObj);

  client = this;
  Sleep(30); // this needs to be here to make the threads work for some reason

  //listenThread.join();
  //messageThread.join();
}


// sends message to server and all clients connected,
// returns how many bytes sent
int NetworkClient::sendPacket(NetPacket *packet)
{
  sentTime = (float)glfwGetTime();
  return sock_.sendBytes(packet, sizeof(NetPacket));
}


void NetworkClient::shutdown()
{
  // close down connection
  try{
    sock_.shutdown();
  } 
  catch (...) 
  {
    std::cout << "Connection closing failed: connection already closed\n" << std::endl;
  }
}






/// ANTWEAK BAR TESTING
void NetworkClient::AntTweakInit()
{
  //TwInit(TW_OPENGL, NULL);
  //GraphicsSystem *graphics = static_cast<GraphicsSystem *>(ENGINE->GetSystem(sys_Graphics));
  //int width = graphics->GetWindowWidth();
  //int height = graphics->GetWindowHeight();
  //TwWindowSize(width, height);

}


void NetworkClient::AntTweakUpdate()
{
  TwBar *myBar;
  myBar = TwNewBar("Tweak bar hiding in NetworkClient.cpp");

  // variants :    TwAddVarRW, TwAddVarRO, TwAddVarCB, or TwAddButton
  TwAddVarRW(myBar, "TweakTest", TW_TYPE_FLOAT, &tweakTest, " label='Time' precision=1 help='Time (in seconds).' "); // WAS ""


  TwDraw();
}