//	File name:		NetworkClient.h
//	Project name:	Shot In The Dark
//	Author(s):		Connor Drose
//	
//	All content � 2014 DigiPen (USA) Corporation, all rights reserved.

#pragma once

// This causes problems with SOIL, so we're ingnoring the warnings
#pragma warning(disable : 4005)

#include "Poco/Net/StreamSocket.h"
#include "Poco/Net/SocketStream.h"
#include "Poco/Net/SocketAddress.h"
#include "Poco/Exception.h"

#include "Poco/Thread.h"
#include "Poco/Runnable.h"


#include "../../System.hpp"

#include <string.h>

#define MAX_CLIENTS 10    // WAS 4

struct NetPacket
{
  char msg[50];

};


class NetworkClient
{
public:
  NetworkClient(std::string ip, int port) : sa_(ip, port) // create address of server
  {
    tweakTest = 0.f;
    AntTweakInit();
  }

  void shutdown();

  bool connect();
  void createThreads();
  int sendPacket(NetPacket *packet);
  bool listen();

  void AntTweakInit();
  void AntTweakUpdate();

private:
  Poco::Net::SocketAddress sa_;
  Poco::Net::StreamSocket sock_;

  Poco::Thread listenThread;
  Poco::Thread messageThread;

  float sentTime;


  float tweakTest;
};
