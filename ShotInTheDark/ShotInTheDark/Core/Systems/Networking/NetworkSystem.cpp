//	File name:		NetworkSystem.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Connor Drose
//	
//	All content � 2014 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "NetworkSystem.h"

#include "NetworkClient.h"
#define PORT 1234

//#define IP "127.0.0.1"      // LOCAL
//#define IP "10.0.0.10"      // CONNOR COMPUTER AT HOME
#define IP "192.168.82.245" // CONNOR COMPUTER AT DIGIPEN (hopefully... this one isn't a static IP)
//#define IP "192.168.82.245"

void NetworkSystem::Init()
{
  // TEMPORARY
  client = new NetworkClient(IP, PORT); // set ip address & port to connect to
  bool connected = client->connect();  // connect to server
  
  if(connected)
    client->createThreads();
}

void NetworkSystem::Update(float dt)
{
    
}

void NetworkSystem::Shutdown()
{
  client->shutdown();
  //delete client;
}
