//	File name:		NetworkSystem.h
//	Project name:	Shot In The Dark
//	Author(s):		Connor Drose
//	
//	All content � 2014 DigiPen (USA) Corporation, all rights reserved.

#include "../../System.hpp"
#include "../Networking/NetworkClient.h"

class NetworkSystem : public System
{
public:
  NetworkSystem(void) : System(sys_Networking, "Network System"){}
  void Init(void);
  void Update(float dt);
  void Shutdown(void);

  NetworkClient *client;
};


