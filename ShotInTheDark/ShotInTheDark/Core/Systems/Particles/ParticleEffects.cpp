//	File name:		ParticleEffects.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Gonzalo Rojo
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "ParticleSystem.h"

void CircleEffect::Init()
{
  p_sys = new ParticleSystem(m_particlecount);
  ParticleEmitter* emitter = new ParticleEmitter();
  EulerUpdater* e_updater = new EulerUpdater();
  LifeUpdater* l_updater = new LifeUpdater();
  ColorUpdater* c_updater = new ColorUpdater();
  TimeBombUpdater* tb_updater = new TimeBombUpdater(m_maxTime);
  Particle_Renderer* renderer = new Particle_Renderer();
  glm::vec3 maxOff = glm::vec3(0.5f,0.5f,0.0f);
  glm::vec3 radius = glm::vec3(1.f, 1.f, 0.0f);
  glm::vec3 pos = glm::vec3(mp_trans->Position.x, mp_trans->Position.y, 1.0f);
  RoundPositionGenerator *rpg = new RoundPositionGenerator(pos, radius);
  VelocityGenerator *vg = new VelocityGenerator(glm::vec3 (-0.4f, -0.4f, -0.2f), glm::vec3 (0.4f,0.4f,0.2f));
  ColorGenerator* cg = new ColorGenerator(m_minstartcolor, m_maxstartcolor, 
                                          m_minendcolor, m_maxendcolor);
  LifeGenerator* lg = new LifeGenerator(0.75f, 3.0f);
  emitter->m_rate = p_sys->GetMaxCount() / 2;
  emitter->AddGenerator(rpg);
  emitter->AddGenerator(vg);
  emitter->AddGenerator(lg);
  emitter->AddGenerator(cg);

  p_sys->AddEmitter(emitter);
  p_sys->AddUpdater(e_updater);
  p_sys->AddUpdater(l_updater);
  p_sys->AddUpdater(c_updater);
  p_sys->AddUpdater(tb_updater);
  p_sys->AddRenderer(renderer);


  p_sys->Init();
}

void CircleEffect::Update(float dt)
{
  p_sys->Update(dt);
}

void CircleEffect::Draw()
{
  p_sys->Draw();
}




void ChaseEffect::Init()
{
  p_sys = new ParticleSystem(m_particlecount);
  ParticleEmitter* emitter = new ParticleEmitter();
  EulerUpdater* e_updater = new EulerUpdater();
  LifeUpdater* l_updater = new LifeUpdater();
  ColorUpdater* c_updater = new ColorUpdater();
  TimeBombUpdater* tb_updater = new TimeBombUpdater(m_maxTime);
  Particle_Renderer* renderer = new Particle_Renderer();
  glm::vec3 maxOff = glm::vec3(0.5f, 0.5f, 0.0f);
  ChaseGenerator *rpg = new ChaseGenerator(mp_trans);
  VelocityGenerator *vg = new VelocityGenerator(glm::vec3(-0.4f, -0.4f, -0.2f), glm::vec3(0.4f, 0.4f, 0.2f));
  //BoxPositionGenerator* bpg = new BoxPositionGenerator(pos, maxOff);
  LifeGenerator* lg = new LifeGenerator(0.3f, 0.5f);
  ColorGenerator* cg = new ColorGenerator(m_minstartcolor, m_maxstartcolor,
    m_minendcolor, m_maxendcolor);
	emitter->m_rate = p_sys->GetMaxCount() / m_rate;
  emitter->AddGenerator(rpg);
  emitter->AddGenerator(vg);
  emitter->AddGenerator(lg);
  emitter->AddGenerator(cg);

  p_sys->AddEmitter(emitter);
  p_sys->AddUpdater(e_updater);
  p_sys->AddUpdater(l_updater);
  p_sys->AddUpdater(c_updater);
  p_sys->AddUpdater(tb_updater);
  p_sys->AddRenderer(renderer);


  p_sys->Init();
}

void ChaseEffect::Update(float dt)
{
  p_sys->Update(dt);
}

void ChaseEffect::Draw()
{
  p_sys->Draw();
}


void ShieldEffect::Init()
{
  p_sys = new ParticleSystem(m_particlecount);
  ParticleEmitter* emitter = new ParticleEmitter();
  EulerUpdater* e_updater = new EulerUpdater();
  LifeUpdater* l_updater = new LifeUpdater();
  ColorUpdater* c_updater = new ColorUpdater();
  TimeBombUpdater* tb_updater = new TimeBombUpdater(m_maxTime);
  Particle_Renderer* renderer = new Particle_Renderer();
  glm::vec3 maxOff = glm::vec3(0.5f, 0.5f, 0.0f);
  VelocityGenerator *vg = new VelocityGenerator(glm::vec3(0.f, 0.f, 0.0f), glm::vec3(0.f, 0.0f, 0.0f));
  //BoxPositionGenerator* bpg = new BoxPositionGenerator(pos, maxOff);
  glm::vec3 radius = glm::vec3(1.f, 1.f, 0.0f);
  RoundPositionChaseGenerator *rpcg = new RoundPositionChaseGenerator(mp_trans, radius);
  LifeGenerator* lg = new LifeGenerator(0.05f, 0.1f);
  ColorGenerator* cog = new ColorGenerator(m_minstartcolor, m_maxstartcolor,
    m_minendcolor, m_maxendcolor);

  emitter->m_rate = p_sys->GetMaxCount() / m_rate;
  emitter->AddGenerator(rpcg);
  emitter->AddGenerator(vg);
  emitter->AddGenerator(lg);
  emitter->AddGenerator(cog);

  p_sys->AddEmitter(emitter);
  p_sys->AddUpdater(e_updater);
  p_sys->AddUpdater(l_updater);
  p_sys->AddUpdater(c_updater);
  p_sys->AddUpdater(tb_updater);
  p_sys->AddRenderer(renderer);


  p_sys->Init();
}

void ShieldEffect::Update(float dt)
{
  p_sys->Update(dt);
}

void ShieldEffect::Draw()
{
  p_sys->Draw();
}


void GoToPosEffect::Init()
{
  p_sys = new ParticleSystem(m_particlecount);
  ParticleEmitter* emitter = new ParticleEmitter();
  GoToPosUpdater* g_updater = new GoToPosUpdater(m_PosToChase);
  EulerUpdater* e_updater = new EulerUpdater();
  ColorUpdater* c_updater = new ColorUpdater();
  Particle_Renderer* renderer = new Particle_Renderer();
  glm::vec3 radius = glm::vec3(1.f, 1.f, 0.0f);
  glm::vec3 pos = glm::vec3(mp_trans->Position.x, mp_trans->Position.y, 1.0f);
  RoundPositionGenerator *rpg = new RoundPositionGenerator(pos, radius);
  ColorGenerator* cg = new ColorGenerator(m_minstartcolor, m_maxstartcolor,
    m_minendcolor, m_maxendcolor);
 
  emitter->m_rate = p_sys->GetMaxCount() / 2;
  emitter->AddGenerator(rpg);
  emitter->AddGenerator(cg);

  p_sys->AddEmitter(emitter);

  p_sys->AddUpdater(g_updater);
  p_sys->AddUpdater(e_updater);
  p_sys->AddUpdater(c_updater);

  p_sys->AddRenderer(renderer);


  p_sys->Init();
}

void GoToPosEffect::Update(float dt)
{
  p_sys->Update(dt);
}

void GoToPosEffect::Draw()
{
  p_sys->Draw();
}

void FireEffect::Init()
{
  p_sys = new ParticleSystem(m_particlecount);
  ParticleEmitter* emitter = new ParticleEmitter();
  EulerUpdater* e_updater = new EulerUpdater();
  LifeUpdater* l_updater = new LifeUpdater();
  ColorUpdater* c_updater = new ColorUpdater();
  TimeBombUpdater* tb_updater = new TimeBombUpdater(m_maxTime);
  Particle_Renderer* renderer = new Particle_Renderer();
  glm::vec3 maxOff = glm::vec3(0.5f, 0.5f, 0.0f);
  glm::vec3 radius = glm::vec3(0.f, 0.f, 0.0f);
  glm::vec3 pos = glm::vec3(mp_trans->Position.x, mp_trans->Position.y, 1.0f);
  RoundPositionGenerator *rpg = new RoundPositionGenerator(pos, radius);
  VelocityGenerator *vg = new VelocityGenerator(glm::vec3(-0.1f, -0.05f, -0.6f), glm::vec3(0.1f, 0.2f, 0.6f));
  LifeGenerator* lg = new LifeGenerator(2.f, 3.f);
  ColorGenerator* cg = new ColorGenerator(m_minstartcolor, m_maxstartcolor,
    m_minendcolor, m_maxendcolor);

  emitter->m_rate = p_sys->GetMaxCount() / 2;
  emitter->AddGenerator(rpg);
  emitter->AddGenerator(vg);
  emitter->AddGenerator(lg);
  emitter->AddGenerator(cg);

  p_sys->AddEmitter(emitter);
  p_sys->AddUpdater(e_updater);
  p_sys->AddUpdater(l_updater);
  p_sys->AddUpdater(c_updater);
  p_sys->AddUpdater(tb_updater);
  p_sys->AddRenderer(renderer);


  p_sys->Init();
}

void FireEffect::Update(float dt)
{
  p_sys->Update(dt);
}

void FireEffect::Draw()
{
  YesOrNo = true;
  p_sys->Draw();
}


void ExplosionEffect::Init()
{
  p_sys = new ParticleSystem(m_particlecount);
  ParticleEmitter* emitter = new ParticleEmitter();
  EulerUpdater* e_updater = new EulerUpdater();
  LifeUpdater* l_updater = new LifeUpdater();
  ColorUpdater* c_updater = new ColorUpdater();
  TimeBombUpdater* tb_updater = new TimeBombUpdater(m_maxTime);
  Particle_Renderer* renderer = new Particle_Renderer();
  glm::vec3 maxOff = glm::vec3(0.5f, 0.5f, 0.0f);
  glm::vec3 radius = glm::vec3(0.0f, 0.0f, 0.0f);
  glm::vec3 pos = glm::vec3(mp_trans->Position.x, mp_trans->Position.y, 1.0f);
  RoundPositionGenerator *rpg = new RoundPositionGenerator(pos, radius);
  VelocityGenerator *vg = new VelocityGenerator(glm::vec3(-1.5f, -1.5f, -2.0f), glm::vec3(1.5f, 1.5f, 2.0f));
  LifeGenerator* lg = new LifeGenerator(0.5f, 0.75f);
  ColorGenerator* cg = new ColorGenerator();

  cg->m_minstartcolor = m_minstartcolor;
  cg->m_maxstartcolor = m_maxstartcolor;
  cg->m_minendcolor = m_minendcolor;
  cg->m_maxendcolor = m_maxendcolor;

  emitter->m_rate = p_sys->GetMaxCount() / 2;
  emitter->AddGenerator(rpg);
  emitter->AddGenerator(vg);
  emitter->AddGenerator(lg);
  emitter->AddGenerator(cg);
  emitter->AddGenerator(cg);

  p_sys->AddEmitter(emitter);
  p_sys->AddUpdater(e_updater);
  p_sys->AddUpdater(l_updater);
  p_sys->AddUpdater(c_updater);
  p_sys->AddUpdater(tb_updater);
  p_sys->AddRenderer(renderer);


  p_sys->Init();
}

void ExplosionEffect::Update(float dt)
{
  p_sys->Update(dt);
}

void ExplosionEffect::Draw()
{
  p_sys->Draw();
}
