//	File name:		ParticleEffects.h
//	Project name:	Shot In The Dark
//	Author(s):		Gonzalo Rojo
//	
//	All content � 2014 DigiPen (USA) Corporation, all rights reserved.
#include "../../Components/ParticleEffect.h"
//class ParticleSystem;
//
////COMPONENT
//
//class Particle_Effect
//{
//public:
//  virtual void Init() = 0;
//  virtual void Update() = 0;
//  virtual void Draw() = 0;
//};
//
//
//class CircleEffect : public Particle_Effect
//{
//public:
//  CircleEffect(int ParticleCount, double maxTime, glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f))
//  {
//    m_particlecount = ParticleCount;
//    m_maxTime = maxTime;
//    m_position = position;
//  }
//  virtual void Init();
//  virtual void Update();
//  virtual void Draw();
//  ParticleSystem *p_sys;
//  unsigned m_particlecount = 0;
//  double m_maxTime = 0.0;
//  glm::vec3 m_position;
//};