//	File name:		ParticleGenerator.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Gonzalo Rojo
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "ParticleSystem.h"

void RoundPositionGenerator::Generate(int startIndex, int endIndex, ParticleArray *p)
{
  double ang = glm::linearRand(0.0, _PI_ * 2.0);
  for (int i = startIndex; i < endIndex; ++i)
  {
    p->m_pos[i] = m_center + glm::vec3 (m_radius.x * sin(ang), m_radius.y * cos(ang), 0.0f);
  }
}

void RoundPositionChaseGenerator::Generate(int startIndex, int endIndex, ParticleArray *p)
{
  double ang;
  for (int i = startIndex; i < endIndex; ++i)
  {
    ang = glm::linearRand(0.0, _PI_ * 2.0);
    p->m_pos[i] = glm::vec3(m_trans->Position.x, m_trans->Position.y, 0) + glm::vec3(m_radius.x * sin(ang), m_radius.y * cos(ang), 0.0f);
  }
}

void ChaseGenerator::Generate(int startIndex, int endIndex, ParticleArray *p)
{
  double ang = glm::linearRand(0.0, _PI_ * 2.0);
  for (int i = startIndex; i < endIndex; ++i)
  {
    p->m_pos[i] = glm::vec3(m_trans->Position.x, m_trans->Position.y - m_trans->scale.y / 2 + glm::linearRand(0.f, m_trans->scale.y), 0);
  }
}

void VelocityGenerator::Generate(int startIndex, int endIndex, ParticleArray *p)
{
  for(int i = startIndex; i < endIndex; ++i)
  {
    p->m_vel[i] = glm::linearRand(m_minVel, m_maxVel);
  }
}

void BoxPositionGenerator::Generate(int startIndex, int endIndex, ParticleArray *p)
{
  glm::vec3 min_pos = m_position - offset;
  glm::vec3 max_pos = m_position + offset;
  
  for(int i = 0; i < endIndex; ++i)
  {
    p->m_pos[i] = glm::linearRand(min_pos, max_pos);
  }

}

void LifeGenerator::Generate(int startIndex, int endIndex, ParticleArray *p)
{
  for (int i = startIndex; i < endIndex; ++i)
  {
    p->m_time[i] = glm::linearRand(minLife, maxLife);
  }
}

void ColorGenerator::Generate(int startIndex, int endIndex, ParticleArray *p)
{
  for (int i = startIndex; i < endIndex; ++i)
  {
    p->m_startcol[i] = glm::linearRand(m_minstartcolor, m_maxstartcolor);
    p->m_endcol[i] = glm::linearRand(m_minendcolor, m_maxendcolor);
  }
}
