//	File name:		ParticleGenerator.h
//	Project name:	Shot In The Dark
//	Author(s):		Gonzalo Rojo
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

//////////////////////////////////////////////////////////////////////////
/// PARTICLE GENERATORS
//////////////////////////////////////////////////////////////////////////

struct ParticleArray;

template <typename T>
static T defaultParameter (float x = 0.2f, float y = 0.2f, float z = 0.2f, float w = 0.0f)
{
  switch (sizeof (T))
  {
 // case 8:
//    return glm::vec2 (x, y);
  case 12:
    return glm::vec3 (x,y,z);
 // case 16:
  //  return glm::vec4 (x,y,z,w);
  default:
    break;
  }
}

struct ParticleGenerator
{
  virtual void Generate(int startIndex, int endIndex, ParticleArray *p) = 0;

private:
  glm::vec3 min_pos;
  glm::vec3 max_pos;
};

struct RoundPositionGenerator : ParticleGenerator
{
  RoundPositionGenerator(glm::vec3 center, glm::vec3 radius = defaultParameter<glm::vec3>()) : m_radius(radius), m_center(center)
  {}
  virtual void Generate(int startIndex, int endIndex, ParticleArray *p);
  glm::vec3 m_radius;
  glm::vec3 m_center;
};

struct RoundPositionChaseGenerator : ParticleGenerator
{
  RoundPositionChaseGenerator(Transform* trans, glm::vec3 radius = defaultParameter<glm::vec3>()) : m_radius(radius), m_trans(trans)
  {}
  virtual void Generate(int startIndex, int endIndex, ParticleArray *p);
  glm::vec3 m_radius;
  Transform *m_trans;
};

struct ChaseGenerator : ParticleGenerator
{
  ChaseGenerator(Transform* trans) : m_trans(trans)
  {}
  virtual void Generate(int startIndex, int endIndex, ParticleArray *p);
  Transform *m_trans;
};


struct VelocityGenerator : ParticleGenerator
{

  VelocityGenerator(glm::vec3 minVel = defaultParameter<glm::vec3>(-0.1f,-0.1f,0.0f), glm::vec3 maxVel = defaultParameter<glm::vec3>(0.1f, 0.1f, 0.0f)) : m_minVel(minVel), m_maxVel(maxVel)
  {}
  virtual void Generate(int startIndex, int endIndex, ParticleArray *p);
  glm::vec3 m_minVel;
  glm::vec3 m_maxVel;
};

struct BoxPositionGenerator : ParticleGenerator
{
  BoxPositionGenerator(glm::vec3 position, glm::vec3 maxPos) : offset (maxPos), m_position(position)
  {}
  virtual void Generate(int startIndex, int endIndex, ParticleArray *p);
  glm::vec3 offset;
  glm::vec3 m_position;
};

struct LifeGenerator : ParticleGenerator
{
  LifeGenerator(float min = 0.1f, float max = 1.0f) : minLife(min), maxLife(max)
  {}
  virtual void Generate(int startIndex, int endIndex, ParticleArray *p);
  float minLife;
  float maxLife;
};

struct ColorGenerator : ParticleGenerator
{
  ColorGenerator(glm::vec4 minstartcolor = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f),
                 glm::vec4 maxstartcolor = glm::vec4(0.0f,0.0f, 0.0f, 1.0f),
                 glm::vec4 minendcolor = glm::vec4(0.5f, 0.0f, 0.3f, 0.5f),
                 glm::vec4 maxendcolor = glm::vec4(0.5f, 0.0f, 0.3f, 0.0f)
                 )
                 : m_minstartcolor(minstartcolor), m_maxstartcolor(maxstartcolor), 
                   m_minendcolor(minendcolor), m_maxendcolor(maxendcolor)
  {}
  virtual void Generate(int startIndex, int endIndex, ParticleArray *p);

  glm::vec4 m_minstartcolor;
  glm::vec4 m_maxstartcolor;
  glm::vec4 m_minendcolor;
  glm::vec4 m_maxendcolor;
};