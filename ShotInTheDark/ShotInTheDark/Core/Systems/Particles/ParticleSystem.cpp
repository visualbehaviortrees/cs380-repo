//	File name:		ParticleSystem.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Gonzalo Rojo
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "ParticleSystem.h"
#include "Particle_Renderer.h"
#include <algorithm>



void ParticleEmitter::Emit(ParticleArray *p_array)
{
  int startindex = p_array->m_aliveParticles;
  int endindex = std::min (p_array->m_aliveParticles + int (m_rate), p_array->m_maxCount - 1);

  for(auto& i : m_generators)
  {
    i->Generate(startindex,endindex, p_array);
  }

  for(int i = startindex; i< endindex; ++i)
  {
    p_array->Wake(i);
  }
}

void ParticleSystem::AddEmitter(ParticleEmitter* e)
{
  m_emitters.push_back(e);
}

void ParticleSystem::AddUpdater(ParticleUpdater* u)
{
  m_updaters.push_back(u);
}

void ParticleSystem::AddRenderer(Particle_Renderer* r)
{
  m_renderer = r;
}

void ParticleSystem::Init()
{
  m_renderer->Init(this);
}

void ParticleSystem::Update(float dt)
{
  for (auto& i : m_emitters)
  {
    i->Emit(particleData);
  }

  for (auto& i : m_updaters)
  {
    i->Update(particleData, dt);
  }

  if(particleData->m_aliveParticles == 0)
    Destroy();
}

void ParticleSystem::Draw()
{
  m_renderer->Render();
}

void ParticleSystem::Destroy()
{
  m_emitters.clear();
  m_updaters.clear();
}

glm::vec3* ParticleSystem::GetPositionData()
{
  return particleData->m_pos.data ();
}

glm::vec4* ParticleSystem::GetColorData()
{
  return particleData->m_col.data();
}

ParticleArray::ParticleArray(int maxCount) : m_maxCount(maxCount)
{
  for(int i = 0; i < maxCount; ++i)
  {
    m_pos.push_back(glm::vec3 (0,0,0));
    m_vel.push_back(glm::vec3 (0,0, 0));
    m_col.push_back(glm::vec4 (0,0,0,0));
    m_startcol.push_back(glm::vec4(0, 0, 0, 0));
    m_endcol.push_back(glm::vec4(0, 0, 0, 0));
    m_time.push_back(1);
    m_alive.push_back(false);
  }
}