//	File name:		ParticleSystem.h
//	Project name:	Shot In The Dark
//	Author(s):		Gonzalo Rojo
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../../Libraries/Math/Math2D.hpp"
#include "../Graphics/OpenGL.hpp"
#include "../Graphics/GraphicsMath.hpp"
#include "Particle_Renderer.h"
#include "ParticleGenerator.h"
#include "ParticleUpdater.h"
#include "ParticleEffects.h"
#include "../../Engine.hpp"
#include <iostream>
#include <vector>


struct ParticleArray
{
  std::vector <glm::vec3> m_pos;
  std::vector <glm::vec3> m_vel;
  std::vector <glm::vec4> m_startcol;
  std::vector <glm::vec4> m_endcol;
  std::vector <glm::vec4> m_col;
  std::vector <float> m_time;
  std::vector <bool> m_alive;
  const int m_maxCount;
  int m_aliveParticles;

  //////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////
  // PARTICLE DATA
  //////////////////////////////////////////////////////////////////////////


  ParticleArray(int maxCount);

  void Wake (int a)
  {
    ++m_aliveParticles;
    m_alive[a] = true;
  }

  void Kill(int a)
  {
        --m_aliveParticles;
    m_alive [a] = m_alive[m_aliveParticles - 1];
    m_pos [a] = m_pos [m_aliveParticles - 1];
    m_startcol [a] = m_startcol [m_aliveParticles - 1];
    m_endcol[a] = m_endcol[m_aliveParticles - 1];
    m_col[a] = m_col[m_aliveParticles - 1];
    m_time [a] = m_time[m_aliveParticles - 1];
  }
};


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// PARTICLE EMITER
//////////////////////////////////////////////////////////////////////////
class ParticleEmitter
{
  public:
    ParticleEmitter()
    {
      m_rate = 500;
    }
    void AddGenerator(ParticleGenerator* generator)
    {
      m_generators.push_back(generator);
    }
    void Emit(ParticleArray *p_array);
        int m_rate;
  private:
    std::vector <ParticleGenerator*> m_generators;
};

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// PARTICLE SYSTEM
//////////////////////////////////////////////////////////////////////////

class ParticleSystem
{
public:
  ParticleSystem(int maxCount)
  {
    particleData = new ParticleArray(maxCount);
    particleData->m_aliveParticles = 0;
    m_maxcount = maxCount;

  }

  void AddEmitter (ParticleEmitter*);
  void AddUpdater(ParticleUpdater*);
  void AddRenderer(Particle_Renderer*);

  void Init();
  void Update(float dt);
  void Draw();
  void Destroy();

  int GetMaxCount()
  {
    return m_maxcount;
  }
  int GetAliveParticleCount()
  {
    return particleData->m_aliveParticles;
  }
  glm::vec3* GetPositionData ();
  glm::vec4* GetColorData();
private:
  ParticleArray* particleData;
  int m_maxcount;
  Particle_Renderer* m_renderer;
  std::vector <ParticleEmitter*> m_emitters;
  std::vector <ParticleUpdater*> m_updaters;
};
