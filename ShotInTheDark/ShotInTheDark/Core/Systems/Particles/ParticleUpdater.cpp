//	File name:		ParticleUpdater.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Gonzalo Rojo
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "ParticleSystem.h"

void EulerUpdater::Update (ParticleArray *p, float dt)
{
  for(int i = 0; i < p->m_aliveParticles; ++i)
  {
    //p->m_pos[i] += p->m_vel[i];
    p->m_pos[i] += p->m_vel[i] * dt;
  }
}

void LifeUpdater::Update(ParticleArray *p_array, float dt)
{
  int endId = p_array->m_aliveParticles;

  for (int i = 0; i < endId; ++i)
  {
    p_array->m_time [i] -= dt;
    if (p_array->m_time[i] < 0.f)
    {
      p_array->Kill(i);
      endId = p_array->m_aliveParticles < p_array->m_maxCount ? p_array->m_aliveParticles : p_array->m_maxCount;
    }
  }
}

void ColorUpdater::Update(ParticleArray *p, float dt)
{
  for(int i = 0; i < p->m_aliveParticles; ++i)
  {
    p->m_col[i] = glm::mix(p->m_startcol[i], p->m_endcol[i], p->m_time[i]);
  }
}

void TimeBombUpdater::Update(ParticleArray *p_array, float dt)
{
  if(m_maxTime != -1.0)
  {
    int endId = p_array->m_aliveParticles;
    m_timeCounter += dt;
    if(m_maxTime <= m_timeCounter)
    {
      p_array->m_aliveParticles = 0;
    }
  }
  //std::cout << p_array->m_aliveParticles << std::endl;
}

void GoToPosUpdater::Update(ParticleArray* p_array, float dt)
{
  for (int i = 0; i < p_array->m_aliveParticles; ++i)
  {
    m_Distance.x = m_PosToGo->Position.x - p_array->m_pos[i].x;
    m_Distance.x = m_PosToGo->Position.y - p_array->m_pos[i].y;
    m_Distance.x = 0.0f - p_array->m_pos[i].z;

    if (m_Distance.x != 0.0f && m_Distance.y != 0.0f && m_Distance.z != 0.0f)
    {
      if (m_Distance.x > 0.0f)
        p_array->m_vel[i].x = 0.1f;
      else if (m_Distance.x < 0.0f)
        p_array->m_vel[i].x = -0.1f;
    }
  }
}