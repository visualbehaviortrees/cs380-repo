//	File name:		ParticleUpdater.h
//	Project name:	Shot In The Dark
//	Author(s):		Gonzalo Rojo
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../Graphics/GraphicsMath.hpp"


struct ParticleArray;

struct ParticleUpdater
{
  virtual void Update (ParticleArray *p_array, float dt){}
};


struct EulerUpdater : ParticleUpdater
{
  EulerUpdater()
  {
    gravity = glm::vec3 (0, -9.8f, 0);
  }
  virtual void Update (ParticleArray *p_array, float dt) override;
  glm::vec3 gravity;
};

struct LifeUpdater : ParticleUpdater
{
  virtual void Update (ParticleArray *p_array, float dt) override; 
  float m_minTime;
  float m_maxTime;
};

struct ColorUpdater : ParticleUpdater
{
  virtual void Update(ParticleArray *p_array, float dt) override;
};

struct TimeBombUpdater : ParticleUpdater
{
  TimeBombUpdater(double maxTime)
  {
    m_maxTime = maxTime;
    m_timeCounter = 0.0;
  }
  virtual void Update(ParticleArray *p_array, float dt) override;
  double m_maxTime;
  double m_timeCounter;
};

struct GoToPosUpdater : ParticleUpdater
{
  GoToPosUpdater(Transform* pos)
  {
    m_PosToGo = pos;
    m_Distance = glm::vec3(0.0f, 0.0f, 0.0f);
  }
  virtual void Update(ParticleArray* p_array, float dt) override;
  Transform* m_PosToGo;
  glm::vec3 m_Distance;
};