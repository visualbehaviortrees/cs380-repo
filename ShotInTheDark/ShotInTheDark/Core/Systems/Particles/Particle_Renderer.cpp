//	File name:		Particle_Renderer.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Gonzalo Rojo
//	
//	All 
#include "stdafx.h"
#include "ParticleSystem.h"
#include "../Graphics/BufferObjects.hpp"
#include "../Graphics/Graphics.hpp"
#include "../Graphics/LoadShader.hpp"
#include "../Graphics/soil/SOIL.h"

const int postionXYZ = 3; // xyz pos
GLuint ParticleProgram;
Math::Matrix4 wv_m;
Math::Matrix4 vp_m;
Texture* texture;
GLuint uniWVM = 0;
GLuint uniVPM = 0;

void Particle_Renderer::Specify_Attributes()
{
  GLuint posAttrib = glGetAttribLocation(ParticleProgram,"position");
  glEnableVertexAttribArray(posAttrib);
  glVertexAttribPointer(posAttrib, 3, GL_FLOAT,GL_FALSE, 3 * sizeof(float), 0);

  GLuint colAttrib = glGetAttribLocation(ParticleProgram, "color");
  glEnableVertexAttribArray(colAttrib);
  glVertexAttribPointer(colAttrib, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 0);
}

void Particle_Renderer::Init(ParticleSystem* sys)
{

  p_sys = sys;
  if (!ParticleProgram)
  {
    texture = RESOURCES->Get_Texture("Particle.bmp");
    ParticleProgram = LoadShaders("Core/Systems/Graphics/Shaders/ParticleVertexShader.glsl", "Core/Systems/Graphics/Shaders/ParticleFragmentShader.glsl");
  }

  glGenBuffers(1, &vbo_pos);
  glBindBuffer (GL_ARRAY_BUFFER, vbo_pos);
  GLint posAttrib = glGetAttribLocation(ParticleProgram, "position");
  glBufferData(GL_ARRAY_BUFFER, postionXYZ * p_sys->GetMaxCount() * sizeof (float), nullptr, GL_STREAM_DRAW);
  glEnableVertexAttribArray(posAttrib);
  glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 3 * sizeof (float), 0);

  glGenBuffers(1, &vbo_col);
  glBindBuffer (GL_ARRAY_BUFFER, vbo_col);
  GLint colAttrib = glGetAttribLocation(ParticleProgram, "color");
  glBufferData(GL_ARRAY_BUFFER, 4 * p_sys->GetMaxCount() * sizeof (float), nullptr, GL_STREAM_DRAW);
  glEnableVertexAttribArray(colAttrib);
  glVertexAttribPointer(colAttrib, 4, GL_FLOAT, GL_FALSE, 4 * sizeof (float), 0);
  //Specify_Attributes();
  vao. unBind();
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  texture->bind();
  GLuint uniTex = glGetUniformLocation(ParticleProgram, "image");
  glUniform1i(uniTex, 0);
  texture->unBind();

  currWindow = GETSYS(Graphics)->GetCurrentWindow();

  uniWVM = glGetUniformLocation(ParticleProgram, "wv_m");
  uniVPM = glGetUniformLocation(ParticleProgram, "vp_m");
}

void Particle_Renderer::Render()
{
  if (p_sys->GetAliveParticleCount() > 0)
  {
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    texture->bind();
    wv_m = Math::Inverse(*currWindow->Current_CameraToWorld);
    vp_m = currWindow->CameraToNDC;
    glEnable(GL_POINT_SPRITE);
    glEnable(GL_PROGRAM_POINT_SIZE);
    glUseProgram(ParticleProgram);


    glUniformMatrix4fv(uniWVM, 1, GL_FALSE, &wv_m.matrix4[0][0]);
    glUniformMatrix4fv(uniVPM, 1, GL_FALSE, &vp_m.matrix4[0][0]);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_pos);
    glBufferSubData(GL_ARRAY_BUFFER, 0, p_sys->GetAliveParticleCount() * 3 * sizeof (float), (void*)p_sys->GetPositionData());
    glBindBuffer(GL_ARRAY_BUFFER, vbo_col);
    glBufferSubData(GL_ARRAY_BUFFER, 0, p_sys->GetAliveParticleCount() * 4 * sizeof(float), (void*)p_sys->GetColorData());
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glPointSize(20);
    vao.bind();
    glDrawArrays(GL_POINTS, 0, p_sys->GetAliveParticleCount());
    glUseProgram(0);
    vao.unBind();
    texture->unBind();
  }
}