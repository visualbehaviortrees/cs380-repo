//	File name:		Particle_Renderer.h
//	Project name:	Shot In The Dark
//	Author(s):		Gonzalo Rojo
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

class ParticleSystem;

class Particle_Renderer
{
public:
  Particle_Renderer(){};
  ~Particle_Renderer(){};
  void Init(ParticleSystem* sys);
  void Render();
  void Specify_Attributes();

private:
  ParticleSystem *p_sys;
  Window* currWindow;
  VAO vao;
  GLuint vbo_pos;
  GLuint vbo_col;
};