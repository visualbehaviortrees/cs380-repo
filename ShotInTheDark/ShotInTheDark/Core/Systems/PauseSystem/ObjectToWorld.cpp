//	File name:		PauseSystem.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include <vector>
#include <iostream>
#include "ObjectToWorld.hpp"
#include "../../Engine.hpp"
#include "../../Components/Components.h"
#include "../../Libraries/Math/Math2D.hpp"
#include "../Graphics/ResourceManager.h"

extern Engine *ENGINE;
void ObjectToWorldSystem::Init()
{
	REGISTER_COMPONENT(Transform);
}

void ObjectToWorld(Transform* TF, RigidBody* RB, float dt)
{
	TF->transform = Math::Trans(TF->Position + (RB->vel*dt)) *
		Math::Rot(TF->rotation) *
		Math::Scale(TF->scale.X(), TF->scale.Y());
}

// I wrote this for objects that are visible but not collidable
// - <3 Allan
void ObjectToWorld(Transform* TF)
{
	TF->transform = Math::Trans(TF->Position) *
		Math::Rot(TF->rotation) *
		Math::Scale(TF->scale.X(), TF->scale.Y());
}

void ObjectToWorldSystem::Update(float dt)
{  
	// I wrote this for objects that are visible but not collidable
	// - <3 Allan
	std::vector<Entity *> transformers = ENGINE->OM->FilterEntities(MC_Transform);
	for (auto it : transformers)
	{
		if (it->HasComponent(MC_Drawable))
			if (it->GET_COMPONENT(Drawable)->clipped)
				continue;

		// If it has a rigidbody, include that info with dt when computing the Object to World matrix
		if (it->HasComponent(MC_RigidBody))
			ObjectToWorld(it->GET_COMPONENT(Transform), it->GET_COMPONENT(RigidBody), dt);
		// Otherwise just use the static position.
		else
			ObjectToWorld(it->GET_COMPONENT(Transform));
	}
}

void ObjectToWorldSystem::Shutdown()
{

}

