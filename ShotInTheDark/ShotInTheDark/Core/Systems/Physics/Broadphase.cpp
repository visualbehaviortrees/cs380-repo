//	File name:		Broadphase.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Dereck Crouse
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "Broadphase.hpp"
#include "Collision.hpp"
#include "../Core/Systems/Gameplay/DungeonGameplay.h"

float BP_ROOM_HEIGHT = 12.0f;
float BP_ROOM_WIDTH = 28.0f;

void BroadPhaseSystem::Init()
{
	REGISTER_COMPONENT(Transform);
	REGISTER_COMPONENT(RigidBody);
}

void BroadPhaseSystem::Update(float dt)
{
	//if (check)
	//{
	//	Statics.MaxEntities() = 10;
	//	Statics.InitializeBroadphase(_entities, 200);
	//
	//	check = 0;
	//}

}

void BroadPhaseSystem::Shutdown(void)
{

}

Broadphase2D::Broadphase2D(void)
{
	EntitiesPerList = 0;
	Initialized = false;
	update = true;
}

Broadphase2D::Broadphase2D(unsigned num)
{
	EntitiesPerList = num;
	Initialized = false;
	update = true;
}

Broadphase2D::Broadphase2D(const Broadphase2D& rhs)
{
	EntitiesPerList = rhs.EntitiesPerList;
	Nodes = rhs.Nodes;

	for (unsigned i = 0; i < rhs.CollisionSubLists.size(); ++i)
		CollisionSubLists[i] = rhs.CollisionSubLists[i];

	update = true;
	Initialized = true;
}

Broadphase2D::~Broadphase2D(void) {}

Broadphase2D& Broadphase2D::operator= (const Broadphase2D& rhs)
{
	EntitiesPerList = rhs.EntitiesPerList;
	Nodes = rhs.Nodes;

	for (unsigned i = 0; i < rhs.CollisionSubLists.size(); ++i)
		CollisionSubLists[i] = rhs.CollisionSubLists[i];

	Initialized = true;
	update = true;

	return *this;
}

void Broadphase2D::InitializeBroadphase(const std::vector<Entity*>& ObList, float size)
{

	if (update && Initialized)
	{
		for (unsigned i = 0; i < CollisionSubLists.size(); ++i)
			CollisionSubLists[i].clear();
		CollisionSubLists.clear();

		Nodes.clear();
		update = false;
	}

	Nodes.push_back(Node(Square(Math::Point2D(0, 0), size), 0));

	std::vector<Entity*> Static = ObList;
	StaticFilter(Static);

	std::vector<Entity*> temp;

	for (unsigned i = 0; i < Static.size(); ++i)
	{
		if (AABB_CollisionCheck(Static[i], Nodes[0].COLLIDER()))
		{
			temp.push_back(Static[i]);
			++(Nodes[0].HITS());
		}
	}

	if (Static.size() != temp.size())
		assert("Broadphase Collision too small, increase size");

	if (Nodes[0].HITS() <= EntitiesPerList)
	{
		CollisionSubLists.push_back(temp);
		return;
	}

	unsigned Size = Nodes.size();
	unsigned PassCount = 0;

	do
	{
		Size = Nodes.size();
		PassCount = 0;

		for (auto it = Nodes.begin(); it != Nodes.end(); ++it)
		{
			if (it->HITS() > EntitiesPerList)
			{
				SplitNode(it - Nodes.begin(), Static);
				break;
			}
			else
				++PassCount;
		}
	} while (Size > PassCount);

	std::vector<Node> temp_Nodes;

	for (unsigned i = 0; i < Nodes.size(); ++i)
	{
		if (Nodes[i].HITS())
			temp_Nodes.push_back(Nodes[i]);
	}

	Nodes = temp_Nodes;

	for (unsigned j = 0; j < Nodes.size(); ++j)
	{
		std::vector<Entity*> temp;

		for (unsigned i = 0; i < Static.size(); ++i)
		{
			if (AABB_CollisionCheck(Nodes[j].COLLIDER(), Static[i]))
				temp.push_back(Static[i]);
		}

		if (temp.size())
			CollisionSubLists.push_back(temp);
	}

	Initialized = true;
}

void StaticFilter(std::vector<Entity*>& list)
{
	std::vector<Entity*> temp;

	for (unsigned i = 0; i < list.size(); ++i)
	{
		if ((list[i])->GET_COMPONENT(RigidBody)->isStatic && list[i]->GetActive())
			temp.push_back(list[i]);
	}

	list = temp;
}

void Broadphase2D::SplitNode(unsigned i, const std::vector<Entity*>& obList)
{
	float length = Nodes[i].COLLIDER().SIDE() * 0.5f;

	Math::Point2D PosPos_Center = Nodes[i].COLLIDER().CENTER() +
		Math::Vector2D((length*0.5f), (length*0.5f));
	Math::Point2D PosNeg_Center = Nodes[i].COLLIDER().CENTER() +
		Math::Vector2D((length*0.5f), -(length*0.5f));
	Math::Point2D NegPos_Center = Nodes[i].COLLIDER().CENTER() +
		Math::Vector2D(-(length*0.5f), (length*0.5f));
	Math::Point2D NegNeg_Center = Nodes[i].COLLIDER().CENTER() +
		Math::Vector2D(-(length*0.5f), -(length*0.5f));

	Node splitters[4] = { Square(PosPos_Center, length),
		Square(PosNeg_Center, length),
		Square(NegPos_Center, length),
		Square(NegNeg_Center, length) };

	for (unsigned j = 0; j < 4; ++j)
		splitters[j].SetHits(obList);

	Nodes[i] = splitters[3];

	std::vector<Node>::iterator it = i + Nodes.begin();

	Nodes.insert(it, splitters, splitters + 3);
}

void Node::SetHits(const std::vector<Entity*>& obList)
{
	for (unsigned i = 0; i < obList.size(); ++i)
	{
		//Entity* watch = obList[i];
		if (AABB_CollisionCheck(obList[i], Collider))
			++Hits;
	}
}

void Node3D::SetHits(const std::vector<Entity*>& obList)
{
	Hits = 0;

	for (unsigned i = 0; i < obList.size(); ++i)
	{
		//Entity* watch = obList[i];
		if (AABB_CollisionCheck(obList[i], Collider))
			++Hits;
	}
}

Room& Room::operator= (const Room& rhs)
{
	Collider = rhs.Collider;
	Hits = rhs.Hits;
	return *this;
}

Broadphase& Broadphase::operator = (const Broadphase& rhs)
{
	EntitiesPerList = rhs.EntitiesPerList;
	Initialized = rhs.Initialized;
	update = rhs.update;
	return *this;
}

Broadphase3D::Broadphase3D(void)
{
	EntitiesPerList = 0;
	Initialized = false;
	update = false;
}

Broadphase3D::Broadphase3D(unsigned num)
{
	EntitiesPerList = num;
	Initialized = false;
	update = false;
}

Broadphase3D::Broadphase3D(const Broadphase3D& rhs)
{
	EntitiesPerList = rhs.EntitiesPerList;
	Initialized = rhs.Initialized;
	update = rhs.update;

	CollisionSubLists = rhs.CollisionSubLists;
	Nodes = rhs.Nodes;
}

Broadphase3D::~Broadphase3D(void)
{
	this->CollisionSubLists.clear();
	this->Nodes.clear();
}

Broadphase3D& Broadphase3D::operator= (const Broadphase3D& rhs)
{
	EntitiesPerList = rhs.EntitiesPerList;
	Initialized = rhs.Initialized;
	update = rhs.update;

	CollisionSubLists = rhs.CollisionSubLists;
	Nodes = rhs.Nodes;

	return *this;
}
std::vector<std::vector<Entity*>>& Broadphase3D::operator[] (unsigned index)
{
	if (index >= this->CollisionSubLists.size())
		assert("indexing outside of Broadphase array");

	return this->CollisionSubLists[index];
}

void Broadphase3D::InitializeBroadphase(const std::vector<Entity*>& ObList, float Size)
{
	if (update && Initialized)
	{
		CollisionSubLists.clear();
		Nodes.clear();

		update = false;
	}

	std::vector<Entity*> Static = ObList;

	StaticFilter(Static);

	GetRooms(*this, ObList);

	if (!Nodes.size())
		assert("Broadphase failed to initialize");

	// Create Nodes for each room
	for (unsigned i = 0; i < Nodes.size(); ++i)
	{
		Node3D temp;

		temp.COLLIDER() = Nodes[i].first.COLLIDER();
		temp.SetHits(Static);

		Nodes[i].first.HITS() = temp.HITS();
		Nodes[i].first.ROOMNUMBER() = i;
		Nodes[i].second.push_back(temp);
	}

	// Loop through each Room
	for (unsigned i = 0; i < Nodes.size(); ++i)
	{
		unsigned OldSize = 0;
		unsigned PassCount = 0;

		do
		{
			// Get the number of nodes for growth comparison
			OldSize = Nodes[i].second.size();
			PassCount = 0;

			for (auto it = Nodes[i].second.begin(); it != Nodes[i].second.end(); ++it)
			{
				// Check Hits vs EntitiesPerList
				// if too many hits
				unsigned check = it->HITS();

				if (it->HITS() > EntitiesPerList)
				{
					// split the nodes
					SplitNode(i, it - Nodes[i].second.begin(), Static);
					break;
				}
				else
				{
					++PassCount;
				}
			}
			// break when we loop through more times than the size
		} while (OldSize > PassCount);
	}

	// Now lets fill the vector with Entities
	for (unsigned i = 0; i < Nodes.size(); ++i)
	{
		std::vector<std::vector<Entity*>> middle;

		for (unsigned j = 0; j < Nodes[i].second.size(); ++j)
		{
			std::vector<Entity*> bottom;

			for (auto it : Static)
			{
				if (AABB_CollisionCheck(Nodes[i].second[j].COLLIDER(), it))
				{
					bottom.push_back(it);
				}
			}

			middle.push_back(bottom);
		}

		CollisionSubLists.push_back(middle);
	}

	Initialized = true;
}

void Broadphase3D::SplitNode(unsigned Room, unsigned index, const std::vector<Entity*>& s)
{
	// index is of the current node

	// the lengths and widths of the new rooms are 1/2 of the old
	float high = Nodes[Room].second[index].COLLIDER().HEIGHT() / 2.0f;
	float wide = Nodes[Room].second[index].COLLIDER().WIDTH() / 2.0f;

	// get four new centers for the new nodes
	std::vector<Math::Point2D> Centers;

	for (unsigned i = 0; i < 4; ++i)
		Centers.push_back(Nodes[Room].second[index].COLLIDER().CENTER());
	Centers[0] -= Math::Vector2D(wide * 0.5f, high * 0.5f);
	Centers[1] += Math::Vector2D(wide * 0.5f, high * 0.5f);
	Centers[2] -= Math::Vector2D(wide * 0.5f, -high * 0.5f);
	Centers[3] += Math::Vector2D(wide * 0.5f, -high * 0.5f);

	// create an set the new nodes
	Node3D splitters[4];

	for (unsigned i = 0; i < 4; ++i)
		splitters[i].COLLIDER() = Rect(Centers[i], high, wide);

	// set hits for the new nodes
	for (unsigned i = 0; i < 4; ++i)
		splitters[i].SetHits(s);

	Nodes[Room].second[index] = splitters[3];

	// add the new nodes to the list of nodes
	std::vector<Node3D>::iterator it = index + Nodes[Room].second.begin();

	Nodes[Room].second.insert(it, splitters, splitters + 3);
}

void GetRooms(Broadphase3D& RoomList, const std::vector<Entity*>& ObList)
{
	DungeonGameplay* dungeon = dynamic_cast<DungeonGameplay*>(ENGINE->getCurrentGamestate());

	if (dungeon == nullptr)
		return;

	// Update the width and height of rooms
	BP_ROOM_WIDTH = dungeon->GetRoomDimensions().x;
	BP_ROOM_HEIGHT = dungeon->GetRoomDimensions().y;


	// Get the Rooms into Nodes[i].first
	// Get the Center of Each Room
	std::vector<Math::Point2D> Centers;
	std::vector<Point2D> NWSE;

	// The vectors to add to centers for next rooms
	Math::Vector2D Horizontal(BP_ROOM_WIDTH, 0);
	Math::Vector2D Vertical(0, BP_ROOM_HEIGHT);

	// Start at (0,0) Nodes[0].first
	Point2D Start = dungeon->GetRoomCenter(Point2D(0, 0));

	Centers.push_back(Start);

	// while size of first list != size of last list
	unsigned OldSize;

	do
	{
		OldSize = Centers.size();

		// Get centers for N, W, S, E
		NWSE.clear();

		for (auto it : Centers)
		{
			NWSE.push_back(it + Horizontal);
			NWSE.push_back(it - Horizontal);
			NWSE.push_back(it + Vertical);
			NWSE.push_back(it - Vertical);
		}

		Math::Point2D temp;

		for (auto it : NWSE)
		{
			temp = dungeon->GetRoomCenter(it);

			// if InARoom(current_center) == true;
			if (InARoom(temp, ObList))
			{
				// Add the Center to a vector of points
				Centers.push_back(temp);
			}
		}

		// cull the excess points
		for (auto it = Centers.begin(); it != Centers.end(); ++it)
		{
			for (auto it2 = Centers.begin(); it2 != Centers.end(); ++it2)
			{
				// If any of these Centers exist in our vector already
				if (*it == *it2 && it != it2)
				{
					Centers.erase(it2);
					it = Centers.begin();
					it2 = Centers.begin();
				}
			}
		}

	} while (OldSize != Centers.size());

	for (auto it : Centers)
	{
		std::pair<Room, std::vector<Node3D>> temp;

		temp.first.COLLIDER().WIDTH() = BP_ROOM_WIDTH;
		temp.first.COLLIDER().HEIGHT() = BP_ROOM_HEIGHT;
		temp.first.COLLIDER().CENTER() = it;

		RoomList.Nodes.push_back(temp);
	}
}

bool InARoom(const Math::Point2D& Center, const std::vector<Entity*>& ObjectList)
{
	DungeonGameplay* dungeon = dynamic_cast<DungeonGameplay*>(ENGINE->getCurrentGamestate());
	Math::Point2D Cen = dungeon->GetRoomCenter(Center);

	// using center, find the 4 corners of the room
	std::vector<Math::Point2D> Corners;
	Math::Vector2D Translate(BP_ROOM_WIDTH / 2.0f, BP_ROOM_HEIGHT / 2.0f);

	Corners.push_back(Cen + Translate);
	Corners.push_back(Cen - Translate);
	Corners.push_back(Cen + Math::Vector2D(-Translate.x, Translate.y));
	Corners.push_back(Cen - Math::Vector2D(-Translate.x, Translate.y));

	// for each point 
	for (auto it : Corners)
	{
		// if a corner isn't hitting an object
		if (AABB_PointCollision(it) == NULL)
			return false;
	}
	return true;
}

int Broadphase3D::GetCurrentRoom(void)
{
	Math::Point2D CamPos = GETSYS(Graphics)->GetCameraPosition();

	for (unsigned Room = 0; Room < Nodes.size(); ++Room)
	{
		if (AABB_CollisionCheck(CamPos, Nodes[Room].first.COLLIDER()))
			return Room;
	}

	return -1;
}