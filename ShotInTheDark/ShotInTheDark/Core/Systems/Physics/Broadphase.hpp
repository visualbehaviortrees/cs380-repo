//	File name:		Broadphase.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Dereck Crouse
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "stdafx.h"
#include <vector>
#include "Shape.hpp"


class BroadPhaseSystem : public System
{
public:
	BroadPhaseSystem() :System(sys_Physics, "BroadPhase System"){}
	void Init(void);
	void Update(float dt);
	void Shutdown(void);
};

class Node
{
public:
	Node(void) : Collider(Square()), Hits(0) {}
	Node(const Square& S, unsigned H = 0) : Collider(S), Hits(H) {}
	Node(const Node& rhs) : Collider(rhs.Collider), Hits(rhs.Hits) {}
	~Node(void) {}
	Node& operator= (const Node& rhs)
	{
		Collider = rhs.Collider; Hits = rhs.Hits;  return *this;
	}

	Square& COLLIDER(void) { return Collider; }
	unsigned& HITS(void) { return Hits; }

	const Square& COLLIDER(void) const { return Collider; }
	unsigned HITS(void) const { return Hits; }

	void SetHits(const std::vector<Entity*>&);

private:
	Square Collider;
	unsigned Hits;
};

class Node3D
{
public:
	Node3D(void) : Collider(Rect()), Hits(0) {}
	Node3D(const Rect& S, unsigned H = 0) : Collider(S), Hits(H) {}
	Node3D(const Node3D& rhs) : Collider(rhs.Collider), Hits(rhs.Hits) {}
	~Node3D(void) {}
	Node3D& operator= (const Node3D& rhs)
	{
		Collider = rhs.Collider; Hits = rhs.Hits;  return *this;
	}

	Rect& COLLIDER(void) { return Collider; }
	unsigned& HITS(void) { return Hits; }

	const Rect& COLLIDER(void) const { return Collider; }
	unsigned HITS(void) const { return Hits; }

	void SetHits(const std::vector<Entity*>&);

private:
	Rect Collider;
	unsigned Hits;
};

class Room
{
public:
	Room(void) { Hits = 0; Collider = Rect(Math::Point2D(), 0, 0); }
	Room(Math::Point2D C, float W = 0, float H = 0, unsigned HITS = 0)
	{
		Hits = HITS; Collider = Rect(C, W, H);
	}
	Room(const Room& rhs) :
		Collider(rhs.Collider), Hits(rhs.Hits) {}
	~Room(void) {}
	Room& operator= (const Room& rhs);

	Rect& COLLIDER(void) { return Collider; }
	Rect COLLIDER(void) const { return Collider; }

	unsigned& HITS(void) { return Hits; }
	unsigned HITS(void) const { return Hits; }

	unsigned& ROOMNUMBER(void) { return RoomNumber; }
	unsigned ROOMNUMBER(void) const { return RoomNumber; }

private:
	Rect Collider;
	unsigned Hits;
	unsigned RoomNumber;
};

class Broadphase
{
public:
	Broadphase(unsigned E = 0, bool I = false, bool U = false)
		: EntitiesPerList(E), Initialized(I), update(U) {}
	Broadphase(const Broadphase& rhs)
		: EntitiesPerList(rhs.EntitiesPerList), Initialized(rhs.Initialized), update(rhs.update) {}
	~Broadphase(void) {}
	Broadphase& operator= (const Broadphase&);

	unsigned& MaxEntities(void) { return EntitiesPerList; }
	unsigned MaxEntities(void) const { return EntitiesPerList; }

	bool& UPDATE(void) { return update; }
	bool UPDATE(void) const { return update; }
	void Update(void) { update = true; }

	bool& INITIALIZED(void) { return Initialized; }
	bool INITIALIZED(void) const { return Initialized; }

protected:
	unsigned EntitiesPerList;
	bool Initialized;
	bool update;
};

class Broadphase2D : public Broadphase
{
public:
	Broadphase2D(void);
	Broadphase2D(unsigned);
	Broadphase2D(const Broadphase2D&);
	~Broadphase2D(void);
	Broadphase2D& operator= (const Broadphase2D&);

	void InitializeBroadphase(const std::vector<Entity*>&, float);
	void SplitNode(unsigned, const std::vector<Entity*>&);

	std::vector<Node> Nodes;
	std::vector<std::vector<Entity*>> CollisionSubLists;
};

class Broadphase3D : public Broadphase
{
public:
	Broadphase3D(void);
	Broadphase3D(unsigned);
	Broadphase3D(const Broadphase3D&);
	~Broadphase3D(void);
	Broadphase3D& operator= (const Broadphase3D&);
	std::vector<std::vector<Entity*>>& operator[] (unsigned);

	unsigned CURRENTROOM(void) const { return CurrentRoom; }
	unsigned& CURRENTROOM(void) { return CurrentRoom; }

	unsigned PREVIOUSROOM(void) const { return PreviousRoom; }
	unsigned& PREVIOUSROOM(void) { return PreviousRoom; }

	void InitializeBroadphase(const std::vector<Entity*>&, float);
	void SplitNode(unsigned, unsigned, const std::vector<Entity*>&);

	std::vector<std::pair<Room, std::vector<Node3D>>> Nodes;
	std::vector<std::vector<std::vector<Entity*>>> CollisionSubLists;

	int GetCurrentRoom(void);

private:
	unsigned CurrentRoom;
	unsigned PreviousRoom;
};

void StaticFilter(std::vector<Entity*>&);
void GetRooms(Broadphase3D& RoomList, const std::vector<Entity*>&);
bool InARoom(const Math::Point2D&, const std::vector<Entity*>&);