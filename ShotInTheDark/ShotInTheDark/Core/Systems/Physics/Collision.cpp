//	File name:		Collision.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Dereck Crouse
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "Collision.hpp"
#include "../Graphics/Graphics.hpp"
#include "PhysicsSystem.hpp"
#include "../Test/TestSystem.hpp"

#define DEBUG 0

#define ALLANHANDEDMETHISSTUFF 1

bool CollisionFound(Entity *e1, Entity *e2)
{
#if DEBUG
	if (!e1->hasComponent(EC_RigidBody) || !e2->hasComponent(EC_RigidBody) || !e1->hasComponent(EC_Transform) || !e2->hasComponent(EC_Transform))
		return false
#endif
		for (auto &it : e1->GET_COMPONENT(RigidBody)->collisionList)
			if (it.object == e2)
				return true;
	return false;
}

void CollisionResolved(Entity *e1, Entity *e2)
{
	for (auto &it : e1->GET_COMPONENT(RigidBody)->collisionList)
		if (it.object == e2)
		{
		it.resolved = true;
		break;
		}
	for (auto &it : e2->GET_COMPONENT(RigidBody)->collisionList)
		if (it.object == e1)
		{
		it.resolved = true;
		break;
		}

}


bool PotatoPhysics(Entity *e1, Entity *e2)
{
	Transform *t1 = (e1)->GET_COMPONENT(Transform);
	Transform *t2 = (e2)->GET_COMPONENT(Transform);

	float xdist = (t1->Position.x - t2->Position.x) * (t1->Position.x - t2->Position.x);
	float ydist = (t1->Position.y - t2->Position.y) * (t1->Position.y - t2->Position.y);
	float widths = t1->scale.x * 1.25f + t2->scale.x * 1.25f;
	float heights = t1->scale.y * 1.25f + t2->scale.y* 1.25f;

	if (xdist <= widths * widths && ydist <= heights * heights)
		return 1;
	return 0;
}

bool PotatoPhysics(Poly& A, Math::Point2D& B)
{
	float MaxX = A.GetVert(0).x;
	float MinX = A.GetVert(0).x;
	float MaxY = A.GetVert(0).y;
	float MinY = A.GetVert(0).y;

	for (unsigned i = 1; i < A.GetVertNum(); ++i)
	{
		if (A.GetVert(i).x < MinX)
			MinX = A.GetVert(i).x;
		if (A.GetVert(i).x > MaxX)
			MaxX = A.GetVert(i).x;
		if (A.GetVert(i).y < MinY)
			MinY = A.GetVert(i).y;
		if (A.GetVert(i).y > MaxY)
			MaxY = A.GetVert(i).y;
	}

	if (B.x >= MinX && B.x <= MaxX)
		if (B.y >= MinY && B.y <= MaxY)
			return true;

	return false;
}

bool AABB_CollisionCheck(Entity *EA, Entity *EB)
{
	Poly A = EA->GET_COMPONENT(Transform)->transform * EA->GET_COMPONENT(Drawable)->mpMesh->Poly;
	Poly B = EB->GET_COMPONENT(Transform)->transform * EB->GET_COMPONENT(Drawable)->mpMesh->Poly;

	float MaxXA = A.GetVert(0).x;
	float MinXA = A.GetVert(0).x;
	float MaxYA = A.GetVert(0).y;
	float MinYA = A.GetVert(0).y;

	for (unsigned i = 1; i < A.GetVertNum(); ++i)
	{
		if (A.GetVert(i).x < MinXA)
			MinXA = A.GetVert(i).x;
		if (A.GetVert(i).x > MaxXA)
			MaxXA = A.GetVert(i).x;
		if (A.GetVert(i).y < MinYA)
			MinYA = A.GetVert(i).y;
		if (A.GetVert(i).y > MaxYA)
			MaxYA = A.GetVert(i).y;
	}

	float MaxXB = B.GetVert(0).x;
	float MinXB = B.GetVert(0).x;
	float MaxYB = B.GetVert(0).y;
	float MinYB = B.GetVert(0).y;

	for (unsigned i = 1; i < B.GetVertNum(); ++i)
	{
		if (B.GetVert(i).x < MinXB)
			MinXB = B.GetVert(i).x;
		if (B.GetVert(i).x > MaxXB)
			MaxXB = B.GetVert(i).x;
		if (B.GetVert(i).y < MinYB)
			MinYB = B.GetVert(i).y;
		if (B.GetVert(i).y > MaxYB)
			MaxYB = B.GetVert(i).y;
	}

	return (MaxXA >= MinXB) && (MaxXB >= MinXA) &&
		(MaxYB >= MinYA) && (MaxYA >= MinYB);
}

bool AABB_CollisionCheck(Entity *EA, const Square& B)
{
	Poly A = EA->GET_COMPONENT(Transform)->transform * EA->GET_COMPONENT(Drawable)->mpMesh->Poly;

	float MaxXA = A.GetVert(0).x;
	float MinXA = A.GetVert(0).x;
	float MaxYA = A.GetVert(0).y;
	float MinYA = A.GetVert(0).y;

	for (unsigned i = 1; i < A.GetVertNum(); ++i)
	{
		if (A.GetVert(i).x < MinXA)
			MinXA = A.GetVert(i).x;
		if (A.GetVert(i).x > MaxXA)
			MaxXA = A.GetVert(i).x;
		if (A.GetVert(i).y < MinYA)
			MinYA = A.GetVert(i).y;
		if (A.GetVert(i).y > MaxYA)
			MaxYA = A.GetVert(i).y;
	}

	float MaxXB = B.CENTER().X() + (B.SIDE()*0.5f);
	float MinXB = B.CENTER().X() - (B.SIDE()*0.5f);
	float MaxYB = B.CENTER().Y() + (B.SIDE()*0.5f);
	float MinYB = B.CENTER().Y() - (B.SIDE()*0.5f);

	return (MaxXA >= MinXB) && (MaxXB >= MinXA) &&
		(MaxYB >= MinYA) && (MaxYA >= MinYB);
}

bool AABB_CollisionCheck(const Square& B, Entity *EA)
{
	return AABB_CollisionCheck(EA, B);
}

bool AABB_CollisionCheck(const Square& A, const Square& B)
{
	float MaxXA = A.CENTER().X() + (A.SIDE()*0.5f);
	float MinXA = A.CENTER().X() - (A.SIDE()*0.5f);
	float MaxYA = A.CENTER().Y() + (A.SIDE()*0.5f);
	float MinYA = A.CENTER().Y() - (A.SIDE()*0.5f);

	float MaxXB = B.CENTER().X() + (B.SIDE()*0.5f);
	float MinXB = B.CENTER().X() - (B.SIDE()*0.5f);
	float MaxYB = B.CENTER().Y() + (B.SIDE()*0.5f);
	float MinYB = B.CENTER().Y() - (B.SIDE()*0.5f);

	return (MaxXA >= MinXB) && (MaxXB >= MinXA) &&
		(MaxYB >= MinYA) && (MaxYA >= MinYB);
}

bool PotatoPhysics(Math::Point2D& B, Poly& A)
{
	return PotatoPhysics(A, B);
}

bool CollisionCheck(Entity* EA, Entity* EB, Math::Vector2D& MTV)
{
	if (!PotatoPhysics(EA, EB))
		return false;


	Poly A = EA->GET_COMPONENT(Transform)->transform * EA->GET_COMPONENT(Drawable)->mpMesh->Poly;
	Poly B = EB->GET_COMPONENT(Transform)->transform * EB->GET_COMPONENT(Drawable)->mpMesh->Poly;

	if (ALLANHANDEDMETHISSTUFF)
	{
		Poly C(A.GetVertNum() - 1);
		for (unsigned i = 0; i<C.GetVertNum(); ++i)
		{
			C.SetVert(i) = A.GetVert(i);
		}

		A = C;

		Poly D(B.GetVertNum() - 1);
		for (unsigned i = 0; i<D.GetVertNum(); ++i)
		{
			D.SetVert(i) = B.GetVert(i);
		}

		B = D;
	}

#if DEBUG
	std::cout << A << B;
#endif

	unsigned i, j;
	unsigned TotalVerts = A.GetVertNum() + B.GetVertNum();
	Math::Vector2D* TestAxes = new Math::Vector2D[TotalVerts];
	unsigned TA = 0;

#if DEBUG
	{
		std::cout << A << B;
		std::cout << MTV;
	}
#endif

	// Get your edges from A
	for (i = 0; i<A.GetVertNum(); ++i, ++TA)
	{
		if (i == A.GetVertNum() - 1)
			TestAxes[TA] = A.GetVert(0) - A.GetVert(i);
		else
			TestAxes[TA] = A.GetVert(i + 1) - A.GetVert(i);
	}

	// Get your edges from B
	for (i = 0; i<B.GetVertNum(); ++i, ++TA)
	{
		if (i == B.GetVertNum() - 1)
			TestAxes[TA] = B.GetVert(0) - B.GetVert(i);
		else
			TestAxes[TA] = B.GetVert(i + 1) - B.GetVert(i);
	}

	// replace edges with normalized normals (Needed for Later)
	// gets LHS Normal (outside vector)
	for (i = 0; i<TotalVerts; ++i)
	{
		TestAxes[i] = Math::Vector2D(TestAxes[i].Y(), -TestAxes[i].X());

#if DEBUG
		std::cout << TestAxes[i] << "  ";
#endif

		TestAxes[i].Normalize();

#if DEBUG
		std::cout << TestAxes[i] << TestAxes[i].Length() << std::endl;
#endif
	}

	float mindotA, maxdotA, mindotB, maxdotB;
	// Math::Point2D minA, maxA, minB, maxB;
	float Over;
	float SmallestOver;

	for (i = 0, j = 0; i < TotalVerts; ++i, j = 0)
	{
		// Set your initial max and min values
		mindotA = Math::dot(TestAxes[i], A.GetVert(j));
		maxdotA = mindotA;

		mindotB = Math::dot(TestAxes[i], B.GetVert(j));
		maxdotB = mindotB;

		// Find max & min A verts
		for (j = 1; j<A.GetVertNum(); ++j)
		{
			float dot = Math::dot(TestAxes[i], A.GetVert(j));

			if (dot > maxdotA)
				maxdotA = dot;
			if (dot < mindotA)
				mindotA = dot;
		}

		// Find max & min B verts
		for (j = 1; j<B.GetVertNum(); ++j)
		{
			float dot = Math::dot(TestAxes[i], B.GetVert(j));

			if (dot > maxdotB)
				maxdotB = dot;
			if (dot < mindotB)
				mindotB = dot;
		}

		Over = Overlap(mindotA, maxdotA, mindotB, maxdotB);

		if (i == 0)
		{
			SmallestOver = Over;
			MTV = TestAxes[i];
		}

		if (Over >= 0 && Over < SmallestOver)
		{
			SmallestOver = Over;
			MTV = TestAxes[i];
		}

		if (Over<0)
		{
			MTV = Math::Vector2D(0, 0);
			delete[] TestAxes;
			return false;
		}
	}

	delete[] TestAxes;

#if DEBUG
	std::cout << A << B;
#endif

	MTV *= SmallestOver;

	return true;
}

bool CollisionCheck(Entity* EA, Entity* EB)
{
	//if(!PotatoPhysics(EA, EB))
	//	return false;

	//  if((EA->HasComponent(EC_Player) && !EA->GET_COMPONENT(Player)->alive) ||
	//     (EB->HasComponent(EC_Player) && !EB->GET_COMPONENT(Player)->alive) )
	//return false; Players are always alive now :) -- Luc

	Poly A = EA->GET_COMPONENT(Transform)->transform * EA->GET_COMPONENT(Drawable)->mpMesh->Poly;
	Poly B = EB->GET_COMPONENT(Transform)->transform * EB->GET_COMPONENT(Drawable)->mpMesh->Poly;

	if (ALLANHANDEDMETHISSTUFF)
	{
		Poly C(A.GetVertNum() - 1);
		for (unsigned i = 0; i<C.GetVertNum(); ++i)
		{
			C.SetVert(i) = A.GetVert(i);
		}

		A = C;

		Poly D(B.GetVertNum() - 1);
		for (unsigned i = 0; i<D.GetVertNum(); ++i)
		{
			D.SetVert(i) = B.GetVert(i);
		}

		B = D;
	}

#if DEBUG
	std::cout << A << B;
#endif

	unsigned i, j;
	unsigned TotalVerts = A.GetVertNum() + B.GetVertNum();
	Math::Vector2D* TestAxes = new Math::Vector2D[TotalVerts];
	unsigned TA = 0;

#if DEBUG
	std::cout << A << B;
#endif

	// Get your edges from A
	for (i = 0; i<A.GetVertNum(); ++i, ++TA)
	{
		if (i == A.GetVertNum() - 1)
			TestAxes[TA] = A.GetVert(0) - A.GetVert(i);
		else
			TestAxes[TA] = A.GetVert(i + 1) - A.GetVert(i);
	}

	// Get your edges from B
	for (i = 0; i<B.GetVertNum(); ++i, ++TA)
	{
		if (i == B.GetVertNum() - 1)
			TestAxes[TA] = B.GetVert(0) - B.GetVert(i);
		else
			TestAxes[TA] = B.GetVert(i + 1) - B.GetVert(i);
	}

	// replace edges with normalized normals (Needed for Later)
	// gets LHS Normal (outside vector)
	for (i = 0; i<TotalVerts; ++i)
	{
		TestAxes[i] = Math::Vector2D(TestAxes[i].Y(), -TestAxes[i].X());

#if DEBUG
		std::cout << TestAxes[i] << "  ";
#endif
		TestAxes[i].Normalize();

#if DEBUG
		std::cout << TestAxes[i] << TestAxes[i].Length() << std::endl;
#endif
	}

	float mindotA, maxdotA, mindotB, maxdotB;
	// Math::Point2D minA, maxA, minB, maxB;
	float Over;
	float SmallestOver;

	for (i = 0, j = 0; i < TotalVerts; ++i, j = 0)
	{
		// Set your initial max and min values
		mindotA = Math::dot(TestAxes[i], A.GetVert(j));
		maxdotA = mindotA;

		mindotB = Math::dot(TestAxes[i], B.GetVert(j));
		maxdotB = mindotB;

		// Find max & min A verts
		for (j = 1; j<A.GetVertNum(); ++j)
		{
			float dot = Math::dot(TestAxes[i], A.GetVert(j));

			if (dot > maxdotA)
				maxdotA = dot;
			if (dot < mindotA)
				mindotA = dot;
		}

		// Find max & min B verts
		for (j = 1; j<B.GetVertNum(); ++j)
		{
			float dot = Math::dot(TestAxes[i], B.GetVert(j));

			if (dot > maxdotB)
				maxdotB = dot;
			if (dot < mindotB)
				mindotB = dot;
		}

		Over = Overlap(mindotA, maxdotA, mindotB, maxdotB);

		if (i == 0)
		{
			SmallestOver = Over;
		}

		if (Over >= 0 && Over < SmallestOver)
		{
			SmallestOver = Over;
		}

		if (Over<0)
		{
			delete[] TestAxes;
			return false;
		}
	}

	delete[] TestAxes;

#if DEBUG
	std::cout << A << B;
#endif

	return true;
}


bool CollisionCheck(Poly& A, Poly& B, Math::Vector2D& MTV)
{

	unsigned i, j;
	unsigned TotalVerts = A.GetVertNum() + B.GetVertNum();
	Math::Vector2D* TestAxes = new Math::Vector2D[TotalVerts];
	unsigned TA = 0;
#if DEBUG
	{
		std::cout << A << B;
		std::cout << MTV;
	}
#endif

	// Get your edges from A
	for (i = 0; i<A.GetVertNum(); ++i, ++TA)
	{
		if (i == A.GetVertNum() - 1)
			TestAxes[TA] = A.GetVert(0) - A.GetVert(i);
		else
			TestAxes[TA] = A.GetVert(i + 1) - A.GetVert(i);
	}

	// Get your edges from B
	for (i = 0; i<B.GetVertNum(); ++i, ++TA)
	{
		if (i == B.GetVertNum() - 1)
			TestAxes[TA] = B.GetVert(0) - B.GetVert(i);
		else
			TestAxes[TA] = B.GetVert(i + 1) - B.GetVert(i);
	}

	// replace edges with normalized normals (Needed for Later)
	// gets LHS Normal (outside vector)
	for (i = 0; i<TotalVerts; ++i)
	{
		TestAxes[i] = Math::Vector2D(TestAxes[i].Y(), -TestAxes[i].X());
#if DEBUG
		std::cout << TestAxes[i] << "  ";
#endif
		TestAxes[i].Normalize();

#if DEBUG
		std::cout << TestAxes[i] << TestAxes[i].Length() << std::endl;
#endif
	}

	float mindotA, maxdotA, mindotB, maxdotB;
	// Math::Point2D minA, maxA, minB, maxB;
	float Over;
	float SmallestOver;

	for (i = 0, j = 0; i < TotalVerts; ++i, j = 0)
	{
		// Set your initial max and min values
		mindotA = Math::dot(TestAxes[i], A.GetVert(j));
		maxdotA = mindotA;

		mindotB = Math::dot(TestAxes[i], B.GetVert(j));
		maxdotB = mindotB;

		// Find max & min A verts
		for (j = 1; j<A.GetVertNum(); ++j)
		{
			float dot = Math::dot(TestAxes[i], A.GetVert(j));

			if (dot > maxdotA)
				maxdotA = dot;
			if (dot < mindotA)
				mindotA = dot;
		}

		// Find max & min B verts
		for (j = 1; j<B.GetVertNum(); ++j)
		{
			float dot = Math::dot(TestAxes[i], B.GetVert(j));

			if (dot > maxdotB)
				maxdotB = dot;
			if (dot < mindotB)
				mindotB = dot;
		}

		Over = Overlap(mindotA, maxdotA, mindotB, maxdotB);

		if (i == 0)
		{
			SmallestOver = Over;
			MTV = TestAxes[i];
		}

		if (Over >= 0 && Over < SmallestOver)
		{
			SmallestOver = Over;
			MTV = TestAxes[i];
		}

		if (Over<0)
		{
			MTV = Math::Vector2D(0, 0);
			return false;
		}
	}

	delete[] TestAxes;

	MTV *= SmallestOver;
	return true;
}


// Projects vector V onto U
Math::Vector2D Projection(Math::Vector2D& U, Math::Vector2D& V)
{
	return (dot(U, V) / U.SquareLength()) * U;
}

float Overlap(float Amin, float Amax, float Bmin, float Bmax)
{
	if (Amin <= Bmax && Amax >= Bmin)
		return Bmax - Amin;
	if (Amax >= Bmin && Amin <= Bmax)
		return Amax - Bmin;

	return -1;
}

Math::Point2D Poly::CENTER(void)
{
	Math::Point2D Center;

	for (unsigned i = 1; i<GetVertNum() - 1; ++i)
	{
		Center.x += GetVert(i).x;
		Center.y += GetVert(i).y;
	}

	Center *= (1.0f) / GetVertNum();

	return Center;
}

bool CollisionCheck(Entity* EA, Math::Point2D& P)
{
	//if((EA->HasComponent(EC_Player) && !EA->GET_COMPONENT(Player)->alive) )
	// return false;

	Poly A = EA->GET_COMPONENT(Transform)->transform * EA->GET_COMPONENT(Drawable)->mpMesh->Poly;

	return CollisionCheck(A, P);
}

bool CollisionCheck(Math::Point2D& P, Entity* EA)
{
	return CollisionCheck(EA, P);
}

bool CollisionCheck(Poly& A, Math::Point2D& P)
{
	std::vector<Math::Hcoords> planes;

	for (unsigned i = 0; i < A.GetVertNum(); ++i)
	{
		int in = i - 1;
		if (in<0)
			in = A.GetVertNum() - 1;

		if (i == A.GetVertNum() - 1)
			planes.push_back(HalfPlane(A.GetVert(i), A.GetVert(0), A.GetVert(in)));
		else
			planes.push_back(HalfPlane(A.GetVert(i), A.GetVert(i + 1), A.GetVert(in)));
	}

	for (auto it : planes)
	{
		if (dot(it, P) > 0)
			return false;
	}

	return true;
}

bool CollisionCheck(Math::Point2D& P, Poly& A)
{
	return CollisionCheck(A, P);
}
bool CollisionCheck(float x, float y, Poly& A)
{
	return CollisionCheck(A, Math::Point2D(x, y));
}

bool CollisionCheck(Poly& A, float x, float y)
{
	return CollisionCheck(A, Math::Point2D(x, y));
}

bool CollisionCheck(Entity*, Math::Point2D&, Math::Vector2D&, Math::Point2D&);
bool CollisionCheck(Math::Point2D&, Entity*, Math::Vector2D&, Math::Point2D&);
bool CollisionCheck(Poly&, Math::Point2D&, Math::Vector2D&, Math::Point2D&);
bool CollisionCheck(Math::Point2D&, Poly&, Math::Vector2D&, Math::Point2D&);
bool CollisionCheck(float, float, Poly&, Math::Vector2D&, Math::Point2D&);
bool CollisionCheck(Poly&, float, float, Math::Vector2D&, Math::Point2D&);

bool CollisionCheck(Poly&, Circle&);
bool CollisionCheck(Circle&, Poly&);
bool CollisionCheck(Entity*, Circle&);
bool CollisionCheck(Circle&, Entity*);

bool CollisionCheck(Poly&, Math::Point2D&, float);
bool CollisionCheck(Math::Point2D&, float, Poly&);
bool CollisionCheck(Entity*, Math::Point2D&, float);
bool CollisionCheck(Math::Point2D&, float, Entity*);

Entity* SomethingPhysicsy(Math::Point2D& Cursor)
{
	std::vector<Entity*> Clickables = ENGINE->OM->FilterEntities(MC_Transform | MC_Drawable);
	FilterForMesh(Clickables);

	std::vector<Entity*> EP;

	for (auto _entity : Clickables)
	{

		Poly A = _entity->GET_COMPONENT(Transform)->transform * _entity->GET_COMPONENT(Drawable)->mpMesh->Poly;

		if (PotatoPhysics(A, Cursor))
			EP.push_back(_entity);
	}

	if (EP.empty())
		return NULL;

	unsigned index = 0;
	unsigned lowestlayer = EP[0]->GET_COMPONENT(Drawable)->layer;

	for (unsigned i = 1; i < EP.size(); ++i)
	{
		unsigned layer = EP[i]->GET_COMPONENT(Drawable)->layer;
		if (layer > lowestlayer)
			index = i;
	}

	return EP[index];
}

Entity* AABB_PointCollision(Math::Point2D& Cursor)
{
	std::vector<Entity*> Clickables = ENGINE->OM->FilterEntities(MC_Transform | MC_Drawable | MC_Drawable);
	FilterForMesh(Clickables);

	std::vector<Entity*> EP;

	for (auto _entity : Clickables)
	{
		if (!_entity->GET_COMPONENT(Drawable)->mpMesh)
			continue;

		Poly A = _entity->GET_COMPONENT(Transform)->transform * _entity->GET_COMPONENT(Drawable)->mpMesh->Poly;

		if (PotatoPhysics(A, Cursor))
			EP.push_back(_entity);
	}

	if (EP.empty())
		return NULL;

	unsigned index = 0;
	unsigned lowestlayer = EP[0]->GET_COMPONENT(Drawable)->layer;

	for (unsigned i = 1; i < EP.size(); ++i)
	{
		unsigned layer = EP[i]->GET_COMPONENT(Drawable)->layer;
		if (layer > lowestlayer)
			index = i;
	}

	return EP[index];
}

bool AABB_CollisionCheck(Entity* EA, const Rect& B)
{
	Poly A = EA->GET_COMPONENT(Transform)->transform * EA->GET_COMPONENT(Drawable)->mpMesh->Poly;

	float MaxXA = A.GetVert(0).x;
	float MinXA = A.GetVert(0).x;
	float MaxYA = A.GetVert(0).y;
	float MinYA = A.GetVert(0).y;

	for (unsigned i = 1; i < A.GetVertNum(); ++i)
	{
		if (A.GetVert(i).x < MinXA)
			MinXA = A.GetVert(i).x;
		if (A.GetVert(i).x > MaxXA)
			MaxXA = A.GetVert(i).x;
		if (A.GetVert(i).y < MinYA)
			MinYA = A.GetVert(i).y;
		if (A.GetVert(i).y > MaxYA)
			MaxYA = A.GetVert(i).y;
	}

	float MaxXB = B.CENTER().X() + (B.WIDTH()*0.5f);
	float MinXB = B.CENTER().X() - (B.WIDTH()*0.5f);
	float MaxYB = B.CENTER().Y() + (B.HEIGHT()*0.5f);
	float MinYB = B.CENTER().Y() - (B.HEIGHT()*0.5f);

	return (MaxXA >= MinXB) && (MaxXB >= MinXA) &&
		(MaxYB >= MinYA) && (MaxYA >= MinYB);
}
bool AABB_CollisionCheck(const Rect& B, Entity* EA)
{
	return AABB_CollisionCheck(EA, B);
}
bool AABB_CollisionCheck(const Rect& A, const Rect& B)
{
	float MaxXA = A.CENTER().X() + (A.WIDTH()*0.5f);
	float MinXA = A.CENTER().X() - (A.WIDTH()*0.5f);
	float MaxYA = A.CENTER().Y() + (A.HEIGHT()*0.5f);
	float MinYA = A.CENTER().Y() - (A.HEIGHT()*0.5f);

	float MaxXB = B.CENTER().X() + (B.WIDTH()*0.5f);
	float MinXB = B.CENTER().X() - (B.WIDTH()*0.5f);
	float MaxYB = B.CENTER().Y() + (B.HEIGHT()*0.5f);
	float MinYB = B.CENTER().Y() - (B.HEIGHT()*0.5f);

	return (MaxXA >= MinXB) && (MaxXB >= MinXA) &&
		(MaxYB >= MinYA) && (MaxYA >= MinYB);
}

bool AABB_CollisionCheck(const Rect& A, const Math::Point2D& B)
{
	float MaxXA = A.CENTER().X() + (A.WIDTH()*0.5f);
	float MinXA = A.CENTER().X() - (A.WIDTH()*0.5f);
	float MaxYA = A.CENTER().Y() + (A.HEIGHT()*0.5f);
	float MinYA = A.CENTER().Y() - (A.HEIGHT()*0.5f);

	return (MaxXA >= B.x) && (MinXA <= B.x) &&
		(MaxYA >= B.y) && (MinYA <= B.y);
}

bool AABB_CollisionCheck(const Math::Point2D& B, const Rect& A)
{
	return AABB_CollisionCheck(A, B);
}