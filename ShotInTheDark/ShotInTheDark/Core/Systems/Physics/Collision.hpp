//	File name:		Collision.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Dereck Crouse
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "Shape.hpp"
#include"../../Components/Components.h"

// Use this function to check for collisions from outside the physics system, NOTHING ELSE!
bool CollisionFound(Entity *, Entity *);

// After resolving the collision, call this function to set it to resolved.
void CollisionResolved(Entity *, Entity *);



bool PotatoPhysics(Entity*, Entity*);
bool PotatoPhysics(Poly&, Math::Point2D&);
bool PotatoPhysics(Math::Point2D&, Poly&);

bool CollisionCheck(Entity*, Entity*, Math::Vector2D&);
bool CollisionCheck(Poly&, Poly&, Math::Vector2D&);
bool CollisionCheck(Entity*, Entity*);

bool CollisionCheck(Entity*, Math::Point2D&);
bool CollisionCheck(Math::Point2D&, Entity*);
bool CollisionCheck(Poly&, Math::Point2D&);
bool CollisionCheck(Math::Point2D&, Poly&);
bool CollisionCheck(float, float, Poly&);
bool CollisionCheck(Poly&, float, float);

bool CollisionCheck(Entity*, Math::Point2D&, Math::Vector2D&, Math::Point2D&);
bool CollisionCheck(Math::Point2D&, Entity*, Math::Vector2D&, Math::Point2D&);
bool CollisionCheck(Poly&, Math::Point2D&, Math::Vector2D&, Math::Point2D&);
bool CollisionCheck(Math::Point2D&, Poly&, Math::Vector2D&, Math::Point2D&);
bool CollisionCheck(float, float, Poly&, Math::Vector2D&, Math::Point2D&);
bool CollisionCheck(Poly&, float, float, Math::Vector2D&, Math::Point2D&);

bool CollisionCheck(Poly&, Circle&);
bool CollisionCheck(Circle&, Poly&);
bool CollisionCheck(Entity*, Circle&);
bool CollisionCheck(Circle&, Entity*);

bool CollisionCheck(Poly&, Math::Point2D&, float);
bool CollisionCheck(Math::Point2D&, float, Poly&);
bool CollisionCheck(Entity*, Math::Point2D&, float);
bool CollisionCheck(Math::Point2D&, float, Entity*);

Math::Vector2D Projection(Math::Vector2D&, Math::Vector2D&);
float Overlap(float, float, float, float);

Entity* SomethingPhysicsy(Math::Point2D&);
Entity* AABB_PointCollision(Math::Point2D&);

bool AABB_CollisionCheck(Entity*, Entity*);
bool AABB_CollisionCheck(Entity*, const Square&);
bool AABB_CollisionCheck(const Square&, Entity*);
bool AABB_CollisionCheck(const Square&, const Square&);

bool AABB_CollisionCheck(Entity*, const Rect&);
bool AABB_CollisionCheck(const Rect&, Entity*);
bool AABB_CollisionCheck(const Rect&, const Rect&);
bool AABB_CollisionCheck(const Rect&, const Math::Point2D&);
bool AABB_CollisionCheck(const Math::Point2D&, const Rect&);