#include "Collision.hpp"
#include "Vector.hpp"

bool CollisionCheck(const Circle& C, const Coordinate& P)
{
	Coordinate distance = C.CENTER() - P;
	return (distance.length2() <= (C.RADIUS()*C.RADIUS()));
}

bool CollisionCheck(const Circle& C1, const Circle& C2)
{
	Coordinate distance = C1.CENTER() - C2.CENTER;
	return (distance.length2() <= ((C1.RADIUS()*C1.RADIUS()) + (C2.RADIUS()*C2.RADIUS())));
}

bool CollisionCheck(const Circle& C, const Rectangle& R)
{
	Coordinate ClosestPoint;

	if (C.CENTER().X() > R.NECorner().X())
		ClosestPoint[0] = R.NECorner().X();
	else if (C.CENTER().X() < R.SWCorner().X())
		ClosestPoint[0] = R.SWCorner().X();
	else
		ClosestPoint[0] = C.CENTER().X();

	if (C.CENTER().Y() > R.NECorner().Y())
		ClosestPoint[1] = R.NECorner().Y();
	else if (C.CENTER().Y() < R.SWCorner().Y())
		ClosestPoint[1] = R.SWCorner().Y();
	else
		ClosestPoint[1] = C.CENTER().Y();

	return CollisionCheck(C, ClosestPoint);
}
bool CollisionCheck(const Circle& C, const Triangle& T)
{
	LineSegment LS[3] = { LineSegment(T.PointA(), T.PointB()),
						  LineSegment(T.PointB(), T.PointC()),
						  LineSegment(T.PointC(), T.PointA()) };

	Coordinate CheckPoint[7] = {  Foot(C.CENTER(), LS[0]), Foot(C.CENTER(), LS[1]), 
		Foot(C.CENTER(), LS[2]), T.CENTER(), T.PointA(), T.PointB(), T.PointC() };

	for (int i = 0; i < 7; ++i)
	{
		if (i < 3)
		{
			if (PointOnLine(CheckPoint[i], LS[i]))
			{
				if (CollisionCheck(C, CheckPoint[i]))
					return true;
			}
		}
		else
		{
			if (CollisionCheck(C, CheckPoint[i]))
				return true;
		}
	}

	return false;
}

bool CollisionCheck(const Triangle& T, const Coordinate& P)
{
	float AreaCheck = Triangle(P, T.PointB(), T.PointC()).Area()
					+ Triangle(P, T.PointA(), T.PointC()).Area()
					+ Triangle(P, T.PointA(), T.PointB()).Area();

	return (T.Area() == AreaCheck);
}

bool CollisionCheck(const Triangle& T, const Circle& C)
{
	return CollisionCheck(C, T);
}

bool CollisionCheck(const Rectangle& R, const Coordinate& P)
{
	return !(P.X() >= R.NWCorner().X() ||
			 P.X() <= R.SECorner().X() ||
			 P.Y() >= R.NWCorner().Y() ||
			 P.Y() <= R.SECorner().Y() );
}

bool CollisionCheck(const Rectangle& R, const Circle& C)
{
	return CollisionCheck(C, R);
}

bool CollisionCheck(const Rectangle& R1, const Rectangle& R2)
{
	return (R1.SECorner().X() <= R2.NWCorner().X() &&
			R1.NWCorner().X() >= R2.SECorner().X() &&
			R1.SECorner().Y() <= R2.NWCorner().Y() &&
			R1.NWCorner().Y() >= R2.SECorner().Y());
}

bool CollisionCheck(const Coordinate& P, const Circle& C)
{
	return CollisionCheck(C, P);
}
bool CollisionCheck(const Coordinate& P, const Rectangle& R)
{
	return CollisionCheck(P, R);
}
bool CollisionCheck(const Coordinate& P, const Triangle& T)
{
	return CollisionCheck(P, T);
}