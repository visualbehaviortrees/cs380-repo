#ifndef COLLISION_HPP
#define COLLISION_HPP

#include "Shapes.hpp"

bool CollisionCheck(Circle&, Coordinate&);
bool CollisionCheck(Circle&, Circle&);
bool CollisionCheck(Circle&, Rectangle&);
bool CollisionCheck(Circle&, Triangle&);

bool CollisionCheck(Triangle&, Coordinate&);
bool CollisionCheck(Triangle&, Circle&);
bool CollisionCheck(Triangle&, Rectangle&);
bool CollisionCheck(Triangle&, Triangle&);

bool CollisionCheck(Rectangle&, Coordinate&);
bool CollisionCheck(Rectangle&, Circle&);
bool CollisionCheck(Rectangle&, Rectangle&);
bool CollisionCheck(Rectangle&, Triangle&);

bool CollisionCheck(Coordinate&, Circle&);
bool CollisionCheck(Coordinate&, Rectangle&);
bool CollisionCheck(Coordinate&, Triangle&);

#endif