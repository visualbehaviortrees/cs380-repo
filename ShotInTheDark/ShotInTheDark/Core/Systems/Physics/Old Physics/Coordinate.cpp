#include "Coordinate.hpp"

using namespace std;

// Name:		(Homogeneous) Coordinate constructor
// Description:	Creates an instance of the Coordinate class
// Input:		const Coordinate& P
//					P: Coordinate to Copy
// Output:
Coordinate::Coordinate(const Coordinate& P) : x(P.x), y(P.y), type(P.type) {}

// Name:		(Homogeneous) Coordinate constructor
// Description:	Creates an instance of the Coordinate class
// Input:		float X, float Y, unsigned TYPE
//					X:  X value to store
//					Y:  Y Value to store
//					TYPE:	0 for vector, 1 for point
// Output:
Coordinate::Coordinate(float X, float Y, unsigned TYPE) : x(X), y(Y), type(TYPE) 
{
	if (type > 1)
		type = 1;
}


// Name:		X
// Description:	gets vectors private x value
// Input:		void
// Output:		float
//					the value of x
float Coordinate::X(void) const
{
	return x;
}

// Name:		Y
// Description:	gets vectors private y value
// Input:		void
// Output:		float
//					the value of y
float Coordinate::Y(void) const
{
	return y;
}

// Name:		operator[]
// Description:	gets either the X or Y coordinate
// Input:		int i
//					i = 0 for x, i = 1 for y
// Output:		float
//					the value requested
float Coordinate::operator[](int i) const
{
	if (i == 0)
		return x;
	else
		return y;
}

// Name:		operator[]
// Description:	gets/sets either the X or Y coordinate
// Input:		int i
//					i = 0 for x, i = 1 for y
// Output:		float&
//					the value requested
float& Coordinate::operator[](int i)
{
	if (i == 0)
		return *(&x);
	else
		return *(&y);
}

// Name:		operator=
// Description:	assignment operator overload for Coordinate Class
// Input:		const Coordinate& C
//					C = coordinate to copy
// Output:		Coordinate&
//					Reference to the changed Coordinate
Coordinate& Coordinate::operator=(const Coordinate& C)
{
	x = C.x;
	y = C.y;
	type = C.type;
	return *this;
}

// Name:		operator+=
// Description:	Sums a vector to another
// Input:		const Coordinate& C
//					C = coordinate to add to current instance
// Output:		Coordinate&
//					Reference to the changed Coordinate
Coordinate& Coordinate::operator+=(const Coordinate& C)
{
	if (C.type == 1 && type == 1)
	{
		cout << "Cannot Add 2 points." << endl;
	}
	else
	{
		x += C.x;
		y += C.y;
		type += C.type;
	}
	return *this;
}

// Name:		operator-=
// Description:	Subtracts a Coordinate from another
// Input:		const Coordinate& C
//					C = coordinate to subtract with current instance
// Output:		Coordinate&
//					Reference to the changed Coordinate
Coordinate& Coordinate::operator-=(const Coordinate& C)
{
	x -= C.x;
	y -= C.y;
	if (type == 1 && C.type == 0)
		type = 1;
	else
		type = 0;
	return *this;
}

// Name:		operator*=
// Description:	Scales a Coordinate with a float
// Input:		float S
//					S = scalar to multiply Coordinate with
// Output:		Coordinate&
//					Reference to the changed Coordinate
Coordinate& Coordinate::operator*=(float S)
{
	x *= S;
	y *= S;
	return *this;
}

// Name:		operator+
// Description:	Adds two Coordinates
// Input:		const Coordinate& u, const Coordinate& v
//					u = Coordinate to Add
//					v = Coordinate to Add
// Output:		Coordinate
//					Sum of the 2 coordinates
Coordinate operator+(const Coordinate& u, const Coordinate& v)
{
	unsigned vs;

	vs = u.type + v.type;

	if (vs > 1)
	{
		cout << "Cannot Add 2 Points" << endl;
		return Coordinate(0, 0, 0);
	}

	return Coordinate(u.X() + v.X(), u.Y() + v.Y(), vs);
}

// Name:		operator-
// Description:	Subtracts two Coordinates
// Input:		const Coordinate& u, const Coordinate& v
//					u = Coordinate to Subtract from
//					v = Coordinate to Subtract
// Output:		Coordinate
//					Difference of the 2 coordinates
Coordinate operator-(const Coordinate& u, const Coordinate& v)
{
	unsigned vs;

	if (u.type == 1 && v.type == 0)
		vs = 1;
	else
		vs = 0;

	return Coordinate(u.X() - v.X(), u.Y() - v.Y(), vs);
}

// Name:		operator- (unary)
// Description:	Negates a Coordinate
// Input:		const Coordinate& u
//					u = Coordinate to negate
// Output:		Coordinate
//					negative values of the given coordinate
Coordinate operator-(const Coordinate& u)
{
	return Coordinate(-u.X(), -u.Y(), u.type);
}

// Name:		operator*
// Description:	Scales a Coordinate
// Input:		float r, const Coordinate& u
//					r = scalar value
//					u = Coordinate to scale
// Output:		Coordinate
//					scaled Coordinate
Coordinate operator*(float r, const Coordinate& u)
{
	Coordinate Scale = u;
	Scale *= r;

	return Scale;
}

// Name:		operator*
// Description:	Scales a Coordinate
// Input:		const Coordinate& u, float r
//					u = Coordinate to scale
//					r = scalar value
// Output:		Coordinate
//					scaled Coordinate
Coordinate operator*(const Coordinate& u, float r)
{
	Coordinate Scale = u;
	Scale *= r;

	return Scale;
}

// Name:		operator*
// Description:	Dot Product of 2 Coordinates
// Input:		const Coordinate& u, const Coordinate& v
//					u = Coordinate for dot product
//					v = Coordinate for dot product
// Output:		float
//					dot product of the 2 Coordinates
float operator*(const Coordinate& u, const Coordinate& v)
{
	return (u.X() * v.X()) + (u.Y() * v.Y());
}

// Name:		operator==
// Description:	checks equivalency of 2 coordinates
// Input:		const Coordinate& u, const Coordinate& v
//					u = Coordinate to compare against
//					v = Coordinate to compare
// Output:		bool
//					returns true if same, else false
bool operator==(const Coordinate& u, const Coordinate& v)
{
	return (u.X() == v.X() && u.Y() == v.Y() && u.type == v.type);
}

// Name:		length
// Description:	returns length of A Coordinate
// Input:		void
// Output:		float
//					returns the length
float Coordinate::length(void)
{
	return sqrt( ( x*x ) + ( y*y ) );
}

// Name:		length2
// Description:	returns squared length of A Coordinate
// Input:		void
// Output:		float
//					returns the length squared
float Coordinate::length2(void)
{
	return ((x*x) + (y*y));
}

// Name:		Normalize
// Description:	Normalizes a Vector
// Input:		const Coordinate& v
// Output:		Coordinate
//					The Normalized Coordinate
Coordinate Normalize(Coordinate V)
{
	float magnitude = V.length();
	return Coordinate(V.X() / magnitude, V.Y() / magnitude, 0);
}

// Name:		operator<<
// Description:	prints a coordinate using either vector or point notation
// Input:		std::ostream& stream, const Coordinate& u
// Output:		std::ostream&
//					the stream being outputted to
std::ostream& operator<<(std::ostream& stream, const Coordinate& u)
{
	if (u.type == 0)
		stream << '<' << u.X() << ',' << u.Y() << '>';
	else
		stream << '(' << u.X() << ',' << u.Y() << ')';

	return stream;
}