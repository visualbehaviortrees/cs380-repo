#ifndef VECTOR_HPP
#define VECTOR_HPP

#include <iostream>
#include <cmath>

class Coordinate
{
public:
	Coordinate(const Coordinate&);
	Coordinate(float = 0, float = 0, unsigned = 0);
	float X(void) const;
	float Y(void) const;
	float operator[](int i) const;
	float& operator[](int i);
	Coordinate& operator=(const Coordinate&);
	Coordinate& operator+=(const Coordinate&);
	Coordinate& operator-=(const Coordinate&);
	Coordinate& operator*=(float);
	float length(void);
	float length2(void);

	unsigned type;
private:
	float x, y;
};

Coordinate operator+(const Coordinate& u, const Coordinate& v);
Coordinate operator-(const Coordinate& u, const Coordinate& v);
Coordinate operator-(const Coordinate& u);
Coordinate operator*(float r, const Coordinate& u);
Coordinate operator*(const Coordinate& u, float r);
float operator*(const Coordinate& u, const Coordinate& v);
bool operator==(const Coordinate& u, const Coordinate& v);
Coordinate Normalize(Coordinate);
std::ostream& operator<<(std::ostream& s, const Coordinate& u);

#endif