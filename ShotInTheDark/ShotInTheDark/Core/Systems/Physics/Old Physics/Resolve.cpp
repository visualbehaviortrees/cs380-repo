#include "Resolve.hpp"
#include "Collision.hpp"

void Resolve(Circle& a, Circle& b)
{
	Circle a1(a);
	Circle b1(b);
	a1.SetCenter(a.CENTER().X() + a.VELOCITY().X(), a.CENTER().Y() + a.VELOCITY().Y());
	b1.SetCenter(b.CENTER().X() + b.VELOCITY().X(), b.CENTER().Y() + b.VELOCITY().Y());

	if (CollisionCheck(a1, b1))
	{
		a.SetVelocity(-a.VELOCITY());
		b.SetVelocity(-b.VELOCITY());
	}
}

void Resolve(Circle& a, Rectangle& b)
{
	Circle a1(a);
	Rectangle b1(b);
	a1.SetCenter(a.CENTER().X() + a.VELOCITY().X(), a.CENTER().Y() + a.VELOCITY().Y());
	b1.SetCenter(b.CENTER().X() + b.VELOCITY().X(), b.CENTER().Y() + b.VELOCITY().Y());

	if (CollisionCheck(a1, b1))
	{
		a.SetVelocity(-a.VELOCITY());
		b.SetVelocity(-b.VELOCITY());
	}
}

void Resolve(Circle& a, Triangle& b)
{
	Circle a1(a);
	Triangle b1(b);

	a1.SetCenter(a.CENTER() + a.VELOCITY());

	b1.PointASet(b.PointA() + b.VELOCITY());
	b1.PointBSet(b.PointB() + b.VELOCITY());
	b1.PointCSet(b.PointC() + b.VELOCITY());

	if (CollisionCheck(a1, b1))
	{
		a.SetVelocity(-a.VELOCITY());
		b.SetVelocity(-b.VELOCITY());
	}
}

void Resolve(Triangle& a, Circle& b)
{
	Resolve(b, a);
}

void Resolve(Triangle& a, Rectangle& b)
{
	Triangle a1(a);
	Rectangle b1(b);

	a1.PointASet(a.PointA() + a.VELOCITY());
	a1.PointBSet(a.PointB() + a.VELOCITY());
	a1.PointCSet(a.PointC() + a.VELOCITY());

	b1.SetCenter(b.CENTER() + b.VELOCITY());

	if (CollisionCheck(a1, b1))
	{
		a.SetVelocity(-a.VELOCITY());
		b.SetVelocity(-b.VELOCITY());
	}
}

void Resolve(Triangle& a, Triangle& b)
{
	Triangle a1(a);
	Triangle b1(b);

	a1.PointASet(a.PointA() + a.VELOCITY());
	a1.PointBSet(a.PointB() + a.VELOCITY());
	a1.PointCSet(a.PointC() + a.VELOCITY());

	b1.PointASet(b.PointA() + b.VELOCITY());
	b1.PointBSet(b.PointB() + b.VELOCITY());
	b1.PointCSet(b.PointC() + b.VELOCITY());

	if (CollisionCheck(a1, b1))
	{
		a.SetVelocity(-a.VELOCITY());
		b.SetVelocity(-b.VELOCITY());
	}
}

void Resolve(Rectangle& a, Circle& b)
{
	Resolve(b, a);
}

void Resolve(Rectangle& a, Rectangle& b)
{
	Rectangle a1(a);
	Rectangle b1(b);
	a1.SetCenter(a.CENTER().X() + a.VELOCITY().X(), a.CENTER().Y() + a.VELOCITY().Y());
	b1.SetCenter(b.CENTER().X() + b.VELOCITY().X(), b.CENTER().Y() + b.VELOCITY().Y());

	if (CollisionCheck(a1, b1))
	{
		a.SetVelocity(-a.VELOCITY());
		b.SetVelocity(-b.VELOCITY());
	}
}

void Resolve(Rectangle& a, Triangle& b)
{
	Resolve(b, a);
}

void UpdatePosition(Circle& a)
{
	a.SetCenter(a.CENTER() + a.VELOCITY());
}

void UpdatePosition(Triangle& a)
{
	a.PointASet(a.PointA() + a.VELOCITY());
	a.PointBSet(a.PointB() + a.VELOCITY());
	a.PointCSet(a.PointC() + a.VELOCITY());
}

void UpdatePosition(Rectangle& a)
{
	a.SetCenter(a.CENTER() + a.VELOCITY());
}