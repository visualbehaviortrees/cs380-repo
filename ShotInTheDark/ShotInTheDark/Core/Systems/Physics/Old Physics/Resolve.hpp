#ifndef RESOLVE_HPP
#define RESOLVE_HPP

#include "Shapes.hpp"

void Resolve(Circle& a, Circle& b)
void Resolve(Circle&, Rectangle&);
void Resolve(Circle&, Triangle&);

void Resolve(Triangle&, Circle&);
void Resolve(Triangle&, Rectangle&);
void Resolve(Triangle&, Triangle&);

void Resolve(Rectangle&, Circle&);
void Resolve(Rectangle&, Rectangle&);
void Resolve(Rectangle&, Triangle&);

#endif