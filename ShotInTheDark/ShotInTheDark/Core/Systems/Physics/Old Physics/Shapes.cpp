#include "Shapes.hpp"
#include <iostream>

// Needed for Circle Logic
static float PI = 3.14159265358979f;

void PhysicsBody::SetVelocity(float x, float y)
{
	velocity = Coordinate(x, y, 0);
}

void PhysicsBody::SetVelocity(Coordinate& vel)
{
	velocity = vel;
}

//--------------------------------------------------------------

// Name: Circle Constructor
// Description: Creates a Circle using an X and Y value
// Input: float X, float Y, float RAD
//			X: X value of Center point
//			Y: Y value of the Center point
//			RAD: Radius of the created circle
// Output: void
Circle::Circle(float X, float Y, float RAD) : Radius(RAD)
{
	Center[0] = X;
	Center[1] = Y;
	Center.type = 1;

	if (Radius < 0)
	{
		std::cout << "Radius being set to a negative" << std::endl;
		Radius *= -1;
	}
}

// Name: Circle Constructor
// Description: Creates a Circle using a Coordinate
// Input: Coordinate P, float RAD
//			P: Coordinate of the Center point
//			RAD: Radius of the Created Circle
// Output: void
Circle::Circle(Coordinate P, float RAD) : Radius(RAD)
{
	if (P.type == 0)
		std::cout << "Circle created using a Vector" << std::endl;
	Center = P;
	Center.type = 1;

	if (Radius < 0)
	{
		std::cout << "Radius being set to a negative" << std::endl;
		Radius *= -1;
	}
}

// Name: CENTER (Circle)
// Description: Returns the Centerpoint of the Circle
// Input: void
// Output: Coordinate
//			Coordinate point of the Circle's Center
Coordinate Circle::CENTER(void) const
{
	return Center;
}

// Name: RADIUS (Circle)
// Description: Returns the Radius of the Circle
// Input: void
// Output: float
//			returns the radius as a float
float Circle::RADIUS(void) const
{
	return Radius;
}

// Name: SetCenter (Circle)
// Description: Sets the Centerpoint of the Circle
// Input: float X, float Y
//			X: x value of Center
//			Y: y value of Center
// Output: void
void Circle::SetCenter(float X, float Y)
{
	Center[0] = X;
	Center[1] = Y;
	Center.type = 1;
}

// Name: SetCenter (Circle)
// Description: Sets the Centerpoint of the Circle
// Input: Coordinate P
//			P: point to set as center
// Output: void
void Circle::SetCenter(Coordinate P)
{
	if (P.type == 0)
		std::cout << "Circle created using a Vector" << std::endl;
	Center = P;
	Center.type = 1;
}

// Name: SetRadius (Circle)
// Description: Sets the Radius of the Circle
// Input: float RAD
//			RAD: point to set as radius
// Output: void
void Circle::SetRadius(float RAD)
{
	if (RAD < 0)
	{
		std::cout << "Radius being set to a negative" << std::endl;
		RAD *= -1;
	}

	Radius = RAD;
}

// Name: Area (Circle)
// Description: Gets the Area of the Circle
// Input: void
// Output: float
//			Returns the area as a float
float Circle::Area(void)
{
	return (PI*Radius*Radius);
}

//--------------------------------------------------------------

// Name: Rectangle Constructor
// Description: Creates a Rectangle using an X and Y value
// Input: float X, float Y, float LEN, float HEIGHT
//			X: X value of Center point
//			Y: Y value of the Center point
//			LEN: Length of the Rectangle
//			HEIGHT: Height of the Rectangle
// Output: void
Rectangle::Rectangle(float X, float Y, float LEN, float HEIGHT) : Length(LEN), Height(HEIGHT)
{
	if (HEIGHT < 0)
	{
		Height *= -1;
		std::cout << "Height was set to a negative" << std::endl;
	}

	if (LEN < 0)
	{
		Length *= -1;
		std::cout << "Length was set to a negative" << std::endl;
	}

	Center = Coordinate(X, Y, 1);
}

// Name: Rectangle Constructor
// Description: Creates a Rectangle using an X and Y value
// Input: Coordinate P, float LEN, float HEIGHT
//			P: Coordinate for the Rectangle's center
//			LEN: Length of the Rectangle
//			HEIGHT: Height of the Rectangle
// Output: void
Rectangle::Rectangle(Coordinate P, float LEN, float HEIGHT) : Length(LEN), Height(HEIGHT)
{
	if (P.type == 0)
		std::cout << "Rectangle created using a Vector" << std::endl;

	if (HEIGHT < 0)
	{
		Height *= -1;
		std::cout << "Height was set to a negative" << std::endl;
	}

	if (LEN < 0)
	{
		Length *= -1;
		std::cout << "Length was set to a negative" << std::endl;
	}

	Center = Coordinate(P.X(), P.Y(), 1);
}

// Name: CENTER (Rectangle)
// Description: A---B
//				|   |
//				D---C
// Input: Coordinate A, Coordinate B, Coordinate C, Coordinate D
//			A: NW Corner
//			B: NE Corner
//			C: SE Corner
//			D: SW Corner
// Output: Coordinate
//			returns the Centerpoint
Rectangle::Rectangle(Coordinate A, Coordinate B, Coordinate C, Coordinate D)
{
	Length = (B - A).length();
	Height = (D - A).length();
	Center = (A + B + C + D) * 0.25f;
}

// Name: CENTER (Rectangle)
// Description: Returns the Centerpoint of the Rectangle
// Input: void
// Output: Coordinate
//			returns the Centerpoint
Coordinate Rectangle::CENTER(void) const
{
	return Center;
}

// Name: LENGTH (Rectangle)
// Description: Returns the Length of the Rectangle
// Input: void
// Output: float
//			returns the Length
float Rectangle::LENGTH(void) const
{
	return Length;
}

// Name: HEIGHT (Rectangle)
// Description: Returns the Height of the Rectangle
// Input: void
// Output: float
//			returns the Height
float Rectangle::HEIGHT(void) const
{
	return Height;
}

// Name: NECorner (Rectangle)
// Description: Returns the Coordinate of the NE Corner of the Rectangle
// Input: void
// Output: Coordinate
//			NE Corner
Coordinate Rectangle::NECorner(void) const
{
	return Coordinate((Center.X() - (Length / 2.0f)), (Center.Y() + (Height / 2.0f)), 1);
}

// Name: NWCorner (Rectangle)
// Description: Returns the Coordinate of the NW Corner of the Rectangle
// Input: void
// Output: Coordinate
//			NW Corner
Coordinate Rectangle::NWCorner(void) const
{
	return Coordinate((Center.X() + (Length / 2.0f)), (Center.Y() + (Height / 2.0f)), 1);
}

// Name: SWCorner (Rectangle)
// Description: Returns the Coordinate of the SW Corner of the Rectangle
// Input: void
// Output: Coordinate
//			SW Corner
Coordinate Rectangle::SWCorner(void) const
{
	return Coordinate((Center.X() + (Length / 2.0f)), (Center.Y() - (Height / 2.0f)), 1);
}

// Name: SECorner (Rectangle)
// Description: Returns the Coordinate of the SE Corner of the Rectangle
// Input: void
// Output: Coordinate
//			SE Corner
Coordinate Rectangle::SECorner(void) const
{
	return Coordinate((Center.X() - (Length / 2.0f)), (Center.Y() - (Height / 2.0f)), 1);
}

// Name: SetCenter (Rectangle)
// Description: Sets the center to a given X and Y
// Input: float X, float Y
//			X: X value to set as center
//			Y: Y Value to set as center
// Output: void
void Rectangle::SetCenter(float X, float Y)
{
	Center = Coordinate(X, Y, 1);
}

// Name: SetCenter (Rectangle)
// Description: Sets the center to a given Coordinate
// Input: Coordinate P
//			P: point to set as center
// Output: void
void Rectangle::SetCenter(Coordinate P)
{
	if (P.type == 0)
		std::cout << "Rectangle Center set to a Vector" << std::endl;
	Center = P;
	Center.type = 1;
}

// Name: SetLength (Rectangle)
// Description: Sets the Length of a rectangle
// Input: float LEN
//			LEN: new length of rectangle
// Output: void
void Rectangle::SetLength(float LEN)
{
	if (LEN < 0)
	{
		LEN *= -1;
		std::cout << "Length was set to a negative" << std::endl;
	}

	Length = LEN;
}

// Name: SetHeight (Rectangle)
// Description: Sets the Length of a rectangle
// Input: float HEIGHT
//			HEIGHT: new Height of rectangle
// Output: void
void Rectangle::SetHeight(float HEIGHT)
{
	if (HEIGHT < 0)
	{
		HEIGHT *= -1;
		std::cout << "Height was set to a negative" << std::endl;
	}

	Height = HEIGHT;
}

// Name: Area (Rectangle)
// Description: returns the rectangle's area
// Input: void
// Output: float
//			returns the rectangle's area
float Rectangle::Area(void)
{
	return (Height*Length);
}

//--------------------------------------------------------------

// Name: Triangle Constructor
// Description: creates a triangle using 6 floats for vertices
// Input: float x1, float y1, float x2, float y2, float x3, float y3
//			x1: x value for Vertex A
//			y1: y value for Vertex A
//			x2: x value for Vertex B
//			y2: y value for Vertex B
//			x3: x value for Vertex C
//			y3: y value for Vertex C
// Output: void
Triangle::Triangle(float x1, float y1, float x2, float y2, float x3, float y3)
{
	A = Coordinate(x1, y1, 1);
	B = Coordinate(x2, y2, 1);
	C = Coordinate(x3, y3, 1);
}

// Name: Triangle Constructor
// Description: creates a triangle using 3 Coordinates for vertices
// Input: Coordinate P1, Coordinate P2, Coordinate P3
//			P1: Coordinate for Vertex A
//			P2: Coordinate for Vertex A
//			P3: Coordinate for Vertex B
// Output: void
Triangle::Triangle(Coordinate P1, Coordinate P2, Coordinate P3) : A(P1), B(P2), C(P3)
{
	if (P1.type == 0)
	{
		A.type = 1;
		std::cout << "Point A set to a Vector" << std::endl;
	}

	if (P2.type == 0)
	{
		B.type = 1;
		std::cout << "Point B set to a Vector" << std::endl;
	}

	if (P3.type == 0)
	{
		C.type = 1;
		std::cout << "Point C set to a Vector" << std::endl;
	}
}

// Name: CENTER (Triangle)
// Description: gets the triangle's Centerpoint
// Input: void
// Output: Coordinate
//			returns the center of the triangle
Coordinate Triangle::CENTER(void) const
{
	float X = (A.X() + B.X() + C.X()) / 3.0f;
	float Y = (A.Y() + B.Y() + C.Y()) / 3.0f;

	return Coordinate(X, Y, 1);
}

// Name: PointA (Triangle)
// Description: gets the triangle's Vertex A
// Input: void
// Output: Coordinate
//			returns Vertex A
Coordinate Triangle::PointA(void) const
{
	return A;
}

// Name: PointB (Triangle)
// Description: gets the triangle's Vertex B
// Input: void
// Output: Coordinate
//			returns Vertex B
Coordinate Triangle::PointB(void) const
{
	return B;
}

// Name: PointC (Triangle)
// Description: gets the triangle's Vertex C
// Input: void
// Output: Coordinate
//			returns Vertex C
Coordinate Triangle::PointC(void) const
{
	return C;
}

// Name: PointASet (Triangle)
// Description: Sets the triangle's Vertex A
// Input: float X, float Y
//			X: new X value of Vertex A
//			Y: new Y value of Vertex A
// Output: void
void Triangle::PointASet(float X, float Y)
{
	A = Coordinate(X, Y, 1);
}

// Name: PointBSet (Triangle)
// Description: Sets the triangle's Vertex B
// Input: float X, float Y
//			X: new X value of Vertex B
//			Y: new Y value of Vertex B
// Output: void
void Triangle::PointBSet(float X, float Y)
{
	B = Coordinate(X, Y, 1);
}

// Name: PointCSet (Triangle)
// Description: Sets the triangle's Vertex C
// Input: float X, float Y
//			X: new X value of Vertex C
//			Y: new Y value of Vertex C
// Output: void
void Triangle::PointCSet(float X, float Y)
{
	C = Coordinate(X, Y, 1);
}

// Name: PointASet (Triangle)
// Description: Sets the triangle's Vertex A
// Input: Coordinate P
//			P: Coordinate to set Vertex A to
// Output: void
void Triangle::PointASet(Coordinate P)
{
	A = P;
}

// Name: PointBSet (Triangle)
// Description: Sets the triangle's Vertex B
// Input: Coordinate P
//			P: Coordinate to set Vertex B to
// Output: void
void Triangle::PointBSet(Coordinate P)
{
	B = P;
}

// Name: PointCSet (Triangle)
// Description: Sets the triangle's Vertex C
// Input: Coordinate P
//			P: Coordinate to set Vertex C to
// Output: void
void Triangle::PointCSet(Coordinate P)
{
	C = P;
}

// Name: Area (Triangle)
// Description: returns the triangle's area
// Input: void
// Output: float
//			The triangle's Area
float Triangle::Area(void) const
{
	float area = (A.X() * (B.Y() - C.Y())) +
				 (B.X() * (C.Y() - A.Y())) +
				 (C.X() * (A.Y() - B.Y()));

	area = (area < 0) ? -area : area;
	area /= 2;

	return area;
}

//--------------------------------------------------------------

// Name: CENTER (LineSegment)
// Description: returns the LineSegment's Center
// Input: void
// Output: Coordinate
//			The LineSegment's Centerpoint
Coordinate LineSegment::CENTER(void)
{
	return (p1 + p2) * 0.5f;
}

// Name: Area (LineSegment)
// Description: returns the LineSegment's area
// Input: void
// Output: float
//			0
float LineSegment::Area(void)
{
	return 0.0f;
}

// Name: P1 (LineSegment)
// Description: returns the LineSegment's first point
// Input: void
// Output: Coordinate
//			The LineSegment's first point
Coordinate LineSegment::P1(void) const
{
	return Coordinate(p1);
}

// Name: P2 (LineSegment)
// Description: returns the LineSegment's second point
// Input: void
// Output: Coordinate
//			The LineSegment's second point
Coordinate LineSegment::P2(void) const
{
	return Coordinate(p2);
}

// Name: P1Set (LineSegment)
// Description: Sets the 1st point
// Input: void
// Output: Coordinate&
//			reference to the 1st point
Coordinate& LineSegment::P1Set(void)
{
	return p1;
}

// Name: P2Set (LineSegment)
// Description: Sets the 2nd point
// Input: void
// Output: Coordinate&
//			reference to the 2nd point
Coordinate& LineSegment::P2Set(void)
{
	return p2;
}

// Name: length (LineSegment)
// Description: gets the length of the LineSegment
// Input: void
// Output: float
//			length of the LineSegment
float LineSegment::length(void)
{
	return (P1() + P2()).length();
}