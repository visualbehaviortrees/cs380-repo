#ifndef SHAPES_HPP
#define SHAPES_HPP

#include "Coordinate.hpp"

class PhysicsBody
{
public:
	PhysicsBody(void) : mass(0), velocity(0) {}
	PhysicsBody(float MASS, Coordinate VEL) : mass(MASS), velocity(VEL) {}
	virtual Coordinate CENTER(void);
	virtual float Area(void);
	void SetVelocity(float = 0, float = 0);
	void SetVelocity(Coordinate&);
	Coordinate VELOCITY(void){ return velocity; }
private:
	float mass;
	Coordinate velocity;
};

class Circle : public PhysicsBody
{
public:
	Circle(float = 0, float = 0, float = 0);
	Circle(Coordinate, float = 0);
	virtual Coordinate CENTER(void) const;
	float RADIUS(void) const;
	void SetCenter(float = 0, float = 0 );
	void SetCenter(Coordinate);
	void SetRadius(float = 0);
	virtual float Area(void);
private:
	float Radius;
	Coordinate Center;
};

class Rectangle : public PhysicsBody
{
public:
	Rectangle(float = 0, float = 0, float = 0, float = 0);
	Rectangle(Coordinate, float = 0, float = 0);
	Rectangle(Coordinate, Coordinate, Coordinate, Coordinate);
	virtual Coordinate CENTER(void) const;
	float LENGTH(void) const;
	float HEIGHT(void) const;
	Coordinate NECorner(void) const;
	Coordinate NWCorner(void) const;
	Coordinate SWCorner(void) const;
	Coordinate SECorner(void) const;
	void SetCenter(float = 0, float = 0);
	void SetCenter(Coordinate);
	void SetLength(float = 0);
	void SetHeight(float = 0);
	virtual float Area(void);
private:
	float Length, Height;
	Coordinate Center;
};

class Triangle : public PhysicsBody
{
public:
	Triangle(float = 0, float = 0, float = 0, float = 0, float = 0, float = 0);
	Triangle(Coordinate, Coordinate, Coordinate);
	virtual Coordinate CENTER(void) const;
	Coordinate PointA(void) const;
	Coordinate PointB(void) const;
	Coordinate PointC(void) const;
	void PointASet(float, float);
	void PointBSet(float, float);
	void PointCSet(float, float);
	void PointASet(Coordinate);
	void PointBSet(Coordinate);
	void PointCSet(Coordinate);
	virtual float Area(void) const;
private:
	Coordinate A, B, C;
};

class LineSegment : public PhysicsBody
{
public:
	LineSegment(void) : p1(), p2() {}
	LineSegment(Coordinate A, Coordinate B) : p1(A), p2(B) {}
	virtual Coordinate CENTER(void);
	virtual float Area(void);
	Coordinate P1(void) const;
	Coordinate P2(void) const;
	Coordinate& P1Set(void);
	Coordinate& P2Set(void);
	float length(void);
private:
	Coordinate p1, p2;
};

#endif