#include "Coordinate.hpp"
using namespace std;

int main (void)
{
	Coordinate V(1, 1, 0);
	Coordinate P(1, 1, 1);	

	cout << V + V << endl;
	cout << V + P << endl;
	cout << P + P << endl;

	return 0;
}