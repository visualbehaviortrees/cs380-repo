#include "Vector.hpp"

// Name:		dot
// Description:	takes the dot product of 2 vectors
// Input:		const Coordinate& u, const Coordinate& v
// Output:		float
//					the dot product
float dot(const Coordinate& u, const Coordinate& v)
{
	return (u.X() * v.X()) + (u.Y() * v.Y());
}

// Name:		Proj
// Description: Proj(v)U (Projection of u onto v)
// Input:		const Coordinate& u, const Coordinate& v
// Output:		Coordinate
//					the projected Vector
Coordinate Proj(Coordinate v, Coordinate u)
{
	return ((dot(u, v) / v.length()) * Normalize(v));
}

// Name:		Foot
// Description: Finds the Foot of a Point on a LineSegment
// Input:		Coordinate P, LineSegment LS
// Output:		Coordinate
//					the projected Vector
Coordinate Foot(Coordinate P, LineSegment LS)
{
	return LS.P1() + Proj((LS.P2() - LS.P1()), (P - LS.P1()));
}

// Name:		PointOnLine
// Description: Determines is a Point is located on a line
// Input:		Coordinate P, LineSegment LS
// Output:		bool
//					is the point on the line?
bool PointOnLine(Coordinate P, LineSegment LS)
{
	return ((P - LS.P1()).length2() + (LS.P2() - P).length2()) == 
			(LS.P2() - LS.P1()).length2();
}