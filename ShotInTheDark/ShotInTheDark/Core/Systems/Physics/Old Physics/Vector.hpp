#ifndef VECTOR2_H
#define VECTOR2_H

#include "Coordinate.hpp"
#include "Shapes.hpp"

float dot(const Coordinate&, const Coordinate&);

Coordinate Proj(Coordinate, Coordinate);

Coordinate Foot(Coordinate P, LineSegment LS);

bool PointOnLine(Coordinate P, LineSegment LS);

#endif