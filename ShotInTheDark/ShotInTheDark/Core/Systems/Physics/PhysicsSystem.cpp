//	File name:		PhysicsSystem.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Dereck Crouse
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include <vector>
#include "PhysicsSystem.hpp"
#include "../../Engine.hpp"
#include "../../Components/Components.h"
#include "Collision.hpp"
#include "../Input/InputSystem.hpp"
#include "../Gameplay/DMGameplay.hpp"
#include "Broadphase.hpp"

extern Engine *ENGINE;
extern Entity *Red, *Blue;

// Broadphase2D* Stats;
Broadphase3D* Statics;

static unsigned check = 0;

void PhysicsSystem::Init()
{
	REGISTER_COMPONENT(Transform);
	REGISTER_COMPONENT(RigidBody);
	REGISTER_COMPONENT(Drawable);

	//Stats = new Broadphase2D;
	Statics = new Broadphase3D;
}

void PhysicsSystem::Update(float p_dt)
{

	// Ignore absurdly large dts
	dt = std::fminf(p_dt, 0.33f);

	int i = 0;
	for (auto it = _entities.begin(); it != _entities.end(); ++it, ++i)
	{
		if (!(*it)->GC(Drawable)->visible || (*it)->GC(Drawable)->clipped)
		{
			continue;
		}

		(*it)->GC(RigidBody)->StartPoint = (*it)->GET_COMPONENT(Transform)->Position;

		auto dir = (*it)->GET_COMPONENT(RigidBody)->direction;

		if (std::abs(dir.x) > 0.00001 || std::abs(dir.y) > 0.00001)
		{
			(*it)->GET_COMPONENT(RigidBody)->vel =
				(*it)->GET_COMPONENT(RigidBody)->direction;

			(*it)->GET_COMPONENT(RigidBody)->vel.Normalize();
			(*it)->GET_COMPONENT(RigidBody)->vel =
				(*it)->GET_COMPONENT(RigidBody)->vel *
				(*it)->GET_COMPONENT(RigidBody)->speed /
				(*it)->GET_COMPONENT(RigidBody)->density;

			(*it)->GET_COMPONENT(Transform)->Position +=
				(*it)->GET_COMPONENT(RigidBody)->vel
				* dt;
		}
	}

	// Update current room for statics
	UpdateCurrentRoom();
}

void PhysicsSystem::Shutdown(void)
{
	// delete Stats;
	delete Statics;
}

void PhysicsSystem::UpdateCurrentRoom()
{
	Statics->PREVIOUSROOM() = Statics->CURRENTROOM();
	Statics->CURRENTROOM() = Statics->GetCurrentRoom();
}

void PhysicsSystem::SendMsg(Entity* a, Entity* b, message m)
{
	switch (m)
	{
	case MSG_Built_Dungeon:
		Statics->Update();
		break;
	}
}