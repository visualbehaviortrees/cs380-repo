//	File name:		PhysicsSystem.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Dereck Crouse
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../../System.hpp"
#include "../../Components/Components.h"
#include "Broadphase.hpp"

class PhysicsSystem : public System
{
public:
	PhysicsSystem() :System(sys_Physics, "Physics System"){}
	void Init(void);
	void Update(float dt);
	void Shutdown(void);
	void SendMsg(Entity* a, Entity*b, message m);

private:
	void UpdateCurrentRoom();
};

void ObjectToWorld(Transform*, RigidBody*, float);
void ObjectToWorld(Transform*);