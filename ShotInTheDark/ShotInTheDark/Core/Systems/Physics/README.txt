 - Complex Polygon Collision Detection has been implemented (SAT)                    (done)
     - Further Testing required to ensure accuracy in all cases                      (done)
     - Returning Minimum Translation Vector (MTV) for Collision Resolution purposes  (done)

 - Optimization set for a later date

 - Broad Phase currently using sphere colliders, will be Dynamic AABB Tree
    - Possibly just Brute Force by Segment Prototype

 - Collision Resolution will be completed by Tuesday (10/28/14) at the latest

 Down The Line:
  - Circle - Circle & Circle - Polygon
  - Joints and tethers
  - Rope Physics