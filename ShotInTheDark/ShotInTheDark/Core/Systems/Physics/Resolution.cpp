//	File name:		Resolution.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Dereck Crouse
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include <vector>
#include "Resolution.hpp"
#include "../../Engine.hpp"
#include "../../Components/Components.h"
#include "Collision.hpp"
#include "PhysicsSystem.hpp"

extern Engine *ENGINE;

void ResolutionSystem::Init()
{
	REGISTER_COMPONENT(Drawable);
	REGISTER_COMPONENT(Transform);
	REGISTER_COMPONENT(RigidBody);
}

void ResolutionSystem::Update(float dt)
{
	for (auto it = _entities.begin(); it != _entities.end(); ++it)
	{
		// Skip Bullets, bases, flags, and static objects
		if ((*it)->GET_COMPONENT(RigidBody)->isStatic)
			continue;

		if ((*it)->GET_COMPONENT(Drawable)->clipped)
			continue;

		for (auto collisions = (*it)->GET_COMPONENT(RigidBody)->collisionList.begin(); collisions != (*it)->GET_COMPONENT(RigidBody)->collisionList.end(); ++collisions)
		{
			if (collisions->object != NULL && !collisions->resolved)
			{
				ResolveCollision(*it, collisions->object, collisions->mtv);
				for (CollisionInfo& resolved : collisions->object->GET_COMPONENT(RigidBody)->collisionList)
					if (resolved.object == *it)
					{
					resolved.resolved = true;
					break;
					}
				collisions->resolved = true;
			}
		}
	}
}

void ResolutionSystem::Shutdown(void)
{

}

void ResolveCollision(Entity* A, Entity* B, const Math::Vector2D& MTV)
{
	Math::Vector2D move = MTV;

	if (A->GetName() == "Player Bullet" || B->GetName() == "Player Bullet")
	{
		return;
	}

	float A_Speed = A->GET_COMPONENT(RigidBody)->speed;
	float B_Speed = B->GET_COMPONENT(RigidBody)->speed;

	if (A->GET_COMPONENT(RigidBody)->ghost || B->GET_COMPONENT(RigidBody)->ghost)
		move = Math::Vector2D();

	if (A->GET_COMPONENT(RigidBody)->isStatic && B->GET_COMPONENT(RigidBody)->isStatic)
	{
		return;
	}

	if (A->GET_COMPONENT(RigidBody)->isStatic)
	{
		B->GET_COMPONENT(Transform)->Position -= move;
	}
	else if (B->GET_COMPONENT(RigidBody)->isStatic)
	{
		A->GET_COMPONENT(Transform)->Position += move;
	}
	else
	{
		if (A_Speed + B_Speed)
		{
			A->GET_COMPONENT(Transform)->Position += (move*(A_Speed / (A_Speed + B_Speed)));
			B->GET_COMPONENT(Transform)->Position -= (move*(B_Speed / (A_Speed + B_Speed)));
		}
		else
		{
			A->GET_COMPONENT(Transform)->Position += move;
			B->GET_COMPONENT(Transform)->Position -= move;
		}
	}
}