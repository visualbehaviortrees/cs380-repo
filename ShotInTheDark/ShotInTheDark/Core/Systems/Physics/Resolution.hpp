//	File name:		Resolution.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Dereck Crouse
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../../System.hpp"
#include "../../Libraries/Math/Math2D.hpp"

class ResolutionSystem : public System
{
public:
	ResolutionSystem() :System(sys_CollisionResolution, "Collision Resolution System"){}
	void Init(void);
	void Update(float dt);
	void Shutdown(void);
};

void ResolveCollision(Entity*, Entity*, const Math::Vector2D&);