//	File name:		Shape.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Dereck Crouse
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "Shape.hpp"

Poly::Poly(int N)
{
	VertNum = N;
	Vertices = new Math::Point2D[VertNum];
}

Poly::Poly(const Poly& P)
{
	VertNum = P.VertNum;
	 Vertices = new Math::Point2D[VertNum];

	for (unsigned i = 0; i<VertNum; ++i)
		Vertices[i] = P.Vertices[i];
}

Poly& Poly::operator= (const Poly& P)
{
	if (VertNum != P.VertNum)
	{
		 delete Vertices;
		VertNum = P.VertNum;

		 Vertices = new Math::Point2D[VertNum];

	}

	for (unsigned i = 0; i<VertNum; ++i)
		Vertices[i] = P.Vertices[i];

	return *this;
}

Poly::Poly (Poly &&in)
	: Vertices(in.Vertices), VertNum(in.VertNum)
{
	in.Vertices = nullptr;
}

void Poly::operator= (Poly &&in)
{
	Vertices = in.Vertices;
	VertNum = in.VertNum;
	in.Vertices = nullptr; 
}

std::ostream& operator<<(std::ostream& out, Poly& A)
{
	out << "Vert#:  " << A.GetVertNum() << std::endl;

	for (unsigned i = 0; i < A.GetVertNum(); ++i)
	{
		out << '(' << A.GetVert(i).X() << ',' << A.GetVert(i).Y() << ')' << std::endl;
	}


	return out << std::endl;
}

Poly& Poly::operator +=(const Math::Vector2D& Vec)
{
	for (unsigned i = 0; i<GetVertNum(); ++i)
		SetVert(i) += Vec;

	return *this;
}

Poly& Poly::operator -=(const Math::Vector2D& Vec)
{
	for (unsigned i = 0; i<GetVertNum(); ++i)
		SetVert(i) -= Vec;

	return *this;
}

Poly operator *(Math::Affine& A, Poly& P)
{
	Poly Ret(P.GetVertNum());

	for (unsigned i = 0; i<P.GetVertNum(); ++i)
		Ret.SetVert(i) = A * P.GetVert(i);

	return Ret;
}

Math::Hcoords HalfPlane(Math::Point2D& A, Math::Point2D& B, Math::Point2D& P)
{
	float a;
	float b;
	float c;

	Math::Vector2D AB(B - A);

	Math::Vector2D m(-AB.y, AB.x);

	if (dot(m, (P - A)) > 0.0F)
		m = -m;

	a = m.x;
	b = m.y;
	c = a * A.x + b * A.y;

	return Math::Hcoords(a, b, -c);
}

Square::Square(void)
{
	Center = Math::Point2D(0, 0);
	Side = 0;
}

Square::Square(const Square& rhs)
{
	Center = rhs.Center;
	Side = rhs.Side;
}

Square::Square(const Math::Point2D& C, float S)
{
	Center = C;
	Side = S;
}

Square::~Square(void)
{ }

Square& Square::operator= (const Square& rhs)
{
	Center = rhs.Center;
	Side = rhs.Side;

	return *this;
}

Math::Point2D& Square::CENTER(void)
{
	return Center;
}

float& Square::SIDE(void)
{
	return Side;
}

const Math::Point2D& Square::CENTER(void) const
{
	return Center;
}

const float& Square::SIDE(void) const
{
	return Side;
}

Rect::Rect(void)
{
	Center = Math::Point2D(0, 0);
	Height = 0;
	Width = 0;
}

Rect::Rect(const Rect& rhs)
{
	Center = rhs.Center;
	Height = rhs.Height;
	Width = rhs.Width;
}

Rect::Rect(const Math::Point2D& C, float H, float W)
{
	Center = C;
	Height = H;
	Width = W;
}

Rect::~Rect(void)
{ }

Rect& Rect::operator= (const Rect& rhs)
{
	Center = rhs.Center;
	Height = rhs.Height;
	Width = rhs.Width;

	return *this;
}