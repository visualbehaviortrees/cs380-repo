//	File name:		Shape.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Dereck Crouse
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

# pragma once

#include "../../Libraries/Math/Math2D.hpp"


class Poly
{
public:
	Poly(int = 0);
	Poly(const Poly&);
	
	Poly& operator= (const Poly&);
	/*http://www.cprogramming.com/c++11/rvalue-references-and-move-semantics-in-c++11.html*/
	Poly (Poly &&in);
	void operator=(Poly &&in);

	unsigned GetVertNum(void) { return VertNum; }
	unsigned GetVertNum(void) const { return VertNum; }

	Math::Point2D GetVert(int i) { return Vertices[i]; }
	Math::Point2D GetVert(int i) const { return Vertices[i]; }

	Math::Point2D& SetVert(int i) { return Vertices[i]; }

	~Poly()  { Vertices != nullptr ? delete Vertices : 0; }

	Math::Point2D CENTER(void);

	Poly& operator +=(const Math::Vector2D&);
	Poly& operator -=(const Math::Vector2D&);

	Math::Point2D& operator[](unsigned i)
	{
		return (i < VertNum) ? Vertices[i] : Vertices[0];
	}

	Math::Point2D operator[](unsigned i) const
	{
		return (i < VertNum) ? Vertices[i] : Vertices[0];
	}

private:
	unsigned VertNum;
	Math::Point2D *Vertices;
};

class Circle
{
public:
	Circle(const Math::Point2D& Cen, float Rad) : Center(Cen), Radius(Rad) {}
	Circle(const Circle& C) : Center(C.Center), Radius(C.Radius) {}
	Circle& operator= (const Circle& C) { Center = C.Center; Radius = C.Radius; }

	Math::Point2D& CENTER(void) { return Center; }
	Math::Point2D CENTER(void) const{ return Center; }

	float& RADIUS(void) { return Radius; }
	float RADIUS(void) const{ return Radius; }

	~Circle(){}
private:
	Math::Point2D Center;
	float Radius;
};

class LineSegment
{
public:
	LineSegment(void)
	{
		Start = Math::Point2D();
		End = Math::Point2D();
		Normal = Math::Vector2D();
	}

	LineSegment(const Math::Point2D& A, const Math::Point2D& B) : Start(A), End(B)
	{
		ComputeNormal();
	}

	LineSegment(const LineSegment& LS) : Start(LS.Start), End(LS.End), Normal(LS.Normal) {}

	LineSegment& operator= (const LineSegment& LS)
	{
		Start = LS.Start; End = LS.End; Normal = LS.Normal; return *this;
	}

	void ComputeNormal(void)
	{
		Normal.x = -(Start.y - End.y);
		Normal.y = Start.x - End.x; Normal.Normalize();
	}

	Math::Point2D START(void) const { return Start; }
	Math::Point2D& START(void) { return Start;  ComputeNormal(); }

	Math::Point2D END(void) const { return End; }
	Math::Point2D& END(void) { return End;  ComputeNormal(); }

	Math::Vector2D NORMAL(void) const { return Normal; }

private:
	Math::Point2D Start, End;
	Math::Vector2D Normal;
};

std::ostream& operator<<(std::ostream&, Poly&);
Poly operator *(Math::Affine&, Poly&);

Math::Hcoords HalfPlane(Math::Point2D& A, Math::Point2D& B, Math::Point2D& P);

class Square
{
public:
	Square(void);
	Square(const Square&);
	Square(const Math::Point2D&, float);
	~Square(void);
	Square& operator= (const Square&);

	Math::Point2D& CENTER(void);
	float& SIDE(void);

	const Math::Point2D& CENTER(void) const;
	const float& SIDE(void) const;

private:
	Math::Point2D Center;
	float Side;
};

class Rect
{
public:
	Rect(void);
	Rect(const Rect&);
	Rect(const Math::Point2D&, float, float);
	~Rect(void);
	Rect& operator= (const Rect&);

	Math::Point2D& CENTER(void) { return Center; }
	const Math::Point2D& CENTER(void) const { return Center; }

	float& HEIGHT(void){ return Height; }
	float HEIGHT(void) const { return Height; }

	float& WIDTH(void) { return Width; }
	float WIDTH(void) const { return Width; }

private:
	Math::Point2D Center;
	float Height, Width;
};