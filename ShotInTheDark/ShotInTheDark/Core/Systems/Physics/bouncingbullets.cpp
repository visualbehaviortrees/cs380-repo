//	File name:		bouncingbullets.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Dereck Crouse
//	
//	All content � 2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "bouncingbullets.hpp"
#include "../../Engine.hpp"
#include "../../Components/Components.h"
#include "Collision.hpp"

// Returns the Intersection time
// Takes in the Point, Vector that the point will move, and the LineSegment we are checking
float AnimP2LS(const Math::Point2D& Pt, const Math::Vector2D& Vec, const LineSegment& LS)
{
	float slopeLS, slopeVec;
	Math::Point2D PoI;

	// if both the line segment and vertical vectors have undefined slopes
	if (FloatCheck(LS.START().x - LS.END().x) == false &&
		FloatCheck(Vec.x) == false)
	{
		return -1.0f;
	}


	// if it is a verical line segment && not a vertical vector
	if (!FloatCheck(LS.START().x - LS.END().x))
	{
		// y = mx + b for Pt & Vec
		slopeVec = (Vec.y / Vec.x);
		float b = Pt.y - (slopeVec*Pt.x);
		PoI.y = (slopeVec*LS.START().x) + b;
		PoI.x = LS.START().x;

		if (FloatBetween(LS.START().y, PoI.y, LS.END().y) &&
			FloatBetween(Pt.x, PoI.x, Pt.x + Vec.x))
		{
			return (PoI.x - Pt.x) / (Vec.x);
		}
		return -1.0f;

	}


	// if it is a vertical vector && not a vertical line segment
	else if (!FloatCheck(Vec.x))
	{
		// y = mx + b for LS
		slopeLS = (LS.START().y - LS.END().y) / (LS.START().x - LS.END().x);
		float b = LS.START().y - (slopeLS*LS.START().x);
		PoI.y = (slopeLS*Pt.x) + b;
		PoI.x = Pt.x;

		if (FloatBetween(Pt.y, PoI.y, Pt.y + Vec.y) &&
			FloatBetween(LS.START().x, PoI.x, LS.END().x))
			return (PoI.y - Pt.y) / (Vec.y);
		return -1.0f;
	}

	// if neither line segment is vertical
	else
	{
		slopeVec = Vec.y / Vec.x;
		slopeLS = (LS.START().y - LS.END().y) / (LS.START().x - LS.END().x);

		PoI.x = (Pt.y - (slopeVec*Pt.x) - LS.START().y + (slopeLS*LS.START().x)) / (slopeLS - slopeVec);
		PoI.y = (slopeLS*PoI.x) + LS.START().y - (slopeLS*LS.START().x);

		if (FloatBetween(Pt.x, PoI.x, Pt.x + Vec.x) &&
			FloatBetween(LS.START().x, PoI.x, LS.END().x))
			return (PoI.x - Pt.x) / (Vec.x);
		return -1.0f;
	}
}

// Returns the Intersection time
// Takes in the Point, Vector that the point will move, & the Poly we are checking
// the non-const holds the contact surface
float AnimP2Poly(const Math::Point2D& Pt, const Math::Vector2D& Vec, const Poly& Shape, LineSegment& CS)
{
	float OldVal = 2.0f;
	int Side = -1;
	std::vector<LineSegment> Originals;

	for (unsigned i = 0; i < Shape.GetVertNum(); ++i)
	{
		LineSegment temp(Shape[i], Shape[(i + 1) % Shape.GetVertNum()]);
		Originals.push_back(temp);
	}

	for (unsigned i = 0; i < Originals.size(); ++i)
	{
		float NewVal = AnimP2LS(Pt, Vec, Originals[i]);
		if (NewVal != -1.0f && NewVal < OldVal)
		{
			OldVal = NewVal;
			Side = i;
		}
	}

	if (OldVal != 2.0f)
	{
		CS = Originals[Side];
		return OldVal;
	}

	return -1.0f;
}

// Returns the Intersection time
// Takes in the Circle, Vector that the point will move, and the LineSegment we are checking
float AnimCircle2LS(const Circle& C, const Math::Vector2D& Vec, const LineSegment& LS)
{
	float slopeLS, slopeVec;
	Math::Point2D PoI;

	Math::Vector2D Normal = (Math::dot(LS.NORMAL(), (LS.START() - C.CENTER())) > 0.0f) ? -LS.NORMAL() : LS.NORMAL();

	LineSegment NewLS(LS.START() + Normal*C.RADIUS(), LS.END() + Normal*C.RADIUS());

	// if both the line segment and vertical vectors have undefined slopes
	if (FloatCheck(NewLS.START().x - NewLS.END().x) == false &&
		FloatCheck(Vec.x) == false)
	{
		return -1.0f;
	}


	// if it is a verical line segment && not a vertical vector
	if (!FloatCheck(NewLS.START().x - NewLS.END().x))
	{
		// y = mx + b for C.CENTER() & Vec
		slopeVec = (Vec.y / Vec.x);
		float b = C.CENTER().y - (slopeVec*C.CENTER().x);
		PoI.y = (slopeVec*NewLS.START().x) + b;
		PoI.x = NewLS.START().x;

		if (FloatBetween(NewLS.START().y, PoI.y, NewLS.END().y) &&
			FloatBetween(C.CENTER().x, PoI.x, C.CENTER().x + Vec.x))
			return (PoI.x - C.CENTER().x) / (Vec.x);
		return -1.0f;

	}


	// if it is a vertical vector && not a vertical line segment
	else if (!FloatCheck(Vec.x))
	{
		// y = mx + b for NewLS
		slopeLS = (NewLS.START().y - NewLS.END().y) / (NewLS.START().x - NewLS.END().x);
		float b = NewLS.START().y - (slopeLS*NewLS.START().x);
		PoI.y = (slopeLS*C.CENTER().x) + b;
		PoI.x = C.CENTER().x;

		if (FloatBetween(C.CENTER().y, PoI.y, C.CENTER().y + Vec.y) &&
			FloatBetween(NewLS.START().x, PoI.x, NewLS.END().x))
			return (PoI.y - C.CENTER().y) / (Vec.y);
		return -1.0f;
	}

	// if neither line segment is vertical
	else
	{
		slopeVec = Vec.y / Vec.x;
		slopeLS = (NewLS.START().y - NewLS.END().y) / (NewLS.START().x - NewLS.END().x);

		PoI.x = (C.CENTER().y - (slopeVec*C.CENTER().x) - NewLS.START().y + (slopeLS*NewLS.START().x)) / (slopeLS - slopeVec);
		PoI.y = (slopeLS*PoI.x) + NewLS.START().y - (slopeLS*NewLS.START().x);

		if (FloatBetween(C.CENTER().x, PoI.x, C.CENTER().x + Vec.x) &&
			FloatBetween(NewLS.START().x, PoI.x, NewLS.END().x))
			return (PoI.x - C.CENTER().x) / (Vec.x);
		return -1.0f;
	}
}

// Returns the Intersection time
// Takes in the Circle, Vector that the point will move, the Poly we are checking
// the non-const holds the contact surface
float AnimCircle2Poly(const Circle& C, const Math::Vector2D& Vec, const Poly& Shape, LineSegment& CS)
{
	float OldVal = 2.0f;
	int Side = -1;
	std::vector<LineSegment> Originals;

	for (unsigned i = 0; i < Shape.GetVertNum(); ++i)
	{
		LineSegment temp(Shape[i], Shape[(i + 1) % Shape.GetVertNum()]);
		Originals.push_back(temp);
	}

	for (unsigned i = 0; i < Originals.size(); ++i)
	{
		float NewVal = AnimCircle2LS(C, Vec, Originals[i]);
		if (NewVal != -1.0f && NewVal < OldVal)
		{
			OldVal = NewVal;
			Side = i;
		}
	}

	if (OldVal != 2.0f)
	{
		CS = Originals[Side];
		return OldVal;
	}

	return -1.0f;
}

// Returns the Intersection time
// Takes in the Point, Vector that the point will move, and the LineSegment we are checking
// the non-const holds the Point after reflection and the reflection vector
float ReflectP2LS(const Math::Point2D& Pt, const Math::Vector2D& Vec, const LineSegment& LS, Math::Point2D& NewPt, Math::Vector2D& Reflection)
{
	float IT = AnimP2LS(Pt, Vec, LS);

	if (IT == -1.0f)
		return IT;

	NewPt = Math::Point2D(Pt) + Vec.Normalize();
	Reflection = NewPt - Pt;

	Math::Vector2D Normal = (Math::dot(LS.NORMAL(), (LS.START() - Pt)) > 0) ? LS.NORMAL() : -LS.NORMAL();
	Reflection = Reflection - Normal * (2.0f * Math::dot(Reflection, Normal));
	NewPt += Reflection;

	return IT;
}

// Returns the Intersection time
// Takes in the Point, Vector that the point will move, and the Poly we are checking
// the non-const holds the Point after reflection, the reflection vector and the contact surface
float ReflectP2Poly(const Math::Point2D& Pt, const Math::Vector2D& Vec, const Poly& Shape, Math::Point2D& NewPt, Math::Vector2D& Reflection, LineSegment& CS)
{
	float OldVal = 2.0f;
	int Side = -1;
	Math::Point2D TempNewPt;
	Math::Vector2D TempReflection;
	std::vector<LineSegment> Originals;

	for (unsigned i = 0; i < Shape.GetVertNum(); ++i)
		Originals.push_back(LineSegment(Shape[i], Shape[(i + 1) % Shape.GetVertNum()]));


	for (unsigned i = 0; i < Originals.size(); ++i)
	{
		float NewVal = ReflectP2LS(Pt, Vec, Originals[i], TempNewPt, TempReflection);
		if (NewVal != -1.0f && NewVal < OldVal)
		{
			OldVal = NewVal;
			Side = i;
			NewPt = TempNewPt;
			Reflection = TempReflection;
		}
	}

	if (OldVal != 2.0f)
	{
		CS = Originals[Side];
		return OldVal;
	}

	return -1.0f;
}

// Returns the Intersection time
// Takes in the Point, Vector that the point will move, and the LineSegment we are checking
// the non-const holds the Point after reflection and the reflection vector
float ReflectCircle2LS(const Circle& C, const Math::Vector2D& Vec, const LineSegment& LS, Math::Point2D& NewPt, Math::Vector2D& Reflection)
{
	Math::Vector2D Normal = (Math::dot(LS.NORMAL(), (LS.START() - C.CENTER())) > 0.0f) ? -LS.NORMAL() : LS.NORMAL();
	LineSegment NewLS(LS.START() + Normal*C.RADIUS(), LS.END() + Normal*C.RADIUS());
	return ReflectP2LS(C.CENTER(), Vec, NewLS, NewPt, Reflection);
}

// Returns the Intersection time
// Takes in the Point, Vector that the point will move, and the Poly we are checking
// the non-const holds the Point after reflection, the reflection vector and the contact surface
float ReflectCircle2Poly(const Circle& C, const Math::Vector2D& Vec, const Poly& Shape, Math::Point2D& NewPt, Math::Vector2D& Reflection, LineSegment& CS)
{
	float OldVal = 2.0f;
	int Side = -1;
	Math::Point2D TempNewPt;
	Math::Vector2D TempReflection;
	std::vector<LineSegment> Originals;

	for (unsigned i = 0; i < Shape.GetVertNum(); ++i)
	{
		LineSegment temp(Shape[i], Shape[(i + 1) % Shape.GetVertNum()]);
		Originals.push_back(temp);
	}

	for (unsigned i = 0; i < Originals.size(); ++i)
	{
		float NewVal = ReflectCircle2LS(C, Vec, Originals[i], TempNewPt, TempReflection);
		if (NewVal != -1.0f)
		{
			OldVal = (NewVal < OldVal) ? NewVal : OldVal;
			Side = i;
			NewPt = TempNewPt;
			Reflection = TempReflection;
		}
	}

	if (OldVal != 2.0f)
	{
		CS = Originals[Side];
		return OldVal;
	}

	return -1.0f;
}

// Returns the Distance
// Takes in the Start Point, Look At Vector(non-normalized), & List of Entities to check
// the two non-const hold the contact surface and a pointer to the corresponding Entity
float Raycast(const Math::Point2D& Pt, const Math::Vector2D& Vec, const std::vector<Entity*>& ObList, LineSegment& CS, Entity* Hit)
{
	// Filter out all clipped Objects
	std::vector<Entity*> NewList = ObList;
	FilterList(NewList);

	// Build a Bounding Box for the Pt & Vec
	Math::Point2D PtVecCen(Pt);
	PtVecCen += Vec*0.5f;
	float PtVecH = Math::abs(Vec.y), PtVecW = Math::abs(Vec.x);
	Rect PtVecBB(PtVecCen, PtVecH, PtVecW);

	// Filter out all objects that are not colliding with PtVecBB
	for (unsigned i = 0; i < NewList.size();)
	{
		if (NewList[i]->HasComponent(MC_Drawable))
		{
			if (!AABB_CollisionCheck(PtVecBB, NewList[i]))
			{
				NewList[i] = ObList[ObList.size() - 1];
				NewList.pop_back();
				continue;
			}
		}
		++i;
	}

	float OldVal = 2.0f;
	int Side = -1;
	LineSegment TempCS;

	for (unsigned i = 0; i < NewList.size(); ++i)
	{
		Poly A = NewList[i]->GC(Transform)->transform * NewList[i]->GC(Drawable)->mpMesh->Poly;
		float NewVal = AnimP2Poly(Pt, Vec, A, TempCS);
		if (NewVal != -1.0f && NewVal < OldVal)
		{
			OldVal = NewVal;
			CS = TempCS;
			Hit = NewList[i];
		}
	}

	if (OldVal != 2.0f)
	{
		return OldVal * Vec.Length();
	}

	return -1.0f;
}

// uses EPSILON to determine if a float is near enough to zero to be considered false for boolean checking
bool FloatCheck(float val)
{
	return !((val < EPSILON) && (val > -EPSILON));
}

// returns true if val lies between x1 and x2
bool FloatBetween(float x1, float val, float x2)
{
	return ((val <= x1) && (val >= x2) || (val <= x2) && (val >= x1));
}

// Takes a list of Entity pointers and eliminates all clipped objects
void FilterList(std::vector<Entity*>& ObList)
{
	for (unsigned i = 0; i < ObList.size();)
	{
		if (ObList[i]->HasComponent(MC_Drawable))
		{
			if (ObList[i]->GC(Drawable)->clipped == true)
			{
				ObList[i] = ObList[ObList.size() - 1];
				ObList.pop_back();
				continue;
			}
		}
		++i;
	}
}

float LS2LS(LineSegment& LS1, LineSegment& LS2)
{
	Math::Point2D P, Q;
	Math::Vector2D R, S;

	P = LS1.START();
	Q = LS2.START();

	R = LS1.END() - LS1.START();
	S = LS2.END() - LS2.START();

	if (Cross2D(R, S) == 0)
	{
		return 2.0f;
	}

	float retval = Cross2D((Q - P), S) / Cross2D(R, S);

	return retval >= 0.0f ? retval : -1.0f;
}

float Cross2D(const Math::Hcoords& V, const Math::Hcoords& W)
{
	return ((V.x*W.y) - (V.y*W.x));
}

Math::Point2D Foot(LineSegment& LS, Math::Point2D& P)
{
	Math::Point2D foot;

	// k = ((y2 - y1) * (x3 - x1) - (x2 - x1) * (y3 - y1)) / ((y2 - y1) ^ 2 + (x2 - x1) ^ 2)
	float k = (((LS.END().y - LS.START().y) * (P.x - LS.START().x)) -
		((LS.END().x - LS.START().x) * (P.y - LS.START().y))) /
		((LS.END().y - LS.START().y)*(LS.END().y - LS.START().y) +
		(LS.END().x - LS.START().x)*(LS.END().x - LS.START().x));

	// x4 = x3 - k * (y2 - y1)
	foot.x = P.x - (k * (LS.END().y - LS.START().y));

	// y4 = y3 + k * (x2 - x1)
	foot.y = P.y + (k * (LS.END().x - LS.START().x));

	return foot;
}