//	File name:		bouncingbullets.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Dereck Crouse
//	
//	All content � 2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "Shape.hpp"
#include <vector>
#include "../../System.hpp"

#ifndef EPSILON
#define EPSILON 0.00001f
#endif

// Returns the Intersection time
// Takes in the Point, Vector that the point will move, and the LineSegment we are checking
float AnimP2LS(const Math::Point2D&, const Math::Vector2D&, const LineSegment&);

// Returns the Intersection time
// Takes in the Point, Vector that the point will move, the Poly we are checking, and a place to hold the contact surface
float AnimP2Poly(const Math::Point2D&, const Math::Vector2D&, const Poly&, LineSegment&);

// Returns the Intersection time
// Takes in the Circle, Vector that the point will move, and the LineSegment we are checking
float AnimCircle2LS(const Circle&, const Math::Vector2D&, const LineSegment&);

// Returns the Intersection time
// Takes in the Circle, Vector that the point will move, the Poly we are checking
// the non-const holds the contact surface
float AnimCircle2Poly(const Circle&, const Math::Vector2D&, const Poly&, LineSegment&);

// Returns the Intersection time
// Takes in the Point, Vector that the point will move, and the LineSegment we are checking
// the non-const holds the Point after reflection and the reflection vector
float ReflectP2LS(const Math::Point2D&, const Math::Vector2D&, const LineSegment&, Math::Point2D&, Math::Vector2D&);

// Returns the Intersection time
// Takes in the Point, Vector that the point will move, and the Poly we are checking
// the non-const holds the Point after reflection, the reflection vector and the contact surface
float ReflectP2Poly(const Math::Point2D&, const Math::Vector2D&, const Poly&, Math::Point2D&, Math::Vector2D&, LineSegment&);

// Returns the Intersection time
// Takes in the Point, Vector that the point will move, and the LineSegment we are checking
// the non-const holds the Point after reflection and the reflection vector
float ReflectCircle2LS(const Circle&, const Math::Vector2D&, const LineSegment&, Math::Point2D&, Math::Vector2D&);

// Returns the Intersection time
// Takes in the Point, Vector that the point will move, and the Poly we are checking
// the non-const holds the Point after reflection, the reflection vector and the contact surface
float ReflectCircle2Poly(const Circle&, const Math::Vector2D&, const Poly&, Math::Point2D&, Math::Vector2D&, LineSegment&);

// Returns the Distance
// Takes in the Start Point, Look At Vector(non-normalized), & List of Entities to check
// the two non-const hold the contact surface and a pointer to the corresponding Entity
float Raycast(const Math::Point2D&, const Math::Vector2D&, const std::vector<Entity*>&, LineSegment&, Entity*);

// uses EPSILON to determine if a float is near enough to zero to be considered false for boolean checking
bool FloatCheck(float);

// tells whether the middle float lies between the two outer floats
bool FloatBetween(float, float, float);

// Takes a list of Entity pointers and eliminates all clipped objects
void FilterList(std::vector<Entity*>&);

float LS2LS(LineSegment& LS1, LineSegment& LS2);

float Cross2D(const Math::Hcoords& V, const Math::Hcoords& W);

Math::Point2D Foot(LineSegment& LS, Math::Point2D& P);