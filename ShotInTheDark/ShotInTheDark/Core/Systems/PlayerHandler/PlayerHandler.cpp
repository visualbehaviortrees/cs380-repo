//	File name:		PlayerHandler.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Luc Kadletz
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "PlayerHandler.hpp"
#include "../../Components/Components.h"
#include "../../Engine.hpp"
#include "../Physics/Collision.hpp"
#include "../Input/InputSystem.hpp"
#include <math.h>
#include "Libraries/ADLib/ADLib.hpp"

#include "Systems/Gameplay/PowerupHandler.hpp"
#include "../../Systems/Gameplay/DungeonGameplay.h"


void Cheat_CheckAddPowerups(Entity* player)
{
	if (CheckKeyTriggered(KEY_9))
		player->GET_COMPONENT(Powerup)->AddEffect( GETSYS(PowerupHandler)
		->GetPowerupEffect("Cheat_GodMode", PowerupHandler::POWERUP_CHEAT), player);
	if (CheckKeyTriggered(KEY_0))
	{
		PowerupEffect *p = player->GET_COMPONENT(Powerup)->RemoveLastEffect(player);
		//GETSYS(PowerupHandler)->CreatePowerupEntityFromEffect(p,
		//	player->GET_COMPONENT(Transform)->Position + Vector2D(0.0f, 5.0f));
	}
}

PlayerHandler::PlayerHandler() : System(sys_PlayerHandler, "Player Handler System")
{
	_playerTurnSpeed = 3.0f / 1.0f;

}

PlayerHandler::~PlayerHandler()
{


}

Entity* PlayerHandler::CreatePlayer(Point2D pos, int playernum)
{
	// PLAYER
	Player* player = new Player(playernum);
	player->Load_Texture_Set();
	// TEAM
	Team *playerTeam = new Team();
	playerTeam->teamID = (Teams)(Team_Player_1 + (playernum - 1));
	// HEALTH
	Health *playerHP = new Health(1000.0f);
		playerHP->safeTime = 1.00f;
	// SPRITE
  Sprite* pSprite = new Sprite(player->tex_down.CurrentSprite());
	// LIGHT
	Light *pLight = new Light();
	pLight->intensity = player->base_light;
	//pLight->r = player->GetColor().r;
	//pLight->g = player->GetColor().g;
	//pLight->b = player->GetColor().b;

	// TRANSFORM
	Transform* playerTran = new Transform();
	playerTran->Position = (pos);
	playerTran->rotation = 0.0f;
	playerTran->scale.Set(1.f, 1.f);
	// RIGIDBODY
	RigidBody* playerRBody = new RigidBody();
	playerRBody->direction.Set(0.0f, 0.0f);
	playerRBody->isStatic = false;
	playerRBody->speed = player->base_speed;
	// DRAWABLE
	Drawable* playerDraw = new Drawable();
	*playerDraw = Drawable::RedScheme;
	playerDraw->layer = 6;
	playerDraw->mpMesh = Mesh::Get_Mesh("RedPlayer");
	Powerup* playerPup = new Powerup();

	Score* playerScore = new Score();

	Audio* playerAudio = new Audio(playernum);
	playerAudio->Narrate(Audio::AE_JOIN);

  ParticleEffect* pEffect = new ChaseEffect(playerTran, 100, 0.5f);

  Behavior* pBehavior = new Behavior(ET_PLAYER);

  Movement *pMovement = new Movement;

	Entity *playerEntity = ENGINE->Factory->Compose(14,
		player,
		playerTran,
		playerRBody,
		playerDraw,
		playerHP,
		playerTeam,
		pLight,
		pSprite,
		playerPup,
		playerScore,
		pEffect,
		playerAudio,
		pBehavior,
		pMovement
		);
	playerEntity->SetName(std::string("Player"));
	pMovement->PreInit(playerEntity);

	return playerEntity;
}

void PlayerHandler::Init(void)
{
	REGISTER_COMPONENT(Player);
	REGISTER_COMPONENT(RigidBody);
	REGISTER_COMPONENT(Transform);
	REGISTER_COMPONENT(Drawable);
	REGISTER_COMPONENT(Team);
	REGISTER_COMPONENT(Health);
	REGISTER_COMPONENT(Sprite);
	REGISTER_COMPONENT(Light);
	REGISTER_COMPONENT(Powerup);
}

void PlayerHandler::Update(float dt)
{
	for (Entity *it : _entities)
	{
		Player *player = (it)->GET_COMPONENT(Player);
		RigidBody *Rigid = (it)->GET_COMPONENT(RigidBody);
		Transform *Trans = (it)->GET_COMPONENT(Transform);
		Drawable *Draw = (it)->GET_COMPONENT(Drawable);
		Team *playerTeam = (it)->GET_COMPONENT(Team);
		Health *health = (it)->GET_COMPONENT(Health);
		Light *pLight = (it)->GET_COMPONENT(Light);
		Mesh* mesh = Draw->mpMesh;
		Sprite* pSprite = (it)->GET_COMPONENT(Sprite);
		Powerup* pPup = (it)->GET_COMPONENT(Powerup);

		if (player->state == Player_Dead)
		{
			if (health->GetHealthPercent() > 0)
				player->state = Player_Alive;
			else
				continue;
		}
		else if (player->state == Player_Alive)
		{
			Trans->rotation = 0.0f;
			Rigid->ghost = false;
			Draw->visible = true;
		}

		Cheat_CheckAddPowerups(it);

		// HEALTH ==============================================================
		health->Update(dt);

    //Handling death
		if (health->health <= 0.0f)
		{
			// You died :(
			player->state = Player_Dead;
			// Play a sound
			it->GET(Audio)->Play(Audio::AE_HURT);
			it->GET(Audio)->Narrate(Audio::AE_DIE);
			//audioSys->PlaySound(SFX::PLAYER_DEATH);
			// Stop ourselves from moving
			Rigid->direction.Set(0.0f, 0.0f);
			Rigid->ghost = true;
			// And look dead
			pSprite->ChangeTexture(player->tex_up.CurrentSprite());
			Trans->rotation = 180.0f;
			// drop your last powerup
			PowerupEffect *p = it->GET_COMPONENT(Powerup)->RemoveLastEffect(it);
			GETSYS(PowerupHandler)->CreatePowerupEntityFromEffect(p,
				it->GET_COMPONENT(Transform)->Position);
			return;
		}
		// Blink if we're invulnerable
		if (health->timeSinceLastHit < health->safeTime)
		{
      // Make the player's transparency flash
      if(pSprite->Color.a == 1.f)
        pSprite->Color.a = 0.f;
      else if (pSprite->Color.a == 0.f)
        pSprite->Color.a = 1.f;
      
      
      // This was the red light flash. Players found it jarring so I switched to opacity.
      /*
      Color tempColor = pLight->GetCurrentColor();
      Color flashColor = tempColor;
      flashColor.r = 1.0f;
      flashColor.g = flashColor.g == 1.0f ? 0.0f : 1.0f;
      flashColor.b = flashColor.b == 1.0f ? 0.0f : 1.0f;
      pLight->SetCurrentColor(flashColor);
      */
		}
		else
		{
			const float brightness = 0.25f + 0.75f / _entities.size();
      pLight->SetCurrentColor(glm::vec4(brightness, brightness, brightness, 1.0f));
      // This sets the player back to full opacity
      pSprite->Color.a =  1.f;
		}


		// Update the sprite direction  ========================
		if (player->controls.Shoot() )
		{
			if (player->controls.Look_Right() || Rigid->direction.x > 0.5f)
			{
				player->tex_right.Update(dt);
				pSprite->ChangeTexture(player->tex_right.CurrentSprite());
			}
			else if (player->controls.Look_Left() || Rigid->direction.x < -0.5f)
			{
				player->tex_left.Update(dt);
				pSprite->ChangeTexture(player->tex_left.CurrentSprite());
			}
			if (player->controls.Look_Down() || Rigid->direction.y < -0.5f)
			{
				player->tex_down.Update(dt);
				pSprite->ChangeTexture(player->tex_down.CurrentSprite());
			}
			else if (player->controls.Look_Up() || Rigid->direction.y > 0.5f)
			{
				player->tex_up.Update(dt);
				pSprite->ChangeTexture(player->tex_up.CurrentSprite());
			}
		}
		else
		{
			if (player->controls.Move_Right() || Rigid->direction.x > 0.5f)
			{
				player->tex_right.Update(dt);
				pSprite->ChangeTexture(player->tex_right.CurrentSprite());
			}
			else if (player->controls.Move_Left() || Rigid->direction.x < -0.5f)
			{
				player->tex_left.Update(dt);
				pSprite->ChangeTexture(player->tex_left.CurrentSprite());
			}
			if (player->controls.Move_Down() || Rigid->direction.y < -0.5f)
			{
				player->tex_down.Update(dt);
				pSprite->ChangeTexture(player->tex_down.CurrentSprite());
			}
			else if (player->controls.Move_Up() || Rigid->direction.y > 0.5f)
			{
				player->tex_up.Update(dt);
				pSprite->ChangeTexture(player->tex_up.CurrentSprite());
			}

		}

		// MOVEMENT ==============================================
		const float Acceleration = 1.0f;
		Rigid->direction.Set(0.0f, 0.0f);
		//if (player->state == Player_Alive)
		{
			if (player->controls.Move_Right())
			{
				Rigid->direction.x += player->controls.Move_Right();
			}
			else if (player->controls.Move_Left())
				Rigid->direction.x += -player->controls.Move_Left();

			if (player->controls.Move_Up())
				Rigid->direction.y += player->controls.Move_Up();

			else if (player->controls.Move_Down())
				Rigid->direction.y += -player->controls.Move_Down();


			// Compute a new velocity
			Vector2D newVel = Rigid->vel + Rigid->direction * player->moveSpeed * Acceleration;

			if (newVel.SquareLength() > 0.0f)
			{
				if (newVel.SquareLength() > player->moveSpeed * player->moveSpeed)
				{
					newVel.Normalize();
					newVel *= player->moveSpeed;
				}
				Rigid->speed = newVel.Length();
				Rigid->direction = newVel;
			}
			
		}
		// UpdatePlayerLight(it, dt);


		// SHOOTING ============================================================
		const float knockback = 0.3f;
		player->reloadCounter -= dt;
		// If we shoot, we want our sprite to face that direction, otherwise we want the movement direction
		Vector2D shoot_direction(player->controls.Look_Right() - player->controls.Look_Left(),
			player->controls.Look_Up() - player->controls.Look_Down());


		// How much of the player's movement direction goes onto the bullet
		const float player_direction_transfer = 0.00f; 
		// How much of the player's speed goes into the bullet
		const float player_speed_transfer = 0.0f;
		//shoot_direction += (Rigid->direction * player_direction_transfer);


		if (player->controls.Shoot() &&
			shoot_direction.SquareLength() != 0.0f &&
			player->reloadCounter < 0.0f &&
			player->state == Player_Alive)
		{
			// start the reload timer
			player->reloadCounter = player->reloadSpeed;
			// Start with a single bullet and pass it to powerups to handle
			std::vector<Entity*> bullets;


			bullets.push_back(CreateBullet(it, shoot_direction));
			pPup->OnShoot(it, bullets);
			// Play a sound
			it->GET(Audio)->Play(Audio::AE_ATTACK);

			// Apply knockback
			// Rigid->direction += -knockback * shoot_direction;
		}


	}

}

// Makes players brighter if they're standing still
void PlayerHandler::UpdatePlayerLight(Entity* player, float dt)
{
	const float movement_intensity_penalty = Player::base_light * Player::base_light * 0.5f;
	const float standing_change_rate = 1.0f;
	float current_brightness = player->GET_COMPONENT(Light)->intensity;

	// 
	float movement_brightness_change = movement_intensity_penalty *
		player->GET_COMPONENT(RigidBody)->direction.SquareLength();
	float target_brightness = Player::base_light + movement_brightness_change;

	// Smoothly move current to target
	current_brightness += (target_brightness - current_brightness) * standing_change_rate * dt;

	player->GET_COMPONENT(Light)->intensity = current_brightness;
}


void PlayerHandler::SendMsg(Entity *a, Entity *b, message m)
{
	switch (m)
	{
	case (MSG_Door) :
		if (a->GET(Player)->state == Player_Alive)
			EnterDoor(a, b);
		else if (a->GET(Player)->state == Player_InDoor)
			ExitDoor(a, b);
		break;
	case MSG_Built_Dungeon:
		for (Entity * p : _entities)
		{
			p->GET_COMPONENT(Player)->state = Player_Alive;
			// pEnt->GET_COMPONENT(RigidBody)->ghost = false;
			p->GET_COMPONENT(Drawable)->visible = true;
			p->GET_COMPONENT(Team)->teamID =
				(Teams)(Team_Player_1 + (p->GET(Player)->GetNum() - 1));
		}
		break;
		
	}
}

Entity* PlayerHandler::CreateBullet(Entity* player, Vector2D vel)
{
	const float base_speed = 7.5f;

	Transform *Trans = (player)->GET_COMPONENT(Transform);
	Team *playerTeam = (player)->GET_COMPONENT(Team);

	Bullet *bullet = new Bullet();
	bullet->damage = 1.0f;
	bullet->pierces = false;
	bullet->particle_info.count = 20;
	Parent *parent = new Parent(player);
	Transform *trans = new Transform();
	trans->scale.Set(.25f, .25f);
	trans->rotation = Trans->rotation;
	trans->Position = Trans->Position;
	RigidBody *rigid = new RigidBody();
	rigid->isStatic = false;
	rigid->ghost = true;
	rigid->direction = vel;
	rigid->speed = vel.Length() + base_speed; // previously 7.5f;
	rigid->isStatic = false;
	Audio *audio = new Audio();
	Drawable *draw = new Drawable();
	draw->layer = 100;
	Sprite* sprite = new Sprite();
	sprite->ChangeTexture("muffin_projectile.png");

	Team *team = new Team();
	team->teamID = playerTeam->teamID;

	Light* light = new Light(100.0f);

//	ParticleEffect* effect = new CircleEffect(50, -1);


	draw->mpMesh = Mesh::Get_Mesh("BlueBullet");
	Entity * theBullet = ENGINE->Factory->Compose(9,
		light,
		parent,
		team,
		bullet,
		rigid,
		trans,
		sprite,
		audio,
		draw
	);
	theBullet->SetName(std::string("Bullet"));
	return theBullet;
}



void PlayerHandler::ExitDoor(Entity* pEnt, Entity* door)
{
	pEnt->GET_COMPONENT(Player)->state = Player_Alive;
	// pEnt->GET_COMPONENT(RigidBody)->ghost = false;
	pEnt->GET_COMPONENT(Drawable)->visible = true;
	pEnt->GET_COMPONENT(Team)->teamID = 
		(Teams)(Team_Player_1 + (pEnt->GET(Player)->GetNum() - 1));
}
void PlayerHandler::EnterDoor(Entity* pEnt, Entity* door)
{
	pEnt->GET_COMPONENT(Player)->state = Player_InDoor;
	pEnt->GET_COMPONENT(RigidBody)->ghost = true;
	pEnt->GET_COMPONENT(Drawable)->visible = true;
	pEnt->GET_COMPONENT(Team)->teamID = Team_ALL;
	pEnt->GET_COMPONENT(Transform)->Position
		= door->GET_COMPONENT(Transform)->Position;
}



void PlayerHandler::Shutdown(void)
{


}
