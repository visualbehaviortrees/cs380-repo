//	File name:		PlayerHandler.hpp
//	Project name:	Shot In The Dark
//	Author(s):		(Insert Name Here)
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../../System.hpp"

class PlayerHandler : public System
{
public:
  PlayerHandler();
  ~PlayerHandler();
  void Init(void);
  void Update(float dt);
  void Shutdown(void);
  void SendMsg(Entity *, Entity *, message);


  Entity* CreateBullet(Entity *player, Vector2D vel);

  static Entity* CreatePlayer(Point2D pos, int playernum = 0);
  

private:
	void EnterDoor(Entity* player, Entity* door);
	void ExitDoor(Entity* player, Entity* door);

	void UpdatePlayerLight(Entity* player, float dt);

	float _playerTurnSpeed;
};