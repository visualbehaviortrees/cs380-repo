//	File name:		TestSystem.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include <vector>
#include "TestSystem.hpp"
#include "../../Engine.hpp"
#include "../../Components/Components.h"
#include "Systems/Physics/PhysicsSystem.hpp"

extern Broadphase3D* Statics;
static Poly linePoly = Poly(4);

static std::vector<Entity*> NonStatics;

void TestSystem::Init()
{
	REGISTER_COMPONENT(Drawable);
	REGISTER_COMPONENT(Transform);
	REGISTER_COMPONENT(RigidBody);
	// Stats->Update();
	Statics->Update();
}

void TestSystem::Update(float dt)
{
	if (Statics->UPDATE())
	{
		// Stats->MaxEntities() = 25;
		// Stats->InitializeBroadphase(_entities, 200);
		// Stats->UPDATE() = false;
		Statics->MaxEntities() = 10;
		Statics->InitializeBroadphase(_entities, 200);
		Statics->UPDATE() = false;

		int Room;
		if ((Room = Statics->GetCurrentRoom()) >= 0)
		{
			for (auto it : _entities)
			{
				if (AABB_CollisionCheck(Statics->Nodes[Room].first.COLLIDER(), it))
				{
					it->GET_COMPONENT(Drawable)->clipped = false;
					Statics->CURRENTROOM() = Room;
					Statics->PREVIOUSROOM() = Room;
				}
				else
				{
					it->GET_COMPONENT(Drawable)->clipped = true;
				}
			}
		}
		else
		{
			for (auto it : _entities)
			{
				it->GET_COMPONENT(Drawable)->clipped = false;
			}
		}
	}

	// clear out old collision list data
	for (auto it : _entities)
		it->GET_COMPONENT(RigidBody)->collisionList.clear();

	FilterForMesh(_entities);

	NonStatics = _entities;
	NonStaticFilter(NonStatics);

	if (Statics->CURRENTROOM() != Statics->PREVIOUSROOM())
	{
		int Room;
		if ((Room = Statics->GetCurrentRoom()) >= 0)
		{
			for (auto it : _entities)
			{
				if (AABB_CollisionCheck(Statics->Nodes[Room].first.COLLIDER(), it))
				{
					it->GET_COMPONENT(Drawable)->clipped = false;
					Statics->CURRENTROOM() = Room;
					Statics->PREVIOUSROOM() = Room;
				}
				else
				{
					it->GET_COMPONENT(Drawable)->clipped = true;
				}
			}
		}
		else
		{
			for (auto it : _entities)
			{
				it->GET_COMPONENT(Drawable)->clipped = false;
			}
		}
	}


	for (auto it : NonStatics)
	{
		RigidBody* rigid = it->GET_COMPONENT(RigidBody);

		if (it->GetName() == "Player Bullet")
		{
			int i = (2 + 2);
			++i;
		}

		for (unsigned i = 0; i < Statics->CollisionSubLists.size(); ++i)
		{
			if (AABB_CollisionCheck(Statics->Nodes[i].first.COLLIDER(), it))
			{
				for (unsigned j = 0; j < Statics->CollisionSubLists[i].size(); ++j)
				{
					if (AABB_CollisionCheck(Statics->Nodes[i].second[j].COLLIDER(), it))
					{
						for (auto it2 : Statics->CollisionSubLists[i][j])
						{
							Math::Vector2D MTV;

							if (CollisionCheck(it, it2, MTV))
							{
								rigid->collisionList.push_back(CollisionInfo(it2, MTV*0.5f));
								it2->GET_COMPONENT(RigidBody)->collisionList.push_back(CollisionInfo(it, -(MTV*0.5f)));
							}
						}
					}
				}
			}
		}

		if (NonStatics.size() > 1)
			for (auto it2 = NonStatics.begin() + 1; it2 != NonStatics.end(); ++it2)
			{
			Math::Vector2D MTV;
			if (CollisionCheck(it, (*it2), MTV))
			{
				rigid->collisionList.push_back(CollisionInfo((*it2), MTV*0.5f));
				(*it2)->GET_COMPONENT(RigidBody)->collisionList.push_back(CollisionInfo(it, -(MTV*0.5f)));
			}
			}
	}

	//	for(auto e1 = _entities.begin(); e1 != _entities.end(); ++e1)
	//	{
	//     
	//		// loop through all objects after the current one
	//		for(auto e2 = e1+1; e2 != _entities.end(); ++e2)
	//		{
	// 
	//			if(rigid->isStatic && (*e2)->GC(RigidBody)->isStatic)
	//				continue;
	//			Math::Vector2D MTV;
	//			if(CollisionCheck(*e1, *e2, MTV))
	//			{
	//				rigid->collisionList.push_back(CollisionInfo(*e2, MTV));
	//				(*e2)->GET_COMPONENT(RigidBody)->collisionList.push_back(CollisionInfo(*e1, -MTV));
	//			}
	//		}
	//	}

}

void NonStaticFilter(std::vector<Entity*>& list)
{
	std::vector<Entity*> temp;

	for (unsigned i = 0; i < list.size(); ++i)
	{
		if (!(list[i])->GET_COMPONENT(RigidBody)->isStatic)
			temp.push_back(list[i]);
	}

	list = temp;
}

void FilterForMesh(std::vector<Entity*>& EV)
{
	for (unsigned i = 0; i < EV.size(); ++i)
	{
		if (EV[i]->GET_COMPONENT(Drawable)->mpMesh == nullptr)
		{
			EV[i] = EV.back();
			EV.pop_back();
		}
	}
}

// std::vector<Stuff*> SV;
// 
// for (auto it : copy)
// {
// 	SV.push_back(new Stuff);
// 	SV->data = copy->data;
// }