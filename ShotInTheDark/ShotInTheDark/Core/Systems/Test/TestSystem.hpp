//	File name:		TestSystem.hpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#pragma once

#include "../../System.hpp"

class TestSystem : public System
{
public:
	TestSystem(void) :System(sys_Test, "Test System"){}
	void Init(void);
	void Update(float dt);
	void Shutdown(void){}
};

void NonStaticFilter(std::vector<Entity*>&);
void FilterForMesh(std::vector<Entity*>&);