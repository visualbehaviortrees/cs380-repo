//	File name:		main.cpp
//	Project name:	Shot In The Dark
//	Author(s):		Allan Deutsch
//	
//	All content � 2014-2015 DigiPen (USA) Corporation, all rights reserved.

#include "stdafx.h"
#include "Components/Components.h"
#include "Libraries\JSON\JSONHeaders.hpp"
//#include "Engine.hpp"
#include <stdio.h>

#define JSON 0
#define LEVEL 1
std::string Level;
// using the engine systems
int main(void)
{
  // This is used to initialize the Engine. it should only happen once.
  ENGINE = new Engine();
  // This is used to add a system to the Engine.
  // For yours just stick them in the Engine.cpp Init() function.
  // Make sure it is in whatever order you want it to be in, IE graphics last.
  // ENGINE->AddSystem(new TestSystem()); <<<< Sample code from Init()
  // This initializes the Engine. It should also add your systems to it.
  try
  {
    ENGINE->Init();
  }
  catch(char* str)
  {
    std::cout << str << std::endl;
    getchar();
    return 0;
  }
  #if JSON
    JSONReader reader;
    Level = SetLevel(LEVEL);
    reader.JSONLoad(Level.c_str());
    reader.SerializeParent();
  #endif

  ENGINE->MainLoop(); // infinite loop

  
  return 0;
}