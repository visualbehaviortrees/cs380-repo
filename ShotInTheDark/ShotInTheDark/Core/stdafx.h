#pragma once

// engine
#include "Engine.hpp"

/* All preincluded in Engine.hpp
#include "Component.hpp"
#include "DebugDefines.hpp"
#include "Entity.hpp"
#include "ObjectManager.hpp"
#include "System.hpp"
#include "ObjectFactory.h"
*/

#include "Libraries/ADLib/ADLib.hpp"
// random stuff we won't change very often
#include "Libraries/Math/Math2D.hpp"
#include "Libraries/AntTweakBar/AntTweakBar.hpp"
#include "Libraries/JSON/JSONHeaders.hpp"

#include "Systems/Graphics/GraphicsMath.hpp"
#include "Systems/Graphics/LoadShader.hpp"

#include "Systems/Audio/AudioSystem.hpp"

#include "Libraries/Memory_Manager/Page_Allocator.hpp"

extern Page_Allocator* P_A;











