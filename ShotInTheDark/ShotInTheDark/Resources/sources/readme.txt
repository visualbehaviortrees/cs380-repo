Drop assets into one of these folders
run rebuild_atlases.bat to rebuild the assets for use in the game
If a folder can't be atlassed into a 2048 x 2048 png
	Add a folder to hold the assets
	Add the proper line to rebuild_atlases.bat
	Add the *.atlas to atlases.txt
	Add the *.png to textures.txt
Run rebuild_atlases and run the game to test