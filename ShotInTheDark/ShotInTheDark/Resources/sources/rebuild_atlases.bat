@echo off

AtlasGen.exe ./bg/ 2048 1024 ./../ backgrounds.png backgrounds.atlas
AtlasGen.exe ./environment/ 2048 2048 ./../ environment.png environment.atlas 
AtlasGen.exe ./hud/ 2048 4000 ./../ hud.png hud.atlas
AtlasGen.exe ./menus/ 2048 4000 ./../ menus.png menus.atlas
AtlasGen.exe ./buttons/ 2048 2048 ./../ buttons.png buttons.atlas
AtlasGen.exe ./players/wizard_orange/ 2048 2048 ./../ wizard_orange.png wizard_orange.atlas
AtlasGen.exe ./players/wizard_purple/ 2048 2048 ./../ wizard_purple.png wizard_purple.atlas
AtlasGen.exe ./players/wizard_teal/ 2048 2048 ./../ wizard_teal.png wizard_teal.atlas
AtlasGen.exe ./players/wizard_pink/ 2048 2048 ./../ wizard_pink.png wizard_pink.atlas
AtlasGen.exe ./powerups/ 1024 2048 ./../ powerups.png powerups.atlas
AtlasGen.exe ./enemies/ 2048 2048 ./../ enemies.png enemies.atlas

@echo building Normals...

AtlasGen.exe ./bg_Normal/ 2048 1024 ./../ backgrounds_Normal.png backgrounds_Normal.atlas
AtlasGen.exe ./environment_Normal/ 2048 2048 ./../ environment_Normal.png environment_Normal.atlas 
AtlasGen.exe ./players_Normal/wizard_orange/ 2048 2048 ./../ wizard_orange_Normal.png wizard_orange_Normal.atlas
AtlasGen.exe ./players_Normal/wizard_purple/ 2048 2048 ./../ wizard_purple_Normal.png wizard_purple_Normal.atlas
AtlasGen.exe ./players_Normal/wizard_teal/ 2048 2048 ./../ wizard_teal_Normal.png wizard_teal_Normal.atlas
AtlasGen.exe ./players_Normal/wizard_pink/ 2048 2048 ./../ wizard_pink_Normal.png wizard_pink_Normal.atlas
AtlasGen.exe ./powerups_Normal/ 1024 2048 ./../ powerups_Normal.png powerups_Normal.atlas
AtlasGen.exe ./enemies_Normal/ 2048 2048 ./../ enemies_Normal.png enemies_Normal.atlas

@pause