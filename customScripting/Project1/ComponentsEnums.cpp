#include <fstream>
#include "ComponentsEnums.h"



#undef DEFINE_COMPONENT
#define DEFINE_COMPONENT(X,Y) #X ,
#undef COMPONENTS_INCLUDE_H

const char * EnumCmpToStr[] = 
{
#include "ComponentsInclude.h"
  "EnumComponent_Max"
};

Enum generateCmpEnum()
{
  std::ofstream Instructions{ "Components.txt", std::ios_base::out };
  Enum cmpEnum{ "EnumeratedComponent" };
  for (int i = 0; i < EnumComponent_Max; ++i)
  {
    cmpEnum.addValue(i, EnumCmpToStr[i]);
    Instructions << EnumCmpToStr[i] << std::endl;
  }
  return cmpEnum;
}

const Enum& getCmpEnum()
{
  static Enum cmpEnum = generateCmpEnum();
  return cmpEnum;
}