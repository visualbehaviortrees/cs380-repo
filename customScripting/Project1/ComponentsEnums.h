#pragma once
#include "Enum.h"
#undef ADD_MEMBER_VAR
#define ADD_MEMBER_VAR(X,Y,Z)
#undef DEFINE_COMPONENT
#define DEFINE_COMPONENT(X,Y) EnumComponent_##X ,
#undef COMPONENTS_INCLUDE_H

enum EnumeratedComponents
{
#include "ComponentsInclude.h"
  EnumComponent_Max
};

const char *EnumCmpToStr[];

Enum generateCmpEnum();
const Enum& getCmpEnum();
#undef COMPONENTS_INCLUDE_H

#undef DEFINE_COMPONENT
