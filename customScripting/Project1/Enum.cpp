
#include "Enum.h"
int Enum::toenum(const std::string& string) const 
{ 
  auto it = enums.find(string);
  if (it != enums.end())
    return (*it).second; 
  return -1;
}