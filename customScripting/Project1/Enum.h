#pragma once
#include <string>
#include <map>

struct Enum
{
  Enum(const std::string& Name) : name(Name) {}
  const std::string name;
  const std::string& tostr(int value) const { return strings.at(value); }
  int toenum(const std::string& string) const;
  void addValue(int value, const std::string& string)
  {
    strings.emplace(value, string);
    enums.emplace(string, value);
  }
private:
  std::map<int, std::string> strings;
  std::map<std::string, int> enums;
};