#pragma once
#include "Enum.h"



#undef INSTRUCTION
#define INSTRUCTION(NAME) NAME ,

enum Instructions
{
#include "Instructions.h"
  INST_MAX,
  INVALID_INSTRUCTION = -1
};



const char *InstEnumToStr[];

Enum generateInstEnum();
const Enum& getInstEnum();

#undef INSTRUCTION
