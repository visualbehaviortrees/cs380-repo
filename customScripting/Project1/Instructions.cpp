#include <fstream>
#include "InstructionEnums.h"


#undef INSTRUCTION
#define INSTRUCTION(NAME) #NAME ,

const char *InstEnumToStr[] =
{
#include "Instructions.h"
  "INVALID_INSTRUCTION"
};

Enum generateInstEnum()
{
  std::ofstream Instructions{ "instructions.txt", std::ios_base::out };
  Enum instEnum{ "Instruction Enums" };
  for (int i = 0; i < INST_MAX; ++i)
  {
    instEnum.addValue(i, InstEnumToStr[i]);
    Instructions << InstEnumToStr[i] << std::endl;
  }


  return instEnum;
}

const Enum& getInstEnum()
{
  static Enum cmpEnum = generateInstEnum();
  return cmpEnum;
}