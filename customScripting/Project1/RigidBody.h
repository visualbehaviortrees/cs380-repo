DEFINE_COMPONENT(RigidBody,
  ADD_MEMBER_VAR(float, Mass, 1.f)
  ADD_MEMBER_VAR(float, Density, 1.f)
  ADD_MEMBER_VAR(float, Velocity, 0.f)
  )