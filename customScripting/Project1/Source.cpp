#include <iostream>
#include "engine.h"
using std::cout;
using std::endl;
int main()
{
  Engine engine = Engine::get(new Interpreter);
  engine.Init();
  engine.Update();
  return 0;
}
