DEFINE_COMPONENT(Transform,
  ADD_MEMBER_ARR(float, pos, 3)
  ADD_MEMBER_ARR(float, rot, 3)
  ADD_MEMBER_ARR(float, scale, 3)
  )