#include <iostream>
#include "engine.h"

Entity * createPlayer()
{
  Entity * e{ new Entity };
  e->id = Engine::get().entities.size();
  e->addComponent(new Status);
  e->addComponent(new Skills);
  Engine::get().entities.emplace_back(e);
  return e;
}

void Engine::Init()
{
  for (int i = 0; i < 6; ++i)
    createPlayer();
}

void Engine::Update()
{
  char c;
  std::string s;
  Interpreter *interpreter = nullptr;
  while (true)
  {
    if (interpreter == nullptr)
    {
      delete interpreter;
      interpreter = nullptr;
    }
    std::cout << "To enter an instruction, enter I. \nTo run a script, enter S: \n";
    std::cin >> c;
    switch (c)
    {
    case 'I':
      std::cout << "Valid instructions can be found in Instructions.txt.\nEnter an instruction:\n";
      std::cin >> s;
      console->interpret(getInstEnum().toenum(s));
      break;
    case 'S':
      std::cout << "Enter a script filename:\n";
      std::cin >> s;
      interpreter = new Interpreter{ s };
      interpreter->parse();
      break;
    default:
      std::cout << "Invalid choice. Try again.\n";
      break;
    }
  }
}


void Interpreter::interpret(int instr)
{
  int predeclare;
  switch (instr)
  {
  case ADD:
    push(pop() + pop());
    break;
  case SUBTRACT:
    push(pop() + pop());
    break;
  case MULTIPLY:
    push(pop() * pop());
    break;
  case DIVIDE:
    push(pop() / pop());
    break;
  case MODULO:
    push(pop() % pop());
    break;
  case VAR:
    if (usecin)
      std::cin >> predeclare;
    else
      script >> predeclare;
    push(predeclare);
    break;
  case PRINT:
    std::cout << stack.top() << std::endl;
    break;
  case GET_HP:
    push(Engine::get().entities[pop()]->C(Status)->hp);
    break;
  case SET_HP:
    predeclare = pop();
    Engine::get().entities[predeclare]->C(Status)->hp = pop();
    break;
  case GET_MANA:
    push(Engine::get().entities[pop()]->C(Status)->mana);
    break;
  case SET_MANA:
    predeclare = pop();
    Engine::get().entities[predeclare]->C(Status)->mana = pop();
    break;
  case GET_LEVEL:
    push(Engine::get().entities[pop()]->C(Status)->level);
    break;
  case SET_LEVEL:
    predeclare = pop();
    Engine::get().entities[predeclare]->C(Status)->level = pop();
    break;
  case GET_WISDOM:
    push(Engine::get().entities[pop()]->C(Skills)->wisdom);
    break;
  case SET_WISDOM:
    predeclare = pop();
    Engine::get().entities[predeclare]->C(Skills)->wisdom = pop();
    break;
  case GET_AGILITY:
    push(Engine::get().entities[pop()]->C(Skills)->agility);
    break;
  case SET_AGILITY:
    predeclare = pop();
    Engine::get().entities[predeclare]->C(Skills)->agility = pop();
    break;
  case GET_STRENGTH:
    push(Engine::get().entities[pop()]->C(Skills)->strength);
    break;
  case SET_STRENGTH:
    predeclare = pop();
    Engine::get().entities[predeclare]->C(Skills)->strength = pop();
    break;
  case GET_DEXTERITY:
    push(Engine::get().entities[pop()]->C(Skills)->dexterity);
    break;
  case SET_DEXTERITY:
    predeclare = pop();
    Engine::get().entities[predeclare]->C(Skills)->dexterity = pop();
    break;
  case CLEAR:
    while (stack.size())
      stack.pop();
    break;

  default:
  case INVALID_INSTRUCTION:
    std::cout << "invalid instruction encountered.\n";
    break;
    
  }
}

void Interpreter::parse()
{
  int instrVal;
  std::string instruction;
  while (!script.eof())
  {
    script >> instruction;
    instrVal = getInstEnum().toenum(instruction);
    if (instrVal == -1)
    {
      std::cout << "Invalid Instruction: " << instruction << std::endl;
      break;
    }
    interpret(instrVal);
  }
}

