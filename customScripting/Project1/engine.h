#pragma once

#include <array>
#include <vector>
#include <memory>
#include <cassert>
#include <stack>
#include <fstream>
#include "ComponentsEnums.h"
#include "ComponentClasses.h"
#include "InstructionEnums.h"
struct Entity
{
  unsigned id;
  std::array<std::unique_ptr<ComponentBase>, EnumComponent_Max> components;
#define C(CMP) \
  getComponent<CMP>(EnumComponent_##CMP)

  template<typename T>
  T * getComponent(EnumeratedComponents ec)
  {
    assert(components[ec].get());
    return reinterpret_cast<T *>(components[ec].get());
  }
  bool hasComponent(EnumeratedComponents ec)
  {
    return components[ec].get() != nullptr;
  }

  void addComponent(ComponentBase *cmp)
  {
    assert( !(hasComponent( cmp->type )) );
    components[cmp->type] = std::unique_ptr<ComponentBase>(cmp);
  }
};

// NOTE: Adds the Entity to the Engine.
Entity * createPlayer();
struct Interpreter;

struct Engine
{
  template<typename... Args>
  static Engine& get(Args... args)
  {
    static Engine* engine{ new Engine(std::forward<Args>(args)...) };
    return *engine;
  }
  std::vector<Entity *> entities;
  void Init();
  void Update();
private:
  Interpreter* console;
  Engine() {}
  Engine(Interpreter *interpreter) : console(interpreter) { }
};

struct Interpreter
{
  Interpreter(std::string scriptFile) : script(scriptFile) {}
  Interpreter(std::ifstream &scriptFile)  { script.swap(scriptFile); }
  Interpreter() : usecin(true){}
  bool usecin = false;
  std::ifstream script;
  std::stack<int> stack;
  void push(int val) { stack.push(val); }
  int pop() { int val = stack.top(); stack.pop(); return val; }
  void interpret(int instr);
  void parse();
};
